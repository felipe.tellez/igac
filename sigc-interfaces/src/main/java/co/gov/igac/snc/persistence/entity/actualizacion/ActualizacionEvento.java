package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ACTUALIZACION_EVENTO database table.
 *
 * @author franz.gamba
 */
@Entity
@Table(name = "ACTUALIZACION_EVENTO")
public class ActualizacionEvento implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4373793311485240233L;
    private Long id;
    private Long actaDocumentoId;
    private String descripcion;
    private Date fecha;
    private Date fechaLog;
    private String nombre;
    private String tipo;
    private String usuarioLog;

    private Actualizacion actualizacion;
    private List<EventoAsistente> eventoAsistentes;

    public ActualizacionEvento() {
        this.eventoAsistentes = new ArrayList<EventoAsistente>();
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_EVENTO_ID_GENERATOR", sequenceName =
        "ACTUALIZACION_EVENTO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_EVENTO_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACTA_DOCUMENTO_ID", precision = 10)
    public Long getActaDocumentoId() {
        return this.actaDocumentoId;
    }

    public void setActaDocumentoId(Long actaDocumentoId) {
        this.actaDocumentoId = actaDocumentoId;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    //bi-directional many-to-one association to EventoAsistente
    @OneToMany(mappedBy = "actualizacionEvento")
    public List<EventoAsistente> getEventoAsistentes() {
        return this.eventoAsistentes;
    }

    public void setEventoAsistentes(List<EventoAsistente> eventoAsistentes) {
        this.eventoAsistentes = eventoAsistentes;
    }

}
