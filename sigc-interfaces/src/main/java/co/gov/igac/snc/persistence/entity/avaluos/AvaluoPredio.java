package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * The persistent class for the AVALUO_PREDIO database table.
 *
 * @modified felipe.cadena::se cambio el tipo de dato de Long a Double en el campo avaluoComercial
 * @modified christian.rodriguez::agregado constructor AvaluoPredio(SolicitudPredio)
 * @modified christian.rodriguez::agregados métodos equals y hashcode
 * @modified christian.rodriguez::cambiado el tipo Long de todos los valores (de moneda) a Double
 */
@Entity
@Table(name = "AVALUO_PREDIO", schema = "SNC_AVALUOS")
public class AvaluoPredio implements Serializable {

    private static final long serialVersionUID = -3751315480218453018L;

    private Long id;
    private Long anio;
    private Double areaConstruccion;
    private Double areaTerreno;
    private Double avaluoComercial;
    private String circuloRegistral;
    private String condicionJuridica;
    private SolicitanteSolicitud contacto;
    private String destino;
    private String direccion;
    private Date fechaLog;
    private String justificacionValorAjustado;
    private String libro;
    private String numeroPredial;
    private String numeroRegistro;
    private String numeroRegistroAnterior;
    private String observaciones;
    private String oficina;
    private String pagina;
    private Long predioId;
    private String requiereTramiteCatastral;
    private String tipoAvaluoPredio;
    private String tipoInmueble;
    private String tomo;
    private String usuarioLog;
    private Double valorCotizacion;
    private Double valorCotizacionAjustado;
    private Double valorIva;
    private Double valorPredio;
    private Double valorPreliminar;
    private Double valorTotalAnexos;
    private Double valorTotalConstrucciones;
    private Double valorTotalCultivos;
    private Double valorTotalMaquinaria;
    private Double valorTotalTerreno;

    private Avaluo avaluo;
    private Municipio municipio;
    private Departamento departamento;

    public AvaluoPredio() {
    }

    public AvaluoPredio(SolicitudPredio predio) {
        this.anio = predio.getAnio();
        this.areaConstruccion = predio.getAreaConstruccion();
        this.areaTerreno = predio.getAreaTerreno();
        this.circuloRegistral = predio.getCirculoRegistral();
        this.condicionJuridica = predio.getCondicionJuridica();
        this.destino = predio.getDestino();
        this.direccion = predio.getDireccion();
        this.libro = predio.getLibro();
        this.numeroPredial = predio.getNumeroPredial();
        this.numeroRegistro = predio.getNumeroRegistro();
        this.numeroRegistroAnterior = predio.getNumeroRegistroAnterior();
        this.oficina = predio.getOficina();
        this.pagina = predio.getPagina();
        this.predioId = predio.getPredioId();
        this.tipoAvaluoPredio = predio.getTipoAvaluoPredio();
        this.tipoInmueble = predio.getTipoInmueble();
        this.tomo = predio.getTomo();

        this.avaluoComercial = 0D;
        this.requiereTramiteCatastral = ESiNo.NO.getCodigo();

        this.valorIva = 0.0;
        this.valorPredio = 0.0;
        this.valorPreliminar = 0.0;
        this.valorTotalAnexos = 0.0;
        this.valorTotalConstrucciones = 0.0;
        this.valorTotalCultivos = 0.0;
        this.valorTotalMaquinaria = 0.0;
        this.valorTotalTerreno = 0.0;

        this.departamento = predio.getDepartamento();
        this.municipio = predio.getMunicipio();
    }

    public AvaluoPredio(Predio predio) {
        this.areaConstruccion = predio.getAreaConstruccion();
        this.areaTerreno = predio.getAreaTerreno();
        this.condicionJuridica = predio.getCondicionPropiedad();
        this.destino = predio.getDestino();
        this.numeroPredial = predio.getNumeroPredial();
        this.numeroRegistro = predio.getNumeroRegistro();
        this.numeroRegistroAnterior = predio.getNumeroRegistroAnterior();
        this.predioId = predio.getId();
        this.tipoAvaluoPredio = predio.getTipoAvaluoPredio();

        this.avaluoComercial = 0D;
        this.requiereTramiteCatastral = ESiNo.NO.getCodigo();

        this.valorIva = 0.0;
        this.valorPredio = 0.0;
        this.valorPreliminar = 0.0;
        this.valorTotalAnexos = 0.0;
        this.valorTotalConstrucciones = 0.0;
        this.valorTotalCultivos = 0.0;
        this.valorTotalMaquinaria = 0.0;
        this.valorTotalTerreno = 0.0;
        this.valorCotizacion = 0.0;
        this.valorCotizacionAjustado = 0.0;

        this.departamento = predio.getDepartamento();
        this.municipio = predio.getMunicipio();
    }

    @Id
    @SequenceGenerator(name = "AVALUO_PREDIO_ID_GENERATOR", sequenceName = "AVALUO_PREDIO_ID_SEQ",
        allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AVALUO_PREDIO_ID_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAnio() {
        return this.anio;
    }

    public void setAnio(Long anio) {
        this.anio = anio;
    }

    @Column(name = "AREA_CONSTRUCCION")
    public Double getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "AREA_TERRENO")
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "AVALUO_COMERCIAL")
    public Double getAvaluoComercial() {
        return this.avaluoComercial;
    }

    public void setAvaluoComercial(Double avaluoComercial) {
        this.avaluoComercial = avaluoComercial;
    }

    //bi-directional many-to-one association to Avaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    @Column(name = "CIRCULO_REGISTRAL")
    public String getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(String circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "CONDICION_JURIDICA")
    public String getCondicionJuridica() {
        return this.condicionJuridica;
    }

    public void setCondicionJuridica(String condicionJuridica) {
        this.condicionJuridica = condicionJuridica;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTA_SOLICITANTE_SOLICITUD_ID")
    public SolicitanteSolicitud getContacto() {
        return this.contacto;
    }

    public void setContacto(SolicitanteSolicitud contacto) {
        this.contacto = contacto;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO")
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "JUSTIFICACION_VALOR_AJUSTADO")
    public String getJustificacionValorAjustado() {
        return this.justificacionValorAjustado;
    }

    public void setJustificacionValorAjustado(String justificacionValorAjustado) {
        this.justificacionValorAjustado = justificacionValorAjustado;
    }

    public String getLibro() {
        return this.libro;
    }

    public void setLibro(String libro) {
        this.libro = libro;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO")
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_REGISTRO")
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "NUMERO_REGISTRO_ANTERIOR")
    public String getNumeroRegistroAnterior() {
        return this.numeroRegistroAnterior;
    }

    public void setNumeroRegistroAnterior(String numeroRegistroAnterior) {
        this.numeroRegistroAnterior = numeroRegistroAnterior;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getOficina() {
        return this.oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public String getPagina() {
        return this.pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    @Column(name = "PREDIO_ID")
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    @Column(name = "REQUIERE_TRAMITE_CATASTRAL")
    public String getRequiereTramiteCatastral() {
        return this.requiereTramiteCatastral;
    }

    public void setRequiereTramiteCatastral(String requiereTramiteCatastral) {
        this.requiereTramiteCatastral = requiereTramiteCatastral;
    }

    @Column(name = "TIPO_AVALUO_PREDIO")
    public String getTipoAvaluoPredio() {
        return this.tipoAvaluoPredio;
    }

    public void setTipoAvaluoPredio(String tipoAvaluoPredio) {
        this.tipoAvaluoPredio = tipoAvaluoPredio;
    }

    @Column(name = "TIPO_INMUEBLE")
    public String getTipoInmueble() {
        return this.tipoInmueble;
    }

    public void setTipoInmueble(String tipoInmueble) {
        this.tipoInmueble = tipoInmueble;
    }

    public String getTomo() {
        return this.tomo;
    }

    public void setTomo(String tomo) {
        this.tomo = tomo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_COTIZACION")
    public Double getValorCotizacion() {
        return this.valorCotizacion;
    }

    public void setValorCotizacion(Double valorCotizacion) {
        this.valorCotizacion = valorCotizacion;
    }

    @Column(name = "VALOR_COTIZACION_AJUSTADO")
    public Double getValorCotizacionAjustado() {
        return this.valorCotizacionAjustado;
    }

    public void setValorCotizacionAjustado(Double valorCotizacionAjustado) {
        this.valorCotizacionAjustado = valorCotizacionAjustado;
    }

    @Column(name = "VALOR_IVA")
    public Double getValorIva() {
        return this.valorIva;
    }

    public void setValorIva(Double valorIva) {
        this.valorIva = valorIva;
    }

    @Column(name = "VALOR_PREDIO")
    public Double getValorPredio() {
        return this.valorPredio;
    }

    public void setValorPredio(Double valorPredio) {
        this.valorPredio = valorPredio;
    }

    @Column(name = "VALOR_PRELIMINAR")
    public Double getValorPreliminar() {
        return this.valorPreliminar;
    }

    public void setValorPreliminar(Double valorPreliminar) {
        this.valorPreliminar = valorPreliminar;
    }

    @Column(name = "VALOR_TOTAL_ANEXOS")
    public Double getValorTotalAnexos() {
        return this.valorTotalAnexos;
    }

    public void setValorTotalAnexos(Double valorTotalAnexos) {
        this.valorTotalAnexos = valorTotalAnexos;
    }

    @Column(name = "VALOR_TOTAL_CONSTRUCCIONES")
    public Double getValorTotalConstrucciones() {
        return this.valorTotalConstrucciones;
    }

    public void setValorTotalConstrucciones(Double valorTotalConstrucciones) {
        this.valorTotalConstrucciones = valorTotalConstrucciones;
    }

    @Column(name = "VALOR_TOTAL_CULTIVOS")
    public Double getValorTotalCultivos() {
        return this.valorTotalCultivos;
    }

    public void setValorTotalCultivos(Double valorTotalCultivos) {
        this.valorTotalCultivos = valorTotalCultivos;
    }

    @Column(name = "VALOR_TOTAL_MAQUINARIA")
    public Double getValorTotalMaquinaria() {
        return this.valorTotalMaquinaria;
    }

    public void setValorTotalMaquinaria(Double valorTotalMaquinaria) {
        this.valorTotalMaquinaria = valorTotalMaquinaria;
    }

    @Column(name = "VALOR_TOTAL_TERRENO")
    public Double getValorTotalTerreno() {
        return this.valorTotalTerreno;
    }

    public void setValorTotalTerreno(Double valorTotalTerreno) {
        this.valorTotalTerreno = valorTotalTerreno;
    }

    // -----------------------------------------------------------------------------------
    /**
     * Método utilizado para acceder al campo {@link #requiereTramiteCatastral} como si fuera un
     * boolean, se creo con el fin de poder editar este campo desde un selectBooleanCheckbox
     *
     * @author christian.rodriguez
     * @return true si el campo {@link #requiereTramiteCatastral} es SI, false en otro caso
     */
    @Transient
    public boolean isRequiereTramiteCatastralBoolean() {
        if (this.requiereTramiteCatastral != null &&
            this.requiereTramiteCatastral.equals(ESiNo.SI.getCodigo())) {
            return true;
        }
        return false;
    }

    // -----------------------------------------------------------------------------------
    /**
     * Método utilizado para asignar al campo {@link #requiereTramiteCatastral} como si fuera un
     * boolean, se creo con el fin de poder editar este campo desde un selectBooleanCheckbox
     *
     * @author christian.rodriguez
     */
    public void setRequiereTramiteCatastralBoolean(
        boolean requiereTramiteCatastral) {
        if (requiereTramiteCatastral) {
            this.requiereTramiteCatastral = ESiNo.SI.getCodigo();
        } else {
            this.requiereTramiteCatastral = ESiNo.NO.getCodigo();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AvaluoPredio other = (AvaluoPredio) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (id.compareTo(other.id) != 0) {
            return false;
        }
        return true;
    }

}
