package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the COMITE_AVALUO_PONENTE database table.
 *
 */
@Entity
@Table(name = "COMITE_AVALUO_PONENTE")
public class ComiteAvaluoPonente implements Serializable {

    private static final long serialVersionUID = -1676308011312056230L;

    private Long id;
    private Date fechaLog;
    private ProfesionalAvaluo profesionalAvaluos;
    private String usuarioLog;
    private ComiteAvaluo comiteAvaluo;

    public ComiteAvaluoPonente() {
    }

    @Id
    @SequenceGenerator(name = "COMITE_AVALUO_PONENTE_ID_GENERATOR", sequenceName =
        "COMITE_AVALUO_PONENTE_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "COMITE_AVALUO_PONENTE_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESIONAL_AVALUOS_ID")
    public ProfesionalAvaluo getProfesionalAvaluos() {
        return this.profesionalAvaluos;
    }

    public void setProfesionalAvaluos(ProfesionalAvaluo profesionalAvaluos) {
        this.profesionalAvaluos = profesionalAvaluos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to ComiteAvaluo
    @ManyToOne
    @JoinColumn(name = "COMITE_AVALUO_ID")
    public ComiteAvaluo getComiteAvaluo() {
        return this.comiteAvaluo;
    }

    public void setComiteAvaluo(ComiteAvaluo comiteAvaluo) {
        this.comiteAvaluo = comiteAvaluo;
    }

}
