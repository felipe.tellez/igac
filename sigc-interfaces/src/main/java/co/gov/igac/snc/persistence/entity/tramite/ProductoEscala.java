package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the PRODUCTO_ESCALA database table.
 *
 */
@Entity
@Table(name = "PRODUCTO_ESCALA")
public class ProductoEscala implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String escala;
    private Date fechaLog;
    private String usuarioLog;
    private Producto producto;

    public ProductoEscala() {
    }

    // Property accessors
    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEscala() {
        return this.escala;
    }

    public void setEscala(String escala) {
        this.escala = escala;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Producto
    @ManyToOne(fetch = FetchType.LAZY)
    public Producto getProducto() {
        return this.producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

}
