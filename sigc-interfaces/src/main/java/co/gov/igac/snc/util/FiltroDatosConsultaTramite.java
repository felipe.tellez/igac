package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author juan.agudelo
 *
 * @description Filtro de datos para la consulta de trámites
 * @version 2.0
 *
 * @modified by javier.aponte: se agrega territorialId, departamentoId, municipioId clasificación,
 * funcionarioEjecutor se agrega el atributo fecha de radicación final porque en algunas pantallas
 * es necesario buscar entre fecha de radicación inicial y fecha de radicación final, el atributo
 * fecha de radicación se deja igual pero se va a tomar como fecha de radicación inicial
 * @modified by juanfelipe.garcia:: 15-10-2013 :: se agrega estadoTramite y codidoUnidadOperativa
 */
public class FiltroDatosConsultaTramite implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8191143239137302418L;

    private String territorialId;
    private String departamentoId;
    private String municipioId;

    private String numeroSolicitud;
    private String numeroRadicacion;
    private Date fechaSolicitud;
    private Date fechaRadicacion;
    private Date fechaRadicacionFinal;
    private String tipoSolicitud;
    private String tipoTramite;
    private String claseMutacion;
    private String subtipo;
    private String numeroResolucion;
    private Date fechaResolucion;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String digitoVerificacion;
    private NumeroPredialPartes numeroPredialPartes;

    private String clasificacion;
    private String funcionarioEjecutor;
    private String estadoTramite;
    private String codidoUnidadOperativa;

    // Recuperacion de tramites
    private String tarea;
    private String paso;
    private String resultado;

    /**
     * Default constructor
     */
    public FiltroDatosConsultaTramite() {
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public Date getFechaRadicacion() {
        return fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    public Date getFechaRadicacionFinal() {
        return fechaRadicacionFinal;
    }

    public void setFechaRadicacionFinal(Date fechaRadicacionFinal) {
        this.fechaRadicacionFinal = fechaRadicacionFinal;
    }

    public String getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(String tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getClaseMutacion() {
        return claseMutacion;
    }

    public void setClaseMutacion(String claseMutacion) {
        this.claseMutacion = claseMutacion;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    public String getNumeroResolucion() {
        return numeroResolucion;
    }

    public void setNumeroResolucion(String numeroResolucion) {
        this.numeroResolucion = numeroResolucion;
    }

    public Date getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getDigitoVerificacion() {
        return digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    public NumeroPredialPartes getNumeroPredialPartes() {
        return numeroPredialPartes;
    }

    public void setNumeroPredialPartes(NumeroPredialPartes numeroPredialPartes) {
        this.numeroPredialPartes = numeroPredialPartes;
    }

    public String getTerritorialId() {
        return territorialId;
    }

    public void setTerritorialId(String territorialId) {
        this.territorialId = territorialId;
    }

    public String getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(String departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(String municipioId) {
        this.municipioId = municipioId;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getFuncionarioEjecutor() {
        return funcionarioEjecutor;
    }

    public void setFuncionarioEjecutor(String funcionarioEjecutor) {
        this.funcionarioEjecutor = funcionarioEjecutor;
    }

    public String getEstadoTramite() {
        return estadoTramite;
    }

    public void setEstadoTramite(String estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    public String getCodidoUnidadOperativa() {
        return codidoUnidadOperativa;
    }

    public void setCodidoUnidadOperativa(String codidoUnidadOperativa) {
        this.codidoUnidadOperativa = codidoUnidadOperativa;
    }

    public String getTarea() {
        return tarea;
    }

    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    public String getPaso() {
        return paso;
    }

    public void setPaso(String paso) {
        this.paso = paso;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

}
