package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * PersonaPredioPropiedad entity. @author MyEclipse Persistence Tools fredy.wilches	se adicionaron
 * los atributos Transient: entidadEmisoraValor; modoAdquisicionValor; tipoTituloValor; Cambio de
 * Departamento y Municipio de String a Objetos
 *
 * @modified juan.agudelo 13-10-11 Cambio de documentoSoporteId al Objeto de tipo Documento Se
 * agregó la secuencia PERSONA_PREDIO_PROPIEDA_ID_SEQ
 */
@Entity
@Table(name = "PERSONA_PREDIO_PROPIEDAD", schema = "SNC_CONSERVACION")
public class PersonaPredioPropiedad implements java.io.Serializable {

    // Fields
    private Long id;
    private PersonaPredio personaPredio;
    private String tipo;
    private String entidadEmisora;
    private Departamento departamento;
    private Municipio municipio;
    private String falsaTradicion;
    private Date fechaRegistro;
    private String numeroRegistro;
    private String modoAdquisicion;
    private Double valor;
    private String tipoTitulo;
    private String numeroTitulo;
    private Date fechaTitulo;
    private String libro;
    private String tomo;
    private String pagina;
    private Documento documentoSoporte;
    private String justificacion;
    private String usuarioLog;
    private Date fechaLog;

    private String entidadEmisoraValor;
    private String modoAdquisicionValor;
    private String tipoTituloValor;
    // Constructors

    /** default constructor */
    public PersonaPredioPropiedad() {
    }

    /** minimal constructor */
    public PersonaPredioPropiedad(Long id, PersonaPredio personaPredio,
        String tipo, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.personaPredio = personaPredio;
        this.tipo = tipo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PersonaPredioPropiedad(Long id, PersonaPredio personaPredio,
        String tipo, String entidadEmisora, Departamento departamento,
        Municipio municipio, String falsaTradicion,
        Date fechaRegistro, String numeroRegistro,
        String modoAdquisicion, Double valor, String tipoTitulo,
        String numeroTitulo, Date fechaTitulo, String libro,
        String tomo, String pagina, Documento documentoSoporte,
        String justificacion, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.personaPredio = personaPredio;
        this.tipo = tipo;
        this.entidadEmisora = entidadEmisora;
        this.departamento = departamento;
        this.municipio = municipio;
        this.falsaTradicion = falsaTradicion;
        this.fechaRegistro = fechaRegistro;
        this.numeroRegistro = numeroRegistro;
        this.modoAdquisicion = modoAdquisicion;
        this.valor = valor;
        this.tipoTitulo = tipoTitulo;
        this.numeroTitulo = numeroTitulo;
        this.fechaTitulo = fechaTitulo;
        this.libro = libro;
        this.tomo = tomo;
        this.pagina = pagina;
        this.documentoSoporte = documentoSoporte;
        this.justificacion = justificacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSONA_PREDIO_PROPIEDA_ID_SEQ")
    @SequenceGenerator(name = "PERSONA_PREDIO_PROPIEDA_ID_SEQ", sequenceName =
        "PERSONA_PREDIO_PROPIEDA_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSONA_PREDIO_ID", nullable = false)
    public PersonaPredio getPersonaPredio() {
        return this.personaPredio;
    }

    public void setPersonaPredio(PersonaPredio personaPredio) {
        this.personaPredio = personaPredio;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "ENTIDAD_EMISORA", length = 100)
    public String getEntidadEmisora() {
        return this.entidadEmisora;
    }

    public void setEntidadEmisora(String entidadEmisora) {
        this.entidadEmisora = entidadEmisora;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "FALSA_TRADICION", length = 2)
    public String getFalsaTradicion() {
        return this.falsaTradicion;
    }

    public void setFalsaTradicion(String falsaTradicion) {
        this.falsaTradicion = falsaTradicion;
    }

    @Column(name = "FECHA_REGISTRO", length = 7)
    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Column(name = "NUMERO_REGISTRO", length = 20)
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "MODO_ADQUISICION", length = 30)
    public String getModoAdquisicion() {
        return this.modoAdquisicion;
    }

    public void setModoAdquisicion(String modoAdquisicion) {
        this.modoAdquisicion = modoAdquisicion;
    }

    @Column(name = "VALOR", precision = 18)
    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Column(name = "TIPO_TITULO", length = 30)
    public String getTipoTitulo() {
        return this.tipoTitulo;
    }

    public void setTipoTitulo(String tipoTitulo) {
        this.tipoTitulo = tipoTitulo;
    }

    @Column(name = "NUMERO_TITULO", length = 20)
    public String getNumeroTitulo() {
        return this.numeroTitulo;
    }

    public void setNumeroTitulo(String numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }

    @Column(name = "FECHA_TITULO", length = 7)
    public Date getFechaTitulo() {
        return this.fechaTitulo;
    }

    public void setFechaTitulo(Date fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }

    @Column(name = "LIBRO", length = 20)
    public String getLibro() {
        return this.libro;
    }

    public void setLibro(String libro) {
        this.libro = libro;
    }

    @Column(name = "TOMO", length = 20)
    public String getTomo() {
        return this.tomo;
    }

    public void setTomo(String tomo) {
        this.tomo = tomo;
    }

    @Column(name = "PAGINA", length = 20)
    public String getPagina() {
        return this.pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = true)
    public Documento getDocumentoSoporte() {
        return this.documentoSoporte;
    }

    public void setDocumentoSoporte(Documento documentoSoporte) {
        this.documentoSoporte = documentoSoporte;
    }

    @Column(name = "JUSTIFICACION", length = 2000)
    public String getJustificacion() {
        return this.justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Transient
    public String getEntidadEmisoraValor() {
        return entidadEmisoraValor;
    }

    public void setEntidadEmisoraValor(String entidadEmisoraValor) {
        this.entidadEmisoraValor = entidadEmisoraValor;
    }

    @Transient
    public String getModoAdquisicionValor() {
        return modoAdquisicionValor;
    }

    public void setModoAdquisicionValor(String modoAdquisicionValor) {
        this.modoAdquisicionValor = modoAdquisicionValor;
    }

    @Transient
    public String getTipoTituloValor() {
        return tipoTituloValor;
    }

    public void setTipoTituloValor(String tipoTituloValor) {
        this.tipoTituloValor = tipoTituloValor;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static PersonaPredioPropiedad parsePersonaPredioPropiedad(
        HPersonaPredioPropiedad hPersonaPredioPropiedad) {
        PersonaPredioPropiedad personaPredioPropiedad = new PersonaPredioPropiedad();

        personaPredioPropiedad.setId(hPersonaPredioPropiedad.getId().getId());
//            personaPredioPropiedad.setPersonaPredio(PersonaPredio.parsePersonaPredio(hPersonaPredioPropiedad.gethPersonaPredio()));
        personaPredioPropiedad.setTipo(hPersonaPredioPropiedad.getTipo());
        personaPredioPropiedad.setEntidadEmisora(hPersonaPredioPropiedad.getEntidadEmisora());
        personaPredioPropiedad.setDepartamento(hPersonaPredioPropiedad.getDepartamento());
        personaPredioPropiedad.setMunicipio(hPersonaPredioPropiedad.getMunicipio());
        personaPredioPropiedad.setFalsaTradicion(hPersonaPredioPropiedad.getFalsaTradicion());
        personaPredioPropiedad.setFechaRegistro(hPersonaPredioPropiedad.getFechaRegistro());
        personaPredioPropiedad.setNumeroRegistro(hPersonaPredioPropiedad.getNumeroRegistro());
        personaPredioPropiedad.setModoAdquisicion(hPersonaPredioPropiedad.getModoAdquisicion());
        personaPredioPropiedad.setValor(hPersonaPredioPropiedad.getValor());
        personaPredioPropiedad.setTipoTitulo(hPersonaPredioPropiedad.getTipoTitulo());
        personaPredioPropiedad.setNumeroTitulo(hPersonaPredioPropiedad.getNumeroTitulo());
        personaPredioPropiedad.setFechaTitulo(hPersonaPredioPropiedad.getFechaTitulo());
        personaPredioPropiedad.setLibro(hPersonaPredioPropiedad.getLibro());
        personaPredioPropiedad.setTomo(hPersonaPredioPropiedad.getTomo());
        personaPredioPropiedad.setPagina(hPersonaPredioPropiedad.getPagina());
        personaPredioPropiedad.setDocumentoSoporte(hPersonaPredioPropiedad.getDocumentoSoporte());
        personaPredioPropiedad.setJustificacion(hPersonaPredioPropiedad.getJustificacion());
        personaPredioPropiedad.setUsuarioLog(hPersonaPredioPropiedad.getUsuarioLog());
        personaPredioPropiedad.setFechaLog(hPersonaPredioPropiedad.getFechaLog());

        return personaPredioPropiedad;
    }

}
