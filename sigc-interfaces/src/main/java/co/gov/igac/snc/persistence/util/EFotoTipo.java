/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio de FOTO_TIPO
 *
 * @author juan.agudelo
 */
public enum EFotoTipo {

    ANEXO("25"),
    ACABADOS_PRINCIPALES("24"),
    ACABADOS_PRINCIPALES_CONSERVACION("1"),
    ACABADOS_PRINCIPALES_CUBRIMIENTO_MUROS("2"),
    ACABADOS_PRINCIPALES_FACHADAS("3"),
    ACABADOS_PRINCIPALES_NOMENCLATURA("23"),
    ACABADOS_PRINCIPALES_PISOS("4"),
    BANIO("19"),
    BANIO_CONSERVACION("5"),
    BANIO_ENCHAPES("6"),
    BANIO_MOBILIARIO("7"),
    BANIO_TAMANIO("8"),
    COCINA("20"),
    COCINA_CONSERVACION("9"),
    COCINA_ENCHAPES("10"),
    COCINA_MOBILIARIO("11"),
    COCINA_TAMANIO("12"),
    COMPLEMENTO_INDUSTRIA("21"),
    COMPLEMENTO_INDUSTRIA_CERCHAS("13"),
    COMPLEMENTO_INDUSTRIA_CERCHAS_ALTURA("14"),
    ESTRUCTURA("22"),
    ESTRUCTURA_ARMAZON("15"),
    ESTRUCTURA_CONSERVACION("16"),
    ESTRUCTURA_CUBIERTA("17"),
    ESTRUCTURA_MUROS("18");

    private String codigo;

    private EFotoTipo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
