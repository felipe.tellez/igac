package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the DETALLE_CAPTURA_OFERTA database table.
 *
 * @modified pedro.garcia + adición de secuencia para el id + cambio de tipo de dato del id: de long
 * a Long
 *
 */
@Entity
@Table(name = "DETALLE_CAPTURA_OFERTA")
public class DetalleCapturaOferta implements Serializable {

    private static final long serialVersionUID = 786555431L;

    private Long id;
    private Date fechaLog;
    private String manzanaVeredaCodigo;
    private String usuarioLog;
    private AreaCapturaOferta areaCapturaOferta;
    private RegionCapturaOferta regionCapturaOferta;

    public DetalleCapturaOferta() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DetalleCapturaOferta_ID_SEQ")
    @SequenceGenerator(name = "DetalleCapturaOferta_ID_SEQ",
        sequenceName = "DETALLE_CAPTURA_OFERTA_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "MANZANA_VEREDA_CODIGO")
    public String getManzanaVeredaCodigo() {
        return this.manzanaVeredaCodigo;
    }

    public void setManzanaVeredaCodigo(String manzanaVeredaCodigo) {
        this.manzanaVeredaCodigo = manzanaVeredaCodigo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to AreaCapturaOferta
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AREA_CAPTURA_OFERTA_ID")
    public AreaCapturaOferta getAreaCapturaOferta() {
        return this.areaCapturaOferta;
    }

    public void setAreaCapturaOferta(AreaCapturaOferta areaCapturaOferta) {
        this.areaCapturaOferta = areaCapturaOferta;
    }

    //bi-directional many-to-one association to RegionCapturaOferta
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REGION_CAPTURA_OFERTA_ID")
    public RegionCapturaOferta getRegionCapturaOferta() {
        return this.regionCapturaOferta;
    }

    public void setRegionCapturaOferta(RegionCapturaOferta regionCapturaOferta) {
        this.regionCapturaOferta = regionCapturaOferta;
    }

}
