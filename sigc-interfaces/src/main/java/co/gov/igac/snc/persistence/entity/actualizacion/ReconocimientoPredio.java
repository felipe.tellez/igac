package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the RECONOCIMIENTO_PREDIO database table.
 *
 */
@Entity
@Table(name = "RECONOCIMIENTO_PREDIO")
public class ReconocimientoPredio implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String estado;
    private Date fecha;
    private Date fechaLog;
    private String numeroPredial;
    private String usuarioLog;
    private ActualizacionReconocimiento actualizacionReconocimiento;
    private RecursoHumano recursoHumano;

    // Transient usado en la asignacion de predios
    private List<SaldoConservacion> tramitesAsociadosSaldosConservacion;

    public ReconocimientoPredio() {
    }

    @Id
    @SequenceGenerator(name = "RECONOCIMIENTO_PREDIO_ID_GENERATOR", sequenceName =
        "RECONOCIMIENTO_PREDIO_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "RECONOCIMIENTO_PREDIO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ActualizacionReconocimiento
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_RECO_ID")
    public ActualizacionReconocimiento getActualizacionReconocimiento() {
        return this.actualizacionReconocimiento;
    }

    public void setActualizacionReconocimiento(
        ActualizacionReconocimiento actualizacionReconocimiento) {
        this.actualizacionReconocimiento = actualizacionReconocimiento;
    }

    //bi-directional many-to-one association to RecursoHumano
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECURSO_HUMANO_ID")
    public RecursoHumano getRecursoHumano() {
        return this.recursoHumano;
    }

    public void setRecursoHumano(RecursoHumano recursoHumano) {
        this.recursoHumano = recursoHumano;
    }

    @Transient
    public List<SaldoConservacion> getTramitesAsociadosSaldosConservacion() {
        return tramitesAsociadosSaldosConservacion;
    }

    public void setTramitesAsociadosSaldosConservacion(
        List<SaldoConservacion> tramitesAsociadosSaldosConservacion) {
        this.tramitesAsociadosSaldosConservacion = tramitesAsociadosSaldosConservacion;
    }

}
