package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con el nombre de los subprocesos de Conservación.
 *
 * @author david.cifuentes
 */
public enum EConservacionSubproceso {

    ASIGNACION("Asignación"),
    DEPURACION("Depuración"),
    EJECUCION("Ejecución"),
    RADICACION("Radicación"),
    VALIDACION("Validación");

    private String nombre;

    private EConservacionSubproceso(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
