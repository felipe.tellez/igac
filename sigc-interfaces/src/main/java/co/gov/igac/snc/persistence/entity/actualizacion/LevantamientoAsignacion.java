package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the LEVANTAMIENTO_ASIGNACION database table.
 *
 */
@Entity
@Table(name = "LEVANTAMIENTO_ASIGNACION")
public class LevantamientoAsignacion implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fecha;
    private Date fechaLog;
    private String usuarioLog;
    private List<ActualizacionLevantamiento> actualizacionLevantamientos;
    private List<AsignacionControlCalidad> asignacionControlCalidads;
    private RecursoHumano recursoHumano;
    private List<LevantamientoAsignacionInf> levantamientoAsignacionInfs;

    public LevantamientoAsignacion() {
    }

    @Id
    @SequenceGenerator(name = "LEVANTAMIENTO_ASIGNACION_ID_GENERATOR", sequenceName =
        "LEVANTAMIENTO_ASIGNACIO_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "LEVANTAMIENTO_ASIGNACION_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ActualizacionLevantamiento
    @OneToMany(mappedBy = "levantamientoAsignacion")
    public List<ActualizacionLevantamiento> getActualizacionLevantamientos() {
        return this.actualizacionLevantamientos;
    }

    public void setActualizacionLevantamientos(
        List<ActualizacionLevantamiento> actualizacionLevantamientos) {
        this.actualizacionLevantamientos = actualizacionLevantamientos;
    }

    //bi-directional many-to-one association to AsignacionControlCalidad
    @OneToMany(mappedBy = "levantamientoAsignacion")
    public List<AsignacionControlCalidad> getAsignacionControlCalidads() {
        return this.asignacionControlCalidads;
    }

    public void setAsignacionControlCalidads(
        List<AsignacionControlCalidad> asignacionControlCalidads) {
        this.asignacionControlCalidads = asignacionControlCalidads;
    }

    //bi-directional many-to-one association to RecursoHumano
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECURSO_HUMANO_ID")
    public RecursoHumano getRecursoHumano() {
        return this.recursoHumano;
    }

    public void setRecursoHumano(RecursoHumano recursoHumano) {
        this.recursoHumano = recursoHumano;
    }

    //bi-directional many-to-one association to LevantamientoAsignacionInf
    @OneToMany(mappedBy = "levantamientoAsignacion")
    public List<LevantamientoAsignacionInf> getLevantamientoAsignacionInfs() {
        return this.levantamientoAsignacionInfs;
    }

    public void setLevantamientoAsignacionInfs(
        List<LevantamientoAsignacionInf> levantamientoAsignacionInfs) {
        this.levantamientoAsignacionInfs = levantamientoAsignacionInfs;
    }

}
