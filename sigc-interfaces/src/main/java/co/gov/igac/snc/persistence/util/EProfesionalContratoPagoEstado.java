/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los estados de un pago asociado a un contrato de profesional de avaluos
 * PROFESIONAL_COP_ESTADO
 *
 * @author christian.rodriguez
 */
public enum EProfesionalContratoPagoEstado {

    PROYECTADO("PROYECTADO"),
    CAUSADO("CAUSADO");

    private String codigo;

    private EProfesionalContratoPagoEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
