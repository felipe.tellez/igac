package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.List;

import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;

/**
 *
 * @author juan.copypasteelo
 */
public class DocumentoResultadoPPRedios implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4413929913141272729L;

    /**
     * Ruta del documento resultado del trámite para visualización
     */
    private String documentoResultado;

    /**
     * Lista de ppredios asociados al trámite
     */
    private List<PPredio> pprediosTramite;

    /**
     * Histórico de las observaciones del trámite
     */
    private List<TramiteEstado> historicoObservaciones;

    /**
     * Observación temporal que no se consolida hasta que se avanza el proceso.
     */
    private TramiteEstado observacionTemporal;

    /**
     * Constructor por defecto
     */
    public DocumentoResultadoPPRedios() {

    }

    /**
     * Constructor general
     */
    public DocumentoResultadoPPRedios(String documentoResultado,
        List<PPredio> pprediosTramite, List<TramiteEstado> historicoObservaciones) {
        super();
        this.documentoResultado = documentoResultado;
        this.pprediosTramite = pprediosTramite;
        this.historicoObservaciones = historicoObservaciones;
    }

    public String getDocumentoResultado() {
        return documentoResultado;
    }

    public void setDocumentoResultado(String documentoResultado) {
        this.documentoResultado = documentoResultado;
    }

    public List<PPredio> getPprediosTramite() {
        return pprediosTramite;
    }

    public void setPprediosTramite(List<PPredio> pprediosTramite) {
        this.pprediosTramite = pprediosTramite;
    }

    public TramiteEstado getObservacionTemporal() {
        return observacionTemporal;
    }

    public void setObservacionTemporal(TramiteEstado observacionTemporal) {
        this.observacionTemporal = observacionTemporal;
    }

    public List<TramiteEstado> getHistoricoObservaciones() {
        return historicoObservaciones;
    }

    public void setHistoricoObservaciones(List<TramiteEstado> historicoObservaciones) {
        this.historicoObservaciones = historicoObservaciones;
    }

}
