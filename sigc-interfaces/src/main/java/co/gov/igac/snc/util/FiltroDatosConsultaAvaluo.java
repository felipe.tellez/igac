package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Objeto utilizado para agrupar ciertos datos relacionados con avalúos, pero que no pertenecen
 * directamente al objeto Avaluo, y enviarlo como parametro al los metodos de consulta.
 *
 * @author felipe.cadena
 */
/*
 * @modified pedro.garcia 20-09-2012 Adición de campos 'estadoTramite' y 'aprobado'
 *
 * @modified christian.rodriguez 08-10-2012 Adición de campo 'fechaCancelacionDesde'
 *
 * @modified christian.rodriguez 08-10-2012 Adición de campo 'fechaCancelacionHasta'
 *
 * @modified christian.rodriguez 08-10-2012 Adición de campo 'sigla'
 *
 * @modified christian.rodriguez 08-10-2012 Adición de campos 'primerNombre', 'segundoNombre',
 * 'primerApellido' y 'segundoApellido'
 *
 * @modified christian.rodriguez 30-10-2012 Adición del campo 'conActaControlCalidad'
 *
 * @modified christian.rodriguez 26-11-2012 Adición del campo tramitesId @modified
 * christian.rodriguez 27-11-2012 Adición del campo solicitanteTipoIdentificacion @modified
 * christian.rodriguez 27-11-2012 Adición del campo solicitanteNumeroIdentificacion @modified
 * christian.rodriguez 19-12-2012 Adición del campo valorCotizacionDesde @modified
 * christian.rodriguez 19-12-2012 Adición del campo valorCotizacionHasta
 */
public class FiltroDatosConsultaAvaluo implements Serializable {

    /**
     * Serial uid autogenerado
     */
    private static final long serialVersionUID = 1698354015746003878L;

    private String numeroRadicacion;
    private String numeroRadicacionDefinitivo;
    private String numeroSecRadicado;
    private Date fechaInicial;
    private Date fechaFinal;
    private String nombreSolicitante;
    private String nombreRazonSocial;
    private String territorial;
    private String nombreAvaluador;
    private String tipoTramite;
    private String tipoAvaluo;
    private String departamento;
    private String municipio;
    private String tipoEmpresa;
    private String direccion;
    private String numeroPredial;
    private Date fechaCancelacionDesde;
    private Date fechaCancelacionHasta;
    private String sigla;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private Long idProfesional;
    private String reservado;
    private String anioAprobacion;
    private List<Long> tramitesId;
    private String solicitanteTipoIdentificacion;
    private String solicitanteNumeroIdentificacion;
    private Double valorCotizacionDesde;
    private Double valorCotizacionHasta;

    /**
     * estado del trámite del avalúo
     */
    private String estadoTramiteAvaluo;

    private String aprobado;
    private boolean conActaControlCalidad;

    public String getNumeroRadicacion() {
        return this.numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public String getNumeroRadicacionDefinitivo() {
        return this.numeroRadicacionDefinitivo;
    }

    public void setNumeroRadicacionDefinitivo(String numeroRadicacionDefinitivo) {
        this.numeroRadicacionDefinitivo = numeroRadicacionDefinitivo;
    }

    public String getNombreRazonSocial() {
        return nombreRazonSocial;
    }

    public void setNombreRazonSocial(String nombreRazonSocial) {
        this.nombreRazonSocial = nombreRazonSocial;
    }

    public String getNombreAvaluador() {
        return this.nombreAvaluador;
    }

    public void setNombreAvaluador(String nombreAvaluador) {
        this.nombreAvaluador = nombreAvaluador;
    }

    public String getTipoAvaluo() {
        return this.tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    public String getNumeroSecRadicado() {
        return this.numeroSecRadicado;
    }

    public void setNumeroSecRadicado(String numeroSecRadicado) {
        this.numeroSecRadicado = numeroSecRadicado;
    }

    public Date getFechaInicial() {
        return this.fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return this.fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public String getNombreSolicitante() {
        return this.nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getTerritorial() {
        return this.territorial;
    }

    public void setTerritorial(String territorial) {
        this.territorial = territorial;
    }

    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getTipoEmpresa() {
        return this.tipoEmpresa;
    }

    public void setTipoEmpresa(String tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getEstadoTramiteAvaluo() {
        return this.estadoTramiteAvaluo;
    }

    public void setEstadoTramiteAvaluo(String estadoTramite) {
        this.estadoTramiteAvaluo = estadoTramite;
    }

    public String getAprobado() {
        return this.aprobado;
    }

    public void setAprobado(String aprobado) {
        this.aprobado = aprobado;
    }

    public Date getFechaCancelacionDesde() {
        return this.fechaCancelacionDesde;
    }

    public void setFechaCancelacionDesde(Date fechaCancelacionDesde) {
        this.fechaCancelacionDesde = fechaCancelacionDesde;
    }

    public Date getFechaCancelacionHasta() {
        return this.fechaCancelacionHasta;
    }

    public void setFechaCancelacionHasta(Date fechaCancelacionHasta) {
        this.fechaCancelacionHasta = fechaCancelacionHasta;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public boolean isConActaControlCalidad() {
        return this.conActaControlCalidad;
    }

    public void setConActaControlCalidad(boolean conActaControlCalidad) {
        this.conActaControlCalidad = conActaControlCalidad;
    }

    public Long getIdProfesional() {
        return this.idProfesional;
    }

    public void setIdProfesional(Long idProfesional) {
        this.idProfesional = idProfesional;
    }

    public String getReservado() {
        return this.reservado;
    }

    public void setReservado(String reservado) {
        this.reservado = reservado;
    }

    public String getAnioAprobacion() {
        return this.anioAprobacion;
    }

    public void setAnioAprobacion(String anioAprobacion) {
        this.anioAprobacion = anioAprobacion;
    }

    public List<Long> getTramitesId() {
        return this.tramitesId;
    }

    public void setTramitesId(List<Long> tramitesId) {
        this.tramitesId = tramitesId;
    }

    public String getSolicitanteTipoIdentificacion() {
        return this.solicitanteTipoIdentificacion;
    }

    public void setSolicitanteTipoIdentificacion(
        String solicitanteTipoIdentificacion) {
        this.solicitanteTipoIdentificacion = solicitanteTipoIdentificacion;
    }

    public String getSolicitanteNumeroIdentificacion() {
        return this.solicitanteNumeroIdentificacion;
    }

    public void setSolicitanteNumeroIdentificacion(
        String solicitanteNumeroIdentificacion) {
        this.solicitanteNumeroIdentificacion = solicitanteNumeroIdentificacion;
    }

    public Double getValorCotizacionDesde() {
        return this.valorCotizacionDesde;
    }

    public void setValorCotizacionDesde(Double valorCotizacionDesde) {
        this.valorCotizacionDesde = valorCotizacionDesde;
    }

    public Double getValorCotizacionHasta() {
        return this.valorCotizacionHasta;
    }

    public void setValorCotizacionHasta(Double valorCotizacionHasta) {
        this.valorCotizacionHasta = valorCotizacionHasta;
    }

}
