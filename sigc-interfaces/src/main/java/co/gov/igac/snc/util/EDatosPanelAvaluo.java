/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con datos de la tabla antes despues para el panel de resumen correspondiente avalúos
 *
 * @author juan.agudelo
 */
public enum EDatosPanelAvaluo {

    VALOR_TERRENO("Valor terreno"),
    VALOR_TOTAL_CONSTRUCCION("Valor total construcción"),
    VALOR_TOTAL_CONSTRUCCION_CONVENCIONAL("Valor total construcción convencional"),
    VALOR_TOTAL_CONSTRUCCION_NO_CONVENCIONAL("Valor total construcción no convencional"),
    AREA_TOTAL_TERRENO("Área total terreno"),
    AREA_TOTAL_CONSTRUCCION("Área total construcción"),
    AREA_TOTAL_CONSTRUCCION_CONVENCIONAL("Área total construcción convencional"),
    AREA_TOTAL_CONSTRUCCION_NO_CONVENCIONAL("Área total construcción no convencional"),
    CONSTRUCCIONES_CONVENCIONALES("Construcciones convencionales"),
    CONSTRUCCIONES_NO_CONVENCIONALES("Construcciones no convencionales");

    private String dato;

    private EDatosPanelAvaluo(String dato) {
        this.dato = dato;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String id) {
        this.dato = id;
    }

}
