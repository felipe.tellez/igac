package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con el nombre de las actividades del proceso de conservación.
 *
 * @author david.cifuentes
 */
public enum EProcesoConservacionNombreActividad {

    // Subproceso de radicación
    ACT_RADICACION_REVISAR_SOLICITUDES("RevisarSolicitudes", "Revisar solicitudes"),
    // Subproceso de digitalización
    ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO("ControlarCalidadEscaneo",
        "Controlar la calidad del escaneo"),
    ACT_DIGITALIZACION_ESCANEAR("Escanear", "Escanear"),
    // Subproceso de asignación
    ACT_ASIGNACION_ASIGNAR_TRAMITES("AsignarTramites", "Asignar trámites"),
    ACT_ASIGNACION_COMISIONAR("ComisionarTramitesTerreno", "Comisionar trámites de terreno"),
    ACT_ASIGNACION_COMPLETAR_DOC_SOLICITADA("CompletarDocumentacionSolicitada",
        "Completar documentación solicitada"),
    ACT_ASIGNACION_DEPURAR_INFO_CATASTRAL_DIGITAL("DepurarInformacionCatastralDigital",
        "Depurar información catastral digital"),
    ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE("DeterminarProcedencia",
        "Determinar procedencia del trámite"),
    ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO("RevisarTramiteAsignado", "Revisar trámite asignado"),
    ACT_ASIGNACION_REVISAR_TRAMITE_DEVUELTO("RevisarTramiteDevuelto", "Revisar trámite devuelto"),
    ACT_ASIGNACION_SOLICITAR_DOC_REQUERIDO_TRAMITE("SolicitarDocumentoRequeridoFaltante",
        "Solicitar documento requerido faltante"),
    ACT_ASIGNACION_TRAMITE_NO_PROCEDE("Terminar", "Terminar"),
    // Subproceso de ejecución	
    ACT_EJECUCION_APROBAR_NUEVOS_TRAMITES("AprobarNuevosTramites", "Aprobar nuevos trámites"),
    ACT_EJECUCION_APROBAR_SOLICITUD_PRUEBA("AprobarSolicitudPrueba", "Aprobar solicitud de prueba"),
    ACT_EJECUCION_ASOCIAR_DOC_AL_NUEVO_TRAMITE("AsociarDocumentoNuevoTramite",
        "Asociar documento a nuevo trámite"),
    ACT_EJECUCION_CARGAR_AL_SNC("CargarSNC", "Cargar SNC"),
    ACT_EJECUCION_CARGAR_PRUEBA("CargarPrueba", "Cargar prueba"),
    ACT_EJECUCION_CARGAR_PRUEBA_COMISIONAR("CargarPrueba|ComisionarTramitesTerreno",
        "Cargar prueba | Comisionar tramites de terreno"),
    ACT_EJECUCION_COMISIONAR("ComisionarTramitesTerreno", "Comisionar trámites de terreno"),
    ACT_EJECUCION_COMUNICAR_AUTO_INTERESADO("ComunicarAutoInteresado", "Comunicar auto interesado"),
    ACT_EJECUCION_ALISTAR_INFORMACION("AlistarInformacion", "Alistar información"),
    ACT_EJECUCION_ELABORAR_AUTO_PRUEBA("ElaborarAutoPrueba", "Elaborar auto de prueba"),
    ACT_EJECUCION_ELABORAR_INFORME_CONCEPTO("ElaborarInformeSustentandoConcepto",
        "Elaborar informe sustentando el concepto"),
    ACT_EJECUCION_ESTABLECER_PROCEDENCIA("EstablecerProcedencia",
        "Establecer procedencia del trámite"),
    ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA("ModificarInformacionAlfanumerica",
        "Modificar información alfanumérica"),
    ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA("ModificarInformacionGeografica",
        "Modificar información geográfica"),
    ACT_EJECUCION_REVISAR_AUTO_PRUEBA("RevisarAutoPrueba", "Revisar auto de prueba"),
    ACT_EJECUCION_SOLICITAR_PRUEBA("SolicitarPrueba", "Solicitar prueba"),
    ACT_EJECUCION_TERMINAR_EJECUCION_TRAMITE("Terminar", "Terminar"),
    // Subproceso de validación
    ACT_VALIDACION_APLICAR_CAMBIOS("AplicarCambios", "Aplicar cambios"),
    ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION("ComunicarNotificacionResolucion",
        "Comunicar notificación de resolución"),
    ACT_VALIDACION_GENERAR_RESOLUCION("GenerarResolucion", "Generar resolución"),
    ACT_VALIDACION_GENERAR_RESOLUCION_IPER("GenerarResolucionIPER", "Generar resolución IPER"),
    ACT_VALIDACION_MODIFICAR_INFORMACION_ALFANUMERICA("ModificarInformacionAlfanumerica",
        "Modificar información alfanumérica"),
    ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA("ModificarInformacionGeografica",
        "Modificar información geográfica"),
    ACT_VALIDACION_NOTIFICAR_POR_EDICTO("NotificarPorEdicto", "notificar por edicto"),
    ACT_VALIDACION_RADICAR_RECURSO("RadicarRecurso", "Radicar recurso"),
    ACT_VALIDACION_REGISTRAR_NOTIFICACION("RegistrarNotificacion", "Registrar notificación"),
    ACT_VALIDACION_REVISAR_PROYECCION("RevisarProyeccion", "Revisar proyección"),
    ACT_VALIDACION_REVISAR_PROYECCION_RESOLUCION("RevisarProyeccionResolucion",
        "Revisar proyección de resolución"),
    ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE("Terminar", "Terminar");

    private String codigo;

    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private EProcesoConservacionNombreActividad(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
