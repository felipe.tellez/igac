package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the V_JURISDICCION_PRODUCTO database table.
 *
 * @author jamir.avila
 */
@NamedQueries({
    @NamedQuery(name = "recuperarPorCodigoMunicipio",
        query = "SELECT o FROM VJurisdiccionProducto o WHERE o.municipioCodigo = :codigoMunicipio")
})
@Entity
@Table(name = "V_JURISDICCION_PRODUCTO")
public class VJurisdiccionProducto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "CODIGO_ESTRUCTURA")
    private String codigoEstructura;

    @Column(name = "CODIGO_TERRITORIAL")
    private String codigoTerritorial;

    private String departamento;

    @Id
    private String municipio;

    @Column(name = "MUNICIPIO_CODIGO")
    private String municipioCodigo;

    @Column(name = "NOMBRE_ESTRUCTURA")
    private String nombreEstructura;

    @Column(name = "NOMBRE_TERRITORIAL")
    private String nombreTerritorial;

    private String tipo;

    public VJurisdiccionProducto() {
    }

    public String getCodigoEstructura() {
        return this.codigoEstructura;
    }

    public void setCodigoEstructura(String codigoEstructura) {
        this.codigoEstructura = codigoEstructura;
    }

    public String getCodigoTerritorial() {
        return this.codigoTerritorial;
    }

    public void setCodigoTerritorial(String codigoTerritorial) {
        this.codigoTerritorial = codigoTerritorial;
    }

    public String getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public String getNombreEstructura() {
        return this.nombreEstructura;
    }

    public void setNombreEstructura(String nombreEstructura) {
        this.nombreEstructura = nombreEstructura;
    }

    public String getNombreTerritorial() {
        return this.nombreTerritorial;
    }

    public void setNombreTerritorial(String nombreTerritorial) {
        this.nombreTerritorial = nombreTerritorial;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "VJurisdiccionProducto [codigoEstructura=" + codigoEstructura +
            ", codigoTerritorial=" + codigoTerritorial +
            ", departamento=" + departamento + ", municipio=" + municipio +
            ", municipioCodigo=" + municipioCodigo +
            ", nombreEstructura=" + nombreEstructura +
            ", nombreTerritorial=" + nombreTerritorial + ", tipo=" + tipo +
            "]";
    }
}
