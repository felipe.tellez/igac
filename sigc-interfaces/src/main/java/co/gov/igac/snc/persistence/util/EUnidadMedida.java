package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los valores para el dominio UNIDAD_MEDIDA
 *
 * @author felipe.cadena
 */
//TODO :: felipe.cadena :: revisar si le sirve mejor EUnidadMedidaArea :: pedro.garcia
public enum EUnidadMedida implements ECampoDetalleAvaluo {
    METRO("M", "Metros"),
    METRO_CUADRADO("M2", "Metros Cuadrados");

    private String codigo;
    private String valor;

    private EUnidadMedida(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

    @Override
    public String getValor() {
        return valor;
    }
}
