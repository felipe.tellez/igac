package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con el nombre de las actividades asociadas a cada uno de los subprocesos del proceso
 * de Conservación.
 *
 * @author david.cifuentes
 */
public enum EConservacionSubprocesoActividad {

    // Subproceso de Radicación
    ACT_RADICACION_REVISAR_SOLICITUDES(EConservacionSubproceso.RADICACION, "Revisar solicitudes"),
    ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO(EConservacionSubproceso.RADICACION,
        "Controlar la calidad del escaneo"),
    ACT_DIGITALIZACION_ESCANEAR(EConservacionSubproceso.RADICACION, "Escanear"),
    // Subproceso de Asignación
    ACT_ASIGNACION_ASIGNAR_TRAMITES(EConservacionSubproceso.ASIGNACION, "Asignar trámites"),
    ACT_ASIGNACION_COMISIONAR(EConservacionSubproceso.ASIGNACION, "Comisionar trámites de terreno"),
    ACT_ASIGNACION_COMPLETAR_DOC_SOLICITADA(EConservacionSubproceso.ASIGNACION,
        "Completar documentación solicitada"),
    ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE(EConservacionSubproceso.ASIGNACION,
        "Determinar procedencia del trámite"),
    ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO(EConservacionSubproceso.ASIGNACION,
        "Revisar trámite asignado"),
    ACT_ASIGNACION_REVISAR_TRAMITE_DEVUELTO(EConservacionSubproceso.ASIGNACION,
        "Revisar trámite devuelto"),
    ACT_ASIGNACION_SOLICITAR_DOC_REQUERIDO_TRAMITE(EConservacionSubproceso.ASIGNACION,
        "Solicitar documento requerido faltante"),
    // Subproceso de Ejecución	
    ACT_EJECUCION_APROBAR_NUEVOS_TRAMITES(EConservacionSubproceso.EJECUCION,
        "Aprobar nuevos trámites"),
    ACT_EJECUCION_APROBAR_SOLICITUD_PRUEBA(EConservacionSubproceso.EJECUCION,
        "Aporbar solicitud de prueba"),
    ACT_EJECUCION_ASOCIAR_DOC_AL_NUEVO_TRAMITE(EConservacionSubproceso.EJECUCION,
        "Asociar documento a nuevo trámite"),
    ACT_EJECUCION_CARGAR_AL_SNC(EConservacionSubproceso.EJECUCION, "Cargar SNC"),
    ACT_EJECUCION_CARGAR_PRUEBA(EConservacionSubproceso.EJECUCION, "Cargar prueba"),
    // TODO :: david.cifuentes :: Revisar en proces la siguiente actividad :: 09/08/2016
    ACT_EJECUCION_CARGAR_PRUEBA_COMISIONAR(EConservacionSubproceso.EJECUCION,
        "Cargar prueba | Comisionar tramites de terreno"),
    ACT_EJECUCION_COMISIONAR(EConservacionSubproceso.EJECUCION, "Comisionar trámites de terreno"),
    ACT_EJECUCION_COMUNICAR_AUTO_INTERESADO(EConservacionSubproceso.EJECUCION,
        "Comunicar auto interesado"),
    ACT_EJECUCION_ALISTAR_INFORMACION(EConservacionSubproceso.EJECUCION, "Alistar información"),
    ACT_EJECUCION_ELABORAR_AUTO_PRUEBA(EConservacionSubproceso.EJECUCION, "Elaborar auto de prueba"),
    ACT_EJECUCION_ELABORAR_INFORME_CONCEPTO(EConservacionSubproceso.EJECUCION,
        "Elaborar informe sustentando el concepto"),
    // TODO :: david.cifuentes :: Revisar en proces la siguiente actividad :: 09/08/2016
    ACT_EJECUCION_ESTABLECER_PROCEDENCIA(EConservacionSubproceso.EJECUCION,
        "Establecer procedencia del trámite"),
    ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA(EConservacionSubproceso.EJECUCION,
        "Modificar información alfanumérica"),
    ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA(EConservacionSubproceso.EJECUCION,
        "Modificar información geográfica"),
    ACT_EJECUCION_REVISAR_AUTO_PRUEBA(EConservacionSubproceso.EJECUCION, "Revisar auto de prueba"),
    ACT_EJECUCION_SOLICITAR_PRUEBA(EConservacionSubproceso.EJECUCION, "Solicitar prueba"),
    // Subproceso de Validación
    ACT_VALIDACION_APLICAR_CAMBIOS(EConservacionSubproceso.VALIDACION, "Aplicar cambios"),
    ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION(EConservacionSubproceso.VALIDACION,
        "Comunicar notificación de resolución"),
    ACT_VALIDACION_GENERAR_RESOLUCION(EConservacionSubproceso.VALIDACION, "Generar resolución"),
    ACT_VALIDACION_GENERAR_RESOLUCION_IPER(EConservacionSubproceso.VALIDACION,
        "Generar resolución IPER"),
    ACT_VALIDACION_MODIFICAR_INFORMACION_ALFANUMERICA(EConservacionSubproceso.VALIDACION,
        "Modificar información alfanumérica"),
    ACT_VALIDACION_MODIFICAR_INFORMACION_GEOGRAFICA(EConservacionSubproceso.VALIDACION,
        "Modificar información geográfica"),
    ACT_VALIDACION_NOTIFICAR_POR_EDICTO(EConservacionSubproceso.VALIDACION, "notificar por edicto"),
    ACT_VALIDACION_RADICAR_RECURSO(EConservacionSubproceso.VALIDACION, "Radicar recurso"),
    ACT_VALIDACION_REGISTRAR_NOTIFICACION(EConservacionSubproceso.VALIDACION,
        "Registrar notificación"),
    ACT_VALIDACION_REVISAR_PROYECCION(EConservacionSubproceso.VALIDACION, "Revisar proyección"),
    ACT_VALIDACION_REVISAR_PROYECCION_RESOLUCION(EConservacionSubproceso.VALIDACION,
        "Revisar proyección de resolución"),
    // Subproceso de Depuración
    ACT_DEPURACION_AJUSTAR_INFORMACION_ESPACIAL_POR_DIGITALIAZACION(
        EConservacionSubproceso.DEPURACION, "Ajustar información espacial por digitalización"),
    ACT_DEPURACION_AJUSTAR_INFORMACION_ESPACIAL_POR_EJECUTOR(EConservacionSubproceso.DEPURACION,
        "Ajustar información espacial por el ejecutor"),
    ACT_DEPURACION_AJUSTAR_INFORMACION_ESPACIAL_POR_TOPOGRAFIA(EConservacionSubproceso.DEPURACION,
        "Ajustar información espacial por topografía"),
    ACT_DEPURACION_APROBAR_COMISION(EConservacionSubproceso.DEPURACION, "Aprobar comisión"),
    ACT_DEPURACION_ASIGNAR_DIGITALIZADOR(EConservacionSubproceso.DEPURACION, "Asignar digitalizador"),
    ACT_DEPURACION_ASIGNAR_EJECUTOR(EConservacionSubproceso.DEPURACION, "Asignar ejecutor"),
    ACT_DEPURACION_ASIGNAR_TOPOGRAFO(EConservacionSubproceso.DEPURACION, "Asignar topógrafo"),
    ACT_DEPURACION_ESTUDIAR_AJUSTE_ESPACIAL_REQUERIDO(EConservacionSubproceso.DEPURACION,
        "Estudiar ajuste espacial requerido"),
    ACT_DEPURACION_EVALUAR_INFORMACION_ASOCIADA(EConservacionSubproceso.DEPURACION,
        "Evaluar información asociada"),
    ACT_DEPURACION_EXPORTAR_AREA_A_LEVANTAR_E_IDENTIFICAR_PUNTOS_TOPOGRAFICOS(
        EConservacionSubproceso.DEPURACION,
        "Exportar área a levantar e identificar puntos topográficos"),
    ACT_DEPURACION_REALIZAR_CALIDAD_DE_DIGITALIZACION(EConservacionSubproceso.DEPURACION,
        "Realizar control de calidad por digitalización"),
    ACT_DEPURACION_REALIZAR_CALIDAD_DE_EJECUCION(EConservacionSubproceso.DEPURACION,
        "Realizar control de calidad de ejecución"),
    ACT_DEPURACION_REALIZAR_CALIDAD_DE_TOPOGRAFIA(EConservacionSubproceso.DEPURACION,
        "Realizar control de calidad de topografía"),
    ACT_DEPURACION_REALIZAR_INFORME_VISITA(EConservacionSubproceso.DEPURACION,
        "Realizar informe de visita"),
    ACT_DEPURACION_REGISTRAR_RESULTADO_DE_VISITA_EN_TERRENO(EConservacionSubproceso.DEPURACION,
        "Registrar resultado de visita en terreno"),
    ACT_DEPURACION_REVISAR_TAREAS_A_REALIZAR(EConservacionSubproceso.DEPURACION,
        "Revisar tareas a realizar"),
    ACT_DEPURACION_REVISAR_TAREAS_A_REALIZAR_EN_TERRENO(EConservacionSubproceso.DEPURACION,
        "Revisar tareas a realizar en terreno"),
    ACT_DEPURACION_REVISAR_Y_PREPARAR_INFORMACION(EConservacionSubproceso.DEPURACION,
        "Revisar y preparar información"),
    ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_POR_DIGITALIZACION(EConservacionSubproceso.DEPURACION,
        "Solicitar ajuste espacial por digitalización"),
    ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_POR_EJECUTOR(EConservacionSubproceso.DEPURACION,
        "Solicitar ajuste espacial por el ejecutor"),
    ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_POR_TOPOGRAFIA(EConservacionSubproceso.DEPURACION,
        "Solicitar ajuste espacial por topografía"),
    ACT_DEPURACION_SOLICITAR_COMISION(EConservacionSubproceso.DEPURACION, "Solicitar comisión"),
    ACT_DEPURACION_SOLICITAR_COMISION_DE_EJECUTOR(EConservacionSubproceso.DEPURACION,
        "Solicitar comisión de ejecutor"),
    // TODO :: david.cifuentes :: Revisar en proces la siguiente actividad :: 09/08/2016
    ACT_DEPURACION_DEPURAR_INFO_CATASTRAL_DIGITAL(EConservacionSubproceso.DEPURACION,
        "Depurar información catastral digital");

    private EConservacionSubproceso subproceso;

    private String nombreActividad;

    private EConservacionSubprocesoActividad(EConservacionSubproceso subproceso,
        String nombreActividad) {
        this.subproceso = subproceso;
        this.nombreActividad = nombreActividad;
    }

    public EConservacionSubproceso getSubproceso() {
        return this.subproceso;
    }

    public void setSubproceso(EConservacionSubproceso subproceso) {
        this.subproceso = subproceso;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

}
