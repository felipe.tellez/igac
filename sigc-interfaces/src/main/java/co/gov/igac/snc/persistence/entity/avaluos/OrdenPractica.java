package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.EOrdenPracticaEstado;

/**
 * The persistent class for the ORDEN_PRACTICA database table.
 *
 */
/*
 * @modified pedro.garcia - adición de secuencia para el id - cambio de tipo de datos del atributo
 * documentoId, de BigDecimal a Long
 *
 * @modified christian.rodriguez - Adición de la relación con el objeto AvaluoAsignacionProfesional
 *
 * @modified christian.rodriguez - Adición del método transient isModificable
 */
@Entity
@Table(name = "ORDEN_PRACTICA")
public class OrdenPractica implements Serializable {

    private static final long serialVersionUID = 8964221L;

    private Long id;
    private Long documentoId;
    private String estado;
    private Date fecha;
    private Date fechaEnvio;
    private Date fechaLog;
    private String numero;
    private String observaciones;
    private String usuarioLog;

    private List<AvaluoAsignacionProfesional> avaluoAsignacionProfesionals;

    public OrdenPractica() {
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OrdenPractica_ID_SEQ")
    @SequenceGenerator(name = "OrdenPractica_ID_SEQ", sequenceName = "ORDEN_PRACTICA_ID_SEQ",
        allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DOCUMENTO_ID")
    public Long getDocumentoId() {
        return this.documentoId;
    }

    public void setDocumentoId(Long documentoId) {
        this.documentoId = documentoId;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ENVIO")
    public Date getFechaEnvio() {
        return this.fechaEnvio;
    }

    public void setFechaEnvio(Date fecha) {
        this.fechaEnvio = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to AvaluoAsignacionProfesional
    @OneToMany(mappedBy = "ordenPractica")
    public List<AvaluoAsignacionProfesional> getAvaluoAsignacionProfesionals() {
        return this.avaluoAsignacionProfesionals;
    }

    public void setAvaluoAsignacionProfesionals(
        List<AvaluoAsignacionProfesional> avaluoAsignacionProfesionals) {
        this.avaluoAsignacionProfesionals = avaluoAsignacionProfesionals;
    }

    /**
     * Determina si la orden de práctica es modificable o no dependiendo del estado.
     *
     * @author christian.rodriguez
     * @return true si el estado es generada, false en otro caso
     */
    @Transient
    public boolean isModificable() {
        if (this.estado != null &&
            this.estado.equalsIgnoreCase(EOrdenPracticaEstado.GENERADA
                .getCodigo())) {
            return true;
        }
        return false;
    }

}
