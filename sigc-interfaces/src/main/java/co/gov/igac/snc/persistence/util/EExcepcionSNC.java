package co.gov.igac.snc.persistence.util;

/**
 *
 * @author juan.agudelo
 *
 */
public enum EExcepcionSNC {

    //D: valor ("codigo")
    ERROR_DURANTE_LA_EJECUCION("DC0003");

    private String codigo;

    EExcepcionSNC(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
