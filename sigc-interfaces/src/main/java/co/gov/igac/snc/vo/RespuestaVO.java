package co.gov.igac.snc.vo;

import java.io.Serializable;

//TODO::franz.gamba::08-11-2011:: cambiar nombre y documentar la clase. El nombre del clase no dice
//  nada respecto a ella, y tampoco se puede inferir nada del paquete en que se encuentra:: pedro.garcia
/**
 *
 * @author franz.gamba
 *
 */
public class RespuestaVO implements Serializable {

    private static final long serialVersionUID = -3374794903895496635L;

    private String mensaje;
    private Object documentos;

    public static final String CARGA_ARCHIVO_OK = "El archivo fué cargado con éxito";

    /**
     *
     * @param isWorking
     * @param mensaje
     */
    public RespuestaVO(String mensaje) {

        this.mensaje = mensaje;
        this.documentos = null;
    }

    public RespuestaVO(String mensaje, Object documentos) {

        this.mensaje = mensaje;
        this.documentos = documentos;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getDocumentos() {
        return documentos;
    }

    public void setDocumentos(Object documentos) {
        this.documentos = documentos;
    }

}
