package co.gov.igac.snc.util;

/**
 * Enumeracion para los posibles valores del dominio LOG_ACCION_EVENTO.
 *
 * @author felipe.cadena
 */
public enum ELogAccionEvento {

    LOGIN,
    LOGOUT,
    ACCION;
}
