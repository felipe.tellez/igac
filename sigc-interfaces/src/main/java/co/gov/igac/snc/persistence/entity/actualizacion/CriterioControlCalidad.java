package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CRITERIO_CONTROL_CALIDAD database table.
 *
 */
@Entity
@Table(name = "CRITERIO_CONTROL_CALIDAD")
public class CriterioControlCalidad implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5384368211715986474L;

    private Long id;
    private String categoria;
    private String criterio;
    private Date fechaLog;
    private String usuarioLog;
    private List<ControlCalidadCriterio> controlCalidadCriterios;

    public CriterioControlCalidad() {
    }

    @Id
    @SequenceGenerator(name = "CRITERIO_CONTROL_CALIDA_ID_GENERATOR", sequenceName =
        "CRITERIO_CONTROL_CALIDA_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "CRITERIO_CONTROL_CALIDA_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false, length = 30)
    public String getCategoria() {
        return this.categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Column(nullable = false, length = 250)
    public String getCriterio() {
        return this.criterio;
    }

    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ControlCalidadCriterio
    @OneToMany(mappedBy = "criterioControlCalidad")
    public List<ControlCalidadCriterio> getControlCalidadCriterios() {
        return this.controlCalidadCriterios;
    }

    public void setControlCalidadCriterios(List<ControlCalidadCriterio> controlCalidadCriterios) {
        this.controlCalidadCriterios = controlCalidadCriterios;
    }

}
