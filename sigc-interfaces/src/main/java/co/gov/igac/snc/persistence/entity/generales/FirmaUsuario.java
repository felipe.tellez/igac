package co.gov.igac.snc.persistence.entity.generales;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * FIRMA_USUARIO entity.
 *
 * @author leidy.gonzalez
 */
@Entity
@Table(name = "FIRMA_USUARIO")
public class FirmaUsuario implements java.io.Serializable, Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = 5909724768466153907L;

    // Fields
    private Long id;
    private Documento documentoId;
    private String nombreFuncionarioFirma;
    private String usuarioIngresaFirma;
    private String usuarioLog;
    private Date fechaLog;
    private Date fechaFirmaDocumento;// Fecha de creación asignada por el usuario

    /**
     * almacena la ruta de ubicación del archivo en la máquina local (la del usuario), en la carpeta
     * temporal del sistema.
     */
    private String usuarioConFirma;

    // Constructors
    /** default constructor */
    public FirmaUsuario() {
    }

    /** full constructor */
    public FirmaUsuario(Long id, Documento documentoId, String funcionarioFirma,
        String nombreFuncionarioFirma, String usuarioLog, Date fechaLog,
        String usuarioIngresaFirma, Date fechaFirmaDocumento) {
        this.id = id;
        this.documentoId = documentoId;
        this.nombreFuncionarioFirma = nombreFuncionarioFirma;
        this.usuarioIngresaFirma = usuarioIngresaFirma;
        this.fechaFirmaDocumento = fechaFirmaDocumento;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FIRMA_USUARIO_ID_SEQ_KEY")
    @SequenceGenerator(name = "FIRMA_USUARIO_ID_SEQ_KEY", sequenceName = "FIRMA_USUARIO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DOCUMENTO_ID", nullable = false)
    public Documento getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(Documento documentoId) {
        this.documentoId = documentoId;
    }

    @Column(name = "NOMBRE_FUNCIONARIO_FIRMA", nullable = false, length = 100)
    public String getNombreFuncionarioFirma() {
        return nombreFuncionarioFirma;
    }

    public void setNombreFuncionarioFirma(String nombreFuncionarioFirma) {
        this.nombreFuncionarioFirma = nombreFuncionarioFirma;
    }

    @Column(name = "USUARIO_INGRESA_FIRMA", nullable = false, length = 250)
    public String getUsuarioIngresaFirma() {
        return usuarioIngresaFirma;
    }

    public void setUsuarioIngresaFirma(String usuarioIngresaFirma) {
        this.usuarioIngresaFirma = usuarioIngresaFirma;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "FECHA_FIRMA_DOCUMENTO", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaFirmaDocumento() {
        return fechaFirmaDocumento;
    }

    public void setFechaFirmaDocumento(Date fechaFirmaDocumento) {
        this.fechaFirmaDocumento = fechaFirmaDocumento;
    }

    @Column(name = "USUARIO_CON_FIRMA", nullable = true, length = 2)
    public String getUsuarioConFirma() {
        return usuarioConFirma;
    }

    public void setUsuarioConFirma(String usuarioConFirma) {
        this.usuarioConFirma = usuarioConFirma;
    }

    /**
     * @author leidy.gonzalez Método para poder hacer un clon de documentación
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }
//end of class    
}
