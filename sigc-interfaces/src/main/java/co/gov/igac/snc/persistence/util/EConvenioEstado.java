/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los estados de un convenio
 *
 * @author david.cifuentes
 */
public enum EConvenioEstado {

    APROBADO("Aprobado"),
    PROPUESTA("Prpuesta");

    private String codigo;

    private EConvenioEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }
}
