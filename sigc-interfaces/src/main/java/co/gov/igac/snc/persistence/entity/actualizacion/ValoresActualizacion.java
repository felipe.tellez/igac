/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.persistence.entity.actualizacion;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 * Entidad para relacionar los archivos del proceso de carga de tablas de valores para la
 * actulizacion
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "AX_VALORES_ACTUALIZACION")
public class ValoresActualizacion implements Serializable {

    private static final long serialVersionUID = -2219934966617987967L;
    private Long id;
    private Departamento departamento;
    private Municipio municipio;
    private String tipo;
    private String nombre;
    private Date fechaLog;
    private String usuarioLog;
    private String estado;
    private Long reglaId;
    private Long documentoId;

    //estructura tablas de liquidacion
    private String zona;
    private String sector;
    private String zhf;
    private String destino;
    private String uso;

    private Date vigencia;
    private Long reporteId;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AX_VALORES_ACTUALIZA_ID_SEQ_GEN")
    @SequenceGenerator(name = "AX_VALORES_ACTUALIZA_ID_SEQ_GEN", sequenceName =
        "AX_VALORES_ACTUALIZA_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "TIPO_ARCHIVO")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "NOMBRE_ARCHIVO")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "VALORES_ACTUALIZACION_ID")
    public Long getReglaId() {
        return reglaId;
    }

    public void setReglaId(Long reglaId) {
        this.reglaId = reglaId;
    }

    @Column(name = "DOCUMENTO_ID")
    public Long getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(Long documentoId) {
        this.documentoId = documentoId;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getZhf() {
        return zhf;
    }

    public void setZhf(String zhf) {
        this.zhf = zhf;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getUso() {
        return uso;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    @Column(name = "VIGENCIA", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getVigencia() {
        return vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "REPORTE_ID")
    public Long getReporteId() {
        return reporteId;
    }

    public void setReporteId(Long reporteId) {
        this.reporteId = reporteId;
    }

    //Transients para obtener el primer datos asociado a la tabla deactualizacion
    @Transient
    public String getFirstZona() {
        return this.getFirstValue(this.zona);
    }

    @Transient
    public String getFirstSector() {
        return this.getFirstValue(this.sector);
    }

    @Transient
    public String getFirstZhf() {
        return this.getFirstValue(this.zhf);
    }

    @Transient
    public String getFirstDestino() {
        return this.getFirstValue(this.destino);
    }

    @Transient
    public String getFirstUso() {
        return this.getFirstValue(this.uso);
    }

    private String getFirstValue(String dato) {

        if (dato != null && !dato.isEmpty()) {
            String first;
            if (!dato.contains(",")) {
                return dato;
            } else {
                first = dato.substring(0, dato.indexOf(","));
                return first;
            }
        } else {
            return "";
        }

    }
}
