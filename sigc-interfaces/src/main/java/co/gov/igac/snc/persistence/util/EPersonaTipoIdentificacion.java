package co.gov.igac.snc.persistence.util;

public enum EPersonaTipoIdentificacion {
    CEDULA_CIUDADANIA("CC"),
    CEDULA_EXTRANJERIA("CE"),
    CEDULA_MILITAR("CM"),
    LIBRETA_MILITAR("LM"),
    NIT("NIT"),
    NO_ESPECIFICADO("X"),
    NUIP("NUIP"),
    PASAPORTE("P"),
    REGISTRO_CIVIL("RC"),
    TARJETA_IDENTIDAD("TI");

    private String codigo;

    EPersonaTipoIdentificacion(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
