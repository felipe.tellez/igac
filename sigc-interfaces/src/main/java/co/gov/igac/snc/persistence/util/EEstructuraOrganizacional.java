package co.gov.igac.snc.persistence.util;

//TODO::fredy.wilches::01-08-2011::¿por qué no documenta quién creó esto?::pedro.garcia
/**
 *
 * @modified by pedro.garcia se movió de paquete para que coincida con las otras enumeraciones del
 * mismo tipo
 */
public enum EEstructuraOrganizacional {
    DELEGACION("DELEGACION"),
    DEPENDENCIA("DEPENDENCIA"),
    DIRECCION("DIRECCION"),
    DIRECCION_TERRITORIAL("DIRECCION TERRITORIAL"),
    GRUPO_INTERNO_TRABAJO("GIT"),
    OFICINA_APOYO("OFICINA DE APOYO"),
    SUBDIRECCION("SUBDIRECCION"),
    UNIDAD_OPERATIVA_CATASTRAL("UOC"),
    HABILITADA("HABILITADA");

    private String tipo;

    private EEstructuraOrganizacional(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

}
