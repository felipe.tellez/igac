package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Entidad para el control de reportes
 *
 * @author javier.aponte
 *
 */
@Entity
@Table(name = "REP_CONTROL_REPORTE")
public class RepControlReporte implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String descripcion;
    private String estado;
    private Date fechaFin;
    private Date fechaInicio;
    private String idRepositorioDocumentos;
    private BigDecimal logErrorId;
    private BigDecimal logMensajeId;
    private String parametros;
    private String reporte;
    private String usuario;
    private List<RepDatosReporte> repDatosReportes;

    public RepControlReporte() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REP_CONTROL_REPORTE_ID_SEQ")
    @SequenceGenerator(name = "REP_CONTROL_REPORTE_ID_SEQ", sequenceName =
        "REP_CONTROL_REPORTE_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Column(name = "ID_REPOSITORIO_DOCUMENTOS")
    public String getIdRepositorioDocumentos() {
        return this.idRepositorioDocumentos;
    }

    public void setIdRepositorioDocumentos(String idRepositorioDocumentos) {
        this.idRepositorioDocumentos = idRepositorioDocumentos;
    }

    @Column(name = "LOG_ERROR_ID")
    public BigDecimal getLogErrorId() {
        return this.logErrorId;
    }

    public void setLogErrorId(BigDecimal logErrorId) {
        this.logErrorId = logErrorId;
    }

    @Column(name = "LOG_MENSAJE_ID")
    public BigDecimal getLogMensajeId() {
        return this.logMensajeId;
    }

    public void setLogMensajeId(BigDecimal logMensajeId) {
        this.logMensajeId = logMensajeId;
    }

    public String getParametros() {
        return this.parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public String getReporte() {
        return this.reporte;
    }

    public void setReporte(String reporte) {
        this.reporte = reporte;
    }

    public String getUsuario() {
        return this.usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    //bi-directional many-to-one association to RepDatosReporte
    @OneToMany(mappedBy = "repControlReporte")
    public List<RepDatosReporte> getRepDatosReportes() {
        return this.repDatosReportes;
    }

    public void setRepDatosReportes(List<RepDatosReporte> repDatosReportes) {
        this.repDatosReportes = repDatosReportes;
    }

    public RepDatosReporte addRepDatosReporte(RepDatosReporte repDatosReporte) {
        getRepDatosReportes().add(repDatosReporte);
        repDatosReporte.setRepControlReporte(this);

        return repDatosReporte;
    }

    public RepDatosReporte removeRepDatosReporte(RepDatosReporte repDatosReporte) {
        getRepDatosReportes().remove(repDatosReporte);
        repDatosReporte.setRepControlReporte(null);

        return repDatosReporte;
    }

}
