package co.gov.igac.snc.vo;

import java.io.Serializable;

/**
 * Clase para transportar los datos de una imagen del predio generada desde Arcgis Server
 *
 * @author juan.mendez
 */
public class ImagenPredioVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4850580167475845795L;

    private String numeroPredial;
    private String nombreArchivo;
    private String imagenStringBase64;

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getImagenStringBase64() {
        return imagenStringBase64;
    }

    public void setImagenStringBase64(String imagenStringBase64) {
        this.imagenStringBase64 = imagenStringBase64;
    }

    /** Empty constructor */
    public ImagenPredioVO() {

    }
}
