package co.gov.igac.snc.util.constantes;

/**
 *
 * Clase para manejar constantes usadas en DiligenciamientoSolicitudMB
 *
 * @author rodrigo.hernandez
 *
 */
public class ConstantesDiligenciamientoSolicitudAvaluoComercial {

    /**
     * Id panel de informacion del solicitante
     */
    public static final int CODIGO_PANEL_INFO_SOLICITANTE = 0;
    /**
     * Id panel de informacion del tipo de tramite
     */
    public static final int CODIGO_PANEL_INFO_TIPO_TRAMITE = 1;
    /**
     * Id panel de informacion de predios
     */
    public static final int CODIGO_PANEL_INFO_PREDIOS = 2;
    /**
     * Id panel de informacion de documentos
     */
    public static final int CODIGO_PANEL_INFO_DOCUMENTOS = 3;
    /**
     * Id panel de informacion de forma de pago
     */
    public static final int CODIGO_PANEL_INFO_FORMA_PAGO = 4;
}
