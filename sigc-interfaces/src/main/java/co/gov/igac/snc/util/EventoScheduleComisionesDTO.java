/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase DTO para transportar los datos que se necesitan para armar un evento para el schedule de
 * comisiones.
 *
 * Se usa para evitar hacer repetidas consultas a bd desde la capa web, haciendo todas las consultas
 * en la capa de negocio y devolviendo un objeto con los datos necesarios.
 *
 *
 * @author pedro.garcia
 */
public class EventoScheduleComisionesDTO implements Serializable {

    private static final long serialVersionUID = 15667839067870784L;

    private Date fechaInicio;

    private Date fechaFin;

    private String titulo;

//--------------------------------------------------------------------------------------------------    
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

}
