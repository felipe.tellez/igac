package co.gov.igac.snc.persistence.entity.generales;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Usuario entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "USUARIO", schema = "SNC_GENERALES", uniqueConstraints = @UniqueConstraint(
    columnNames = "IDENTIFICADOR"))
public class Usuario implements java.io.Serializable {

    // Fields
    private Long id;
    private Usuario usuario;
    private String identificador;
    private String nombre;
    private String estructuraOrganizacionalCod;
    private String activo;
    private String usuarioLog;
    private Date fechaLog;
    private Set<Usuario> usuarios = new HashSet<Usuario>(0);
    private Set<GrupoUsuario> grupoUsuarios = new HashSet<GrupoUsuario>(0);

    // Constructors
    /** default constructor */
    public Usuario() {
    }

    /** minimal constructor */
    public Usuario(Long id, String identificador,
        String estructuraOrganizacionalCod, String activo,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.identificador = identificador;
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
        this.activo = activo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Usuario(Long id, Usuario usuario, String identificador,
        String nombre, String estructuraOrganizacionalCod, String activo,
        String usuarioLog, Date fechaLog, Set<Usuario> usuarios,
        Set<GrupoUsuario> grupoUsuarios) {
        this.id = id;
        this.usuario = usuario;
        this.identificador = identificador;
        this.nombre = nombre;
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
        this.activo = activo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.usuarios = usuarios;
        this.grupoUsuarios = grupoUsuarios;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PADRE_USUARIO_ID")
    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Column(name = "IDENTIFICADOR", unique = true, nullable = false, length = 50)
    public String getIdentificador() {
        return this.identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    @Column(name = "NOMBRE", length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD", nullable = false, length = 20)
    public String getEstructuraOrganizacionalCod() {
        return this.estructuraOrganizacionalCod;
    }

    public void setEstructuraOrganizacionalCod(
        String estructuraOrganizacionalCod) {
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
    }

    @Column(name = "ACTIVO", nullable = false, length = 2)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usuario")
    public Set<Usuario> getUsuarios() {
        return this.usuarios;
    }

    public void setUsuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usuario")
    public Set<GrupoUsuario> getGrupoUsuarios() {
        return this.grupoUsuarios;
    }

    public void setGrupoUsuarios(Set<GrupoUsuario> grupoUsuarios) {
        this.grupoUsuarios = grupoUsuarios;
    }

}
