package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de bd RegPreCriterioBusqueda
 *
 * @author javier.aponte
 */
public enum ERegPreCriterioBusqueda {

    POR_PREDIO("1"),
    POR_NOMBRE_DE_PROPIETARIO("2"),
    POR_DOCUMENTO_DE_IDENTIDAD("3");

    private String codigo;

    private ERegPreCriterioBusqueda(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
