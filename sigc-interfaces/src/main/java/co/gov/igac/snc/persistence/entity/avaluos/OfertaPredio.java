package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the OFERTA_PREDIO database table.
 *
 * @author christian.rodriguez
 * @description Se crea está clase para asociar varios predios a una misma oferta inmobiliaria
 *
 */
@Entity
@Table(name = "OFERTA_PREDIO")
public class OfertaPredio implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private Date fechaLog;
    private String numeroPredial;
    private String observaciones;
    private java.math.BigDecimal predioId;
    private String usuarioLog;
    private OfertaInmobiliaria ofertaInmobiliaria;

    public OfertaPredio() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OFERTA_PREDIO_ID_SEQ")
    @SequenceGenerator(name = "OFERTA_PREDIO_ID_SEQ", sequenceName = "OFERTA_PREDIO_ID_SEQ",
        allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NUMERO_PREDIAL", length = 30)
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "PREDIO_ID", precision = 10)
    public java.math.BigDecimal getPredioId() {
        return this.predioId;
    }

    public void setPredioId(java.math.BigDecimal predioId) {
        this.predioId = predioId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to OfertaInmobiliaria
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OFERTA_INMOBILIARIA_ID", nullable = false)
    public OfertaInmobiliaria getOfertaInmobiliaria() {
        return this.ofertaInmobiliaria;
    }

    public void setOfertaInmobiliaria(OfertaInmobiliaria ofertaInmobiliaria) {
        this.ofertaInmobiliaria = ofertaInmobiliaria;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((numeroPredial == null) ? 0 : numeroPredial.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OfertaPredio other = (OfertaPredio) obj;
        if (numeroPredial == null) {
            if (other.numeroPredial != null) {
                return false;
            }
        } else if (!numeroPredial.equals(other.numeroPredial)) {
            return false;
        }
        return true;
    }

}
