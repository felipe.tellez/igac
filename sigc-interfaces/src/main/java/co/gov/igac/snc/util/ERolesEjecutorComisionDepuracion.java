/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con los posibles roles que tiene el ejecutor de una comisión de depuración
 *
 * @author pedro.garcia
 */
public enum ERolesEjecutorComisionDepuracion {

    DIGITALIZADOR("Digitalizador"),
    EJECUTOR("Ejecutor"),
    TOPOGRAFO("Topógrafo");

    private String nombre;

    private ERolesEjecutorComisionDepuracion(String rolN) {
        this.nombre = rolN;
    }

    public String getNombre() {
        return this.nombre;
    }

}
