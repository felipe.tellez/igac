package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de bd Seccion_Reportes_Prediales
 *
 * @author leidy.gonzalez
 */
public enum ESeccionReportesPrediales {
    OBLIGATORIO("O"),
    OPCIONAL("OP"),
    DESABILITADO("D");

    private String codigo;

    private ESeccionReportesPrediales(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
