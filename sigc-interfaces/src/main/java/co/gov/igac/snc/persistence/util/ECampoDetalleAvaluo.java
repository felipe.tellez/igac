package co.gov.igac.snc.persistence.util;

/**
 *
 * @author felipe.cadena
 */
public interface ECampoDetalleAvaluo {

    public String getCodigo();

    public String getValor();

}
