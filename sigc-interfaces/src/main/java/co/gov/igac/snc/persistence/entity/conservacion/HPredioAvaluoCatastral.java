package co.gov.igac.snc.persistence.entity.conservacion;

import java.sql.Timestamp;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * HPredioAvaluoCatastral entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "H_PREDIO_AVALUO_CATASTRAL", schema = "SNC_CONSERVACION")
public class HPredioAvaluoCatastral implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -2411287701203034122L;
    private HPredioAvaluoCatastralId id;
    private HPredio HPredio;
    private Predio predio;
    private String autoavaluo;
    private String justificacionAvaluo;
    private String notaAvaluo;
    private Double valorTotalAvaluoCatastral;
    private Double valorTerreno;
    private Double valorTerrenoPrivado;
    private Double valorTerrenoComun;
    private Double valorTotalConstruccionPrivada;
    private Double valorConstruccionComun;
    private Double valorTotalConstruccion;
    private Double valorTotalConsConvencional;
    private Double valorTotalConsNconvencional;
    private Timestamp vigencia;
    private Timestamp fechaInscripcionCatastral;
    private String cancelaInscribe;
    private String usuarioLog;
    private Timestamp fechaLog;
    private Double valorTotalAvaluoVigencia;

    // Constructors
    /** default constructor */
    public HPredioAvaluoCatastral() {
    }

    /** minimal constructor */
    public HPredioAvaluoCatastral(HPredioAvaluoCatastralId id, HPredio HPredio,
        Predio predio, String autoavaluo, Double valorTotalAvaluoCatastral,
        Double valorTerreno, Double valorTotalConstruccion,
        Double valorTotalConsConvencional, Timestamp vigencia,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predio = predio;
        this.autoavaluo = autoavaluo;
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
        this.valorTerreno = valorTerreno;
        this.valorTotalConstruccion = valorTotalConstruccion;
        this.valorTotalConsConvencional = valorTotalConsConvencional;
        this.vigencia = vigencia;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public HPredioAvaluoCatastral(HPredioAvaluoCatastralId id, HPredio HPredio,
        Predio predio, String autoavaluo, String justificacionAvaluo,
        String notaAvaluo, Double valorTotalAvaluoCatastral,
        Double valorTerreno, Double valorTerrenoPrivado, Double valorTerrenoComun,
        Double valorTotalConstruccionPrivada, Double valorConstruccionComun,
        Double valorTotalConstruccion,
        Double valorTotalConsConvencional,
        Double valorTotalConsNconvencional, Timestamp vigencia,
        Timestamp fechaInscripcionCatastral, String cancelaInscribe,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predio = predio;
        this.autoavaluo = autoavaluo;
        this.justificacionAvaluo = justificacionAvaluo;
        this.notaAvaluo = notaAvaluo;
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
        this.valorTerreno = valorTerreno;
        this.valorTerrenoPrivado = valorTerrenoPrivado;
        this.valorTerrenoComun = valorTerrenoComun;
        this.valorTotalConstruccionPrivada = valorTotalConstruccionPrivada;
        this.valorConstruccionComun = valorConstruccionComun;
        this.valorTotalConstruccion = valorTotalConstruccion;
        this.valorTotalConsConvencional = valorTotalConsConvencional;
        this.valorTotalConsNconvencional = valorTotalConsNconvencional;
        this.vigencia = vigencia;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "HPredioId", column = @Column(name = "H_PREDIO_ID", nullable =
            false, precision = 10, scale = 0)),
        @AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false, precision =
            10, scale = 0))})
    public HPredioAvaluoCatastralId getId() {
        return this.id;
    }

    public void setId(HPredioAvaluoCatastralId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "H_PREDIO_ID", nullable = false, insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

//	@Column(name = "PREDIO_ID", nullable = false, precision = 10, scale = 0)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "AUTOAVALUO", nullable = false, length = 2)
    public String getAutoavaluo() {
        return this.autoavaluo;
    }

    public void setAutoavaluo(String autoavaluo) {
        this.autoavaluo = autoavaluo;
    }

    @Column(name = "JUSTIFICACION_AVALUO", length = 600)
    public String getJustificacionAvaluo() {
        return this.justificacionAvaluo;
    }

    public void setJustificacionAvaluo(String justificacionAvaluo) {
        this.justificacionAvaluo = justificacionAvaluo;
    }

    @Column(name = "NOTA_AVALUO", length = 600)
    public String getNotaAvaluo() {
        return this.notaAvaluo;
    }

    public void setNotaAvaluo(String notaAvaluo) {
        this.notaAvaluo = notaAvaluo;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_CATASTRAL", nullable = false, precision = 18)
    public Double getValorTotalAvaluoCatastral() {
        return this.valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(Double valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    @Column(name = "VALOR_TOTAL_TERRENO", nullable = false, precision = 18)
    public Double getValorTerreno() {
        return this.valorTerreno;
    }

    public void setValorTerreno(Double valorTerreno) {
        this.valorTerreno = valorTerreno;
    }

    @Column(name = "VALOR_TERRENO_PRIVADO", nullable = false, precision = 18)
    public Double getValorTerrenoPrivado() {
        return this.valorTerrenoPrivado;
    }

    public void setValorTerrenoPrivado(Double valorTerrenoPrivado) {
        this.valorTerrenoPrivado = valorTerrenoPrivado;
    }

    @Column(name = "VALOR_TERRENO_COMUN", nullable = false, precision = 18)
    public Double getValorTerrenoComun() {
        return this.valorTerrenoComun;
    }

    public void setValorTerrenoComun(Double valorTerrenoComun) {
        this.valorTerrenoComun = valorTerrenoComun;
    }

    @Column(name = "VALOR_TOTAL_CONS_PRIVADA", nullable = false, precision = 18)
    public Double getValorTotalConstruccionPrivada() {
        return this.valorTotalConstruccionPrivada;
    }

    public void setValorTotalConstruccionPrivada(Double valorTotalConstruccionPrivada) {
        this.valorTotalConstruccionPrivada = valorTotalConstruccionPrivada;
    }

    @Column(name = "VALOR_CONSTRUCCION_COMUN", nullable = false, precision = 18)
    public Double getValorConstruccionComun() {
        return this.valorConstruccionComun;
    }

    public void setValorConstruccionComun(Double valorConstruccionComun) {
        this.valorConstruccionComun = valorConstruccionComun;
    }

    @Column(name = "VALOR_TOTAL_CONSTRUCCION", nullable = false, precision = 18)
    public Double getValorTotalConstruccion() {
        return this.valorTotalConstruccion;
    }

    public void setValorTotalConstruccion(Double valorTotalConstruccion) {
        this.valorTotalConstruccion = valorTotalConstruccion;
    }

    @Column(name = "VALOR_CONS_CONVENCIONAL", nullable = false, precision = 18)
    public Double getValorTotalConsConvencional() {
        return this.valorTotalConsConvencional;
    }

    public void setValorTotalConsConvencional(Double valorTotalConsConvencional) {
        this.valorTotalConsConvencional = valorTotalConsConvencional;
    }

    @Column(name = "VALOR_CONS_NO_CONVENCIONAL", precision = 18)
    public Double getValorTotalConsNconvencional() {
        return this.valorTotalConsNconvencional;
    }

    public void setValorTotalConsNconvencional(
        Double valorTotalConsNconvencional) {
        this.valorTotalConsNconvencional = valorTotalConsNconvencional;
    }

    @Column(name = "VIGENCIA", nullable = false, length = 7)
    public Timestamp getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Timestamp vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    public Timestamp getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Timestamp fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_VIGENCIA", nullable = false, precision = 18)
    public Double getValorTotalAvaluoVigencia() {
        return valorTotalAvaluoVigencia;
    }

    public void setValorTotalAvaluoVigencia(Double valorTotalAvaluoVigencia) {
        this.valorTotalAvaluoVigencia = valorTotalAvaluoVigencia;
    }

}
