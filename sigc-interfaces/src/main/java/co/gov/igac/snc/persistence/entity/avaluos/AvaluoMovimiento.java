/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.EAvaluoMovimientoJustifica;

/**
 * The persistent class for the AVALUO_MOVIMIENTO database table.
 *
 * @modified christian.rodriguez se agrega relación con la tabla de avaluo
 */
@Entity
@Table(name = "AVALUO_MOVIMIENTO")
public class AvaluoMovimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long diasAmpliacion;
    private Date fecha;
    private Date fechaDesde;
    private Date fechaHasta;
    private Date fechaLog;
    private String justificacion;
    private String otraJustificacion;
    private Long profesionalAvaluosId;
    private String tipoMovimiento;
    private String usuarioLog;
    private String usuarioMovimientoId;

    private Avaluo avaluo;

    public AvaluoMovimiento() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_MOVIMIENTO_ID_GENERATOR", sequenceName =
        "AVALUO_MOVIMIENTO_ID_SEQ", allocationSize = 1)
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AVALUO_MOVIMIENTO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DIAS_AMPLIACION")
    public Long getDiasAmpliacion() {
        return this.diasAmpliacion;
    }

    public void setDiasAmpliacion(Long diasAmpliacion) {
        this.diasAmpliacion = diasAmpliacion;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DESDE")
    public Date getFechaDesde() {
        return this.fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_HASTA")
    public Date getFechaHasta() {
        return this.fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getJustificacion() {
        return this.justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    @Column(name = "OTRA_JUSTIFICACION")
    public String getOtraJustificacion() {
        return this.otraJustificacion;
    }

    public void setOtraJustificacion(String otraJustificacion) {
        this.otraJustificacion = otraJustificacion;
    }

    @Column(name = "PROFESIONAL_AVALUOS_ID")
    public Long getProfesionalAvaluosId() {
        return this.profesionalAvaluosId;
    }

    public void setProfesionalAvaluosId(Long profesionalAvaluosId) {
        this.profesionalAvaluosId = profesionalAvaluosId;
    }

    @Column(name = "TIPO_MOVIMIENTO")
    public String getTipoMovimiento() {
        return this.tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "USUARIO_MOVIMIENTO_ID")
    public String getUsuarioMovimientoId() {
        return this.usuarioMovimientoId;
    }

    public void setUsuarioMovimientoId(String usuarioMovimientoId) {
        this.usuarioMovimientoId = usuarioMovimientoId;
    }

    // bi-directional many-to-one association to Avaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    @Transient
    public String getDescripcionJustificacion() {

        String descJustificacion = "";

        if (this.justificacion != null) {
            if (this.justificacion
                .equals(EAvaluoMovimientoJustifica.OTRA_JUSTIFICACION
                    .getCodigo())) {
                descJustificacion = this.getOtraJustificacion();
            } else {
                descJustificacion = this.getJustificacion();
            }
        }

        return descJustificacion;
    }

}
