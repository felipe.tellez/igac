/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import co.gov.igac.snc.persistence.entity.tramite.Solicitante;

/**
 *
 * Interfaz creada para las clases de SolicitanteSolicitud y SolicitanteTramite. En el momento de
 * creación se hizo específicamente para evitar tener que hacer introspección de clases en el método
 * que me devuelve el solicitante a quien se envía la comunicación de notificación, ya que debía
 * recibir ambos tipos de solicitantes -como un genérico- y por introspección ejecutar algunos
 * métodos. Se definien los métodos comunes a ambas clases y que se usan para obtener la información
 * necesaria para armar objetos Solicitante que son los que se usan en las interfaces de usuario
 *
 *
 * @author pedro.garcia
 */
public interface ISolicitanteSolicitudOTramite {

    /**
     * retorna el Solicitante como tal de la Solicitud o el Tramite
     *
     * @return
     */
    public Solicitante getSolicitante();

    /**
     * retorna la relación del solicitante con el Tramite o Solicitud
     *
     * @return
     */
    public String getRelacion();

    /**
     * retorna el id del solicitante
     *
     * @return
     */
    public Long getId();

}
