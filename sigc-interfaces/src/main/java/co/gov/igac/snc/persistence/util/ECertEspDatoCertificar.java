package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de bd CertEspDatoCertificar
 *
 * @author javier.aponte
 */
public enum ECertEspDatoCertificar {

    ULTIMO_ACTO_ADMINISTRATIVO("1"),
    AVALUOS_ANTERIORES("2"),
    DESTINO_ECONOMICO("3"),
    NUMERO_PREDIAL_ANTERIOR("4"),
    PREDIOS_COLINDANTES("5"),
    JUSTIFICACION_DEL_DERECHO_DE_PROPIEDAD_O_POSESION("6"),
    ZONA_GEOECONOMICA("7"),
    COTAS("8");

    private String codigo;

    private ECertEspDatoCertificar(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
