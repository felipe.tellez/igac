/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio de OFERTA_UNIDAD_AREA
 *
 * @author christian.rodriguez
 */
public enum EOfertaUnidadArea {

    CUADRA("CUADRA"),
    FANEGADA("FANEGADA"),
    HECTAREAS("HECTAREAS"),
    METRO_CUADRADO("METRO CUADRADO"),
    PLAZA("PLAZA");

    private String codigo;

    private EOfertaUnidadArea(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }
}
