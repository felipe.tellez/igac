package co.gov.igac.snc.persistence.util;

public enum EPersonaTipoPersona {
    JURIDICA("JURIDICA"),
    NATURAL("NATURAL");

    private String codigo;

    EPersonaTipoPersona(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
