package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los valores y códigos del dominio PREDIO_CONDICION_PROPIEDAD
 *
 * @author ?
 * @documented pedro.garcia
 * @modified by fredy.wilches Enumeraciones modificadas para facilitar comparacion y hacer mas
 * legible en algunos casos el codigo fuente, el codigo de estas corresponde a los codigos
 * anteriores que tienen la descripcion larga
 */
public enum EPredioCondicionPropiedad {

    CP_0("0", "Predio no reglamentado en PH", "PREDIO_NO_REGLAMENTADO_EN_PH"),
    CP_1("1", "Mixto", "PREDIO_EN_PH_COMDOMINIO"),
    CP_2("2", "No Ley 14", "NO_LEY_14"),
    CP_3("3", "Bienes de uso público diferentes a las vías",
        "BIENES_DE_USO_PUBLICO_DIFERENTES_A_LAS_VIAS"),
    CP_4("4", "Vías", "VIAS"),
    CP_5("5", "Mejoras por edificaciones en terreno ajeno de propiedades no reglamentadas en PH",
        "MEJORAS_POR_EDIF_EN_TERRENO_AJENO_DE_PROPS_NO_REGLAMENT_EN_PH"),
    CP_6("6", "Mejoras por edificaciones en terreno ajeno en PH",
        "MEJORAS_POR_EDIF_EN_TERRENO_AJENO_EN_PH"),
    CP_7("7", "Parques cementerios", "PARQUES_CEMENTERIOS"),
    CP_8("8", "Predio en condominio", "PREDIO_EN_CONDOMINIO"),
    CP_9("9", "Predio en PH", "PREDIO_EN_PH");

    private String codigo;
    private String valor;
    private String descripcion;

    EPredioCondicionPropiedad(String codigo, String valor, String descripcion) {
        this.codigo = codigo;
        this.valor = valor;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

}
