/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * @author felipe.cadena
 */
public enum EInfoAdicionalCampo {

    ENGLOBE_VIRTUAL("ENGLOBE_VIRTUAL", "ENGLOBE VIRTUAL"),
    NUEVA_FICHA_MATRIZ_EV("NUEVA_FICHA_MATRIZ_EV", "NUEVA FICHA MATRIZ EV"),
    NUMERO_FICHA_MATRIZ_EV("NUMERO_FICHA_MATRIZ_EV", "NUMERO FICHA MATRIZ EV"),
    TIPO_FICHA_MATRIZ_EV("TIPO_FICHA_MATRIZ_EV", "TIPO FICHA MATRIZ EV"),
    ENGLOBE_VIRTUAL_APROBADO("ENGLOBE_VIRTUAL_APROBADO", "ENGLOBE VIRTUAL APROBADO"),;

    private String codigo;
    private String valor;

    private EInfoAdicionalCampo(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
