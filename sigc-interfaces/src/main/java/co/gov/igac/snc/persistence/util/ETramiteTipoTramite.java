package co.gov.igac.snc.persistence.util;

/**
 *
 * Enumeración para el dominio TRAMITE_TIPO_TRAMITE
 *
 * @author ...
 * @modified juan.agudelo adición del atributo nombre
 *
 */
public enum ETramiteTipoTramite {
    AUTOESTIMACION("12", "Auto estimación"),
    CANCELACION_DE_PREDIO("4", "Cancelación de predio"),
    COMPLEMENTACION("3", "Complementación"),
    DERECHO_DE_PETICION("7", "Derecho de petición"),
    MODIFICACION_INSCRIPCION_CATASTRAL("5", "Modificación de inscripción catastral"),
    MUTACION("1", "Mutación"),
    RECURSO_DE_QUEJA("11", "Recurso de queja"),
    RECTIFICACION("2", "Rectificación"),
    RECURSO_DE_APELACION("9", "Recurso de apelación"),
    RECURSO_DE_REPOSICION("8", "Recurso de reposición"),
    RECURSO_REPOSICION_EN_SUBSIDIO_APELACION("10", "Recurso de reposición en subsidio de apelación"),
    REVISION_DE_AVALUO("6", "Revisión de avalúo"),
    REVOCATORIA_DIRECTA("13", "Revocatoria directa"),
    SOLICITUD_DE_COTIZACION("14", "Solicitud de cotización"),
    SOLICITUD_DE_AVALUO("15", "Solicitud de avalúo"),
    SOLICITUD_DE_IMPUGNACION("16", "Solicitud de impugnación"),
    SOLICITUD_DE_REVISION("17", "Solicitud de revisión"),
    SOLICITUD_DE_CORRECCION("18", "Solicitud de corrección"),
    VIA_GUBERNATIVA("11", "Via Gubernativa");

    private String codigo;
    private String nombre;

    private ETramiteTipoTramite(String codigo, String valor) {
        this.codigo = codigo;
        this.nombre = valor;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
