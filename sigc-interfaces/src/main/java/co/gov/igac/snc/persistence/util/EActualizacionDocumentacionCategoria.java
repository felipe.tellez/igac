package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para categorizar la documentacion de la actualizacion
 *
 * @author franz.gamba
 *
 */
public enum EActualizacionDocumentacionCategoria {

    GENERAL("General"),
    GEOREFERENCIADO("Georeferenciado"),
    RECONOCIMIENTO_PREDIAL("Reconocimiento predial");

    private String descripcion;

    EActualizacionDocumentacionCategoria(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
