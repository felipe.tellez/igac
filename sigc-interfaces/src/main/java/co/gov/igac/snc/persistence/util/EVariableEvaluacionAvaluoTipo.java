/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los tipos de variable evaluacion de avalúo.
 *
 * @author felipe.cadena
 */
public enum EVariableEvaluacionAvaluoTipo {
    CALIDAD("CALIDAD"),
    EFICACIA("EFICACIA"),
    EFICIENCIA("EFICIENCIA");

    private String valor;

    private EVariableEvaluacionAvaluoTipo(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return this.valor;
    }
}
