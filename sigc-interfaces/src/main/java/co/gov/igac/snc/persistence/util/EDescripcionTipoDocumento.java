package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para recuperar la descripción para cada uno de los Tipos de documentos
 *
 * Corresponden a los que están el la tabla TIPO_DOCUMENTO
 *
 * @author juan.agudelo
 */
public enum EDescripcionTipoDocumento {

    //D: nombre (id en la tabla)
    CERTIFICADO_DE_LIBERTAD_Y_TRADICION_ORIGINAL_O_COPIA(
        "//TODO debe poner la descripción acorde al tipo de doc"),
    CERTIFICADO_DE_NOMENCLATURA("//TODO debe poner la descripción acorde al tipo de doc"),
    COMUNICACION_DE_NOTIFICACION("//TODO debe poner la descripción acorde al tipo de doc"),
    DECLARACION_DE_CONSTRUCCION("//TODO debe poner la descripción acorde al tipo de doc"),
    DOCUMENTO_DE_AUTORIZACION_DE_NOTIFICACION(
        "//TODO debe poner la descripción acorde al tipo de doc"),
    DOCUMENTO_DE_NOTIFICACION("//TODO debe poner la descripción acorde al tipo de doc"),
    DOCUMENTO_DONDE_SE_ESPECIFIQUE_EL_AREA_DE_CONSTRUCCION_CON_SU_RESPECTIVO_VALOR_POR_METRO_CUADRADO(
        "//TODO debe poner la descripción acorde al tipo de doc"),
    DOCUMENTO_DONDE_SE_ESPECIFIQUE_EL_AREA_DE_TERRENO_CON_SU_RESPECTIVO_VALOR_POR_METRO_CUADRADO(
        "//TODO debe poner la descripción acorde al tipo de doc"),
    DOCUMENTO_QUE_SUSTENTE_LA_SOLICITUD("//TODO debe poner la descripción acorde al tipo de doc"),
    DOC_BLOQUEO_MASIVO_PREDIO("//TODO debe poner la descripción acorde al tipo de doc"),
    DOC_BLOQUEO_MASIVO_PERSONA("//TODO debe poner la descripción acorde al tipo de doc"),
    DOC_SOPORTE_BLOQUEO_PREDIO("//TODO debe poner la descripción acorde al tipo de doc"),
    DOC_SOPORTE_BLOQUEO_PERSONA("//TODO debe poner la descripción acorde al tipo de doc"),
    DOC_SOPORTE_CANCELACION("//TODO debe poner la descripción acorde al tipo de doc"),
    DOC_SOPORTE_DESBLOQUEO_PREDIO("//TODO debe poner la descripción acorde al tipo de doc"),
    DOC_SOPORTE_DESBLOQUEO_PERSONA("//TODO debe poner la descripción acorde al tipo de doc"),
    DOCUMENTO_RESULTADO_DE_PRUEBAS("//TODO debe poner la descripción acorde al tipo de doc"),
    EDICTO("Documento de edicto generado por el sistema"),
    FOTOCOPIA_DE_ESCRITURA_PUBLICA("//TODO debe poner la descripción acorde al tipo de doc"),
    FOTOCOPIA_DE_LA_CEDULA("//TODO debe poner la descripción acorde al tipo de doc"),
    FOTOCOPIA_DE_LA_ESCRITURA_PUBLICA_AJUSTADA(
        "//TODO debe poner la descripción acorde al tipo de doc"),
    FOTOCOPIA_DEL_CERTIFICADO_DE_EXISTENCIA_Y_REPRESENTACION_LEGAL15(
        "//TODO debe poner la descripción acorde al tipo de doc"),
    FOTOCOPIA_DE_LA_ESCRITURA_PUBLICA_DONDE_SE_INCLUYE_EL_REGIMEN_DE_PROPIEDAD_HORIZONTAL(
        "//TODO debe poner la descripción acorde al tipo de doc"),
    FOTOCOPIA_DE_LA_ESCRITURA_PUBLICA_O_CERTIFICADO_DE_LIBERTAD_ORIGINAL_O_COPIA(
        "//TODO debe poner la descripción acorde al tipo de doc"),
    FOTOGRAFIA("//TODO debe poner la descripción acorde al tipo de doc"),
    IMA_GEO_PREDIO_ANTES_TRAMITE("//TODO debe poner la descripción acorde al tipo de doc"),
    IMA_GEO_PREDIO_DESPUES_TRAMITE("//TODO debe poner la descripción acorde al tipo de doc"),
    OFICIOS_REMISORIOS("//TODO debe poner la descripción acorde al tipo de doc"),
    OTRO("//TODO debe poner la descripción acorde al tipo de doc"),
    RESOLUCION_CONSERVACION("//TODO debe poner la descripción acorde al tipo de doc"),
    SOLICITUD_DE_DOCUMENTOS_CATASTRALES("//TODO debe poner la descripción acorde al tipo de doc"),
    SOLICITUD_ESCRITA_CON_SOPORTES("//TODO debe poner la descripción acorde al tipo de doc"),
    SOLICITUD_ESCRITA_POR_CORREO_CERTIFICADO(
        "//TODO debe poner la descripción acorde al tipo de doc"),
    SOLICITUD_ESCRITA_VERBAL_O_POR_CORREO("//TODO debe poner la descripción acorde al tipo de doc");

    private String descripcion;

    private EDescripcionTipoDocumento(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
