package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the PROFESIONAL_CONTRATO_PAGO database table.
 */
/*
 * @modified christian.rodriguez agregado método transient para saber si el pago es causado o no
 * @modified christian.rodriguez modificado el constructor para que inicialize los valores
 * monetarios en 0
 */
@Entity
@Table(name = "PROFESIONAL_CONTRATO_PAGO")
public class ProfesionalContratoPago implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long consecutivoPago;
    private String estado;
    private Date fechaInterventoria;
    private Date fechaLog;
    private Date fechaPago;
    private String numeroOrdenPago;
    private String numeroReciboParafiscales;
    private String usuarioLog;
    private Double valor;
    private Double valorPagado;
    private Double valorPagarPension;
    private Double valorPagarSalud;

    private List<ContratoPagoAvaluo> contratoPagoAvaluos;
    private ProfesionalAvaluosContrato profesionalAvaluosContrato;

    public ProfesionalContratoPago() {
        this.valor = 0.0;
        this.valorPagado = 0.0;
        this.valorPagarPension = 0.0;
        this.valorPagarSalud = 0.0;
    }

    @Id
    @SequenceGenerator(name = "PROFESIONAL_CONTRATO_PAGO_ID_GENERATOR", sequenceName =
        "PROFESIONAL_AVALUOS_CON_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "PROFESIONAL_CONTRATO_PAGO_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CONSECUTIVO_PAGO", nullable = false, precision = 2)
    public Long getConsecutivoPago() {
        return this.consecutivoPago;
    }

    public void setConsecutivoPago(Long consecutivoPago) {
        this.consecutivoPago = consecutivoPago;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INTERVENTORIA", nullable = false)
    public Date getFechaInterventoria() {
        return this.fechaInterventoria;
    }

    public void setFechaInterventoria(Date fechaInterventoria) {
        this.fechaInterventoria = fechaInterventoria;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_PAGO")
    public Date getFechaPago() {
        return this.fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    @Column(name = "NUMERO_ORDEN_PAGO", length = 20)
    public String getNumeroOrdenPago() {
        return this.numeroOrdenPago;
    }

    public void setNumeroOrdenPago(String numeroOrdenPago) {
        this.numeroOrdenPago = numeroOrdenPago;
    }

    @Column(name = "NUMERO_RECIBO_PARAFISCALES", length = 20)
    public String getNumeroReciboParafiscales() {
        return this.numeroReciboParafiscales;
    }

    public void setNumeroReciboParafiscales(String numeroReciboParafiscales) {
        this.numeroReciboParafiscales = numeroReciboParafiscales;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(nullable = false, precision = 12, scale = 2)
    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Column(name = "VALOR_PAGADO", nullable = false, precision = 12, scale = 2)
    public Double getValorPagado() {
        return this.valorPagado;
    }

    public void setValorPagado(Double valorPagado) {
        this.valorPagado = valorPagado;
    }

    @Column(name = "VALOR_PAGAR_PENSION", nullable = false, precision = 12, scale = 2)
    public Double getValorPagarPension() {
        return this.valorPagarPension;
    }

    public void setValorPagarPension(Double valorPagarPension) {
        this.valorPagarPension = valorPagarPension;
    }

    @Column(name = "VALOR_PAGAR_SALUD", nullable = false, precision = 12, scale = 2)
    public Double getValorPagarSalud() {
        return this.valorPagarSalud;
    }

    public void setValorPagarSalud(Double valorPagarSalud) {
        this.valorPagarSalud = valorPagarSalud;
    }

    // bi-directional many-to-one association to ContratoPagoAvaluo
    @OneToMany(mappedBy = "profesionalContratoPago")
    public List<ContratoPagoAvaluo> getContratoPagoAvaluos() {
        return this.contratoPagoAvaluos;
    }

    public void setContratoPagoAvaluos(
        List<ContratoPagoAvaluo> contratoPagoAvaluos) {
        this.contratoPagoAvaluos = contratoPagoAvaluos;
    }

    // bi-directional many-to-one association to ProfesionalAvaluosContrato
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESIONAL_AVALUOS_CONTRAT_ID", nullable = false)
    public ProfesionalAvaluosContrato getProfesionalAvaluosContrato() {
        return this.profesionalAvaluosContrato;
    }

    public void setProfesionalAvaluosContrato(ProfesionalAvaluosContrato profesionalAvaluosContrato) {
        this.profesionalAvaluosContrato = profesionalAvaluosContrato;
    }

    /**
     * Retorna si el pago es causado o no (si tiene numero de orden de pago es causado)
     *
     * @author christian.rodriguez
     * @return true si es un pago causado (con numero de orden de pago), false en otro caso
     */
    @Transient
    public boolean isCausado() {
        if (this.numeroOrdenPago != null && !this.numeroOrdenPago.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

}
