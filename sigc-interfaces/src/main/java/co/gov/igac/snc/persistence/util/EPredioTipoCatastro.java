package co.gov.igac.snc.persistence.util;

//TODO::fabio.navarrete::01-08-2011::DOCUMENTAR!!!!::pedro.garcia
public enum EPredioTipoCatastro {
    LEY_14("L"),
    AUTOAVALUO("A"),
    VIGENCIAS_VIEJAS("V");

    private String codigo;

    private EPredioTipoCatastro(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
