package co.gov.igac.snc.persistence.util;

import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioCierreVigencia;

/**
 * Enumeración con los posibles estados que puede tomar un {@link MunicipioCierreVigencia} al
 * ejecutar alguno de los procedimientos de cierre anual sobre éste.
 *
 * @author david.cifuentes
 *
 */
public enum EEstadoCierreMunicipio {

    CERRADO("CERRADO"),
    CONFIRMADO("CONFIRMADO"),
    ESTADISTICAS_PRECIERRE("ESTADISTICAS PRECIERRE"),
    ESTADISTICAS_POSCIERRE("ESTADISTICAS POSCIERRE"),
    SIN_EJECUTAR("SIN EJECUTAR"),
    VALIDADO("VALIDADO");

    private String estado;

    private EEstadoCierreMunicipio(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
