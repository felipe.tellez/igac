package co.gov.igac.snc.persistence.entity.actualizacion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "AJUSTE_MANZANA_AREA")
public class AjusteManzanaArea {

    private Long id;

    private ActualizacionOrdenAjuste actualizacionOrdenAjuste;

    private String identificador;

    private String levantamiento;

    private String usuarioLog;

    private Date fechaLog;

    @Id
    @SequenceGenerator(name = "AJUSTE_MANZ_AREA_ID_GENERATOR", sequenceName =
        "AJUSTE_MANZANA_AREA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AJUSTE_MANZ_AREA_ID_GENERATOR")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "IDENTIFICADOR")
    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    @Column(name = "LEVANTAMIENTO")
    public String getLevantamiento() {
        return levantamiento;
    }

    public void setLevantamiento(String levantamiento) {
        this.levantamiento = levantamiento;
    }

    //bi-directional many-to-one association to ActualizacionOrdenAjuste
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDEN_AJUSTE_ID")
    public ActualizacionOrdenAjuste getActualizacionOrdenAjuste() {
        return this.actualizacionOrdenAjuste;
    }

    public void setActualizacionOrdenAjuste(ActualizacionOrdenAjuste actualizacionOrdenAjuste) {
        this.actualizacionOrdenAjuste = actualizacionOrdenAjuste;
    }

}
