package co.gov.igac.snc.persistence.entity.conservacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteRadicacionEspecial;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.persistence.util.IInscripcionCatastral;
import co.gov.igac.snc.persistence.util.IProyeccionObject;
import co.gov.igac.snc.util.ETipoAvaluo;
import co.gov.igac.snc.util.Utilidades;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/*
 * MODIFICACIONES PROPIAS A LA CLASE PPredio: para consultar el valor de la propiedad
 * cancelaInscribe @author pedro.garcia -> Se modificó la variable de departamento para convertirla
 * en un objeto de tipo departamento. -> Se modificó la variable de municipio para convertirla en un
 * objeto de tipo municipio. @author juan.agudelo 27-05-11 se cambio PPersonaPredios de Set a List
 * @author javier.aponte se agrego una lista de tipo p_predio_servidumbre @author fabio.navarrete ->
 * Se agregó serialVersionUID -> Se modificó el tipo de dato de PPredioZonas de set a list -> Se
 * modificó el tipo de dato de PUnidadConstruccions de set a list -> Se agregó el método Transient
 * getTotalUnidades -> Se agregó el método y atributo transient -> Se modificó el tipo de dato de
 * PPredioAvaluoCatastrals de set a list -> se agrega propiedad tipoValor transient para almacenar
 * el valor del dominio Tipo -> se agrega propiedad estadoValor transient para almacenar el valor
 * del dominio estado -> se agrega propiedad destinoValor transient para almacenar el valor del
 * dominio destino -> Se agregó el método transient getUnidadesConstruccionNoConvencional -> Se
 * agregó el método transient getUnidadesConstruccionConvencional -> Se agregó el método transient
 * getAreaTotalConsNoConvencional -> Se agregó el método transient getAreaTotalConsConvencional ->
 * Se agregó el método transient getAreaTotalConstrucción -> Se agregó el método transient
 * getAreaTotalTerreno -> Se agregó el método transient getTotalUnidadesConstruccion -> Se agregó el
 * método transient getDerechosPropiedad -> Se agrega la propiedad selected, su getter y setter
 * transient -> Se sobrecarga el método equals. -> Se cambio el generador de la llave primaria por
 * el generador para proyecciones. -> Se agrega método generarComponentesAPartirDeNumeroPredial.
 *
 * @modified david.cifuentes 31-10-11 Se agregó el atributo Date fechaRegistro, y tambien sus
 * getters y setters.
 *
 *
 * @modified by fredy.wilches Todas las colecciones fueron modificadas a List Se adicionó método
 * transient que retorna el número de propietarios
 *
 * @modified juan.agudelo 06-10-11 Se agregaron los métodos transient para ley14 y numero predial
 * formateado juan.agudelo 19-10-11 Se agregaron los métodos transient
 * avaluoPUnidadesConstruccionConvencional y avaluoPUnidadesConstruccionNoConvencional juan.agudelo
 * 26-10-11 Se agrego el método transient avaluoPUnidadesConstruccion.
 *
 * @modified david.cifuentes 04-11-11 Se agrego el método transient isPredioRural que se encontraba
 * en Predio. david.cifuentes 20-12-11 Modificación del método getFormattedNumeroPredial, para que
 * muestre el barrio separado de la comuna.
 *
 * @modified pedro.garcia 24-04-2012 cambio del atributo circuloRegistral para que sea del tipo
 * CirculoRegistral (no String)
 *
 * @modified pedro.garcia 21-06-2012 - adición de atributo correspondiente a "terreno" (se creó el
 * getter sin el atributo) - adición de método que retorna el número de piso que está en el número
 * predial
 *
 * @modified david.cifuentes 22-08-12 Se agrego el método transient isEsPredioFichaMatriz.
 * david.cifuentes 22-08-12 Se agrego el método atributo coeficienteCopropiedadFM con sus
 * respectivos métodos transiente getCoeficientePFichaMatrizPredio y setCoeficienteCopropiedadFM.
 * @modified pedro.garcia 08-05-2013 - adición de namedquery que retorna el ppredio por el número
 * predial
 *
 * @modified felipe.cadena :: se agrega el campo originalTramite :: 08-05-2015
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "findPPredioByNumeroPredial",
        query =
        "from PPredio p join fetch p.departamento join fetch p.municipio where p.numeroPredial = :numeroPredial")})
@Table(name = "P_PREDIO", schema = "SNC_CONSERVACION")
public class PPredio implements java.io.Serializable, IProyeccionObject, IInscripcionCatastral {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -2839349185829646384L;

    private Long id;
    private Tramite tramite;
    private Long documentoId;
    private Departamento departamento;
    private Municipio municipio;
    private String tipoAvaluo;
    private String sectorCodigo;
    private String barrioCodigo;
    private String manzanaCodigo;
    private String predio;
    private String condicionPropiedad;
    private String edificio;
    private String piso;
    private String unidad;
    private String numeroPredial;
    private String numeroPredialAnterior;
    private String nip;
    private BigDecimal consecutivoCatastral;
    private BigDecimal consecutivoCatastralAnterior;
    private String zonaUnidadOrganica;
    private String localidadCodigo;
    private String corregimientoCodigo;
    private String nombre;
    private String destino;
    private String tipo;
    private Double areaTerreno;
    private CirculoRegistral circuloRegistral;
    private String numeroRegistro;
    private String numeroRegistroAnterior;
    private String tipoCatastro;
    private String este;
    private String norte;
    private String estado;
    private String estrato;
    private String chip;
    private String cancelaInscribe;
    private String numeroUltimaResolucion;
    private Short anioUltimaResolucion;
    private String usuarioLog;
    private Date fechaLog;
    private Double areaConstruccion;
    private Date fechaInscripcionCatastral;
    private Date fechaRegistro;
    private List<PFichaMatriz> PFichaMatrizs = new ArrayList<PFichaMatriz>();
    private List<PPredioZona> PPredioZonas = new ArrayList<PPredioZona>();
    private List<PPredioDireccion> PPredioDireccions = new ArrayList<PPredioDireccion>();
    private List<PPredioServidumbre> PPredioServidumbre = new ArrayList<PPredioServidumbre>();
    private List<PFoto> PFotos = new ArrayList<PFoto>();
    private List<PPersonaPredio> PPersonaPredios = new ArrayList<PPersonaPredio>();
    private List<PUnidadConstruccion> PUnidadConstruccions = new ArrayList<PUnidadConstruccion>();
    private List<PPredioAvaluoCatastral> PPredioAvaluoCatastrals =
        new ArrayList<PPredioAvaluoCatastral>();
    private List<PUnidadConstruccion> PUnidadesConstruccionConvencionalNuevas =
        new ArrayList<PUnidadConstruccion>();
    private List<PUnidadConstruccion> PUnidadesConstruccionNoConvencionalNuevas =
        new ArrayList<PUnidadConstruccion>();

    private List<PReferenciaCartografica> PReferenciaCartograficas =
        new ArrayList<PReferenciaCartografica>();

    private String cancelaInscribeValor;
    private String tipoValor;
    private String destinoValor;
    private String estadoValor;

    private boolean selected;

    private boolean ultimoDesenglobe;

    private Double coeficienteCopropiedadFM;

    private String terreno;

    private Double avaluoCatastral;

    /**
     * Transient que almacena el numero predial del predio en firme.
     */
    private String numeroPredialOriginal;

    /**
     * Marca los predios iniciales de un tramite, solo aplica para tramites de condiciones
     * especiales {@link ETramiteRadicacionEspecial}
     */
    private String originalTramite;

    /**
     * Determina si ya se a madificado informacion en la proyeccion del predio para tramites de
     * desenglobes por etapas.
     *
     * @author felipe.cadena
     */
    private int estadoEdicionEtapas;

    // Constructors
    /** default constructor */
    public PPredio() {
    }

    /** minimal constructor */
    public PPredio(Long id, Departamento departamento, Municipio municipio,
        String tipoAvaluo, String sectorCodigo, String barrioCodigo,
        String manzanaCodigo, String predio, String condicionPropiedad,
        String edificio, String piso, String unidad, String numeroPredial,
        String destino, String tipo, Double areaTerreno,
        String tipoCatastro, String estado, String cancelaInscribe,
        String usuarioLog, Date fechaLog, Double areaConstruccion) {
        this.id = id;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoAvaluo = tipoAvaluo;
        this.sectorCodigo = sectorCodigo;
        this.barrioCodigo = barrioCodigo;
        this.manzanaCodigo = manzanaCodigo;
        this.predio = predio;
        this.condicionPropiedad = condicionPropiedad;
        this.edificio = edificio;
        this.piso = piso;
        this.unidad = unidad;
        this.numeroPredial = numeroPredial;
        this.destino = destino;
        this.tipo = tipo;
        this.areaTerreno = areaTerreno;
        this.tipoCatastro = tipoCatastro;
        this.estado = estado;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.areaConstruccion = areaConstruccion;
    }

    /** full constructor */
    public PPredio(Long id, Tramite tramite, Long documentoId,
        Departamento departamento, Municipio municipio, String tipoAvaluo,
        String sectorCodigo, String barrioCodigo, String manzanaCodigo,
        String predio, String condicionPropiedad, String edificio,
        String piso, String unidad, String numeroPredial,
        String numeroPredialAnterior, String nip,
        BigDecimal consecutivoCatastral,
        BigDecimal consecutivoCatastralAnterior, String zonaUnidadOrganica,
        String localidadCodigo, String corregimientoCodigo, String nombre,
        String destino, String tipo, Double areaTerreno,
        CirculoRegistral circuloRegistral, String numeroRegistro,
        String numeroRegistroAnterior, String tipoCatastro, String este,
        String norte, String estado, String estrato, String chip,
        String cancelaInscribe, String numeroUltimaResolucion,
        Short anioUltimaResolucion, String usuarioLog, Date fechaLog,
        List<PFichaMatriz> PFichaMatrizs, List<PPredioZona> PPredioZonas,
        List<PPredioDireccion> PPredioDireccions,
        List<PPredioServidumbre> PPredioServidumbre, List<PFoto> PFotos,
        List<PPersonaPredio> PPersonaPredios,
        List<PReferenciaCartografica> PReferenciaCartograficas,
        List<PUnidadConstruccion> PUnidadConstruccions,
        Date fechaInscripcionCatastral, Date fechaRegistro,
        List<PPredioAvaluoCatastral> PPredioAvaluoCatastrals,
        Double areaConstruccion) {
        this.id = id;
        this.tramite = tramite;
        this.documentoId = documentoId;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoAvaluo = tipoAvaluo;
        this.sectorCodigo = sectorCodigo;
        this.barrioCodigo = barrioCodigo;
        this.manzanaCodigo = manzanaCodigo;
        this.predio = predio;
        this.condicionPropiedad = condicionPropiedad;
        this.edificio = edificio;
        this.piso = piso;
        this.unidad = unidad;
        this.numeroPredial = numeroPredial;
        this.numeroPredialAnterior = numeroPredialAnterior;
        this.nip = nip;
        this.consecutivoCatastral = consecutivoCatastral;
        this.consecutivoCatastralAnterior = consecutivoCatastralAnterior;
        this.zonaUnidadOrganica = zonaUnidadOrganica;
        this.localidadCodigo = localidadCodigo;
        this.corregimientoCodigo = corregimientoCodigo;
        this.nombre = nombre;
        this.destino = destino;
        this.tipo = tipo;
        this.areaTerreno = areaTerreno;
        this.circuloRegistral = circuloRegistral;
        this.numeroRegistro = numeroRegistro;
        this.numeroRegistroAnterior = numeroRegistroAnterior;
        this.tipoCatastro = tipoCatastro;
        this.este = este;
        this.norte = norte;
        this.estado = estado;
        this.estrato = estrato;
        this.chip = chip;
        this.cancelaInscribe = cancelaInscribe;
        this.numeroUltimaResolucion = numeroUltimaResolucion;
        this.anioUltimaResolucion = anioUltimaResolucion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.PFichaMatrizs = PFichaMatrizs;
        this.PPredioZonas = PPredioZonas;
        this.PPredioDireccions = PPredioDireccions;
        this.PPredioServidumbre = PPredioServidumbre;
        this.PFotos = PFotos;
        this.PPersonaPredios = PPersonaPredios;
        this.PUnidadConstruccions = PUnidadConstruccions;
        this.PReferenciaCartograficas = PReferenciaCartograficas;
        this.PPredioAvaluoCatastrals = PPredioAvaluoCatastrals;
        this.areaConstruccion = areaConstruccion;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
        this.fechaRegistro = fechaRegistro;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PREDIO_ID_SEQ")
    @GenericGenerator(name = "PREDIO_ID_SEQ", strategy =
        "co.gov.igac.snc.persistence.util.CustomProyeccionIdGenerator", parameters = {
            @Parameter(name = "sequence", value = "PREDIO_ID_SEQ"),
            @Parameter(name = "allocationSize", value = "1")})
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "DOCUMENTO_ID", precision = 10, scale = 0)
    public Long getDocumentoId() {
        return this.documentoId;
    }

    public void setDocumentoId(Long documentoId) {
        this.documentoId = documentoId;
    }

    // D: cambiados para reflejar el cambio de tipo de dato en estas columnas
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "TIPO_AVALUO", nullable = false, length = 2)
    public String getTipoAvaluo() {
        return this.tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    @Column(name = "SECTOR_CODIGO", nullable = false, length = 2)
    public String getSectorCodigo() {
        return this.sectorCodigo;
    }

    public void setSectorCodigo(String sectorCodigo) {
        this.sectorCodigo = sectorCodigo;
    }

    @Column(name = "BARRIO_CODIGO", nullable = false, length = 4)
    public String getBarrioCodigo() {
        return this.barrioCodigo;
    }

    public void setBarrioCodigo(String barrioCodigo) {
        this.barrioCodigo = barrioCodigo;
    }

    @Column(name = "MANZANA_CODIGO", nullable = false, length = 4)
    public String getManzanaCodigo() {
        return this.manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    @Column(name = "PREDIO", nullable = false, length = 4)
    public String getPredio() {
        return this.predio;
    }

    public void setPredio(String predio) {
        this.predio = predio;
    }

    @Column(name = "CONDICION_PROPIEDAD", nullable = false, length = 1)
    public String getCondicionPropiedad() {
        return this.condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    @Column(name = "EDIFICIO", nullable = false, length = 2)
    public String getEdificio() {
        return this.edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    @Column(name = "PISO", nullable = false, length = 2)
    public String getPiso() {
        return this.piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    @Column(name = "UNIDAD", nullable = false, length = 4)
    public String getUnidad() {
        return this.unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    @Column(name = "NUMERO_PREDIAL", nullable = false, length = 30)
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_PREDIAL_ANTERIOR", length = 20)
    public String getNumeroPredialAnterior() {
        return this.numeroPredialAnterior;
    }

    public void setNumeroPredialAnterior(String numeroPredialAnterior) {
        this.numeroPredialAnterior = numeroPredialAnterior;
    }

    @Column(name = "NIP", length = 20)
    public String getNip() {
        return this.nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    @Column(name = "CONSECUTIVO_CATASTRAL", precision = 20, scale = 0)
    public BigDecimal getConsecutivoCatastral() {
        return this.consecutivoCatastral;
    }

    public void setConsecutivoCatastral(BigDecimal consecutivoCatastral) {
        this.consecutivoCatastral = consecutivoCatastral;
    }

    @Column(name = "CONSECUTIVO_CATASTRAL_ANTERIOR", precision = 20, scale = 0)
    public BigDecimal getConsecutivoCatastralAnterior() {
        return this.consecutivoCatastralAnterior;
    }

    public void setConsecutivoCatastralAnterior(
        BigDecimal consecutivoCatastralAnterior) {
        this.consecutivoCatastralAnterior = consecutivoCatastralAnterior;
    }

    @Column(name = "ZONA_UNIDAD_ORGANICA", length = 30)
    public String getZonaUnidadOrganica() {
        return this.zonaUnidadOrganica;
    }

    public void setZonaUnidadOrganica(String zonaUnidadOrganica) {
        this.zonaUnidadOrganica = zonaUnidadOrganica;
    }

    @Column(name = "LOCALIDAD_CODIGO", length = 7)
    public String getLocalidadCodigo() {
        return this.localidadCodigo;
    }

    public void setLocalidadCodigo(String localidadCodigo) {
        this.localidadCodigo = localidadCodigo;
    }

    @Column(name = "CORREGIMIENTO_CODIGO", length = 7)
    public String getCorregimientoCodigo() {
        return this.corregimientoCodigo;
    }

    public void setCorregimientoCodigo(String corregimientoCodigo) {
        this.corregimientoCodigo = corregimientoCodigo;
    }

    @Column(name = "NOMBRE", length = 250)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "DESTINO", nullable = false, length = 30)
    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "AREA_TERRENO", nullable = false, precision = 18, scale = 2)
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

//------------------
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CIRCULO_REGISTRAL", nullable = true)
    public CirculoRegistral getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(CirculoRegistral circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }
//------------------

    @Column(name = "NUMERO_REGISTRO", length = 20)
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "NUMERO_REGISTRO_ANTERIOR", length = 20)
    public String getNumeroRegistroAnterior() {
        return this.numeroRegistroAnterior;
    }

    public void setNumeroRegistroAnterior(String numeroRegistroAnterior) {
        this.numeroRegistroAnterior = numeroRegistroAnterior;
    }

    @Column(name = "TIPO_CATASTRO", nullable = false, length = 2)
    public String getTipoCatastro() {
        return this.tipoCatastro;
    }

    public void setTipoCatastro(String tipoCatastro) {
        this.tipoCatastro = tipoCatastro;
    }

    @Column(name = "ESTE", length = 20)
    public String getEste() {
        return this.este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    @Column(name = "NORTE", length = 20)
    public String getNorte() {
        return this.norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    @Column(name = "ESTADO", nullable = false, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "ESTRATO", length = 30)
    public String getEstrato() {
        return this.estrato;
    }

    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    @Column(name = "CHIP", length = 14)
    public String getChip() {
        return this.chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "NUMERO_ULTIMA_RESOLUCION", length = 30)
    public String getNumeroUltimaResolucion() {
        return this.numeroUltimaResolucion;
    }

    public void setNumeroUltimaResolucion(String numeroUltimaResolucion) {
        this.numeroUltimaResolucion = numeroUltimaResolucion;
    }

    @Column(name = "ANIO_ULTIMA_RESOLUCION", precision = 4, scale = 0)
    public Short getAnioUltimaResolucion() {
        return this.anioUltimaResolucion;
    }

    public void setAnioUltimaResolucion(Short anioUltimaResolucion) {
        this.anioUltimaResolucion = anioUltimaResolucion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPredio")
    public List<PFichaMatriz> getPFichaMatrizs() {
        return this.PFichaMatrizs;
    }

    public void setPFichaMatrizs(List<PFichaMatriz> PFichaMatrizs) {
        this.PFichaMatrizs = PFichaMatrizs;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPredio")
    public List<PPredioZona> getPPredioZonas() {
        return this.PPredioZonas;
    }

    public void setPPredioZonas(List<PPredioZona> PPredioZonas) {
        this.PPredioZonas = PPredioZonas;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPredio")
    public List<PPredioDireccion> getPPredioDireccions() {
        return this.PPredioDireccions;
    }

    public void setPPredioDireccions(List<PPredioDireccion> PPredioDireccions) {
        this.PPredioDireccions = PPredioDireccions;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPredio")
    public List<PPredioServidumbre> getPPredioServidumbre() {
        return this.PPredioServidumbre;
    }

    public void setPPredioServidumbre(
        List<PPredioServidumbre> PPredioServidumbre) {
        this.PPredioServidumbre = PPredioServidumbre;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPredio")
    public List<PFoto> getPFotos() {
        return this.PFotos;
    }

    public void setPFotos(List<PFoto> PFotos) {
        this.PFotos = PFotos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPredio")
    public List<PPersonaPredio> getPPersonaPredios() {
        return this.PPersonaPredios;
    }

    public void setPPersonaPredios(List<PPersonaPredio> PPersonaPredios) {
        this.PPersonaPredios = PPersonaPredios;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPredio")
    public List<PUnidadConstruccion> getPUnidadConstruccions() {
        return this.PUnidadConstruccions;
    }

    public void setPUnidadConstruccions(
        List<PUnidadConstruccion> PUnidadConstruccions) {
        this.PUnidadConstruccions = PUnidadConstruccions;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPredio")
    public List<PPredioAvaluoCatastral> getPPredioAvaluoCatastrals() {
        return this.PPredioAvaluoCatastrals;
    }

    public void setPPredioAvaluoCatastrals(
        List<PPredioAvaluoCatastral> PPredioAvaluoCatastrals) {
        this.PPredioAvaluoCatastrals = PPredioAvaluoCatastrals;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PPredio")
    public List<PReferenciaCartografica> getPReferenciaCartograficas() {
        return this.PReferenciaCartograficas;
    }

    public void setPReferenciaCartograficas(
        List<PReferenciaCartografica> PReferenciaCartograficas) {
        this.PReferenciaCartograficas = PReferenciaCartograficas;
    }

    @Column(name = "AVALUO_CATASTRAL")
    public Double getAvaluoCatastral() {
        return this.avaluoCatastral;
    }

    public void setAvaluoCatastral(Double avaluoCatastral) {
        this.avaluoCatastral = avaluoCatastral;
    }

    @Transient
    public Integer getNumeroPropietarios() {
        if (this.PPersonaPredios != null) {
            return this.PPersonaPredios.size();
        } else {
            return 0;
        }
    }

    /**
     * @author fabio.navarrete Método que determina el número total de unidades de construcción del
     * PPredio
     * @return
     */
    @Transient
    public int getTotalUnidadesConstruccion() {
        if (this.PUnidadConstruccions != null) {
            return this.PUnidadConstruccions.size();
        }
        return 0;
    }

    @Column(name = "AREA_CONSTRUCCION", nullable = false, precision = 18)
    public Double getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete Método que arma el número predial con el formato usado en los caos de
     * uso de consulta para la interfaz gráfica del sistema. Retorna el dato sin códigos de
     * departamento y municipio.
     * @return Número predial con el formato especificado.
     */
    @Transient
    public String getFormattedNumeroPredial() {
        String formattedNumPredial = new String();
        /*
         * //Departamento formattedNumPredial = numeroPredial.substring(0, 2).concat("-");
         * //Municipio formattedNumPredial = formattedNumPredial.concat(numeroPredial.substring(2,
         * 5)) .concat("-");
         */
        // Tipo Avalúo
        formattedNumPredial = formattedNumPredial.concat(
            numeroPredial.substring(5, 7)).concat("-");
        // Sector
        formattedNumPredial = formattedNumPredial.concat(
            numeroPredial.substring(7, 9)).concat("-");
        // Comuna
        formattedNumPredial = formattedNumPredial.concat(
            numeroPredial.substring(9, 11)).concat("-");
        // Barrio
        formattedNumPredial = formattedNumPredial.concat(
            numeroPredial.substring(11, 13)).concat("-");
        // Manzana
        formattedNumPredial = formattedNumPredial.concat(
            numeroPredial.substring(13, 17)).concat("-");
        // Predio
        formattedNumPredial = formattedNumPredial.concat(
            numeroPredial.substring(17, 21)).concat("-");
        // Condición Propiedad
        formattedNumPredial = formattedNumPredial.concat(
            numeroPredial.substring(21, 22)).concat("-");
        // Edificio
        formattedNumPredial = formattedNumPredial.concat(
            numeroPredial.substring(22, 24)).concat("-");
        // Piso
        formattedNumPredial = formattedNumPredial.concat(
            numeroPredial.substring(24, 26)).concat("-");
        // Unidad
        formattedNumPredial = formattedNumPredial.concat(numeroPredial
            .substring(26, 30));
        return formattedNumPredial;
    }

    public void setTipoValor(String tipoValor) {
        this.tipoValor = tipoValor;
    }

    @Transient
    public String getTipoValor() {
        return tipoValor;
    }

    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

    public void setEstadoValor(String estadoValor) {
        this.estadoValor = estadoValor;
    }

    @Transient
    public String getEstadoValor() {
        return estadoValor;
    }

    public void setDestinoValor(String destinoValor) {
        this.destinoValor = destinoValor;
    }

    @Transient
    public String getDestinoValor() {
        return destinoValor;
    }

    /**
     * Método que retorna el área total del terreno del predio.
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getAreaTotalTerreno() {
        Double areaTotal = 0.0;
        for (PPredioZona predZona : this.getPPredioZonas()) {
            areaTotal += predZona.getArea();
        }
        return areaTotal.toString();
    }

    /**
     * Método que retorna el área total de construcción del predio a partir del subconjunto de
     * construcción convencional y no convencional
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getAreaTotalConstruccion() {
        Double areaTotal = new Double(this.getAreaTotalConsConvencional());
        areaTotal += new Double(this.getAreaTotalConsNoConvencional());
        return areaTotal.toString();
    }

    /**
     * Método que retorna el área de construcción convencional del predio convencional
     *
     * @return
     * @author fabio.navarrete
     * @modified leidy.gonzalez se agrega validacion para que PUnidadesConstruccionConvencional no
     * sea nulo al momento de recorrer la lista
     */
    /*
     * @modified by juanfelipe.garcia :: 10-06-2014 :: El área de construcción convencional no debe
     * tener en cuenta las unidades canceladas
     */
    @Transient
    public String getAreaTotalConsConvencional() {
        Double area = 0.0;
        if (this.getPUnidadesConstruccionConvencional() != null) {
            for (IModeloUnidadConstruccion unidad : this
                .getPUnidadesConstruccionConvencional()) {
                if (unidad.getCancelaInscribe() != null &&
                    !unidad.getCancelaInscribe().
                        equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {

                    area += unidad.getAreaConstruida();
                }
            }
        }
        return area.toString();
    }

    /**
     * Método que retorna el área de construcción NO convencional del predio
     *
     * @return
     * @author fabio.navarrete
     * @modified leidy.gonzalez se agrega validacion para que PUnidadesConstruccionNoConvencional no
     * sea nulo al momento de recorrer la lista
     */
    /*
     * @modified by juanfelipe.garcia :: 10-06-2014 :: El área de construcción no convencional no
     * debe tener en cuenta las unidades canceladas
     */
    @Transient
    public String getAreaTotalConsNoConvencional() {
        Double area = 0.0;
        if (this.getPUnidadesConstruccionNoConvencional() != null) {
            for (IModeloUnidadConstruccion unidad : this
                .getPUnidadesConstruccionNoConvencional()) {
                if (unidad.getCancelaInscribe() != null &&
                    !unidad.getCancelaInscribe().
                        equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {

                    area += unidad.getAreaConstruida();
                }
            }
        }
        return area.toString();
    }

    /**
     * Método que retorna las unidades de construcción convencionales
     *
     * @return
     * @author fabio.navarrete
     * @modified leidy.gonzalez se agrega validacion para que PUnidadConstruccions no sea nulo al
     * momento de recorrer la lista
     * @modified leidy.gonzalez :: Adición de validacion con el atributo anioCancelacion y sus para
     * que no se muestre en la vista de las consultas de detalle avaluo :: 22/05/2014
     */
    @Transient
    public List<PUnidadConstruccion> getPUnidadesConstruccionConvencional() {
        ArrayList<PUnidadConstruccion> unidadesConst = new ArrayList<PUnidadConstruccion>();
        if (this.getPUnidadConstruccions() != null && !this.getPUnidadConstruccions().isEmpty()) {
            for (PUnidadConstruccion uni : this.getPUnidadConstruccions()) {
                if (uni.getTipoConstruccion().trim()
                    .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                    uni.getAnioCancelacion() == null) {
                    unidadesConst.add(uni);
                }
            }
        }
        return unidadesConst;
    }

    /**
     * Método que retorna las unidades de construcción convencionales Vigentes
     *
     * @return
     * @author fredy.wilches
     * @modified leidy.gonzalez se agrega validacion para que PUnidadConstruccions no sea nulo al
     * momento de recorrer la lista
     * @modified leidy.gonzalez :: Adición de validacion con el atributo anioCancelacion y sus para
     * que no se muestre en la vista de las consultas de detalle avaluo :: 22/05/2014
     */
    @Transient
    public List<PUnidadConstruccion> getPUnidadesConstruccionConvencionalVigentes() {
        ArrayList<PUnidadConstruccion> unidadesConst = new ArrayList<PUnidadConstruccion>();
        if (this.getPUnidadConstruccions() != null && !this.getPUnidadConstruccions().isEmpty()) {
            for (PUnidadConstruccion uni : this.getPUnidadConstruccions()) {
                if (uni.getTipoConstruccion().trim().equals(EUnidadTipoConstruccion.CONVENCIONAL.
                    getCodigo()) &&
                    (uni.getCancelaInscribe() == null || uni.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                    uni.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) &&
                    uni.getAnioCancelacion() == null) {
                    unidadesConst.add(uni);
                }
            }
        }
        Collections.sort(unidadesConst);
        return unidadesConst;
    }

    /**
     * Método que retorna las unidades de construcción convencionales Nuevas
     *
     * @return
     * @author fredy.wilches
     * @modified leidy.gonzalez se agrega validacion para que PUnidadConstruccions no sea nulo al
     * momento de recorrer la lista
     */
    @Transient
    public List<PUnidadConstruccion> getPUnidadesConstruccionConvencionalNuevas() {
        ArrayList<PUnidadConstruccion> unidadesConst = new ArrayList<PUnidadConstruccion>();

        if (this.getPUnidadConstruccions() != null && !this.getPUnidadConstruccions().isEmpty()) {
            for (PUnidadConstruccion uni : this.getPUnidadConstruccions()) {
                if (this.tramite.isEnglobeVirtual() &&
                    uni.getTipoConstruccion().trim().equals(EUnidadTipoConstruccion.CONVENCIONAL.
                        getCodigo()) &&
                    uni.getAnioCancelacion() == null) {

                    unidadesConst.add(uni);

                } else if (uni.getTipoConstruccion().trim().equals(
                    EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                    (uni.getCancelaInscribe() == null || uni.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                    uni.getCancelaInscribe().
                        equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) &&
                    uni.getAnioCancelacion() == null) {
                    //felipe.cadena:: 09-09-2014 :: #9350 :: No se permiten construcciones con CANCELA_INSCRIBE en null para tramites de desenglobe
                    if (this.tramite != null) {
                        if (this.tramite.isSegundaDesenglobe() && uni.getCancelaInscribe() == null) {
                            continue;
                        }
                    }
                    unidadesConst.add(uni);
                }
            }
        }

        Collections.sort(unidadesConst);
        return unidadesConst;
    }

    /**
     * Método que adiciona las unidades de construcción convencionales Nuevas
     *
     * @return
     * @author leidy.gonzalez
     */
    public void setPUnidadesConstruccionConvencionalNuevas(
        List<PUnidadConstruccion> PUnidadesConstruccionConvencionalNuevas) {

        for (PUnidadConstruccion unidadesConstConv : PUnidadesConstruccionConvencionalNuevas) {

            if (this.tramite.isEnglobeVirtual() &&
                unidadesConstConv.getTipoConstruccion().trim().equals(
                    EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                unidadesConstConv.getAnioCancelacion() == null) {

                this.PUnidadesConstruccionConvencionalNuevas.add(unidadesConstConv);

            } else if (unidadesConstConv.getTipoConstruccion().trim().equals(
                EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                (unidadesConstConv.getCancelaInscribe() == null || unidadesConstConv.
                getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                unidadesConstConv.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.
                    getCodigo())) &&
                unidadesConstConv.getAnioCancelacion() == null) {
                //felipe.cadena:: 09-09-2014 :: #9350 :: No se permiten construcciones con CANCELA_INSCRIBE en null para tramites de desenglobe
                if (this.tramite != null) {
                    if (this.tramite.isSegundaDesenglobe() &&
                        unidadesConstConv.getCancelaInscribe() == null) {
                    } else {
                        this.PUnidadesConstruccionConvencionalNuevas.add(unidadesConstConv);
                    }
                }

            }
        }

        Collections.sort(this.PUnidadesConstruccionConvencionalNuevas);

        if (this.tramite.isEnglobeVirtual()) {
            this.getPUnidadConstruccions().addAll(this.PUnidadesConstruccionConvencionalNuevas);
        }

    }

    /**
     * Método que retorna las unidades de construcción no convencionales
     *
     * @return
     * @author fabio.navarrete
     * @modified leidy.gonzalez se agrega validacion para que PUnidadConstruccions no sea nulo al
     * momento de recorrer la lista
     * @modified leidy.gonzalez :: Adición de validacion con el atributo anioCancelacion y sus para
     * que no se muestre en la vista de las consultas de detalle avaluo :: 22/05/2014
     */
    @Transient
    public List<PUnidadConstruccion> getPUnidadesConstruccionNoConvencional() {
        ArrayList<PUnidadConstruccion> unidadesConst = new ArrayList<PUnidadConstruccion>();
        if (this.getPUnidadConstruccions() != null && !this.getPUnidadConstruccions().isEmpty()) {
            for (PUnidadConstruccion uni : this.getPUnidadConstruccions()) {
                if (uni.getTipoConstruccion().trim()
                    .equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()) &&
                    uni.getAnioCancelacion() == null) {
                    unidadesConst.add(uni);
                }
            }
        }
        return unidadesConst;
    }

    /**
     * Método que retorna las unidades de construcción no convencionales Vigentes
     *
     * @return
     * @author fredy.wilches
     * @modified leidy.gonzalez se agrega validacion para que PUnidadConstruccions no sea nulo al
     * momento de recorrer la lista
     * @modified leidy.gonzalez :: Adición de validacion con el atributo anioCancelacion y sus para
     * que no se muestre en la vista de las consultas de detalle avaluo :: 22/05/2014
     */
    @Transient
    public List<PUnidadConstruccion> getPUnidadesConstruccionNoConvencionalVigentes() {
        ArrayList<PUnidadConstruccion> unidadesConst = new ArrayList<PUnidadConstruccion>();
        if (this.getPUnidadConstruccions() != null && !this.getPUnidadConstruccions().isEmpty()) {
            for (PUnidadConstruccion uni : this.getPUnidadConstruccions()) {
                if (uni.getTipoConstruccion().trim().equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.
                    getCodigo()) &&
                    (uni.getCancelaInscribe() == null || uni.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                    uni.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) &&
                    uni.getAnioCancelacion() == null) {
                    unidadesConst.add(uni);
                }
            }
        }
        Collections.sort(unidadesConst);
        return unidadesConst;
    }

    /**
     * Método que retorna las unidades de construcción no convencionales Nuevas
     *
     * @return
     * @author fredy.wilches
     * @modified leidy.gonzalez se agrega validacion para que PUnidadConstruccions no sea nulo al
     * momento de recorrer la lista
     */
    @Transient
    public List<PUnidadConstruccion> getPUnidadesConstruccionNoConvencionalNuevas() {
        ArrayList<PUnidadConstruccion> unidadesConst = new ArrayList<PUnidadConstruccion>();

        if (this.getPUnidadConstruccions() != null && !this.getPUnidadConstruccions().isEmpty()) {
            for (PUnidadConstruccion uni : this.getPUnidadConstruccions()) {
                if (this.tramite.isEnglobeVirtual() &&
                    uni.getTipoConstruccion().trim().equals(
                        EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()) &&
                    uni.getAnioCancelacion() == null) {

                    unidadesConst.add(uni);

                } else if (uni.getTipoConstruccion().trim().equals(
                    EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()) &&
                    (uni.getCancelaInscribe() == null || uni.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                    uni.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) &&
                    uni.getAnioCancelacion() == null) {
                    //felipe.cadena:: 09-09-2014 :: #9350 :: No se permiten construcciones con CANCELA_INSCRIBE en null para tramites de desenglobe
                    if (this.tramite != null) {
                        if (this.tramite.isSegundaDesenglobe() && uni.getCancelaInscribe() == null) {
                            continue;
                        }
                    }
                    unidadesConst.add(uni);
                }
            }
        }

        Collections.sort(unidadesConst);
        return unidadesConst;
    }

    /**
     * Método que adiciona las unidades de construcción no convencionales Nuevas
     *
     * @return
     * @author leidy.gonzalez
     */
    public void setPUnidadesConstruccionNoConvencionalNuevas(
        List<PUnidadConstruccion> PUnidadesConstruccionNoConvencionalNuevas) {

        for (PUnidadConstruccion unidadesConstNoConv : PUnidadesConstruccionNoConvencionalNuevas) {

            if (this.tramite.isEnglobeVirtual() &&
                unidadesConstNoConv.getTipoConstruccion().trim().equals(
                    EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                unidadesConstNoConv.getAnioCancelacion() == null) {

                this.PUnidadesConstruccionNoConvencionalNuevas.add(unidadesConstNoConv);

            } else if (unidadesConstNoConv.getTipoConstruccion().trim().equals(
                EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()) &&
                (unidadesConstNoConv.getCancelaInscribe() == null || unidadesConstNoConv.
                getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                unidadesConstNoConv.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) &&
                unidadesConstNoConv.getAnioCancelacion() == null) {
                //felipe.cadena:: 09-09-2014 :: #9350 :: No se permiten construcciones con CANCELA_INSCRIBE en null para tramites de desenglobe
                if (this.tramite != null) {
                    if (this.tramite.isSegundaDesenglobe() && unidadesConstNoConv.
                        getCancelaInscribe() == null) {
                    } else {
                        this.PUnidadesConstruccionNoConvencionalNuevas.add(unidadesConstNoConv);
                    }
                }
            }

        }

        Collections.sort(this.PUnidadesConstruccionNoConvencionalNuevas);
        if (this.tramite.isEnglobeVirtual()) {
            this.getPUnidadConstruccions().addAll(this.PUnidadesConstruccionNoConvencionalNuevas);
        }
    }

    /**
     * Método que retorna TODOS los Derechos de propiedad del Predio.
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public List<PPersonaPredioPropiedad> getDerechosPropiedad() {
        List<PPersonaPredioPropiedad> derechosPropiedad = new ArrayList<PPersonaPredioPropiedad>();
        if (this.getPPersonaPredios() != null) {
            for (PPersonaPredio perPre : this.getPPersonaPredios()) {
                List<PPersonaPredioPropiedad> personaPredioPropiedades = perPre
                    .getPPersonaPredioPropiedads();
                for (PPersonaPredioPropiedad perPrePro : personaPredioPropiedades) {
                    derechosPropiedad.add(perPrePro);
                }
            }
        }
        return derechosPropiedad;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "FECHA_REGISTRO", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Column(name = "ORIGINAL_TRAMITE")
    public String getOriginalTramite() {
        return this.originalTramite;
    }

    public void setOriginalTramite(String originalTramite) {
        this.originalTramite = originalTramite;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo Método que arma el número predial
     * @return Número predial completo con el formato especificado.
     */
    /*
     * @modified pedro.garcia 15-08-2012. El Gabriel ni para hacer copy y paste. Faltaba separar
     * comuna y barrio
     */
    @Transient
    public String getFormattedNumeroPredialCompleto() {
        String formatedNumPredial = new String();

        if (this.numeroPredial != null && !this.numeroPredial.isEmpty()) {
            // Departamento
            formatedNumPredial = numeroPredial.substring(0, 2).concat("-");
            // Municipio
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(2, 5)).concat("-");
            // Tipo Avalúo
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(5, 7)).concat("-");
            // Sector
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(7, 9)).concat("-");
            // Comuna
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(9, 11)).concat("-");
            // Barrio
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(11, 13)).concat("-");
            // Manzana
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(13, 17)).concat("-");
            // Predio
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(17, 21)).concat("-");
            // Condición Propiedad
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(21, 22)).concat("-");
            // Edificio
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(22, 24)).concat("-");
            // Piso
            formatedNumPredial = formatedNumPredial.concat(
                numeroPredial.substring(24, 26)).concat("-");
            // Unidad
            formatedNumPredial = formatedNumPredial.concat(numeroPredial
                .substring(26, 30));
        }
        return formatedNumPredial;
    }

    public void setLey14(boolean ley14) {
        if (ley14) {
            this.tipoCatastro = EPredioTipoCatastro.LEY_14.getCodigo();
        }
    }

    @Transient
    public boolean getLey14() {
        return this.tipoCatastro.equals(EPredioTipoCatastro.LEY_14.getCodigo());
    }

    /**
     * Método que retorna el avaluo de las unidades de construcción convencionales
     *
     * @return
     * @author juan.agudelo
     * @modified leidy.gonzalez se agrega validacion para que PUnidadConstruccions no sea nulo al
     * momento de recorrer la lista
     */
    @Transient
    public Double getAvaluoPUnidadesConstruccionConvencional() {
        Double avaluoConstruccionConverncional = 0.0;
        if (this.getPUnidadConstruccions() != null && !this.getPUnidadConstruccions().isEmpty()) {
            for (IModeloUnidadConstruccion uni : this.getPUnidadConstruccions()) {
                if (uni.getTipoConstruccion().trim()
                    .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                    avaluoConstruccionConverncional += uni.getAvaluo();
                }
            }
        }
        return avaluoConstruccionConverncional;
    }

    /**
     * Método que retorna el avaluo de las unidades de construcción no convencionales
     *
     * @return
     * @author juan.agudelo
     * @modified leidy.gonzalez se agrega validacion para que PUnidadConstruccions no sea nulo al
     * momento de recorrer la lista
     */
    @Transient
    public Double getAvaluoPUnidadesConstruccionNoConvencional() {
        Double avaluoConstruccionConverncional = 0.0;
        if (this.getPUnidadConstruccions() != null && !this.getPUnidadConstruccions().isEmpty()) {
            for (IModeloUnidadConstruccion uni : this.getPUnidadConstruccions()) {
                if (uni.getTipoConstruccion()
                    .trim()
                    .equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
                    avaluoConstruccionConverncional += uni.getAvaluo();
                }
            }
        }
        return avaluoConstruccionConverncional;
    }

    /**
     * Método que retorna el avaluo total
     *
     * @return
     * @author juan.agudelo
     * @modified leidy.gonzalez se agrega validacion para que PUnidadConstruccions no sea nulo al
     * momento de recorrer la lista
     */
    @Transient
    public Double getAvaluoPUnidadesConstruccion() {
        Double avaluoConstrucciones = 0.0;
        if (this.getPUnidadConstruccions() != null && !this.getPUnidadConstruccions().isEmpty()) {
            for (IModeloUnidadConstruccion uni : this.getPUnidadConstruccions()) {
                avaluoConstrucciones += uni.getAvaluo();
            }
        }
        return avaluoConstrucciones;
    }

    /**
     * Determina si un predio es urbano o rural. Utiliza la propiedad de tipoAvaluo
     *
     * @return
     */
    @Transient
    public boolean isPredioRural() {
        if (this.tipoAvaluo != null) {
            return this.tipoAvaluo.equals(ETipoAvaluo.RURAL.getCodigo());
        }
        return false;
    }

    @Transient
    public boolean isPHCondominio() {
        if (this.numeroPredial != null && !this.numeroPredial.isEmpty()) {
            String condiPropiedad = this.numeroPredial.substring(21, 22);
            if (condiPropiedad
                .equals(EPredioCondicionPropiedad.CP_8
                    .getCodigo()) ||
                condiPropiedad
                    .equals(EPredioCondicionPropiedad.CP_9
                        .getCodigo())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que retorna la zona como una cadena de texto obtenida a partir del número predial y su
     * estructura predefinida OJO: la zona corresponde en la tabla al campo "tipo_avaluo"
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getZona() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(5, 7);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de una zona.
     *
     * @param zona
     * @author fabio.navarrete
     */
    /*
     * @modified pedro.garcia 10-07-2012 se contempla el caso en el que se envía el número de zona
     * como una cadena que representa el número predial hasta ese segmento (7 caracteres)
     */
    public void setZona(String zona) {

        StringBuilder sb = new StringBuilder(this.numeroPredial);
        String segmentoZona;

        if (zona.length() < 3) {
            while (zona.length() < 2) {
                zona = "0" + zona;
            }

            sb.replace(5, 7, zona);
            this.numeroPredial = sb.toString();
            this.tipoAvaluo = zona;
        } else if (zona.length() == 7) {
            segmentoZona = zona.substring(5, 7);
            sb.replace(5, 7, segmentoZona);
            this.numeroPredial = sb.toString();
            this.tipoAvaluo = segmentoZona;
        }
    }

    /**
     * Método que retorna el sector como una cadena de texto obtenida a partir del número predial y
     * su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getSector() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(7, 9);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de un sector.
     *
     * @param sector
     * @author fabio.navarrete
     *
     * @modified pedro.garcia 10-07-2012 se contempla el caso en el que se envía el número de sector
     * como una cadena que representa el número predial hasta ese segmento (9 caracteres)
     */
    public void setSector(String sector) {

        StringBuilder sb = new StringBuilder(this.numeroPredial);
        String segmentoSector;

        if (sector.length() < 3) {
            while (sector.length() < 2) {
                sector = "0" + sector;
            }

            sb.replace(7, 9, sector);
            this.numeroPredial = sb.toString();
            this.sectorCodigo = sector;
        } else if (sector.length() == 9) {
            segmentoSector = sector.substring(7, 9);
            sb.replace(7, 9, segmentoSector);
            this.numeroPredial = sb.toString();
            this.sectorCodigo = segmentoSector;
        }

    }

    /**
     * Método que retorna la comuna como una cadena de texto obtenida a partir del número predial y
     * su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getComuna() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(9, 11);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de una comuna.
     *
     * OJO: el código de barrio incluye el código de comuna: son los 2 primeros dígitos de éste
     *
     * @param comuna
     * @author fabio.navarrete
     *
     * @modified pedro.garcia 10-07-2012 + arreglo: asignaba el código de barrio y no de comuna + se
     * contempla el caso en el que se envía el número de comuna como una cadena que representa el
     * número predial hasta ese segmento (11 caracteres)
     */
    public void setComuna(String comuna) {

        StringBuilder sb = new StringBuilder(this.numeroPredial);
        String segmentoComuna;

        if (comuna.length() < 3) {
            while (comuna.length() < 2) {
                comuna = "0" + comuna;
            }

            sb.replace(9, 11, comuna);
            this.numeroPredial = sb.toString();
            this.barrioCodigo = sb.substring(9, 13);
        } else if (comuna.length() == 11) {
            segmentoComuna = comuna.substring(9, 11);
            sb.replace(9, 11, segmentoComuna);
            this.numeroPredial = sb.toString();
            this.barrioCodigo = sb.substring(9, 13);
        }

    }

    /**
     * Método que retorna el barrio como una cadena de texto obtenida a partir del número predial y
     * su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getBarrio() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(11, 13);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de un barrio.
     *
     * @param barrio
     * @author fabio.navarrete
     *
     * @modified pedro.garcia 10-07-2012 se contempla el caso en el que se envía el número de barrio
     * como una cadena que representa el número predial hasta ese segmento (13 caracteres)
     */
    public void setBarrio(String barrio) {

        StringBuilder sb = new StringBuilder(this.numeroPredial);
        String segmentoBarrio;

        if (barrio.length() < 3) {
            while (barrio.length() < 2) {
                barrio = "0" + barrio;
            }

            sb.replace(11, 13, barrio);
            this.numeroPredial = sb.toString();
            this.barrioCodigo = sb.substring(9, 13);
        } else if (barrio.length() == 13) {
            segmentoBarrio = barrio.substring(11, 13);
            sb.replace(11, 13, segmentoBarrio);
            this.numeroPredial = sb.toString();
            this.barrioCodigo = sb.substring(9, 13);
        }

    }

    /**
     * Método que retorna la manzana o vereda como una cadena de texto obtenida a partir del número
     * predial y su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getManzanaVereda() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(13, 17);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de una manzana o vereda.
     *
     * @param manzanaVereda
     * @author fabio.navarrete
     */
    /*
     * @modified pedro.garcia 10-07-2012 se contempla el caso en el que se envía el número de
     * manzana como una cadena que representa el número predial hasta ese segmento (17 caracteres)
     */
    public void setManzanaVereda(String manzanaVereda) {

        String segmentoManzana;
        StringBuilder sb = new StringBuilder(this.numeroPredial);

        if (manzanaVereda.length() < 5) {
            while (manzanaVereda.length() < 4) {
                manzanaVereda = "0" + manzanaVereda;
            }

            sb.replace(13, 17, manzanaVereda);
            this.numeroPredial = sb.toString();
            this.manzanaCodigo = manzanaVereda;
        } else if (manzanaVereda.length() == 17) {
            segmentoManzana = manzanaVereda.substring(13, 17);
            sb.replace(13, 17, segmentoManzana);
            this.numeroPredial = sb.toString();
            this.manzanaCodigo = segmentoManzana;
        }
    }

    /**
     * Método que retorna el terreno como una cadena de texto obtenida a partir del número predial y
     * su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    /*
     * @modified pedro.garcia 18-07-2012 adición de condición porque el número predial puede ser
     * vacío pero no null
     */
    @Transient
    public String getTerreno() {
        if (this.numeroPredial != null && !this.numeroPredial.isEmpty()) {
            return this.numeroPredial.substring(17, 21);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial del terreno.
     *
     * @param terreno
     * @author fabio.navarrete
     */
    public void setTerreno(String terreno) {

        if (null == terreno) {
            terreno = "";
        }

        if (terreno.length() < 5) {
            while (terreno.length() < 4) {
                terreno = "0" + terreno;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(17, 21, terreno);
            this.numeroPredial = sb.toString();

            //D: el campo 'predio' es el que representa el 'terreno'
            this.predio = terreno;
        }
    }

    /**
     * Método que retorna la condición de propiedad como una cadena de texto obtenida a partir del
     * número predial y su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getCondicionPropiedadNumeroPredial() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(21, 22);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de la condición de propiedad.
     *
     * @param condicionPropiedad
     * @author fabio.navarrete
     */
    public void setCondicionPropiedadNumeroPredial(String condicionPropiedad) {
        if (condicionPropiedad.length() < 2) {
            while (condicionPropiedad.length() < 1) {
                condicionPropiedad = "0" + condicionPropiedad;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(21, 22, condicionPropiedad);
            this.numeroPredial = sb.toString();
            this.condicionPropiedad = condicionPropiedad;
        }
    }

    /**
     * Método que retorna el número de edificio o torre como una cadena de texto obtenida a partir
     * del número predial y su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getNumeroEdificioTorre() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(22, 24);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial del número de edificio o torre.
     *
     * @param numeroEdificioTorre
     * @author fabio.navarrete
     */
    public void setNumeroEdificioTorre(String numeroEdificioTorre) {
        if (numeroEdificioTorre.length() < 3) {
            while (numeroEdificioTorre.length() < 2) {
                numeroEdificioTorre = "0" + numeroEdificioTorre;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(22, 24, numeroEdificioTorre);
            this.numeroPredial = sb.toString();
            this.edificio = numeroEdificioTorre;
        }
    }

    /**
     * Método que retorna el piso como una cadena de texto obtenida a partir del número predial y su
     * estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getPisoNumeroPredial() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(24, 26);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial del piso.
     *
     * @param pisoNumeroPredial
     * @author fabio.navarrete
     */
    public void setPisoNumeroPredial(String pisoNumeroPredial) {
        if (pisoNumeroPredial.length() < 3) {
            while (pisoNumeroPredial.length() < 2) {
                pisoNumeroPredial = "0" + pisoNumeroPredial;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(24, 26, pisoNumeroPredial);
            this.numeroPredial = sb.toString();
            this.piso = pisoNumeroPredial;
        }
    }

    /**
     * Método que retorna el número de unidad como una cadena de texto obtenida a partir del número
     * predial y su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    /*
     * @modified pedro.garcia 18-07-2012 adición de condición porque el númerompredial puede ser
     * vacío pero no null
     */
    @Transient
    public String getNumeroUnidad() {
        if (this.numeroPredial != null && !this.numeroPredial.isEmpty()) {
            return this.numeroPredial.substring(26, 30);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial del número de unidad.
     *
     * @param numeroUnidad
     * @author fabio.navarrete
     */
    /*
     * @modified pedro.garcia 13-07-2012 se contempla el caso en el que se envía el número de unidad
     * como una cadena que representa el número predial hasta ese segmento (30 caracteres)
     */
    public void setNumeroUnidad(String numeroUnidad) {

        StringBuilder sb = new StringBuilder(this.numeroPredial);
        String segmentoNumeroUnidad;

        if (numeroUnidad.length() < 5) {
            while (numeroUnidad.length() < 4) {
                numeroUnidad = "0" + numeroUnidad;
            }

            sb.replace(26, 30, numeroUnidad);
            this.numeroPredial = sb.toString();
            this.unidad = numeroUnidad;
        } else if (numeroUnidad.length() == 17) {
            segmentoNumeroUnidad = numeroUnidad.substring(26, 30);
            sb.replace(26, 30, segmentoNumeroUnidad);
            this.numeroPredial = sb.toString();
            this.unidad = segmentoNumeroUnidad;
        }

    }

    public void generarNumeroPredialAPartirDeComponentes() {
        StringBuilder sb = new StringBuilder();
        if (this.departamento != null) {
            sb.append(this.departamento.getCodigo());
        } else {
            sb.append("00");
        }

        if (this.municipio != null) {
            sb.append(this.municipio.getCodigo3Digitos());
        } else {
            sb.append("000");
        }

        if (this.tipoAvaluo != null) {
            while (this.tipoAvaluo.length() < 2) {
                this.tipoAvaluo = "0" + this.tipoAvaluo;
            }
        } else {
            this.tipoAvaluo = "00";
        }

        sb.append(this.tipoAvaluo);

        if (this.sectorCodigo != null) {
            while (this.sectorCodigo.length() < 2) {
                this.sectorCodigo = "0" + this.sectorCodigo;
            }
        } else {
            this.sectorCodigo = "00";
        }
        sb.append(this.sectorCodigo);

        if (this.barrioCodigo != null) {
            while (this.barrioCodigo.length() < 4) {
                this.barrioCodigo = "0" + this.barrioCodigo;
            }
        } else {
            this.barrioCodigo = "0000";
        }
        sb.append(this.barrioCodigo);

        if (this.manzanaCodigo != null) {
            while (this.manzanaCodigo.length() < 4) {
                this.manzanaCodigo = "0" + this.manzanaCodigo;
            }
        } else {
            this.manzanaCodigo = "0000";
        }
        sb.append(this.manzanaCodigo);

        if (this.predio != null) {
            while (this.predio.length() < 4) {
                this.predio = "0" + this.predio;
            }
        } else {
            this.predio = "0000";
        }
        sb.append(this.predio);

        if (this.condicionPropiedad == null) {
            this.condicionPropiedad = "0";
        }
        sb.append(this.condicionPropiedad);

        if (this.edificio != null) {
            while (this.edificio.length() < 2) {
                this.edificio = "0" + this.edificio;
            }
        } else {
            this.edificio = "00";
        }
        sb.append(this.edificio);

        if (this.piso != null) {
            while (this.piso.length() < 2) {
                this.piso = "0" + this.piso;
            }
        } else {
            this.piso = "00";
        }
        sb.append(this.piso);

        if (this.unidad != null) {
            while (this.unidad.length() < 4) {
                this.unidad = "0" + this.unidad;
            }
        } else {
            this.unidad = "0000";
        }
        sb.append(this.unidad);

        if (sb.length() == 30) {
            this.numeroPredial = sb.toString();
        }
    }

    /**
     * Método que actualiza los componentes desagregados en campos del predio a partir del número
     * predial.
     *
     * @author fabio.navarrete
     */
    public void generarComponentesAPartirDeNumeroPredial() {

        this.tipoAvaluo = this.numeroPredial.substring(5, 7);
        this.sectorCodigo = this.numeroPredial.substring(7, 9);
        this.barrioCodigo = this.numeroPredial.substring(9, 13);
        this.manzanaCodigo = this.numeroPredial.substring(13, 17);
        this.predio = this.numeroPredial.substring(17, 21);
        this.condicionPropiedad = this.numeroPredial.substring(21, 22);
        this.edificio = this.numeroPredial.substring(22, 24);
        this.piso = this.numeroPredial.substring(24, 26);
        this.unidad = this.numeroPredial.substring(26, 30);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof PPredio)) {
            return false;
        }
        if (this.id == null) {
            return false;
        }
        PPredio castOther = (PPredio) other;
        return (this.id.equals(castOther.getId()));

    }

    /**
     * Determina si el PPredio está seleccionado en alguna pantalla o sitio en que se requiera.
     *
     * @return
     */
    @Transient
    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que retorna el número de piso dentro del edificio o torre como una cadena de texto
     * obtenida a partir del número predial y su estructura predefinida
     *
     * @return
     * @author pedro.garcia
     */
    @Transient
    public String getNumeroPiso() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(24, 26);
        }
        return "";
    }

    // ---------------------------------------- //
    /**
     * Método que retorna si el pPredio hace referencia a una ficha matriz.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isEsPredioFichaMatriz() {
        boolean answer = false;
        if (this.PFichaMatrizs != null && !this.PFichaMatrizs.isEmpty()) {
            for (PFichaMatriz pfm : this.PFichaMatrizs) {
                if (pfm.getPPredio() != null &&
                    pfm.getPPredio().getId() != null &&
                    this.id.longValue() == pfm.getPPredio().getId()
                    .longValue()) {
                    answer = true;
                    break;
                }
            }
        }
        return answer;
    }

    // ---------------------------------------- //
    /**
     * Determina el coeficiente de copropiedad si el pPredio hace referencia a un predio creado a
     * partir de una ficha matriz.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public Double getCoeficienteCopropiedadFM() {
        return coeficienteCopropiedadFM;
    }

    public void setCoeficienteCopropiedadFM(Double coeficienteCopropiedadFM) {
        this.coeficienteCopropiedadFM = coeficienteCopropiedadFM;
    }
    // ---------------------------------------------------------------------------------------------

    /**
     * Método que revisa si un predio se encuentra en propiedad horizontal o condominio
     *
     * @author juan.agudelo
     */
    @Transient
    public boolean isEsPredioEnPH() {

        if (condicionPropiedad != null &&
            !condicionPropiedad.isEmpty() &&
            (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_1.getCodigo()) ||
            condicionPropiedad.equals(EPredioCondicionPropiedad.CP_6.getCodigo()) ||
            condicionPropiedad.equals(EPredioCondicionPropiedad.CP_7.getCodigo()) ||
            condicionPropiedad.equals(EPredioCondicionPropiedad.CP_8.getCodigo()) ||
            condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9.getCodigo()))) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * Condicion 5 o 6 definen si es mejora
     *
     * @author fredy.wilches
     * @return
     */
    @Transient
    public boolean isMejora() {
        if (this.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo()) ||
            this.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_6.getCodigo())) {
            return true;
        }
        return false;
    }

    /**
     * Condicion 8 define el predio como condominio
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public boolean isCondominio() {
        if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(this.getCondicionPropiedad())) {
            return true;
        }
        return false;
    }

    /**
     * valida si es un predio en ph y si en las ultimas 4 posiciones del numero predial es diferente
     * de 0000, si no es asi es una unidad en ph
     *
     * @author franz.gamba
     * @return
     */
    @Transient
    public boolean isUnidadEnPH(){
        if(EPredioCondicionPropiedad.CP_9.getCodigo().equals(condicionPropiedad)){
            if(!this.getUnidad().equals("0000")){
                return true;
            }
        }
        return false;
    }

    /**
     *  Valida si es un predio es un condominio y si sus ultimas 4 posiciones del numero predial son 0000
     * @author david.cifuentes
     * @return
     */
     @Transient
     public boolean isUnidadEnCondominio(){
         if(EPredioCondicionPropiedad.CP_8.getCodigo().equals(condicionPropiedad)){
                if(!this.getUnidad().equals("0000")){
                    return true;
                }
            }
            return false;
        }

    /**
     * Busca la dirreccion principal asociado al predio
     *
     * @return Direccion Principal
     * @author andres.eslava
     */
    @Transient
    public String getDireccionPrincipal() {
        for (PPredioDireccion pd : this.PPredioDireccions) {
            if (pd.getPrincipal().equals(ESiNo.SI.toString())) {
                return pd.getDireccion();
            }
        }
        return "";
    }

    /**
     * Retorna el objeto PredioDireccion relacionado a la direccion principal del predio
     *
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public PPredioDireccion getPredioDireccionPrincipal() {

        if (this.PPredioDireccions == null || this.PPredioDireccions.isEmpty()) {
            return null;
        } else {
            for (PPredioDireccion pd : this.PPredioDireccions) {
                if (pd.getPrincipal().equals(ESiNo.SI.toString())) {
                    return pd;
                }
            }
            return null;
        }
    }

    /**
     * Se usa para determinar cual es el ultimo predio agregado para el desenglobe
     *
     * @return
     */
    @Transient
    public boolean isUltimoDesenglobe() {
        return ultimoDesenglobe;
    }

    public void setUltimoDesenglobe(boolean ultimoDesenglobe) {
        this.ultimoDesenglobe = ultimoDesenglobe;
    }

    /**
     * Método para obtener el area de terreno distribuida en hectareas y metros
     *
     * @author felipe.cadena
     *
     * @return - arreglo de dos posiciones con el area en Hectareas y Metros respectivamente
     *
     */
    @Transient
    public int[] getAreaTerrenoHectareasMetros() {
        return Utilidades.convertirAreaAformatoHectareas(this.areaTerreno);
    }

    /**
     * Método para obtener el area de terreno en hectareas
     *
     * @author felipe.cadena
     *
     * @return
     *
     */
    @Transient
    public double getAreaTerrenoHectareas() {

        Double areaH = ((double) Math.round(this.areaTerreno)) / 10000;

        return areaH;
    }

    /**
     * Determina si un predio es original, aplica para mutaciones de segunda de tipo especial, por
     * ejemplo los desenglobes por etapas.
     *
     * {@link ETramiteRadicacionEspecial}
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public boolean isPredioOriginalTramite() {

        if (this.originalTramite != null) {
            if (ESiNo.SI.getCodigo().equals(this.originalTramite)) {
                return true;
            }
        }
        return false;
    }

    @Transient
    public int getEstadoEdicionEtapas() {
        return estadoEdicionEtapas;
    }

    public void setEstadoEdicionEtapas(int estadoEdicionEtapas) {
        this.estadoEdicionEtapas = estadoEdicionEtapas;
    }

    /**
     * Transient que almacena el numero predial del predio en firme. Se usa para desenglobes por
     * etapas
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public String getNumeroPredialOriginal() {
        return numeroPredialOriginal;
    }

    public void setNumeroPredialOriginal(String numeroPredialOriginal) {
        this.numeroPredialOriginal = numeroPredialOriginal;
    }

    /**
     * Si el numero predial de uno de los predios originales en desenglobes por etapas cambia
     * retorna el nuevo numero, sino retorna vacio. Si el predio no es predio original retorna vacio
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public String getNumeroPredialNuevo() {
        if (this.originalTramite == null || this.originalTramite.isEmpty()) {
            return "";
        }
        if (this.numeroPredialOriginal == null || this.numeroPredialOriginal.isEmpty()) {
            return "";
        }
        if (this.numeroPredial.equals(this.numeroPredialOriginal)) {
            return "";
        }

        return numeroPredial;
    }

    /**
     * Si el numero predial de uno de los predios originales en desenglobes por etapas cambia
     * retorna el nuevo numero, sino retorna vacio. Si el predio no es predio original retorna vacio
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public String getNumeroPredialNuevoConFormato() {
        if (this.originalTramite == null || this.originalTramite.isEmpty()) {
            return "";
        }
        if (this.numeroPredialOriginal == null || this.numeroPredialOriginal.isEmpty()) {
            return "";
        }
        if (this.numeroPredial.equals(this.numeroPredialOriginal)) {
            return "";
        }

        return this.formatNumeroPredial(numeroPredial);
    }

    /**
     * Siempre retorna el numero predial del predio original aun si este ha cambiado
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public String getNumeroPredialAntiguo() {
        if (this.originalTramite == null || this.originalTramite.isEmpty()) {
            return this.numeroPredial;
        }
        if (this.numeroPredialOriginal == null || this.numeroPredialOriginal.isEmpty()) {
            return this.numeroPredial;
        }
        if (this.numeroPredial.equals(this.numeroPredialOriginal)) {
            return this.numeroPredial;
        }

        return numeroPredialOriginal;
    }

    /**
     * Siempre retorna el numero predial del predio original aun si este ha cambiado
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public String getNumeroPredialAntiguoConFormato() {
        if (this.originalTramite == null || this.originalTramite.isEmpty()) {
            return this.formatNumeroPredial(this.numeroPredial);
        }
        if (this.numeroPredialOriginal == null || this.numeroPredialOriginal.isEmpty()) {
            return this.formatNumeroPredial(this.numeroPredial);
        }
        if (this.numeroPredial.equals(this.numeroPredialOriginal)) {
            return this.formatNumeroPredial(this.numeroPredial);
        }

        return this.formatNumeroPredial(this.numeroPredialOriginal);
    }

    /**
     * Retorna el numero predial con formato
     *
     * @author felipe.cadena
     * @param numero
     * @return
     */
    private String formatNumeroPredial(String numero) {
        String formatedNumPredial = "";

        if (numero != null && !numero.isEmpty()) {
            // Departamento
            formatedNumPredial = numero.substring(0, 2).concat("-");
            // Municipio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(2, 5)).concat("-");
            // Tipo Avalúo
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(5, 7)).concat("-");
            // Sector
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(7, 9)).concat("-");
            // Comuna
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(9, 11)).concat("-");
            // Barrio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(11, 13)).concat("-");
            // Manzana
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(13, 17)).concat("-");
            // Predio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(17, 21)).concat("-");
            // Condición Propiedad
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(21, 22)).concat("-");
            // Edificio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(22, 24)).concat("-");
            // Piso
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(24, 26)).concat("-");
            // Unidad
            formatedNumPredial = formatedNumPredial.concat(numero
                .substring(26, 30));
        }
        return formatedNumPredial;

    }

    /**
     * Condicion 1 define predio de ficha matriz mixta (Unidades de condicion 8 y 9)
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public boolean isMixto() {
        if (this.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_1.getCodigo())) {
            return true;
        }
        return false;
    }

}
