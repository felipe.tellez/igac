package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * Objeto que almacena las validaciónes para la cancelación de un trámite
 *
 * @author javier.aponte
 */
public class ValidacionCancelacionTramiteDTO implements Serializable {

    private static final long serialVersionUID = -7457063150673208637L;

    /**
     * Datos
     */
    private String numeroRadicacion;
    private String razonNoCancelacion;

    /**
     * Constructor
     */
    public ValidacionCancelacionTramiteDTO() {
        numeroRadicacion = "";
        razonNoCancelacion = "";
    }

    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public String getRazonNoCancelacion() {
        return razonNoCancelacion;
    }

    public void setRazonNoCancelacion(String razonNoCancelacion) {
        this.razonNoCancelacion = razonNoCancelacion;
    }

}
