/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los estados de una comisión de ofertas inmobiliarias, corresponde al dominio
 * ORDEN_COMISION_ESTADO
 *
 * @author christian.rodriguez
 */
public enum EOfertaComisionEstado {

    // D: código (valor)
    APROBADA("Aprobada"),
    CANCELADA("Cancelada"),
    POR_APROBAR("En espera para aprobación"),
    ENVIO_APROBACION("Por ser enviadas a aprobación");

    private String valor;

    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private EOfertaComisionEstado(String valor) {
        this.valor = valor;
    }

    public String getCodigo() {
        return this.toString();
    }

}
