package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ESiNo;
import java.util.LinkedList;

/**
 * Valid Object para desplegar la información requerida en la lista de tareas geográficas para el
 * editor desktop
 *
 * @author franz.gamba
 *
 * @modified andres.eslava 29/05/2013 se agrega el nombre del subproceso de la actividad.
 * @modified andres.eslava::15/05/2013::refs #12603::15/05/2015::se agrega informacion si es una
 * radicacion especial
 */
public class ActividadVO implements Serializable {

    /*
     * El orden en el que se muestra el archivo XML corresponde con el orden en el que se definen
     * los atributos
     */
    private static final long serialVersionUID = 3360556872325692776L;
    private String idActividad;
    private String numeroSolicitud;
    private Long idTramite;

    private String tipoTramite;
    private String claseMutacion;
    private String subtipoMutacion;
    private String radicacionEspecial;
    private List<String> datosRectificacion;
    private String englobeVirtual;
    private String fichaEnglobeVirtual;

    private String omitidoNuevo;
    private String tipoAvaluo;
    private String numeroRadicacion;
    private String codigoDepartamento;
    private String nombreDepartamento;
    private String codigoMunicipio;
    private String nombreMunicipio;
    private PredioInfoVO predio;
    private String actividadActual;
    private String formaPeticion;

    //Desenglobe
    private Boolean desenglobeManzana;
    private PredioInfoVO predioOriginal;
    private List<PredioInfoVO> prediosResultantes;

    //Englobe
    private PredioInfoVO predioResultante;
    private List<PredioInfoVO> prediosOriginales;

    private String estadoTramite;
    private Date fechaCreacion;
    private Date fechaAsignacion;

    // Depuracion
    private List<TramiteInconsistenciaVO> inconsistenciaTramiteVO;
    private List<PredioInfoVO> prediosManzanaDepuracion;

    //Desenglobe con desenglobe de manzanas
    private List<ManzanaVO> manzanasResultantes;

    //Predios invariantes(Editados prediamente geograficamente)
    private List<PredioInfoVO> prediosInvariantes;

    public ActividadVO() {

    }

    public ActividadVO(Actividad actividad, ITramite tram) {

        this.setIdActividad(actividad.getId());
        this.setFechaCreacion(actividad.getFechaInicio().getTime());
        this.setFechaAsignacion(actividad.getFechaAsignacion().getTime());

        String no = (String) actividad.getNumeroSolicitud();

        this.setNumeroSolicitud(no);

        List<Tramite> tr = tram.consultarTramitesSolicitud(no);
        for (Tramite t : tr) {
            this.setIdTramite(t.getId());
            this.setTipoTramite(t.getTipoTramiteCadenaCompleto());
            this.setClaseMutacion(t.getClaseMutacion());
            this.setSubtipoMutacion(t.getSubtipo());
            this.setEstadoTramite(t.getEstado());
            //El tipo avaluo no es del predio?			
            this.setTipoAvaluo(t.getPredio().getTipoAvaluo());
            this.setNumeroRadicacion(t.getNumeroRadicacion());
            this.setCodigoDepartamento(t.getDepartamento().getCodigo());
            this.setNombreDepartamento(t.getDepartamento().getNombre());
            this.setCodigoMunicipio(t.getMunicipio().getCodigo());
            this.setNombreMunicipio(t.getMunicipio().getCodigo());
            this.setFormaPeticion(t.getFormaPeticion());

        }

    }

    public List<PredioInfoVO> getPrediosInvariantes() {
        return prediosInvariantes;
    }

    public void setPrediosInvariantes(List<PredioInfoVO> prediosInvariantes) {
        this.prediosInvariantes = prediosInvariantes;
    }

    /**
     * Constructor de la ActividadVo en terminos de la actividad y el tramite
     *
     * @param unaActividad actividad obtenida del servidor de procesos asociado a un tramite de
     * edicion geografica
     * @param unTramite tramite asociado a la actividad
     * @return
     * @author andres.eslava
     */
    public ActividadVO(Actividad unaActividad, Tramite unTramite) {
        // Miembros mínimos de la clase Actividad 
        this.setIdActividad(unaActividad.getId());
        this.setNumeroSolicitud(unaActividad.getNumeroSolicitud());
        this.setNumeroRadicacion(unaActividad.getNumeroRadicacion());
        this.setActividadActual(unaActividad.getRutaActividad().getSubprocesoActividad());
        this.setNumeroSolicitud(unTramite.getSolicitud().getNumero());
        this.setFormaPeticion(unTramite.getFormaPeticion());

        if (null != unaActividad.getFechaInicio()) {
            this.setFechaCreacion(unaActividad.getFechaInicio().getTime());
        }
        if (null != unaActividad.getFechaAsignacion()) {
            this.setFechaAsignacion(unaActividad.getFechaAsignacion().getTime());
        }

        // Miembros mínimos de la clase Tramite     
        this.setIdTramite(unTramite.getId());
        this.setTipoTramite(unTramite.getTipoTramiteCadenaCompletoEditorGeografico());
        this.setClaseMutacion(EMutacionClase.getNombreByCodigo(unTramite.getClaseMutacion()));
        this.setSubtipoMutacion(unTramite.getSubtipo());
        this.setCodigoDepartamento(unTramite.getDepartamento().getCodigo());
        this.setNombreDepartamento(unTramite.getDepartamento().getNombre());
        this.setCodigoMunicipio(unTramite.getMunicipio().getCodigo());
        this.setNombreMunicipio(unTramite.getMunicipio().getNombre());
        this.setFormaPeticion(unTramite.getFormaPeticion());
        if (null != unTramite.getOmitidoNuevo()) {
            this.setOmitidoNuevo(unTramite.getOmitidoNuevo());
        }
        if (unTramite.getRadicacionEspecial() != null && !unTramite.getRadicacionEspecial().
            isEmpty()) {
            this.radicacionEspecial = unTramite.getRadicacionEspecial();
        }

        if (unTramite.isEnglobeVirtual()) {
            this.englobeVirtual = ESiNo.SI.getCodigo();
            this.fichaEnglobeVirtual = unTramite.obtenerInfoCampoAdicional(
                EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV);
        } else {
            this.englobeVirtual = ESiNo.NO.getCodigo();
        }

        this.datosRectificacion = new LinkedList<String>();
    }

    /**
     * Constructor que retorna una actividad en terminos del tramite para consultas via WS que no
     * requieren informacion de la actividad BPM
     *
     * @param unTramite identificador del tramite.
     * @author andres.eslava
     */
    public ActividadVO(Tramite unTramite) {
        this.setIdTramite(unTramite.getId());
        this.setTipoTramite(unTramite.getTipoTramiteCadenaCompleto());
        this.setClaseMutacion(EMutacionClase.getNombreByCodigo(unTramite.getClaseMutacion()));
        this.setSubtipoMutacion(unTramite.getSubtipo());
        this.setCodigoDepartamento(unTramite.getDepartamento().getCodigo());
        this.setNombreDepartamento(unTramite.getDepartamento().getNombre());
        this.setCodigoMunicipio(unTramite.getMunicipio().getCodigo());
        this.setNombreMunicipio(unTramite.getMunicipio().getNombre());
        this.setFormaPeticion(unTramite.getFormaPeticion());
        if (null != unTramite.getOmitidoNuevo()) {
            this.setOmitidoNuevo(unTramite.getOmitidoNuevo());
        }
        if (unTramite.getRadicacionEspecial() != null && !unTramite.getRadicacionEspecial().
            isEmpty()) {
            this.radicacionEspecial = unTramite.getRadicacionEspecial();
        }

        if (unTramite.isEnglobeVirtual()) {
            this.englobeVirtual = ESiNo.SI.getCodigo();
            this.fichaEnglobeVirtual = unTramite.obtenerInfoCampoAdicional(
                EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV);
        } else {
            this.englobeVirtual = ESiNo.NO.getCodigo();
        }

    }

    public String getFormaPeticion() {
        return formaPeticion;
    }

    public void setFormaPeticion(String formaPeticion) {
        this.formaPeticion = formaPeticion;
    }

    public String getActividadActual() {
        return actividadActual;
    }

    public void setActividadActual(String subprocesoActividad) {
        this.actividadActual = subprocesoActividad;
    }

    public String getIdActividad() {
        return idActividad;
    }

    public void setIdActividad(String idActividad) {
        this.idActividad = idActividad;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public Long getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(Long idTramite) {
        this.idTramite = idTramite;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getClaseMutacion() {
        return claseMutacion;
    }

    public void setClaseMutacion(String claseMutacion) {
        this.claseMutacion = claseMutacion;
    }

    public String getSubtipoMutacion() {
        return subtipoMutacion;
    }

    public void setSubtipoMutacion(String subtipoMutacion) {
        this.subtipoMutacion = subtipoMutacion;
    }

    public String getOmitidoNuevo() {
        return omitidoNuevo;
    }

    public void setOmitidoNuevo(String omitidoNuevo) {
        this.omitidoNuevo = omitidoNuevo;
    }

    public String getTipoAvaluo() {
        return tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public String getEstadoTramite() {
        return estadoTramite;
    }

    public void setEstadoTramite(String estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(Date fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    public PredioInfoVO getPredio() {
        return predio;
    }

    public void setPredio(PredioInfoVO predio) {
        this.predio = predio;
    }

    public PredioInfoVO getPredioOriginal() {
        return predioOriginal;
    }

    public void setPredioOriginal(PredioInfoVO predioOriginal) {
        this.predioOriginal = predioOriginal;
    }

    public List<PredioInfoVO> getPrediosResultantes() {
        return prediosResultantes;
    }

    public void setPrediosResultantes(List<PredioInfoVO> prediosResultantes) {
        this.prediosResultantes = prediosResultantes;
    }

    public PredioInfoVO getPredioResultante() {
        return predioResultante;
    }

    public void setPredioResultante(PredioInfoVO predioResultante) {
        this.predioResultante = predioResultante;
    }

    public List<PredioInfoVO> getPrediosOriginales() {
        return prediosOriginales;
    }

    public void setPrediosOriginales(List<PredioInfoVO> prediosOriginales) {
        this.prediosOriginales = prediosOriginales;
    }

    public List<TramiteInconsistenciaVO> getInconsistenciaTramite() {
        return inconsistenciaTramiteVO;
    }

    public void setInconsistenciaTramite(List<TramiteInconsistenciaVO> inconsistenciaTramite) {
        this.inconsistenciaTramiteVO = inconsistenciaTramite;
    }

    public List<TramiteInconsistenciaVO> getInconsistenciaTramiteVO() {
        return inconsistenciaTramiteVO;
    }

    public void setInconsistenciaTramiteVO(List<TramiteInconsistenciaVO> inconsistenciaTramiteVO) {
        this.inconsistenciaTramiteVO = inconsistenciaTramiteVO;
    }

    public List<PredioInfoVO> getPrediosManzanaDepuracion() {
        return prediosManzanaDepuracion;
    }

    public void setPrediosManzanaDepuracion(List<PredioInfoVO> prediosManzanaDepuracion) {
        this.prediosManzanaDepuracion = prediosManzanaDepuracion;
    }

    public List<ManzanaVO> getManzanasResultantes() {
        return manzanasResultantes;
    }

    public void setManzanasResultantes(List<ManzanaVO> manzanasResultantes) {
        this.manzanasResultantes = manzanasResultantes;
    }

    public Boolean getDesenglobeManzana() {
        return desenglobeManzana;
    }

    public void setDesenglobeManzana(Boolean desenglobeManzana) {
        this.desenglobeManzana = desenglobeManzana;
    }

    public List<String> getDatosRectificacion() {
        return datosRectificacion;
    }

    public void setDatosRectificacion(String datoRectificacion) {
        this.datosRectificacion.add(datoRectificacion);
    }

    public String getEnglobeVirtual() {
        return englobeVirtual;
    }

    public void setEnglobeVirtual(String englobeVirtual) {
        this.englobeVirtual = englobeVirtual;
    }

    public String getFichaEnglobeVirtual() {
        return fichaEnglobeVirtual;
    }

    public void setFichaEnglobeVirtual(String fichaEnglobeVirtual) {
        this.fichaEnglobeVirtual = fichaEnglobeVirtual;
    }
}
