package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.IProyeccionObject;
import javax.persistence.FetchType;

/**
 * The persistent class for the P_FICHA_MATRIZ_TORRE database table.
 *
 * MODIFICACIONES:
 *
 * @author fabio.navarrete -> Se agrega copy constructor
 * @modified david.cifuentes :: Adición de método compareTo(Object)
 * @modified david.cifuentes :: Adición de la variable unidadesSotanos :: Cambio del name de la
 * columna "UNIDADES" a "UNIDADES_PRIVADAS" :: 19/07/12
 * @modified david.cifuentes :: Se implementa la interfaz Cloneable y se agrega método clone ::
 * 23/10/12
 * @modified david.cifuentes :: Se adiciona variable transient estadoEtapas para uso en trámites de
 * PH por etapas :: 13/05/15
 * @modified david.cifuentes :: se adiciona la variable originalTramite :: 19/05/2015
 * @modified david.cifuentes :: Adición de la variable estado :: 08/09/15
 * @modified david.cifuentes :: Adición del método transient isEsOriginalTramite :: 08/03/2016
 */
@Entity
@Table(name = "P_FICHA_MATRIZ_TORRE")
public class PFichaMatrizTorre implements Serializable, IProyeccionObject,
    Comparable, Cloneable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String cancelaInscribe;
    private Date fechaLog;
    private Long pisos;
    private Long sotanos;
    private Long torre;
    private Long unidades;
    private String usuarioLog;
    private PFichaMatriz PFichaMatriz;
    private Long unidadesSotanos;
    private String originalTramite;
    private String estado;
    private FichaMatriz provieneFichaMatriz;
    private FichaMatrizTorre provieneTorre;

    // Transient
    private String estadoEtapas;

    public PFichaMatrizTorre() {
    }

    /**
     * Copy Constructor
     *
     * @param PFichaMatrizTorre
     * @author fabio.navarrete
     */
    public PFichaMatrizTorre(PFichaMatrizTorre PFichaMatrizTorre) {
        this.cancelaInscribe = PFichaMatrizTorre.getCancelaInscribe();
        this.fechaLog = PFichaMatrizTorre.getFechaLog();
        this.id = PFichaMatrizTorre.getId();
        this.PFichaMatriz = PFichaMatrizTorre.getPFichaMatriz();
        this.pisos = PFichaMatrizTorre.getPisos();
        this.sotanos = PFichaMatrizTorre.getSotanos();
        this.torre = PFichaMatrizTorre.getTorre();
        this.unidades = PFichaMatrizTorre.getUnidades();
        this.usuarioLog = PFichaMatrizTorre.getUsuarioLog();
        this.unidadesSotanos = PFichaMatrizTorre.getUnidadesSotanos();
        this.provieneFichaMatriz = PFichaMatrizTorre.getProvieneFichaMatriz();
        this.provieneTorre = PFichaMatrizTorre.getProvieneTorre();
    }

    @Id
    @GeneratedValue(generator = "FICHA_MATRIZ_TORRE_ID_SEQ")
    @GenericGenerator(name = "FICHA_MATRIZ_TORRE_ID_SEQ", strategy =
        "co.gov.igac.snc.persistence.util.CustomProyeccionIdGenerator", parameters = {
            @Parameter(name = "sequence", value = "FICHA_MATRIZ_TORRE_ID_SEQ"),
            @Parameter(name = "allocationSize", value = "1")})
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public Long getPisos() {
        return this.pisos;
    }

    public void setPisos(Long pisos) {
        this.pisos = pisos;
    }

    public Long getSotanos() {
        return this.sotanos;
    }

    public void setSotanos(Long sotanos) {
        this.sotanos = sotanos;
    }

    public Long getTorre() {
        return this.torre;
    }

    public void setTorre(Long torre) {
        this.torre = torre;
    }

    @Column(name = "UNIDADES_PRIVADAS", nullable = false, precision = 4)
    public Long getUnidades() {
        return this.unidades;
    }

    public void setUnidades(Long unidades) {
        this.unidades = unidades;
    }

    @Column(name = "UNIDADES_SOTANOS", nullable = false, precision = 4)
    public Long getUnidadesSotanos() {
        return this.unidadesSotanos;
    }

    public void setUnidadesSotanos(Long unidadesSotanos) {
        this.unidadesSotanos = unidadesSotanos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to PFichaMatriz
    @ManyToOne
    @JoinColumn(name = "FICHA_MATRIZ_ID")
    public PFichaMatriz getPFichaMatriz() {
        return this.PFichaMatriz;
    }

    public void setPFichaMatriz(PFichaMatriz PFichaMatriz) {
        this.PFichaMatriz = PFichaMatriz;
    }

    @Column(name = "ORIGINAL_TRAMITE", length = 2)
    public String getOriginalTramite() {
        return this.originalTramite;
    }

    public void setOriginalTramite(String resultado) {
        this.originalTramite = resultado;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROVIENE_FICHA_MATRIZ_ID", nullable = true)
    public FichaMatriz getProvieneFichaMatriz() {
        return provieneFichaMatriz;
    }

    public void setProvieneFichaMatriz(FichaMatriz provieneFichaMatriz) {
        this.provieneFichaMatriz = provieneFichaMatriz;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROVIENE_TORRE_ID", nullable = true)
    public FichaMatrizTorre getProvieneTorre() {
        return this.provieneTorre;
    }

    public void setProvieneTorre(FichaMatrizTorre provieneTorre) {
        this.provieneTorre = provieneTorre;
    }

    /**
     * Método que retorna el estado de una torre para trámites de PH en etapas o quinta Masivo.
     *
     * @return
     * @author david.cifuentes
     */
    @Transient
    public String getEstadoEtapas() {
        return this.estadoEtapas;
    }

    public void setEstadoEtapas(String estadoEtapas) {
        this.estadoEtapas = estadoEtapas;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof PFichaMatrizTorre)) {
            return false;
        }
        if (this.id == null) {
            return false;
        }
        PFichaMatrizTorre castOther = (PFichaMatrizTorre) other;
        return (this.id.equals(castOther.getId()));

    }

    // ------------------------------------------//
    /**
     * Determina si una {@link PFichaMatrizTorre} es original del trámite, en trámites masivos
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isEsOriginalTramite() {

        if (this.originalTramite != null) {
            if (ESiNo.SI.getCodigo().equals(this.originalTramite)) {
                return true;
            }
        }
        return false;
    }

    // ------------------------------------------//
    /**
     * Método implementado para comparar objetos de ésta clase entre sí y poder ordenarlos en base
     * al número de torre.
     *
     * @author david.cifuentes
     */
    @Override
    public int compareTo(Object o) {
        PFichaMatrizTorre pFichaMatrizTorre = (PFichaMatrizTorre) o;
        if (this.torre == null ||
            pFichaMatrizTorre.getTorre() == null) {
            return -1;
        }
        if (pFichaMatrizTorre == null) {
            return 1;
        }
        return this.torre.compareTo(pFichaMatrizTorre.getTorre());
    }

    // ------------------------------------------//
    /**
     * Método que realiza el clone de una PFichaMatrizTorre.
     *
     * @author david.cifuentes
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

}
