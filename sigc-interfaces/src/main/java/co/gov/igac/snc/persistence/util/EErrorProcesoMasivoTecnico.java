package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio ERROR_PROC_MASIVO_TECNICO
 *
 * @author juan.agudelo
 */
public enum EErrorProcesoMasivoTecnico {

    //D: valor ("codigo")
    ERROR_TECNICO("1");

    private String codigo;

    EErrorProcesoMasivoTecnico(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
