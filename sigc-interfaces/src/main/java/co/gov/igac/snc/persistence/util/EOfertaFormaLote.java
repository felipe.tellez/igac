/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los codigos de tipos de forma de lote para ofertas
 *
 * @author christian.rodriguez
 */
public enum EOfertaFormaLote {

    IRREGULAR("Irregular"),
    REGULAR("Regular"),;

    private String codigo;

    private EOfertaFormaLote(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
