package co.gov.igac.snc.persistence.entity.formacion;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the TABLA_TERRENO database table.
 *
 */
@Entity
@NamedQueries({@NamedQuery(
        name = "findByZonaVigencia",
        query = "from TablaTerreno tt where tt.zonaCodigo= :zona AND tt.vigencia= :vigencia")})
@Table(name = "TABLA_TERRENO")
public class TablaTerreno implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private String nombre;
    private String usuarioLog;
    private Date vigencia;
    private String zonaCodigo;

    public TablaTerreno() {
    }

    @Id
    @SequenceGenerator(name = "TABLA_TERRENO_ID_GENERATOR", sequenceName = "TABLA_TERRENO_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TABLA_TERRENO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "ZONA_CODIGO")
    public String getZonaCodigo() {
        return this.zonaCodigo;
    }

    public void setZonaCodigo(String zonaCodigo) {
        this.zonaCodigo = zonaCodigo;
    }

}
