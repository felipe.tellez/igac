/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con los identificadores de los tabs para información cancelación inscripción
 *
 * @author david.cifuentes
 */
public enum EIdTabInformacionCancelacionInscripcion {

    CONDICION_PROPIEDAD("condicionPropiedadId"),
    DETALLE_AVALUO("detalleAvaluoId"),
    FICHA_MATRIZ("fichaMatrizId"),
    INSCRIPCIONES_DECRETOS("inscripcionesYDecretosId"),
    PROPIETARIOS_POSEEDORES("propietariosPoseedoresId"),
    SELECCIONAR("seleccionar"),
    UBICACION_DEL_PREDIO("ubicacionDelPredioId");

    private String id;

    private EIdTabInformacionCancelacionInscripcion(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
