package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Clase que reduce la informacion de los pisos de la torre en una Ficha matriz para ser mostrados
 * en las tareas geograficas
 *
 * @author franz.gamba
 */
public class FichaMatrizPisoVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6386565662872439658L;
    private Long numero;
    private List<FichaMatrizPredioVO> predios;

    /** Empty constructor */
    public FichaMatrizPisoVO() {

    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public List<FichaMatrizPredioVO> getPredios() {
        return predios;
    }

    public void setPredios(List<FichaMatrizPredioVO> predios) {
        this.predios = predios;
    }
}
