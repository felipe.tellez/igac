package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * DatoMinimoDocumentacion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "DATO_MINIMO_DOCUMENTACION", schema = "SNC_TRAMITE")
public class DatoMinimoDocumentacion implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private Long tipoDocumentoId;
    private String dato;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public DatoMinimoDocumentacion() {
    }

    /** full constructor */
    public DatoMinimoDocumentacion(Long id, Long tipoDocumentoId, String dato,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tipoDocumentoId = tipoDocumentoId;
        this.dato = dato;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_DOCUMENTO_ID", nullable = false, precision = 10, scale = 0)
    public Long getTipoDocumentoId() {
        return this.tipoDocumentoId;
    }

    public void setTipoDocumentoId(Long tipoDocumentoId) {
        this.tipoDocumentoId = tipoDocumentoId;
    }

    @Column(name = "DATO", nullable = false, length = 30)
    public String getDato() {
        return this.dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
