package co.gov.igac.snc.fachadas;

import java.util.List;

import javax.ejb.Remote;

import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionadaRe;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.util.FiltroDatosConsultaTramitesIper;

/**
 *
 * Clase encargada del negocio para Interrelacion (IPER)
 *
 * @author fredy.wilches
 *
 */
@Remote
public interface IInterrelacion {

    public Solicitud procesarPendientes();

    public void proyectarPropietarios(Solicitud s);

    public List<InfInterrelacionadaRe> getTramites(FiltroDatosConsultaTramitesIper filtro);

}
