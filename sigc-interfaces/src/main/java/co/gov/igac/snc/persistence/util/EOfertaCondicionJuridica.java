/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio de OFERTA_CONDICION_JURIDICA
 *
 * @author javier.aponte
 */
public enum EOfertaCondicionJuridica {

    NPH("NPH"),
    PH("PH");

    private String codigo;

    private EOfertaCondicionJuridica(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }
}
