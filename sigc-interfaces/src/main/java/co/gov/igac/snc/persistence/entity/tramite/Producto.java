package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.snc.persistence.util.ESiNo;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Producto entity. @author MyEclipse Persistence Tools Modified by leidy.gonzalez Se agrega campo
 * ejecucion tipo programa, se inserta en el full constructor los campos ejecucion programa y
 * ejecucion tipo programa
 */
@Entity
@Table(name = "PRODUCTO", schema = "SNC_TRAMITE")
public class Producto implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private String codigo;
    private Date fechaLog;
    private String nombre;
    private String usuarioLog;
    private GrupoProducto grupoProducto;
    private List<ProductoCatastral> productoCatastrals;
    private List<ProductoFormato> productoFormatos;
    private String ejecucionPrograma;
    private String ejecucionTipoPrograma;
    private String activo;

    // Constructors
    /** default constructor */
    public Producto() {
    }

    /** minimal constructor */
    public Producto(Long id, GrupoProducto grupoProducto, String nombre,
        String codigo, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.grupoProducto = grupoProducto;
        this.nombre = nombre;
        this.codigo = codigo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Producto(Long id, GrupoProducto grupoProducto, String nombre,
        String usuarioLog, Date fechaLog, String codigo,
        List<ProductoCatastral> productoCatastrals,
        List<ProductoFormato> productoFormatos,
        String ejecucionPrograma,
        String ejecucionTipoPrograma) {
        this.id = id;
        this.grupoProducto = grupoProducto;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.codigo = codigo;
        this.productoCatastrals = productoCatastrals;
        this.productoFormatos = productoFormatos;
        this.ejecucionPrograma =
            this.ejecucionTipoPrograma = ejecucionTipoPrograma;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CODIGO", nullable = false, length = 20)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name = "NOMBRE", nullable = false, length = 600)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GRUPO_PRODUCTO_ID", nullable = false)
    public GrupoProducto getGrupoProducto() {
        return this.grupoProducto;
    }

    public void setGrupoProducto(GrupoProducto grupoProducto) {
        this.grupoProducto = grupoProducto;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "producto")
    public List<ProductoCatastral> getProductoCatastrals() {
        return this.productoCatastrals;
    }

    public void setProductoCatastrals(List<ProductoCatastral> productoCatastrals) {
        this.productoCatastrals = productoCatastrals;
    }

    // bi-directional many-to-one association to ProductoFormato
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "producto")
    public List<ProductoFormato> getProductoFormatos() {
        return this.productoFormatos;
    }

    public void setProductoFormatos(List<ProductoFormato> productoFormatos) {
        this.productoFormatos = productoFormatos;
    }

    @Column(name = "EJECUCION_PROGRAMA", length = 100)
    public String getEjecucionPrograma() {
        return this.ejecucionPrograma;
    }

    public void setEjecucionPrograma(String ejecucionPrograma) {
        this.ejecucionPrograma = ejecucionPrograma;
    }

    @Column(name = "EJECUCION_TIPO_PROGRAMA", length = 50)
    public String getEjecucionTipoPrograma() {
        return ejecucionTipoPrograma;
    }

    public void setEjecucionTipoPrograma(String ejecucionTipoPrograma) {
        this.ejecucionTipoPrograma = ejecucionTipoPrograma;
    }

    @Column(name = "ACTIVO")
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Transient
    public boolean isProductoActivo() {
        return ESiNo.SI.getCodigo().equals(activo);
    }

}
