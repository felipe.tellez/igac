package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import javax.persistence.Transient;

/**
 * PredioBloqueo entity.
 */
/*
 * juan.agudelo 27/06/11 se agrego la secuencia PREDIO_BLOQUEO_ID_SEQ
 *
 * juan.agudelo 29/06/11 se modifico el objeto Documento documentoSoporte
 *
 * juan.agudelo 07/07/11 se agrego funcionalidad para documentoSoporteDesbloqueo, fechaDesbloqueo,
 * motivoDesbloqueo y usuarioDesbloqueo
 */
@Entity
@Table(name = "PREDIO_BLOQUEO", schema = "SNC_CONSERVACION")
public class PredioBloqueo implements java.io.Serializable, Cloneable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = 297731316305757066L;

    private Long id;
    private Predio predio;
    private Date fechaRecibido;
    private Date fechaInicioBloqueo;
    private Date fechaTerminaBloqueo;
    private Date fechaDesbloqueo;
    private String motivoBloqueo;
    private String motivoDesbloqueo;
    private Documento documentoSoporteBloqueo;
    private Documento documentoSoporteDesbloqueo;
    private Long entidadId;
    private String usuarioLog;
    private String usuarioDesbloqueo;

    private Date fechaLog;

    private Departamento departamento;
    private Municipio municipio;
    private String restitucionTierras;
    private String tipoBloqueo;
    private Date fechaEnvio;
    private String numeroRadicacion;
    private String nombreEntidad;

    private Departamento departamentoDesbloqueo;
    private Municipio municipioDesbloqueo;
    private String restitucionTierrasDesbloqueo;
    private String tipoDesbloqueo;
    private Date fechaEnvioDesbloqueo;
    private String numeroRadicacionDesbloqueo;
    private String usuarioLogDesbloqueo;
    private Date fechaLogDesbloqueo;
    private Entidad entidadDesbloqueo;
    private Date fechaRecibidoDesbloqueo;
    // Constructors

    /** default constructor */
    public PredioBloqueo() {
    }

    /** minimal constructor */
    public PredioBloqueo(Long id, Predio predio, Date fechaInicio,
        Date fechaRecibido, Documento documentoSoporteBloqueo,
        String motivo, Long entidadId, String usuarioLog, Date fechaLog,
        Departamento departamento, Municipio municipio) {

        this.id = id;
        this.predio = predio;
        this.fechaInicioBloqueo = fechaInicio;
        this.fechaRecibido = fechaRecibido;
        this.documentoSoporteBloqueo = documentoSoporteBloqueo;
        this.motivoBloqueo = motivo;
        this.entidadId = entidadId;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.departamento = departamento;
        this.municipio = municipio;
    }

    /** full constructor */
    public PredioBloqueo(Long id, Predio predio, Date fechaInicio,
        Date fechaRecibido, Date fechaDesbloqueo, Date fechaTermina,
        String motivo, Documento documentoSoporteBloqueo,
        Documento documentoSoporteDesbloqueo,
        Long entidadId, String usuarioLog,
        String usuarioDesbloqueo, Date fechaLog, Departamento departamento, Municipio municipio) {
        this.id = id;
        this.predio = predio;
        this.fechaInicioBloqueo = fechaInicio;
        this.fechaTerminaBloqueo = fechaTermina;
        this.fechaRecibido = fechaRecibido;
        this.fechaDesbloqueo = fechaDesbloqueo;
        this.motivoBloqueo = motivo;
        this.documentoSoporteBloqueo = documentoSoporteBloqueo;
        this.documentoSoporteDesbloqueo = documentoSoporteDesbloqueo;
        this.usuarioDesbloqueo = usuarioDesbloqueo;
        this.entidadId = entidadId;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.departamento = departamento;
        this.municipio = municipio;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PREDIO_BLOQUEO_ID_SEQ")
    @SequenceGenerator(name = "PREDIO_BLOQUEO_ID_SEQ", sequenceName = "PREDIO_BLOQUEO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_BLOQUEO", nullable = false, length = 7)
    public Date getFechaInicioBloqueo() {
        return this.fechaInicioBloqueo;
    }

    public void setFechaInicioBloqueo(Date fechaInicioBloqueo) {
        this.fechaInicioBloqueo = fechaInicioBloqueo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_TERMINA_BLOQUEO", length = 7)
    public Date getFechaTerminaBloqueo() {
        return this.fechaTerminaBloqueo;
    }

    public void setFechaTerminaBloqueo(Date fechaTerminaBloqueo) {
        this.fechaTerminaBloqueo = fechaTerminaBloqueo;
    }

    public void setFechaDesbloqueo(Date fechaDesbloqueo) {
        this.fechaDesbloqueo = fechaDesbloqueo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DESBLOQUEO", length = 7)
    public Date getFechaDesbloqueo() {
        return this.fechaDesbloqueo;
    }

    @Column(name = "MOTIVO_BLOQUEO", nullable = false, length = 2000)
    public String getMotivoBloqueo() {
        return this.motivoBloqueo;
    }

    public void setMotivoBloqueo(String motivo) {
        this.motivoBloqueo = motivo;
    }

    @Column(name = "MOTIVO_DESBLOQUEO", length = 250)
    public String getMotivoDesbloqueo() {
        return this.motivoDesbloqueo;
    }

    public void setMotivoDesbloqueo(String motivoDesbloqueo) {
        this.motivoDesbloqueo = motivoDesbloqueo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_BLOQUEO_DOCUMENTO_ID")
    public Documento getDocumentoSoporteBloqueo() {
        return this.documentoSoporteBloqueo;
    }

    public void setDocumentoSoporteBloqueo(Documento documentoSoporteBloqueo) {
        this.documentoSoporteBloqueo = documentoSoporteBloqueo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DBLOQUEO_DOCUMENTO_ID")
    public Documento getDocumentoSoporteDesbloqueo() {
        return this.documentoSoporteDesbloqueo;
    }

    public void setDocumentoSoporteDesbloqueo(
        Documento documentoSoporteDesbloqueo) {
        this.documentoSoporteDesbloqueo = documentoSoporteDesbloqueo;
    }

    @Column(name = "USUARIO_DESBLOQUEO", length = 100)
    public String getUsuarioDesbloqueo() {
        return this.usuarioDesbloqueo;
    }

    public void setUsuarioDesbloqueo(String usuarioDesbloqueo) {
        this.usuarioDesbloqueo = usuarioDesbloqueo;
    }

    @Column(name = "ENTIDAD_ID", precision = 10, scale = 0)
    public Long getEntidadId() {
        return this.entidadId;
    }

    public void setEntidadId(Long entidadId) {
        this.entidadId = entidadId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RECIBIDO", nullable = false, length = 7)
    public Date getFechaRecibido() {
        return this.fechaRecibido;
    }

    public void setFechaRecibido(Date fechaRecibido) {
        this.fechaRecibido = fechaRecibido;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "RESTITUCION_TIERRAS", nullable = false, length = 2)
    public String getRestitucionTierras() {
        return restitucionTierras;
    }

    public void setRestitucionTierras(String restitucionTierras) {
        this.restitucionTierras = restitucionTierras;
    }

    @Column(name = "TIPO_BLOQUEO", nullable = false, length = 50)
    public String getTipoBloqueo() {
        return tipoBloqueo;
    }

    public void setTipoBloqueo(String tipoBloqueo) {
        this.tipoBloqueo = tipoBloqueo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ENVIO", nullable = false, length = 7)
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @Column(name = "NUMERO_RADICACION", nullable = false, length = 30)
    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    @Override
    public PredioBloqueo clone() throws CloneNotSupportedException {
        return (PredioBloqueo) super.clone();
    }

    @Transient
    public String getNombreEntidad() {
        return nombreEntidad;
    }

    public void setNombreEntidad(String nombreEntidad) {
        this.nombreEntidad = nombreEntidad;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO_DESBLOQUEO")
    public Departamento getDepartamentoDesbloqueo() {
        return departamentoDesbloqueo;
    }

    public void setDepartamentoDesbloqueo(Departamento departamentoDesbloqueo) {
        this.departamentoDesbloqueo = departamentoDesbloqueo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO_DESBLOQUEO")
    public Municipio getMunicipioDesbloqueo() {
        return municipioDesbloqueo;
    }

    public void setMunicipioDesbloqueo(Municipio municipioDesbloqueo) {
        this.municipioDesbloqueo = municipioDesbloqueo;
    }

    @Column(name = "RESTITUCION_TIERRAS_DESBLOQUEO", length = 2)
    public String getRestitucionTierrasDesbloqueo() {
        return restitucionTierrasDesbloqueo;
    }

    public void setRestitucionTierrasDesbloqueo(String restitucionTierrasDesbloqueo) {
        this.restitucionTierrasDesbloqueo = restitucionTierrasDesbloqueo;
    }

    @Column(name = "TIPO_DESBLOQUEO", length = 50)
    public String getTipoDesbloqueo() {
        return tipoDesbloqueo;
    }

    public void setTipoDesbloqueo(String tipoDesbloqueo) {
        this.tipoDesbloqueo = tipoDesbloqueo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ENVIO_DESBLOQUEO", length = 7)
    public Date getFechaEnvioDesbloqueo() {
        return fechaEnvioDesbloqueo;
    }

    public void setFechaEnvioDesbloqueo(Date fechaEnvioDesbloqueo) {
        this.fechaEnvioDesbloqueo = fechaEnvioDesbloqueo;
    }

    @Column(name = "NUMERO_RADICACION_DESBLOQUEO", length = 30)
    public String getNumeroRadicacionDesbloqueo() {
        return numeroRadicacionDesbloqueo;
    }

    public void setNumeroRadicacionDesbloqueo(String numeroRadicacionDesbloqueo) {
        this.numeroRadicacionDesbloqueo = numeroRadicacionDesbloqueo;
    }

    @Column(name = "USUARIO_LOG_DESBLOQUEO", length = 100)
    public String getUsuarioLogDesbloqueo() {
        return usuarioLogDesbloqueo;
    }

    public void setUsuarioLogDesbloqueo(String usuarioLogDesbloqueo) {
        this.usuarioLogDesbloqueo = usuarioLogDesbloqueo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG_DESBLOQUEO", length = 7)
    public Date getFechaLogDesbloqueo() {
        return fechaLogDesbloqueo;
    }

    public void setFechaLogDesbloqueo(Date fechaLogDesbloqueo) {
        this.fechaLogDesbloqueo = fechaLogDesbloqueo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ENTIDAD_ID_DESBLOQUEO")
    public Entidad getEntidadDesbloqueo() {
        return entidadDesbloqueo;
    }

    public void setEntidadDesbloqueo(Entidad entidadDesbloqueo) {
        this.entidadDesbloqueo = entidadDesbloqueo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RECIBIDO_DESBLOQUEO", length = 7)
    public Date getFechaRecibidoDesbloqueo() {
        return fechaRecibidoDesbloqueo;
    }

    public void setFechaRecibidoDesbloqueo(Date fechaRecibidoDesbloqueo) {
        this.fechaRecibidoDesbloqueo = fechaRecibidoDesbloqueo;
    }

}
