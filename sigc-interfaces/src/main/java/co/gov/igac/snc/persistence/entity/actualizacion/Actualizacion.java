package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;

/**
 * The persistent class for the ACTUALIZACION database table.
 *
 */
@NamedQueries({
    @NamedQuery(name = "recuperarDepartamentosMunicipiosPorIdActualizacion", query = "SELECT a " +
        "FROM Actualizacion a " +
        "JOIN FETCH a.departamento " +
        "JOIN FETCH a.municipio " + "WHERE a.id = :id"),
    @NamedQuery(name = "buscarActualizacionPorIdConConveniosYEntidades", query = "SELECT a " +
        "FROM Actualizacion a " + "WHERE a.id = :id")})
@Entity
@Table(name = "ACTUALIZACION", schema = "SNC_ACTUALIZACION")
public class Actualizacion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8855331569314356144L;

    private Long id;
    private Integer anio;
    private String conflictosLimitrofes;
    private String conflictosNomenclaturaVial;
    private String conflictosPerimetroUrbano;
    private Departamento departamento;
    private String estado;
    private Date fecha;
    private Date fechaLog;
    private Municipio municipio;
    private String procesoInstanciaId;
    private BigDecimal resolucionDocumentoId;
    private String responsableDiagnostico;
    private String responsableDiagnosticoId;
    private String tipoProceso;
    private String usuarioLog;
    private String zona;
    private List<ActualizacionContrato> actualizacionContratos;
    private List<ActualizacionDocumentacion> actualizacionDocumentacions;
    private List<ActualizacionDocumento> actualizacionDocumentos;
    private List<ActualizacionResponsable> actualizacionResponsables;
    private List<ActualizacionSedeComision> actualizacionSedeComisions;
    private List<Dotacion> dotacions;
    private List<Convenio> convenios;
    private List<InformacionBasicaAgro> informacionBasicaAgros;
    private List<InformacionBasicaCarto> informacionBasicaCartos;
    private List<ActualizacionEvento> actualizacionEventos;
    private List<SaldoConservacion> saldoConservacions;
    private List<ActualizacionComision> actualizacionComisions;
    private List<GeneracionFormularioSbc> generacionFormularioSbcs;
    private List<ActualizacionLevantamiento> actualizacionLevantamientos;

    /**
     * Default constructor
     */
    public Actualizacion() {
    }

    /**
     * Constructor
     *
     * @param departamento
     * @param municipio
     * @param tipoProceso
     * @param usuarioLog
     */
    public Actualizacion(Departamento departamento, Municipio municipio,
        String tipoProceso, String usuarioLog) {
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoProceso = tipoProceso;
        this.usuarioLog = usuarioLog;
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_ID_GENERATOR", sequenceName = "ACTUALIZACION_ID_SEQ",
        allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACTUALIZACION_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ANIO", precision = 4)
    public Integer getAnio() {
        return this.anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Column(name = "CONFLICTOS_LIMITROFES", length = 2)
    public String getConflictosLimitrofes() {
        return this.conflictosLimitrofes;
    }

    public void setConflictosLimitrofes(String conflictosLimitrofes) {
        this.conflictosLimitrofes = conflictosLimitrofes;
    }

    @Column(name = "CONFLICTOS_NOMENCLATURA_VIAL", length = 2)
    public String getConflictosNomenclaturaVial() {
        return this.conflictosNomenclaturaVial;
    }

    public void setConflictosNomenclaturaVial(String conflictosNomenclaturaVial) {
        this.conflictosNomenclaturaVial = conflictosNomenclaturaVial;
    }

    @Column(name = "CONFLICTOS_PERIMETRO_URBANO", length = 2)
    public String getConflictosPerimetroUrbano() {
        return this.conflictosPerimetroUrbano;
    }

    public void setConflictosPerimetroUrbano(String conflictosPerimetroUrbano) {
        this.conflictosPerimetroUrbano = conflictosPerimetroUrbano;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "PROCESO_INSTANCIA_ID", length = 100)
    public String getProcesoInstanciaId() {
        return this.procesoInstanciaId;
    }

    public void setProcesoInstanciaId(String procesoInstanciaId) {
        this.procesoInstanciaId = procesoInstanciaId;
    }

    @Column(name = "RESOLUCION_DOCUMENTO_ID")
    public BigDecimal getResolucionDocumentoId() {
        return this.resolucionDocumentoId;
    }

    public void setResolucionDocumentoId(BigDecimal resolucionDocumentoId) {
        this.resolucionDocumentoId = resolucionDocumentoId;
    }

    @Column(name = "RESPONSABLE_DIAGNOSTICO")
    public String getResponsableDiagnostico() {
        return this.responsableDiagnostico;
    }

    public void setResponsableDiagnostico(String responsableDiagnostico) {
        this.responsableDiagnostico = responsableDiagnostico;
    }

    @Column(nullable = false, length = 30)
    public String getZona() {
        return this.zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @Column(name = "RESPONSABLE_DIAGNOSTICO_ID")
    public String getResponsableDiagnosticoId() {
        return this.responsableDiagnosticoId;
    }

    public void setResponsableDiagnosticoId(String responsableDiagnosticoId) {
        this.responsableDiagnosticoId = responsableDiagnosticoId;
    }

    @Column(name = "TIPO_PROCESO", nullable = false)
    public String getTipoProceso() {
        return this.tipoProceso;
    }

    public void setTipoProceso(String tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    @Column(name = "USUARIO_LOG", nullable = false)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to ActualizacionContrato
    @OneToMany(mappedBy = "actualizacion")
    public List<ActualizacionContrato> getActualizacionContratos() {
        return this.actualizacionContratos;
    }

    public void setActualizacionContratos(
        List<ActualizacionContrato> actualizacionContratos) {
        this.actualizacionContratos = actualizacionContratos;
    }

    // bi-directional many-to-one association to ActualizacionDocumentacion
    @OneToMany(mappedBy = "actualizacion")
    public List<ActualizacionDocumentacion> getActualizacionDocumentacions() {
        return this.actualizacionDocumentacions;
    }

    public void setActualizacionDocumentacions(
        List<ActualizacionDocumentacion> actualizacionDocumentacions) {
        this.actualizacionDocumentacions = actualizacionDocumentacions;
    }

    // bi-directional many-to-one association to ActualizacionDocumento
    @OneToMany(mappedBy = "actualizacion")
    public List<ActualizacionDocumento> getActualizacionDocumentos() {
        return this.actualizacionDocumentos;
    }

    public void setActualizacionDocumentos(
        List<ActualizacionDocumento> actualizacionDocumentos) {
        this.actualizacionDocumentos = actualizacionDocumentos;
    }

    // bi-directional many-to-one association to ActualizacionResponsable
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacion")
    public List<ActualizacionResponsable> getActualizacionResponsables() {
        return this.actualizacionResponsables;
    }

    public void setActualizacionResponsables(
        List<ActualizacionResponsable> actualizacionResponsables) {
        this.actualizacionResponsables = actualizacionResponsables;
    }

    // bi-directional many-to-one association to ActualizacionSedeComision
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacion")
    public List<ActualizacionSedeComision> getActualizacionSedeComisions() {
        return this.actualizacionSedeComisions;
    }

    public void setActualizacionSedeComisions(
        List<ActualizacionSedeComision> actualizacionSedeComisions) {
        this.actualizacionSedeComisions = actualizacionSedeComisions;
    }

    // bi-directional many-to-one association to Dotacion
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacion")
    public List<Dotacion> getDotacions() {
        return this.dotacions;
    }

    public void setDotacions(List<Dotacion> dotacions) {
        this.dotacions = dotacions;
    }

    // bi-directional many-to-one association to Convenio
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacion")
    public List<Convenio> getConvenios() {
        return this.convenios;
    }

    public void setConvenios(List<Convenio> convenios) {
        this.convenios = convenios;
    }

    // bi-directional many-to-one association to InformacionBasicaAgro
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacion")
    public List<InformacionBasicaAgro> getInformacionBasicaAgros() {
        return this.informacionBasicaAgros;
    }

    public void setInformacionBasicaAgros(
        List<InformacionBasicaAgro> informacionBasicaAgros) {
        this.informacionBasicaAgros = informacionBasicaAgros;
    }

    // bi-directional many-to-one association to InformacionBasicaCarto
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacion")
    public List<InformacionBasicaCarto> getInformacionBasicaCartos() {
        return this.informacionBasicaCartos;
    }

    public void setInformacionBasicaCartos(
        List<InformacionBasicaCarto> informacionBasicaCartos) {
        this.informacionBasicaCartos = informacionBasicaCartos;
    }

    // bi-directional many-to-one association to SaldoConservacion
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacion")
    public List<SaldoConservacion> getSaldoConservacions() {
        return this.saldoConservacions;
    }

    public void setSaldoConservacions(List<SaldoConservacion> saldoConservacions) {
        this.saldoConservacions = saldoConservacions;
    }

    // bi-directional many-to-one association to ActualizacionEvento
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacion")
    public List<ActualizacionEvento> getActualizacionEventos() {
        return this.actualizacionEventos;
    }

    public void setActualizacionEventos(
        List<ActualizacionEvento> actualizacionEventos) {
        this.actualizacionEventos = actualizacionEventos;
    }

    // bi-directional many-to-one association to ActualizacionComision
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacion")
    public List<ActualizacionComision> getActualizacionComisions() {
        return this.actualizacionComisions;
    }

    public void setActualizacionComisions(
        List<ActualizacionComision> actualizacionComisions) {
        this.actualizacionComisions = actualizacionComisions;
    }

    // bi-directional many-to-one association to GeneracionFormularioSbc
    @OneToMany(mappedBy = "actualizacion")
    public List<GeneracionFormularioSbc> getGeneracionFormularioSbcs() {
        return this.generacionFormularioSbcs;
    }

    public void setGeneracionFormularioSbcs(
        List<GeneracionFormularioSbc> generacionFormularioSbcs) {
        this.generacionFormularioSbcs = generacionFormularioSbcs;
    }

    // bi-directional many-to-one association to ActualizacionLevantamiento
    @OneToMany(mappedBy = "actualizacion")
    public List<ActualizacionLevantamiento> getActualizacionLevantamientos() {
        return this.actualizacionLevantamientos;
    }

    public void setActualizacionLevantamientos(
        List<ActualizacionLevantamiento> actualizacionLevantamientos) {
        this.actualizacionLevantamientos = actualizacionLevantamientos;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Actualizacion other = (Actualizacion) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Actualizacion [id=" + id + ", departamento=" + departamento +
            ", estado=" + estado + ", fecha=" + fecha + ", anio=" + anio +
            ", procesoInstanciaId=" + procesoInstanciaId +
            ", tipoProceso=" + tipoProceso + ", fechaLog=" + fechaLog +
            ", municipio=" + municipio + ", responsableDiagnostico=" +
            responsableDiagnostico + ", responsableDiagnosticoId=" +
            responsableDiagnosticoId + ", usuarioLog=" + usuarioLog + "]";
    }

}
