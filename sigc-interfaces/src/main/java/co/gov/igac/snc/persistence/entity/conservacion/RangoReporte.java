package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/*
 * Proyecto SNC 2017
 */
/**
 * Clase para describir los rangos usados en un reporte
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "REP_RANGO")
public class RangoReporte implements java.io.Serializable, Cloneable {

    private static final long serialVersionUID = 4262970936756309656L;

// Campos bd
    private Long id;

    private Long reporteEjecucionId;

    private String zona;

    private String tipo;

    private Double valorInicial;

    private Double valorFinal;

    private String usuarioLog;

    private Date fechaLog;

// Campos transient(Deben tener autor y descripcion)
    /**
     * default constructor
     */
    public RangoReporte() {
    }
// Metodos

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REP_RANGO_ID_SEQ_GEN")
    @SequenceGenerator(name = "REP_RANGO_ID_SEQ_GEN", sequenceName = "REP_RANGO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "REPORTE_EJECUCION_ID")
    public Long getReporteEjecucionId() {
        return reporteEjecucionId;
    }

    public void setReporteEjecucionId(Long ejecucionId) {
        this.reporteEjecucionId = ejecucionId;
    }

    @Column(name = "ZONA")
    public String getZona() {
        return this.zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @Column(name = "TIPO_RANGO")
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "VALOR_INICIAL")
    public Double getValorInicial() {
        return this.valorInicial;
    }

    public void setValorInicial(Double valorInicial) {
        this.valorInicial = valorInicial;
    }

    @Column(name = "VALOR_FINAL")
    public Double getValorFinal() {
        return this.valorFinal;
    }

    public void setValorFinal(Double valorFinal) {
        this.valorFinal = valorFinal;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

// Metodos transient(Deben tener autor y descripcion)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RangoReporte other = (RangoReporte) obj;
        if (!this.zona.equals(other.getZona()) &&
                !this.tipo.equals(other.getTipo()) &&
                !this.valorFinal.equals(other.getValorFinal()) &&
                !this.valorInicial.equals(other.getValorInicial())) {
            return false;
        }
        return true;
    }
}
