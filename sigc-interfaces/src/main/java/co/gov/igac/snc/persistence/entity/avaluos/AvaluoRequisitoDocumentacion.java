package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the AVALUO_REQUISITO_DOCUMENTACION database table.
 *
 */
@Entity
@Table(name = "AVALUO_REQUISITO_DOCUMENTACION")
public class AvaluoRequisitoDocumentacion implements Serializable {

    private static final long serialVersionUID = -552183663504129911L;
    private Long id;
    private Long avaluoId;
    private String descripcion;
    private Date fechaLog;
    private Long tipoDocumentoId;
    private String usuarioLog;

    public AvaluoRequisitoDocumentacion() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_REQUISITO_DOCUMENTACION_ID_GENERATOR", sequenceName =
        "AVALUO_REQUISITO_DOCUME_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_REQUISITO_DOCUMENTACION_ID_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AVALUO_ID")
    public Long getAvaluoId() {
        return this.avaluoId;
    }

    public void setAvaluoId(Long avaluoId) {
        this.avaluoId = avaluoId;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "TIPO_DOCUMENTO_ID")
    public Long getTipoDocumentoId() {
        return this.tipoDocumentoId;
    }

    public void setTipoDocumentoId(Long tipoDocumentoId) {
        this.tipoDocumentoId = tipoDocumentoId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
