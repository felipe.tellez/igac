package co.gov.igac.snc.persistence.entity.conservacion;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * HPersonaId entity. @author MyEclipse Persistence Tools
 */
@Entity
@Embeddable
public class HPersonaId implements java.io.Serializable {

    // Fields
    private Long HPredioId;
    @Id
    @GeneratedValue
    private Long id;

    // Constructors
    /** default constructor */
    public HPersonaId() {
    }

    /** full constructor */
    public HPersonaId(Long HPredioId, Long id) {
        this.HPredioId = HPredioId;
        this.id = id;
    }

    // Property accessors
    @Column(name = "H_PREDIO_ID", nullable = false, precision = 10, scale = 0)
    public Long getHPredioId() {
        return this.HPredioId;
    }

    public void setHPredioId(Long HPredioId) {
        this.HPredioId = HPredioId;
    }

    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof HPersonaId)) {
            return false;
        }
        HPersonaId castOther = (HPersonaId) other;

        return ((this.getHPredioId() == castOther.getHPredioId()) || (this
            .getHPredioId() != null &&
            castOther.getHPredioId() != null && this.getHPredioId()
            .equals(castOther.getHPredioId()))) &&
            ((this.getId() == castOther.getId()) || (this.getId() != null &&
            castOther.getId() != null && this.getId().equals(
            castOther.getId())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result +
            (getHPredioId() == null ? 0 : this.getHPredioId().hashCode());
        result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
        return result;
    }

}
