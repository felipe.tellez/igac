package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TramiteDocumento entity. @author MyEclipse Persistence Tools
 */
/*
 * @modified javier.aponte -> Se cambió el nombre del atributo fecha documento por fecha ya que en
 * la base de datos ahora se llama así, y todos los cambios correspondientes con este cambio de
 * nombre -> Se agregarón anotaciones correspondientes para que el id sea autogenerado @modified
 * david.cifuentes -> Se cambió la libreria java.sql.Date; a java.util.Date; -> Se modificó la
 * variable idRepositorioDocumentos de Long a String por cambio de tipo en la base de datos, se
 * actualizaron sus métodos get y set. -> Se modificó la variable documentoId de tipo Long a
 * documento de la clase Documento, así como tambien sus métodos get y set.
 *
 * @modified pedro.garcia campos agregados manualmente por cambios en la tabla
 *
 */
@Entity
@Table(name = "TRAMITE_DOCUMENTO", schema = "SNC_TRAMITE")
public class TramiteDocumento implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2186242433003311892L;
    private Long id;
    private Tramite tramite;
    private Date fecha;
    private Documento documento;
    private Date fechaNotificacion;
    private String idRepositorioDocumentos;
    private String metodoNotificacion;
    private String numeroDocumento;
    private String renunciaRecursos;
    private String personaNotificacion;
    private String observacionNotificacion;
    private String usuarioLog;
    private Date fechaLog;

    /**
     * campos agregados para manejo de notificaciones
     */
    private String tipoIdPersonaNotificacion;
    private String idPersonaNotificacion;
    private Long notificacionDocumentoId;
    private Long autorizacionDocumentoId;

    /**
     * campos agregados para fechas de fijación y desfijación de edictos
     */
    private Date fechaDesfijacionEdicto;
    private Date fechaFijacionEdicto;

    /**
     * agregados para guardar los datos del autorizado para la notificación
     */
    private String personaAutorizacion;
    private String tipoIdPersonaAutorizacion;
    //D: no es el id de Persona sino el documento de identificación
    private String idPersonaAutorizacion;

    // Constructors
    /** default constructor */
    public TramiteDocumento() {
    }

    /** minimal constructor */
    public TramiteDocumento(Long id, Date fecha,
        Documento documento, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.fecha = fecha;
        this.documento = documento;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramiteDocumento(Long id, Tramite tramite, Date fecha,
        Documento documento,
        Date fechaNotificacion, String metodoNotificacion,
        String numeroDocumento, String renunciaRecursos, String personaNotificacion,
        String observacionNotificacion, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.fecha = fecha;
        this.documento = documento;
        this.fechaNotificacion = fechaNotificacion;
        this.metodoNotificacion = metodoNotificacion;
        this.renunciaRecursos = renunciaRecursos;
        this.numeroDocumento = numeroDocumento;
        this.personaNotificacion = personaNotificacion;
        this.observacionNotificacion = observacionNotificacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_DOCUMENTO_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_DOCUMENTO_ID_SEQ", sequenceName = "TRAMITE_DOCUMENTO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID")
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "FECHA", nullable = false, length = 7)
    public Date getfecha() {
        return this.fecha;
    }

    // quien fue el de estas????? eso pasa por escribir getters y setters
    // a mano debe ser setFecha y aplica para el getter tambien
    public void setfecha(Date fecha) {
        this.fecha = fecha;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = false)
    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documentoId) {
        this.documento = documentoId;
    }

    @Column(name = "FECHA_NOTIFICACION", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaNotificacion() {
        return this.fechaNotificacion;
    }

    public void setFechaNotificacion(Date fechaNotificacion) {
        this.fechaNotificacion = fechaNotificacion;
    }

    @Column(name = "ID_REPOSITORIO_DOCUMENTOS")
    public String getIdRepositorioDocumentos() {
        return this.idRepositorioDocumentos;
    }

    public void setIdRepositorioDocumentos(String idRepositorioDocumentos) {
        this.idRepositorioDocumentos = idRepositorioDocumentos;
    }

    @Column(name = "METODO_NOTIFICACION", length = 30)
    public String getMetodoNotificacion() {
        return this.metodoNotificacion;
    }

    public void setMetodoNotificacion(String metodoNotificacion) {
        this.metodoNotificacion = metodoNotificacion;
    }

    @Column(name = "RENUNCIA_RECURSOS", length = 2)
    public String getRenunciaRecursos() {
        return this.renunciaRecursos;
    }

    public void setRenunciaRecursos(String renunciaRecursos) {
        this.renunciaRecursos = renunciaRecursos;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(TemporalType.DATE)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NUMERO_DOCUMENTO", length = 20)
    public String getNumeroDocumento() {
        return this.numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }
//--------------

    @Column(name = "PERSONA_NOTIFICACION", length = 100)
    public String getPersonaNotificacion() {
        return this.personaNotificacion;
    }

    public void setPersonaNotificacion(String personaNotificacion) {
        this.personaNotificacion = personaNotificacion;
    }

    @Column(name = "OBSERVACION_NOTIFICACION", length = 600)
    public String getObservacionNotificacion() {
        return this.observacionNotificacion;
    }

    public void setObservacionNotificacion(String observacionNotificacion) {
        this.observacionNotificacion = observacionNotificacion;
    }

//--------------
    @Column(name = "TIPO_ID_PERSONA_NOTIFICACION", length = 30)
    public String getTipoIdPersonaNotificacion() {
        return this.tipoIdPersonaNotificacion;
    }

    public void setTipoIdPersonaNotificacion(String tipoIdPersonaNotificacion) {
        this.tipoIdPersonaNotificacion = tipoIdPersonaNotificacion;
    }

    @Column(name = "ID_PERSONA_NOTIFICACION", length = 50)
    public String getIdPersonaNotificacion() {
        return this.idPersonaNotificacion;
    }

    public void setIdPersonaNotificacion(String idPersonaNotificacion) {
        this.idPersonaNotificacion = idPersonaNotificacion;
    }

    @Column(name = "NOTIFICACION_DOCUMENTO_ID")
    public Long getNotificacionDocumentoId() {
        return this.notificacionDocumentoId;
    }

    public void setNotificacionDocumentoId(Long notificacionDocumentoId) {
        this.notificacionDocumentoId = notificacionDocumentoId;
    }

    @Column(name = "AUTORIZACION_DOCUMENTO_ID")
    public Long getAutorizacionDocumentoId() {
        return this.autorizacionDocumentoId;
    }

    public void setAutorizacionDocumentoId(Long autorizacionDocumentoId) {
        this.autorizacionDocumentoId = autorizacionDocumentoId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_DESFIJACION_EDICTO")
    public Date getFechaDesfijacionEdicto() {
        return this.fechaDesfijacionEdicto;
    }

    public void setFechaDesfijacionEdicto(Date fechaDesfijacionEdicto) {
        this.fechaDesfijacionEdicto = fechaDesfijacionEdicto;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIJACION_EDICTO")
    public Date getFechaFijacionEdicto() {
        return this.fechaFijacionEdicto;
    }

    public void setFechaFijacionEdicto(Date fechaFijacionEdicto) {
        this.fechaFijacionEdicto = fechaFijacionEdicto;
    }

    @Column(name = "TIPO_ID_PERSONA_AUTORIZACION")
    public String getTipoIdPersonaAutorizacion() {
        return this.tipoIdPersonaAutorizacion;
    }

    public void setTipoIdPersonaAutorizacion(String tipoIdPersonaAutorizacion) {
        this.tipoIdPersonaAutorizacion = tipoIdPersonaAutorizacion;
    }

    @Column(name = "ID_PERSONA_AUTORIZACION")
    public String getIdPersonaAutorizacion() {
        return this.idPersonaAutorizacion;
    }

    public void setIdPersonaAutorizacion(String idPersonaAutorizacion) {
        this.idPersonaAutorizacion = idPersonaAutorizacion;
    }

    @Column(name = "PERSONA_AUTORIZACION")
    public String getPersonaAutorizacion() {
        return this.personaAutorizacion;
    }

    public void setPersonaAutorizacion(String personaAutorizacion) {
        this.personaAutorizacion = personaAutorizacion;
    }

    /**
     * la comparación para saber si son iguales la hace solo por el id
     */
    /*
     * @documented pedro.garcia
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof TramiteDocumento) {
            TramiteDocumento td = (TramiteDocumento) o;
            if (this.id != null && td.getId() != null) {
                return this.id.equals(td.getId());
            }
            return false;
        }
        return false;
    }

}
