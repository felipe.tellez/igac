package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the PREDIO_PROPIETARIO_HIS database table.
 *
 */
@Entity
@NamedQueries(
    {
        @NamedQuery(name = "findPredioPropietarioHisByPredio", query =
            "from PredioPropietarioHis pph LEFT JOIN FETCH pph.municipio LEFT JOIN FETCH pph.departamento WHERE pph.predio.id = :predioId")
    }
)
@Table(name = "PREDIO_PROPIETARIO_HIS")
public class PredioPropietarioHis implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String digitoVerificacion;
    private String estadoCivil;
    private Date fechaLog;
    private Date fechaRadicacion;
    private String numeroIdentificacion;
    private String numeroRadicacion;
    private BigDecimal porcentajePropiedad;
    private Predio predio;
    private String primerApellido;
    private String primerNombre;
    private String razonSocial;
    private String segundoApellido;
    private String segundoNombre;
    private String sigla;
    private String tipoIdentificacion;
    private String tipoTramite;
    private Departamento departamento;
    private Date tituloFecha;
    private String tituloModoAdquisicion;
    private Municipio municipio;
    private String tituloNotaria;
    private String tituloNumero;
    private BigDecimal tituloValorCompra;
    private BigDecimal tramiteId;
    private String usuarioLog;

    public PredioPropietarioHis() {
    }

    @Id
    @SequenceGenerator(name = "PREDIO_PROPIETARIO_HIS_ID_GENERATOR", sequenceName =
        "PREDIO_PROPIETARIO_HIS_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "PREDIO_PROPIETARIO_HIS_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DIGITO_VERIFICACION")
    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    @Column(name = "ESTADO_CIVIL")
    public String getEstadoCivil() {
        return this.estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RADICACION")
    public Date getFechaRadicacion() {
        return this.fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    @Column(name = "NUMERO_IDENTIFICACION")
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "NUMERO_RADICACION")
    public String getNumeroRadicacion() {
        return this.numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    @Column(name = "PORCENTAJE_PROPIEDAD")
    public BigDecimal getPorcentajePropiedad() {
        return this.porcentajePropiedad;
    }

    public void setPorcentajePropiedad(BigDecimal porcentajePropiedad) {
        this.porcentajePropiedad = porcentajePropiedad;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID")
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "PRIMER_APELLIDO")
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "PRIMER_NOMBRE")
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Column(name = "RAZON_SOCIAL")
    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Column(name = "SEGUNDO_APELLIDO")
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "SEGUNDO_NOMBRE")
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Column(name = "TIPO_IDENTIFICACION")
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "TIPO_TRAMITE")
    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TITULO_DEPARTAMENTO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "TITULO_FECHA")
    public Date getTituloFecha() {
        return this.tituloFecha;
    }

    public void setTituloFecha(Date tituloFecha) {
        this.tituloFecha = tituloFecha;
    }

    @Column(name = "TITULO_MODO_ADQUISICION")
    public String getTituloModoAdquisicion() {
        return this.tituloModoAdquisicion;
    }

    public void setTituloModoAdquisicion(String tituloModoAdquisicion) {
        this.tituloModoAdquisicion = tituloModoAdquisicion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TITULO_MUNICIPIO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "TITULO_NOTARIA")
    public String getTituloNotaria() {
        return this.tituloNotaria;
    }

    public void setTituloNotaria(String tituloNotaria) {
        this.tituloNotaria = tituloNotaria;
    }

    @Column(name = "TITULO_NUMERO")
    public String getTituloNumero() {
        return this.tituloNumero;
    }

    public void setTituloNumero(String tituloNumero) {
        this.tituloNumero = tituloNumero;
    }

    @Column(name = "TITULO_VALOR_COMPRA")
    public BigDecimal getTituloValorCompra() {
        return this.tituloValorCompra;
    }

    public void setTituloValorCompra(BigDecimal tituloValorCompra) {
        this.tituloValorCompra = tituloValorCompra;
    }

    @Column(name = "TRAMITE_ID")
    public BigDecimal getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(BigDecimal tramiteId) {
        this.tramiteId = tramiteId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Transient
    public String getNombre() {
        String name = new String();

        if (this.tipoIdentificacion.equals("NIT") || (this.razonSocial != null && !this.razonSocial.
            equals(""))) {
            name = this.razonSocial;
        } else {

            name = this.primerNombre;
            if (this.segundoNombre != null) {
                name = name.concat(" ").concat(this.segundoNombre);
            }
            if (this.primerApellido != null) {
                name = name.concat(" ").concat(this.primerApellido);
            }
            if (this.segundoApellido != null) {
                name = name.concat(" ").concat(this.segundoApellido);
            }
        }
        return name != null ? name.trim() : "";
    }

}
