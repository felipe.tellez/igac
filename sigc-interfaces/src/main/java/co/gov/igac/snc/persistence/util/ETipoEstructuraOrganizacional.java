package co.gov.igac.snc.persistence.util;

/**
 *
 * @author wilmanjose.vega
 */
public enum ETipoEstructuraOrganizacional {
    DIRECCION("DIRECCION"),
    DELEGACION("DELEGACION"),
    SUBDIRECCION("SUBDIRECCION"),
    DIRECCION_TERRITORIAL("DIRECCION TERRITORIAL"),
    UOC("UOC"),
    GIT("GIT"),
    HABILITADA("HABILITADA"),
    OFICINA_APOYO("OFICINA DE APOYO");

    private String codigo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    private ETipoEstructuraOrganizacional(String codigo) {
        this.codigo = codigo;
    }

}
