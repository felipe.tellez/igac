package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * The persistent class for the COMISION_OFERTA database table.
 *
 * @modified pedro.garcia 20-04-2012 - cambio de tipo de dato del id: de long a Long - cambio de
 * tipo de datos del campo 'duracion' de BigDecimal a Double - cambio de tipo de datos del campo
 * 'oficioDocumentoId' de BigDecimal a Long
 */
@Entity
@Table(name = "COMISION_OFERTA")
public class ComisionOferta implements Serializable {

    private static final long serialVersionUID = 22221L;

    private Long id;
    private Double duracion;
    private String estado;
    private String estructuraOrganizacionalCod;
    private Date fechaFin;
    private Date fechaInicio;
    private Date fechaLog;
    private String numero;
    private String objeto;
    private Documento oficioDocumento;
    private String usuarioLog;
    private List<RegionCapturaOferta> regionCapturaOfertas;

    public ComisionOferta() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comision_oferta_ID_SEQ")
    @SequenceGenerator(name = "comision_oferta_ID_SEQ",
        sequenceName = "COMISION_OFERTA_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDuracion() {
        return this.duracion;
    }

    public void setDuracion(Double duracion) {
        this.duracion = duracion;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD")
    public String getEstructuraOrganizacionalCod() {
        return this.estructuraOrganizacionalCod;
    }

    public void setEstructuraOrganizacionalCod(String estructuraOrganizacionalCod) {
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getObjeto() {
        return this.objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OFICIO_DOCUMENTO_ID", nullable = false)
    public Documento getOficioDocumento() {
        return this.oficioDocumento;
    }

    public void setOficioDocumento(Documento oficioDocumento) {
        this.oficioDocumento = oficioDocumento;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to RegionCapturaOferta
    @OneToMany(mappedBy = "comisionOferta")
    public List<RegionCapturaOferta> getRegionCapturaOfertas() {
        return this.regionCapturaOfertas;
    }

    public void setRegionCapturaOfertas(List<RegionCapturaOferta> regionCapturaOfertas) {
        this.regionCapturaOfertas = regionCapturaOfertas;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ComisionOferta other = (ComisionOferta) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
