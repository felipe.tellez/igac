/*
 *Proyecto SNC
 */
package co.gov.igac.snc.ldap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author felipe.cadena
 */
public enum ELDAPEstadoUsuario {

    ACTIVO("512,66080,66048"),
    NO_EXISTE_LDAP("Usuario inexistente en el LDAP");

    private final List<String> codigo;
    private final String value;

    ELDAPEstadoUsuario(String value) {

        String[] codes = value.split(",");

        this.codigo = new ArrayList<String>();
        this.codigo.addAll(Arrays.asList(codes));
        this.value = value;

    }

    /**
     * Determina si el codigo de usuario dado esta contenido en los valores para la enumeracion
     * seleccionada
     *
     * @param code
     * @return
     */
    public boolean codeExist(String code) {

        for (String string : this.codigo) {
            if (code.equals(string)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Se usa para estados que se componen de un conjunto de codigos
     *
     * @return
     */
    public List<String> getCodigo() {
        return codigo;
    }

    /**
     * Se usa para estados que solo se componen de un string
     *
     * @return
     */
    public String getValue() {
        return this.value;
    }

}
