package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the FOTOGRAFIA database table.
 *
 * @modified juan.agudelo 02-11-11 Generación de la entidad y adición de la secuencia
 * FOTOGRAFIA_ID_SEQ juan.agudelo 22-11-11 Métodos transient para fotografiaSeleccionada
 * juan.agudelo 20-03-12 Adición del atributo transient nombreDescargaFotoAlfresco el cual sirve
 * para almacenar la dirección de la fotografia cuando es consultada desde alfresco en la carpeta
 * compartida del proyecto
 */
@Entity
@Table(name = "FOTOGRAFIA")
public class Fotografia implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8428769132398892406L;
    private Long id;
    private String archivoFotografia;
    private Long avaluoPredioId;
    private String descripcion;
    private Date fecha;
    private Date fechaLog;
    private String idRepositorioDocumentos;
    private String tipoFoto;
    private String usuarioLog;
    private OfertaInmobiliaria ofertaInmobiliaria;

    // Transient
    private boolean fotografiaSeleccionada;
    private String nombreDescargaFotoAlfresco;

    public Fotografia() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FOTOGRAFIA_ID_SEQ")
    @SequenceGenerator(name = "FOTOGRAFIA_ID_SEQ", sequenceName = "FOTOGRAFIA_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ARCHIVO_FOTOGRAFIA")
    public String getArchivoFotografia() {
        return this.archivoFotografia;
    }

    public void setArchivoFotografia(String archivoFotografia) {
        this.archivoFotografia = archivoFotografia;
    }

    @Column(name = "AVALUO_PREDIO_ID")
    public Long getAvaluoPredioId() {
        return this.avaluoPredioId;
    }

    public void setAvaluoPredioId(Long avaluoPredioId) {
        this.avaluoPredioId = avaluoPredioId;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "ID_REPOSITORIO_DOCUMENTOS")
    public String getIdRepositorioDocumentos() {
        return this.idRepositorioDocumentos;
    }

    public void setIdRepositorioDocumentos(String idRepositorioDocumentos) {
        this.idRepositorioDocumentos = idRepositorioDocumentos;
    }

    @Column(name = "TIPO_FOTO")
    public String getTipoFoto() {
        return this.tipoFoto;
    }

    public void setTipoFoto(String tipoFoto) {
        this.tipoFoto = tipoFoto;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to OfertaInmobiliaria
    @ManyToOne(fetch = FetchType.LAZY)
    // @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OFERTA_INMOBILIARIA_ID")
    public OfertaInmobiliaria getOfertaInmobiliaria() {
        return this.ofertaInmobiliaria;
    }

    public void setOfertaInmobiliaria(OfertaInmobiliaria ofertaInmobiliaria) {
        this.ofertaInmobiliaria = ofertaInmobiliaria;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Transient
    public boolean isFotografiaSeleccionada() {
        return fotografiaSeleccionada;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    public void setFotografiaSeleccionada(boolean fotografiaSeleccionada) {
        this.fotografiaSeleccionada = fotografiaSeleccionada;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    @Transient
    public String getNombreDescargaFotoAlfresco() {
        return nombreDescargaFotoAlfresco;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author juan.agudelo
     */
    public void setNombreDescargaFotoAlfresco(String nombreDescargaFotoAlfresco) {
        this.nombreDescargaFotoAlfresco = nombreDescargaFotoAlfresco;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Fotografia other = (Fotografia) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
