package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Rango entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "RANGO", schema = "SNC_CONSERVACION")
public class Rango implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private String nombre;
    private Double inicio;
    private Double fin;
    private String descripcion;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public Rango() {
    }

    /** minimal constructor */
    public Rango(Long id, String nombre, Double inicio, Double fin,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.inicio = inicio;
        this.fin = fin;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Rango(Long id, String nombre, Double inicio, Double fin,
        String descripcion, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.inicio = inicio;
        this.fin = fin;
        this.descripcion = descripcion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "INICIO", nullable = false, precision = 12)
    public Double getInicio() {
        return this.inicio;
    }

    public void setInicio(Double inicio) {
        this.inicio = inicio;
    }

    @Column(name = "FIN", nullable = false, precision = 12)
    public Double getFin() {
        return this.fin;
    }

    public void setFin(Double fin) {
        this.fin = fin;
    }

    @Column(name = "DESCRIPCION", length = 250)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
