/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para manejar los valores del ominio AVALUO_AREA_REG_TIPO
 *
 *
 * @author felipe.cadena
 */
public enum EAvaluoAreaRegTipo {

    TERRENO("TERRENO", "Terreno"),
    CONSTRUCCION("CONSTRUCCION", "Construcción");

    private String codigo;
    private String valor;

    private EAvaluoAreaRegTipo(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
