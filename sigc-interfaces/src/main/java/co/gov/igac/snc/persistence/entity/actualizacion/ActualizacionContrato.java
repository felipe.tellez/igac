package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ACTUALIZACION_CONTRATO database table.
 *
 */
@Entity
@Table(name = "ACTUALIZACION_CONTRATO")
public class ActualizacionContrato implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -349077797811503169L;
    private Long id;
    private String actividad;
    private String contratistaIdentificacion;
    private String contratistaNombre;
    private Date fechaLog;
    private String interventorIdentificacion;
    private String interventorNombre;
    private String usuarioLog;
    private Double valorMensual;
    private Double valorTotal;
    private Actualizacion actualizacion;

    public ActualizacionContrato() {
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_CONTRATO_ID_GENERATOR", sequenceName =
        "ACTUALIZACION_CONTRATO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_CONTRATO_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActividad() {
        return this.actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    @Column(name = "CONTRATISTA_IDENTIFICACION")
    public String getContratistaIdentificacion() {
        return this.contratistaIdentificacion;
    }

    public void setContratistaIdentificacion(String contratistaIdentificacion) {
        this.contratistaIdentificacion = contratistaIdentificacion;
    }

    @Column(name = "CONTRATISTA_NOMBRE")
    public String getContratistaNombre() {
        return this.contratistaNombre;
    }

    public void setContratistaNombre(String contratistaNombre) {
        this.contratistaNombre = contratistaNombre;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "INTERVENTOR_IDENTIFICACION")
    public String getInterventorIdentificacion() {
        return this.interventorIdentificacion;
    }

    public void setInterventorIdentificacion(String interventorIdentificacion) {
        this.interventorIdentificacion = interventorIdentificacion;
    }

    @Column(name = "INTERVENTOR_NOMBRE")
    public String getInterventorNombre() {
        return this.interventorNombre;
    }

    public void setInterventorNombre(String interventorNombre) {
        this.interventorNombre = interventorNombre;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_MENSUAL")
    public Double getValorMensual() {
        return this.valorMensual;
    }

    public void setValorMensual(Double valorMensual) {
        this.valorMensual = valorMensual;
    }

    @Column(name = "VALOR_TOTAL")
    public Double getValorTotal() {
        return this.valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    // bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

}
