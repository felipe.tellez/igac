package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;

/**
 * The persistent class for the TMP_TRAMITE_SESSION database table.
 *
 */
@Embeddable
public class TmpTramiteSessionId implements Serializable {

    private static final long serialVersionUID = 1L;

    private BigDecimal sesionId;
    private BigDecimal tramiteId;

    public TmpTramiteSessionId() {
    }

    @Column(name = "SESION_ID")
    public BigDecimal getSesionId() {
        return this.sesionId;
    }

    public void setSesionId(BigDecimal sesionId) {
        this.sesionId = sesionId;
    }

    @Column(name = "TRAMITE_ID")
    public BigDecimal getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(BigDecimal tramiteId) {
        this.tramiteId = tramiteId;
    }

}
