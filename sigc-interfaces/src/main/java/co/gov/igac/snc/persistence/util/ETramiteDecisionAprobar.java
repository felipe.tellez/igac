/**
 * < * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con las posibles decisiones sobre la aprobacion de un nuevo tramite
 *
 * @author franz.gamba
 */
public enum ETramiteDecisionAprobar {
    APROBAR("Aprobar"),
    RECHAZAR("Rechazar");

    private String codigo;

    private ETramiteDecisionAprobar(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
