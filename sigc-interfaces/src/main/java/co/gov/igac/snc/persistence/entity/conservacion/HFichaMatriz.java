package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the H_FICHA_MATRIZ database table.
 *
 * @modified david.cifuentes :: Adición de la variable totalUnidadesSotanos :: Cambio de las
 * variables de tipo BigDecimal a Double :: Cambio de la variable predioId a Predio :: 19/07/12
 */
@Entity
@Table(name = "H_FICHA_MATRIZ")
public class HFichaMatriz implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8868082044120704937L;

    private HFichaMatrizPK id;
    private Double areaTotalConstruidaComun;
    private Double areaTotalConstruidaPrivada;
    private Double areaTotalTerrenoComun;
    private Double areaTotalTerrenoPrivada;
    private String cancelaInscribe;
    private Date fechaLog;
    private Predio predio;
    private Long totalUnidadesPrivadas;
    private String usuarioLog;
    private Double valorTotalAvaluoCatastral;
    private List<HFichaMatrizModelo> HFichaMatrizModelos;
    private List<HFichaMatrizPredio> HFichaMatrizPredios;
    private List<HFichaMatrizTorre> HFichaMatrizTorres;
    private HPredio HPredio;

    private Long totalUnidadesSotanos;

    private String estado;

    public HFichaMatriz() {
    }

    @EmbeddedId
    public HFichaMatrizPK getId() {
        return this.id;
    }

    public void setId(HFichaMatrizPK id) {
        this.id = id;
    }

    @Column(name = "AREA_TOTAL_CONSTRUIDA_COMUN")
    public Double getAreaTotalConstruidaComun() {
        return this.areaTotalConstruidaComun;
    }

    public void setAreaTotalConstruidaComun(Double areaTotalConstruidaComun) {
        this.areaTotalConstruidaComun = areaTotalConstruidaComun;
    }

    @Column(name = "AREA_TOTAL_CONSTRUIDA_PRIVADA")
    public Double getAreaTotalConstruidaPrivada() {
        return this.areaTotalConstruidaPrivada;
    }

    public void setAreaTotalConstruidaPrivada(Double areaTotalConstruidaPrivada) {
        this.areaTotalConstruidaPrivada = areaTotalConstruidaPrivada;
    }

    @Column(name = "AREA_TOTAL_TERRENO_COMUN")
    public Double getAreaTotalTerrenoComun() {
        return this.areaTotalTerrenoComun;
    }

    public void setAreaTotalTerrenoComun(Double areaTotalTerrenoComun) {
        this.areaTotalTerrenoComun = areaTotalTerrenoComun;
    }

    @Column(name = "AREA_TOTAL_TERRENO_PRIVADA")
    public Double getAreaTotalTerrenoPrivada() {
        return this.areaTotalTerrenoPrivada;
    }

    public void setAreaTotalTerrenoPrivada(Double areaTotalTerrenoPrivada) {
        this.areaTotalTerrenoPrivada = areaTotalTerrenoPrivada;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "TOTAL_UNIDADES_PRIVADAS")
    public Long getTotalUnidadesPrivadas() {
        return this.totalUnidadesPrivadas;
    }

    public void setTotalUnidadesPrivadas(Long totalUnidadesPrivadas) {
        this.totalUnidadesPrivadas = totalUnidadesPrivadas;
    }

    @Column(name = "TOTAL_UNIDADES_SOTANOS", nullable = false, precision = 6)
    public Long getTotalUnidadesSotanos() {
        return this.totalUnidadesSotanos;
    }

    public void setTotalUnidadesSotanos(Long totalUnidadesSotanos) {
        this.totalUnidadesSotanos = totalUnidadesSotanos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_CATASTRAL")
    public Double getValorTotalAvaluoCatastral() {
        return this.valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(Double valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HFichaMatriz")
    public List<HFichaMatrizModelo> getHFichaMatrizModelos() {
        return this.HFichaMatrizModelos;
    }

    public void setHFichaMatrizModelos(List<HFichaMatrizModelo> hFichaMatrizModelos) {
        this.HFichaMatrizModelos = hFichaMatrizModelos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HFichaMatriz")
    public List<HFichaMatrizPredio> getHFichaMatrizPredios() {
        return this.HFichaMatrizPredios;
    }

    public void setHFichaMatrizPredios(List<HFichaMatrizPredio> hFichaMatrizPredios) {
        this.HFichaMatrizPredios = hFichaMatrizPredios;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HFichaMatriz")
    public List<HFichaMatrizTorre> getHFichaMatrizTorres() {
        return this.HFichaMatrizTorres;
    }

    public void setHFichaMatrizTorres(List<HFichaMatrizTorre> hFichaMatrizTorres) {
        this.HFichaMatrizTorres = hFichaMatrizTorres;
    }

    @ManyToOne
    @JoinColumn(name = "H_PREDIO_ID", insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
