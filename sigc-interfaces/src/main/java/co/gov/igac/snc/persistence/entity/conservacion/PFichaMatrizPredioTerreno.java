package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/*
 * Proyecto SNC 2016
 */
/**
 * Clase para representar las proyecciones de la entidad FICHA_MATRIZ_PREDIO_TERRENO
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "P_FICHA_MATRIZ_PREDIO_TERRENO")
public class PFichaMatrizPredioTerreno implements java.io.Serializable, Cloneable {

    private static final long serialVersionUID = 4262970936756309656L;

// Campos bd
    private Long id;

    private PFichaMatriz fichaMatriz;

    private Predio predio;

    private String cancelaInscribe;

    private String estado;

    private String usuarioLog;

    private Date fechaLog;

    private Tramite tramite;

    //Campos asociados al origen
    private String numeroPredialOrigen;
    private String direccionPrincipalOrigen;
    private String matriculaOrigen;
    private Double areaTotalTerrenoOrigen;
    private Double areaConstruidaPrivadaOrigen;
    private Double areaConstruidaComunOrigen;

// Campos transient(Deben tener autor y descripcion)
    /**
     * default constructor
     */
    public PFichaMatrizPredioTerreno() {
    }
// Metodos

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FM_PREDIO_TERRENO_ID_SEQ_GEN")
    @SequenceGenerator(name = "FM_PREDIO_TERRENO_ID_SEQ_GEN", sequenceName =
        "FM_PREDIO_TERRENO_ID_SEQ", allocationSize = 1)
    @Column(name = "ID")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FICHA_MATRIZ_ID")
    public PFichaMatriz getFichaMatriz() {
        return this.fichaMatriz;
    }

    public void setFichaMatriz(PFichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID")
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "ESTADO")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID")
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "NUMERO_PREDIAL_ORIGEN")
    public String getNumeroPredialOrigen() {
        return numeroPredialOrigen;
    }

    public void setNumeroPredialOrigen(String numeroPredialOrigen) {
        this.numeroPredialOrigen = numeroPredialOrigen;
    }

    @Column(name = "DIRECCION_PPAL_ORIGEN")
    public String getDireccionPrincipalOrigen() {
        return direccionPrincipalOrigen;
    }

    public void setDireccionPrincipalOrigen(String direccionPrincipalOrigen) {
        this.direccionPrincipalOrigen = direccionPrincipalOrigen;
    }

    @Column(name = "MATRICULA_INMOB_ORIGEN")
    public String getMatriculaOrigen() {
        return matriculaOrigen;
    }

    public void setMatriculaOrigen(String matriculaOrigen) {
        this.matriculaOrigen = matriculaOrigen;
    }

    @Column(name = "AREA_TOTAL_TERRENO_ORIGEN")
    public Double getAreaTotalTerrenoOrigen() {
        return areaTotalTerrenoOrigen;
    }

    public void setAreaTotalTerrenoOrigen(Double areaTotalTerrenoOrigen) {
        this.areaTotalTerrenoOrigen = areaTotalTerrenoOrigen;
    }

    @Column(name = "AREA_CONSTRUIDA_PRIVADA_ORIGEN")
    public Double getAreaConstruidaPrivadaOrigen() {
        return areaConstruidaPrivadaOrigen;
    }

    public void setAreaConstruidaPrivadaOrigen(Double areaConstruidaPrivadaOrigen) {
        this.areaConstruidaPrivadaOrigen = areaConstruidaPrivadaOrigen;
    }

    @Column(name = "AREA_CONSTRUIDA_COMUN_ORIGEN")
    public Double getAreaConstruidaComunOrigen() {
        return areaConstruidaComunOrigen;
    }

    public void setAreaConstruidaComunOrigen(Double areaConstruidaComunOrigen) {
        this.areaConstruidaComunOrigen = areaConstruidaComunOrigen;
    }

// Metodos transient(Deben tener autor y descripcion)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PFichaMatrizPredioTerreno other = (PFichaMatrizPredioTerreno) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
