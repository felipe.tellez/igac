package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;

/**
 * The persistent class for the REP_TERRITORIAL_REPORTE database table.
 *
 */
@Entity
@Table(name = "REP_TERRITORIAL_REPORTE")
@NamedQuery(name = "RepTerritorialReporte.findAll", query = "SELECT r FROM RepTerritorialReporte r")
public class RepTerritorialReporte implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private EstructuraOrganizacional territorial;
    private EstructuraOrganizacional UOC;
    private Departamento departamento;
    private Municipio municipio;
    private Date fechaLog;
    private String usuarioLog;
    private RepUsuarioRolReporte repUsuarioRolReporte;

    public RepTerritorialReporte() {
    }

    // Property accessors
    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REP_TERRITORIAL_REPORTE_ID_SEQ")
    @SequenceGenerator(name = "REP_TERRITORIAL_REPORTE_ID_SEQ", sequenceName =
        "REP_TERRITORIAL_REPORTE_ID_SEQ", allocationSize = 0)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO")
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO")
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TERRITORIAL_CODIGO", nullable = false)
    public EstructuraOrganizacional getTerritorial() {
        return this.territorial;
    }

    public void setTerritorial(EstructuraOrganizacional territorial) {
        this.territorial = territorial;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UOC_CODIGO")
    public EstructuraOrganizacional getUOC() {
        return this.UOC;
    }

    public void setUOC(EstructuraOrganizacional uoc) {
        this.UOC = uoc;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to RepUsuarioRolReporte
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USUARIO_ROL_REPORTE_ID")
    public RepUsuarioRolReporte getRepUsuarioRolReporte() {
        return this.repUsuarioRolReporte;
    }

    public void setRepUsuarioRolReporte(RepUsuarioRolReporte repUsuarioRolReporte) {
        this.repUsuarioRolReporte = repUsuarioRolReporte;
    }

    /**
     * Método que realiza un clone del objeto {@link RepTerritorialReporte}.
     *
     * @author david.cifuentes
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

}
