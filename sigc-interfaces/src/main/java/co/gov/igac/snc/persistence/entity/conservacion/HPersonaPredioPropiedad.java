package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import javax.persistence.JoinColumns;

/**
 * HPersonaPredioPropiedad entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "H_PERSONA_PREDIO_PROPIEDAD", schema = "SNC_CONSERVACION")
public class HPersonaPredioPropiedad implements Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -417804469624975421L;
    protected HPersonaPredioPropiedadId id;
    private HPersonaPredio HPersonaPredio;
    private String tipo;
    private String entidadEmisora;
    private Departamento departamento;
    private Municipio municipio;
    private String falsaTradicion;
    private Date fechaRegistro;
    private String numeroRegistro;
    private String modoAdquisicion;
    private Double valor;
    private String tipoTitulo;
    private String numeroTitulo;
    private Date fechaTitulo;
    private String libro;
    private String tomo;
    private String pagina;
    private String justificacion;
    private Documento documentoSoporte;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;

    private HPredio HPredio;

    // Constructors
    /** default constructor */
    public HPersonaPredioPropiedad() {
    }

    /** minimal constructor */
    public HPersonaPredioPropiedad(HPersonaPredioPropiedadId id, HPersonaPredio HPersonaPredio,
        String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.HPersonaPredio = HPersonaPredio;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public HPersonaPredioPropiedad(HPersonaPredioPropiedadId id, HPersonaPredio HPersonaPredio,
        String tipo,
        String entidadEmisora, Departamento departamento,
        Municipio municipio, String falsaTradicion,
        Date fechaRegistro, String numeroRegistro,
        String modoAdquisicion, Double valor, String tipoTitulo,
        String numeroTitulo, Date fechaTitulo, String libro,
        String tomo, String pagina, String justificacion,
        Documento documentoSoporte, String cancelaInscribe, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.HPersonaPredio = HPersonaPredio;
        this.tipo = tipo;
        this.entidadEmisora = entidadEmisora;
        this.departamento = departamento;
        this.municipio = municipio;
        this.falsaTradicion = falsaTradicion;
        this.fechaRegistro = fechaRegistro;
        this.numeroRegistro = numeroRegistro;
        this.modoAdquisicion = modoAdquisicion;
        this.valor = valor;
        this.tipoTitulo = tipoTitulo;
        this.numeroTitulo = numeroTitulo;
        this.fechaTitulo = fechaTitulo;
        this.libro = libro;
        this.tomo = tomo;
        this.pagina = pagina;
        this.justificacion = justificacion;
        this.documentoSoporte = documentoSoporte;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @EmbeddedId
    public HPersonaPredioPropiedadId getId() {
        return this.id;
    }

    public void setId(HPersonaPredioPropiedadId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "H_PREDIO_ID", referencedColumnName = "H_PREDIO_ID", nullable = false,
            insertable = false, updatable = false),
        @JoinColumn(name = "PERSONA_PREDIO_ID", referencedColumnName = "ID", nullable = false,
            insertable = false, updatable = false)})
    public HPersonaPredio getHPersonaPredio() {
        return HPersonaPredio;
    }

    public void setHPersonaPredio(HPersonaPredio HPersonaPredio) {
        this.HPersonaPredio = HPersonaPredio;
    }

    @Column(name = "TIPO", length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "ENTIDAD_EMISORA", length = 100)
    public String getEntidadEmisora() {
        return this.entidadEmisora;
    }

    public void setEntidadEmisora(String entidadEmisora) {
        this.entidadEmisora = entidadEmisora;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "FALSA_TRADICION", length = 2)
    public String getFalsaTradicion() {
        return this.falsaTradicion;
    }

    public void setFalsaTradicion(String falsaTradicion) {
        this.falsaTradicion = falsaTradicion;
    }

    @Column(name = "FECHA_REGISTRO", length = 7)
    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Column(name = "NUMERO_REGISTRO", length = 20)
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "MODO_ADQUISICION", length = 30)
    public String getModoAdquisicion() {
        return this.modoAdquisicion;
    }

    public void setModoAdquisicion(String modoAdquisicion) {
        this.modoAdquisicion = modoAdquisicion;
    }

    @Column(name = "VALOR", precision = 18)
    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Column(name = "TIPO_TITULO", length = 30)
    public String getTipoTitulo() {
        return this.tipoTitulo;
    }

    public void setTipoTitulo(String tipoTitulo) {
        this.tipoTitulo = tipoTitulo;
    }

    @Column(name = "NUMERO_TITULO", length = 20)
    public String getNumeroTitulo() {
        return this.numeroTitulo;
    }

    public void setNumeroTitulo(String numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }

    @Column(name = "FECHA_TITULO", length = 7)
    public Date getFechaTitulo() {
        return this.fechaTitulo;
    }

    public void setFechaTitulo(Date fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }

    @Column(name = "LIBRO", length = 20)
    public String getLibro() {
        return this.libro;
    }

    public void setLibro(String libro) {
        this.libro = libro;
    }

    @Column(name = "TOMO", length = 20)
    public String getTomo() {
        return this.tomo;
    }

    public void setTomo(String tomo) {
        this.tomo = tomo;
    }

    @Column(name = "PAGINA", length = 20)
    public String getPagina() {
        return this.pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    @Column(name = "JUSTIFICACION", length = 2000)
    public String getJustificacion() {
        return this.justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = true)
    public Documento getDocumentoSoporte() {
        return this.documentoSoporte;
    }

    public void setDocumentoSoporte(Documento documentoSoporte) {
        this.documentoSoporte = documentoSoporte;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30, nullable = true)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @JoinColumn(name = "H_PREDIO_ID", referencedColumnName = "ID", insertable = false, updatable =
        false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    public HPredio getHPredio() {
        return HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HPersonaPredioPropiedad)) {
            return false;
        }
        HPersonaPredioPropiedad other = (HPersonaPredioPropiedad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
