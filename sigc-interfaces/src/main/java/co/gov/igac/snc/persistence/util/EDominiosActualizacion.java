package co.gov.igac.snc.persistence.util;

/**
 * Dominios de actualizaciòn.
 *
 * @author jamir.avila.
 *
 */
public enum EDominiosActualizacion {
    ACTUALIZACION_TIPO_PROCESO,
    ACTUALIZACION_CONTRATO_ACTI,
    ACTUALIZACION_ZONA,
    CONVENIO_CLASE,
    CONVENIO_TIPO_PROYECTO;
}
