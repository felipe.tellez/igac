package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de bd solicitud forma de petición
 *
 * @author javier.aponte
 */
public enum ESolicitudFormaPeticion {
    DERECHO_DE_PETICION("DERECHO PETICION", "Derecho de petición"),
    TUTELA("TUTELA", "Tutela");

    private String codigo;
    private String valor;

    private ESolicitudFormaPeticion(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
