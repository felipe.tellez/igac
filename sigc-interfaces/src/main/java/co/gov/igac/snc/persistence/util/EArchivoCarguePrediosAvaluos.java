package co.gov.igac.snc.persistence.util;

/**
 * Enumeraciòn que contiene la estructura del archivo de cargue de predios para avaluos, se usa
 * principalmente para validar la estructura del archivo y para guardar la relacion del campo y la
 * columna dentro de la estructura del archivo
 *
 * @author christian.rodriguez
 */
public enum EArchivoCarguePrediosAvaluos {

    AREA_CONSTRUCCION("Área Construcción", 12, 30),
    AREA_TERRENO("Área Terreno", 11, 30),
    CIRCULO_REGISTRAL("Circulo Registral", 5, 3),
    CONDICION_JURIDICA("Condición Jurídica", 8, 10),
    DEPARTAMENTO("Departamento", 0, 2),
    DIRECCION_PREDIO("Dirección Predio", 7, 300),
    MATRICULA_ANTIGUA("Matricula Antigua", 4, 1),
    MUNICIPIO("Municipio", 1, 3),
    NUMERO_PREDIAL("Número Predial", 3, 30),
    NO_MATRICULA("No. Matrícula", 6, 12),
    TIPO_AVALUO_PREDIO("Tipo Avalúo Predio", 2, 15),
    UNIDAD_MEDIDA_AREA_TERRENO("Unidad Medida Área Terreno", 9, 30),
    UNIDAD_MEDIDA_CONSTRUCCION("Unidad Medida Construcción", 10, 30);

    private String descripcion;
    private int longitud;
    private int columna;

    private EArchivoCarguePrediosAvaluos(String descripcion, int columna, int longitud) {
        this.descripcion = descripcion;
        this.columna = columna;
        this.longitud = longitud;
    }

    /**
     * Corresponde al nombre de la columna
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Corresponde al tamanio maximo de la columna
     */
    public int getLongitud() {
        return longitud;
    }

    /**
     * Corresponde al contenido de la columna
     */
    public int getColumna() {
        return columna;
    }

}
