package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the CONTRATO_INTERADMINIS_ADICION database table.
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "CONTRATO_INTERADMINIS_ADICION")
public class ContratoInteradminisAdicion implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private Long adicionDocumentoId;
    private Long contratoInteradminsitrativoId;
    private Date fechaFin;
    private Date fechaLog;
    private String numero;
    private String usuarioLog;
    private Double valor;

    public ContratoInteradminisAdicion() {
    }

    @Id
    @SequenceGenerator(name = "CONTRATO_INTERADMINIS_A_ID_GENERATOR", sequenceName =
        "CONTRATO_INTERADMINIS_A_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "CONTRATO_INTERADMINIS_A_ID_GENERATOR")
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "ADICION_DOCUMENTO_ID")
    public Long getAdicionDocumentoId() {
        return this.adicionDocumentoId;
    }

    public void setAdicionDocumentoId(Long adicionDocumentoId) {
        this.adicionDocumentoId = adicionDocumentoId;
    }

    @Column(name = "CONTRATO_INTERADMINSITRATIV_ID")
    public Long getContratoInteradminsitrativoId() {
        return this.contratoInteradminsitrativoId;
    }

    public void setContratoInteradminsitrativoId(Long contratoInteradminsitrativoId) {
        this.contratoInteradminsitrativoId = contratoInteradminsitrativoId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaHasta) {
        this.fechaFin = fechaHasta;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

}
