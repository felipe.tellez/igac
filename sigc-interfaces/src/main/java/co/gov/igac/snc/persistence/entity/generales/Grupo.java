package co.gov.igac.snc.persistence.entity.generales;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * Grupo entity. @author MyEclipse Persistence Tools
 */
/**
 * MODIFICACIONES HECHAS A LA CLASE Grupo:
 *
 * @modified :: david.cifuentes :: Se adiciona el método transient isActivo() :: 19/10/16
 */
@Entity
@Table(name = "GRUPO", schema = "SNC_GENERALES", uniqueConstraints = @UniqueConstraint(columnNames =
    "NOMBRE"))
public class Grupo implements java.io.Serializable {

    // Serial
    private static final long serialVersionUID = -7760981599620364607L;

    // Fields
    private Long id;
    private String nombre;
    private String activo;
    private String descripcion;
    private String usuarioLog;
    private Date fechaLog;
    private Set<GrupoComponente> grupoComponentes = new HashSet<GrupoComponente>(
        0);
    private Set<GrupoUsuario> grupoUsuarios = new HashSet<GrupoUsuario>(0);

    // Constructors
    /** default constructor */
    public Grupo() {
    }

    /** minimal constructor */
    public Grupo(Long id, String nombre, String activo, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.activo = activo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Grupo(Long id, String nombre, String activo, String descripcion,
        String usuarioLog, Date fechaLog,
        Set<GrupoComponente> grupoComponentes,
        Set<GrupoUsuario> grupoUsuarios) {
        this.id = id;
        this.nombre = nombre;
        this.activo = activo;
        this.descripcion = descripcion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.grupoComponentes = grupoComponentes;
        this.grupoUsuarios = grupoUsuarios;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GRUPO_ID_SEQ")
    @SequenceGenerator(name = "GRUPO_ID_SEQ", sequenceName = "GRUPO_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NOMBRE", unique = true, nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "ACTIVO", nullable = false, length = 2)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "DESCRIPCION", length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "grupo")
    public Set<GrupoComponente> getGrupoComponentes() {
        return this.grupoComponentes;
    }

    public void setGrupoComponentes(Set<GrupoComponente> grupoComponentes) {
        this.grupoComponentes = grupoComponentes;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "grupo")
    public Set<GrupoUsuario> getGrupoUsuarios() {
        return this.grupoUsuarios;
    }

    public void setGrupoUsuarios(Set<GrupoUsuario> grupoUsuarios) {
        this.grupoUsuarios = grupoUsuarios;
    }

    /**
     * Determina si el grupo es activo o no.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isEstadoActivo() {
        if (ESiNo.SI.getCodigo().equals(this.activo)) {
            return true;
        } else {
            return false;
        }
    }

}
