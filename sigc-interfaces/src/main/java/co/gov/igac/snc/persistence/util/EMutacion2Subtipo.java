/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * Enumeración para el dominio MUTACION_2_SUBTIPO
 *
 * @author pedro.garcia.
 * @modified juan.agudelo adición del atributo nombre
 */
public enum EMutacion2Subtipo {

    //VALOR - código
    ENGLOBE("ENGLOBE", "Englobe"),
    DESENGLOBE("DESENGLOBE", "Desenglobe");

    private String codigo;
    private String nombre;

    private EMutacion2Subtipo(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getNombre() {
        return nombre;
    }

}
