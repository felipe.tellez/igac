package co.gov.igac.snc.persistence.entity.tramite;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * SolicitudPago entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "SOLICITUD_PAGO", schema = "SNC_TRAMITE")
public class SolicitudPago implements java.io.Serializable {

    // Fields
    private Long id;
    private Solicitud solicitud;
    private String tipo;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String referencia1;
    private String referencia2;
    private String nombre;
    private String numeroComprobante;
    private String numeroCuenta;
    private String tipoCuenta;
    private String entidadFinanciera;
    private Double valor;
    private Timestamp fecha;
    private String estado;
    private String usuarioLog;
    private Timestamp fechaLog;

    // Constructors
    /** default constructor */
    public SolicitudPago() {
    }

    /** minimal constructor */
    public SolicitudPago(Long id, Solicitud solicitud,
        String tipoIdentificacion, Double valor, Timestamp fecha,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.solicitud = solicitud;
        this.tipoIdentificacion = tipoIdentificacion;
        this.valor = valor;
        this.fecha = fecha;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public SolicitudPago(Long id, Solicitud solicitud, String tipo,
        String tipoIdentificacion, String numeroIdentificacion,
        String referencia1, String referencia2, String nombre,
        String numeroComprobante, String numeroCuenta, String tipoCuenta,
        String entidadFinanciera, Double valor, Timestamp fecha,
        String estado, String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.solicitud = solicitud;
        this.tipo = tipo;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.referencia1 = referencia1;
        this.referencia2 = referencia2;
        this.nombre = nombre;
        this.numeroComprobante = numeroComprobante;
        this.numeroCuenta = numeroCuenta;
        this.tipoCuenta = tipoCuenta;
        this.entidadFinanciera = entidadFinanciera;
        this.valor = valor;
        this.fecha = fecha;
        this.estado = estado;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_ID", nullable = false)
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @Column(name = "TIPO", length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "TIPO_IDENTIFICACION", nullable = false, length = 30)
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "NUMERO_IDENTIFICACION", length = 20)
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "REFERENCIA1", length = 50)
    public String getReferencia1() {
        return this.referencia1;
    }

    public void setReferencia1(String referencia1) {
        this.referencia1 = referencia1;
    }

    @Column(name = "REFERENCIA2", length = 50)
    public String getReferencia2() {
        return this.referencia2;
    }

    public void setReferencia2(String referencia2) {
        this.referencia2 = referencia2;
    }

    @Column(name = "NOMBRE", length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "NUMERO_COMPROBANTE", length = 20)
    public String getNumeroComprobante() {
        return this.numeroComprobante;
    }

    public void setNumeroComprobante(String numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    @Column(name = "NUMERO_CUENTA", length = 20)
    public String getNumeroCuenta() {
        return this.numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    @Column(name = "TIPO_CUENTA", length = 30)
    public String getTipoCuenta() {
        return this.tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    @Column(name = "ENTIDAD_FINANCIERA", length = 50)
    public String getEntidadFinanciera() {
        return this.entidadFinanciera;
    }

    public void setEntidadFinanciera(String entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    @Column(name = "VALOR", nullable = false, precision = 18)
    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Column(name = "FECHA", nullable = false, length = 7)
    public Timestamp getFecha() {
        return this.fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    @Column(name = "ESTADO", length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

}
