package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * MODIFICACIONES PROPIAS A LA CLASE PUnidadConstruccionComp:
 *
 * @author fabio.navarrete Se agregó serialVersionUID
 * @author fabio.navarrete Se agregó el método y atributo transient para consultar el valor de la
 * propiedad cancelaInscribe
 */
/**
 * PUnidadConstruccionComp entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "P_UNIDAD_CONSTRUCCION_COMP", schema = "SNC_CONSERVACION")
public class PUnidadConstruccionComp implements java.io.Serializable, IProyeccionObject,
    IModeloUnidadConstruccionComponente {

    /**
     *
     */
    private static final long serialVersionUID = 3230702521377967753L;
    // Fields

    private Long id;
    private PUnidadConstruccion PUnidadConstruccion;
    private String componente;
    private String elementoCalificacion;
    private String detalleCalificacion;
    private Double puntos;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;

    private String cancelaInscribeValor;

    // Constructors
    /** default constructor */
    public PUnidadConstruccionComp() {
    }

    /** minimal constructor */
    public PUnidadConstruccionComp(Long id,
        PUnidadConstruccion PUnidadConstruccion, Double puntos,
        String cancelaInscribe) {
        this.id = id;
        this.PUnidadConstruccion = PUnidadConstruccion;
        this.puntos = puntos;
        this.cancelaInscribe = cancelaInscribe;
    }

    /** full constructor */
    public PUnidadConstruccionComp(Long id,
        PUnidadConstruccion PUnidadConstruccion, String componente,
        String elementoCalificacion, String detalleCalificacion,
        Double puntos, String cancelaInscribe, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.PUnidadConstruccion = PUnidadConstruccion;
        this.componente = componente;
        this.elementoCalificacion = elementoCalificacion;
        this.detalleCalificacion = detalleCalificacion;
        this.puntos = puntos;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    public PUnidadConstruccionComp(UnidadConstruccionComp comp, String usuario) {
        this.id = null;

        this.componente = comp.getComponente();
        this.elementoCalificacion = comp.getElementoCalificacion();
        this.detalleCalificacion = comp.getDetalleCalificacion();
        this.puntos = comp.getPuntos();
        this.usuarioLog = usuario;
        this.fechaLog = new Date();

    }

    // Property accessors
    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getId()
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "generador_id_P_UNIDAD_CONSTRUCCION_COMP")
    @SequenceGenerator(name = "generador_id_P_UNIDAD_CONSTRUCCION_COMP",
        sequenceName = "UNIDAD_CONSTRUCCION_COM_ID_SEQ", allocationSize = 0)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setId(java.lang.Long)
     */
    @Override
    public void setId(Long id) {
        this.id = id;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getPUnidadConstruccion()
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIDAD_CONSTRUCCION_ID", nullable = false)
    public PUnidadConstruccion getPUnidadConstruccion() {
        return this.PUnidadConstruccion;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setPUnidadConstruccion(co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion)
     */
    public void setPUnidadConstruccion(PUnidadConstruccion PUnidadConstruccion) {
        this.PUnidadConstruccion = PUnidadConstruccion;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getComponente()
     */
    @Override
    @Column(name = "COMPONENTE", length = 30)
    public String getComponente() {
        return this.componente;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setComponente(java.lang.String)
     */
    @Override
    public void setComponente(String componente) {
        this.componente = componente;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getElementoCalificacion()
     */
    @Override
    @Column(name = "ELEMENTO_CALIFICACION", length = 30)
    public String getElementoCalificacion() {
        return this.elementoCalificacion;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setElementoCalificacion(java.lang.String)
     */
    @Override
    public void setElementoCalificacion(String elementoCalificacion) {
        this.elementoCalificacion = elementoCalificacion;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getDetalleCalificacion()
     */
    @Override
    @Column(name = "DETALLE_CALIFICACION", length = 250)
    public String getDetalleCalificacion() {
        return this.detalleCalificacion;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setDetalleCalificacion(java.lang.String)
     */
    @Override
    public void setDetalleCalificacion(String detalleCalificacion) {
        this.detalleCalificacion = detalleCalificacion;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getPuntos()
     */
    @Override
    @Column(name = "PUNTOS", nullable = false, precision = 5)
    public Double getPuntos() {
        return this.puntos;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setPuntos(java.lang.Double)
     */
    @Override
    public void setPuntos(Double puntos) {
        this.puntos = puntos;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getCancelaInscribe()
     */
    @Override
    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setCancelaInscribe(java.lang.String)
     */
    @Override
    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getUsuarioLog()
     */
    @Override
    @Column(name = "USUARIO_LOG", length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setUsuarioLog(java.lang.String)
     */
    @Override
    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getFechaLog()
     */
    @Override
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    /* (non-Javadoc) @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setFechaLog(java.util.Date)
     */
    @Override
    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Transient
    @Override
    public IModeloUnidadConstruccion getPModeloUnidadConstruccion() {
        return this.PUnidadConstruccion;
    }

    @Override
    public void setPModeloUnidadConstruccion(
        IModeloUnidadConstruccion PUnidadConstruccion) {
        this.PUnidadConstruccion = (PUnidadConstruccion) PUnidadConstruccion;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#getCancelaInscribeValor()
     */
    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    /*
     * @see
     * co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccionComponente#setCancelaInscribeValor(java.lang.String)
     */
    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

}
