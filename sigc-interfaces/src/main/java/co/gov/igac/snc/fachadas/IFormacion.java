package co.gov.igac.snc.fachadas;

import javax.ejb.Remote;

/**
 * Interfaz Local para el acceso a los servicios de IFormacion
 *
 * Nota: Los Métodos se declaran en la interfaz Local (IFormacionLocal)
 *
 * @author juan.mendez
 * @version 2.0
 */
@Remote
public interface IFormacion extends IFormacionLocal {

}
