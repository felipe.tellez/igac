package co.gov.igac.snc.fachadas;

import javax.ejb.Remote;

/**
 * IGAC proyecto SNC - Interfaz para el uso de transacciones.
 *
 * @author david.cifuentes
 */
@Remote
public interface ITransaccional extends ITransaccionalLocal {

}
