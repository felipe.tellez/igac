package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.List;

import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;

/**
 * Clase que reduce la informacion del predio de la ficha matriz para ser mostrado en las tareas
 * geograficas
 *
 * @author franz.gamba
 */
public class FichaMatrizPredioVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7959284793425377797L;
    private Long id;
    private Double coeficiente;
    private String numeroPredial;
    private String provieneFichaMatriz;
    private List<UnidadConstruccionVO> unidadesConstruccions;
    private List<PUnidadConstruccionVO> pUnidadesConstruccions;

    /** Empty constructor */
    public FichaMatrizPredioVO() {

    }

    /** Constructor a partir de un FichaMatrizPredio */
    public FichaMatrizPredioVO(FichaMatrizPredio predio) {
        this.coeficiente = predio.getCoeficiente();
        this.numeroPredial = predio.getNumeroPredial();
        if (predio.getProvieneFichaMatriz() != null) {
            this.provieneFichaMatriz = predio.getProvieneFichaMatriz().getPredio().
                getNumeroPredial();
        }
    }

    /** Constructor a partir de un PFichaMatrizPredio */
    public FichaMatrizPredioVO(PFichaMatrizPredio predio) {
        this.coeficiente = predio.getCoeficiente();
        this.numeroPredial = predio.getNumeroPredial();
        if (predio.getProvieneFichaMatriz() != null) {
            this.provieneFichaMatriz = predio.getProvieneFichaMatriz().getPredio().
                getNumeroPredial();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getCoeficiente() {
        return coeficiente;
    }

    public void setCoeficiente(Double coeficiente) {
        this.coeficiente = coeficiente;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public List<UnidadConstruccionVO> getUnidadesConstruccions() {
        return unidadesConstruccions;
    }

    public void setUnidadesConstruccions(
        List<UnidadConstruccionVO> unidadesConstruccions) {
        this.unidadesConstruccions = unidadesConstruccions;
    }

    public List<PUnidadConstruccionVO> getpUnidadesConstruccions() {
        return pUnidadesConstruccions;
    }

    public void setpUnidadesConstruccions(
        List<PUnidadConstruccionVO> pUnidadesConstruccions) {
        this.pUnidadesConstruccions = pUnidadesConstruccions;
    }

    public String getProvieneFichaMatriz() {
        return provieneFichaMatriz;
    }

    public void setProvieneFichaMatriz(String provieneFichaMatriz) {
        this.provieneFichaMatriz = provieneFichaMatriz;
    }

}
