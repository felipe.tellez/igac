package co.gov.igac.snc.fachadas;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioColindante;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.vo.ZonasFisicasGeoeconomicasVO;

/**
 * Fachada para la carga de datos y ejecución de métodos durante las pruebas de integración. (Las
 * pruebas de integración sirven para evaluar la ejecución del código dentro del servidor de
 * aplicaciones)
 *
 * @author juan.mendez
 */
@Local
public interface IPruebasLocal extends Serializable {

    /**
     * Método para cargar un predio de forma aleatoria
     *
     * @author juan.mendez
     * @return
     * @throws ExcepcionSNC
     */
    public Predio cargarPredioAleatorio() throws ExcepcionSNC;

    /**
     *
     * @param codigoInicioNumeroPredial
     * @return
     * @throws ExcepcionSNC
     */
    public Predio cargarPredioAleatorio(String codigoInicioNumeroPredial) throws ExcepcionSNC;

    /**
     * Método para cargar un predio de forma aleatoria
     *
     * @author juan.mendez
     * @return
     * @throws ExcepcionSNC
     */
    public Solicitud cargarSolicitudAleatoria() throws ExcepcionSNC;

    /**
     * Obtiene los colindantes de un predio a partir de su número predial y el tipo de colindancia
     *
     * @author juan.mendez
     * @param numeroPredial
     * @param tipo
     * @return
     */
    public List<PredioColindante> getPredioColindanteByNumeroPredialYTipo(String numeroPredial,
        String tipo);

    /**
     * Metodo encargado de buscar un predio que no se encuentre en tramite alguno, para utilizarlo
     * en la siguiente prueba
     *
     * @author fredy.wilches
     * @version 2.0
     *
     * @return
     */
    public Predio buscarPredioLibreTramite(Integer codigoEstructuraOrganizacional,
        String codigoMunicipio);

    /**
     * Prueba. Ejecuta un procedimiento de prueba para ver cómo se aisla la transacción para que en
     * caso de que haya que hacer rollback no afecte el flujo del caso de uso. Se debe hacer como
     * prueba de integración porque debe haber un contexto de persistencia para poder hacer
     * rollback. El onjetivo de este método es ser ejecutado como una prueba de integración
     *
     * @author pedro.garcia
     *
     * @param codigoPrueba un código de error para diferenciar la prueba
     * @param mensajePrueba un mensaje de error para diferenciar la prueba
     * @param generarError para decidir si el procedimiento debe generar un error en su ejecución
     */
    public Object[] ejecutarSPAislado(String codigoPrueba, String mensajePrueba,
        boolean generarError);

    /**
     * Prueba de integración para el método de sig enviarJobExportarDatosGeograficosTramite
     *
     * @author juan.mendez
     *
     * @param tramite
     * @param identificadoresPredios
     * @param tipoMutacion
     * @param formatoArchivo
     * @param usuario
     */
    public void enviarJobExportarDatosGeograficosTramite(Tramite tramite,
        String identificadoresPredios,
        String tipoMutacion, String formatoArchivo, UsuarioDTO usuario);

}
