package co.gov.igac.snc.util;

import java.io.Serializable;

import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;

/**
 * Filtro creado para realizar busquedas sobre la entidad {@link ContratoInteradministrativo}
 *
 * @author christian.rodriguez
 *
 */
public class FiltroDatosConsultaContratoInteradministrativo implements Serializable {

    private static final long serialVersionUID = -4460829223847867545L;

    private String numeroContrato;
    private Long vigencia;
    private String numeroDocumentoInterventor;
    private String nombreCompletoInterventor;
    private Long idInterventor;
    private boolean activo;
    private Long entidadSolicitanteSolicitudId;

    public FiltroDatosConsultaContratoInteradministrativo() {
    }

    public String getNumeroContrato() {
        return this.numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public Long getIdInterventor() {
        return idInterventor;
    }

    public void setIdInterventor(Long idInterventor) {
        this.idInterventor = idInterventor;
    }

    public Long getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Long vigencia) {
        this.vigencia = vigencia;
    }

    public String getNumeroDocumentoInterventor() {
        return this.numeroDocumentoInterventor;
    }

    public void setNumeroDocumentoInterventor(String numeroDocumentoInterventor) {
        this.numeroDocumentoInterventor = numeroDocumentoInterventor;
    }

    public String getNombreCompletoInterventor() {
        return this.nombreCompletoInterventor;
    }

    public void setNombreCompletoInterventor(String nombreCompletoInterventor) {
        this.nombreCompletoInterventor = nombreCompletoInterventor;
    }

    public boolean isActivo() {
        return this.activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Long getEntidadSolicitanteSolicitudId() {
        return this.entidadSolicitanteSolicitudId;
    }

    public void setEntidadSolicitanteSolicitudId(
        Long entidadSolicitanteSolicitudId) {
        this.entidadSolicitanteSolicitudId = entidadSolicitanteSolicitudId;
    }

}
