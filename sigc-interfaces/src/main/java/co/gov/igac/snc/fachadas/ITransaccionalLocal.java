package co.gov.igac.snc.fachadas;

import java.util.List;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Colindante;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.generales.PlantillaSeccionReporte;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/**
 * IGAC proyecto SNC - Interfaz para el uso de transacciones.
 *
 * @author david.cifuentes
 */
@Local
public interface ITransaccionalLocal {

    /**
     * Método que actualiza un trámite iniciando una nueva transacción y realizando un commit de
     * ella.
     *
     * @author david.cifuentes
     * @param numeroRadicacion
     * @return
     */
    public Tramite actualizarTramiteNuevaTransaccion(Tramite tramite);

    // ----------------------------------------------------------//
    /**
     * Método que realiza el llamado a método del mismo nombre en IMunicipioCierreVigenciaDAO
     * iniciando una nueva transacción.
     *
     * @author david.cifuentes
     *
     * @param decretoAvaluoAnual {@link DecretoAvaluoanual} decreto actual.
     * @param usuario {@link UsuarioDTO} usuario en sesión.
     *
     * @throws {@link ExcepcionSNC} Excepción lanzada en caso de algún error.
     *
     */
    public void validarExistenciaMunicipiosPorDecretoNuevaTransaccion(
        DecretoAvaluoAnual decretoAvaluoAnual, UsuarioDTO usuario) throws ExcepcionSNC;

    // ----------------------------------------------------------//
    /**
     * Método que realiza el llamado a método del mismo nombre en IColindanteDAO iniciando una nueva
     * transacción.
     *
     * @author david.cifuentes
     * @param numeroPredial
     * @return
     */
    public List<String> findPrediosColindantesByNumeroPredialNuevaTransaccion(
        String numeroPredial);

    // ----------------------------------------------------------//
    /**
     * Método que realiza el llamado a método del mismo nombre en IPredioDAO iniciando una nueva
     * transacción.
     *
     * @author david.cifuentes
     * @param numerosPrediales
     * @return
     */
    public List<Predio> getPrediosByNumerosPredialesNuevaTransaccion(
        List<String> numerosPrediales);

    // ----------------------------------------------------------//
    /**
     * Método que realiza el llamado a método del mismo nombre en IColindanteDAO iniciando una nueva
     * transacción.
     *
     * @author david.cifuentes
     * @param numPredial
     * @return
     */
    public List<Colindante> buscarColindantesNumeroPredialNuevaTransaccion(
        String numPredial);

    // ----------------------------------------------------------//
    /**
     * Método que realiza una eliminación múltiple de {@link Colindante} dada una lista de objetos y
     * creando una transacción independiente
     *
     * @author david.cifuentes
     * @param colAnteriores
     */
    public void deleteMultipleColindantesNuevaTransaccion(List<Colindante> colAnteriores);

    // ----------------------------------------------------------//
    /**
     * Método que realiza la actualización de un {@link Colindante} creando una transacción
     * independiente
     *
     * @author david.cifuentes
     * @param colAnteriores
     */
    public void updateColindanteNuevaTransaccion(Colindante colindante);

    // ----------------------------------------------------------//
    /**
     * Método que realiza el llamado a método getPredioByNumeroPredial en IPredioDAO iniciando una
     * nueva transacción.
     *
     * @author david.cifuentes
     * @param numeroPredial
     * @return
     */
    public Predio findPredioByNumeroPredialNuevaTransaccion(String numeroPredial);

    // ----------------------------------------------------------//
    /**
     * Retorna el trámite y las pruebas solicitadas si existen a partir del id del trámite creando
     * una nueva transacción para la consulta.
     *
     * @author david.cifuentes
     * @param tramiteId
     * @return
     */
    public Tramite findTramitePruebasByTramiteIdNuevaTransaccion(Long tramiteId);

    // ----------------------------------------------------------//
    /**
     * Retorna la solicitud si existe a partir del id de la misma creando una nueva transacción para
     * la consulta.
     *
     * @author david.cifuentes
     * @param idSolicitud
     * @return
     */
    public Solicitud buscarSolicitudFetchTramitesBySolicitudIdNuevaTransaccion(Long idSolicitud);

    // ----------------------------------------------------------//
    /**
     * Actualiza la solicitud creando una nueva transacción.
     *
     * @author david.cifuentes
     * @param solicitud
     * @return
     */
    public Solicitud updateSolicitudNuevaTransaccion(Solicitud solicitud);

    // ----------------------------------------------------------//
    /**
     * Actualiza una lista de {@link PlantillaSeccionReporte} creando una nueva transacción.
     *
     * @author david.cifuentes
     * @param plantillas
     * @return
     */
    public void actualizarPlantillaSeccionReportesNuevaTransaccion(
        List<PlantillaSeccionReporte> plantillas);

}
