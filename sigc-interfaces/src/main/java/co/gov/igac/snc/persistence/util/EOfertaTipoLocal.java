/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los codigos de tipos de local para ofertas
 *
 * @author christian.rodriguez
 */
public enum EOfertaTipoLocal {

    CENTRO_COMERCIAL("Centro Comercial"),
    NO_CENTRO_COMERCIAL("No Centro Comercial");

    private String codigo;

    private EOfertaTipoLocal(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
