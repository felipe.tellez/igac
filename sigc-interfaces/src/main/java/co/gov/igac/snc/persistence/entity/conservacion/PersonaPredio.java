package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 * MODIFICACIONES PROPIAS A LA CLASE PersonaPredio:
 *
 * @modified fabio.navarrete -> Se agregó el serialVersionUID. -> Se modificó el fetch type del
 * atributo Persona a EAGER. -> Se modificó el fetch type del atributo Predio a EAGER. -> Se
 * modificó el tipo del campo personaPredioPropiedads de Set a List. ->
 */
/**
 * PersonaPredio entity. @author MyEclipse Persistence Tools
 *
 * @modified juan.agudelo 13-10-11 Se agregó la secuencia PERSONA_PREDIO_ID_SEQ
 */
@Entity
@Table(name = "PERSONA_PREDIO", schema = "SNC_CONSERVACION")
public class PersonaPredio implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1354232570819527579L;

    private Long id;
    private Persona persona;
    private Predio predio;
    private String tipo;
    private Double participacion;
    private String usuarioLog;
    private Date fechaLog;
    private List<PersonaPredioPropiedad> personaPredioPropiedads =
        new ArrayList<PersonaPredioPropiedad>();

    private Date fechaInscripcionCatastral;

    private boolean cantidadPredios;

    // Constructors
    /** default constructor */
    public PersonaPredio() {
    }

    /** minimal constructor */
    public PersonaPredio(Long id, Persona persona, Predio predio, String tipo,
        Double participacion, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.persona = persona;
        this.predio = predio;
        this.tipo = tipo;
        this.participacion = participacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PersonaPredio(Long id, Persona persona, Predio predio, String tipo,
        Double participacion, String usuarioLog, Date fechaLog,
        List<PersonaPredioPropiedad> personaPredioPropiedads) {
        this.id = id;
        this.persona = persona;
        this.predio = predio;
        this.tipo = tipo;
        this.participacion = participacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.personaPredioPropiedads = personaPredioPropiedads;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSONA_PREDIO_ID_SEQ")
    @SequenceGenerator(name = "PERSONA_PREDIO_ID_SEQ", sequenceName = "PERSONA_PREDIO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PERSONA_ID", nullable = false)
    public Persona getPersona() {
        return this.persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "PARTICIPACION", nullable = false, precision = 5)
    public Double getParticipacion() {
        return this.participacion;
    }

    public void setParticipacion(Double participacion) {
        this.participacion = participacion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "personaPredio")
    public List<PersonaPredioPropiedad> getPersonaPredioPropiedads() {
        return this.personaPredioPropiedads;
    }

    public void setPersonaPredioPropiedads(
        List<PersonaPredioPropiedad> personaPredioPropiedads) {
        this.personaPredioPropiedads = personaPredioPropiedads;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL")
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Transient
    public boolean isCantidadPredios() {
        return cantidadPredios;
    }

    public void setCantidadPredios(boolean cantidadPredios) {
        this.cantidadPredios = cantidadPredios;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static PersonaPredio parsePersonaPredio(HPersonaPredio hPersonaPredio) {

        PersonaPredio personaPredio = new PersonaPredio();
        personaPredio.setId(hPersonaPredio.getId().getId());
        personaPredio.setPersona(hPersonaPredio.getPersona());
        personaPredio.setPredio(hPersonaPredio.getPredio());
        personaPredio.setTipo(hPersonaPredio.getTipo());
        personaPredio.setParticipacion(hPersonaPredio.getParticipacion());
        personaPredio.setUsuarioLog(hPersonaPredio.getUsuarioLog());
        personaPredio.setFechaLog(hPersonaPredio.getFechaLog());

        List<HPersonaPredioPropiedad> hPersonasPredioPropiedads = hPersonaPredio.
            gethPersonaPredioPropiedads();
        List<PersonaPredioPropiedad> personasPredioPropiedads =
            new ArrayList<PersonaPredioPropiedad>();

        for (HPersonaPredioPropiedad unaHPersonaPredioPropiedad : hPersonasPredioPropiedads) {
            personasPredioPropiedads.add(PersonaPredioPropiedad.parsePersonaPredioPropiedad(
                unaHPersonaPredioPropiedad));
        }
        personaPredio.setPersonaPredioPropiedads(personasPredioPropiedads);

        return personaPredio;

    }

}
