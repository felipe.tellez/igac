package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.Hibernate;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;

import java.util.Date;

/**
 * The persistent class for the TRAMITE_PRUEBA_ENTIDAD database table.
 *
 * @modified by juan.agudelo 28-11-11 Se agrega el método transiet para recuperar el nombre de la
 * entidad que realiza las pruebas
 *
 * @modified by david.cifuentes :: 28-11-11 Se agrega el método transiet nombre completo.
 * @modified by david.cifuentes :: 09-07-12 Se modifica las notaciones de mapeo del atributo
 * estructura organizacional.
 *
 */
@Entity
@Table(name = "TRAMITE_PRUEBA_ENTIDAD")
public class TramitePruebaEntidad implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6209237481685528178L;

    private Long id;
    private String correoElectronico;
    private String correoElectronicoSecundario;
    private String digitoVerificacion;
    private String direccion;
    private Pais direccionPais;
    private Departamento direccionDepartamento;
    private Municipio direccionMunicipio;
    private EstructuraOrganizacional estructuraOrganizacional;
    private String fax;
    private String faxExt;
    private Date fechaLog;
    private String notificacionEmail;
    private String numeroIdentificacion;
    private String primerApellido;
    private String primerNombre;
    private String razonSocial;
    private String relacion;
    private String segundoApellido;
    private String segundoNombre;
    private String sigla;
    private String telefonoCelular;
    private String telefonoPrincipal;
    private String telefonoPrincipalExt;
    private String telefonoSecundario;
    private String telefonoSecundarioExt;
    private String tipoIdentificacion;
    private String tipoPersona;
    private String tipoSolicitante;
    private String usuarioLog;
    private Solicitante solicitante;
    private TramitePrueba tramitePrueba;

    private String nombreCompleto;

    /** Numero de documentos asociados a las pruebas con un trámite */
    private Integer documentosPruebas;

    public TramitePruebaEntidad() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_PRUEBA_ENTIDAD_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_PRUEBA_ENTIDAD_ID_SEQ", sequenceName =
        "TRAMITE_PRUEBA_ENTIDAD_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CORREO_ELECTRONICO")
    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Column(name = "CORREO_ELECTRONICO_SECUNDARIO")
    public String getCorreoElectronicoSecundario() {
        return this.correoElectronicoSecundario;
    }

    public void setCorreoElectronicoSecundario(
        String correoElectronicoSecundario) {
        this.correoElectronicoSecundario = correoElectronicoSecundario;
    }

    @Column(name = "DIGITO_VERIFICACION")
    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_PAIS_CODIGO", nullable = true)
    public Pais getDireccionPais() {
        return this.direccionPais;
    }

    public void setDireccionPais(Pais direccionPais) {
        this.direccionPais = direccionPais;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_DEPARTAMENTO_CODIGO", nullable = true)
    public Departamento getDireccionDepartamento() {
        return this.direccionDepartamento;
    }

    public void setDireccionDepartamento(Departamento direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_MUNICIPIO_CODIGO", nullable = true)
    public Municipio getDireccionMunicipio() {
        return this.direccionMunicipio;
    }

    public void setDireccionMunicipio(Municipio direccionMunicipio) {
        this.direccionMunicipio = direccionMunicipio;
    }

//	@Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ESTRUCTURA_ORGANIZACIONAL_COD", nullable = true)
    public EstructuraOrganizacional getEstructuraOrganizacional() {
        return this.estructuraOrganizacional;
    }

    public void setEstructuraOrganizacional(
        EstructuraOrganizacional estructuraOrganizacionalCod) {
        this.estructuraOrganizacional = estructuraOrganizacionalCod;
    }

    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Column(name = "FAX_EXT")
    public String getFaxExt() {
        return this.faxExt;
    }

    public void setFaxExt(String faxExt) {
        this.faxExt = faxExt;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NOTIFICACION_EMAIL")
    public String getNotificacionEmail() {
        return this.notificacionEmail;
    }

    public void setNotificacionEmail(String notificacionEmail) {
        this.notificacionEmail = notificacionEmail;
    }

    @Column(name = "NUMERO_IDENTIFICACION")
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "PRIMER_APELLIDO")
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "PRIMER_NOMBRE")
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Column(name = "RAZON_SOCIAL")
    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRelacion() {
        return this.relacion;
    }

    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }

    @Column(name = "SEGUNDO_APELLIDO")
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "SEGUNDO_NOMBRE")
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Column(name = "TELEFONO_CELULAR")
    public String getTelefonoCelular() {
        return this.telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    @Column(name = "TELEFONO_PRINCIPAL")
    public String getTelefonoPrincipal() {
        return this.telefonoPrincipal;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    @Column(name = "TELEFONO_PRINCIPAL_EXT")
    public String getTelefonoPrincipalExt() {
        return this.telefonoPrincipalExt;
    }

    public void setTelefonoPrincipalExt(String telefonoPrincipalExt) {
        this.telefonoPrincipalExt = telefonoPrincipalExt;
    }

    @Column(name = "TELEFONO_SECUNDARIO")
    public String getTelefonoSecundario() {
        return this.telefonoSecundario;
    }

    public void setTelefonoSecundario(String telefonoSecundario) {
        this.telefonoSecundario = telefonoSecundario;
    }

    @Column(name = "TELEFONO_SECUNDARIO_EXT")
    public String getTelefonoSecundarioExt() {
        return this.telefonoSecundarioExt;
    }

    public void setTelefonoSecundarioExt(String telefonoSecundarioExt) {
        this.telefonoSecundarioExt = telefonoSecundarioExt;
    }

    @Column(name = "TIPO_IDENTIFICACION")
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "TIPO_PERSONA")
    public String getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    @Column(name = "TIPO_SOLICITANTE")
    public String getTipoSolicitante() {
        return this.tipoSolicitante;
    }

    public void setTipoSolicitante(String tipoSolicitante) {
        this.tipoSolicitante = tipoSolicitante;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to Solicitante
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ENTIDAD_SOLICITANTE_ID")
    public Solicitante getSolicitante() {
        return this.solicitante;
    }

    public void setSolicitante(Solicitante solicitante) {
        this.solicitante = solicitante;
    }

    // bi-directional many-to-one association to TramitePrueba
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_PRUEBA_ID")
    public TramitePrueba getTramitePrueba() {
        return this.tramitePrueba;
    }

    public void setTramitePrueba(TramitePrueba tramitePrueba) {
        this.tramitePrueba = tramitePrueba;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que retorna el nombre completo para el GIT o la Entidad externa que realiza las
     * pruebas
     *
     * @author juan.agudelo
     */
    @Transient
    public String getNombreCompletoEntidad() {
        String sb = "";
        if (this.estructuraOrganizacional != null &&
            !this.estructuraOrganizacional.getCodigo().isEmpty()) {
            sb = this.estructuraOrganizacional.getCodigo() + " - " + this.estructuraOrganizacional.
                getNombre();
        } else if (this.razonSocial != null && !this.razonSocial.isEmpty()) {
            sb = this.razonSocial;
        } else if (this.solicitante != null &&
            this.solicitante.getTipoPersona() != null &&
            !this.solicitante.getTipoPersona().isEmpty() &&
            this.solicitante.getTipoPersona().
                equals(EPersonaTipoPersona.NATURAL.getCodigo())) {
            sb = this.solicitante.getNombreCompleto();
        }
        return sb;
    }

    /**
     * Método que agrega los datos comunes al Solicitante y a TramitePruebaEntidad en el objeto
     * TramitePruebaEntidad
     *
     * @param sol Solicitante del cual se toman los datos.
     * @author fabio.navarrete
     */
    public void incorporarDatosSolicitante(Solicitante sol) {
        this.correoElectronico = sol.getCorreoElectronico();
        this.correoElectronicoSecundario = sol.getCorreoElectronicoSecundario();
        this.digitoVerificacion = sol.getDigitoVerificacion();
        this.direccion = sol.getDireccion();
        this.direccionDepartamento = sol.getDireccionDepartamento();
        this.direccionMunicipio = sol.getDireccionMunicipio();
        this.direccionPais = sol.getDireccionPais();
        this.fax = sol.getFax();
        this.faxExt = sol.getFaxExt();
        this.numeroIdentificacion = sol.getNumeroIdentificacion();
        this.primerApellido = sol.getPrimerApellido();
        this.primerNombre = sol.getPrimerNombre();
        this.razonSocial = sol.getRazonSocial();
        this.segundoApellido = sol.getSegundoApellido();
        this.segundoNombre = sol.getSegundoNombre();
        this.telefonoCelular = sol.getTelefonoCelular();
        this.telefonoPrincipal = sol.getTelefonoPrincipal();
        this.telefonoPrincipalExt = sol.getTelefonoPrincipalExt();
        this.telefonoSecundario = sol.getTelefonoSecundario();
        this.telefonoSecundarioExt = sol.getTelefonoSecundarioExt();
        this.tipoIdentificacion = sol.getTipoIdentificacion();
        this.tipoPersona = sol.getTipoPersona();
        this.tipoSolicitante = sol.getTipoSolicitante();
        this.relacion = sol.getRelacion();
        this.notificacionEmail = sol.getNotificacionEmail();
        this.solicitante = sol;
    }

    // --------------------------------------------------------------- //
    /**
     * Método que retorna el nombre completo concatenando los campos para el solicitante.
     *
     * D: Método basado en el mismo que se encuentra en la entidad Solicitante.
     */
    @Transient
    public String getNombreCompleto() {

        if (this.nombreCompleto != null && !this.nombreCompleto.equals("")) {
            return this.nombreCompleto;
        }

        StringBuffer sb = new StringBuffer();
        sb.append("");
        if (this.tipoIdentificacion.equals(EPersonaTipoIdentificacion.NIT
            .getCodigo())) {
            if (this.razonSocial != null) {
                sb.append(this.razonSocial).append(" ");
            }
        } else {
            if (this.primerNombre != null) {
                sb.append(this.primerNombre).append(" ");
            }
            if (this.segundoNombre != null) {
                sb.append(this.segundoNombre).append(" ");
            }
            if (this.primerApellido != null) {
                sb.append(this.primerApellido).append(" ");
            }
            if (this.segundoApellido != null) {
                sb.append(this.segundoApellido).append(" ");
            }
        }
        return sb.toString();
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }
    // --------------------------------------------------------------- //

    /**
     * Método transient que almacena el numero de documentos asociados con pruebas
     *
     * @param nombreCompleto
     */
    @Transient
    public Integer getDocumentosPruebas() {
        return documentosPruebas;
    }

    public void setDocumentosPruebas(Integer documentosPruebas) {
        this.documentosPruebas = documentosPruebas;
    }

}
