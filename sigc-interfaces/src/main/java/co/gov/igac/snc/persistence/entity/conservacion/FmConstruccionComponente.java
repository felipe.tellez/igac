package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the FM_CONSTRUCCION_COMPONENTE database table.
 *
 * @modified juan.agudelo 21-03-12 Adición de la secuencia FM_CONSTRUCCION_COMPONE_ID_SEQ
 */
@Entity
@Table(name = "FM_CONSTRUCCION_COMPONENTE")
public class FmConstruccionComponente implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3334113971223365799L;

    private long id;
    private String componente;
    private String detalleCalificacion;
    private String elementoCalificacion;
    private Date fechaLog;
    private Double puntos;
    private String usuarioLog;
    private FmModeloConstruccion fmModeloConstruccion;

    public FmConstruccionComponente() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FM_CONSTRUCCION_COMPONE_ID_SEQ")
    @SequenceGenerator(name = "FM_CONSTRUCCION_COMPONE_ID_SEQ", sequenceName =
        "FM_CONSTRUCCION_COMPONE_ID_SEQ", allocationSize = 1)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComponente() {
        return this.componente;
    }

    public void setComponente(String componente) {
        this.componente = componente;
    }

    @Column(name = "DETALLE_CALIFICACION")
    public String getDetalleCalificacion() {
        return this.detalleCalificacion;
    }

    public void setDetalleCalificacion(String detalleCalificacion) {
        this.detalleCalificacion = detalleCalificacion;
    }

    @Column(name = "ELEMENTO_CALIFICACION")
    public String getElementoCalificacion() {
        return this.elementoCalificacion;
    }

    public void setElementoCalificacion(String elementoCalificacion) {
        this.elementoCalificacion = elementoCalificacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public Double getPuntos() {
        return this.puntos;
    }

    public void setPuntos(Double puntos) {
        this.puntos = puntos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to FmModeloConstruccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FM_MODELO_CONSTRUCCION_ID")
    public FmModeloConstruccion getFmModeloConstruccion() {
        return this.fmModeloConstruccion;
    }

    public void setFmModeloConstruccion(
        FmModeloConstruccion fmModeloConstruccion) {
        this.fmModeloConstruccion = fmModeloConstruccion;
    }

    /**
     * Retorna una entidad tipo PFmConstruccionComponente a partir del objeto actual se usa para
     * hacer operaciones definidas sobre proyecciones, pero que no existe proyeccion en BD
     *
     *
     * @author felipe.cadena
     *
     * @return
     */
    public PFmConstruccionComponente obtenerProyectadoDesdeOriginal() {

        PFmConstruccionComponente resultado = new PFmConstruccionComponente();

        resultado.setComponente(this.componente);
        resultado.setDetalleCalificacion(this.detalleCalificacion);
        resultado.setElementoCalificacion(this.elementoCalificacion);
        resultado.setFechaLog(this.fechaLog);
        resultado.setId(this.id);
        resultado.setPuntos(this.puntos);
        resultado.setUsuarioLog(this.usuarioLog);

        return resultado;

    }

}
