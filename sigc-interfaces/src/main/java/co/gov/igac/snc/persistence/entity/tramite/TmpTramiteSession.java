package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the TMP_TRAMITE_SESSION database table.
 *
 */
@Entity
@Table(name = "TMP_TRAMITE_SESSION")
public class TmpTramiteSession implements Serializable {

    private static final long serialVersionUID = 1L;

    //private Date fechaLog;
    private TmpTramiteSessionId id;

    public TmpTramiteSession() {
    }


    /* @Temporal( TemporalType.DATE) @Column(name="FECHA_LOG") public Date getFechaLog() { return
     * this.fechaLog; }
     *
     * public void setFechaLog(Date fechaLog) { this.fechaLog = fechaLog; } */
    @EmbeddedId
    public TmpTramiteSessionId getId() {
        return id;
    }

    public void setId(TmpTramiteSessionId id) {
        this.id = id;
    }

}
