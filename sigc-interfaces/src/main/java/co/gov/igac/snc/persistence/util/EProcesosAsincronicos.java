/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * Determina los procesos que se realizan de forma asincronica
 *
 * @author felipe.cadena
 */
public enum EProcesosAsincronicos {

    APLICAR_CAMBIOS_CONSERVACION,
    APLICAR_CAMBIOS_DEPURACION,
    APLICAR_CAMBIOS_ACTUALIZACION,
    GENERAR_COPIA_CONSULTA,
    GENERAR_COPIA_EDICION,
    //estados de error de los procesos
    APLICAR_CAMBIOS_CONSERVACION_ERROR,
    APLICAR_CAMBIOS_DEPURACION_ERROR,
    GENERAR_COPIA_CONSULTA_ERROR,
    GENERAR_COPIA_EDICION_ERROR;

}
