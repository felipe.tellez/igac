package co.gov.igac.snc.persistence.entity.i11n;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the INF_INTERRELACIONADA database table.
 *
 */
@Entity
@Table(name = "INF_INTERRELACIONADA", schema = "SNC_INTERRELACION")
public class InfInterrelacionada implements Serializable {

    private static final long serialVersionUID = 1L;
    private String anotacionReal;
    private String automatica;
    private String circuloRegistral;
    private String ciudadOrigenInstrumento;
    private String codigoDepartamento;
    private String codigoMunicipio;
    private String direccion;
    private String estado;
    private String estadoFolio;
    private String estadoInterrelacion;
    private Date fechaInstrumento;
    private Date fechaRecibido;
    private BigDecimal idEnvioRegistro;
    private String instrumento;
    private String matricula;
    private String modoAdquisicion;
    private String naturalezaJuridica;
    private Long nciId;
    private String numeroAnotacion;
    private String numeroCatastro;
    private String numeroInstrumento;
    private String oficinaOrigenInstrumento;
    private List<Propietario> propietarios = new ArrayList<Propietario>();

    private boolean seleccionado;

    public InfInterrelacionada() {
    }

    @Column(name = "ANOTACION_REAL")
    public String getAnotacionReal() {
        return this.anotacionReal;
    }

    public void setAnotacionReal(String anotacionReal) {
        this.anotacionReal = anotacionReal;
    }

    public String getAutomatica() {
        return this.automatica;
    }

    public void setAutomatica(String automatica) {
        this.automatica = automatica;
    }

    @Column(name = "CIRCULO_REGISTRAL")
    public String getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(String circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "CIUDAD_ORIGEN_INSTRUMENTO")
    public String getCiudadOrigenInstrumento() {
        return this.ciudadOrigenInstrumento;
    }

    public void setCiudadOrigenInstrumento(String ciudadOrigenInstrumento) {
        this.ciudadOrigenInstrumento = ciudadOrigenInstrumento;
    }

    @Column(name = "CODIGO_DEPARTAMENTO")
    public String getCodigoDepartamento() {
        return this.codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

    @Column(name = "CODIGO_MUNICIPIO")
    public String getCodigoMunicipio() {
        return this.codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "ESTADO_FOLIO")
    public String getEstadoFolio() {
        return this.estadoFolio;
    }

    public void setEstadoFolio(String estadoFolio) {
        this.estadoFolio = estadoFolio;
    }

    @Column(name = "ESTADO_INTERRELACION")
    public String getEstadoInterrelacion() {
        return this.estadoInterrelacion;
    }

    public void setEstadoInterrelacion(String estadoInterrelacion) {
        this.estadoInterrelacion = estadoInterrelacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSTRUMENTO")
    public Date getFechaInstrumento() {
        return this.fechaInstrumento;
    }

    public void setFechaInstrumento(Date fechaInstrumento) {
        this.fechaInstrumento = fechaInstrumento;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RECIBIDO")
    public Date getFechaRecibido() {
        return this.fechaRecibido;
    }

    public void setFechaRecibido(Date fechaRecibido) {
        this.fechaRecibido = fechaRecibido;
    }

    @Column(name = "ID_ENVIO_REGISTRO")
    public BigDecimal getIdEnvioRegistro() {
        return this.idEnvioRegistro;
    }

    public void setIdEnvioRegistro(BigDecimal idEnvioRegistro) {
        this.idEnvioRegistro = idEnvioRegistro;
    }

    public String getInstrumento() {
        return this.instrumento;
    }

    public void setInstrumento(String instrumento) {
        this.instrumento = instrumento;
    }

    public String getMatricula() {
        return this.matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Column(name = "MODO_ADQUISICION")
    public String getModoAdquisicion() {
        return this.modoAdquisicion;
    }

    public void setModoAdquisicion(String modoAdquisicion) {
        this.modoAdquisicion = modoAdquisicion;
    }

    @Column(name = "NATURALEZA_JURIDICA")
    public String getNaturalezaJuridica() {
        return this.naturalezaJuridica;
    }

    public void setNaturalezaJuridica(String naturalezaJuridica) {
        this.naturalezaJuridica = naturalezaJuridica;
    }

    @Id
    @Column(name = "NCI_ID")
    public Long getNciId() {
        return this.nciId;
    }

    public void setNciId(Long nciId) {
        this.nciId = nciId;
    }

    @Column(name = "NUMERO_ANOTACION")
    public String getNumeroAnotacion() {
        return this.numeroAnotacion;
    }

    public void setNumeroAnotacion(String numeroAnotacion) {
        this.numeroAnotacion = numeroAnotacion;
    }

    @Column(name = "NUMERO_CATASTRO")
    public String getNumeroCatastro() {
        return this.numeroCatastro;
    }

    public void setNumeroCatastro(String numeroCatastro) {
        this.numeroCatastro = numeroCatastro;
    }

    @Column(name = "NUMERO_INSTRUMENTO")
    public String getNumeroInstrumento() {
        return this.numeroInstrumento;
    }

    public void setNumeroInstrumento(String numeroInstrumento) {
        this.numeroInstrumento = numeroInstrumento;
    }

    @Column(name = "OFICINA_ORIGEN_INSTRUMENTO")
    public String getOficinaOrigenInstrumento() {
        return this.oficinaOrigenInstrumento;
    }

    public void setOficinaOrigenInstrumento(String oficinaOrigenInstrumento) {
        this.oficinaOrigenInstrumento = oficinaOrigenInstrumento;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy =
        "infInterrelacionadaNciId")
    public List<Propietario> getPropietarios() {
        return propietarios;
    }

    public void setPropietarios(List<Propietario> propietarios) {
        this.propietarios = propietarios;
    }

    @Transient
    public boolean isSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

}
