package co.gov.igac.snc.util;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;

import java.io.Serializable;
import java.util.Map;

/**
 * Datos del filtro de predios proyectados en cargue CICA
 *
 * @author felipe.cadena
 *
 */
public class FiltroCargueDTO implements Serializable {

    //filtros vista
    int first;
    int pageSize;
    String sortField;
    String sortOrder;
    Map<String, String> filters;

    private RangoDTO areaTerrenoInicial;
    private RangoDTO areaTerrenoFinal;
    private RangoDTO areaTerrenoVariacion;
    private RangoDTO areaConstruccionInicial;
    private RangoDTO areaConstruccionFinal;
    private RangoDTO areaConstruccionVariacion;
    private RangoDTO avaluoInicial;
    private RangoDTO avaluoFinal;
    private RangoDTO avaluoVariacion;
    private String deptoCodigo;
    private String municipioCodigo;
    private int vigencia;


    /**
     * Void Constructor
     */
    public FiltroCargueDTO() {
        this.areaTerrenoInicial = new RangoDTO();
        this.areaTerrenoFinal = new RangoDTO();
        this.areaTerrenoVariacion = new RangoDTO();
        this.areaConstruccionInicial = new RangoDTO();
        this.areaConstruccionFinal = new RangoDTO();
        this.areaConstruccionVariacion = new RangoDTO();
        this.avaluoInicial = new RangoDTO();
        this.avaluoFinal = new RangoDTO();
        this.avaluoVariacion = new RangoDTO();

    }

    public RangoDTO getAreaTerrenoInicial() {
        return areaTerrenoInicial;
    }

    public void setAreaTerrenoInicial(RangoDTO areaTerrenoInicial) {
        this.areaTerrenoInicial = areaTerrenoInicial;
    }

    public RangoDTO getAreaTerrenoFinal() {
        return areaTerrenoFinal;
    }

    public void setAreaTerrenoFinal(RangoDTO areaTerrenoFinal) {
        this.areaTerrenoFinal = areaTerrenoFinal;
    }

    public RangoDTO getAreaTerrenoVariacion() {
        return areaTerrenoVariacion;
    }

    public void setAreaTerrenoVariacion(RangoDTO areaTerrenoVariacion) {
        this.areaTerrenoVariacion = areaTerrenoVariacion;
    }

    public RangoDTO getAreaConstruccionInicial() {
        return areaConstruccionInicial;
    }

    public void setAreaConstruccionInicial(RangoDTO areaConstruccionInicial) {
        this.areaConstruccionInicial = areaConstruccionInicial;
    }

    public RangoDTO getAreaConstruccionFinal() {
        return areaConstruccionFinal;
    }

    public void setAreaConstruccionFinal(RangoDTO areaConstruccionFinal) {
        this.areaConstruccionFinal = areaConstruccionFinal;
    }

    public RangoDTO getAreaConstruccionVariacion() {
        return areaConstruccionVariacion;
    }

    public void setAreaConstruccionVariacion(RangoDTO areaConstruccionVariacion) {
        this.areaConstruccionVariacion = areaConstruccionVariacion;
    }

    public RangoDTO getAvaluoInicial() {
        return avaluoInicial;
    }

    public void setAvaluoInicial(RangoDTO avaluoInicial) {
        this.avaluoInicial = avaluoInicial;
    }

    public RangoDTO getAvaluoFinal() {
        return avaluoFinal;
    }

    public void setAvaluoFinal(RangoDTO avaluoFinal) {
        this.avaluoFinal = avaluoFinal;
    }

    public RangoDTO getAvaluoVariacion() {
        return avaluoVariacion;
    }

    public void setAvaluoVariacion(RangoDTO avaluoVariacion) {
        this.avaluoVariacion = avaluoVariacion;
    }

    public String getDeptoCodigo() {
        return deptoCodigo;
    }

    public void setDeptoCodigo(String deptoCodigo) {
        this.deptoCodigo = deptoCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public int getVigencia() {
        return vigencia;
    }

    public void setVigencia(int vigencia) {
        this.vigencia = vigencia;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Map<String, String> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, String> filters) {
        this.filters = filters;
    }
}
