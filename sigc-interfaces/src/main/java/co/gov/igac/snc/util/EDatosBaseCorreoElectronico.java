/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con datos base en la estructura de un correo electrónico
 *
 * @author juan.agudelo
 */
public enum EDatosBaseCorreoElectronico {

    EXTENSION_EMAIL_INSTITUCIONAL_IGAC("@igac.gov.co");

    private String dato;

    private EDatosBaseCorreoElectronico(String dato) {
        this.dato = dato;
    }

    public String getDato() {
        return dato;
    }

}
