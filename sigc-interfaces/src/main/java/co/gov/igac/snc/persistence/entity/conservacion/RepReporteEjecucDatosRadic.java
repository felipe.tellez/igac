package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the REP_REPORTE_EJECUC_DATOS_RADIC database table.
 */
@Entity
@Table(name = "REP_REPORTE_EJECUC_DATOS_RADIC")
@NamedQuery(name = "RepReporteEjecucDatosRadic.findAll", query =
    "SELECT r FROM RepReporteEjecucDatosRadic r")
public class RepReporteEjecucDatosRadic implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;
    private String actividad;
    private String clasificacion;
    private String clasificacionTramite;
    private String descZona;
    private BigDecimal diasHabilesActividad;
    private BigDecimal diasHabilesProceso;
    private BigDecimal diasTranscurridos;
    private String estadoActividad;
    private String estadoTramite;
    private Date fechaElaboracion;
    private Date fechaFin;
    private Date fechaFinProceso;
    private Date fechaFinUltimaActividad;
    private Date fechaInicio;
    private Date fechaInicioProceso;
    private Date fechaInicioUltimaActividad;
    private String lugar;
    private String matriculaInmobiliaria;
    private String nombreDepartamento;
    private String nombreEjecutor;
    private String nombreFuncionario;
    private String nombreMunicipio;
    private String nombreTerritorial;
    private String nombreUoc;
    private String numeroPredial;
    private String numeroRadicacion;
    private String numeroResolucion;
    private String rol;
    private String subproceso;
    private String tipoTramite;
    private BigDecimal totalTramites;
    private BigDecimal totalTramitesOficina;
    private BigDecimal totalTramitesTerreno;
    private BigDecimal tramitesArchivados;
    private BigDecimal tramitesArchivadosOficina;
    private BigDecimal tramitesArchivadosTerreno;
    private BigDecimal tramitesAsignados;
    private BigDecimal tramitesAsignadosOficina;
    private BigDecimal tramitesAsignadosTerreno;
    private BigDecimal tramitesCancelados;
    private BigDecimal tramitesCanceladosOficina;
    private BigDecimal tramitesCanceladosTerreno;
    private BigDecimal tramitesFinalizados;
    private BigDecimal tramitesFinalizadosOficina;
    private BigDecimal tramitesFinalizadosTerreno;
    private BigDecimal tramitesGrupo1;
    private BigDecimal tramitesGrupo10;
    private BigDecimal tramitesGrupo2;
    private BigDecimal tramitesGrupo3;
    private BigDecimal tramitesGrupo4;
    private BigDecimal tramitesGrupo5;
    private BigDecimal tramitesGrupo6;
    private BigDecimal tramitesGrupo7;
    private BigDecimal tramitesGrupo8;
    private BigDecimal tramitesGrupo9;
    private BigDecimal tramitesRadicados;
    private BigDecimal tramitesRechazados;
    private BigDecimal tramitesRechazadosOficina;
    private BigDecimal tramitesRechazadosTerreno;
    private String ultimaActividadEjecutor;
    private String ultimaActividadTramite;
    private String ultimoSubproceso;
    private String usuarioUltimaActividad;
    private RepReporteEjecucion repReporteEjecucion;

    public RepReporteEjecucDatosRadic() {
    }

    @Id
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getActividad() {
        return this.actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getClasificacion() {
        return this.clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    @Column(name = "CLASIFICACION_TRAMITE")
    public String getClasificacionTramite() {
        return this.clasificacionTramite;
    }

    public void setClasificacionTramite(String clasificacionTramite) {
        this.clasificacionTramite = clasificacionTramite;
    }

    @Column(name = "DESC_ZONA")
    public String getDescZona() {
        return this.descZona;
    }

    public void setDescZona(String descZona) {
        this.descZona = descZona;
    }

    @Column(name = "DIAS_HABILES_ACTIVIDAD")
    public BigDecimal getDiasHabilesActividad() {
        return this.diasHabilesActividad;
    }

    public void setDiasHabilesActividad(BigDecimal diasHabilesActividad) {
        this.diasHabilesActividad = diasHabilesActividad;
    }

    @Column(name = "DIAS_HABILES_PROCESO")
    public BigDecimal getDiasHabilesProceso() {
        return this.diasHabilesProceso;
    }

    public void setDiasHabilesProceso(BigDecimal diasHabilesProceso) {
        this.diasHabilesProceso = diasHabilesProceso;
    }

    @Column(name = "DIAS_TRANSCURRIDOS")
    public BigDecimal getDiasTranscurridos() {
        return this.diasTranscurridos;
    }

    public void setDiasTranscurridos(BigDecimal diasTranscurridos) {
        this.diasTranscurridos = diasTranscurridos;
    }

    @Column(name = "ESTADO_ACTIVIDAD")
    public String getEstadoActividad() {
        return this.estadoActividad;
    }

    public void setEstadoActividad(String estadoActividad) {
        this.estadoActividad = estadoActividad;
    }

    @Column(name = "ESTADO_TRAMITE")
    public String getEstadoTramite() {
        return this.estadoTramite;
    }

    public void setEstadoTramite(String estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ELABORACION")
    public Date getFechaElaboracion() {
        return this.fechaElaboracion;
    }

    public void setFechaElaboracion(Date fechaElaboracion) {
        this.fechaElaboracion = fechaElaboracion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_PROCESO")
    public Date getFechaFinProceso() {
        return this.fechaFinProceso;
    }

    public void setFechaFinProceso(Date fechaFinProceso) {
        this.fechaFinProceso = fechaFinProceso;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_ULTIMA_ACTIVIDAD")
    public Date getFechaFinUltimaActividad() {
        return this.fechaFinUltimaActividad;
    }

    public void setFechaFinUltimaActividad(Date fechaFinUltimaActividad) {
        this.fechaFinUltimaActividad = fechaFinUltimaActividad;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_PROCESO")
    public Date getFechaInicioProceso() {
        return this.fechaInicioProceso;
    }

    public void setFechaInicioProceso(Date fechaInicioProceso) {
        this.fechaInicioProceso = fechaInicioProceso;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_ULTIMA_ACTIVIDAD")
    public Date getFechaInicioUltimaActividad() {
        return this.fechaInicioUltimaActividad;
    }

    public void setFechaInicioUltimaActividad(Date fechaInicioUltimaActividad) {
        this.fechaInicioUltimaActividad = fechaInicioUltimaActividad;
    }

    public String getLugar() {
        return this.lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    @Column(name = "MATRICULA_INMOBILIARIA")
    public String getMatriculaInmobiliaria() {
        return this.matriculaInmobiliaria;
    }

    public void setMatriculaInmobiliaria(String matriculaInmobiliaria) {
        this.matriculaInmobiliaria = matriculaInmobiliaria;
    }

    @Column(name = "NOMBRE_DEPARTAMENTO")
    public String getNombreDepartamento() {
        return this.nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    @Column(name = "NOMBRE_EJECUTOR")
    public String getNombreEjecutor() {
        return this.nombreEjecutor;
    }

    public void setNombreEjecutor(String nombreEjecutor) {
        this.nombreEjecutor = nombreEjecutor;
    }

    @Column(name = "NOMBRE_FUNCIONARIO")
    public String getNombreFuncionario() {
        return this.nombreFuncionario;
    }

    public void setNombreFuncionario(String nombreFuncionario) {
        this.nombreFuncionario = nombreFuncionario;
    }

    @Column(name = "NOMBRE_MUNICIPIO")
    public String getNombreMunicipio() {
        return this.nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    @Column(name = "NOMBRE_TERRITORIAL")
    public String getNombreTerritorial() {
        return this.nombreTerritorial;
    }

    public void setNombreTerritorial(String nombreTerritorial) {
        this.nombreTerritorial = nombreTerritorial;
    }

    @Column(name = "NOMBRE_UOC")
    public String getNombreUoc() {
        return this.nombreUoc;
    }

    public void setNombreUoc(String nombreUoc) {
        this.nombreUoc = nombreUoc;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_RADICACION")
    public String getNumeroRadicacion() {
        return this.numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    @Column(name = "NUMERO_RESOLUCION")
    public String getNumeroResolucion() {
        return this.numeroResolucion;
    }

    public void setNumeroResolucion(String numeroResolucion) {
        this.numeroResolucion = numeroResolucion;
    }

    public String getRol() {
        return this.rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getSubproceso() {
        return this.subproceso;
    }

    public void setSubproceso(String subproceso) {
        this.subproceso = subproceso;
    }

    @Column(name = "TIPO_TRAMITE")
    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @Column(name = "TOTAL_TRAMITES")
    public BigDecimal getTotalTramites() {
        return this.totalTramites;
    }

    public void setTotalTramites(BigDecimal totalTramites) {
        this.totalTramites = totalTramites;
    }

    @Column(name = "TOTAL_TRAMITES_OFICINA")
    public BigDecimal getTotalTramitesOficina() {
        return this.totalTramitesOficina;
    }

    public void setTotalTramitesOficina(BigDecimal totalTramitesOficina) {
        this.totalTramitesOficina = totalTramitesOficina;
    }

    @Column(name = "TOTAL_TRAMITES_TERRENO")
    public BigDecimal getTotalTramitesTerreno() {
        return this.totalTramitesTerreno;
    }

    public void setTotalTramitesTerreno(BigDecimal totalTramitesTerreno) {
        this.totalTramitesTerreno = totalTramitesTerreno;
    }

    @Column(name = "TRAMITES_ARCHIVADOS")
    public BigDecimal getTramitesArchivados() {
        return this.tramitesArchivados;
    }

    public void setTramitesArchivados(BigDecimal tramitesArchivados) {
        this.tramitesArchivados = tramitesArchivados;
    }

    @Column(name = "TRAMITES_ARCHIVADOS_OFICINA")
    public BigDecimal getTramitesArchivadosOficina() {
        return this.tramitesArchivadosOficina;
    }

    public void setTramitesArchivadosOficina(BigDecimal tramitesArchivadosOficina) {
        this.tramitesArchivadosOficina = tramitesArchivadosOficina;
    }

    @Column(name = "TRAMITES_ARCHIVADOS_TERRENO")
    public BigDecimal getTramitesArchivadosTerreno() {
        return this.tramitesArchivadosTerreno;
    }

    public void setTramitesArchivadosTerreno(BigDecimal tramitesArchivadosTerreno) {
        this.tramitesArchivadosTerreno = tramitesArchivadosTerreno;
    }

    @Column(name = "TRAMITES_ASIGNADOS")
    public BigDecimal getTramitesAsignados() {
        return this.tramitesAsignados;
    }

    public void setTramitesAsignados(BigDecimal tramitesAsignados) {
        this.tramitesAsignados = tramitesAsignados;
    }

    @Column(name = "TRAMITES_ASIGNADOS_OFICINA")
    public BigDecimal getTramitesAsignadosOficina() {
        return this.tramitesAsignadosOficina;
    }

    public void setTramitesAsignadosOficina(BigDecimal tramitesAsignadosOficina) {
        this.tramitesAsignadosOficina = tramitesAsignadosOficina;
    }

    @Column(name = "TRAMITES_ASIGNADOS_TERRENO")
    public BigDecimal getTramitesAsignadosTerreno() {
        return this.tramitesAsignadosTerreno;
    }

    public void setTramitesAsignadosTerreno(BigDecimal tramitesAsignadosTerreno) {
        this.tramitesAsignadosTerreno = tramitesAsignadosTerreno;
    }

    @Column(name = "TRAMITES_CANCELADOS")
    public BigDecimal getTramitesCancelados() {
        return this.tramitesCancelados;
    }

    public void setTramitesCancelados(BigDecimal tramitesCancelados) {
        this.tramitesCancelados = tramitesCancelados;
    }

    @Column(name = "TRAMITES_CANCELADOS_OFICINA")
    public BigDecimal getTramitesCanceladosOficina() {
        return this.tramitesCanceladosOficina;
    }

    public void setTramitesCanceladosOficina(BigDecimal tramitesCanceladosOficina) {
        this.tramitesCanceladosOficina = tramitesCanceladosOficina;
    }

    @Column(name = "TRAMITES_CANCELADOS_TERRENO")
    public BigDecimal getTramitesCanceladosTerreno() {
        return this.tramitesCanceladosTerreno;
    }

    public void setTramitesCanceladosTerreno(BigDecimal tramitesCanceladosTerreno) {
        this.tramitesCanceladosTerreno = tramitesCanceladosTerreno;
    }

    @Column(name = "TRAMITES_FINALIZADOS")
    public BigDecimal getTramitesFinalizados() {
        return this.tramitesFinalizados;
    }

    public void setTramitesFinalizados(BigDecimal tramitesFinalizados) {
        this.tramitesFinalizados = tramitesFinalizados;
    }

    @Column(name = "TRAMITES_FINALIZADOS_OFICINA")
    public BigDecimal getTramitesFinalizadosOficina() {
        return this.tramitesFinalizadosOficina;
    }

    public void setTramitesFinalizadosOficina(BigDecimal tramitesFinalizadosOficina) {
        this.tramitesFinalizadosOficina = tramitesFinalizadosOficina;
    }

    @Column(name = "TRAMITES_FINALIZADOS_TERRENO")
    public BigDecimal getTramitesFinalizadosTerreno() {
        return this.tramitesFinalizadosTerreno;
    }

    public void setTramitesFinalizadosTerreno(BigDecimal tramitesFinalizadosTerreno) {
        this.tramitesFinalizadosTerreno = tramitesFinalizadosTerreno;
    }

    @Column(name = "TRAMITES_GRUPO_1")
    public BigDecimal getTramitesGrupo1() {
        return this.tramitesGrupo1;
    }

    public void setTramitesGrupo1(BigDecimal tramitesGrupo1) {
        this.tramitesGrupo1 = tramitesGrupo1;
    }

    @Column(name = "TRAMITES_GRUPO_10")
    public BigDecimal getTramitesGrupo10() {
        return this.tramitesGrupo10;
    }

    public void setTramitesGrupo10(BigDecimal tramitesGrupo10) {
        this.tramitesGrupo10 = tramitesGrupo10;
    }

    @Column(name = "TRAMITES_GRUPO_2")
    public BigDecimal getTramitesGrupo2() {
        return this.tramitesGrupo2;
    }

    public void setTramitesGrupo2(BigDecimal tramitesGrupo2) {
        this.tramitesGrupo2 = tramitesGrupo2;
    }

    @Column(name = "TRAMITES_GRUPO_3")
    public BigDecimal getTramitesGrupo3() {
        return this.tramitesGrupo3;
    }

    public void setTramitesGrupo3(BigDecimal tramitesGrupo3) {
        this.tramitesGrupo3 = tramitesGrupo3;
    }

    @Column(name = "TRAMITES_GRUPO_4")
    public BigDecimal getTramitesGrupo4() {
        return this.tramitesGrupo4;
    }

    public void setTramitesGrupo4(BigDecimal tramitesGrupo4) {
        this.tramitesGrupo4 = tramitesGrupo4;
    }

    @Column(name = "TRAMITES_GRUPO_5")
    public BigDecimal getTramitesGrupo5() {
        return this.tramitesGrupo5;
    }

    public void setTramitesGrupo5(BigDecimal tramitesGrupo5) {
        this.tramitesGrupo5 = tramitesGrupo5;
    }

    @Column(name = "TRAMITES_GRUPO_6")
    public BigDecimal getTramitesGrupo6() {
        return this.tramitesGrupo6;
    }

    public void setTramitesGrupo6(BigDecimal tramitesGrupo6) {
        this.tramitesGrupo6 = tramitesGrupo6;
    }

    @Column(name = "TRAMITES_GRUPO_7")
    public BigDecimal getTramitesGrupo7() {
        return this.tramitesGrupo7;
    }

    public void setTramitesGrupo7(BigDecimal tramitesGrupo7) {
        this.tramitesGrupo7 = tramitesGrupo7;
    }

    @Column(name = "TRAMITES_GRUPO_8")
    public BigDecimal getTramitesGrupo8() {
        return this.tramitesGrupo8;
    }

    public void setTramitesGrupo8(BigDecimal tramitesGrupo8) {
        this.tramitesGrupo8 = tramitesGrupo8;
    }

    @Column(name = "TRAMITES_GRUPO_9")
    public BigDecimal getTramitesGrupo9() {
        return this.tramitesGrupo9;
    }

    public void setTramitesGrupo9(BigDecimal tramitesGrupo9) {
        this.tramitesGrupo9 = tramitesGrupo9;
    }

    @Column(name = "TRAMITES_RADICADOS")
    public BigDecimal getTramitesRadicados() {
        return this.tramitesRadicados;
    }

    public void setTramitesRadicados(BigDecimal tramitesRadicados) {
        this.tramitesRadicados = tramitesRadicados;
    }

    @Column(name = "TRAMITES_RECHAZADOS")
    public BigDecimal getTramitesRechazados() {
        return this.tramitesRechazados;
    }

    public void setTramitesRechazados(BigDecimal tramitesRechazados) {
        this.tramitesRechazados = tramitesRechazados;
    }

    @Column(name = "TRAMITES_RECHAZADOS_OFICINA")
    public BigDecimal getTramitesRechazadosOficina() {
        return this.tramitesRechazadosOficina;
    }

    public void setTramitesRechazadosOficina(BigDecimal tramitesRechazadosOficina) {
        this.tramitesRechazadosOficina = tramitesRechazadosOficina;
    }

    @Column(name = "TRAMITES_RECHAZADOS_TERRENO")
    public BigDecimal getTramitesRechazadosTerreno() {
        return this.tramitesRechazadosTerreno;
    }

    public void setTramitesRechazadosTerreno(BigDecimal tramitesRechazadosTerreno) {
        this.tramitesRechazadosTerreno = tramitesRechazadosTerreno;
    }

    @Column(name = "ULTIMA_ACTIVIDAD_EJECUTOR")
    public String getUltimaActividadEjecutor() {
        return this.ultimaActividadEjecutor;
    }

    public void setUltimaActividadEjecutor(String ultimaActividadEjecutor) {
        this.ultimaActividadEjecutor = ultimaActividadEjecutor;
    }

    @Column(name = "ULTIMA_ACTIVIDAD_TRAMITE")
    public String getUltimaActividadTramite() {
        return this.ultimaActividadTramite;
    }

    public void setUltimaActividadTramite(String ultimaActividadTramite) {
        this.ultimaActividadTramite = ultimaActividadTramite;
    }

    @Column(name = "ULTIMO_SUBPROCESO")
    public String getUltimoSubproceso() {
        return this.ultimoSubproceso;
    }

    public void setUltimoSubproceso(String ultimoSubproceso) {
        this.ultimoSubproceso = ultimoSubproceso;
    }

    @Column(name = "USUARIO_ULTIMA_ACTIVIDAD")
    public String getUsuarioUltimaActividad() {
        return this.usuarioUltimaActividad;
    }

    public void setUsuarioUltimaActividad(String usuarioUltimaActividad) {
        this.usuarioUltimaActividad = usuarioUltimaActividad;
    }

    //bi-directional many-to-one association to RepReporteEjecucion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REPORTE_EJECUCION_ID")
    public RepReporteEjecucion getRepReporteEjecucion() {
        return this.repReporteEjecucion;
    }

    public void setRepReporteEjecucion(RepReporteEjecucion repReporteEjecucion) {
        this.repReporteEjecucion = repReporteEjecucion;
    }

}
