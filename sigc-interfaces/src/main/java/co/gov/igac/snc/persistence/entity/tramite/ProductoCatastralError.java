package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the PRODUCTO_CATASTRAL_ERROR database table.
 *
 */
@Entity
@Table(name = "PRODUCTO_CATASTRAL_ERROR")
public class ProductoCatastralError implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long productoCatastralId;
    private Date fechaLog;
    private String inconsistencias;
    private String usuarioLog;

    public ProductoCatastralError() {
    }

    @Id
    @Column(name = "PRODUCTO_CATASTRAL_ID")
    public Long getProductoCatastralId() {
        return productoCatastralId;
    }

    public void setProductoCatastralId(Long productoCatastralId) {
        this.productoCatastralId = productoCatastralId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getInconsistencias() {
        return this.inconsistencias;
    }

    public void setInconsistencias(String inconsistencias) {
        this.inconsistencias = inconsistencias;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
