package co.gov.igac.snc.persistence.entity.generales;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Componente entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "COMPONENTE", schema = "SNC_GENERALES", uniqueConstraints = @UniqueConstraint(
    columnNames = "NOMBRE"))
public class Componente implements java.io.Serializable {

    // Fields
    private Long id;
    private Componente componente;
    private String nombre;
    private String tipo;
    private Byte orden;
    private String url;
    private String urlAyuda;
    private String activo;
    private String descripcion;
    private String usuarioLog;
    private Date fechaLog;
    private Set<Componente> componentes = new HashSet<Componente>(0);
    private Set<GrupoComponente> grupoComponentes = new HashSet<GrupoComponente>(
        0);

    // Constructors
    /** default constructor */
    public Componente() {
    }

    /** minimal constructor */
    public Componente(Long id, String nombre, String tipo, Byte orden,
        String activo, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.tipo = tipo;
        this.orden = orden;
        this.activo = activo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Componente(Long id, Componente componente, String nombre,
        String tipo, Byte orden, String url, String urlAyuda,
        String activo, String descripcion, String usuarioLog,
        Date fechaLog, Set<Componente> componentes,
        Set<GrupoComponente> grupoComponentes) {
        this.id = id;
        this.componente = componente;
        this.nombre = nombre;
        this.tipo = tipo;
        this.orden = orden;
        this.url = url;
        this.urlAyuda = urlAyuda;
        this.activo = activo;
        this.descripcion = descripcion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.componentes = componentes;
        this.grupoComponentes = grupoComponentes;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PADRE_COMPONENTE_ID")
    public Componente getComponente() {
        return this.componente;
    }

    public void setComponente(Componente componente) {
        this.componente = componente;
    }

    @Column(name = "NOMBRE", unique = true, nullable = false, length = 250)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "ORDEN", nullable = false, precision = 2, scale = 0)
    public Byte getOrden() {
        return this.orden;
    }

    public void setOrden(Byte orden) {
        this.orden = orden;
    }

    @Column(name = "URL", length = 600)
    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "URL_AYUDA", length = 600)
    public String getUrlAyuda() {
        return this.urlAyuda;
    }

    public void setUrlAyuda(String urlAyuda) {
        this.urlAyuda = urlAyuda;
    }

    @Column(name = "ACTIVO", nullable = false, length = 2)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "DESCRIPCION", length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "componente")
    public Set<Componente> getComponentes() {
        return this.componentes;
    }

    public void setComponentes(Set<Componente> componentes) {
        this.componentes = componentes;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "componente")
    public Set<GrupoComponente> getGrupoComponentes() {
        return this.grupoComponentes;
    }

    public void setGrupoComponentes(Set<GrupoComponente> grupoComponentes) {
        this.grupoComponentes = grupoComponentes;
    }

}
