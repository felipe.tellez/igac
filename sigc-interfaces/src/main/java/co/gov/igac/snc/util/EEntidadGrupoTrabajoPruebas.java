/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con grupos de trabajo o entidades a ejecutar las pruebas
 *
 * @author pedro.garcia
 */
public enum EEntidadGrupoTrabajoPruebas {

    //D: nombre enum(codigo en BD)
    //D: los códigos de los dominios son varchar2 en la bd
    ENTIDAD_EXTERNA("Entidad externa"),
    GRUPO_DE_TRABAJO_INTERNO("Grupo trabajo interno"),
    TERRITORIAL_AREA_DE_CONSERVACION("Territorial área conservación");

    private String nombre;

    private EEntidadGrupoTrabajoPruebas(String codigoP) {
        this.nombre = codigoP;
    }

    public String getNombre() {
        return this.nombre;
    }

}
