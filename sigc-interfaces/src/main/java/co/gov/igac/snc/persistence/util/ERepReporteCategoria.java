/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;

/**
 * Enumeración con las categorías de un {@link RepReporte}
 *
 * @author david.cifuentes
 *
 */
public enum ERepReporteCategoria {

    CIERRE_ANUAL_DEPARTAMENTAL("CIERRE ANUAL DEPARTAMENTAL", ""),
    CIERRE_ANUAL_GENERAL("CIERRE ANUAL GENERAL", ""),
    CIERRE_ANUAL_MUNICIPAL("CIERRE ANUAL MUNICIPAL", ""),
    CIERRE_ANUAL_PROCESO("CIERRE ANUAL PROCESO", ""),
    LISTADO_PREDIOS("LISTADO PREDIOS", ""),
    REPORTES_RADICACION("REPORTES RADICACION", "Consulta Reportes de radicación"),
    REPORTES_ESTADISTICOS("REPORTES ESTADISTICOS", "Consulta Reportes Estadisticos"),
    REPORTES_PREDIALES("REPORTES PREDIALES", "Consulta Reportes Prediales");

    private final String categoria;
    private final String nombreComponente;

    private ERepReporteCategoria(String categoria, String nombreComponete) {
        this.categoria = categoria;
        this.nombreComponente = nombreComponete;
    }

    public String getCategoria() {
        return this.categoria;
    }

    public String getNombreComponente() {
        return nombreComponente;
    }

}
