package co.gov.igac.snc.vo;

import java.io.Serializable;

import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;

/**
 * Clase que reduce la informacion de las unidades de construccion proyectadas para ser mostradas en
 * el visor
 *
 * @author franz.gamba
 */
public class PUnidadConstruccionVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4753043509093174458L;
    private String unidad;
    private String numeroPredial;
    private String cancelaInscribe;
    private Double areaConstruccionUnidad;
    private Integer numeroBanios;
    private Integer numeroHabitaciones;
    private Integer numeroLocales;
    private Integer numeroPisos;
    private Double puntaje;
    private String usoConstruccion;
    private String tipoConstruccion;
    private String tipoDominio;
    private String provienePredio;
    private String provienePredioDireccion;
    private String provieneUnidad;
    private String pisoUbicacion;

    /**
     * Full constructor
     *
     * @param unidad
     * @param numeroPredial
     * @param cancelaInscribe
     * @param areaConstruccionUnidad
     * @param numeroBanios
     * @param numeroHabitaciones
     * @param numeroLocales
     * @param numeroPisos
     * @param puntaje
     * @param usoConstruccion
     * @param tipoConstruccion
     * @param provienePredio
     * @param provienePredioDireccion
     * @param provieneUnidad
     */
    public PUnidadConstruccionVO(String unidad, String numeroPredial, String cancelaInscribe,
        Double areaConstruccionUnidad, Integer numeroBanios,
        Integer numeroHabitaciones, Integer numeroLocales, Integer numeroPisos,
        Double puntaje, String usoConstruccion, String tipoConstruccion,
        String provienePredio, String provienePredioDireccion, String provieneUnidad) {
        super();
        this.unidad = unidad;
        this.numeroPredial = numeroPredial;
        this.cancelaInscribe = cancelaInscribe;
        this.areaConstruccionUnidad = areaConstruccionUnidad;
        this.numeroBanios = numeroBanios;
        this.numeroHabitaciones = numeroHabitaciones;
        this.numeroLocales = numeroLocales;
        this.numeroPisos = numeroPisos;
        this.puntaje = puntaje;
        this.usoConstruccion = usoConstruccion;
        this.tipoConstruccion = tipoConstruccion;
        this.provienePredio = provienePredio;
        this.provienePredioDireccion = provienePredioDireccion;
        this.provieneUnidad = provieneUnidad;
    }

    /**
     * Empty constructor
     */
    public PUnidadConstruccionVO() {

    }

    /**
     * Constructor a partir de una P unidad de construccion
     */
    public PUnidadConstruccionVO(PUnidadConstruccion unidad) {
        this.unidad = unidad.getUnidad();
        this.numeroPredial = unidad.getPPredio().getNumeroPredial();
        this.areaConstruccionUnidad = unidad.getAreaConstruida();
        this.numeroBanios = unidad.getTotalBanios();
        this.numeroHabitaciones = unidad.getTotalHabitaciones();
        this.numeroLocales = unidad.getTotalLocales();
        this.numeroPisos = unidad.getTotalPisosConstruccion();
        this.puntaje = unidad.getTotalPuntaje();
        if (unidad.getUsoConstruccion() != null) {
            this.usoConstruccion = unidad.getUsoConstruccion().getNombre();
        }
        this.tipoConstruccion = unidad.getTipoConstruccion();
        this.tipoDominio = unidad.getTipoDominio();

        if (unidad.getPisoUbicacion() != null) {
            this.pisoUbicacion = unidad.getPisoUbicacion();
        }

        if (unidad.getProvieneUnidad() != null) {
            this.provieneUnidad = unidad.getProvieneUnidad();
        }

        if (unidad.getProvienePredio() != null) {
            this.provienePredio = unidad.getProvienePredio().getNumeroPredial();
        }

        if (unidad.getCancelaInscribe() != null) {
            this.cancelaInscribe = unidad.getCancelaInscribe();
        }

        if (unidad.getProvienePredio() != null &&
            unidad.getProvienePredio().getDireccionPrincipal() != null) {
            this.provienePredioDireccion = unidad.getProvienePredio().getDireccionPrincipal();
        }
    }

    /**
     *
     */
    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getCancelaInscribe() {
        return cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    public Double getAreaConstruccionUnidad() {
        return areaConstruccionUnidad;
    }

    public void setAreaConstruccionUnidad(Double areaConstruccionUnidad) {
        this.areaConstruccionUnidad = areaConstruccionUnidad;
    }

    public Integer getNumeroBanios() {
        return numeroBanios;
    }

    public void setNumeroBanios(Integer numeroBanios) {
        this.numeroBanios = numeroBanios;
    }

    public Integer getNumeroHabitaciones() {
        return numeroHabitaciones;
    }

    public void setNumeroHabitaciones(Integer numeroHabitaciones) {
        this.numeroHabitaciones = numeroHabitaciones;
    }

    public Integer getNumeroLocales() {
        return numeroLocales;
    }

    public void setNumeroLocales(Integer numeroLocales) {
        this.numeroLocales = numeroLocales;
    }

    public Integer getNumeroPisos() {
        return numeroPisos;
    }

    public void setNumeroPisos(Integer numeroPisos) {
        this.numeroPisos = numeroPisos;
    }

    public Double getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(Double puntaje) {
        this.puntaje = puntaje;
    }

    public String getUsoConstruccion() {
        return usoConstruccion;
    }

    public void setUsoConstruccion(String usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    public String getTipoConstruccion() {
        return tipoConstruccion;
    }

    public void setTipoConstruccion(String tipoConstruccion) {
        this.tipoConstruccion = tipoConstruccion;
    }

    public String getProvienePredio() {
        return provienePredio;
    }

    public void setProvienePredio(String provienePredio) {
        this.provienePredio = provienePredio;
    }

    public String getProvienePredioDireccion() {
        return provienePredioDireccion;
    }

    public void setProvienePredioDireccion(String provienePredioDireccion) {
        this.provienePredioDireccion = provienePredioDireccion;
    }

    public String getProvieneUnidad() {
        return provieneUnidad;
    }

    public void setProvieneUnidad(String provieneUnidad) {
        this.provieneUnidad = provieneUnidad;
    }

    public String getPisoUbicacion() {
        return pisoUbicacion;
    }

    public void setPisoUbicacion(String pisoUbicacion) {
        this.pisoUbicacion = pisoUbicacion;
    }

    public String getTipoDominio() {
        return tipoDominio;
    }

    public void setTipoDominio(String tipoDominio) {
        this.tipoDominio = tipoDominio;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }
}
