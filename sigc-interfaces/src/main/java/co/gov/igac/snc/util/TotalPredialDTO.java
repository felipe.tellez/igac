package co.gov.igac.snc.util;

import java.io.Serializable;
import java.math.BigDecimal;

public class TotalPredialDTO implements Serializable {

    private Integer ano;
    private String zona;
    private BigDecimal totalPredios;
    private BigDecimal totalPropietarios;
    private BigDecimal areaTerreno;
    private BigDecimal areaConstruccion;
    private BigDecimal totalAvaluos;

    public TotalPredialDTO(Integer ano,
        String zona, BigDecimal totalPredios, BigDecimal totalPropietarios,
        BigDecimal areaTerreno, BigDecimal areaConstruccion,
        BigDecimal totalAvaluos) {
        super();
        this.ano = ano;
        this.zona = zona;
        this.totalPredios = totalPredios;
        this.totalPropietarios = totalPropietarios;
        this.areaTerreno = areaTerreno;
        this.areaConstruccion = areaConstruccion;
        this.totalAvaluos = totalAvaluos;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public BigDecimal getTotalPredios() {
        return totalPredios;
    }

    public void setTotalPredios(BigDecimal totalPredios) {
        this.totalPredios = totalPredios;
    }

    public BigDecimal getTotalPropietarios() {
        return totalPropietarios;
    }

    public void setTotalPropietarios(BigDecimal totalPropietarios) {
        this.totalPropietarios = totalPropietarios;
    }

    public BigDecimal getAreaTerreno() {
        return areaTerreno;
    }

    public void setAreaTerreno(BigDecimal areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    public BigDecimal getAreaConstruccion() {
        return areaConstruccion;
    }

    public void setAreaConstruccion(BigDecimal areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    public BigDecimal getTotalAvaluos() {
        return totalAvaluos;
    }

    public void setTotalAvaluos(BigDecimal totalAvaluos) {
        this.totalAvaluos = totalAvaluos;
    }

    public String getZonaCompleta() {
        if (zona != null) {
            if (zona.equals("R")) {
                return "Rural";
            }
            if (zona.equals("U")) {
                return "Urbano";
            }
            if (zona.equals("C")) {
                return "Corregimiento";
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

}
