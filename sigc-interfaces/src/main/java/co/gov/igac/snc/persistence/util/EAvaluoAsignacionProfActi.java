/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los tipos de avalúo de un avalúo. Corresponde al dominio
 * AVALUO_ASIGNACION_PROF_ACTI
 *
 * @author christian.rodriguez
 */
public enum EAvaluoAsignacionProfActi {

    AVALUAR("AVALUAR", "Avaluar"),
    CC_GIT("CC GIT", "Control Calidad Grupo Interno de Trabajo"),
    CC_SEDE_CENTRAL("CC SEDE CENTRAL", "Control Calidad Sede Central"),
    CC_TERRITORIAL("CC TERRITORIAL", "Control Calidad Territorial");

    private String codigo;
    private String valor;

    private EAvaluoAsignacionProfActi(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
