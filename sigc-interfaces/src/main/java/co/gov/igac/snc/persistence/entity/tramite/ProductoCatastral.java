package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * ProductoCatastral entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "PRODUCTO_CATASTRAL", schema = "SNC_TRAMITE")
public class ProductoCatastral implements java.io.Serializable {

    // Fields
    private Long id;
    private Integer cantidad;
    private String claseInconformidad;
    private String datosExtendidos;
    private String datosPropietarios;
    private String descripcionInconformidad;
    private String entregado;
    private String escala;
    private String estado;
    private Date fechaEntrega;
    private Date fechaFinGeneracion;
    private Date fechaInicioGeneracion;
    private Date fechaLog;
    private String formato;
    private String observaciones;
    private ProductoCatastralJob productoCatastralJob;
    private String productoGenerar;
    private String registraInconformidad;
    private String resultadoGeneracion;
    private String revisionAprobada;
    private boolean revisionAprobadaBool;
    private String rutaProductoEjecutado;
    private String usuarioLog;
    private Producto producto;
    private Solicitud solicitud;
    private List<ProductoCatastralDetalle> productoCatastralDetalles;
    private boolean activarRevisionAprobada;

    /**
     * Estados de ejecución de los jobs en SNC
     */
    public static final String SNC_EN_EJECUCION = "SNC_EN_EJECUCION";
    public static final String SNC_ERROR = "SNC_ERROR";
    public static final String SNC_TERMINADO = "SNC_TERMINADO";

    // Constructors
    /** default constructor */
    public ProductoCatastral() {
    }

    /** minimal constructor */
    public ProductoCatastral(Long id, Producto producto, Solicitud solicitud,
        Integer cantidad, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.producto = producto;
        this.solicitud = solicitud;
        this.cantidad = cantidad;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /**
     * full constructor
     *
     * @param productoGenerado
     * @param registraInconformidad
     * @param revisionAprobada
     * @param productoCatastralDetalles
     */
    public ProductoCatastral(Long id, Producto producto, Solicitud solicitud,
        String estado, Integer cantidad, String claseInconformidad,
        String descripcionInconformidad, String usuarioLog, Date fechaLog,
        String productoGenerar, String registraInconformidad,
        String revisionAprobada,
        List<ProductoCatastralDetalle> productoCatastralDetalles) {
        this.id = id;
        this.cantidad = cantidad;
        this.claseInconformidad = claseInconformidad;
        this.descripcionInconformidad = descripcionInconformidad;
        this.estado = estado;
        this.fechaLog = fechaLog;
        this.productoGenerar = productoGenerar;
        this.registraInconformidad = registraInconformidad;
        this.revisionAprobada = revisionAprobada;
        this.usuarioLog = usuarioLog;
        this.producto = producto;
        this.solicitud = solicitud;
        this.productoCatastralDetalles = productoCatastralDetalles;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCTO_CATASTRAL_ID_SEQ")
    @SequenceGenerator(name = "PRODUCTO_CATASTRAL_ID_SEQ", sequenceName =
        "PRODUCTO_CATASTRAL_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCTO_ID", nullable = false)
    public Producto getProducto() {
        return this.producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_ID", nullable = false)
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @Column(name = "ENTREGADO", length = 2)
    public String getEntregado() {
        return this.entregado;
    }

    public void setEntregado(String entregado) {
        this.entregado = entregado;
    }

    public String getEscala() {
        return this.escala;
    }

    public void setEscala(String escala) {
        this.escala = escala;
    }

    @Column(name = "ESTADO", length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ENTREGA")
    public Date getFechaEntrega() {
        return this.fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_GENERACION")
    public Date getFechaFinGeneracion() {
        return this.fechaFinGeneracion;
    }

    public void setFechaFinGeneracion(Date fechaFinGeneracion) {
        this.fechaFinGeneracion = fechaFinGeneracion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_GENERACION")
    public Date getFechaInicioGeneracion() {
        return this.fechaInicioGeneracion;
    }

    public void setFechaInicioGeneracion(Date fechaInicioGeneracion) {
        this.fechaInicioGeneracion = fechaInicioGeneracion;
    }

    @Column(name = "CANTIDAD", nullable = false, precision = 6, scale = 0)
    public Integer getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Column(name = "CLASE_INCONFORMIDAD", length = 30)
    public String getClaseInconformidad() {
        return this.claseInconformidad;
    }

    public void setClaseInconformidad(String claseInconformidad) {
        this.claseInconformidad = claseInconformidad;
    }

    @Column(name = "DATOS_EXTENDIDOS")
    public String getDatosExtendidos() {
        return this.datosExtendidos;
    }

    public void setDatosExtendidos(String datosExtendidos) {
        this.datosExtendidos = datosExtendidos;
    }

    @Column(name = "DATOS_PROPIETARIOS")
    public String getDatosPropietarios() {
        return this.datosPropietarios;
    }

    public void setDatosPropietarios(String datosPropietarios) {
        this.datosPropietarios = datosPropietarios;
    }

    @Column(name = "DESCRIPCION_INCONFORMIDAD", length = 2000)
    public String getDescripcionInconformidad() {
        return this.descripcionInconformidad;
    }

    public void setDescripcionInconformidad(String descripcionInconformidad) {
        this.descripcionInconformidad = descripcionInconformidad;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getFormato() {
        return this.formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCTO_CATASTRAL_JOB_ID")
    public ProductoCatastralJob getProductoCatastralJobId() {
        return this.productoCatastralJob;
    }

    public void setProductoCatastralJobId(ProductoCatastralJob productoCatastralJob) {
        this.productoCatastralJob = productoCatastralJob;
    }

    @Column(name = "PRODUCTO_GENERAR", length = 2)
    public String getProductoGenerar() {
        return productoGenerar;
    }

    public void setProductoGenerar(String productoGenerar) {
        this.productoGenerar = productoGenerar;
    }

    @Column(name = "REGISTRA_INCONFORMIDAD", length = 2)
    public String getRegistraInconformidad() {
        return this.registraInconformidad;
    }

    public void setRegistraInconformidad(String registraInconformidad) {
        this.registraInconformidad = registraInconformidad;
    }

    @Column(name = "RESULTADO_GENERACION")
    public String getResultadoGeneracion() {
        return this.resultadoGeneracion;
    }

    public void setResultadoGeneracion(String resultadoGeneracion) {
        this.resultadoGeneracion = resultadoGeneracion;
    }

    @Column(name = "RUTA_PRODUCTO_EJECUTADO")
    public String getRutaProductoEjecutado() {
        return this.rutaProductoEjecutado;
    }

    public void setRutaProductoEjecutado(String rutaProductoEjecutado) {
        this.rutaProductoEjecutado = rutaProductoEjecutado;
    }

    @Column(name = "REVISION_APROBADA", length = 2)
    public String getRevisionAprobada() {
        return this.revisionAprobada;
    }

    public void setRevisionAprobada(String revisionAprobada) {
        this.revisionAprobada = revisionAprobada;
    }

    @Transient
    public boolean isRevisionAprobadaBool() {

        this.revisionAprobadaBool = false;

        if (ESiNo.SI.getCodigo().equals(this.revisionAprobada)) {
            this.revisionAprobadaBool = true;
        }

        return revisionAprobadaBool;
    }

    public void setRevisionAprobadaBool(boolean revisionAprobadaBool) {

        this.revisionAprobada = ESiNo.NO.getCodigo();
        if (revisionAprobadaBool) {
            this.revisionAprobada = ESiNo.SI.getCodigo();
        }

        this.revisionAprobadaBool = revisionAprobadaBool;
    }

    // bi-directional many-to-one association to ProductoCatastralDetalle
    @OneToMany(mappedBy = "productoCatastral")
    public List<ProductoCatastralDetalle> getProductoCatastralDetalles() {
        return this.productoCatastralDetalles;
    }

    public void setProductoCatastralDetalles(
        List<ProductoCatastralDetalle> productoCatastralDetalles) {
        this.productoCatastralDetalles = productoCatastralDetalles;
    }

    @Transient
    public boolean isActivarRevisionAprobada() {
        return activarRevisionAprobada;
    }

    public void setActivarRevisionAprobada(boolean activarRevisionAprobada) {
        this.activarRevisionAprobada = activarRevisionAprobada;
    }

}
