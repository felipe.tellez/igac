/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los codigos de tipos de servicios comunales para ofertas
 *
 * @author christian.rodriguez
 */
public enum EOfertaServicioComunal {

    ASCENSOR("Ascensor"),
    CANCHA_BALONCESTO("Cancha Baloncesto"),
    CANCHA_MULTIPLE("Cancha Múltiple"),
    CANCHA_TENIS("Cancha Tenis"),
    GIMNASIO("Gimnasio"),
    OTRO("Otro"),
    PISCINA("Piscina"),
    SALON("Salón"),
    ZONAS_VERDES("Zonas Verdes");

    private String codigo;

    private EOfertaServicioComunal(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
