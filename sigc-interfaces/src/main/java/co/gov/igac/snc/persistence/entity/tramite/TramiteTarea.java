package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.snc.util.Constantes;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "TRAMITE_TAREA", schema = "SNC_TRAMITE")
public class TramiteTarea implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Tramite tramite;
    private String tarea;
    private String paso;
    private String resultado;
    private String mensaje;
    private Date fechaLog;
    private String usuarioLog;

    public TramiteTarea() {
    }

    public TramiteTarea(Tramite tramite, String tarea, String paso, String resultado, String mensaje,
        Date fechaLog, String usuarioLog) {
        this.tramite = tramite;
        this.tarea = tarea;
        this.paso = paso;
        this.resultado = resultado;
        this.mensaje = mensaje;
        this.fechaLog = fechaLog;
        this.usuarioLog = usuarioLog;
    }

    @Id
    @SequenceGenerator(name = "TRAMITE_TAREA_ID_GENERATOR", sequenceName = "TRAMITE_TAREA_ID_SEQ",
        allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_TAREA_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public String getTarea() {
        return tarea;
    }

    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    public String getPaso() {
        return paso;
    }

    public void setPaso(String paso) {
        this.paso = paso;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    /**
     * retorna el job asociado al envio geografico si se trata de una tarea geografica, de lo
     * contrario retorna null
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public Long getJobId() {

        Long jobId = null;
        if (this.mensaje.contains(Constantes.PREFIJO_JOB_ID)) {
            int idx = Constantes.PREFIJO_JOB_ID.length();
            String idString = this.mensaje.substring(idx);
            jobId = Long.valueOf(idString.trim());
        }

        return jobId;
    }

    // --------------------------------------- //
    /**
     *
     * Método que realiza un clone del objeto {@link TramiteTarea}
     *
     * @author felipe.cadena
     * @return
     */
    @Override
    @SuppressWarnings("CloneDeclaresCloneNotSupported")
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

}
