package co.gov.igac.snc.persistence.util;

public enum EPersonaPredioPropiedadTipoTitulo {
    ESCRITURA("1", "Escritura"),
    LAUDO_ADMINISTRATIVO("4", "Laudo Administrativo"),
    RESOLUCION("3", "Resolución"),
    SENTENCIA("2", "Sentencia"),
    CARTA_VENTA("5", "Carta Venta");

    private String codigo;
    private String valor;

    EPersonaPredioPropiedadTipoTitulo(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
