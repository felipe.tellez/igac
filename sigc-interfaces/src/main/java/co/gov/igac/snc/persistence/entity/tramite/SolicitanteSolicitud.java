package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitanteTipoSolicitant;
import co.gov.igac.snc.util.ISolicitanteSolicitudOTramite;

/**
 * SolicitanteSolicitud entity. @author MyEclipse Persistence Tools
 */
/*
 * @modified by fabio.navarrete -> Se agrega el método transient getNombreCompleto -> Se agrega el
 * método transient isJuridica -> Se modificó el método setTipoPersona -> se agrega el campo
 * serialVersionUID -> Se agrega el método incorporarDatosSolicitante -> Se agrega método Transient
 * isIntermediario que indica si el solicitante es un intermediario -> Se agrega método
 * isNotificacionEmailBoolean -> Se agrega método setNotificacionEmailBoolean -> Se agrega método
 * isNotificacionEmailBoolean -> Se agrega método setNotificacionEmailBoolean
 *
 * @modified pedro.garcia 27-03-2012 se hizo que implementara la interfaz
 * ISolicitanteSolicitudOTramite @modified rodrigo.hernandez 31-07-2012 se adicionaron metodos
 * hashCode() e equals() @modified rodrigo.hernandez 31-10-2012 se adicionó campo Cargo
 *
 * @modified by javier.aponte -> Se agrega el método transient isTesoreria 12/09/2013 @modified by
 * david.cifuentes :: Se agregan los métodos transient getTipoIdentificacionSolicitante y
 * getNumeroIdentificacionSolicitante :: 06/10/2016
 */
@Entity
@Table(name = "SOLICITANTE_SOLICITUD", schema = "SNC_TRAMITE")
@NamedQueries({@NamedQuery(name = "solicitanteSolicitud.findBySolicitudId", query =
        "SELECT ss FROM SolicitanteSolicitud ss WHERE ss.solicitud.id = :solicitudId")})
public class SolicitanteSolicitud implements java.io.Serializable, ISolicitanteSolicitudOTramite {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -1136803835237776058L;

    private Long id;
    private Solicitante solicitante;
    private Solicitud solicitud;
    private String tipoPersona;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String digitoVerificacion;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String razonSocial;
    private String sigla;
    private Pais direccionPais;
    private Departamento direccionDepartamento;
    private Municipio direccionMunicipio;
    private String direccion;
    private String telefonoPrincipal;
    private String telefonoPrincipalExt;
    private String telefonoSecundario;
    private String telefonoSecundarioExt;
    private String telefonoCelular;
    private String fax;
    private String faxExt;
    private String correoElectronico;
    private String correoElectronicoSecundario;
    private String notificacionEmail;
    private String usuarioLog;
    private Date fechaLog;
    private String tipoSolicitante;
    private String relacion;
    private String cargo;
    private String codigoPostal;

    // Constructors
    /** default constructor */
    public SolicitanteSolicitud() {
    }

    /** minimal constructor */
    public SolicitanteSolicitud(Long id, Solicitante solicitante,
        Solicitud solicitud, String notificacionEmail, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.solicitante = solicitante;
        this.solicitud = solicitud;
        this.notificacionEmail = notificacionEmail;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public SolicitanteSolicitud(Long id, Solicitante solicitante,
        Solicitud solicitud, String tipoPersona, String tipoIdentificacion,
        String tipoSolicitante, String numeroIdentificacion,
        String digitoVerificacion, String primerNombre,
        String segundoNombre, String primerApellido,
        String segundoApellido, String razonSocial, String sigla,
        Pais direccionPais, Departamento direccionDepartamento,
        Municipio direccionMunicipio, String direccion,
        String telefonoPrincipal, String telefonoPrincipalExt,
        String telefonoSecundario, String telefonoSecundarioExt,
        String telefonoCelular, String fax, String faxExt,
        String correoElectronico, String correoElectronicoSecundario,
        String relacion, String notificacionEmail, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.solicitante = solicitante;
        this.solicitud = solicitud;
        this.tipoPersona = tipoPersona;
        this.tipoSolicitante = tipoSolicitante;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.digitoVerificacion = digitoVerificacion;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.razonSocial = razonSocial;
        this.sigla = sigla;
        this.direccionPais = direccionPais;
        this.direccionDepartamento = direccionDepartamento;
        this.direccionMunicipio = direccionMunicipio;
        this.direccion = direccion;
        this.telefonoPrincipal = telefonoPrincipal;
        this.telefonoPrincipalExt = telefonoPrincipalExt;
        this.telefonoSecundario = telefonoSecundario;
        this.telefonoSecundarioExt = telefonoSecundarioExt;
        this.telefonoCelular = telefonoCelular;
        this.fax = fax;
        this.faxExt = faxExt;
        this.correoElectronico = correoElectronico;
        this.correoElectronicoSecundario = correoElectronicoSecundario;
        this.notificacionEmail = notificacionEmail;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.relacion = relacion;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SOLICITANTE_SOLICITUD_ID_SEQ")
    @SequenceGenerator(name = "SOLICITANTE_SOLICITUD_ID_SEQ", sequenceName =
        "SOLICITANTE_SOLICITUD_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITANTE_ID", nullable = false)
    public Solicitante getSolicitante() {
        return this.solicitante;
    }

    public void setSolicitante(Solicitante solicitante) {
        this.solicitante = solicitante;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_ID", nullable = false)
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @Column(name = "TIPO_PERSONA", length = 30)
    public String getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        if (tipoPersona != null && tipoPersona.equals(EPersonaTipoPersona.NATURAL.getCodigo())) {
            this.tipoSolicitante = (ESolicitanteTipoSolicitant.PRIVADA.getCodigo());
        }
        this.tipoPersona = tipoPersona;
    }

    @Column(name = "TIPO_SOLICITANTE", length = 30)
    public String getTipoSolicitante() {
        return this.tipoSolicitante;
    }

    public void setTipoSolicitante(String tipoSolicitante) {
        this.tipoSolicitante = tipoSolicitante;
    }

    @Column(name = "RELACION", length = 30)
    public String getRelacion() {
        return this.relacion;
    }

    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }

    @Column(name = "TIPO_IDENTIFICACION", length = 30)
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "NUMERO_IDENTIFICACION", length = 50)
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "DIGITO_VERIFICACION", length = 2)
    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    @Column(name = "PRIMER_NOMBRE", length = 100)
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Column(name = "SEGUNDO_NOMBRE", length = 100)
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Column(name = "PRIMER_APELLIDO", length = 100)
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "SEGUNDO_APELLIDO", length = 100)
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "RAZON_SOCIAL", length = 100)
    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Column(name = "SIGLA", length = 50)
    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_PAIS_CODIGO", nullable = true)
    public Pais getDireccionPais() {
        return this.direccionPais;
    }

    public void setDireccionPais(Pais direccionPais) {
        this.direccionPais = direccionPais;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_DEPARTAMENTO_CODIGO", nullable = true)
    public Departamento getDireccionDepartamento() {
        return this.direccionDepartamento;
    }

    public void setDireccionDepartamento(
        Departamento direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_MUNICIPIO_CODIGO", nullable = true)
    public Municipio getDireccionMunicipio() {
        return this.direccionMunicipio;
    }

    public void setDireccionMunicipio(Municipio direccionMunicipio) {
        this.direccionMunicipio = direccionMunicipio;
    }

    @Column(name = "DIRECCION", length = 100)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "TELEFONO_PRINCIPAL", length = 15)
    public String getTelefonoPrincipal() {
        return this.telefonoPrincipal;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    @Column(name = "TELEFONO_PRINCIPAL_EXT", length = 6)
    public String getTelefonoPrincipalExt() {
        return this.telefonoPrincipalExt;
    }

    public void setTelefonoPrincipalExt(String telefonoPrincipalExt) {
        this.telefonoPrincipalExt = telefonoPrincipalExt;
    }

    @Column(name = "TELEFONO_SECUNDARIO", length = 15)
    public String getTelefonoSecundario() {
        return this.telefonoSecundario;
    }

    public void setTelefonoSecundario(String telefonoSecundario) {
        this.telefonoSecundario = telefonoSecundario;
    }

    @Column(name = "TELEFONO_SECUNDARIO_EXT", length = 6)
    public String getTelefonoSecundarioExt() {
        return this.telefonoSecundarioExt;
    }

    public void setTelefonoSecundarioExt(String telefonoSecundarioExt) {
        this.telefonoSecundarioExt = telefonoSecundarioExt;
    }

    @Column(name = "TELEFONO_CELULAR", length = 15)
    public String getTelefonoCelular() {
        return this.telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    @Column(name = "FAX", length = 15)
    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Column(name = "FAX_EXT", length = 6)
    public String getFaxExt() {
        return this.faxExt;
    }

    public void setFaxExt(String faxExt) {
        this.faxExt = faxExt;
    }

    @Column(name = "CORREO_ELECTRONICO", length = 100)
    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Column(name = "CORREO_ELECTRONICO_SECUNDARIO", length = 100)
    public String getCorreoElectronicoSecundario() {
        return this.correoElectronicoSecundario;
    }

    public void setCorreoElectronicoSecundario(
        String correoElectronicoSecundario) {
        this.correoElectronicoSecundario = correoElectronicoSecundario;
    }

    @Column(name = "NOTIFICACION_EMAIL", nullable = false, length = 2)
    public String getNotificacionEmail() {
        return this.notificacionEmail;
    }

    public void setNotificacionEmail(String notificacionEmail) {
        this.notificacionEmail = notificacionEmail;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "CARGO", nullable = true, length = 100)
    public String getCargo() {
        return this.cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Column(name = "CODIGO_POSTAL")
    public String getCodigoPostal() {
        return this.codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * Método que retorna el nombre completo concatenando los campos para el solicitante.
     *
     * @author fabio.navarrete
     */
    @Transient
    public String getNombreCompleto() {
        StringBuffer sb = new StringBuffer();
        sb.append("");
        if ((this.tipoIdentificacion != null && (this.tipoIdentificacion
            .equals(EPersonaTipoIdentificacion.NIT.getCodigo()) || this.tipoIdentificacion
            .equals(EPersonaTipoIdentificacion.NO_ESPECIFICADO.getCodigo()))) ||
            (this.tipoPersona != null && this.tipoPersona
                .equals(EPersonaTipoPersona.JURIDICA.getCodigo()))) {
            if (this.razonSocial != null) {
                sb.append(this.razonSocial).append(" ");
            }
        } else {
            if (this.primerNombre != null) {
                sb.append(this.primerNombre).append(" ");
            }
            if (this.segundoNombre != null) {
                sb.append(this.segundoNombre).append(" ");
            }
            if (this.primerApellido != null) {
                sb.append(this.primerApellido).append(" ");
            }
            if (this.segundoApellido != null) {
                sb.append(this.segundoApellido).append(" ");
            }
        }
        return sb.toString();
    }

    /**
     * Método que indica si entre los datos duplicados del solicitante, éste es jurídico o no.
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isJuridica() {

        if (tipoPersona != null) {
            if (tipoPersona.equals(EPersonaTipoPersona.JURIDICA.getCodigo())) {
                this.tipoIdentificacion = EPersonaTipoIdentificacion.NIT
                    .getCodigo();
                return true;
            }
        }
        return false;
    }

    /**
     * Método que agrega los datos comunes al Solicitante y a SolicitanteSolicitud en el objeto
     * SolicitanteSolicitud
     *
     * @param sol Solicitante del cual se toman los datos.
     * @author fabio.navarrete
     */
    public void incorporarDatosSolicitante(Solicitante sol) {
        this.correoElectronico = sol.getCorreoElectronico();
        this.correoElectronicoSecundario = sol.getCorreoElectronicoSecundario();
        this.digitoVerificacion = sol.getDigitoVerificacion();
        this.direccion = sol.getDireccion();
        this.direccionDepartamento = sol.getDireccionDepartamento();
        this.direccionMunicipio = sol.getDireccionMunicipio();
        this.direccionPais = sol.getDireccionPais();
        this.fax = sol.getFax();
        this.faxExt = sol.getFaxExt();
        this.numeroIdentificacion = sol.getNumeroIdentificacion();
        this.primerApellido = sol.getPrimerApellido();
        this.primerNombre = sol.getPrimerNombre();
        this.razonSocial = sol.getRazonSocial();
        this.segundoApellido = sol.getSegundoApellido();
        this.segundoNombre = sol.getSegundoNombre();
        this.sigla = sol.getSigla();
        this.telefonoCelular = sol.getTelefonoCelular();
        this.telefonoPrincipal = sol.getTelefonoPrincipal();
        this.telefonoPrincipalExt = sol.getTelefonoPrincipalExt();
        this.telefonoSecundario = sol.getTelefonoSecundario();
        this.telefonoSecundarioExt = sol.getTelefonoSecundarioExt();
        this.tipoIdentificacion = sol.getTipoIdentificacion();
        this.tipoPersona = sol.getTipoPersona();
        this.tipoSolicitante = sol.getTipoSolicitante();
        this.relacion = sol.getRelacion();
        this.notificacionEmail = sol.getNotificacionEmail();
        if (this.notificacionEmail == null) {
            this.notificacionEmail = ESiNo.NO.getCodigo();
        }
    }

    /**
     * Indica si el objeto SolicitanteSolicitud es un intermediario
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isIntermediario() {
        if (this.relacion != null) {
            return this.relacion
                .equals(ESolicitanteSolicitudRelac.INTERMEDIARIO.toString());
        }
        return false;
    }

    /**
     * Indica si el objeto SolicitanteSolicitud es una tesoreria
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isTesoreria() {
        if (this.relacion != null) {
            return this.relacion
                .equals(ESolicitanteSolicitudRelac.TESORERIA.toString());
        }
        return false;
    }

    /**
     * Método encargado de retornar el valor sí o no del dominio a partir de un booleano
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isNotificacionEmailBoolean() {
        if (this.notificacionEmail != null) {
            return this.notificacionEmail.equals(ESiNo.SI.getCodigo());
        }
        return false;
    }

    /**
     * Método encargado de asignar el valor sí o no del dominio a partir de un booleano
     *
     * @param notificacionEmail
     * @author fabio.navarrete
     */
    public void setNotificacionEmailBoolean(boolean notificacionEmail) {
        if (notificacionEmail) {
            this.notificacionEmail = ESiNo.SI.getCodigo();
        } else {
            this.notificacionEmail = ESiNo.NO.getCodigo();
        }
    }

    /**
     * Método que indica el tipo de identificación del solicitante verificando la variable de
     * coincidencias del mismo seteada en radicación
     *
     * @return
     * @author david.cifuentes
     */
    @Transient
    public String getTipoIdentificacionSolicitante() {
        if (solicitante != null) {
            if (!solicitante.isSolicitanteCoincidente()) {
                return solicitante.getTipoIdentificacion();
            } else {
                return "ID";
            }
        } else {
            return getTipoIdentificacion();
        }
    }

    /**
     * Método que indica el número de identificación del solicitante verificando la variable de
     * coincidencias del mismo seteada en radicación
     *
     * @return
     * @author david.cifuentes
     */
    @Transient
    public String getNumeroIdentificacionSolicitante() {
        if (solicitante != null) {
            if (!solicitante.isSolicitanteCoincidente()) {
                return solicitante.getNumeroIdentificacion();
            } else {
                return solicitante.getId().toString();
            }
        } else {
            return getNumeroIdentificacion();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SolicitanteSolicitud other = (SolicitanteSolicitud) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
