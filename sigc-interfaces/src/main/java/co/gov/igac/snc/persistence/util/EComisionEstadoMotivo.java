/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * enumeración de los códigos y valores del dominio COMISION_ESTADO_MOTIVO
 *
 * @author pedro.garcia
 */
public enum EComisionEstadoMotivo {

    //D: por ahora no se agregan todos, solo los que interesan para la lógica
    //D: enum(codigo, valor)
    CANCELACION_TODOS_LOS_TRAMITES("26", "Cancelación de todos los trámites de la comisión"),
    COMISION_APROBADA("29", "Comisión aprobada"),
    COMISION_FINALIZADA("31", "Comisión finalizada"),
    COMISION_RECHAZADA("30", "Comisión rechazada"),
    CREACION("25", "Creación de la comisión"),
    GENERACION_MEMORANDO("24", "Generación del memorando de comisión"),
    OTRO("22", "Otro"),
    PENDIENTE_DE_APROBACION("28", "Pendiente por aprobar comisión"),
    RETIRO_TODOS_LOS_EJECUTORES("23",
        "Por retiro de todos los ejecutores de trámites asociados a la comisión"),
    RETIRO_TODOS_LOS_TRAMITES("27", "Por retiro de todos los trámites asociados a la comisión");

    private String codigo;
    private String valor;

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private EComisionEstadoMotivo(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

}
