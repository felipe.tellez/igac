package co.gov.igac.snc.persistence.util;

public enum ETipoCorrespondencia {
    EXTERNA_ENVIADA("EE"),
    EXTERNA_RECIBIDA("ER"),
    INTERNA_ENVIADA("IE"),
    INTERNA_RECIBIDA("IR");

    private String codigo;

    private ETipoCorrespondencia(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
