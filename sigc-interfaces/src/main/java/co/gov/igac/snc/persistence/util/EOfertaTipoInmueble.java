/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio de OFERTA_TIPO_INMUEBLE
 *
 * @author juan.agudelo
 */
//TODO::juan.agudelo::29-11-2011::ordenar esta vaina!!. Por orden alfabético y en una columna:: pedro.garcia
public enum EOfertaTipoInmueble {

    CASA("CASA"), APARTAMENTO("APARTAMENTO"), LOTE("LOTE"), CASALOTE("CASA LOTE"), SUITEHOTELERA(
        "SUITE HOTELERA"), LOCAL("LOCAL"), OFICINA("OFICINA"), CONSULTORIO(
        "CONSULTORIO"), BODEGA("BODEGA"), FINCA("FINCA"), EDIFICIO(
        "EDIFICIO"), PARQUEADERO("PARQUEADERO");

    private String codigo;

    private EOfertaTipoInmueble(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }
}
