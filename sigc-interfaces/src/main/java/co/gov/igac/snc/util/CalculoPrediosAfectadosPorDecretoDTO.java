package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * Objeto que almacena el calculo de los predios que fueron afectados por las condicionales de un
 * decreto.
 *
 * @author david.cifuentes
 */
public class CalculoPrediosAfectadosPorDecretoDTO implements Serializable {

    private static final long serialVersionUID = -7041637788715732518L;

    /**
     * Datos
     */
    private Long condicionId;
    private String condicion;
    private Double incremento;
    private Long numeroPrediosAfectados;

    /**
     * Constructor
     */
    public CalculoPrediosAfectadosPorDecretoDTO() {
        condicionId = 0L;
        condicion = "";
        incremento = 0D;
        numeroPrediosAfectados = 0L;
    }

    public Long getCondicionId() {
        return condicionId;
    }

    public void setCondicionId(Long condicionId) {
        this.condicionId = condicionId;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    public Double getIncremento() {
        return incremento;
    }

    public void setIncremento(Double incremento) {
        this.incremento = incremento;
    }

    public Long getNumeroPrediosAfectados() {
        return numeroPrediosAfectados;
    }

    public void setNumeroPrediosAfectados(Long numeroPrediosAfectados) {
        this.numeroPrediosAfectados = numeroPrediosAfectados;
    }
}
