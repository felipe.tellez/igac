package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the H_FM_CONSTRUCCION_COMPONENTE database table.
 *
 */
@Entity
@Table(name = "H_FM_CONSTRUCCION_COMPONENTE")
public class HFmConstruccionComponente implements Serializable {

    private static final long serialVersionUID = 1L;
    private HFmConstruccionComponentePK id;
    private String cancelaInscribe;
    private String componente;
    private String detalleCalificacion;
    private String elementoCalificacion;
    private Date fechaLog;
    private BigDecimal fmModeloConstruccionId;
    private BigDecimal puntos;
    private String usuarioLog;
    private HPredio HPredio;

    public HFmConstruccionComponente() {
    }

    @EmbeddedId
    public HFmConstruccionComponentePK getId() {
        return this.id;
    }

    public void setId(HFmConstruccionComponentePK id) {
        this.id = id;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    public String getComponente() {
        return this.componente;
    }

    public void setComponente(String componente) {
        this.componente = componente;
    }

    @Column(name = "DETALLE_CALIFICACION")
    public String getDetalleCalificacion() {
        return this.detalleCalificacion;
    }

    public void setDetalleCalificacion(String detalleCalificacion) {
        this.detalleCalificacion = detalleCalificacion;
    }

    @Column(name = "ELEMENTO_CALIFICACION")
    public String getElementoCalificacion() {
        return this.elementoCalificacion;
    }

    public void setElementoCalificacion(String elementoCalificacion) {
        this.elementoCalificacion = elementoCalificacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "FM_MODELO_CONSTRUCCION_ID")
    public BigDecimal getFmModeloConstruccionId() {
        return this.fmModeloConstruccionId;
    }

    public void setFmModeloConstruccionId(BigDecimal fmModeloConstruccionId) {
        this.fmModeloConstruccionId = fmModeloConstruccionId;
    }

    public BigDecimal getPuntos() {
        return this.puntos;
    }

    public void setPuntos(BigDecimal puntos) {
        this.puntos = puntos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to HPredio
    @ManyToOne
    @JoinColumn(name = "H_PREDIO_ID", insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

}
