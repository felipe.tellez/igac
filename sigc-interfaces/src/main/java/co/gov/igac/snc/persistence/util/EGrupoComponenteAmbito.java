package co.gov.igac.snc.persistence.util;

public enum EGrupoComponenteAmbito {
    TODOS,
    TERRITORIAL,
    UOC;
}
