package co.gov.igac.snc.vo;

import java.io.Serializable;

import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;

/**
 * Clase que reduce la informacion del modelo de la ficha matriz para ser mostrado en las tareas
 * geograficas
 *
 * @author franz.gamba
 */
public class FichaMatrizModeloVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3607252710126023924L;
    private Long id;
    private Double areaConstruida;
    private Double areaTerreno;
    private String nombre;
    private Integer numeroUnidades;
    private Long planoDocumentoId;
    private String descripcion;

    /** Empty constructor */
    public FichaMatrizModeloVO() {

    }

    /** Constructor a partir de un FichaMatrizModelo */
    public FichaMatrizModeloVO(FichaMatrizModelo modelo) {
        this.id = modelo.getId();
        this.areaConstruida = modelo.getAreaConstruida();
        this.areaTerreno = modelo.getAreaTerreno();
        this.nombre = modelo.getNombre();
        this.numeroUnidades = modelo.getNumeroUnidades();
        this.planoDocumentoId = modelo.getPlanoDocumentoId();
        this.descripcion = modelo.getDescripcion();
    }

    /** Constructor a partir de un FichaMatrizModelo */
    public FichaMatrizModeloVO(PFichaMatrizModelo modelo) {
        this.id = modelo.getId();
        this.areaConstruida = modelo.getAreaConstruida();
        this.areaTerreno = modelo.getAreaTerreno();
        this.nombre = modelo.getNombre();
        if (modelo.getNumeroUnidades() != null) {
            this.numeroUnidades = modelo.getNumeroUnidades().intValue();
        }
        if (modelo.getPlanoDocumentoId() != null) {
            this.planoDocumentoId = modelo.getPlanoDocumentoId().longValue();
        }
        this.descripcion = modelo.getDescripcion();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAreaConstruida() {
        return areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    public Double getAreaTerreno() {
        return areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getNumeroUnidades() {
        return numeroUnidades;
    }

    public void setNumeroUnidades(Integer numeroUnidades) {
        this.numeroUnidades = numeroUnidades;
    }

    public Long getPlanoDocumentoId() {
        return planoDocumentoId;
    }

    public void setPlanoDocumentoId(Long planoDocumentoId) {
        this.planoDocumentoId = planoDocumentoId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
