package co.gov.igac.snc.persistence.entity.tramite;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TramiteRequisito entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRAMITE_REQUISITO", schema = "SNC_TRAMITE")
public class TramiteRequisito implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private String tipoTramite;
    private String claseMutacion;
    private String subtipo;
    private Short duracion;
    private String tipoDias;
    private String observaciones;
    private String clasificacion;
    private Long numeracionDocumentoId;
    private String usuarioLog;
    private Date fechaLog;
    private List<TramiteRequisitoDocumento> tramiteRequisitoDocumentos =
        new ArrayList<TramiteRequisitoDocumento>();
    private Set<TramiteRequisitoEstado> tramiteRequisitoEstados =
        new HashSet<TramiteRequisitoEstado>(
            0);

    // Constructors
    /** default constructor */
    public TramiteRequisito() {
    }

    /** minimal constructor */
    public TramiteRequisito(Long id, String tipoTramite, Short duracion,
        String tipoDias, Long numeracionDocumentoId, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.tipoTramite = tipoTramite;
        this.duracion = duracion;
        this.tipoDias = tipoDias;
        this.numeracionDocumentoId = numeracionDocumentoId;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramiteRequisito(Long id, String tipoTramite, String claseMutacion,
        String subtipo, Short duracion, String tipoDias,
        String observaciones, String clasificacion,
        Long numeracionDocumentoId, String usuarioLog, Date fechaLog,
        List<TramiteRequisitoDocumento> tramiteRequisitoDocumentos,
        Set<TramiteRequisitoEstado> tramiteRequisitoEstados) {
        this.id = id;
        this.tipoTramite = tipoTramite;
        this.claseMutacion = claseMutacion;
        this.subtipo = subtipo;
        this.duracion = duracion;
        this.tipoDias = tipoDias;
        this.observaciones = observaciones;
        this.clasificacion = clasificacion;
        this.numeracionDocumentoId = numeracionDocumentoId;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.tramiteRequisitoDocumentos = tramiteRequisitoDocumentos;
        this.tramiteRequisitoEstados = tramiteRequisitoEstados;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_TRAMITE", nullable = false, length = 30)
    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @Column(name = "CLASE_MUTACION", length = 30)
    public String getClaseMutacion() {
        return this.claseMutacion;
    }

    public void setClaseMutacion(String claseMutacion) {
        this.claseMutacion = claseMutacion;
    }

    @Column(name = "SUBTIPO", length = 30)
    public String getSubtipo() {
        return this.subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    @Column(name = "DURACION", nullable = false, precision = 4, scale = 0)
    public Short getDuracion() {
        return this.duracion;
    }

    public void setDuracion(Short duracion) {
        this.duracion = duracion;
    }

    @Column(name = "TIPO_DIAS", nullable = false, length = 30)
    public String getTipoDias() {
        return this.tipoDias;
    }

    public void setTipoDias(String tipoDias) {
        this.tipoDias = tipoDias;
    }

    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "CLASIFICACION", length = 30)
    public String getClasificacion() {
        return this.clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    @Column(name = "NUMERACION_ID", nullable = false, precision = 10, scale = 0)
    public Long getNumeracionDocumentoId() {
        return this.numeracionDocumentoId;
    }

    public void setNumeracionDocumentoId(Long numeracionDocumentoId) {
        this.numeracionDocumentoId = numeracionDocumentoId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramiteRequisito")
    public List<TramiteRequisitoDocumento> getTramiteRequisitoDocumentos() {
        return this.tramiteRequisitoDocumentos;
    }

    public void setTramiteRequisitoDocumentos(
        List<TramiteRequisitoDocumento> tramiteRequisitoDocumentos) {
        this.tramiteRequisitoDocumentos = tramiteRequisitoDocumentos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramiteRequisito")
    public Set<TramiteRequisitoEstado> getTramiteRequisitoEstados() {
        return this.tramiteRequisitoEstados;
    }

    public void setTramiteRequisitoEstados(
        Set<TramiteRequisitoEstado> tramiteRequisitoEstados) {
        this.tramiteRequisitoEstados = tramiteRequisitoEstados;
    }

}
