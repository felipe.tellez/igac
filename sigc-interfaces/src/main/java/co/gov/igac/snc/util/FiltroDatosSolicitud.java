package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;

/**
 *
 * @author jamir.avila
 *
 */
public class FiltroDatosSolicitud implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6308729705137363770L;
    // Se refiere al número de radicación de la solicitud
    private String numero;
    private Date fechaInicial;
    private Date fechaFinal;
    private Long solicitanteId;
    // Se refiere al tipo de solicitud
    private String tipo;
    private String paisCodigo;
    private String departamentoCodigo;
    private String municipioCodigo;
    private String radicacionInicial;
    private String radicacionDefinitiva;
    private String territorialCodigo;
    private String tipoTramite;
    // Se refiere al solicitante
    private String tipoPersona;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String razonSocial;
    private String sigla;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;

    public FiltroDatosSolicitud() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Long getSolicitanteId() {
        return solicitanteId;
    }

    public void setSolicitanteId(Long solicitanteId) {
        this.solicitanteId = solicitanteId;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public String getRadicacionInicial() {
        return radicacionInicial;
    }

    public void setRadicacionInicial(String radicacionInicial) {
        this.radicacionInicial = radicacionInicial;
    }

    public String getRadicacionDefinitiva() {
        return radicacionDefinitiva;
    }

    public void setRadicacionDefinitiva(String radicacionDefinitiva) {
        this.radicacionDefinitiva = radicacionDefinitiva;
    }

    public String getTerritorialCodigo() {
        return territorialCodigo;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.territorialCodigo = territorialCodigo;
    }

    public String getPaisCodigo() {
        return paisCodigo;
    }

    public void setPaisCodigo(String paisCodigo) {
        this.paisCodigo = paisCodigo;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Override
    public String toString() {
        return "FiltroDatosSolicitud [numero=" + numero +
            ", fechaInicial=" + fechaInicial +
            ", fechaFinal=" + fechaFinal +
            ", solicitanteId=" + solicitanteId +
            ", tipoPersona=" + tipoPersona +
            ", paisCodigo=" + paisCodigo +
            ", departamentoCodigo=" + departamentoCodigo +
            ", municipioCodigo=" + municipioCodigo +
            ", radicacionInicial=" + radicacionInicial +
            ", radicacionDefinitiva=" + radicacionDefinitiva +
            ", tipoTramite=" + tipoTramite +
            ", territorialCodigo=" + territorialCodigo +
            "]";
    }

    /**
     * Método encargado de determinar si el tipo de identificación es NIT
     *
     * @author javier.aponte
     */
    public boolean isEsNIT() {

        if (this.tipoIdentificacion != null) {
            if (EPersonaTipoIdentificacion.NIT.getCodigo().equals(this.tipoIdentificacion)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método encargado de determinar si el tipo de persona es persona natural
     *
     * @author javier.aponte
     */
    public boolean isPersonaNatural() {

        if (this.tipoPersona != null) {
            if (EPersonaTipoPersona.NATURAL.getCodigo().equals(this.tipoPersona)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método encargado de determinar si el tipo de persona es persona juridica
     *
     * @author javier.aponte
     */
    public boolean isPersonaJuridica() {

        if (this.tipoPersona != null) {
            if (EPersonaTipoPersona.JURIDICA.getCodigo().equals(this.tipoPersona)) {
                return true;
            }
        }
        return false;
    }

}
