/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * enumeración con los códigos y valores del dominio COMISION_TRAMITE_RESULT_AV
 *
 * @author javier.aponte
 */
public enum EComisionTramiteResultAv {

    //N:
    //  Note that each enum type has a static values method that returns an array containing all
    //  of the values of the enum type in the order they are declared.
    ACEPTA("ACEPTA", "Acepta"),
    CONFIRMA("CONFIRMA", "Confirma"),
    MODIFICA("MODIFICA", "Modifica"),
    RECHAZA("RECHAZA", "Rechaza");

    private String codigo;
    private String valor;

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
     * constructor
     *
     * @param codigo
     * @param valor
     */
    private EComisionTramiteResultAv(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

}
