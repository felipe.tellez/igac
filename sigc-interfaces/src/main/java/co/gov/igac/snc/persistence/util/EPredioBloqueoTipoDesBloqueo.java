/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los tipos de desbloqueo en Predio Bloqueo Corresponde al dominio
 * PREDIO_TIPO_DESBLOQUEO
 *
 * @author dumar.penuela
 */
public enum EPredioBloqueoTipoDesBloqueo {

    FALLADO("FALLADO", "Fallado");

    private String codigo;
    private String valor;

    private EPredioBloqueoTipoDesBloqueo(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
