/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * MODIFICACIONES PROPIAS A LA CLASE PManzanaVereda: -> Se agregó el serialVersionUID. -> Se agregó
 * el Named Query: findMunicipioByCodigo -> Se agregó el método Transient getCodigo3Digitos que
 * sirve para obtener el código de municipio sin los dígitos de departamento.
 */
/**
 * ManzanaVereda entity.
 *
 * @author felipe.cadena
 */
@Cacheable
@Entity
@Table(name = "P_MANZANA_VEREDA", schema = "SNC_CONSERVACION")
public class PManzanaVereda implements java.io.Serializable, Comparable<PManzanaVereda> {

    /**
     *
     */
    private static final long serialVersionUID = 8292666946607753326L;
    /**
     *
     */
    // Fields

    private String codigo;
    private Barrio barrio;
    private String nombre;
    private String usuarioLog;
    private Date fechaLog;
    private Tramite tramite;
    private String cancelaInscribe;

    //Transient
    private boolean selected;

    /**
     * Método creado para retornar el código requerido en la interfaz gráfica que debe tener sólo
     * los tres últimos dígitos (excluyendo el código de departamento)
     *
     * @return
     */
    @Transient
    public String getCodigo3Digitos() {
        return codigo.substring(codigo.length() - 4, codigo.length());
    }

    // Constructors
    /**
     * default constructor
     */
    public PManzanaVereda() {
    }

    /**
     * minimal constructor
     */
    public PManzanaVereda(String codigo, Barrio barrio, String nombre,
        String usuarioLog, Date fechaLog) {
        this.codigo = codigo;
        this.barrio = barrio;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "CODIGO", nullable = false, length = 5)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BARRIO_CODIGO", nullable = false)
    public Barrio getBarrio() {
        return this.barrio;
    }

    public void setBarrio(Barrio barrio) {
        this.barrio = barrio;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Transient
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Transient
    public String getDeptoCodigo() {
        if (this.codigo != null && !this.codigo.isEmpty()) {
            return this.codigo.substring(0, 2);
        } else {
            return "";
        }
    }

    @Transient
    public String getMunicipioCodigo() {
        if (this.codigo != null && !this.codigo.isEmpty()) {
            return this.codigo.substring(2, 5);
        } else {
            return "";
        }
    }

    @Transient
    public String getZonaCodigo() {
        if (this.codigo != null && !this.codigo.isEmpty()) {
            return this.codigo.substring(5, 7);
        } else {
            return "";
        }
    }

    @Transient
    public String getComunaCodigo() {
        if (this.codigo != null && !this.codigo.isEmpty()) {
            return this.codigo.substring(7, 9);
        } else {
            return "";
        }
    }

    @Transient
    public String getBarrioCodigo() {
        if (this.codigo != null && !this.codigo.isEmpty()) {
            return this.codigo.substring(9, 13);
        } else {
            return "";
        }
    }

    /**
     * Retorna el codigo de 4 digitos de la manzana
     *
     * @return
     */
    @Transient
    public String getManzanaCodigo() {
        if (this.codigo != null && !this.codigo.isEmpty()) {
            return this.codigo.substring(13, 17);
        } else {
            return "";
        }
    }

    @Override
    public int compareTo(PManzanaVereda p) {
        return this.codigo.compareTo(p.codigo);
    }

    public static Comparator<PManzanaVereda> getComparatorNombre() {
        return new Comparator<PManzanaVereda>() {
            @Override
            public int compare(PManzanaVereda p1, PManzanaVereda p2) {
                return p1.nombre.compareTo(p2.nombre);
            }
        };
    }

}
