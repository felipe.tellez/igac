package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * Clase que contiene la informacion a visualizar en el historico de avaluos de un predio
 *
 * @author leidy.gonzalez::#12528::26/05/2015 Se modifica por CU_187
 *
 */
public class HistoricoAvaluosVO implements Serializable {

    private static final long serialVersionUID = 2249817785924600145L;

    private String autoavaluo;
    private Date decretoFechaOResolucionFecha;
    private String decretoNumeroOResolucion;
    private Date vigenciaDecretoOResolucion;
    private Double valorTotalAvaluoCatastralDecretoOResolucion;
    private boolean resolucion;

    /**
     * Empty constructor
     */
    public HistoricoAvaluosVO() {

    }

    /**
     * Full constructor
     */
    public HistoricoAvaluosVO(String autoavaluo, Date decretoFechaOResolucionFecha,
        String decretoNumeroOResolucion, Date vigenciaDecretoOResolucion,
        Double valorTotalAvaluoCatastralDecretoOResolucion) {
        this.autoavaluo = autoavaluo;
        this.decretoFechaOResolucionFecha = decretoFechaOResolucionFecha;
        this.decretoNumeroOResolucion = decretoNumeroOResolucion;
        this.vigenciaDecretoOResolucion = vigenciaDecretoOResolucion;
        this.valorTotalAvaluoCatastralDecretoOResolucion =
            valorTotalAvaluoCatastralDecretoOResolucion;
    }

    public String getAutoavaluo() {
        return autoavaluo;
    }

    public void setAutoavaluo(String autoavaluo) {
        this.autoavaluo = autoavaluo;
    }

    public Date getDecretoFechaOResolucionFecha() {
        return decretoFechaOResolucionFecha;
    }

    public void setDecretoFechaOResolucionFecha(Date decretoFechaOResolucionFecha) {
        this.decretoFechaOResolucionFecha = decretoFechaOResolucionFecha;
    }

    public String getDecretoNumeroOResolucion() {
        return decretoNumeroOResolucion;
    }

    public void setDecretoNumeroOResolucion(String decretoNumeroOResolucion) {
        this.decretoNumeroOResolucion = decretoNumeroOResolucion;
    }

    public Date getVigenciaDecretoOResolucion() {
        return vigenciaDecretoOResolucion;
    }

    public void setVigenciaDecretoOResolucion(Date vigenciaDecretoOResolucion) {
        this.vigenciaDecretoOResolucion = vigenciaDecretoOResolucion;
    }

    public Double getValorTotalAvaluoCatastralDecretoOResolucion() {
        return valorTotalAvaluoCatastralDecretoOResolucion;
    }

    public void setValorTotalAvaluoCatastralDecretoOResolucion(
        Double valorTotalAvaluoCatastralDecretoOResolucion) {
        this.valorTotalAvaluoCatastralDecretoOResolucion =
            valorTotalAvaluoCatastralDecretoOResolucion;
    }

    @Transient
    public boolean isExisteAutoestimacion() {
        boolean esAutoestimacion = false;

        if (this.autoavaluo != null && this.autoavaluo.equals(ESiNo.SI.getCodigo())) {
            esAutoestimacion = true;
        } else {
            esAutoestimacion = false;
        }

        return esAutoestimacion;
    }

    public boolean isResolucion() {
        return resolucion;
    }

    public void setResolucion(boolean resolucion) {
        this.resolucion = resolucion;
    }

}
