package co.gov.igac.snc.persistence.entity.actualizacion;

/*
 * Proyecto SNC 2019
 */

import javax.persistence.*;
import java.util.Date;


/**
 * Persistencia asociada a las clase VCargueCica
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "V_CARGUE_CICA")
public class VCargueCica implements java.io.Serializable, Cloneable {

	private static final long serialVersionUID = 4262970936756309656L;

	// Campos bd

	private String departamento;
	private String municipio;
	private String departamentoCodigo;
	private String municipioCodigo;
	private Long vigencia;

	private String numeroPredialSnc;

	private String numeroPredialCica;

	private String estado;

	private Double areaTerrenoInicial;

	private Double areaTerrenoFinal;

	private Double areaTerrenoVariacion;

	private Double areaConstruccionInicial;

	private Double areaConstruccionFinal;

	private Double areaConstruccionVariacion;

	private Double avaluoInicial;

	private Double avaluoFinal;

	private Double avaluoVariacion;

	private Date fechaCargue;

    private Long tramiteId;
    private Long predioId;
    private Long solicitudId;


// Campos transient(Deben tener autor y descripcion)

	/** default constructor */
	public VCargueCica() {
	}
	// Metodos
	@Id
	@Column(name = "NUMERO_PREDIAL_SNC")
	public String getNumeroPredialSnc(){
		return this.numeroPredialSnc;
	}

	public void setNumeroPredialSnc(String numeroPredialSnc){
		this.numeroPredialSnc = numeroPredialSnc;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	@Column(name = "DEPARTAMENTO_CODIGO")
	public String getDepartamentoCodigo() {
		return departamentoCodigo;
	}

	public void setDepartamentoCodigo(String departamentoCodigo) {
		this.departamentoCodigo = departamentoCodigo;
	}

	@Column(name = "MUNICIPIO_CODIGO")
	public String getMunicipioCodigo() {
		return municipioCodigo;
	}

	public void setMunicipioCodigo(String municipioCodigo) {
		this.municipioCodigo = municipioCodigo;
	}

	@Column(name = "VIGENCIA")
	public Long getVigencia() {
		return vigencia;
	}

	public void setVigencia(Long vigencia) {
		this.vigencia = vigencia;
	}

	@Column(name = "NUMERO_PREDIAL_CICA")
	public String getNumeroPredialCica(){
		return this.numeroPredialCica;
	}

	public void setNumeroPredialCica(String numeroPredialCica){
		this.numeroPredialCica = numeroPredialCica;
	}

	@Column(name = "ESTADO")
	public String getEstado(){
		return this.estado;
	}

	public void setEstado(String estado){
		this.estado = estado;
	}

	@Column(name = "AREA_TERRENO_INICIAL")
	public Double getAreaTerrenoInicial(){
		return this.areaTerrenoInicial;
	}

	public void setAreaTerrenoInicial(Double areaTerrenoInicial){
		this.areaTerrenoInicial = areaTerrenoInicial;
	}

	@Column(name = "AREA_TERRENO_FINAL")
	public Double getAreaTerrenoFinal(){
		return this.areaTerrenoFinal;
	}

	public void setAreaTerrenoFinal(Double areaTerrenoFinal){
		this.areaTerrenoFinal = areaTerrenoFinal;
	}

	@Column(name = "AREA_TERRENO_VARIACION")
	public Double getAreaTerrenoVariacion(){
		return this.areaTerrenoVariacion;
	}

	public void setAreaTerrenoVariacion(Double areaTerrenoVariacion){
		this.areaTerrenoVariacion = areaTerrenoVariacion;
	}

	@Column(name = "AREA_CONSTRUCCION_INICIAL")
	public Double getAreaConstruccionInicial(){
		return this.areaConstruccionInicial;
	}

	public void setAreaConstruccionInicial(Double areaConstruccionInicial){
		this.areaConstruccionInicial = areaConstruccionInicial;
	}

	@Column(name = "AREA_CONSTRUCCION_FINAL")
	public Double getAreaConstruccionFinal(){
		return this.areaConstruccionFinal;
	}

	public void setAreaConstruccionFinal(Double areaConstruccionFinal){
		this.areaConstruccionFinal = areaConstruccionFinal;
	}

	@Column(name = "AREA_CONSTRUCCION_VARIACION")
	public Double getAreaConstruccionVariacion(){
		return this.areaConstruccionVariacion;
	}

	public void setAreaConstruccionVariacion(Double areaConstruccionVariacion){
		this.areaConstruccionVariacion = areaConstruccionVariacion;
	}

	@Column(name = "AVALUO_INICIAL")
	public Double getAvaluoInicial(){
		return this.avaluoInicial;
	}

	public void setAvaluoInicial(Double avaluoInicial){
		this.avaluoInicial = avaluoInicial;
	}

	@Column(name = "AVALUO_FINAL")
	public Double getAvaluoFinal(){
		return this.avaluoFinal;
	}

	public void setAvaluoFinal(Double avaluoFinal){
		this.avaluoFinal = avaluoFinal;
	}

	@Column(name = "AVALUO_VARIACION")
	public Double getAvaluoVariacion(){
		return this.avaluoVariacion;
	}

	public void setAvaluoVariacion(Double avaluoVariacion){
		this.avaluoVariacion = avaluoVariacion;
	}

	@Column(name = "FECHA_CARGUE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaCargue(){
        return this.fechaCargue;
    }

    public void setFechaCargue(Date fechaCargue){
        this.fechaCargue = fechaCargue;
    }

    @Column(name = "TRAMITE_ID")
    public Long getTramiteId() {
        return tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

	@Column(name = "PPREDIO_ID")
	public Long getPredioId() {
		return predioId;
	}

	public void setPredioId(Long predioId) {
		this.predioId = predioId;
	}

	@Column(name = "SOLICITUD_ID")
	public Long getSolicitudId() {
		return solicitudId;
	}

	public void setSolicitudId(Long solicitudId) {
		this.solicitudId = solicitudId;
	}

	// Metodos transient(Deben tener autor y descripcion)

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final VCargueCica other = (VCargueCica) obj;
		if (this.numeroPredialSnc != other.numeroPredialSnc && (this.numeroPredialSnc == null || !this.numeroPredialSnc.equals(other.numeroPredialSnc))) {
			return false;
		}
		return true;
	}
}