/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Filtro para los campos de la consulta de predios
 *
 * @author pedro.garcia
 * @modify david.cifuentes -> adición del método validateSegmentsNumeroPredial
 * @modify javier.barajas -> adicion de rango numero predial, zonas homogeneas, moda
 * adiquisicion,tipo calificacion, destino, tipo,, tipificacion
 */
public class FiltroGenerarReportesconCriterios implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String territorialId;
    private String departamentoId;
    private String municipioId;

    /**
     * segmentos del número predial Inicial
     */
    private String numeroPredialS1;
    private String numeroPredialS2;
    private String numeroPredialS3;
    private String numeroPredialS4;
    private String numeroPredialS5;
    private String numeroPredialS6;
    private String numeroPredialS7;
    private String numeroPredialS8;
    private String numeroPredialS9;
    private String numeroPredialS10;
    private String numeroPredialS11;
    private String numeroPredialS12;
    /**
     * segmentos del número predial Final
     */
    private String numeroPredialS1f;
    private String numeroPredialS2f;
    private String numeroPredialS3f;
    private String numeroPredialS4f;
    private String numeroPredialS5f;
    private String numeroPredialS6f;
    private String numeroPredialS7f;
    private String numeroPredialS8f;
    private String numeroPredialS9f;
    private String numeroPredialS10f;
    private String numeroPredialS11f;
    private String numeroPredialS12f;

    private String direccionPredio;
    private String numeroPredialAnterior;
    private String numeroPredial;
    private String numeroPredialFinal;

    private String nombrePropietario;
    private String identificacionPropietario;
    private String digitoVerificacion;

    /**
     * datos de la búsqueda avanzada
     */
    private String numeroRegistro;
    private String circuloRegistral;

    private BigDecimal consecutivoCatastral;

    private String usoConstruccionId;

    private String nip;

    private String destinoPredioId;
    private List<String> tipoPredioSelect;
    private List<String> destinoPredioSelect;
    private List<String> modoAdquisicionSelect;

    private List<String> usoConstruccionSelect;
    private List<String> tipoCalificacionSelect;
    private List<String> tipificacionConstruccionSelect;

    private double areaTerrenoMin;
    private double areaTerrenoMax;
    private double areaConstruidaMin;
    private double areaConstruidaMax;

    /**
     * adicionados por cambios en el procedimiento de búsqueda
     */
    private String primerNombrePropietario;
    private String segundoNombrePropietario;
    private String primerApellidoPropietario;
    private String segundoApellidoPropietario;
    private String razonSocialPropietario;
    private String tipoPersonaPropietario;
    private String tipoIdentificacionPropietario;

    private double avaluoDesde;
    private double avaluoHasta;

    //--------    methods     ------------------------------------------------------------
    public String getNumeroPredialFinal() {
        return numeroPredialFinal;
    }

    public void setNumeroPredialFinal(String numeroPredialFinal) {
        this.numeroPredialFinal = numeroPredialFinal;
    }

    public String getNumeroPredialS1f() {
        return numeroPredialS1f;
    }

    public void setNumeroPredialS1f(String numeroPredialS1f) {
        this.numeroPredialS1f = numeroPredialS1f;
    }

    public String getNumeroPredialS2f() {
        return numeroPredialS2f;
    }

    public void setNumeroPredialS2f(String numeroPredialS2f) {
        this.numeroPredialS2f = numeroPredialS2f;
    }

    public String getNumeroPredialS3f() {
        return numeroPredialS3f;
    }

    public void setNumeroPredialS3f(String numeroPredialS3f) {
        this.numeroPredialS3f = numeroPredialS3f;
    }

    public String getNumeroPredialS4f() {
        return numeroPredialS4f;
    }

    public void setNumeroPredialS4f(String numeroPredialS4f) {
        this.numeroPredialS4f = numeroPredialS4f;
    }

    public String getNumeroPredialS5f() {
        return numeroPredialS5f;
    }

    public void setNumeroPredialS5f(String numeroPredialS5f) {
        this.numeroPredialS5f = numeroPredialS5f;
    }

    public String getNumeroPredialS6f() {
        return numeroPredialS6f;
    }

    public void setNumeroPredialS6f(String numeroPredialS6f) {
        this.numeroPredialS6f = numeroPredialS6f;
    }

    public String getNumeroPredialS7f() {
        return numeroPredialS7f;
    }

    public void setNumeroPredialS7f(String numeroPredialS7f) {
        this.numeroPredialS7f = numeroPredialS7f;
    }

    public String getNumeroPredialS8f() {
        return numeroPredialS8f;
    }

    public void setNumeroPredialS8f(String numeroPredialS8f) {
        this.numeroPredialS8f = numeroPredialS8f;
    }

    public String getNumeroPredialS9f() {
        return numeroPredialS9f;
    }

    public void setNumeroPredialS9f(String numeroPredialS9f) {
        this.numeroPredialS9f = numeroPredialS9f;
    }

    public String getNumeroPredialS10f() {
        return numeroPredialS10f;
    }

    public void setNumeroPredialS10f(String numeroPredialS10f) {
        this.numeroPredialS10f = numeroPredialS10f;
    }

    public String getNumeroPredialS11f() {
        return numeroPredialS11f;
    }

    public void setNumeroPredialS11f(String numeroPredialS11f) {
        this.numeroPredialS11f = numeroPredialS11f;
    }

    public String getNumeroPredialS12f() {
        return numeroPredialS12f;
    }

    public void setNumeroPredialS12f(String numeroPredialS12f) {
        this.numeroPredialS12f = numeroPredialS12f;
    }

    public List<String> getModoAdquisicionSelect() {
        return modoAdquisicionSelect;
    }

    public void setModoAdquisicionSelect(List<String> modoAdquisicionSelect) {
        this.modoAdquisicionSelect = modoAdquisicionSelect;
    }

    public List<String> getUsoConstruccionSelect() {
        return usoConstruccionSelect;
    }

    public void setUsoConstruccionSelect(List<String> usoConstruccionSelect) {
        this.usoConstruccionSelect = usoConstruccionSelect;
    }

    public List<String> getTipoCalificacionSelect() {
        return tipoCalificacionSelect;
    }

    public void setTipoCalificacionSelect(List<String> tipoCalificacionSelect) {
        this.tipoCalificacionSelect = tipoCalificacionSelect;
    }

    public List<String> getTipificacionConstruccionSelect() {
        return tipificacionConstruccionSelect;
    }

    public void setTipificacionConstruccionSelect(
        List<String> tipificacionConstruccionSelect) {
        this.tipificacionConstruccionSelect = tipificacionConstruccionSelect;
    }

    public List<String> getDestinoPredioSelect() {
        return destinoPredioSelect;
    }

    public void setDestinoPredioSelect(List<String> destinoPredioSelect) {
        this.destinoPredioSelect = destinoPredioSelect;
    }

    public String getDigitoVerificacion() {
        return digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    public List<String> getTipoPredioSelect() {
        return tipoPredioSelect;
    }

    public void setTipoPredioSelect(List<String> tipoPredioSelect) {
        this.tipoPredioSelect = tipoPredioSelect;
    }

    public double getAvaluoDesde() {
        return this.avaluoDesde;
    }

    public void setAvaluoDesde(double avaluoDesde) {
        this.avaluoDesde = avaluoDesde;
    }

    public double getAvaluoHasta() {
        return this.avaluoHasta;
    }

    public void setAvaluoHasta(double avaluoHasta) {
        this.avaluoHasta = avaluoHasta;
    }

    public String getTipoPersonaPropietario() {
        return this.tipoPersonaPropietario;
    }

    public String getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(String circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    public void setTipoPersonaPropietario(String tipoPersonaPropietario) {
        this.tipoPersonaPropietario = tipoPersonaPropietario;
    }

    public String getPrimerNombrePropietario() {
        return this.primerNombrePropietario;
    }

    public void setPrimerNombrePropietario(String primerNombrePropietario) {
        this.primerNombrePropietario = primerNombrePropietario;
    }

    public String getSegundoNombrePropietario() {
        return this.segundoNombrePropietario;
    }

    public void setSegundoNombrePropietario(String segundoNombrePropietario) {
        this.segundoNombrePropietario = segundoNombrePropietario;
    }

    public String getPrimerApellidoPropietario() {
        return this.primerApellidoPropietario;
    }

    public void setPrimerApellidoPropietario(String primerApellidoPropietario) {
        this.primerApellidoPropietario = primerApellidoPropietario;
    }

    public String getSegundoApellidoPropietario() {
        return this.segundoApellidoPropietario;
    }

    public void setSegundoApellidoPropietario(String segundoApellidoPropietario) {
        this.segundoApellidoPropietario = segundoApellidoPropietario;
    }

    public String getRazonSocialPropietario() {
        return this.razonSocialPropietario;
    }

    public void setRazonSocialPropietario(String razonSocialPropietario) {
        this.razonSocialPropietario = razonSocialPropietario;
    }

    public String getTerritorialId() {
        return this.territorialId;
    }

    public void setTerritorialId(String territorialId) {
        this.territorialId = territorialId;
    }

    public String getDepartamentoId() {
        return this.departamentoId;
    }

    public void setDepartamentoId(String departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getMunicipioId() {
        return this.municipioId;
    }

    public void setMunicipioId(String municipioId) {
        this.municipioId = municipioId;
    }

    public String getNumeroPredialS1() {
        return this.numeroPredialS1;
    }

    public void setNumeroPredialS1(String numeroPredialS1) {
        this.numeroPredialS1 = numeroPredialS1;
    }

    public String getNumeroPredialS2() {
        return this.numeroPredialS2;
    }

    public void setNumeroPredialS2(String numeroPredialS2) {
        this.numeroPredialS2 = numeroPredialS2;
    }

    public String getNumeroPredialS3() {
        return this.numeroPredialS3;
    }

    public void setNumeroPredialS3(String numeroPredialS3) {
        this.numeroPredialS3 = numeroPredialS3;
    }

    public String getNumeroPredialS4() {
        return this.numeroPredialS4;
    }

    public void setNumeroPredialS4(String numeroPredialS4) {
        this.numeroPredialS4 = numeroPredialS4;
    }

    public String getNumeroPredialS5() {
        return this.numeroPredialS5;
    }

    public void setNumeroPredialS5(String numeroPredialS5) {
        this.numeroPredialS5 = numeroPredialS5;
    }

    public String getNumeroPredialS6() {
        return this.numeroPredialS6;
    }

    public void setNumeroPredialS6(String numeroPredialS6) {
        this.numeroPredialS6 = numeroPredialS6;
    }

    public String getNumeroPredialS7() {
        return this.numeroPredialS7;
    }

    public void setNumeroPredialS7(String numeroPredialS7) {
        this.numeroPredialS7 = numeroPredialS7;
    }

    public String getNumeroPredialS8() {
        return this.numeroPredialS8;
    }

    public void setNumeroPredialS8(String numeroPredialS8) {
        this.numeroPredialS8 = numeroPredialS8;
    }

    public String getNumeroPredialS9() {
        return this.numeroPredialS9;
    }

    public void setNumeroPredialS9(String numeroPredialS9) {
        this.numeroPredialS9 = numeroPredialS9;
    }

    public String getNumeroPredialS10() {
        return this.numeroPredialS10;
    }

    public void setNumeroPredialS10(String numeroPredialS10) {
        this.numeroPredialS10 = numeroPredialS10;
    }

    public String getNumeroPredialS11() {
        return this.numeroPredialS11;
    }

    public void setNumeroPredialS11(String numeroPredialS11) {
        this.numeroPredialS11 = numeroPredialS11;
    }

    public String getNumeroPredialS12() {
        return numeroPredialS12;
    }

    public void setNumeroPredialS12(String numeroPredialS12) {
        this.numeroPredialS12 = numeroPredialS12;
    }

    public String getDireccionPredio() {
        return this.direccionPredio;
    }

    public void setDireccionPredio(String direccionPredio) {
        this.direccionPredio = direccionPredio;
    }

    public String getNumeroPredialAnterior() {
        return this.numeroPredialAnterior;
    }

    public void setNumeroPredialAnterior(String numeroPredialAnterior) {
        this.numeroPredialAnterior = numeroPredialAnterior;
    }

    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getNombrePropietario() {
        return this.nombrePropietario;
    }

    public void setNombrePropietario(String nombrePropietario) {
        this.nombrePropietario = nombrePropietario;
    }

    public String getIdentificacionPropietario() {
        return identificacionPropietario;
    }

    public void setIdentificacionPropietario(String identificacionPropietario) {
        this.identificacionPropietario = identificacionPropietario;
    }

    public String getUsoConstruccionId() {
        return this.usoConstruccionId;
    }

    public void setUsoConstruccionId(String usoConstruccionId) {
        this.usoConstruccionId = usoConstruccionId;
    }

    public String getDestinoPredioId() {
        return this.destinoPredioId;
    }

    public void setDestinoPredioId(String destinoPredioId) {
        this.destinoPredioId = destinoPredioId;
    }

    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    public BigDecimal getConsecutivoCatastral() {
        return this.consecutivoCatastral;
    }

    public void setConsecutivoCatastral(BigDecimal consecutivoCatastral) {
        this.consecutivoCatastral = consecutivoCatastral;
    }

    public String getNip() {
        return this.nip;
    }

    public void setNip(String chip) {
        this.nip = chip;
    }

    public double getAreaTerrenoMin() {
        return this.areaTerrenoMin;
    }

    public void setAreaTerrenoMin(double areaTerrenoMin) {
        this.areaTerrenoMin = areaTerrenoMin;
    }

    public double getAreaTerrenoMax() {
        return this.areaTerrenoMax;
    }

    public void setAreaTerrenoMax(double areaTerrenoMax) {
        this.areaTerrenoMax = areaTerrenoMax;
    }

    public double getAreaConstruidaMin() {
        return this.areaConstruidaMin;
    }

    public void setAreaConstruidaMin(double areaConstruidaMin) {
        this.areaConstruidaMin = areaConstruidaMin;
    }

    public double getAreaConstruidaMax() {
        return this.areaConstruidaMax;
    }

    public void setAreaConstruidaMax(double areaConstruidaMax) {
        this.areaConstruidaMax = areaConstruidaMax;
    }

    public String getTipoIdentificacionPropietario() {
        return tipoIdentificacionPropietario;
    }

    public void setTipoIdentificacionPropietario(
        String tipoIdentificacionPropietario) {
        this.tipoIdentificacionPropietario = tipoIdentificacionPropietario;
    }
//-------------------------------------------------------------------------------------------------

    public FiltroGenerarReportesconCriterios() {
//		this.departamentoId = "";
//		this.municipioId = "";
//		this.territorialId = "";
        this.tipoPredioSelect = new ArrayList<String>();
        this.destinoPredioSelect = new ArrayList<String>();
        this.modoAdquisicionSelect = new ArrayList<String>();
        this.usoConstruccionSelect = new ArrayList<String>();
        this.tipoCalificacionSelect = new ArrayList<String>();
        this.tipificacionConstruccionSelect = new ArrayList<String>();

    }
//--------------------------------------------------------------------------------------------------

    public void assembleNumeroPredialFromSegments() {
        StringBuilder numeroPredialAssembled;
        numeroPredialAssembled = new StringBuilder();
        numeroPredialAssembled.append(this.numeroPredialS1);
        numeroPredialAssembled.append(this.numeroPredialS2);
        numeroPredialAssembled.append(this.numeroPredialS3);
        numeroPredialAssembled.append(this.numeroPredialS4);
        numeroPredialAssembled.append(this.numeroPredialS5);
        numeroPredialAssembled.append(this.numeroPredialS6);
        numeroPredialAssembled.append(this.numeroPredialS7);
        numeroPredialAssembled.append(this.numeroPredialS8);
        numeroPredialAssembled.append(this.numeroPredialS9);
        numeroPredialAssembled.append(this.numeroPredialS10);
        numeroPredialAssembled.append(this.numeroPredialS11);
        numeroPredialAssembled.append(this.numeroPredialS12);
        this.numeroPredial = numeroPredialAssembled.toString();
        StringBuilder numeroPredialFinalAssembled;
        numeroPredialFinalAssembled = new StringBuilder();
        numeroPredialFinalAssembled.append(this.numeroPredialS1f);
        numeroPredialFinalAssembled.append(this.numeroPredialS2f);
        numeroPredialFinalAssembled.append(this.numeroPredialS3f);
        numeroPredialFinalAssembled.append(this.numeroPredialS4f);
        numeroPredialFinalAssembled.append(this.numeroPredialS5f);
        numeroPredialFinalAssembled.append(this.numeroPredialS6f);
        numeroPredialFinalAssembled.append(this.numeroPredialS7f);
        numeroPredialFinalAssembled.append(this.numeroPredialS8f);
        numeroPredialFinalAssembled.append(this.numeroPredialS9f);
        numeroPredialFinalAssembled.append(this.numeroPredialS10f);
        numeroPredialFinalAssembled.append(this.numeroPredialS11f);
        numeroPredialFinalAssembled.append(this.numeroPredialS12f);
        this.numeroPredialFinal = numeroPredialFinalAssembled.toString();
    }

    /**
     * @author javier.barajas Metodo para la consulta segun la lista de tipos de predio
     */
    public void addTipoPredio(String tipoPredio) {
        this.tipoPredioSelect.add(tipoPredio);

    }

    /**
     * @author javier.barajas Metodo para la consulta segun la lista de destinos de predio
     */
    public void addDestinoPredio(String destinoPredio) {
        this.destinoPredioSelect.add(destinoPredio);

    }

    public void addModoAdquisicion(String modoAdquisicion) {
        this.modoAdquisicionSelect.add(modoAdquisicion);

    }

    public void addUsoConstruccion(String usoConstruccion) {
        this.usoConstruccionSelect.add(usoConstruccion);

    }

    public void addTipoCalificacion(String tipoCalificacion) {
        this.tipoCalificacionSelect.add(tipoCalificacion);

    }

    public void addTipificacionConstruccion(String tipificacionConstruccion) {
        this.tipificacionConstruccionSelect.add(tipificacionConstruccion);

    }
    //--------------------------------------------------------------------------------------------------

    /**
     * @author david.cifuentes Método que valida que todos los segmentos del número predial sean
     * válidos Éste método se debe ejecutar antes del assembleNumeroPredialFromSegments para que no
     * concatene null por error.
     */
    public boolean validateSegmentsNumeroPredial() {

        if (this.numeroPredialS1 == null || this.numeroPredialS1.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS2 == null || this.numeroPredialS2.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS3 == null || this.numeroPredialS3.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS4 == null || this.numeroPredialS4.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS5 == null || this.numeroPredialS5.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS6 == null || this.numeroPredialS6.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS7 == null || this.numeroPredialS7.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS8 == null || this.numeroPredialS8.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS9 == null || this.numeroPredialS9.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS10 == null || this.numeroPredialS10.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS11 == null || this.numeroPredialS11.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS12 == null || this.numeroPredialS12.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS1f == null || this.numeroPredialS1f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS2f == null || this.numeroPredialS2f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS3f == null || this.numeroPredialS3f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS4f == null || this.numeroPredialS4f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS5f == null || this.numeroPredialS5f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS6f == null || this.numeroPredialS6f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS7f == null || this.numeroPredialS7f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS8f == null || this.numeroPredialS8f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS9f == null || this.numeroPredialS9f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS10f == null || this.numeroPredialS10f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS11f == null || this.numeroPredialS11f.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS12f == null || this.numeroPredialS12f.trim().isEmpty()) {
            return false;
        }

        return true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     * @return
     */
    public boolean isPersonaNatural() {
        boolean answer = false;
        if (this.tipoPersonaPropietario != null) {
            answer =
                (this.tipoPersonaPropietario.equals(EPersonaTipoPersona.NATURAL.getCodigo())) ?
                true : false;
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     * @return
     */
    public boolean isPersonaJuridica() {
        boolean answer = false;
        if (this.tipoPersonaPropietario != null) {
            answer =
                (this.tipoPersonaPropietario.equals(EPersonaTipoPersona.JURIDICA.getCodigo())) ?
                true : false;
        }

        return answer;
    }

    public boolean isNit() {
        boolean answer = false;
        if (this.tipoIdentificacionPropietario != null) {
            answer =
                (this.tipoIdentificacionPropietario.equals(EPersonaTipoIdentificacion.NIT.
                    getCodigo())) ?
                    true : false;
        }

        return answer;
    }

//end of class
}
