package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para manejar los detalles asociados al avalúo
 *
 *
 * @author felipe.cadena
 */
public enum EAvaluoDetalle {

    ZONAS_ECONOMICAS(1, 6, "Zonas economicas"),
    CULTIVOS(2, 10, "Cultivos"),
    CONSTRUCCIONES(3, 10, "Construcciones"),
    MAQUINARIA(4, 7, "Maquinaria"),
    ANEXOS(5, 9, "Anexos"),
    PREDIOS(6, 10, "Predios");

    private int codigo;
    private int columnas;
    private String nombre;

    private EAvaluoDetalle(int codigo, int columnas, String nombre) {
        this.codigo = codigo;
        this.columnas = columnas;
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getColumnas() {
        return columnas;
    }

    public String getNombre() {
        return nombre;
    }
}
