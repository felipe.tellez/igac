package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.*;

/**
 * PredioColindante entity
 *
 * @author juan.mendez
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "findPredioColindanteByNumeroPredial",
        query = "from PredioColindante pc where pc.numeroPredial = :numeroPredial"),
    @NamedQuery(name = "findPredioColindanteByNumeroPredialYTipo",
        query =
        "from PredioColindante pc where pc.numeroPredial = :numeroPredial and pc.tipo = :tipo")
})
@Table(name = "COLINDANTE", schema = "SNC_CONSERVACION")
public class PredioColindante implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7400131431268398546L;

    @GeneratedValue
    private Long id;
    private String numeroPredial;
    private String numeroPredialColindante;
    private String cardinalidad;
    private String tipo;
    private String usuarioLog;
    private Date fechaLog;

    /** default constructor */
    public PredioColindante() {
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NUMERO_PREDIAL", nullable = false, length = 30)
    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_PREDIAL_COLINDANTE", nullable = false, length = 30)
    public String getNumeroPredialColindante() {
        return numeroPredialColindante;
    }

    public void setNumeroPredialColindante(String numeroPredialColindante) {
        this.numeroPredialColindante = numeroPredialColindante;
    }

    @Column(name = "CARDINALIDAD", nullable = false, length = 30)
    public String getCardinalidad() {
        return cardinalidad;
    }

    public void setCardinalidad(String cardinalidad) {
        this.cardinalidad = cardinalidad;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
