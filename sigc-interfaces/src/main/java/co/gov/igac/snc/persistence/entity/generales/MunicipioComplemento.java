/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 *
 * ComisionTramite entity.
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "MUNICIPIO_COMPLEMENTO", schema = "SNC_GENERALES")
public class MunicipioComplemento implements Serializable {

    // Fields
    private static final long serialVersionUID = -4105850103809634493L;

    private String municipioCodigo;
    private String conexionGdb;
    private String esquemaGdb;
    private Long anioFormacionActualizacionU;
    private Long anioFormacionActualizacionR;
    private Long anioFormacionActualizacionC;
    private String conInformacionGdb;
    private String usuarioLog;
    private Date fechaLog;

    @Id
    @Column(name = "MUNICIPIO_CODIGO", nullable = false)
    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    @Column(name = "CONEXION_GDB")
    public String getConexionGdb() {
        return conexionGdb;
    }

    public void setConexionGdb(String conexionGdb) {
        this.conexionGdb = conexionGdb;
    }

    @Column(name = "ESQUEMA_GDB")
    public String getEsquemaGdb() {
        return esquemaGdb;
    }

    public void setEsquemaGdb(String esquemaGdb) {
        this.esquemaGdb = esquemaGdb;
    }

    @Column(name = "ANIO_FORMACION_ACTUALIZACION_U")
    public Long getAnioFormacionActualizacionU() {
        return anioFormacionActualizacionU;
    }

    public void setAnioFormacionActualizacionU(Long anioFormacionActualizacionU) {
        this.anioFormacionActualizacionU = anioFormacionActualizacionU;
    }

    @Column(name = "ANIO_FORMACION_ACTUALIZACION_R")
    public Long getAnioFormacionActualizacionR() {
        return anioFormacionActualizacionR;
    }

    public void setAnioFormacionActualizacionR(Long anioFormacionActualizacionR) {
        this.anioFormacionActualizacionR = anioFormacionActualizacionR;
    }

    @Column(name = "ANIO_FORMACION_ACTUALIZACION_C")
    public Long getAnioFormacionActualizacionC() {
        return anioFormacionActualizacionC;
    }

    public void setAnioFormacionActualizacionC(Long anioFormacionActualizacionC) {
        this.anioFormacionActualizacionC = anioFormacionActualizacionC;
    }

    @Column(name = "CON_INFORMACION_GDB")
    public String getConInformacionGdb() {
        return conInformacionGdb;
    }

    public void setConInformacionGdb(String conInformacionGdb) {
        this.conInformacionGdb = conInformacionGdb;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
