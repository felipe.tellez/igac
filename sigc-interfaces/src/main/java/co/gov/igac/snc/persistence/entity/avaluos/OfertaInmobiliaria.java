package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.util.EOfertaCondicionJuridica;
import co.gov.igac.snc.persistence.util.EOfertaEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.enumsOfertasInmob.EOfertaTipoOferta;

/**
 * The persistent class for the OFERTA_INMOBILIARIA database table.
 *
 * @modified juan.agudelo 02-11-11 Modificación de la entidad en los objetos BigDecimal pasandolos a
 * Double o Integer de acuerdo a la información en la DB, adicion de la relación con Fotografia y
 * adición de la secuencia OFERTA_INMOBILIARIA_ID_SEQ
 *
 * @modified by: javier.aponte: -> Se agrega getter transient isPropiedadHorizontal.
 *
 * @modified pedro.garcia 15-11-2011 se cambió la relación con municipio y departamento a que se
 * haga por la entidad completa y no solo por el código Adición de método isEsProyecto Adición de
 * método isTieneCasa
 *
 * @modified christian.rodriguez 10-04-2012 se cambia el atributo numero predial por una lista de
 * ofertas predio para permitir asociar varios predios a la oferta
 *
 * @modified rodrigo.hernandez 14-06-2012 se añaden las columnas recolectorId y
 * regionCapturaOfertaId
 *
 *
 */
@Entity
@Table(name = "OFERTA_INMOBILIARIA")
public class OfertaInmobiliaria implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3492830447738326095L;
    private Long id;
    private String altura;
    private String anexos;
    private String archivoProyecto;
    private Double areaConstruccion;
    private Double areaTerreno;
    private Double areaTotalConstruccionCata;
    private Double areaTotalTerrenoCatastral;
    private String areasUsoExclusivo;
    private String cableadoRed;
    private String casa;
    private String clima;
    private String codigoIdentificacion;
    private String codigoOferta;
    private String condicionJuridica;
    private String constructora;
    private String descripcionAnexos;
    private String descripcionConstrucciones;
    private String destino;
    private String direccion;
    private String estado;
    private String estadoVias;
    private String estratoSocioeconomico;
    private Date fechaIngresoOferta;
    private Date fechaLog;
    private Date fechaOferta;
    private Double fondoLote;
    private String formaLote;
    private String formaNegociacion;
    private Double frenteLote;
    private String frenteVias;
    private String fuente;
    private String investigacionIngreso;
    private String local;
    private String mezanine;
    private String nombreBarrio;
    private String nombreFuente;
    private String nombreProyecto;
    private String nombreVereda;
    private Integer numeroApartamentos;
    private Integer numeroBanios;
    private Integer numeroBodegas;
    private Integer numeroCocinas;
    private Integer numeroDepositos;
    private Integer numeroHabitaciones;
    private Integer numeroLocales;
    private Integer numeroOficinas;
    private Integer numeroParqueaderos;
    private Integer numeroPisos;
    private Integer numeroSotanos;
    private String observaciones;
    private String otrosServiciosComunales;
    private Double porcentajeNegociacion;
    private String puenteGrua;
    private String recursosHidricos;
    private String responsableInformacion;
    private String riego;
    private String sector;
    private String servidumbre;
    private String tamanioParqueadero;
    private String telefonoFuente;
    private String tipoBodega;
    private String tipoCercha;
    private String tipoCocina;
    private String tipoEdificio;
    private String tipoFinca;
    private String tipoInmueble;
    private String tipoInvestigacion;
    private String tipoLocal;
    private String tipoLote;
    private String tipoOferta;
    private String tipoParqueadero;
    private String tipoPredio;
    private String topografia;
    private String ubicacion;
    private String ubicacionBodega;
    private Integer ubicacionEnPiso;
    private String ubicacionLocal;
    private String ubicacionManzana;
    private String ubicacionOficina;
    private String unidadArea;
    private String usuarioLog;
    private Double valorAdministracion;
    private Double valorArriendo;
    private Double valorDepositos;
    private Double valorDepurado;
    private Double valorParqueaderos;
    private Double valorPedido;
    private String vetustez;
    private String zonaFranca;
    private List<Fotografia> fotografias;
    private List<OfertaAreaUsoExclusivo> ofertaAreaUsoExclusivos;
    private OfertaInmobiliaria ofertaInmobiliaria;
    private List<OfertaInmobiliaria> ofertaInmobiliarias;
    private List<OfertaRecursoHidrico> ofertaRecursoHidricos;
    private List<OfertaServicioComunal> ofertaServicioComunals;
    private List<OfertaServicioPublico> ofertaServicioPublicos;
    private List<OfertaPredio> ofertaPredios;

    private String recolectorId;
    private Long regionCapturaOfertaId;

    private Municipio municipio;
    private Departamento departamento;

    public OfertaInmobiliaria() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OfertaInmobiliaria_id_seq")
    @SequenceGenerator(name = "OfertaInmobiliaria_id_seq", sequenceName =
        "OFERTA_INMOBILIARIA_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAltura() {
        return this.altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getAnexos() {
        return this.anexos;
    }

    public void setAnexos(String anexos) {
        this.anexos = anexos;
    }

    @Column(name = "ARCHIVO_PROYECTO")
    public String getArchivoProyecto() {
        return this.archivoProyecto;
    }

    public void setArchivoProyecto(String archivoProyecto) {
        this.archivoProyecto = archivoProyecto;
    }

    @Column(name = "AREA_CONSTRUCCION")
    public Double getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "AREA_TERRENO")
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "AREA_TOTAL_CONSTRUCCION_CATA")
    public Double getAreaTotalConstruccionCata() {
        return this.areaTotalConstruccionCata;
    }

    public void setAreaTotalConstruccionCata(Double areaTotalConstruccionCata) {
        this.areaTotalConstruccionCata = areaTotalConstruccionCata;
    }

    @Column(name = "AREA_TOTAL_TERRENO_CATASTRAL")
    public Double getAreaTotalTerrenoCatastral() {
        return this.areaTotalTerrenoCatastral;
    }

    public void setAreaTotalTerrenoCatastral(Double areaTotalTerrenoCatastral) {
        this.areaTotalTerrenoCatastral = areaTotalTerrenoCatastral;
    }

    @Column(name = "AREAS_USO_EXCLUSIVO")
    public String getAreasUsoExclusivo() {
        return this.areasUsoExclusivo;
    }

    public void setAreasUsoExclusivo(String areasUsoExclusivo) {
        this.areasUsoExclusivo = areasUsoExclusivo;
    }

    @Column(name = "CABLEADO_RED")
    public String getCableadoRed() {
        return this.cableadoRed;
    }

    public void setCableadoRed(String cableadoRed) {
        this.cableadoRed = cableadoRed;
    }

    public String getCasa() {
        return this.casa;
    }

    public void setCasa(String casa) {
        this.casa = casa;
    }

    public String getClima() {
        return this.clima;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }

    public void setCodigoIdentificacion(String codigoIdentificacion) {
        this.codigoIdentificacion = codigoIdentificacion;
    }

    @Column(name = "CODIGO_IDENTIFICACION")
    public String getCodigoIdentificacion() {
        return this.codigoIdentificacion;
    }

    @Column(name = "CODIGO_OFERTA")
    public String getCodigoOferta() {
        return this.codigoOferta;
    }

    public void setCodigoOferta(String codigoOferta) {
        this.codigoOferta = codigoOferta;
    }

    @Column(name = "CONDICION_JURIDICA")
    public String getCondicionJuridica() {
        return this.condicionJuridica;
    }

    public void setCondicionJuridica(String condicionJuridica) {
        this.condicionJuridica = condicionJuridica;
    }

    public String getConstructora() {
        return this.constructora;
    }

    public void setConstructora(String constructora) {
        this.constructora = constructora;
    }

    @Column(name = "DESCRIPCION_ANEXOS")
    public String getDescripcionAnexos() {
        return this.descripcionAnexos;
    }

    public void setDescripcionAnexos(String descripcionAnexos) {
        this.descripcionAnexos = descripcionAnexos;
    }

    @Column(name = "DESCRIPCION_CONSTRUCCIONES")
    public String getDescripcionConstrucciones() {
        return this.descripcionConstrucciones;
    }

    public void setDescripcionConstrucciones(String descripcionConstrucciones) {
        this.descripcionConstrucciones = descripcionConstrucciones;
    }

    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Column(name = "ESTADO")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "ESTADO_VIAS")
    public String getEstadoVias() {
        return this.estadoVias;
    }

    public void setEstadoVias(String estadoVias) {
        this.estadoVias = estadoVias;
    }

    @Column(name = "ESTRATO_SOCIOECONOMICO")
    public String getEstratoSocioeconomico() {
        return this.estratoSocioeconomico;
    }

    public void setEstratoSocioeconomico(String estratoSocioeconomico) {
        this.estratoSocioeconomico = estratoSocioeconomico;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INGRESO_OFERTA")
    public Date getFechaIngresoOferta() {
        return this.fechaIngresoOferta;
    }

    public void setFechaIngresoOferta(Date fechaIngresoOferta) {
        this.fechaIngresoOferta = fechaIngresoOferta;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_OFERTA")
    public Date getFechaOferta() {
        return this.fechaOferta;
    }

    public void setFechaOferta(Date fechaOferta) {
        this.fechaOferta = fechaOferta;
    }

    @Column(name = "FONDO_LOTE")
    public Double getFondoLote() {
        return this.fondoLote;
    }

    public void setFondoLote(Double fondoLote) {
        this.fondoLote = fondoLote;
    }

    @Column(name = "FORMA_LOTE")
    public String getFormaLote() {
        return this.formaLote;
    }

    public void setFormaLote(String formaLote) {
        this.formaLote = formaLote;
    }

    @Column(name = "FORMA_NEGOCIACION")
    public String getFormaNegociacion() {
        return this.formaNegociacion;
    }

    public void setFormaNegociacion(String formaNegociacion) {
        this.formaNegociacion = formaNegociacion;
    }

    @Column(name = "FRENTE_LOTE")
    public Double getFrenteLote() {
        return this.frenteLote;
    }

    public void setFrenteLote(Double frenteLote) {
        this.frenteLote = frenteLote;
    }

    @Column(name = "FRENTE_VIAS")
    public String getFrenteVias() {
        return this.frenteVias;
    }

    public void setFrenteVias(String frenteVias) {
        this.frenteVias = frenteVias;
    }

    public String getFuente() {
        return this.fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    @Column(name = "INVESTIGACION_INGRESO")
    public String getInvestigacionIngreso() {
        return this.investigacionIngreso;
    }

    public void setInvestigacionIngreso(String investigacionIngreso) {
        this.investigacionIngreso = investigacionIngreso;
    }

    public String getLocal() {
        return this.local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getMezanine() {
        return this.mezanine;
    }

    public void setMezanine(String mezanine) {
        this.mezanine = mezanine;
    }
//--------------------------------------------------------------------------------------------------

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO")
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }
//-----------------------------------------------------------

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO")
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }
//--------------------------------------------------------------------------------------------------

    @Column(name = "NOMBRE_BARRIO")
    public String getNombreBarrio() {
        return this.nombreBarrio;
    }

    public void setNombreBarrio(String nombreBarrio) {
        this.nombreBarrio = nombreBarrio;
    }

    @Column(name = "NOMBRE_FUENTE")
    public String getNombreFuente() {
        return this.nombreFuente;
    }

    public void setNombreFuente(String nombreFuente) {
        this.nombreFuente = nombreFuente;
    }

    @Column(name = "NOMBRE_PROYECTO")
    public String getNombreProyecto() {
        return this.nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    @Column(name = "NOMBRE_VEREDA")
    public String getNombreVereda() {
        return this.nombreVereda;
    }

    public void setNombreVereda(String nombreVereda) {
        this.nombreVereda = nombreVereda;
    }

    @Column(name = "NUMERO_APARTAMENTOS")
    public Integer getNumeroApartamentos() {
        return this.numeroApartamentos;
    }

    public void setNumeroApartamentos(Integer numeroApartamentos) {
        this.numeroApartamentos = numeroApartamentos;
    }

    @Column(name = "NUMERO_BANIOS")
    public Integer getNumeroBanios() {
        return this.numeroBanios;
    }

    public void setNumeroBanios(Integer numeroBanios) {
        this.numeroBanios = numeroBanios;
    }

    @Column(name = "NUMERO_BODEGAS")
    public Integer getNumeroBodegas() {
        return this.numeroBodegas;
    }

    public void setNumeroBodegas(Integer numeroBodegas) {
        this.numeroBodegas = numeroBodegas;
    }

    @Column(name = "NUMERO_COCINAS")
    public Integer getNumeroCocinas() {
        return this.numeroCocinas;
    }

    public void setNumeroCocinas(Integer numeroCocinas) {
        this.numeroCocinas = numeroCocinas;
    }

    @Column(name = "NUMERO_DEPOSITOS")
    public Integer getNumeroDepositos() {
        return this.numeroDepositos;
    }

    public void setNumeroDepositos(Integer numeroDepositos) {
        this.numeroDepositos = numeroDepositos;
    }

    @Column(name = "NUMERO_HABITACIONES")
    public Integer getNumeroHabitaciones() {
        return this.numeroHabitaciones;
    }

    public void setNumeroHabitaciones(Integer numeroHabitaciones) {
        this.numeroHabitaciones = numeroHabitaciones;
    }

    @Column(name = "NUMERO_LOCALES")
    public Integer getNumeroLocales() {
        return this.numeroLocales;
    }

    public void setNumeroLocales(Integer numeroLocales) {
        this.numeroLocales = numeroLocales;
    }

    @Column(name = "NUMERO_OFICINAS")
    public Integer getNumeroOficinas() {
        return this.numeroOficinas;
    }

    public void setNumeroOficinas(Integer numeroOficinas) {
        this.numeroOficinas = numeroOficinas;
    }

    @Column(name = "NUMERO_PARQUEADEROS")
    public Integer getNumeroParqueaderos() {
        return this.numeroParqueaderos;
    }

    public void setNumeroParqueaderos(Integer numeroParqueaderos) {
        this.numeroParqueaderos = numeroParqueaderos;
    }

    @Column(name = "NUMERO_PISOS")
    public Integer getNumeroPisos() {
        return this.numeroPisos;
    }

    public void setNumeroPisos(Integer numeroPisos) {
        this.numeroPisos = numeroPisos;
    }

    @Column(name = "NUMERO_SOTANOS")
    public Integer getNumeroSotanos() {
        return this.numeroSotanos;
    }

    public void setNumeroSotanos(Integer numeroSotanos) {
        this.numeroSotanos = numeroSotanos;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "OTROS_SERVICIOS_COMUNALES")
    public String getOtrosServiciosComunales() {
        return this.otrosServiciosComunales;
    }

    public void setOtrosServiciosComunales(String otrosServiciosComunales) {
        this.otrosServiciosComunales = otrosServiciosComunales;
    }

    @Column(name = "PORCENTAJE_NEGOCIACION")
    public Double getPorcentajeNegociacion() {
        return this.porcentajeNegociacion;
    }

    public void setPorcentajeNegociacion(Double porcentajeNegociacion) {
        this.porcentajeNegociacion = porcentajeNegociacion;
    }

    @Column(name = "PUENTE_GRUA")
    public String getPuenteGrua() {
        return this.puenteGrua;
    }

    public void setPuenteGrua(String puenteGrua) {
        this.puenteGrua = puenteGrua;
    }

    @Column(name = "RECURSOS_HIDRICOS")
    public String getRecursosHidricos() {
        return this.recursosHidricos;
    }

    public void setRecursosHidricos(String recursosHidricos) {
        this.recursosHidricos = recursosHidricos;
    }

    @Column(name = "RESPONSABLE_INFORMACION")
    public String getResponsableInformacion() {
        return this.responsableInformacion;
    }

    public void setResponsableInformacion(String responsableInformacion) {
        this.responsableInformacion = responsableInformacion;
    }

    public String getRiego() {
        return this.riego;
    }

    public void setRiego(String riego) {
        this.riego = riego;
    }

    public String getSector() {
        return this.sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getServidumbre() {
        return this.servidumbre;
    }

    public void setServidumbre(String servidumbre) {
        this.servidumbre = servidumbre;
    }

    @Column(name = "TAMANIO_PARQUEADERO")
    public String getTamanioParqueadero() {
        return this.tamanioParqueadero;
    }

    public void setTamanioParqueadero(String tamanioParqueadero) {
        this.tamanioParqueadero = tamanioParqueadero;
    }

    @Column(name = "TELEFONO_FUENTE")
    public String getTelefonoFuente() {
        return this.telefonoFuente;
    }

    public void setTelefonoFuente(String telefonoFuente) {
        this.telefonoFuente = telefonoFuente;
    }

    @Column(name = "TIPO_BODEGA")
    public String getTipoBodega() {
        return this.tipoBodega;
    }

    public void setTipoBodega(String tipoBodega) {
        this.tipoBodega = tipoBodega;
    }

    @Column(name = "TIPO_CERCHA")
    public String getTipoCercha() {
        return this.tipoCercha;
    }

    public void setTipoCercha(String tipoCercha) {
        this.tipoCercha = tipoCercha;
    }

    @Column(name = "TIPO_COCINA")
    public String getTipoCocina() {
        return this.tipoCocina;
    }

    public void setTipoCocina(String tipoCocina) {
        this.tipoCocina = tipoCocina;
    }

    @Column(name = "TIPO_EDIFICIO")
    public String getTipoEdificio() {
        return this.tipoEdificio;
    }

    public void setTipoEdificio(String tipoEdificio) {
        this.tipoEdificio = tipoEdificio;
    }

    @Column(name = "TIPO_FINCA")
    public String getTipoFinca() {
        return this.tipoFinca;
    }

    public void setTipoFinca(String tipoFinca) {
        this.tipoFinca = tipoFinca;
    }

    @Column(name = "TIPO_INMUEBLE")
    public String getTipoInmueble() {
        return this.tipoInmueble;
    }

    public void setTipoInmueble(String tipoInmueble) {
        this.tipoInmueble = tipoInmueble;
    }

    @Column(name = "TIPO_INVESTIGACION")
    public String getTipoInvestigacion() {
        return this.tipoInvestigacion;
    }

    public void setTipoInvestigacion(String tipoInvestigacion) {
        this.tipoInvestigacion = tipoInvestigacion;
    }

    @Column(name = "TIPO_LOCAL")
    public String getTipoLocal() {
        return this.tipoLocal;
    }

    public void setTipoLocal(String tipoLocal) {
        this.tipoLocal = tipoLocal;
    }

    @Column(name = "TIPO_LOTE")
    public String getTipoLote() {
        return this.tipoLote;
    }

    public void setTipoLote(String tipoLote) {
        this.tipoLote = tipoLote;
    }

    @Column(name = "TIPO_OFERTA")
    public String getTipoOferta() {
        return this.tipoOferta;
    }

    public void setTipoOferta(String tipoOferta) {
        this.tipoOferta = tipoOferta;
    }

    @Column(name = "TIPO_PARQUEADERO")
    public String getTipoParqueadero() {
        return this.tipoParqueadero;
    }

    public void setTipoParqueadero(String tipoParqueadero) {
        this.tipoParqueadero = tipoParqueadero;
    }

    @Column(name = "TIPO_PREDIO")
    public String getTipoPredio() {
        return this.tipoPredio;
    }

    public void setTipoPredio(String tipoPredio) {
        this.tipoPredio = tipoPredio;
    }

    public String getTopografia() {
        return this.topografia;
    }

    public void setTopografia(String topografia) {
        this.topografia = topografia;
    }

    public String getUbicacion() {
        return this.ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Column(name = "UBICACION_BODEGA")
    public String getUbicacionBodega() {
        return this.ubicacionBodega;
    }

    public void setUbicacionBodega(String ubicacionBodega) {
        this.ubicacionBodega = ubicacionBodega;
    }

    @Column(name = "UBICACION_EN_PISO")
    public Integer getUbicacionEnPiso() {
        return this.ubicacionEnPiso;
    }

    public void setUbicacionEnPiso(Integer ubicacionEnPiso) {
        this.ubicacionEnPiso = ubicacionEnPiso;
    }

    @Column(name = "UBICACION_LOCAL")
    public String getUbicacionLocal() {
        return this.ubicacionLocal;
    }

    public void setUbicacionLocal(String ubicacionLocal) {
        this.ubicacionLocal = ubicacionLocal;
    }

    @Column(name = "UBICACION_MANZANA")
    public String getUbicacionManzana() {
        return this.ubicacionManzana;
    }

    public void setUbicacionManzana(String ubicacionManzana) {
        this.ubicacionManzana = ubicacionManzana;
    }

    @Column(name = "UBICACION_OFICINA")
    public String getUbicacionOficina() {
        return this.ubicacionOficina;
    }

    public void setUbicacionOficina(String ubicacionOficina) {
        this.ubicacionOficina = ubicacionOficina;
    }

    @Column(name = "UNIDAD_AREA")
    public String getUnidadArea() {
        return this.unidadArea;
    }

    public void setUnidadArea(String unidadArea) {
        this.unidadArea = unidadArea;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_ADMINISTRACION")
    public Double getValorAdministracion() {
        return this.valorAdministracion;
    }

    public void setValorAdministracion(Double valorAdministracion) {
        this.valorAdministracion = valorAdministracion;
    }

    @Column(name = "VALOR_ARRIENDO")
    public Double getValorArriendo() {
        return this.valorArriendo;
    }

    public void setValorArriendo(Double valorArriendo) {
        this.valorArriendo = valorArriendo;
    }

    @Column(name = "VALOR_DEPOSITOS")
    public Double getValorDepositos() {
        return this.valorDepositos;
    }

    public void setValorDepositos(Double valorDepositos) {
        this.valorDepositos = valorDepositos;
    }

    @Column(name = "VALOR_DEPURADO")
    public Double getValorDepurado() {
        return this.valorDepurado;
    }

    public void setValorDepurado(Double valorDepurado) {
        this.valorDepurado = valorDepurado;
    }

    @Column(name = "VALOR_PARQUEADEROS")
    public Double getValorParqueaderos() {
        return this.valorParqueaderos;
    }

    public void setValorParqueaderos(Double valorParqueaderos) {
        this.valorParqueaderos = valorParqueaderos;
    }

    @Column(name = "VALOR_PEDIDO")
    public Double getValorPedido() {
        return this.valorPedido;
    }

    public void setValorPedido(Double valorPedido) {
        this.valorPedido = valorPedido;
    }

    public String getVetustez() {
        return this.vetustez;
    }

    public void setVetustez(String vetustez) {
        this.vetustez = vetustez;
    }

    @Column(name = "ZONA_FRANCA")
    public String getZonaFranca() {
        return this.zonaFranca;
    }

    public void setZonaFranca(String zonaFranca) {
        this.zonaFranca = zonaFranca;
    }

    // bi-directional many-to-one association to OfertaAreaUsoExclusivo
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaInmobiliaria")
    public List<OfertaAreaUsoExclusivo> getOfertaAreaUsoExclusivos() {
        return this.ofertaAreaUsoExclusivos;
    }

    public void setOfertaAreaUsoExclusivos(
        List<OfertaAreaUsoExclusivo> ofertaAreaUsoExclusivos) {
        this.ofertaAreaUsoExclusivos = ofertaAreaUsoExclusivos;
    }

    // bi-directional many-to-one association to OfertaInmobiliaria
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OFERTA_INMOBILIARIA_ID")
    public OfertaInmobiliaria getOfertaInmobiliaria() {
        return this.ofertaInmobiliaria;
    }

    public void setOfertaInmobiliaria(OfertaInmobiliaria ofertaInmobiliaria) {
        this.ofertaInmobiliaria = ofertaInmobiliaria;
    }

    // bi-directional many-to-one association to OfertaInmobiliaria
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaInmobiliaria")
    public List<OfertaInmobiliaria> getOfertaInmobiliarias() {
        return this.ofertaInmobiliarias;
    }

    public void setOfertaInmobiliarias(
        List<OfertaInmobiliaria> ofertaInmobiliarias) {
        this.ofertaInmobiliarias = ofertaInmobiliarias;
    }

    // bi-directional many-to-one association to OfertaRecursoHidrico
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaInmobiliaria")
    public List<OfertaRecursoHidrico> getOfertaRecursoHidricos() {
        return this.ofertaRecursoHidricos;
    }

    public void setOfertaRecursoHidricos(
        List<OfertaRecursoHidrico> ofertaRecursoHidricos) {
        this.ofertaRecursoHidricos = ofertaRecursoHidricos;
    }

    // bi-directional many-to-one association to OfertaServicioComunal
    @OneToMany(mappedBy = "ofertaInmobiliaria", cascade = CascadeType.ALL)
    public List<OfertaServicioComunal> getOfertaServicioComunals() {
        return this.ofertaServicioComunals;
    }

    public void setOfertaServicioComunals(
        List<OfertaServicioComunal> ofertaServicioComunals) {
        this.ofertaServicioComunals = ofertaServicioComunals;
    }

    // bi-directional many-to-one association to OfertaServicioPublico
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaInmobiliaria")
    public List<OfertaServicioPublico> getOfertaServicioPublicos() {
        return this.ofertaServicioPublicos;
    }

    public void setOfertaServicioPublicos(
        List<OfertaServicioPublico> ofertaServicioPublicos) {
        this.ofertaServicioPublicos = ofertaServicioPublicos;
    }

    //bi-directional many-to-one association to OfertaPredio
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ofertaInmobiliaria", fetch = FetchType.LAZY)
    public List<OfertaPredio> getOfertaPredios() {
        return this.ofertaPredios;
    }

    public void setOfertaPredios(List<OfertaPredio> ofertaPredios) {
        this.ofertaPredios = ofertaPredios;
    }

    // bi-directional many-to-one association to Fotografia
    @OneToMany(mappedBy = "ofertaInmobiliaria", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<Fotografia> getFotografias() {
        return this.fotografias;
    }

    public void setFotografias(List<Fotografia> fotografias) {
        this.fotografias = fotografias;
    }

    /**
     * Método que determina si la condición juridica de la oferta inmobiliaria es propiedad
     * horizontal
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isPropiedadHorizontal() {
        if (this.condicionJuridica != null &&
            this.condicionJuridica.equals(EOfertaCondicionJuridica.PH.getCodigo())) {
            return true;
        }
        return false;
    }

    /**
     * Metodo que determina si la oferta es un inmueble asociado a un proyecto
     *
     * @author christian.rodriguez
     * @return verdadero si es inmueble, falso si es proyecto o usado
     */
    @Transient
    public boolean isInmuebleDeProyecto() {
        if (this.tipoOferta != null &&
            this.tipoOferta.equalsIgnoreCase(EOfertaTipoOferta.PROYECTOS.getCodigo()) &&
            this.tipoInmueble != null) {
            return true;
        }
        return false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * decide si esta oferta es de tipo PROYECTOS
     *
     * @autor pedro.garcia
     * @return
     */
    @Transient
    public boolean isEsProyecto() {
        boolean answer;
        answer = (this.tipoOferta.equals(EOfertaTipoOferta.PROYECTOS.getCodigo())) ? true : false;
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * verifica si esta oferta es tiene el atributo 'casa' en SI
     *
     * @autor pedro.garcia
     * @return
     */
    @Transient
    public boolean isTieneCasa() {
        boolean answer;
        answer = (this.casa.equals(ESiNo.SI.toString())) ? true : false;
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * verifica si esta oferta es tiene el atributo 'recursosHidricos' en SI
     *
     * @autor pedro.garcia
     * @return
     */
    @Transient
    public boolean isTieneRecursosHidricos() {
        boolean answer;
        answer = (this.recursosHidricos.equals(ESiNo.SI.toString())) ? true : false;
        return answer;
    }
    //--------------------------------------------------------------------------------------------------

    /**
     * verifica si esta oferta esta finalizada
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    @Transient
    public boolean isEstaFinalizada() {
        boolean answer;

        if (this.estado != null && this.estado.equals(EOfertaEstado.FINALIZADA.getCodigo())) {
            answer = true;
        } else {
            answer = false;
        }
        return answer;
    }
    //--------------------------------------------------------------------------------------------------

    /**
     * Indica si para esta oferta el Visor GIS se puede visualizar
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    @Transient
    public boolean isEsVisorVisible() {
        if (this.ofertaPredios.isEmpty() || this.ofertaPredios == null) {
            return false;
        }
        return true;
    }

    @Column(name = "RECOLECTOR_ID")
    public String getRecolectorId() {
        return recolectorId;
    }

    public void setRecolectorId(String recolectorId) {
        this.recolectorId = recolectorId;
    }

    @Column(name = "REGION_CAPTURA_OFERTA_ID")
    public Long getRegionCapturaOfertaId() {
        return regionCapturaOfertaId;
    }

    public void setRegionCapturaOfertaId(Long regionCapturaOfertaId) {
        this.regionCapturaOfertaId = regionCapturaOfertaId;
    }

    @Transient
    public String getCadenaDeManzanasVeredas() {
        String cadenaManzanasVeredas = "";

        if (this.ofertaPredios != null && !this.ofertaPredios.isEmpty()) {

            for (OfertaPredio predio : this.ofertaPredios) {

                if (predio.getNumeroPredial() != null && !predio.getNumeroPredial().isEmpty()) {

                    if (!cadenaManzanasVeredas.isEmpty()) {
                        cadenaManzanasVeredas = cadenaManzanasVeredas.concat("," +
                            predio.getNumeroPredial());
                    } else {
                        cadenaManzanasVeredas = predio.getNumeroPredial();
                    }
                }
            }
        }
        return cadenaManzanasVeredas;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OfertaInmobiliaria other = (OfertaInmobiliaria) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
