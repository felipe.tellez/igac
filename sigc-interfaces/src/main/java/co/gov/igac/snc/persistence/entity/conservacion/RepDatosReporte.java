package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;

/**
 * Entidad donde se almacenan los reportes prediales
 *
 * @author javier.aponte
 *
 */
@Entity
@Table(name = "REP_DATOS_REPORTE")
public class RepDatosReporte implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private BigDecimal areaConstruccion;
    private BigDecimal areaTerreno;
    private String circuloRegistral;
    private String departamentoCodigo;
    private String destino;
    private String direccion;
    private String municipioCodigo;
    private String nombre;
    private String numeroIdentificacion;
    private String numeroPredial20;
    private String numeroPredial30;
    private String numeroRegistro;
    private String numeroRegistroAnterior;
    private BigDecimal predioId;
    private String tipo;
    private String tipoIdentificacion;
    private BigDecimal valorAvaluo;
    private RepControlReporte repControlReporte;

    public RepDatosReporte() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REP_DATOS_REPORTE_ID_SEQ")
    @SequenceGenerator(name = "REP_DATOS_REPORTE_ID_SEQ", sequenceName = "REP_DATOS_REPORTE_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA_CONSTRUCCION")
    public BigDecimal getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(BigDecimal areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "AREA_TERRENO")
    public BigDecimal getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(BigDecimal areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "CIRCULO_REGISTRAL")
    public String getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(String circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "DEPARTAMENTO_CODIGO")
    public String getDepartamentoCodigo() {
        return this.departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "MUNICIPIO_CODIGO")
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "NUMERO_IDENTIFICACION")
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "NUMERO_PREDIAL_20")
    public String getNumeroPredial20() {
        return this.numeroPredial20;
    }

    public void setNumeroPredial20(String numeroPredial20) {
        this.numeroPredial20 = numeroPredial20;
    }

    @Column(name = "NUMERO_PREDIAL_30")
    public String getNumeroPredial30() {
        return this.numeroPredial30;
    }

    public void setNumeroPredial30(String numeroPredial30) {
        this.numeroPredial30 = numeroPredial30;
    }

    @Column(name = "NUMERO_REGISTRO")
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "NUMERO_REGISTRO_ANTERIOR")
    public String getNumeroRegistroAnterior() {
        return this.numeroRegistroAnterior;
    }

    public void setNumeroRegistroAnterior(String numeroRegistroAnterior) {
        this.numeroRegistroAnterior = numeroRegistroAnterior;
    }

    @Column(name = "PREDIO_ID")
    public BigDecimal getPredioId() {
        return this.predioId;
    }

    public void setPredioId(BigDecimal predioId) {
        this.predioId = predioId;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "TIPO_IDENTIFICACION")
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "VALOR_AVALUO")
    public BigDecimal getValorAvaluo() {
        return this.valorAvaluo;
    }

    public void setValorAvaluo(BigDecimal valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

    //bi-directional many-to-one association to RepControlReporte
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTROL_REPORTE_ID")
    public RepControlReporte getRepControlReporte() {
        return this.repControlReporte;
    }

    public void setRepControlReporte(RepControlReporte repControlReporte) {
        this.repControlReporte = repControlReporte;
    }

}
