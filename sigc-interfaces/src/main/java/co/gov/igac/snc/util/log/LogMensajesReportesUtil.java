package co.gov.igac.snc.util.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.comun.interfaces.ILogMensaje;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.persistence.entity.generales.LogMensaje;
import co.gov.igac.snc.util.LogMensajeFactory;

/**
 * Maneja el log de mensajes para los reportes
 *
 * @author juan.mendez
 * @version 2.0
 */
public class LogMensajesReportesUtil implements ILogMensaje {

    public static Logger LOGGER = LoggerFactory.getLogger(LogMensajesReportesUtil.class);

    public final static String SERVICIO_REPORTES = "Servicio.Reportes";
    public final static String SERVICIO_SIG = "Servicio.SIG";

    private IGenerales generalesService;
    private UsuarioDTO usuario;

    public LogMensajesReportesUtil(IGenerales generalesService, UsuarioDTO usuario) {
        this.generalesService = generalesService;
        this.usuario = usuario;
    }

    /**
     *
     * @param mensaje
     * @param tipoMensaje
     */
    @Override
    public void logMensaje(String mensaje, String tipoMensaje) {
        LogMensaje log = LogMensajeFactory.getLogMensaje(usuario, tipoMensaje);
        log.setMensaje(mensaje);
        this.generalesService.agregarLogMensaje(log);
    }

}
