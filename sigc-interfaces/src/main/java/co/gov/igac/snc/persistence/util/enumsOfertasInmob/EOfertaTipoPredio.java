/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util.enumsOfertasInmob;

/**
 *
 * @author pedro.garcia
 */
public enum EOfertaTipoPredio {

    //D: código (valor)
    RURAL("Rural", "00"),
    URBANO("Urbano", "0X");

    private String valor;
    private String codigo;

    private EOfertaTipoPredio(String valor, String codigo) {
        this.valor = valor;
        this.codigo = codigo;
    }

    public String getValor() {
        return this.valor;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
