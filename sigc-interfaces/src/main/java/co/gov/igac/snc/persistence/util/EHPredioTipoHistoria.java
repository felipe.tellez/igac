package co.gov.igac.snc.persistence.util;

public enum EHPredioTipoHistoria {
    ORIGINAL("O"),
    PROYECCION("P");

    private String codigo;

    EHPredioTipoHistoria(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
