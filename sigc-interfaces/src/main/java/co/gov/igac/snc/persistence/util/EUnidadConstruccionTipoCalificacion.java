/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los tipos de calificación de una construcción
 *
 * @author pedro.garcia
 * @modified se cambió el uso de un valor ANY para invocación de métodos de la enumeración por la
 * definición estática de los mismos métodos.
 */
public enum EUnidadConstruccionTipoCalificacion {

    //nombre local (codigo, valor)
    //D: los códigos de los dominios son varchar2 en la bd
    ANEXO("ANEXO", "ANEXO"),
    COMERCIAL("COMERCIAL", "COMERCIAL"),
    INDUSTRIAL("INDUSTRIAL", "INDUSTRIAL"),
    RESIDENCIAL("RESIDENCIAL", "RESIDENCIAL");

    //D: adicionado para poder invocar el método getCodigoByNombre con un valor cualquiera
    //ANY ("0", "any");
    private String codigo;
    private String nombre;

    private EUnidadConstruccionTipoCalificacion(String codigoP, String nombreP) {
        this.codigo = codigoP;
        this.nombre = nombreP;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public static String getCodigoByNombre(String nombreP) {
        String answer = "";
        for (EUnidadConstruccionTipoCalificacion tipoCalificacion
            : EUnidadConstruccionTipoCalificacion.values()) {

            if (tipoCalificacion.getNombre().compareToIgnoreCase(nombreP) == 0) {
                answer = tipoCalificacion.getCodigo();
                break;
            }
        }
        return answer;
    }

}
