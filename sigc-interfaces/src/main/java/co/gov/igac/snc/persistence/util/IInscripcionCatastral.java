package co.gov.igac.snc.persistence.util;

import java.util.Date;

/**
 * Interfaz que deben implementar los pojos que tienen la fecha de inscripcion catastral para que al
 * cancelar, esta fecha sea seteada
 *
 * @author fredy.wilches
 *
 */
public interface IInscripcionCatastral {

    public Date getFechaInscripcionCatastral();

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral);
}
