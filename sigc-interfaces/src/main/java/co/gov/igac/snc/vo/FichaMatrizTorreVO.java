package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.List;

import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;

/**
 * Clase que reduce la informacion de las torres de la ficha matriz para ser mostrada en las tareas
 * geograficas
 *
 * @author franz.gamba
 */
public class FichaMatrizTorreVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8763863852387888171L;

    private Long id;
    private Long pisos;
    private Long sotanos;
    private Long torre;
    private Long unidades;
    private Long unidadesSotanos;
    private String provieneFicha;
    private Long provieneTorre;
    //private List<FichaMatrizPredioVO> predios;
    private List<FichaMatrizPisoVO> fichaMatrizPisos;
    private List<FichaMatrizSotanoVO> fichaMatrizSotanos;

    /** Empty constructor */
    public FichaMatrizTorreVO() {

    }

    /** Constructor a partir de un FichaMatrizTorre */
    public FichaMatrizTorreVO(FichaMatrizTorre torre) {
        this.id = torre.getId();
        this.pisos = torre.getPisos();
        this.sotanos = torre.getSotanos();
        this.torre = torre.getTorre();
        this.unidades = torre.getUnidades();
        this.unidadesSotanos = torre.getUnidadesSotanos();
    }

    /** Constructor a partir de un PFichaMatrizTorre */
    public FichaMatrizTorreVO(PFichaMatrizTorre torre) {
        this.id = torre.getId();
        this.pisos = torre.getPisos();
        this.sotanos = torre.getSotanos();
        this.torre = torre.getTorre();
        this.unidades = torre.getUnidades();
        this.unidadesSotanos = torre.getUnidadesSotanos();
        if (torre.getProvieneFichaMatriz() != null && torre.getProvieneTorre() != null) {
            this.provieneFicha = torre.getProvieneFichaMatriz().getPredio().getNumeroPredial();
            this.provieneTorre = torre.getProvieneTorre().getTorre();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPisos() {
        return pisos;
    }

    public void setPisos(Long pisos) {
        this.pisos = pisos;
    }

    public Long getSotanos() {
        return sotanos;
    }

    public void setSotanos(Long sotanos) {
        this.sotanos = sotanos;
    }

    public Long getTorre() {
        return torre;
    }

    public void setTorre(Long torre) {
        this.torre = torre;
    }

    public Long getUnidades() {
        return unidades;
    }

    public void setUnidades(Long unidades) {
        this.unidades = unidades;
    }

    public Long getUnidadesSotanos() {
        return unidadesSotanos;
    }

    public void setUnidadesSotanos(Long unidadesSotanos) {
        this.unidadesSotanos = unidadesSotanos;
    }

    public List<FichaMatrizPisoVO> getFichaMatrizPisos() {
        return fichaMatrizPisos;
    }

    public void setFichaMatrizPisos(List<FichaMatrizPisoVO> fichaMatrizPisos) {
        this.fichaMatrizPisos = fichaMatrizPisos;
    }

    public List<FichaMatrizSotanoVO> getFichaMatrizSotanos() {
        return fichaMatrizSotanos;
    }

    public void setFichaMatrizSotanos(List<FichaMatrizSotanoVO> fichaMatrizSotanos) {
        this.fichaMatrizSotanos = fichaMatrizSotanos;
    }

//	public List<FichaMatrizPredioVO> getPredios() {
//		return predios;
//	}
//
//	public void setPredios(List<FichaMatrizPredioVO> predios) {
//		this.predios = predios;
//	}
    public Long getProvieneTorre() {
        return provieneTorre;
    }

    public void setProvieneTorre(Long provieneTorre) {
        this.provieneTorre = provieneTorre;
    }

    public String getProvieneFicha() {
        return provieneFicha;
    }

    public void setProvieneFicha(String provieneFicha) {
        this.provieneFicha = provieneFicha;
    }

}
