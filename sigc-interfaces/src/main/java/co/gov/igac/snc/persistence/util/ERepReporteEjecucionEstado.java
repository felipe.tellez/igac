/**
 * IGAC proyecto SNC
 */

package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles estados de un reporte de ejecucion
 * @author leidy.gonzalez
 */
public enum ERepReporteEjecucionEstado {

	// D: el atributo 'codigo' corresponde al código en la bd
	// el nombre de cada enumeración es el usado en la aplicación
	// D: los códigos de los dominios son varchar2 en la bd
	
	INICIADO ("INICIADO"),
	EN_PROCESO("EN PROCESO"),
	ERROR("ERROR"),
	ERROR_ALMACENANDO("ERROR_ALMACENANDO"),
	ERROR_GENERANDO("ERROR_GENERANDO"),
	TERMINADO ("TERMINADO"),
	
	// Este estado solo se utiliza para mantener persistida una observación
	// temporal mientras se avanza el proceso.
	FINALIZADO("FINALIZADO");

	private String codigo;

	private ERepReporteEjecucionEstado(String codigoP){
		this.codigo = codigoP;
	}
	    
	public String getCodigo() {
		return this.codigo;
	}

}
