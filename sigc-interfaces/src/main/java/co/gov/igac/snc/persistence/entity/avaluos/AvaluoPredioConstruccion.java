package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 * The persistent class for the AVALUO_PREDIO_CONSTRUCCION database table.
 *
 */
@Entity
@Table(name = "AVALUO_PREDIO_CONSTRUCCION")
public class AvaluoPredioConstruccion implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Avaluo avaluo;
    private Long avaluoPredioId;
    private String conservacion;
    private String descripcion;
    private Double dimension;
    private Double edad;
    private Date fechaLog;
    private String tipo;
    private String unidadMedidaDimension;
    private String usuarioLog;
    private Double valorTotal;
    private Double valorUnitario;
    private List<AvaluoAreaRegistrada> avaluoAreaRegistradas;

    //Transients para mostrar información del predio
    private String numeroPredial;
    private String codigoMunicipio;
    private String codigoDepartamento;

    public AvaluoPredioConstruccion() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_PREDIO_CONSTRUCCION_ID_GENERATOR", sequenceName =
        "AVALUO_PREDIO_CONSTRUCC_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_PREDIO_CONSTRUCCION_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    @Column(name = "AVALUO_PREDIO_ID")
    public Long getAvaluoPredioId() {
        return this.avaluoPredioId;
    }

    public void setAvaluoPredioId(Long avaluoPredioId) {
        this.avaluoPredioId = avaluoPredioId;
    }

    public String getConservacion() {
        return this.conservacion;
    }

    public void setConservacion(String conservacion) {
        this.conservacion = conservacion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getDimension() {
        return this.dimension;
    }

    public void setDimension(Double dimension) {
        this.dimension = dimension;
    }

    public Double getEdad() {
        return this.edad;
    }

    public void setEdad(Double edad) {
        this.edad = edad;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "UNIDAD_MEDIDA_DIMENSION")
    public String getUnidadMedidaDimension() {
        return this.unidadMedidaDimension;
    }

    public void setUnidadMedidaDimension(String unidadMedidaDimension) {
        this.unidadMedidaDimension = unidadMedidaDimension;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_TOTAL")
    public Double getValorTotal() {
        return this.valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    @Column(name = "VALOR_UNITARIO")
    public Double getValorUnitario() {
        return this.valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    @Transient
    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "avaluoPredioConstruccion")
    public List<AvaluoAreaRegistrada> getAvaluoAreaRegistradas() {
        return avaluoAreaRegistradas;
    }

    public void setAvaluoAreaRegistradas(List<AvaluoAreaRegistrada> avaluoAreaRegistradas) {
        this.avaluoAreaRegistradas = avaluoAreaRegistradas;
    }

    //---------------------------------------------------------------------------------------------
    @Transient
    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    @Transient
    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

}
