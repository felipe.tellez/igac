package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the CONVENIO_ENTIDAD database table.
 */
@Entity
@Table(name = "CONVENIO_ENTIDAD", schema = "SNC_ACTUALIZACION")
public class ConvenioEntidad implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5856521749574798868L;
    private Long id;
    private Date fechaLog;
    private Double monto;
    private String nit;
    private String nombre;
    private String numeroConvenio;
    private Double porcentajeParticipacion;
    private String usuarioLog;
    private Convenio convenio;

    public ConvenioEntidad() {
    }

    @Id
    @SequenceGenerator(name = "CONVENIO_ENTIDAD_ID_SEQ",
        sequenceName = "CONVENIO_ENTIDAD_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONVENIO_ENTIDAD_ID_SEQ")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(nullable = false, precision = 18, scale = 2)
    public Double getMonto() {
        return this.monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    @Column(nullable = false, length = 20)
    public String getNit() {
        return this.nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    @Column(nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "NUMERO_CONVENIO", length = 20)
    public String getNumeroConvenio() {
        return this.numeroConvenio;
    }

    public void setNumeroConvenio(String numeroConvenio) {
        this.numeroConvenio = numeroConvenio;
    }

    @Column(name = "PORCENTAJE_PARTICIPACION", nullable = false, precision = 5, scale = 2)
    public Double getPorcentajeParticipacion() {
        return this.porcentajeParticipacion;
    }

    public void setPorcentajeParticipacion(Double porcentajeParticipacion) {
        this.porcentajeParticipacion = porcentajeParticipacion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Convenio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONVENIO_ID", nullable = false)
    public Convenio getConvenio() {
        return this.convenio;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }

}
