package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;

/**
 * Filtro para los campos de la consulta de trámites notificados y pendientes de notificar
 *
 * @author javier.aponte
 */
public class FiltroDatosConsultaTramitesNotificacion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6445584706810020167L;
    private String departamentoId;
    private String municipioId;
    private String numeroPredial;
    private String numeroSolicitud;

    /**
     * segmentos del número predial
     */
    private String numeroPredialS1;
    private String numeroPredialS2;
    private String numeroPredialS3;
    private String numeroPredialS4;
    private String numeroPredialS5;
    private String numeroPredialS6;
    private String numeroPredialS7;
    private String numeroPredialS8;
    private String numeroPredialS9;
    private String numeroPredialS10;
    private String numeroPredialS11;
    private String numeroPredialS12;

    private String tipoTramite;

    private Date fechaRadicacion;

    public String getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(String departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(String municipioId) {
        this.municipioId = municipioId;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getNumeroPredialS1() {
        return numeroPredialS1;
    }

    public void setNumeroPredialS1(String numeroPredialS1) {
        this.numeroPredialS1 = numeroPredialS1;
    }

    public String getNumeroPredialS2() {
        return numeroPredialS2;
    }

    public void setNumeroPredialS2(String numeroPredialS2) {
        this.numeroPredialS2 = numeroPredialS2;
    }

    public String getNumeroPredialS3() {
        return numeroPredialS3;
    }

    public void setNumeroPredialS3(String numeroPredialS3) {
        this.numeroPredialS3 = numeroPredialS3;
    }

    public String getNumeroPredialS4() {
        return numeroPredialS4;
    }

    public void setNumeroPredialS4(String numeroPredialS4) {
        this.numeroPredialS4 = numeroPredialS4;
    }

    public String getNumeroPredialS5() {
        return numeroPredialS5;
    }

    public void setNumeroPredialS5(String numeroPredialS5) {
        this.numeroPredialS5 = numeroPredialS5;
    }

    public String getNumeroPredialS6() {
        return numeroPredialS6;
    }

    public void setNumeroPredialS6(String numeroPredialS6) {
        this.numeroPredialS6 = numeroPredialS6;
    }

    public String getNumeroPredialS7() {
        return numeroPredialS7;
    }

    public void setNumeroPredialS7(String numeroPredialS7) {
        this.numeroPredialS7 = numeroPredialS7;
    }

    public String getNumeroPredialS8() {
        return numeroPredialS8;
    }

    public void setNumeroPredialS8(String numeroPredialS8) {
        this.numeroPredialS8 = numeroPredialS8;
    }

    public String getNumeroPredialS9() {
        return numeroPredialS9;
    }

    public void setNumeroPredialS9(String numeroPredialS9) {
        this.numeroPredialS9 = numeroPredialS9;
    }

    public String getNumeroPredialS10() {
        return numeroPredialS10;
    }

    public void setNumeroPredialS10(String numeroPredialS10) {
        this.numeroPredialS10 = numeroPredialS10;
    }

    public String getNumeroPredialS11() {
        return numeroPredialS11;
    }

    public void setNumeroPredialS11(String numeroPredialS11) {
        this.numeroPredialS11 = numeroPredialS11;
    }

    public String getNumeroPredialS12() {
        return numeroPredialS12;
    }

    public void setNumeroPredialS12(String numeroPredialS12) {
        this.numeroPredialS12 = numeroPredialS12;
    }

    //--------------------------------------------------------------------------------------------------
    public void assembleNumeroPredialFromSegments() {
        StringBuilder numeroPredialAssembled;
        numeroPredialAssembled = new StringBuilder();
        numeroPredialAssembled.append(this.numeroPredialS1);
        numeroPredialAssembled.append(this.numeroPredialS2);
        numeroPredialAssembled.append(this.numeroPredialS3);
        numeroPredialAssembled.append(this.numeroPredialS4);
        numeroPredialAssembled.append(this.numeroPredialS5);
        numeroPredialAssembled.append(this.numeroPredialS6);
        numeroPredialAssembled.append(this.numeroPredialS7);
        numeroPredialAssembled.append(this.numeroPredialS8);
        numeroPredialAssembled.append(this.numeroPredialS9);
        numeroPredialAssembled.append(this.numeroPredialS10);
        numeroPredialAssembled.append(this.numeroPredialS11);
        numeroPredialAssembled.append(this.numeroPredialS12);
        this.numeroPredial = numeroPredialAssembled.toString();
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    public Date getFechaRadicacion() {
        return fechaRadicacion;
    }
}
