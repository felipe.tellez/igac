package co.gov.igac.snc.persistence.entity.formacion;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the PARAMETRO_DECRETO_CONDICION database table.
 *
 */
@Entity
@Table(name = "PARAMETRO_DECRETO_CONDICION", schema = "SNC_FORMACION")
public class ParametroDecretoCondicion implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String columna;
    private String dominio;
    private Date fechaLog;
    private String nombre;
    private String tabla;
    private String usuarioLog;

    // Constructors
    /** default constructor */
    public ParametroDecretoCondicion() {
    }

    // Property accessors
    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARAMETRO_DECRETO_CONDI_Id_SEQ")
    @SequenceGenerator(name = "PARAMETRO_DECRETO_CONDI_Id_SEQ", sequenceName =
        "PARAMETRO_DECRETO_CONDI_Id_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColumna() {
        return this.columna;
    }

    public void setColumna(String columna) {
        this.columna = columna;
    }

    public String getDominio() {
        return this.dominio;
    }

    public void setDominio(String dominio) {
        this.dominio = dominio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTabla() {
        return this.tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
