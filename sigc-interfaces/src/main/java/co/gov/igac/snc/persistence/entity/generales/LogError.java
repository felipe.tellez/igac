package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the LOG_ERROR database table.
 *
 */
@Entity
@Table(name = "LOG_ERROR")
public class LogError implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private String codigoOra;
    private Date fecha;
    private String mensajeOra;
    private String mensajeOraDetallado;
    private String mensajeUsuario;
    private String programa;

    public LogError() {
    }

    @Id
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "CODIGO_ORA")
    public String getCodigoOra() {
        return this.codigoOra;
    }

    public void setCodigoOra(String codigoOra) {
        this.codigoOra = codigoOra;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Column(name = "MENSAJE_ORA")
    public String getMensajeOra() {
        return this.mensajeOra;
    }

    public void setMensajeOra(String mensajeOra) {
        this.mensajeOra = mensajeOra;
    }

    @Column(name = "MENSAJE_ORA_DETALLADO")
    public String getMensajeOraDetallado() {
        return this.mensajeOraDetallado;
    }

    public void setMensajeOraDetallado(String mensajeOraDetallado) {
        this.mensajeOraDetallado = mensajeOraDetallado;
    }

    @Column(name = "MENSAJE_USUARIO")
    public String getMensajeUsuario() {
        return this.mensajeUsuario;
    }

    public void setMensajeUsuario(String mensajeUsuario) {
        this.mensajeUsuario = mensajeUsuario;
    }

    public String getPrograma() {
        return this.programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

}
