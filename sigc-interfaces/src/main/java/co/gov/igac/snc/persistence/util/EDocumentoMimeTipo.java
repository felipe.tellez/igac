package co.gov.igac.snc.persistence.util;

/**
 *
 * @author juan.agudelo
 *
 * Contiene la información del tipo MIME de un archivo común
 */
public enum EDocumentoMimeTipo {

    DESCONOCIDO("application/octet-stream", "NA"),
    DOC("application/msword", "doc"),
    DOCX("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx"),
    PDF("application/pd", "pdf"),
    XLS("application/vnd.ms-excel", "xls"),
    XLSX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx");

    private String extension;
    private String mime;

    private EDocumentoMimeTipo(String mime, String extension) {
        this.mime = mime;
        this.extension = extension;
    }

    public String getMime() {
        return mime;
    }

    public String getExtension() {
        return extension;
    }

}
