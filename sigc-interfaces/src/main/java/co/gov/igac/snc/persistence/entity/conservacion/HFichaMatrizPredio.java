package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the H_FICHA_MATRIZ_PREDIO database table.
 *
 * MODIFICACIONES:
 *
 * @modified david.cifuentes :: Adición de la variable estado :: 08/09/15 david.cifuentes ::
 * Modidicación de la variable HFichaMatriz :: 14/09/15
 */
@Entity
@Table(name = "H_FICHA_MATRIZ_PREDIO")
public class HFichaMatrizPredio implements Serializable {

    private static final long serialVersionUID = 1L;
    private HFichaMatrizPredioPK id;
    private String cancelaInscribe;
    private Double coeficiente;
    private Date fechaLog;
    private FichaMatriz fichaMatriz;
    private String numeroPredial;
    private String usuarioLog;
    private HPredio HPredio;
    private String estado;
    private HFichaMatriz HFichaMatriz;

    public HFichaMatrizPredio() {
    }

    @EmbeddedId
    public HFichaMatrizPredioPK getId() {
        return this.id;
    }

    public void setId(HFichaMatrizPredioPK id) {
        this.id = id;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "COEFICIENTE", precision = 16)
    public Double getCoeficiente() {
        return this.coeficiente;
    }

    public void setCoeficiente(Double coeficiente) {
        this.coeficiente = coeficiente;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    // bi-directional many-to-one association to FichaMatriz
    @ManyToOne
    @JoinColumn(name = "FICHA_MATRIZ_ID")
    public FichaMatriz getFichaMatriz() {
        return this.fichaMatriz;
    }

    public void setFichaMatriz(FichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to HPredio
    @ManyToOne
    @JoinColumn(name = "H_PREDIO_ID", insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "H_PREDIO_ID", referencedColumnName = "H_PREDIO_ID", nullable = false,
            insertable = false, updatable = false),
        @JoinColumn(name = "FICHA_MATRIZ_ID", referencedColumnName = "ID", nullable = false,
            insertable = false, updatable = false)})
    public HFichaMatriz getHFichaMatriz() {
        return HFichaMatriz;
    }

    public void setHFichaMatriz(HFichaMatriz HFichaMatriz) {
        this.HFichaMatriz = HFichaMatriz;
    }

}
