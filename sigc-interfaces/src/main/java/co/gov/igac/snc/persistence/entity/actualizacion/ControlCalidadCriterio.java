package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CONTROL_CALIDAD_CRITERIO database table.
 *
 */
@Entity
@Table(name = "CONTROL_CALIDAD_CRITERIO")
public class ControlCalidadCriterio implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7078878419809171122L;

    private Long id;
    private String cumple;
    private String descripcion;
    private Date fechaLog;
    private String usuarioLog;
    private AsignacionControlCalidad asignacionControlCalidad;
    private CriterioControlCalidad criterioControlCalidad;

    public ControlCalidadCriterio() {
    }

    @Id
    @SequenceGenerator(name = "CONTROL_CALIDAD_CRITERI_ID_GENERATOR", sequenceName =
        "CONTROL_CALIDAD_CRITERI_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "CONTROL_CALIDAD_CRITERI_ID_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(length = 2)
    public String getCumple() {
        return this.cumple;
    }

    public void setCumple(String cumple) {
        this.cumple = cumple;
    }

    @Column(length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to AsignacionControlCalidad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASIGNACION_CONTROL_CALIDAD_ID", nullable = false)
    public AsignacionControlCalidad getAsignacionControlCalidad() {
        return this.asignacionControlCalidad;
    }

    public void setAsignacionControlCalidad(
        AsignacionControlCalidad asignacionControlCalidad) {
        this.asignacionControlCalidad = asignacionControlCalidad;
    }

    // bi-directional many-to-one association to CriterioControlCalidad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CRITERIO_CONTROL_CALIDAD_ID", nullable = false)
    public CriterioControlCalidad getCriterioControlCalidad() {
        return this.criterioControlCalidad;
    }

    public void setCriterioControlCalidad(
        CriterioControlCalidad criterioControlCalidad) {
        this.criterioControlCalidad = criterioControlCalidad;
    }

}
