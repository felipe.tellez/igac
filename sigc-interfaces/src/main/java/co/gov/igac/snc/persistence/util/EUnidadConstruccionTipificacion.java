/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con las tipificaciones de una unidad de construcción
 *
 * @author pedro.garcia
 */
public enum EUnidadConstruccionTipificacion {

    //nombre local (codigo, valor)
    //D: los códigos de los dominios son varchar2 en la bd
    BAJO_BAJO("1", "Bajo - bajo"),
    BAJO("2", "Bajo"),
    MEDIO_BAJO("3", "Medio - bajo"),
    MEDIO("4", "Medio"),
    MEDIO_ALTO("5", "Medio - alto"),
    ALTO("6", "Alto"),
    //D: valor acordado para cuando se trata de un anexo
    NA("NA", "No aplica"),
    //D: adicionado para poder invocar el método getCodigoByValor con un valor cualquiera
    ANY("0", "any");

    private String codigo;
    private String valor;

    private EUnidadConstruccionTipificacion(String codigoP, String valorP) {
        this.codigo = codigoP;
        this.valor = valorP;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

    public String getCodigoByValor(String nombreP) {
        String answer = "";
        for (EUnidadConstruccionTipificacion tipificacion : this.values()) {
            if (tipificacion.getValor().compareToIgnoreCase(nombreP) == 0) {
                answer = tipificacion.getCodigo();
                break;
            }
        }
        return answer;
    }

}
