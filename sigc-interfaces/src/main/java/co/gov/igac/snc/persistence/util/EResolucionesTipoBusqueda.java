package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de bd ResTipoBusqueda RegPreCriterioBusqueda
 *
 * @author leidy.gonzalez
 */
public enum EResolucionesTipoBusqueda {

    POR_NUMERO_RESOLUCION_INDIVIDUAL("1"),
    POR_FECHA_RESOLUCION_INDIVIDUAL("2"),
    POR_RANGO_NUMERO_RESOLUCION("3"),
    POR_RANGO_FECHA_RESOLUCION("4"),
    POR_NUMERO_RESOLUCION_INDIVIDUAL_TRAMITE_APLICADO("5"),
    POR_FECHA_RESOLUCION_INDIVIDUAL_TRAMITE_APLICADO("6"),
    POR_RANGO_NUMERO_RESOLUCION_TRAMITE_APLICADO("7"),
    POR_RANGO_FECHA_RESOLUCION_TRAMITE_APLICADO("8");

    private String codigo;

    private EResolucionesTipoBusqueda(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
