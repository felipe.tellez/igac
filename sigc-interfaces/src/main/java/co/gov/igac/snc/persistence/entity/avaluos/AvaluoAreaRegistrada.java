package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the AVALUO_AREA_REGISTRADA database table.
 *
 */
@Entity
@Table(name = "AVALUO_AREA_REGISTRADA")
public class AvaluoAreaRegistrada implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String adoptada;
    private Double area;
    private Avaluo avaluo;
    private Date fechaLog;
    private String fuente;
    private String fuenteDescripcion;
    private String tipo;
    private String usuarioLog;

    private AvaluoPredioConstruccion avaluoPredioConstruccion;

    public AvaluoAreaRegistrada() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_AREA_REGISTRADA_ID_GENERATOR", sequenceName =
        "AVALUO_AREA_REGISTRADA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_AREA_REGISTRADA_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdoptada() {
        return this.adoptada;
    }

    public void setAdoptada(String adoptada) {
        this.adoptada = adoptada;
    }

    public Double getArea() {
        return this.area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    // bi-directional many-to-one association to Avaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getFuente() {
        return this.fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    @Column(name = "FUENTE_DESCRIPCION")
    public String getFuenteDescripcion() {
        return this.fuenteDescripcion;
    }

    public void setFuenteDescripcion(String fuenteDescripcion) {
        this.fuenteDescripcion = fuenteDescripcion;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_PREDIO_CONSTRUCCION_ID", nullable = false)
    public AvaluoPredioConstruccion getAvaluoPredioConstruccion() {
        return this.avaluoPredioConstruccion;
    }

    public void setAvaluoPredioConstruccion(AvaluoPredioConstruccion usuarioLog) {
        this.avaluoPredioConstruccion = usuarioLog;
    }

}
