package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 *
 * @author juan.agudelo
 *
 * @description	Número predial completo en partes
 * @version 2.0
 *
 */
public class NumeroPredialPartes implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1824599879451313781L;

    private String departamentoCodigo;
    private String municipioCodigo;
    private String tipoAvaluo;
    private String sectorCodigo;
    private String comunaCodigo;
    private String barrioCodigo;
    private String manzanaVeredaCodigo;
    private String predio;
    private String condicionPropiedad;
    private String edificio;
    private String piso;
    private String unidad;

    /**
     * Default constructor
     */
    public NumeroPredialPartes() {
        super();
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public String getTipoAvaluo() {
        return tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    public String getSectorCodigo() {
        return sectorCodigo;
    }

    public void setSectorCodigo(String sectorCodigo) {
        this.sectorCodigo = sectorCodigo;
    }

    public String getComunaCodigo() {
        return comunaCodigo;
    }

    public void setComunaCodigo(String comunaCodigo) {
        this.comunaCodigo = comunaCodigo;
    }

    public String getBarrioCodigo() {
        return barrioCodigo;
    }

    public void setBarrioCodigo(String barrioCodigo) {
        this.barrioCodigo = barrioCodigo;
    }

    public String getManzanaVeredaCodigo() {
        return manzanaVeredaCodigo;
    }

    public void setManzanaVeredaCodigo(String manzanaVeredaCodigo) {
        this.manzanaVeredaCodigo = manzanaVeredaCodigo;
    }

    public String getPredio() {
        return predio;
    }

    public void setPredio(String predio) {
        this.predio = predio;
    }

    public String getCondicionPropiedad() {
        return condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método encargado de ensamblar el número predial
     *
     * @author javier.aponte
     */
    public String assembleNumeroPredialFromSegments() {
        StringBuilder numeroPredialAssembled;
        numeroPredialAssembled = new StringBuilder();

        if (this.departamentoCodigo != null && !this.departamentoCodigo.trim().isEmpty() &&
            this.departamentoCodigo.length() == 2 &&
            this.municipioCodigo != null && !this.municipioCodigo.trim().isEmpty() &&
            this.municipioCodigo.length() == 3 &&
            this.tipoAvaluo != null && !this.tipoAvaluo.trim().isEmpty() && this.tipoAvaluo.
            length() == 2 &&
            this.sectorCodigo != null && !this.sectorCodigo.trim().isEmpty() && this.sectorCodigo.
            length() == 2 &&
            this.comunaCodigo != null && !this.comunaCodigo.trim().isEmpty() && this.comunaCodigo.
            length() == 2 &&
            this.barrioCodigo != null && !this.barrioCodigo.trim().isEmpty() && this.barrioCodigo.
            length() == 2 &&
            this.manzanaVeredaCodigo != null && !this.manzanaVeredaCodigo.trim().isEmpty() &&
            this.manzanaVeredaCodigo.length() == 4 &&
            this.predio != null && !this.predio.trim().isEmpty() && this.predio.length() == 4 &&
            this.condicionPropiedad != null && !this.condicionPropiedad.trim().isEmpty() &&
            this.condicionPropiedad.length() == 1 &&
            this.edificio != null && !this.edificio.trim().isEmpty() && this.edificio.length() == 2 &&
            this.piso != null && !this.piso.trim().isEmpty() && this.piso.length() == 2 &&
            this.unidad != null && !this.unidad.trim().isEmpty() && this.unidad.length() == 4) {

            numeroPredialAssembled.append(this.departamentoCodigo);
            numeroPredialAssembled.append(this.municipioCodigo);
            numeroPredialAssembled.append(this.tipoAvaluo);
            numeroPredialAssembled.append(this.sectorCodigo);
            numeroPredialAssembled.append(this.comunaCodigo);
            numeroPredialAssembled.append(this.barrioCodigo);
            numeroPredialAssembled.append(this.manzanaVeredaCodigo);
            numeroPredialAssembled.append(this.predio);
            numeroPredialAssembled.append(this.condicionPropiedad);
            numeroPredialAssembled.append(this.edificio);
            numeroPredialAssembled.append(this.piso);
            numeroPredialAssembled.append(this.unidad);

        }

        return numeroPredialAssembled.toString();
    }

}
