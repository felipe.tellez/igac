/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util.enumsOfertasInmob;

/**
 * Enumeración con los posibles valores del dominio de OFERTA_TIPO_OFERTA
 *
 * @author juan.agudelo
 */
public enum EOfertaTipoOferta {

    PROYECTOS("PROYECTOS"),
    USADOS("USADOS");

    private String codigo;

    private EOfertaTipoOferta(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
