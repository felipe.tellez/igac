/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los tipos de avalúo de un avalúo. Corresponde al dominio AVALUO_TIPO_AVALUO
 *
 * @author pedro.garcia
 */
public enum EAvaluoTipoAvaluo {

    RURAL("00", "Rural"),
    URBANO("01", "Urbano");

    private String codigo;
    private String valor;

    private EAvaluoTipoAvaluo(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
