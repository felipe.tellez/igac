/*
 * Proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 *
 * @author felipe.cadena
 */
public enum EValoresActualizacionTipo {

    CONSTRUCCION("CONSTRUCCION"),
    TERRENO("TERRENO");

    private String codigo;

    EValoresActualizacionTipo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
