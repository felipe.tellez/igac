package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.util.Constantes;

/**
 * The persistent class for the RECURSO_HUMANO database table.
 *
 * @author franz.gamba
 * @modified by: javier.aponte: adición de método toString(), hashCode(), equals()
 */
@Entity
@Table(name = "RECURSO_HUMANO")
public class RecursoHumano implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6257913423464500419L;
    private Long id;
    private String actividad;
    private String cargo;
    private String documentoIdentificacion;
    private Date fechaLog;
    private String nombre;
    private String tipo;
    private String usuarioLog;
    private List<Dotacion> dotacions;
    private ActualizacionResponsable actualizacionResponsable;
    private List<ActualizacionOrdenAjuste> actualizacionOrdenAjustes;

    private List<ComisionIntegrante> comisionIntegrantes;

    public RecursoHumano() {
    }

    @Id
    @SequenceGenerator(name = "RECURSO_HUMANO_ID_GENERATOR",
        sequenceName = "RECURSO_HUMANO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RECURSO_HUMANO_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActividad() {
        return this.actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getCargo() {
        return this.cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Column(name = "DOCUMENTO_IDENTIFICACION")
    public String getDocumentoIdentificacion() {
        return this.documentoIdentificacion;
    }

    public void setDocumentoIdentificacion(String documentoIdentificacion) {
        this.documentoIdentificacion = documentoIdentificacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Dotacion
    @OneToMany(mappedBy = "recursoHumano")
    public List<Dotacion> getDotacions() {
        return this.dotacions;
    }

    public void setDotacions(List<Dotacion> dotacions) {
        this.dotacions = dotacions;
    }

    //bi-directional many-to-one association to ActualizacionResponsable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_RESPONSABLE_ID", nullable = false)
    public ActualizacionResponsable getActualizacionResponsable() {
        return this.actualizacionResponsable;
    }

    public void setActualizacionResponsable(ActualizacionResponsable actualizacionResponsable) {
        this.actualizacionResponsable = actualizacionResponsable;
    }

    // ------------------------------------------//
    /**
     * @author javier.aponte
     * @return
     */
    @Override
    public String toString() {
        StringBuilder objectAsString;
        String stringSeparator = Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR;

        objectAsString = new StringBuilder();
        objectAsString.append(this.id).append(stringSeparator);
        if (this.actividad != null) {
            objectAsString.append(this.actividad);
        }
        objectAsString.append(stringSeparator);
        if (this.cargo != null) {
            objectAsString.append(this.cargo);
        }
        objectAsString.append(stringSeparator);
        objectAsString.append(this.documentoIdentificacion).append(stringSeparator);
        objectAsString.append(this.nombre).append(stringSeparator);
        objectAsString.append(this.tipo);

        return objectAsString.toString();
    }

    //bi-directional many-to-one association to ComisionIntegrante
    @OneToMany(mappedBy = "recursoHumano")
    public List<ComisionIntegrante> getComisionIntegrantes() {
        return this.comisionIntegrantes;
    }

    public void setComisionIntegrantes(List<ComisionIntegrante> comisionIntegrantes) {
        this.comisionIntegrantes = comisionIntegrantes;
    }

    /**
     * @author javier.aponte
     * @return
     */
    @Override
    public int hashCode() {
        return this.getClass().hashCode();
    }

    /**
     *
     * @return @author andres.eslava
     */
    // bi-directional many-to-one association to ActualizacionOrdenAjuste
    @OneToMany(mappedBy = "recursoHumano")
    public List<ActualizacionOrdenAjuste> getActualizacionOrdenAjustes() {
        return this.actualizacionOrdenAjustes;
    }

    public void setActualizacionOrdenAjustes(
        List<ActualizacionOrdenAjuste> actualizacionOrdenAjustes) {
        this.actualizacionOrdenAjustes = actualizacionOrdenAjustes;
    }

    /**
     * @author javier.aponte
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RecursoHumano other = (RecursoHumano) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

}
