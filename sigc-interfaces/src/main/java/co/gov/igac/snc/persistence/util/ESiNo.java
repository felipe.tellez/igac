package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de bd SINO
 *
 * @author ?
 */
public enum ESiNo {
    NO("NO"),
    SI("SI");

    private String codigo;

    private ESiNo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
