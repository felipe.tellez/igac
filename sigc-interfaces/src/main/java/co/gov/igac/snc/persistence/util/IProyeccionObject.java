package co.gov.igac.snc.persistence.util;

import java.util.Date;

/**
 * interfaz de los objetos de los que se hace una proyección en bd antes de hacerlos definitivos
 *
 * @author ???
 */
public interface IProyeccionObject {

    public Long getId();

    public void setId(Long id);

    public String getCancelaInscribe();

    public void setCancelaInscribe(String cancelaInscribe);

    public String getUsuarioLog();

    public void setUsuarioLog(String usuarioLog);

    public Date getFechaLog();

    public void setFechaLog(Date fechaLog);
}
