package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para estandarizar la zona a la que corresponde la documentacion
 *
 * @author franz.gamba
 *
 */
public enum EActualizacionDocumentacionZona {

    RURAL("RURAL"),
    URBANO("URBANA"),
    URBANO_RURAL("URBANA Y RURAL");

    private String descripcion;

    EActualizacionDocumentacionZona(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
