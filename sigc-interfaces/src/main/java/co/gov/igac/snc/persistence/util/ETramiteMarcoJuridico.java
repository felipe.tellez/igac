package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para los posibles valores de marco juridico de un tramite
 *
 * @author rodrigo.hernandez
 *
 */
public enum ETramiteMarcoJuridico {
    SIN_SELECCIONAR("Seleccione marco jurídico", "0"),
    DECRETO_222_1983_ART_109("Ocupación temporal por obra pública - Decreto 222/1983 art. 109", "1"),
    DECRETO_222_1983_ART_110(
        "Adquisición de inmuebles e Imposición de Servidumbre - Decreto 222/1983 art. 110", "2"),
    LEY_9_1983("Determinación del carácter de Vivienda de Interés Social Ley 9ª. De 1989", "3"),
    DECRETO_151_1998_ART_11(
        "Determinación del monto de la compensación en tratamiento de conservación. Decreto 151/1998;art.11",
        "4"),
    LEY_INTERVENCION_ECONOMICA_ART_60(
        "Avalúo de inmuebles en cumplimiento de la Ley 550/1999 (Ley de Intervención Económica) art.60",
        "5"),
    DECRETO_2056_1995(
        "Beneficio del subsidio en especie para habilitación legal de títulos - Decreto 2056/95",
        "6"),
    DECRETO_540_1998_ART_3("Avalúo de inmuebles en cumplimiento del Decreto 540/1998 art.3", "7"),
    LEY_546_1999_ART_50(
        "Avalúo de inmuebles en cumplimiento de la Ley 546 /1999 (Ley de vivienda) art. 50", "8"),
    CIRCULAR_60_2005(
        "Avalúo de inmuebles para efectos contables – Circular 60 de diciembre 19 de 2005", "9"),
    DECRETO_919_1989(
        "Avalúo para la prevención y atención de desastres en el marco del Decreto 919/1989", "10"),
    LEY_388_1997_ART_56("Enajenación forzosa en pública subasta - Ley  388/1997 art. 56", "11"),
    LEY_388_1997_ART_61("Enajenación Voluntaria - Ley 388/1997 art. 61", "12"),
    LEY_56_1981("Comisión Tripartita. – Ley 56/1981", "13"),
    LEY_388_1997_ART_75(
        "Valoración de inmuebles beneficiarios del efecto de plusvalía. Ley 388/1997 arts. 75 – 76 – 77",
        "14"),
    DECRETO_208_2004_ART_27(
        "Avalúo de inmuebles de particulares con fines privados – Decreto 208 de 2004 art. 27 numeral 2",
        "15"),
    DECRETO_2150_1995("Avalúo de Bienes Inmuebles - Decreto 2150/1995", "16"),
    LEY_1152_2007("Avalúo de inmuebles para efectos de la Ley 1152/2007", "17"),
    DECRETO_1420_1998("Avalúos en el marco del decreto 1420 de/1998", "18"),
    DECRETO_2474_2008("Enajenación voluntaria en el marco del Decreto 2474 / 2008", "19"),
    LEY_99_1993("Avalúo de inmuebles para efectos de la  Ley 99/1993", "20");

    private String valor;
    private String codigo;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    private ETramiteMarcoJuridico(String valor, String codigo) {
        this.valor = valor;
        this.codigo = codigo;
    }
}
