package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;

/**
 * TramiteRequisitoDocumento entity. @author MyEclipse Persistence Tools -> FGWL tipoDocumentoId
 * Long convertido a TipoDocumento
 */
@Entity
@Table(name = "TRAMITE_REQUISITO_DOCUMENTO", schema = "SNC_TRAMITE")
public class TramiteRequisitoDocumento implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private TramiteRequisito tramiteRequisito;
    private TipoDocumento tipoDocumento;
    private String zonaUnidadOrganica;
    private String clasificacion;
    private String requerido;
    private String usuarioLog;
    private String condicionPropiedad;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public TramiteRequisitoDocumento() {
    }

    /** minimal constructor */
    public TramiteRequisitoDocumento(Long id, TipoDocumento tipoDocumento,
        String zonaUnidadOrganica, String clasificacion, String requerido, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.tipoDocumento = tipoDocumento;
        this.zonaUnidadOrganica = zonaUnidadOrganica;
        this.clasificacion = clasificacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.requerido = requerido;
    }

    /** full constructor */
    public TramiteRequisitoDocumento(Long id,
        TramiteRequisito tramiteRequisito, TipoDocumento tipoDocumento,
        String zonaUnidadOrganica, String clasificacion, String requerido, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.tramiteRequisito = tramiteRequisito;
        this.tipoDocumento = tipoDocumento;
        this.zonaUnidadOrganica = zonaUnidadOrganica;
        this.clasificacion = clasificacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.requerido = requerido;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_REQUISITO_ID")
    public TramiteRequisito getTramiteRequisito() {
        return this.tramiteRequisito;
    }

    public void setTramiteRequisito(TramiteRequisito tramiteRequisito) {
        this.tramiteRequisito = tramiteRequisito;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_DOCUMENTO_ID", nullable = false)
    public TipoDocumento getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Column(name = "ZONA_UNIDAD_ORGANICA", nullable = false, length = 30)
    public String getZonaUnidadOrganica() {
        return this.zonaUnidadOrganica;
    }

    public void setZonaUnidadOrganica(String zonaUnidadOrganica) {
        this.zonaUnidadOrganica = zonaUnidadOrganica;
    }

    @Column(name = "CLASIFICACION", nullable = false, length = 30)
    public String getClasificacion() {
        return this.clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "REQUERIDO", nullable = false, length = 2)
    public String getRequerido() {
        return requerido;
    }

    public void setRequerido(String requerido) {
        this.requerido = requerido;
    }

    @Column(name = "CONDICION_PROPIEDAD")
    public String getCondicionPropiedad() {
        return condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

}
