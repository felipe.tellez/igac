package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the P_REFERENCIA_CARTOGRAFICA database table.
 *
 * @modified :: david.cifuentes :: Se implementa la interfaz Cloneable y se agrega método clone ::
 * 22/08/12
 */
@Entity
@Table(name = "P_REFERENCIA_CARTOGRAFICA", schema = "SNC_CONSERVACION")
public class PReferenciaCartografica implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private String foto;
    private String plancha;
    private PPredio PPredio;
    private String usuarioLog;
    private String vuelo;
    private String cancelaInscribe;

    public PReferenciaCartografica() {
    }

    @Id
    @Column(unique = true, nullable = false, precision = 10)
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REFERENCIA_CARTOGRAFICA_ID_SEQ")
    @SequenceGenerator(name = "REFERENCIA_CARTOGRAFICA_ID_SEQ", sequenceName =
        "REFERENCIA_CARTOGRAFICA_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(length = 20)
    public String getFoto() {
        return this.foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Column(length = 20)
    public String getPlancha() {
        return this.plancha;
    }

    public void setPlancha(String plancha) {
        this.plancha = plancha;
    }

    // bi-directional many-to-one association to PPredio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(length = 20)
    public String getVuelo() {
        return this.vuelo;
    }

    public void setVuelo(String vuelo) {
        this.vuelo = vuelo;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    /**
     * @author david.cifuentes Método que realiza un clone del objeto pReferenciaCartografica.
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

}
