package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Zona;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the AREA_CAPTURA_OFERTA database table.
 *
 * @modified pedro.garcia 20-04-2012 cambio de tipo de dato del id: de long a Long
 * @modified rodrigo.hernandez 18-05-2012 cambio de tipos de dato para departamento, municipio y
 * zona
 */
@Entity
@Table(name = "AREA_CAPTURA_OFERTA")
public class AreaCapturaOferta implements Serializable {

    private static final long serialVersionUID = 1545645764576456L;

    private Long id;
    private Departamento departamento;
    private String estado;
    private String estructuraOrganizacionalCod;
    private Date fecha;
    private Date fechaLog;
    private Municipio municipio;
    private String observaciones;
    private String procesoInstanciaId;
    private String usuarioLog;
    private Zona zona;
    private List<DetalleCapturaOferta> detalleCapturaOfertas;
    private List<RegionCapturaOferta> regionCapturaOfertas;

    public AreaCapturaOferta() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "area_captura_oferta_ID_SEQ")
    @SequenceGenerator(name = "area_captura_oferta_ID_SEQ",
        sequenceName = "AREA_CAPTURA_OFERTA_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD")
    public String getEstructuraOrganizacionalCod() {
        return this.estructuraOrganizacionalCod;
    }

    public void setEstructuraOrganizacionalCod(String estructuraOrganizacionalCod) {
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ZONA_CODIGO", nullable = false)
    public Zona getZona() {
        return this.zona;
    }

    public void setZona(Zona zona) {
        this.zona = zona;
    }

    //bi-directional many-to-one association to DetalleCapturaOferta
    @OneToMany(mappedBy = "areaCapturaOferta")
    public List<DetalleCapturaOferta> getDetalleCapturaOfertas() {
        return this.detalleCapturaOfertas;
    }

    public void setDetalleCapturaOfertas(List<DetalleCapturaOferta> detalleCapturaOfertas) {
        this.detalleCapturaOfertas = detalleCapturaOfertas;
    }

    //bi-directional many-to-one association to RegionCapturaOferta
    @OneToMany(mappedBy = "areaCapturaOferta")
    public List<RegionCapturaOferta> getRegionCapturaOfertas() {
        return this.regionCapturaOfertas;
    }

    public void setRegionCapturaOfertas(List<RegionCapturaOferta> regionCapturaOfertas) {
        this.regionCapturaOfertas = regionCapturaOfertas;
    }

    @Column(name = "PROCESO_INSTANCIA_ID")
    public String getProcesoInstanciaId() {
        return this.procesoInstanciaId;
    }

    public void setProcesoInstanciaId(String procesoInstanciaId) {
        this.procesoInstanciaId = procesoInstanciaId;
    }

    @Transient
    public String getCadenaDeManzanasVeredas() {
        String cadenaManzanasVeredas = "";

        if (this.detalleCapturaOfertas != null && !this.detalleCapturaOfertas.isEmpty()) {

            for (DetalleCapturaOferta detalle : this.detalleCapturaOfertas) {

                if (!cadenaManzanasVeredas.isEmpty()) {
                    cadenaManzanasVeredas = cadenaManzanasVeredas.concat("," +
                        detalle.getManzanaVeredaCodigo());
                } else {
                    cadenaManzanasVeredas = detalle.getManzanaVeredaCodigo();
                }
            }
        }
        return cadenaManzanasVeredas;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AreaCapturaOferta other = (AreaCapturaOferta) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
