/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los codigos de ubicacion para locales
 *
 * @author christian.rodriguez
 */
public enum EOfertaUbicacionLocal {

    EXTERIOR("Exterior"),
    INTERIOR("Interior");

    private String codigo;

    private EOfertaUbicacionLocal(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
