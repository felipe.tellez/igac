package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the COMISION_INTEGRANTE database table.
 *
 */
@Entity
@Table(name = "COMISION_INTEGRANTE")
public class ComisionIntegrante implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1331630080871807881L;
    private Long id;
    private String estado;
    private Date fechaLog;
    private String usuarioLog;
    private ActualizacionComision actualizacionComision;
    private RecursoHumano recursoHumano;

    public ComisionIntegrante() {
    }

    @Id
    @SequenceGenerator(name = "COMISION_INTEGRANTE_ID_GENERATOR",
        sequenceName = "COMISION_INTEGRANTE_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "COMISION_INTEGRANTE_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ActualizacionComision
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_COMISION_ID", nullable = false)
    public ActualizacionComision getActualizacionComision() {
        return this.actualizacionComision;
    }

    public void setActualizacionComision(ActualizacionComision actualizacionComision) {
        this.actualizacionComision = actualizacionComision;
    }

    //bi-directional many-to-one association to RecursoHumano
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECURSO_HUMANO_ID", nullable = false)
    public RecursoHumano getRecursoHumano() {
        return this.recursoHumano;
    }

    public void setRecursoHumano(RecursoHumano recursoHumano) {
        this.recursoHumano = recursoHumano;
    }

}
