/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * Enumeración para el dominio MUTACION_CLASE
 *
 * @author pedro.garcia
 * @modified juan.agudelo adición del atributo nombre
 */
public enum EMutacionClase {

    //valor - código
    CUARTA("4", "Cuarta"),
    PRIMERA("1", "Primera"),
    QUINTA("5", "Quinta"),
    SEGUNDA("2", "Segunda"),
    TERCERA("3", "Tercera");

    private String codigo;
    private String nombre;

    private EMutacionClase(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getNombre() {
        return nombre;
    }

    /**
     * Devuelve el nombre de la enumeracion a partir de su codigo.
     *
     * @param codigoClaseMutacion
     * @return
     * @author andres.eslava
     */
    public static String getNombreByCodigo(String codigoClaseMutacion) {

        for (EMutacionClase valorEnumeracion : EMutacionClase.values()) {
            if (valorEnumeracion.getCodigo().equals(codigoClaseMutacion)) {
                return "Mutación " + valorEnumeracion.getNombre();
            }
        }
        return " ";
    }

}
