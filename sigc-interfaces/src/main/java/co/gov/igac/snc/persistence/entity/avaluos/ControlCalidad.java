package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CONTROL_CALIDAD database table.
 *
 * @author rodrigo.hernandez
 */
@Entity
@Table(name = "CONTROL_CALIDAD")
public class ControlCalidad implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String aprobado;
    private Double areaConstruida;
    private Double areaTerreno;
    private String departamentoCodigo;
    private String estado;
    private Date fecha;
    private Date fechaLog;
    private String municipioCodigo;
    private String procesoInstanciaId;
    private Double totalOfertasMuestra;
    private Double totalOfertasRecolectadas;
    private String usuarioLog;
    private Double valorCalculado;
    private Double valorPedido;
    private List<ControlCalidadOferta> controlCalidadOfertas;
    private List<ControlCalidadRecolector> controlCalidadRecolectors;

    public ControlCalidad() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTROL_CALIDAD_ID_SEQ")
    @SequenceGenerator(name = "CONTROL_CALIDAD_ID_SEQ", sequenceName = "CONTROL_CALIDAD_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "APROBADO")
    public String getAprobado() {
        return this.aprobado;
    }

    public void setAprobado(String aprobado) {
        this.aprobado = aprobado;
    }

    @Column(name = "AREA_CONSTRUIDA")
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "AREA_TERRENO")
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "DEPARTAMENTO_CODIGO")
    public String getDepartamentoCodigo() {
        return this.departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    @Column(name = "ESTADO")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "MUNICIPIO_CODIGO")
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    @Column(name = "PROCESO_INSTANCIA_ID")
    public String getProcesoInstanciaId() {
        return this.procesoInstanciaId;
    }

    public void setProcesoInstanciaId(String procesoInstanciaId) {
        this.procesoInstanciaId = procesoInstanciaId;
    }

    @Column(name = "TOTAL_OFERTAS_MUESTRA")
    public Double getTotalOfertasMuestra() {
        return this.totalOfertasMuestra;
    }

    public void setTotalOfertasMuestra(Double totalOfertasMuestra) {
        this.totalOfertasMuestra = totalOfertasMuestra;
    }

    @Column(name = "TOTAL_OFERTAS_RECOLECTADAS")
    public Double getTotalOfertasRecolectadas() {
        return this.totalOfertasRecolectadas;
    }

    public void setTotalOfertasRecolectadas(Double totalOfertasRecolectadas) {
        this.totalOfertasRecolectadas = totalOfertasRecolectadas;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_CALCULADO")
    public Double getValorCalculado() {
        return this.valorCalculado;
    }

    public void setValorCalculado(Double valorCalculado) {
        this.valorCalculado = valorCalculado;
    }

    @Column(name = "VALOR_PEDIDO")
    public Double getValorPedido() {
        return this.valorPedido;
    }

    public void setValorPedido(Double valorPedido) {
        this.valorPedido = valorPedido;
    }

    //bi-directional many-to-one association to ControlCalidadOferta
    @OneToMany(mappedBy = "controlCalidad")
    public List<ControlCalidadOferta> getControlCalidadOfertas() {
        return this.controlCalidadOfertas;
    }

    public void setControlCalidadOfertas(List<ControlCalidadOferta> controlCalidadOfertas) {
        this.controlCalidadOfertas = controlCalidadOfertas;
    }

    //bi-directional many-to-one association to ControlCalidadRecolector
    @OneToMany(mappedBy = "controlCalidad")
    public List<ControlCalidadRecolector> getControlCalidadRecolectors() {
        return this.controlCalidadRecolectors;
    }

    public void setControlCalidadRecolectors(
        List<ControlCalidadRecolector> controlCalidadRecolectors) {
        this.controlCalidadRecolectors = controlCalidadRecolectors;
    }

}
