package co.gov.igac.snc.persistence.util;

/**
 * Enumeración que representa los datos contenidos en el Dominio SOLICITUD_ORIGEN
 *
 * Datos de posibles tipos de solicitud segun el tipo de solicitante PRIVADA o tipo de solicitante
 * PUBLICA o tipo de solicitante MIXTA
 *
 *
 * @author rodrigo.hernandez
 *
 */
public enum ESolicitudOrigenPriPubMix {

    AVALUO_NORMAL("AN", "Avaluo normal");

    private String codigo;
    private String valor;

    private ESolicitudOrigenPriPubMix(String codigo, String valor) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
