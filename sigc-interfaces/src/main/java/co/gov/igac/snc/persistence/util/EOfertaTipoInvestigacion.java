/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio de OFERTA_TIPO_INVESTIGACION
 *
 * @author christian.rodriguez
 */
public enum EOfertaTipoInvestigacion {

    ARRIENDO("Arriendo"),
    VENTA("Venta");

    private String codigo;

    private EOfertaTipoInvestigacion(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }
}
