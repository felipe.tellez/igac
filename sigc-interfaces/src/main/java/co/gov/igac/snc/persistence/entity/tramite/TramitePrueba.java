package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * TramitePrueba entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRAMITE_PRUEBA", schema = "SNC_TRAMITE")
public class TramitePrueba implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 821974517114467423L;

    // Fields
    private Long id;
    private Tramite tramite;
    private String aprobadas;
    private Date fechaInicio;
    private Date fechaFin;
    private String motivoRechazo;
    private String pruebasSolicitadas;
    private String consideraciones;
    private String dispone;
    private String usuarioLog;
    private Date fechaLog;
    private Documento autoPruebasDocumento;
    private Documento comunicacionAfecDocumento;
    private List<TramitePruebaEntidad> tramitePruebaEntidads;

    // Constructors
    /** default constructor */
    public TramitePrueba() {
    }

    /** minimal constructor */
    public TramitePrueba(Long id, Tramite tramite, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramitePrueba(Long id, Tramite tramite, String aprobadas,
        Date fechaInicio, Date fechaFin, String motivoRechazo,
        String pruebasSolicitadas, String consideraciones, String dispone,
        String usuarioLog, Date fechaLog, Documento autoPruebasDocumento,
        Documento comunicacionAfecDocumento,
        List<TramitePruebaEntidad> tramitePruebaEntidads) {
        super();
        this.id = id;
        this.tramite = tramite;
        this.aprobadas = aprobadas;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.motivoRechazo = motivoRechazo;
        this.pruebasSolicitadas = pruebasSolicitadas;
        this.consideraciones = consideraciones;
        this.dispone = dispone;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.autoPruebasDocumento = autoPruebasDocumento;
        this.comunicacionAfecDocumento = comunicacionAfecDocumento;
        this.tramitePruebaEntidads = tramitePruebaEntidads;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_PRUEBA_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_PRUEBA_ID_SEQ", sequenceName = "TRAMITE_PRUEBA_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", unique = true, nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "APROBADAS", length = 2)
    public String getAprobadas() {
        return this.aprobadas;
    }

    public void setAprobadas(String aprobadas) {
        this.aprobadas = aprobadas;
    }

    @Column(name = "FECHA_INICIO", length = 7)
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Column(name = "FECHA_FIN", length = 7)
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Column(name = "MOTIVO_RECHAZO", length = 600)
    public String getMotivoRechazo() {
        return this.motivoRechazo;
    }

    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    @Column(name = "PRUEBAS_SOLICITADAS")
    public String getPruebasSolicitadas() {
        return this.pruebasSolicitadas;
    }

    public void setPruebasSolicitadas(String pruebasSolicitadas) {
        this.pruebasSolicitadas = pruebasSolicitadas;
    }

    @Column(name = "CONSIDERACIONES")
    public String getConsideraciones() {
        return this.consideraciones;
    }

    public void setConsideraciones(String consideraciones) {
        this.consideraciones = consideraciones;
    }

    @Column(name = "DISPONE")
    public String getDispone() {
        return this.dispone;
    }

    public void setDispone(String dispone) {
        this.dispone = dispone;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "AUTO_PRUEBAS_DOCUMENTO_ID", nullable = true)
    public Documento getAutoPruebasDocumento() {
        return this.autoPruebasDocumento;
    }

    public void setAutoPruebasDocumento(Documento autoPruebasDocumento) {
        this.autoPruebasDocumento = autoPruebasDocumento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMUNICACION_AFEC_DOCUMENTO_ID", nullable = true)
    public Documento getComunicacionAfecDocumento() {
        return comunicacionAfecDocumento;
    }

    public void setComunicacionAfecDocumento(Documento comunicacionAfecDocumento) {
        this.comunicacionAfecDocumento = comunicacionAfecDocumento;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramitePrueba")
    public List<TramitePruebaEntidad> getTramitePruebaEntidads() {
        return tramitePruebaEntidads;
    }

    public void setTramitePruebaEntidads(
        List<TramitePruebaEntidad> tramitePruebaEntidads) {
        this.tramitePruebaEntidads = tramitePruebaEntidads;
    }

}
