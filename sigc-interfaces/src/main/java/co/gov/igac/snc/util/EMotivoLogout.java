/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.util;

/**
 *
 * @author felipe.cadena
 */
public enum EMotivoLogout {

    CIERRE_SESION("Cierre de sesión"),
    SESION_EXPIRADA("La sesión expiro por tiempo");

    private String valor;

    private EMotivoLogout(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

}
