package co.gov.igac.snc.persistence.util;

public enum ESolicitanteTipoSolicitant {
    TERRITORIAL_AREA_DE_CONSERVACION("TERRITORIAL AREA CONSERV"),
    GRUPO_INTERNO_DE_TRABAJO("GRUPO INTERNO DE TRABAJO"),
    ENTIDAD_EXTERNA("ENTIDAD EXTERNA"),
    FISCALIA("FISCALIA"),
    JUZGADO("JUZGADO"),
    MIXTA("MIXTA"),
    PRIVADA("PRIVADA"),
    PROCURADURIA("PROCURADURIA"),
    PUBLICA("PUBLICA"),
    TESORERIA("TESORERIA");

    private String codigo;

    ESolicitanteTipoSolicitant(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
