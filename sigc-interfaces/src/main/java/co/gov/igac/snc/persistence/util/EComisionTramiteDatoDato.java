/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * enumeración con los códigos y valores del dominio COMISION_TRAMITE_DATO_DATO
 *
 * @author pedro.garcia
 */
public enum EComisionTramiteDatoDato {

    //N:
    //  Note that each enum type has a static values method that returns an array containing all
    //  of the values of the enum type in the order they are declared.
    AREA_CONSTRUIDA("5", "Área construida"),
    AREA_DE_TERRENO("2", "Área de terreno"),
    CONSTRUCCIONES_NUEVAS("9", "Construcciones nuevas"),
    DESTINOS_ECONOMICOS("8", "Destinos económicos"),
    LOCALIZACION_PREDIO("1", "Localización del predio"),
    OTROS("10", "Otros"),
    PUNTAJE_DE_CONSTRUCCION("6", "Puntaje de construcción"),
    USOS_DE_LA_CONSTRUCCION("7", "Usos de la construcción"),
    ZONAS_HOMOGENEAS_FISICAS("4", "Zonas homogéneas físicas"),
    ZONAS_HOMOGENEAS_GEOECONOMICAS("3", "Zonas homogéneas geoeconómicas");

    private String codigo;
    private String valor;

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
     * constructor
     *
     * @param codigo
     * @param valor
     */
    private EComisionTramiteDatoDato(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

}
