package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * The persistent class for the ACTUALIZACION_COMISION database table.
 *
 */
@Entity
@Table(name = "ACTUALIZACION_COMISION")
public class ActualizacionComision implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2219934966617987967L;
    private Long id;
    private Double duracion;
    private Date fechaFin;
    private Date fechaInicio;
    private Date fechaLog;
    private String objeto;
    private Documento soporteDocumentoId;
    private String usuarioLog;
    private Actualizacion actualizacion;
    private List<ComisionIntegrante> comisionIntegrantes;

    public ActualizacionComision() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACTUALIZACION_COMISION_ID_SEQ")
    @SequenceGenerator(name = "ACTUALIZACION_COMISION_ID_SEQ",
        sequenceName = "ACTUALIZACION_COMISION_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDuracion() {
        return this.duracion;
    }

    public void setDuracion(Double duracion) {
        this.duracion = duracion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getObjeto() {
        return this.objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = true)
    public Documento getSoporteDocumentoId() {
        return this.soporteDocumentoId;
    }

    public void setSoporteDocumentoId(Documento soporteDocumentoId) {
        this.soporteDocumentoId = soporteDocumentoId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    //bi-directional many-to-one association to ComisionIntegrante
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actualizacionComision")
    public List<ComisionIntegrante> getComisionIntegrantes() {
        return this.comisionIntegrantes;
    }

    public void setComisionIntegrantes(List<ComisionIntegrante> comisionIntegrantes) {
        this.comisionIntegrantes = comisionIntegrantes;
    }

}
