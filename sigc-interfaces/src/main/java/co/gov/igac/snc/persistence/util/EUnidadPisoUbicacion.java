package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los valores del dominio UNIDAD_PISO_UBICACION usado para asignar el piso de
 * ubicación a una unidad de construcción.
 *
 * @author david.cifuentes
 */
public enum EUnidadPisoUbicacion {

    //nombre local (codigo, valor)
    MEZANINE_1("MZ-01", "Mezanine 1"),
    MEZANINE_2("MZ-02", "Mezanine 2"),
    MEZANINE_3("MZ-03", "Mezanine 3"),
    PISO_1("PS-01", "Piso 1"),
    PISO_2("PS-02", "Piso 2"),
    PISO_3("PS-03", "Piso 3"),
    PISO_4("PS-04", "Piso 4"),
    PISO_5("PS-05", "Piso 5"),
    PISO_6("PS-06", "Piso 6"),
    PISO_7("PS-07", "Piso 7"),
    PISO_8("PS-08", "Piso 8"),
    PISO_9("PS-09", "Piso 9"),
    PISO_10("PS-10", "Piso 10"),
    PISO_11("PS-11", "Piso 11"),
    PISO_12("PS-12", "Piso 12"),
    PISO_13("PS-13", "Piso 13"),
    PISO_14("PS-14", "Piso 14"),
    PISO_15("PS-15", "Piso 15"),
    PISO_16("PS-16", "Piso 16"),
    PISO_17("PS-17", "Piso 17"),
    PISO_18("PS-18", "Piso 18"),
    PISO_19("PS-19", "Piso 19"),
    PISO_20("PS-20", "Piso 20"),
    PISO_21("PS-21", "Piso 21"),
    PISO_22("PS-22", "Piso 22"),
    PISO_23("PS-23", "Piso 23"),
    PISO_24("PS-24", "Piso 24"),
    PISO_25("PS-25", "Piso 25"),
    PISO_26("PS-26", "Piso 26"),
    PISO_27("PS-27", "Piso 27"),
    PISO_28("PS-28", "Piso 28"),
    PISO_29("PS-29", "Piso 29"),
    PISO_30("PS-30", "Piso 30"),
    PISO_31("PS-31", "Piso 31"),
    PISO_32("PS-32", "Piso 32"),
    PISO_33("PS-33", "Piso 33"),
    PISO_34("PS-34", "Piso 34"),
    PISO_35("PS-35", "Piso 35"),
    PISO_36("PS-36", "Piso 36"),
    PISO_37("PS-37", "Piso 37"),
    PISO_38("PS-38", "Piso 38"),
    PISO_39("PS-39", "Piso 39"),
    PISO_40("PS-40", "Piso 40"),
    PISO_41("PS-41", "Piso 41"),
    PISO_42("PS-42", "Piso 42"),
    PISO_43("PS-43", "Piso 43"),
    PISO_44("PS-44", "Piso 44"),
    PISO_45("PS-45", "Piso 45"),
    PISO_46("PS-46", "Piso 46"),
    PISO_47("PS-47", "Piso 47"),
    PISO_48("PS-48", "Piso 48"),
    PISO_49("PS-49", "Piso 49"),
    PISO_50("PS-50", "Piso 50"),
    PISO_51("PS-51", "Piso 51"),
    PISO_52("PS-52", "Piso 52"),
    PISO_53("PS-53", "Piso 53"),
    PISO_54("PS-54", "Piso 54"),
    PISO_55("PS-55", "Piso 55"),
    PISO_56("PS-56", "Piso 56"),
    PISO_57("PS-57", "Piso 57"),
    PISO_58("PS-58", "Piso 58"),
    PISO_59("PS-59", "Piso 59"),
    PISO_60("PS-60", "Piso 60"),
    PISO_61("PS-61", "Piso 61"),
    PISO_62("PS-62", "Piso 62"),
    PISO_63("PS-63", "Piso 63"),
    PISO_64("PS-64", "Piso 64"),
    PISO_65("PS-65", "Piso 65"),
    PISO_66("PS-66", "Piso 66"),
    PISO_67("PS-67", "Piso 67"),
    PISO_68("PS-68", "Piso 68"),
    PISO_69("PS-69", "Piso 69"),
    PISO_70("PS-70", "Piso 70"),
    SUBTERRANEO_1("SB-01", "Subterráneo 1"),
    SUBTERRANEO_2("SB-02", "Subterráneo 2"),
    SUBTERRANEO_3("SB-03", "Subterráneo 3"),
    SUBTERRANEO_4("SB-04", "Subterráneo 4"),
    SUBTERRANEO_5("SB-05", "Subterráneo 5"),
    SUBTERRANEO_6("SB-06", "Subterráneo 6"),
    SUBTERRANEO_7("SB-07", "Subterráneo 7"),
    SUBTERRANEO_8("SB-08", "Subterráneo 8"),
    SUBTERRANEO_9("SB-09", "Subterráneo 9"),
    SEMISOTANO_1("SS-01", "Semisótano 1"),
    SEMISOTANO_2("SS-02", "Semisótano 2"),
    SEMISOTANO_3("SS-03", "Semisótano 3"),
    SOTANO_1("ST-01", "Sótano 1"),
    SOTANO_2("ST-02", "Sótano 2"),
    SOTANO_3("ST-03", "Sótano 3"),
    SOTANO_4("ST-04", "Sótano 4"),
    SOTANO_5("ST-05", "Sótano 5"),
    SOTANO_6("ST-06", "Sótano 6"),
    SOTANO_7("ST-07", "Sótano 7"),
    SOTANO_8("ST-08", "Sótano 8"),
    SOTANO_9("ST-09", "Sótano 9");

    private String codigo;
    private String nombre;

    private EUnidadPisoUbicacion(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getNombre() {
        return this.nombre;
    }

}
