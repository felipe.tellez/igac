package co.gov.igac.snc.util;

/**
 * Enumeración para parámetros usados en la selección de manzanas para asignación y desasignación a
 * un recolector
 *
 * @author rodrigo.hernandez
 *
 */
public enum EVisorGISParametrosAsignacion {

    ASIGNAR_MANZANAS_A_RECOLECTOR_OFERTAS("seleccionarManzanasPorAsignarOfertas"),
    DESASIGNAR_MANZANAS_A_RECOLECTOR_OFERTAS("seleccionarManzanasEnEjecucionOfertas"),
    SELECCIONAR_PREDIOS_ACTUALIZACION("seleccionarPrediosActualizacion"),
    SELECCIONAR_PREDIOS_ACTUALIZACION_CU208("seleccionarManzanasActualizacionCU208Evt");

    private String codigo;

    EVisorGISParametrosAsignacion(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
