package co.gov.igac.snc.persistence.entity.conservacion;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @modified by: pedro.garcia what: - adición de named queries - cambio de tipo de datos de Short a
 * Integer para los atributos puntosDesde, puntosHasta
 *
 */
@Entity
@NamedQueries({@NamedQuery(
        name = "findByPoints",
        query =
        "from TipificacionUnidadConst tuc where tuc.puntosDesde <= :puntosP and :puntosP <= tuc.puntosHasta")})
@Table(name = "TIPIFICACION_UNIDAD_CONST", schema = "SNC_CONSERVACION", uniqueConstraints =
    @UniqueConstraint(columnNames = "TIPO"))
public class TipificacionUnidadConst implements java.io.Serializable {

    // Fields
    private Long id;
    private String tipo;
    private String tipificacion;
    private Integer puntosDesde;
    private Integer puntosHasta;
    private String usuarioLog;
    private Timestamp fechaLog;

    // Constructors
    /** default constructor */
    public TipificacionUnidadConst() {
    }

    /** full constructor */
    public TipificacionUnidadConst(Long id, String tipo, String tipificacion,
        Integer puntosDesde, Integer puntosHasta, String usuarioLog,
        Timestamp fechaLog) {
        this.id = id;
        this.tipo = tipo;
        this.tipificacion = tipificacion;
        this.puntosDesde = puntosDesde;
        this.puntosHasta = puntosHasta;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO", unique = true, nullable = false, length = 2)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "TIPIFICACION", nullable = false, length = 30)
    public String getTipificacion() {
        return this.tipificacion;
    }

    public void setTipificacion(String tipificacion) {
        this.tipificacion = tipificacion;
    }

    @Column(name = "PUNTOS_DESDE", nullable = false, precision = 4, scale = 0)
    public Integer getPuntosDesde() {
        return this.puntosDesde;
    }

    public void setPuntosDesde(Integer puntosDesde) {
        this.puntosDesde = puntosDesde;
    }

    @Column(name = "PUNTOS_HASTA", nullable = false, precision = 4, scale = 0)
    public Integer getPuntosHasta() {
        return this.puntosHasta;
    }

    public void setPuntosHasta(Integer puntosHasta) {
        this.puntosHasta = puntosHasta;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

}
