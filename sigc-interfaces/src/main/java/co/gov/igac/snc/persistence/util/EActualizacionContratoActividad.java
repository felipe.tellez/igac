package co.gov.igac.snc.persistence.util;

/**
 *
 * Enumeración para el dominio ACTUALIZACION_CONTRATO_ACT
 *
 * @author javier.aponte
 *
 */
public enum EActualizacionContratoActividad {

    ABOGADO("ABOGADO", "Abogado"),
    AUXILIAR_DE_CAMPO("AUXILIAR DE CAMPO", "Auxiliar de campo"),
    AUXILIAR_OFICINA("AUXILIAR OFICINA", "Auxiliar Oficina"),
    CONDUCTOR("CONDUCTOR", "Conductor"),
    COORDINADORES("COORDINADORES", "Coordinadores"),
    DIGITADOR("DIGITADOR", "Digitador"),
    DIGITALIZADOR("DIGITALIZADOR", "Digitalizador"),
    GERENTE_DE_PROYECTO("GERENTE DE PROYECTO", "Gerente de proyecto"),
    PROFESIONAL_DE_DIGITALIZACION("PROFESIONAL DE DIGITALIZACIÓN", "Profesional de digitalización"),
    PROFESIONAL_DE_RECONOCIMIENTO("PROFESIONAL DE RECONOCIMIENTO", "Profesional de reconocimiento"),
    PROFESIONAL_DE_SISTEMAS("PROFESIONAL DE SISTEMAS", "Profesional de sistemas"),
    PROFESIONAL_DE_ZONAS("PROFESIONAL DE ZONAS", "Profesional de zonas"),
    RECONOCEDORES("RECONOCEDORES", "Reconocedores"),
    RESPONSABLE_SIG("RESPONSABLE SIG", "Responsable Sig"),
    TOPOGRAFO("TOPÓGRAFO", "Topógrafo");

    private String codigo;
    private String valor;

    private EActualizacionContratoActividad(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
