package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * Clase DTO que contiene datos de ejecutores de un trámite, en particular contiene el nombre de
 * funcionario, su id y la cantidad de trámites de terreno y de oficina que tiene asignados
 *
 * @modified by javier.aponte se documentó la clase
 *
 */
public class SEjecutoresTramite implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -237836781561692187L;
    private String funcionario;
    private Long terreno;
    private Long oficina;
    private String funcionarioId;

    public SEjecutoresTramite() {

    }

    public SEjecutoresTramite(String funcionario, Long terreno, Long oficina,
        String funcionarioId) {
        this.funcionario = funcionario;
        this.terreno = terreno;
        this.oficina = oficina;
        this.funcionarioId = funcionarioId;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public Long getTerreno() {
        return terreno;
    }

    public void setTerreno(Long terreno) {
        this.terreno = terreno;
    }

    public Long getOficina() {
        return oficina;
    }

    public void setOficina(Long oficina) {
        this.oficina = oficina;
    }

    public String getFuncionarioId() {
        return funcionarioId;
    }

    public void setFuncionarioId(String funcionarioId) {
        this.funcionarioId = funcionarioId;
    }

}
