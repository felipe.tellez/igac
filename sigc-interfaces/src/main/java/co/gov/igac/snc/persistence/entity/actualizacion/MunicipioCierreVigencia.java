package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.util.EEstadoCierreMunicipio;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.EZonaCierreVigencia;
import co.gov.igac.snc.util.Constantes;

/**
 * The persistent class for the MUNICIPIO_CIERRE_VIGENCIA database table.
 */
/**
 * Modificaciones a la clase:
 *
 * @modified by: david.cifuentes :: Adición de los métodos y variables transient estadoFormacion,
 * estadoActualizacion, y los métodos transient getZonasEnActualizacion y getZonasEnFormación ::
 * 14/05/2013
 * @modified by: david.cifuentes :: Adición del método y variable transient procesoCerrado e
 * isProcesoCerrado :: 24/05/2013
 */
@Entity
@Table(name = "MUNICIPIO_CIERRE_VIGENCIA", schema = "SNC_CONSERVACION")
public class MunicipioCierreVigencia implements Serializable, Comparable<MunicipioCierreVigencia> {

    private static final long serialVersionUID = -2498725939600349653L;

    private Long id;
    private String actualizacionTotal;
    private DecretoAvaluoAnual decretoAvaluoAnual;
    private Date fechaFinActualizacion;
    private Date fechaFinCierre;
    private Date fechaFinFormacion;
    private Date fechaInicioActualizacion;
    private Date fechaInicioCierre;
    private Date fechaInicioFormacion;
    private Date fechaLog;
    private String formacionTotal;
    private String municipioCodigo;
    private Long resolucionActualizacionId;
    private Long resolucionFormacionId;
    private String usuarioLog;
    private Date vigencia;
    private List<ZonaCierreVigencia> zonaCierreVigencias;
    private String estadoCierreActualizacion;
    private String estadoCierreConservacion;

    // Atributos transient
    private boolean estadoFormacion;
    private boolean estadoActualizacion;
    private boolean procesoCerrado;
    private boolean selected;

    public MunicipioCierreVigencia() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MUNICIPIO_CIERRE_VIGENC_ID_SEQ")
    @SequenceGenerator(name = "MUNICIPIO_CIERRE_VIGENC_ID_SEQ", sequenceName =
        "MUNICIPIO_CIERRE_VIGENC_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACTUALIZACION_TOTAL", nullable = false, length = 2)
    public String getActualizacionTotal() {
        return this.actualizacionTotal;
    }

    public void setActualizacionTotal(String actualizacionTotal) {
        this.actualizacionTotal = actualizacionTotal;
    }

    // bi-directional many-to-one association to DecretoAvaluoAnual
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DECRETO_AVALUO_ANUAL_ID", nullable = false)
    public DecretoAvaluoAnual getDecretoAvaluoAnual() {
        return this.decretoAvaluoAnual;
    }

    public void setDecretoAvaluoAnual(DecretoAvaluoAnual decretoAvaluoAnual) {
        this.decretoAvaluoAnual = decretoAvaluoAnual;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_ACTUALIZACION")
    public Date getFechaFinActualizacion() {
        return this.fechaFinActualizacion;
    }

    public void setFechaFinActualizacion(Date fechaFinActualizacion) {
        this.fechaFinActualizacion = fechaFinActualizacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_CIERRE")
    public Date getFechaFinCierre() {
        return this.fechaFinCierre;
    }

    public void setFechaFinCierre(Date fechaFinCierre) {
        this.fechaFinCierre = fechaFinCierre;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN_FORMACION")
    public Date getFechaFinFormacion() {
        return this.fechaFinFormacion;
    }

    public void setFechaFinFormacion(Date fechaFinFormacion) {
        this.fechaFinFormacion = fechaFinFormacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_ACTUALIZACION")
    public Date getFechaInicioActualizacion() {
        return this.fechaInicioActualizacion;
    }

    public void setFechaInicioActualizacion(Date fechaInicioActualizacion) {
        this.fechaInicioActualizacion = fechaInicioActualizacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_CIERRE")
    public Date getFechaInicioCierre() {
        return this.fechaInicioCierre;
    }

    public void setFechaInicioCierre(Date fechaInicioCierre) {
        this.fechaInicioCierre = fechaInicioCierre;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_FORMACION")
    public Date getFechaInicioFormacion() {
        return this.fechaInicioFormacion;
    }

    public void setFechaInicioFormacion(Date fechaInicioFormacion) {
        this.fechaInicioFormacion = fechaInicioFormacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "FORMACION_TOTAL", nullable = false, length = 2)
    public String getFormacionTotal() {
        return this.formacionTotal;
    }

    public void setFormacionTotal(String formacionTotal) {
        this.formacionTotal = formacionTotal;
    }

    @Column(name = "MUNICIPIO_CODIGO", nullable = false, length = 5)
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    @Column(name = "RESOLUCION_ACTUALIZACION_ID", precision = 10)
    public Long getResolucionActualizacionId() {
        return this.resolucionActualizacionId;
    }

    public void setResolucionActualizacionId(Long resolucionActualizacionId) {
        this.resolucionActualizacionId = resolucionActualizacionId;
    }

    @Column(name = "RESOLUCION_FORMACION_ID", nullable = false, precision = 10)
    public Long getResolucionFormacionId() {
        return this.resolucionFormacionId;
    }

    public void setResolucionFormacionId(Long resolucionFormacionId) {
        this.resolucionFormacionId = resolucionFormacionId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "ESTADO_CIERRE_ACTUALIZACION", length = 50)
    public String getEstadoCierreActualizacion() {
        return this.estadoCierreActualizacion;
    }

    public void setEstadoCierreActualizacion(String estadoCierreActualizacion) {
        this.estadoCierreActualizacion = estadoCierreActualizacion;
    }

    @Column(name = "ESTADO_CIERRE_CONSERVACION", length = 50)
    public String getEstadoCierreConservacion() {
        return this.estadoCierreConservacion;
    }

    public void setEstadoCierreConservacion(String estadoCierreConservacion) {
        this.estadoCierreConservacion = estadoCierreConservacion;
    }

    // bi-directional many-to-one association to ZonaCierreVigencia
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
        "municipioCierreVigencia")
    public List<ZonaCierreVigencia> getZonaCierreVigencias() {
        return this.zonaCierreVigencias;
    }

    public void setZonaCierreVigencias(
        List<ZonaCierreVigencia> zonaCierreVigencias) {
        this.zonaCierreVigencias = zonaCierreVigencias;
    }

    public ZonaCierreVigencia addZonaCierreVigencia(
        ZonaCierreVigencia zonaCierreVigencia) {
        getZonaCierreVigencias().add(zonaCierreVigencia);
        zonaCierreVigencia.setMunicipioCierreVigencia(this);

        return zonaCierreVigencia;
    }

    public ZonaCierreVigencia removeZonaCierreVigencia(
        ZonaCierreVigencia zonaCierreVigencia) {
        getZonaCierreVigencias().remove(zonaCierreVigencia);
        zonaCierreVigencia.setMunicipioCierreVigencia(null);

        return zonaCierreVigencia;
    }

    /**
     *
     * Método transient usado para los casos de uso 114 y 179.
     *
     * Retorna true si el municipio o parte del municipio se encuentra en proceso de formación o se
     * realizó formación en el año inmediatamente anterior a la vigencia del decreto a aplicar, de
     * lo contrario retorna false.
     *
     * Esto se sabe si en el campo formacionTotal tiene el valor 'SI' o si tiene zonas en formación.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isEstadoFormacion() {
        estadoFormacion = false;
        if (ESiNo.SI.getCodigo().equals(this.formacionTotal)) {
            estadoFormacion = true;
        } else if (this.zonaCierreVigencias != null &&
            !this.zonaCierreVigencias.isEmpty()) {
            for (ZonaCierreVigencia zcv : this.zonaCierreVigencias) {
                if (zcv.getFechaInicioFormacion() != null &&
                    zcv.getFechaFinCierre() == null) {
                    estadoFormacion = true;
                }
            }
        }
        return estadoFormacion;
    }

    public void setEstadoFormacion(boolean estadoFormacion) {
        this.estadoFormacion = estadoFormacion;
    }

    /**
     *
     * Método transient usado para los casos de uso 114 y 179.
     *
     * Retorna true si el municipio o parte del municipio se encuentra en proceso de actualización o
     * se realizó actualización en el año inmediatamente anterior a la vigencia del decreto a
     * aplicar, de lo contrario retorna false.
     *
     * Esto se sabe si en el campo actualizacionTotal tiene el valor 'SI' o si tiene zonas en
     * actualización.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isEstadoActualizacion() {
        estadoActualizacion = false;
        if (ESiNo.SI.getCodigo().equals(this.actualizacionTotal)) {
            estadoActualizacion = true;
        } else if (this.zonaCierreVigencias != null &&
            !this.zonaCierreVigencias.isEmpty()) {
            for (ZonaCierreVigencia zcv : this.zonaCierreVigencias) {
                if (zcv.getFechaInicioActualizacion() != null &&
                    zcv.getFechaFinCierre() == null) {
                    estadoActualizacion = true;
                }
            }
        }
        return estadoActualizacion;
    }

    public void setEstadoActualizacion(boolean estadoActualizacion) {
        this.estadoActualizacion = estadoActualizacion;
    }

    /**
     * Método transient usado para los casos de uso 114 y 179.
     *
     * Corresponde a la zona que se registró donde se está realizando o se realizó la formación en
     * el año inmediatamente anterior a la vigencia del decreto que se va a aplicar. Cuando se esté
     * formando más de una zona del municipio, se visualizarán cada una de las zonas en formación
     * separadas por punto y coma.
     *
     * @author david.cifuentes
     *
     * @return
     */
    @Transient
    public String getZonasEnActualizacion() {

        String zonasEnActualizacion = "";

        if (ESiNo.SI.getCodigo().equals(this.actualizacionTotal)) {
            zonasEnActualizacion = Constantes.TODAS_LAS_ZONAS;
        } else if (this.zonaCierreVigencias != null &&
            !this.zonaCierreVigencias.isEmpty()) {
            for (ZonaCierreVigencia zcv : this.zonaCierreVigencias) {
                if (zcv != null && zcv.getZonaCodigo() != null &&
                    !zcv.getZonaCodigo().trim().isEmpty() &&
                    zcv.getFechaInicioActualizacion() != null) {
                    if (EZonaCierreVigencia.RURAL.getCodigo().equals(
                        zcv.getZonaCodigo())) {
                        zonasEnActualizacion = zonasEnActualizacion
                            .concat(EZonaCierreVigencia.RURAL.getValor() +
                                "; ");
                    } else if (EZonaCierreVigencia.CORREGIMIENTOS.getCodigo()
                        .equals(zcv.getZonaCodigo())) {
                        zonasEnActualizacion = zonasEnActualizacion
                            .concat(EZonaCierreVigencia.CORREGIMIENTOS
                                .getValor() + "; ");
                    } else if (EZonaCierreVigencia.URBANA.getCodigo().equals(
                        zcv.getZonaCodigo())) {
                        zonasEnActualizacion = zonasEnActualizacion
                            .concat(EZonaCierreVigencia.URBANA.getValor() +
                                "; ");
                    }
                }
            }
            if (zonasEnActualizacion.length() > 0) {
                // Quitarle el último punto y coma
                zonasEnActualizacion = zonasEnActualizacion.substring(0,
                    zonasEnActualizacion.length() - 2);
                this.estadoActualizacion = true;
            }
        }

        return zonasEnActualizacion;
    }

    /**
     * Método transient usado para los casos de uso 114 y 179.
     *
     * Corresponde a la zona que se registró donde se está realizando o se realizó la actualización
     * en el año inmediatamente anterior a la vigencia del decreto que se va a aplicar. Cuando se
     * esté actualizando más de una zona del municipio, se visualizarán cada una de las zonas en
     * actualización separadas por punto y coma.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public String getZonasEnFormacion() {

        String zonasEnFormacion = "";

        if (ESiNo.SI.getCodigo().equals(this.formacionTotal)) {
            this.estadoActualizacion = true;
            zonasEnFormacion = Constantes.TODAS_LAS_ZONAS;
        } else if (this.zonaCierreVigencias != null &&
            !this.zonaCierreVigencias.isEmpty()) {
            for (ZonaCierreVigencia zcv : this.zonaCierreVigencias) {
                if (zcv != null && zcv.getZonaCodigo() != null &&
                    !zcv.getZonaCodigo().trim().isEmpty() &&
                    zcv.getFechaInicioFormacion() != null) {

                    if (EZonaCierreVigencia.RURAL.getCodigo().equals(
                        zcv.getZonaCodigo())) {
                        zonasEnFormacion = zonasEnFormacion
                            .concat(EZonaCierreVigencia.RURAL.getValor() +
                                "; ");
                    } else if (EZonaCierreVigencia.CORREGIMIENTOS.getCodigo()
                        .equals(zcv.getZonaCodigo())) {
                        zonasEnFormacion = zonasEnFormacion
                            .concat(EZonaCierreVigencia.CORREGIMIENTOS
                                .getValor() + "; ");
                    } else if (EZonaCierreVigencia.URBANA.getCodigo().equals(
                        zcv.getZonaCodigo())) {
                        zonasEnFormacion = zonasEnFormacion
                            .concat(EZonaCierreVigencia.URBANA.getValor() +
                                "; ");
                    }
                }
            }
            if (zonasEnFormacion.length() > 0) {
                // Quitarle el último punto y coma
                zonasEnFormacion = zonasEnFormacion.substring(0,
                    zonasEnFormacion.length() - 2);
            }
        }

        return zonasEnFormacion;
    }

    /**
     * Método transient para saber si el proceso de actualización o formación del municipio fue
     * cerrado o no.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isProcesoCerrado() {
        procesoCerrado = false;
        if ((ESiNo.SI.getCodigo().equals(this.formacionTotal) && this.fechaFinFormacion != null) ||
            (ESiNo.SI.getCodigo().equals(this.actualizacionTotal) && this.fechaFinActualizacion !=
            null)) {

            procesoCerrado = true;
        } else if (this.zonaCierreVigencias != null &&
            !this.zonaCierreVigencias.isEmpty() &&
            this.zonaCierreVigencias.get(0) != null) {
            // Se verifica para una de las zonas si el proceso de
            // actualización o formación está cerrado. Sólo se verifica para
            // una de ellas debido a que todas tienen el mismo
            // comportamiento.

            if (this.zonaCierreVigencias.get(0).getFechaFinActualizacion() != null ||
                this.zonaCierreVigencias.get(0).getFechaFinFormacion() != null) {
                this.procesoCerrado = true;
            }
        }

        return procesoCerrado;
    }

    public void setProcesoCerrado(boolean procesoCerrado) {
        this.procesoCerrado = procesoCerrado;
    }

    /**
     * Método transient para saber si ya se generaron las estadisticas precierre o poscierre para el
     * municipio
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isEstadisticasConservacion() {
        if (this.estadoCierreConservacion != null &&
            (this.estadoCierreConservacion
                .equals(EEstadoCierreMunicipio.ESTADISTICAS_PRECIERRE
                    .getEstado()) || this.estadoCierreConservacion
                .equals(EEstadoCierreMunicipio.ESTADISTICAS_POSCIERRE
                    .getEstado()) || this.estadoCierreConservacion
                .equals(EEstadoCierreMunicipio.CERRADO
                    .getEstado()) || this.estadoCierreConservacion
                .equals(EEstadoCierreMunicipio.CONFIRMADO
                    .getEstado()))) {
            return true;
        }
        return false;
    }

    /**
     * Determina si el MunicipioCierreVigencia está seleccionado en alguna pantalla o sitio en que
     * se requiera.
     *
     * @return
     */
    @Transient
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * Implementación del método compareTo para que esta clase sea Comparable. La comparación por
     * defecto es sobre la cadena que guarda el código del municipio.
     *
     * @author david.cifuentes
     * @return
     */
    @Override
    public int compareTo(MunicipioCierreVigencia mcv) {
        int answer = this.municipioCodigo.compareTo(mcv.getMunicipioCodigo());
        return answer;
    }

    /**
     * Método que retorna un Comparator para comparar objetos de este tipo por el criterio de que la
     * código del municipio, por defecto se realiza ascendentemente.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public static Comparator<MunicipioCierreVigencia> getComparatorCodigo() {

        return new Comparator<MunicipioCierreVigencia>() {
            @Override
            public int compare(MunicipioCierreVigencia mcv1,
                MunicipioCierreVigencia mcv2) {
                return mcv1.municipioCodigo.compareTo(mcv2.municipioCodigo);
            }
        };
    }

}
