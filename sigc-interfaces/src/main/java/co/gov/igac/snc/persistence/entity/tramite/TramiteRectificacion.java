package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * TramiteRectificacion entity. @author MyEclipse Persistence Tools
 *
 * @modified by pedro.garcia - adición de anotaciones para la generación por secuencia del id -
 * adición de anotación @Temporal para los campos fecha
 */
@Entity
@NamedQueries({@NamedQuery(name = "findTramiteRectificacionByTramite", query =
        "from TramiteRectificacion tr where tr.tramite= :tramite")})
@Table(name = "TRAMITE_RECTIFICACION", schema = "SNC_TRAMITE")
public class TramiteRectificacion implements java.io.Serializable {

    // Fields
    private Long id;
    private Tramite tramite;
    private DatoRectificar datoRectificar;
    private String valorRectificar;
    private String valorNuevo;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public TramiteRectificacion() {
    }

    /** minimal constructor */
    public TramiteRectificacion(Long id, DatoRectificar datoRectificar,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.datoRectificar = datoRectificar;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramiteRectificacion(Long id, Tramite tramite,
        DatoRectificar datoRectificar, String valorRectificar,
        String valorNuevo, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.datoRectificar = datoRectificar;
        this.valorRectificar = valorRectificar;
        this.valorNuevo = valorNuevo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TramiteRectificacion_ID_SEQ")
    @SequenceGenerator(name = "TramiteRectificacion_ID_SEQ", sequenceName =
        "TRAMITE_RECTIFICACION_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID")
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DATO_RECTIFICAR_ID", nullable = false)
    public DatoRectificar getDatoRectificar() {
        return this.datoRectificar;
    }

    public void setDatoRectificar(DatoRectificar datoRectificar) {
        this.datoRectificar = datoRectificar;
    }

    @Column(name = "VALOR_RECTIFICAR", length = 250)
    public String getValorRectificar() {
        return this.valorRectificar;
    }

    public void setValorRectificar(String valorRectificar) {
        this.valorRectificar = valorRectificar;
    }

    @Column(name = "VALOR_NUEVO", length = 250)
    public String getValorNuevo() {
        return this.valorNuevo;
    }

    public void setValorNuevo(String valorNuevo) {
        this.valorNuevo = valorNuevo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
