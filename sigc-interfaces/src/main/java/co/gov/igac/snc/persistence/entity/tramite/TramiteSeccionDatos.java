package co.gov.igac.snc.persistence.entity.tramite;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TramiteSeccionDatos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRAMITE_SECCION_DATOS", schema = "SNC_TRAMITE")
public class TramiteSeccionDatos implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 732443545626623268L;

    // Fields
    private Long id;
    private String tipoTramite;
    private String claseMutacion;
    private String subtipo;
    private String radicacionEspecial;
    private String tipoInscripcion;
    private String justificacionPropiedad;
    private String ubicacion;
    private String propietarios;
    private String avaluos;
    private String inscripcionesDecretos;
    private String textoMotivado;
    private String visorGis;
    private String fotos;
    private String fichaMatriz;
    private String proyeccionTramite;
    private String usuarioLog;
    private Timestamp fechaLog;

    // Constructors
    /** default constructor */
    public TramiteSeccionDatos() {
    }

    /** minimal constructor */
    public TramiteSeccionDatos(Long id, String justificacionPropiedad,
        String ubicacion, String propietarios, String avaluos,
        String inscripcionesDecretos, String textoMotivado,
        String visorGis, String fotos, String fichaMatriz,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.justificacionPropiedad = justificacionPropiedad;
        this.ubicacion = ubicacion;
        this.propietarios = propietarios;
        this.avaluos = avaluos;
        this.inscripcionesDecretos = inscripcionesDecretos;
        this.textoMotivado = textoMotivado;
        this.visorGis = visorGis;
        this.fotos = fotos;
        this.fichaMatriz = fichaMatriz;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramiteSeccionDatos(Long id, String tipoTramite,
        String claseMutacion, String subtipo, String tipoInscripcion,
        String justificacionPropiedad, String ubicacion,
        String propietarios, String avaluos, String inscripcionesDecretos,
        String textoMotivado, String visorGis, String fotos,
        String fichaMatriz, String proyeccionTramite, String usuarioLog,
        Timestamp fechaLog) {
        this.id = id;
        this.tipoTramite = tipoTramite;
        this.claseMutacion = claseMutacion;
        this.subtipo = subtipo;
        this.tipoInscripcion = tipoInscripcion;
        this.justificacionPropiedad = justificacionPropiedad;
        this.ubicacion = ubicacion;
        this.propietarios = propietarios;
        this.avaluos = avaluos;
        this.inscripcionesDecretos = inscripcionesDecretos;
        this.textoMotivado = textoMotivado;
        this.visorGis = visorGis;
        this.fotos = fotos;
        this.fichaMatriz = fichaMatriz;
        this.proyeccionTramite = proyeccionTramite;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_TRAMITE", length = 30)
    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @Column(name = "CLASE_MUTACION", length = 30)
    public String getClaseMutacion() {
        return this.claseMutacion;
    }

    public void setClaseMutacion(String claseMutacion) {
        this.claseMutacion = claseMutacion;
    }

    @Column(name = "SUBTIPO", length = 30)
    public String getSubtipo() {
        return this.subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    @Column(name = "RADICACION_ESPECIAL", length = 50)
    public String getRadicacionEspecial() {
        return radicacionEspecial;
    }

    public void setRadicacionEspecial(String radicacionEspecial) {
        this.radicacionEspecial = radicacionEspecial;
    }

    @Column(name = "TIPO_INSCRIPCION", length = 30)
    public String getTipoInscripcion() {
        return this.tipoInscripcion;
    }

    public void setTipoInscripcion(String tipoInscripcion) {
        this.tipoInscripcion = tipoInscripcion;
    }

    @Column(name = "JUSTIFICACION_PROPIEDAD", nullable = false, length = 30)
    public String getJustificacionPropiedad() {
        return this.justificacionPropiedad;
    }

    public void setJustificacionPropiedad(String justificacionPropiedad) {
        this.justificacionPropiedad = justificacionPropiedad;
    }

    @Column(name = "UBICACION", nullable = false, length = 30)
    public String getUbicacion() {
        return this.ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Column(name = "PROPIETARIOS", nullable = false, length = 30)
    public String getPropietarios() {
        return this.propietarios;
    }

    public void setPropietarios(String propietarios) {
        this.propietarios = propietarios;
    }

    @Column(name = "AVALUOS", nullable = false, length = 50)
    public String getAvaluos() {
        return this.avaluos;
    }

    public void setAvaluos(String avaluos) {
        this.avaluos = avaluos;
    }

    @Column(name = "INSCRIPCIONES_DECRETOS", nullable = false, length = 30)
    public String getInscripcionesDecretos() {
        return this.inscripcionesDecretos;
    }

    public void setInscripcionesDecretos(String inscripcionesDecretos) {
        this.inscripcionesDecretos = inscripcionesDecretos;
    }

    @Column(name = "TEXTO_MOTIVADO", nullable = false, length = 30)
    public String getTextoMotivado() {
        return this.textoMotivado;
    }

    public void setTextoMotivado(String textoMotivado) {
        this.textoMotivado = textoMotivado;
    }

    @Column(name = "VISOR_GIS", nullable = false, length = 30)
    public String getVisorGis() {
        return this.visorGis;
    }

    public void setVisorGis(String visorGis) {
        this.visorGis = visorGis;
    }

    @Column(name = "FOTOS", nullable = false, length = 30)
    public String getFotos() {
        return this.fotos;
    }

    public void setFotos(String fotos) {
        this.fotos = fotos;
    }

    @Column(name = "FICHA_MATRIZ", nullable = false, length = 30)
    public String getFichaMatriz() {
        return this.fichaMatriz;
    }

    public void setFichaMatriz(String fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    @Column(name = "PROYECCION_TRAMITE", nullable = false, length = 30)
    public String getProyeccionTramite() {
        return this.proyeccionTramite;
    }

    public void setProyeccionTramite(String proyeccionTramite) {
        this.proyeccionTramite = proyeccionTramite;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

}
