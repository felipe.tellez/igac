package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ACTUALIZACION_ORDEN_AJUSTE")
public class ActualizacionOrdenAjuste implements Serializable {

    private static final long serialVersionUID = -6017822179218444588L;

    private Long id;
    private Long actualizacionId;
    private String digitalizarNuevasAreas;
    private String estado;
    private Date fechaLog;
    private String incorporarNuevasManzanas;
    private String observaciones;
    private RecursoHumano recursoHumano;
    private Long soporteDocumentoId;
    private String usuarioLog;
    private List<AjusteManzanaArea> ajusteManzanaAreas;

    public ActualizacionOrdenAjuste() {
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_ORDEN_AJUSTE_ID_GENERATOR", sequenceName =
        "ACTUALIZACION_ORDEN_AJU_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_ORDEN_AJUSTE_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACTUALIZACION_ID")
    public Long getActualizacionId() {
        return this.actualizacionId;
    }

    public void setActualizacionId(Long actualizacionId) {
        this.actualizacionId = actualizacionId;
    }

    @Column(name = "DIGITALIZAR_NUEVAS_AREAS")
    public String getDigitalizarNuevasAreas() {
        return this.digitalizarNuevasAreas;
    }

    public void setDigitalizarNuevasAreas(String digitalizarNuevasAreas) {
        this.digitalizarNuevasAreas = digitalizarNuevasAreas;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "INCORPORAR_NUEVAS_MANZANAS")
    public String getIncorporarNuevasManzanas() {
        return this.incorporarNuevasManzanas;
    }

    public void setIncorporarNuevasManzanas(String incorporarNuevasManzanas) {
        this.incorporarNuevasManzanas = incorporarNuevasManzanas;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    //bi-directional many-to-one association to RecursoHumano
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "RECURSO_HUMANO_ID")
    public RecursoHumano getRecursoHumano() {
        return this.recursoHumano;
    }

    public void setRecursoHumano(RecursoHumano recursoHumano) {
        this.recursoHumano = recursoHumano;
    }

    @Column(name = "SOPORTE_DOCUMENTO_ID")
    public Long getSoporteDocumentoId() {
        return this.soporteDocumentoId;
    }

    public void setSoporteDocumentoId(Long soporteDocumentoId) {
        this.soporteDocumentoId = soporteDocumentoId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to AjusteManzanaArea
    @OneToMany(mappedBy = "actualizacionOrdenAjuste", cascade = CascadeType.ALL)
    public List<AjusteManzanaArea> getAjusteManzanaAreas() {
        return this.ajusteManzanaAreas;
    }

    public void setAjusteManzanaAreas(List<AjusteManzanaArea> ajusteManzanaAreas) {
        this.ajusteManzanaAreas = ajusteManzanaAreas;
    }

}
