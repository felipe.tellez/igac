package co.gov.igac.snc.util;

/**
 * Enumeracion para los posibles valores del log acceso.
 *
 * @author felipe.cadena
 */
public enum EEstadoSesion {

    INICIADA,
    NO_INICIADA,
    CANCELADA;

}
