package co.gov.igac.snc.persistence.entity.tramite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitanteTipoSolicitant;
import co.gov.igac.snc.util.Constantes;

/*
 * MODIFICACIONES PROPIAS A LA CLASE Solicitante: -> Se agregó el serialVersionUID. -> Se agregó el
 * generatedValue para el id @modified by fabio.navarrete -> Se agrega el método transient
 * getNombreCompleto -> Se agrega el método transient isJuridica -> Se modifica el método
 * setTipoPersona -> Se agrega copyConstructor -> Se agrega propiedad Transient relacion con su
 * getter y setter -> Se agrega método Transient isIntermediario que indica si el solicitante es un
 * intermediario -> Se agrega propiedad Transient notificacionEmail con su getter y setter
 *
 * @modified by pedro.garcia merge por cambios en campos de teléfono - adición de campo transient
 * nombreCompleto (estaba el getter, pero necesito el campo y el setter) - adición de campo
 * transient comunicacionDeNotificacionRegistrada para poder discriminar los solicitantes que ya
 * tienen un registro en TramiteDocumento de tipo comunicación de notificación, pero sion haber sido
 * registrada
 *
 * @modified by felipe.cadena se agregacampo transient para relacionar documentos de notificacion
 */
/**
 * Solicitante entity. @author MyEclipse Persistence Tools
 *
 * @modified juan.agudelo 08-05-12 Adición de atributo transient solicitanteNotificado el cual
 * almacena temporalmente el valor de notificación dependiendo de el documento asociado a la
 * notificación
 * @modified david.cifuentes :: Se agrega el atributo transient solicitanteCoincidente :: 06/10/2016
 */
@Entity
@NamedQueries({@NamedQuery(name = "findSolicitanteByTipoYNumeroDocumento", query =
        "from Solicitante s where s.numeroIdentificacion= :numeroIdentificacion and s.tipoIdentificacion= :tipoIdentificacion")})
@Table(name = "SOLICITANTE", schema = "SNC_TRAMITE", uniqueConstraints = @UniqueConstraint(
    columnNames = {
        "NUMERO_IDENTIFICACION", "TIPO_IDENTIFICACION"}))
public class Solicitante implements java.io.Serializable, Cloneable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = 7675825939299190537L;

    private Long id;
    private String tipoPersona;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String digitoVerificacion;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String razonSocial;
    private String sigla;
    private String direccion;
    private String telefonoPrincipal;
    private String telefonoPrincipalExt;
    private String telefonoSecundario;
    private String telefonoSecundarioExt;
    private String telefonoCelular;
    private String fax;
    private String faxExt;
    private String correoElectronico;
    private String correoElectronicoSecundario;
    private String estadoCivil;
    private String regimen;
    private String tipoSolicitante;
    private String usuarioLog;
    private Date fechaLog;
    private List<SolicitanteTramite> solicitanteTramites = new ArrayList<SolicitanteTramite>();
    private List<SolicitanteSolicitud> solicitanteSolicituds = new ArrayList<SolicitanteSolicitud>();
    private Pais direccionPais;
    private Departamento direccionDepartamento;
    private Municipio direccionMunicipio;
    private String relacion;
    private String notificacionEmail;
    private String codigoPostal;

    private String relacionNotificacion;
    private Long tramiteId;

    private String nombreCompleto;

    private boolean comunicacionDeNotificacionRegistrada;

    // transient
    private String solicitanteNotificado;

    private boolean selected;

    private String direccionPredio;

    private TramiteDocumento documentoNotificacion;

    private boolean solicitanteCoincidente;

    // Constructors
    /** default constructor */
    public Solicitante() {
    }

    /** Copy constructor */
    public Solicitante(Solicitante solicitante) {
        this(solicitante.getId(), solicitante.getTipoPersona(), solicitante
            .getTipoIdentificacion(),
            solicitante.getNumeroIdentificacion(), solicitante
            .getDigitoVerificacion(),
            solicitante.getPrimerNombre(), solicitante.getSegundoNombre(),
            solicitante.getPrimerApellido(), solicitante
            .getSegundoApellido(), solicitante.getRazonSocial(),
            solicitante.getSigla(), solicitante.getDireccion(), solicitante
            .getDireccionPais(), solicitante
                .getDireccionDepartamento(), solicitante
                .getDireccionMunicipio(), solicitante
                .getTelefonoPrincipal(), solicitante
                .getTelefonoPrincipalExt(), solicitante
                .getTelefonoSecundario(), solicitante
                .getTelefonoSecundarioExt(), solicitante
                .getTelefonoCelular(), solicitante.getFax(),
            solicitante.getFaxExt(), solicitante.getCorreoElectronico(),
            solicitante.getCorreoElectronicoSecundario(), solicitante
            .getEstadoCivil(), solicitante.getRegimen(),
            solicitante.getTipoSolicitante(), solicitante.getUsuarioLog(),
            solicitante.getFechaLog());
    }

    /**
     * Crea un solicitante a partir de una persona proyectada
     *
     * @author felipe.cadena
     * @param persona
     */
    public Solicitante(PPersona persona) {
        this(null, persona.getTipoPersona(), persona
            .getTipoIdentificacion(),
            persona.getNumeroIdentificacion(), persona
            .getDigitoVerificacion(),
            persona.getPrimerNombre(), persona.getSegundoNombre(),
            persona.getPrimerApellido(), persona
            .getSegundoApellido(), persona.getRazonSocial(),
            persona.getSigla(), persona.getDireccion(), persona
            .getDireccionPais(), persona
                .getDireccionDepartamento(), persona
                .getDireccionMunicipio(), persona
                .getTelefonoPrincipal(), persona
                .getTelefonoPrincipalExt(), persona
                .getTelefonoSecundario(), persona
                .getTelefonoSecundarioExt(), persona
                .getTelefonoCelular(), persona.getFax(),
            persona.getFaxExt(), persona.getCorreoElectronico(),
            persona.getCorreoElectronicoSecundario(), persona
            .getEstadoCivil(), "",
            "", persona.getUsuarioLog(),
            persona.getFechaLog());
    }

    /** minimal constructor */
    public Solicitante(Long id, String tipoPersona, String tipoIdentificacion,
        String numeroIdentificacion, String tipoSolicitante,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tipoPersona = tipoPersona;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.tipoSolicitante = tipoSolicitante;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Solicitante(Long id, String tipoPersona, String tipoIdentificacion,
        String numeroIdentificacion, String digitoVerificacion,
        String primerNombre, String segundoNombre, String primerApellido,
        String segundoApellido, String razonSocial, String sigla,
        String direccion, Pais direccionPais,
        Departamento direccionDepartamento, Municipio direccionMunicipio,
        String telefonoPrincipal, String telefonoPrincipalExt,
        String telefonoSecundario, String telefonoSecundarioExt,
        String telefonoCelular, String fax, String faxExt,
        String correoElectronico, String correoElectronicoSecundario,
        String estadoCivil, String regimen, String tipoSolicitante,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tipoPersona = tipoPersona;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.digitoVerificacion = digitoVerificacion;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.razonSocial = razonSocial;
        this.sigla = sigla;
        this.direccion = direccion;
        this.direccionPais = direccionPais;
        this.direccionDepartamento = direccionDepartamento;
        this.direccionMunicipio = direccionMunicipio;
        this.telefonoPrincipal = telefonoPrincipal;
        this.telefonoPrincipalExt = telefonoPrincipalExt;
        this.telefonoSecundario = telefonoSecundario;
        this.telefonoSecundarioExt = telefonoSecundarioExt;
        this.telefonoCelular = telefonoCelular;
        this.fax = fax;
        this.faxExt = faxExt;
        this.correoElectronico = correoElectronico;
        this.correoElectronicoSecundario = correoElectronicoSecundario;
        this.estadoCivil = estadoCivil;
        this.regimen = regimen;
        this.tipoSolicitante = tipoSolicitante;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SOLICITANTE_ID_SEQ")
    @SequenceGenerator(name = "SOLICITANTE_ID_SEQ", sequenceName = "SOLICITANTE_ID_SEQ",
        allocationSize = 0)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_PERSONA", nullable = false, length = 30)
    public String getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        if (tipoPersona.equals(EPersonaTipoPersona.NATURAL.getCodigo())) {
            this.tipoSolicitante = (ESolicitanteTipoSolicitant.PRIVADA
                .getCodigo());
        }
        this.tipoPersona = tipoPersona;
    }

    @Column(name = "TIPO_IDENTIFICACION", nullable = false, length = 30)
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "NUMERO_IDENTIFICACION", nullable = false, length = 50)
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "DIGITO_VERIFICACION", length = 2)
    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    @Column(name = "PRIMER_NOMBRE", length = 100)
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Column(name = "SEGUNDO_NOMBRE", length = 100)
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Column(name = "PRIMER_APELLIDO", length = 100)
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "SEGUNDO_APELLIDO", length = 100)
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "RAZON_SOCIAL", length = 100)
    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Column(name = "SIGLA", length = 50)
    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Column(name = "DIRECCION", length = 250)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "TELEFONO_PRINCIPAL", length = 15)
    public String getTelefonoPrincipal() {
        return this.telefonoPrincipal;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    @Column(name = "TELEFONO_PRINCIPAL_EXT", length = 6)
    public String getTelefonoPrincipalExt() {
        return this.telefonoPrincipalExt;
    }

    public void setTelefonoPrincipalExt(String telefonoPrincipalExt) {
        this.telefonoPrincipalExt = telefonoPrincipalExt;
    }

    @Column(name = "TELEFONO_SECUNDARIO", length = 15)
    public String getTelefonoSecundario() {
        return this.telefonoSecundario;
    }

    public void setTelefonoSecundario(String telefonoSecundario) {
        this.telefonoSecundario = telefonoSecundario;
    }

    @Column(name = "TELEFONO_SECUNDARIO_EXT", length = 6)
    public String getTelefonoSecundarioExt() {
        return this.telefonoSecundarioExt;
    }

    public void setTelefonoSecundarioExt(String telefonoSecundarioExt) {
        this.telefonoSecundarioExt = telefonoSecundarioExt;
    }

    @Column(name = "TELEFONO_CELULAR", length = 15)
    public String getTelefonoCelular() {
        return this.telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    @Column(name = "FAX", length = 50)
    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Column(name = "FAX_EXT", length = 6)
    public String getFaxExt() {
        return this.faxExt;
    }

    public void setFaxExt(String faxExt) {
        this.faxExt = faxExt;
    }

    @Column(name = "CORREO_ELECTRONICO", length = 100)
    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Column(name = "CORREO_ELECTRONICO_SECUNDARIO", length = 100)
    public String getCorreoElectronicoSecundario() {
        return this.correoElectronicoSecundario;
    }

    public void setCorreoElectronicoSecundario(
        String correoElectronicoSecundario) {
        this.correoElectronicoSecundario = correoElectronicoSecundario;
    }

    @Column(name = "ESTADO_CIVIL", length = 30)
    public String getEstadoCivil() {
        return this.estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @Column(name = "REGIMEN", length = 50)
    public String getRegimen() {
        return this.regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    @Column(name = "TIPO_SOLICITANTE", nullable = true, length = 30)
    public String getTipoSolicitante() {
        return this.tipoSolicitante;
    }

    public void setTipoSolicitante(String tipoSolicitante) {
        this.tipoSolicitante = tipoSolicitante;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "CODIGO_POSTAL")
    public String getCodigoPostal() {
        return this.codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitante")
    public List<SolicitanteTramite> getSolicitanteTramites() {
        return this.solicitanteTramites;
    }

    public void setSolicitanteTramites(
        List<SolicitanteTramite> solicitanteTramites) {
        this.solicitanteTramites = solicitanteTramites;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitante")
    public List<SolicitanteSolicitud> getSolicitanteSolicituds() {
        return this.solicitanteSolicituds;
    }

    public void setSolicitanteSolicituds(
        List<SolicitanteSolicitud> solicitanteSolicituds) {
        this.solicitanteSolicituds = solicitanteSolicituds;
    }

    /**
     * @author fabio.navarrete
     * @return
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_PAIS_CODIGO", nullable = true)
    public Pais getDireccionPais() {
        return this.direccionPais;
    }

    /**
     * @author fabio.navarrete
     * @param direccionPais
     */
    public void setDireccionPais(Pais direccionPais) {
        this.direccionPais = direccionPais;
    }

    /**
     * @author fabio.navarrete
     * @return
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_DEPARTAMENTO_CODIGO", nullable = true)
    public Departamento getDireccionDepartamento() {
        return this.direccionDepartamento;
    }

    /**
     * @author fabio.navarrete
     * @param direccionDepartamento
     */
    public void setDireccionDepartamento(Departamento direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    /**
     * @author fabio.navarrete
     * @return
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_MUNICIPIO_CODIGO", nullable = true)
    public Municipio getDireccionMunicipio() {
        return this.direccionMunicipio;
    }

    /**
     * @author fabio.navarrete
     * @param direccionMunicipio
     */
    public void setDireccionMunicipio(Municipio direccionMunicipio) {
        this.direccionMunicipio = direccionMunicipio;
    }

    @Column(name = "RELACION_NOTIFICACION")
    public String getRelacionNotificacion() {
        return this.relacionNotificacion;
    }

    public void setRelacionNotificacion(String relacionNotificacion) {
        this.relacionNotificacion = relacionNotificacion;
    }

    @Column(name = "TRAMITE_ID")
    public Long getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    /**
     * Método que retorna el nombre completo concatenando los campos para el solicitante.
     *
     * @author fabio.navarrete
     * @modified pedro.garcia pregunto primero si el atributo nombreCompleto es diferente de null y
     * de "" porque a veces se le hace set antes (y no se le hace set individual a las partes del
     * nombre)
     */
    @Transient
    public String getNombreCompleto() {

        if (this.nombreCompleto != null && !this.nombreCompleto.equals("")) {
            return this.nombreCompleto;
        }

        String answer = null;

        if ((this.tipoIdentificacion != null &&
            (this.tipoIdentificacion.equals(EPersonaTipoIdentificacion.NIT.getCodigo()) ||
            this.tipoIdentificacion.equals(Constantes.TIPO_DOC_SOLICITANTE_ACTUALIZACION))) ||
            (this.tipoPersona != null && this.tipoPersona.equals(EPersonaTipoPersona.JURIDICA.
                getCodigo()))) {
            if (this.razonSocial != null) {
                answer = this.razonSocial + " ";
            }
        } else if (this.tipoIdentificacion != null && this.tipoIdentificacion
            .equals(EPersonaTipoIdentificacion.NO_ESPECIFICADO.getCodigo())) {

            /*
             * #19067 :: javier.aponte :: 25/08/2016 Cuando el tipo de identificación es X, primero
             * se consulta si tiene nombre y luego sólo en caso que no lo tenga se consulta si tiene
             * razón social
             */
            answer = this.agregarNombreCompleto();
            if (answer.trim().isEmpty()) {
                answer = this.razonSocial + " ";
            }
        } else {
            answer = this.agregarNombreCompleto();
        }
        return answer;
    }

    /**
     * Método encargado de agregar en un StringBuilder el nombre completo del solicitante
     *
     * @return
     * @author javier.aponte
     */
    private String agregarNombreCompleto() {

        StringBuilder nombreSolicitante = new StringBuilder();

        if (this.primerNombre != null) {
            nombreSolicitante.append(this.primerNombre).append(" ");
        }
        if (this.segundoNombre != null) {
            nombreSolicitante.append(this.segundoNombre).append(" ");
        }
        if (this.primerApellido != null) {
            nombreSolicitante.append(this.primerApellido).append(" ");
        }
        if (this.segundoApellido != null) {
            nombreSolicitante.append(this.segundoApellido).append(" ");
        }

        return nombreSolicitante.toString();

    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que indica si entre los datos duplicados del solicitante, éste es jurídico o no.
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isJuridica() {

        if (tipoPersona != null) {
            if (tipoPersona.equals(EPersonaTipoPersona.JURIDICA.getCodigo())) {
                this.tipoIdentificacion = EPersonaTipoIdentificacion.NIT
                    .getCodigo();
                return true;
            }
        }
        return false;
    }

    @Transient
    public String getRelacion() {
        return relacion;
    }

    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }

    @Transient
    public String getNotificacionEmail() {
        return this.notificacionEmail;
    }

    public void setNotificacionEmail(String notificacionEmail) {
        this.notificacionEmail = notificacionEmail;
    }

    /**
     * Método encargado de retornar el valor sí o no del dominio a partir de un booleano
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isNotificacionEmailBoolean() {
        if (this.notificacionEmail != null) {
            return this.notificacionEmail.equals(ESiNo.SI.getCodigo());
        }
        return false;
    }

    /**
     * Método encargado de asignar el valor sí o no del dominio a partir de un booleano
     *
     * @param notificacionEmail
     * @author fabio.navarrete
     */
    public void setNotificacionEmailBoolean(boolean notificacionEmail) {
        if (notificacionEmail) {
            this.notificacionEmail = ESiNo.SI.getCodigo();
        } else {
            this.notificacionEmail = ESiNo.NO.getCodigo();
        }
    }

    /**
     * Indica si el objeto Solicitante es un intermediario
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isIntermediario() {
        if (this.relacion != null) {
            return this.relacion
                .equals(ESolicitanteSolicitudRelac.TESORERIA.toString());
        }
        return false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Transient
    public boolean isComunicacionDeNotificacionRegistrada() {
        return this.comunicacionDeNotificacionRegistrada;
    }

    public void setComunicacionDeNotificacionRegistrada(
        boolean comunicacionDeNotificacionRegistrada) {
        this.comunicacionDeNotificacionRegistrada = comunicacionDeNotificacionRegistrada;
    }

    // -------------------------------------------------------------------------
    /**
     * Variable que almacena temporalmente si el solicitante fue notificado
     *
     * @author juan.agudelo
     */
    @Transient
    public String getSolicitanteNotificado() {
        return solicitanteNotificado;
    }

    public void setSolicitanteNotificado(String solicitanteNotificado) {
        this.solicitanteNotificado = solicitanteNotificado;
    }

    /**
     * Determina si el Solicitante está seleccionado en alguna pantalla o sitio en que se requiera.
     *
     * @return
     */
    @Transient
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * Determina si el objeto tiene relación de Afectado
     *
     * @return
     */
    @Transient
    public boolean isAfectado() {
        if (this.relacion != null) {
            return this.relacion.equals(ESolicitanteSolicitudRelac.AFECTADO
                .toString());
        }
        return false;
    }

    /**
     * Direccion del predio para notificaciones de tramites masivos
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public String getDireccionPredio() {
        return direccionPredio;
    }

    public void setDireccionPredio(String direccionPredio) {
        this.direccionPredio = direccionPredio;
    }

    /**
     * Documento asociado de la notificacion
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public TramiteDocumento getDocumentoNotificacion() {
        return documentoNotificacion;
    }

    public void setDocumentoNotificacion(TramiteDocumento documentoNotificacion) {
        this.documentoNotificacion = documentoNotificacion;
    }

    /**
     * Determina si el solicitante fue notificado por direccion, en cuyo caso retorna SI, de lo
     * contrario retorna NO
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public String getNotificadoPorDireccion() {
        if (this.documentoNotificacion == null) {
            return ESiNo.NO.getCodigo();
        }

        if (documentoNotificacion.getObservacionNotificacion() != null &&
            documentoNotificacion.getObservacionNotificacion().contains(
                Constantes.NOTIFICADO_CON_DIRECCION)) {
            return ESiNo.SI.getCodigo();
        } else {
            return ESiNo.NO.getCodigo();
        }
    }

    /**
     * Método que indica el id del solicitante.
     *
     * @return
     * @author david.cifuentes
     */
    @Transient
    public boolean isSolicitanteCoincidente() {
        return solicitanteCoincidente;
    }

    public void setSolicitanteCoincidente(boolean solicitanteCoincidente) {
        this.solicitanteCoincidente = solicitanteCoincidente;
    }

    /**
     * @author javier.aponte Método para poder hacer un clon del solicitante
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

    /**
     * Sobrecarga del método equals para la comparación de solicitantes basado en su id, tipo de
     * identificación y numero de identificación.
     *
     * @author david.cifuentes
     */
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Solicitante)) {
            return false;
        }
        Solicitante castOther = (Solicitante) o;
        return (this.id != null && this.id.equals(castOther.getId()) &&
            this.tipoIdentificacion != null && this.tipoIdentificacion.equals(castOther.
                getTipoIdentificacion()) &&
            this.numeroIdentificacion != null && this.numeroIdentificacion.equals(castOther.
                getNumeroIdentificacion()));
    }

}
