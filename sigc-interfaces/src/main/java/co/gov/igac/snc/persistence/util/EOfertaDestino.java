/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los destinos de una oferta inmobiliaria, corresponde al dominio OFERTA_DESTINO
 *
 * @author christian.rodriguez
 */
public enum EOfertaDestino {

    // D: código (valor)
    AGROPECUARIO(""),
    COMERCIAL(""),
    DOTACIONAL(""),
    INDUSTRIAL(""),
    PARQUEADERO_DEPOSITO(""),
    RECREACIONAL(""),
    RESIDENCIAL(""),
    SUELO_DE_PROTECCION(""),
    URBANIZADO_NO_EDIFICADO(""),
    URBANIZABLE_NO_URBANIZADO("");

    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private EOfertaDestino(String valor) {
        this.valor = valor;
    }

    public String getCodigo() {
        return this.toString();
    }

}
