/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * Representa la entidad REP_CONFIG_PARAMETRO_REPORTE
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "REP_CONFIG_PARAMETRO_REPORTE")
public class RepConfigParametroReporte implements java.io.Serializable, Cloneable {

    private static final long serialVersionUID = 4262970936756309656L;

// Campos bd
    private Long id;

    private Long reporteId;

    private String categoria;

    private String nombreParametro;

    private String habilitado;

    private String requerido;

    private String usuarioLog;

    private Date fechaLog;

// Campos transient(Deben tener autor y descripcion)
    /** default constructor */
    public RepConfigParametroReporte() {
    }
// Metodos

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "REP_CONFIG_PARAM_REP_ID_SEQ_GEN")
    @SequenceGenerator(name = "REP_CONFIG_PARAM_REP_ID_SEQ_GEN", sequenceName =
        "REP_CONFIG_PARAM_REP_ID_SEQ", allocationSize = 1)
    @Column(name = "ID")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "REPORTE_ID")
    public Long getReporteId() {
        return this.reporteId;
    }

    public void setReporteId(Long reporteId) {
        this.reporteId = reporteId;
    }

    @Column(name = "CATEGORIA")
    public String getCategoria() {
        return this.categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Column(name = "NOMBRE_PARAMETRO")
    public String getNombreParametro() {
        return this.nombreParametro;
    }

    public void setNombreParametro(String nombreParametro) {
        this.nombreParametro = nombreParametro;
    }

    @Column(name = "HABILITADO")
    public String getHabilitado() {
        return this.habilitado;
    }

    public void setHabilitado(String habilitado) {
        this.habilitado = habilitado;
    }

    @Column(name = "REQUERIDO")
    public String getRequerido() {
        return this.requerido;
    }

    public void setRequerido(String requerido) {
        this.requerido = requerido;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

// Metodos transient(Deben tener autor y descripcion)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RepConfigParametroReporte other = (RepConfigParametroReporte) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
