package co.gov.igac.snc.persistence.entity.generales;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import org.apache.commons.io.FilenameUtils;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDocumentoMimeTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;

/**
 * Documento entity.
 */
/*
 * Modificaciones 26-07-11 @author juan.agudelo se modifico el objeto String
 * estructuraOrganizacionalId a estructuraOrganizacionalCod por cambio en el modelo de datos
 * 22-08-2011 adicion del objeto String idRepositorioDocumentos acorde al cambio en el modelo de
 * datos
 *
 * @modified 02-09-2011 juan.agudelo se agrego el un objeto temporal nombreDeArchivoCargado que
 * permita el visualizar el nombre original del documento cargado, tambien utilizado temporalmente
 * para almacenar la ruta temporal de previsualización de archivos
 *
 * @modified 03-10-11 juan.agudelo se agrego la columna nombreEntidadExterna y se elimino
 * nombreUsuarioCreador
 *
 * @modified pedro.garcia 10-10-2011 se eliminó el atributo transient archivoFisico 16-05-2013
 * adición de atributos rutaArchivoLocal y rutaArchivoWeb (ver documentación)
 *
 * @modified juan.agudelo 13-12-11 se agregaron los objetos errorProcesoMasivos y
 * documentoArchivoAnexos
 *
 * @modified juan.agudelo 13-12-11 se agregaron los atributos transient archivoADescargar
 *
 * @modified david.cifuentes 02-04-2012 Se agregó el atributo cantidadFolios
 *
 * @modified rodrigo.hernandez 07-11-2012 Se agregó implementacion de metodo equals
 *
 * @modified by leidy.gonzalez 04/09/2014 Se agrega interfaz Clonable y metodo clone para hacer un
 * clon de documentacion.
 */
@Entity
@Table(name = "DOCUMENTO", schema = "SNC_GENERALES")
public class Documento implements java.io.Serializable, Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = 5909724768466153907L;

    // Fields
    private Long id;
    private TipoDocumento tipoDocumento;
    private String archivo;// nombre del archivo
    private String estado;
    private String estructuraOrganizacionalCod;
    private Long predioId;
    private Long tramiteId;
    private String procesoId;
    private String descripcion;
    private String observaciones;
    private String bloqueado;
    private String usuarioCreador;
    private String nombreEntidadExterna;
    private String numeroRadicacion;
    private String numeroDocumento;// Numero cuando el documento es generado por
    // la entidad
    private String idRepositorioDocumentos;
    private String usuarioLog;
    private Date fechaRadicacion;// Fecha recibido en el sistema
    private Date fechaLog;
    private Date fechaDocumento;// Fecha de creación asignada por el usuario

    private List<ErrorProcesoMasivo> errorProcesoMasivos;
    private List<DocumentoArchivoAnexo> documentoArchivoAnexos;

    private Integer cantidadFolios;

    // Transient:
    private String nombreDeArchivoCargado;

//TODO :: juan.agudelo :: 31-05-2012 :: wtf? para qué se usa este atributo? qué significa? :: pedro.garcia
    private boolean archivoADescargar;

    /**
     * almacena la ruta de ubicación del archivo en la máquina local (la del usuario), en la carpeta
     * temporal del sistema.
     */
    private String rutaArchivoLocal;

    /**
     * almacena la ruta de ubicación del archivo en la web. Principalmente la url pública del
     * archivo.
     */
    private String rutaArchivoWeb;

    private String numeroPredial;

    private PUnidadConstruccion pUnidadConstruccion;

    // Constructors
    /** default constructor */
    public Documento() {
    }

    /** minimal constructor */
    public Documento(Long id, TipoDocumento tipoDocumento, String archivo,
        String estado, String bloqueado, String usuarioCreador,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tipoDocumento = tipoDocumento;
        this.archivo = archivo;
        this.estado = estado;
        this.bloqueado = bloqueado;
        this.usuarioCreador = usuarioCreador;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Documento(Long id, TipoDocumento tipoDocumento, String archivo,
        String estado, String estructuraOrganizacionalCod, Long predioId,
        Long tramiteId, String procesoId, String descripcion,
        String observaciones, String bloqueado, String usuarioCreador,
        String nombreEntidadExterna, Integer cantidadFolios,
        String numeroRadicacion, Date fechaRadicacion,
        String numeroDocumento, String usuarioLog, Date fechaLog,
        Date fechaDocumento, String idRepositorioDocumentos) {
        this.id = id;
        this.tipoDocumento = tipoDocumento;
        this.archivo = archivo;
        this.estado = estado;
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
        this.predioId = predioId;
        this.tramiteId = tramiteId;
        this.procesoId = procesoId;
        this.descripcion = descripcion;
        this.observaciones = observaciones;
        this.bloqueado = bloqueado;
        this.usuarioCreador = usuarioCreador;
        this.nombreEntidadExterna = nombreEntidadExterna;
        this.numeroRadicacion = numeroRadicacion;
        this.fechaRadicacion = fechaRadicacion;
        this.numeroDocumento = numeroDocumento;
        this.cantidadFolios = cantidadFolios;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.fechaDocumento = fechaDocumento;
        this.idRepositorioDocumentos = idRepositorioDocumentos;
    }

    /**
     * Constructor para subir imagenes al predio
     *
     * @author franz.gamba
     * @return
     */
    public Documento(UsuarioDTO usuario, Tramite tram, String archivo,
        boolean esAntes) {

        TipoDocumento tipoDoc = new TipoDocumento();
        if (esAntes) {
            tipoDoc.setId(ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_ANTES_DEL_TRAMITE.getId());
        } else {
            tipoDoc.setId(ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_DESPUES_DEL_TRAMITE.getId());
        }
        this.tipoDocumento = tipoDoc;
        this.archivo = archivo;
        this.estado = EDocumentoEstado.CARGADO.toString();
        this.bloqueado = "NO";
        this.usuarioCreador = usuario.getNombreCompleto();
        this.usuarioLog = usuario.getLogin();
        this.fechaLog = new Date();
        this.tramiteId = tram.getId();
        if (tram.getPredio() != null) {
            this.predioId = tram.getPredio().getId();
        }
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOCUMENTO_ID_SEQ")
    @SequenceGenerator(name = "DOCUMENTO_ID_SEQ", sequenceName = "DOCUMENTO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_DOCUMENTO_ID", nullable = false)
    public TipoDocumento getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Column(name = "ARCHIVO", nullable = false, length = 250)
    public String getArchivo() {
        return this.archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    @Column(name = "ESTADO", nullable = false, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD", length = 20)
    public String getEstructuraOrganizacionalCod() {
        return this.estructuraOrganizacionalCod;
    }

    public void setEstructuraOrganizacionalCod(
        String estructuraOrganizacionalCod) {
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
    }

    @Column(name = "PREDIO_ID", precision = 10, scale = 0)
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    @Column(name = "TRAMITE_ID", precision = 10, scale = 0)
    public Long getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    @Column(name = "PROCESO_ID", length = 100)
    public String getProcesoId() {
        return this.procesoId;
    }

    public void setProcesoId(String procesoId) {
        this.procesoId = procesoId;
    }

    @Column(name = "DESCRIPCION", length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "OBSERVACIONES", length = 600)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "BLOQUEADO", nullable = false, length = 2)
    public String getBloqueado() {
        return this.bloqueado;
    }

    public void setBloqueado(String bloqueado) {
        this.bloqueado = bloqueado;
    }

    @Column(name = "USUARIO_CREADOR", nullable = false, length = 100)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "NOMBRE_ENTIDAD_EXTERNA", length = 100)
    public String getNombreEntidadExterna() {
        return this.nombreEntidadExterna;
    }

    public void setNombreEntidadExterna(String nombreEntidadExterna) {
        this.nombreEntidadExterna = nombreEntidadExterna;
    }

    @Column(name = "NUMERO_RADICACION", length = 20)
    public String getNumeroRadicacion() {
        return this.numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    @Column(name = "FECHA_RADICACION", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaRadicacion() {
        return this.fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    @Column(name = "NUMERO_DOCUMENTO", length = 20)
    public String getNumeroDocumento() {
        return this.numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    @Column(name = "ID_REPOSITORIO_DOCUMENTOS", length = 256)
    public String getIdRepositorioDocumentos() {
        return this.idRepositorioDocumentos;
    }

    public void setIdRepositorioDocumentos(String idRepositorioDocumentos) {
        this.idRepositorioDocumentos = idRepositorioDocumentos;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    // bi-directional many-to-one association to ErrorProcesoMasivo
    @OneToMany(mappedBy = "soporteDocumento")
    public List<ErrorProcesoMasivo> getErrorProcesoMasivos() {
        return this.errorProcesoMasivos;
    }

    public void setErrorProcesoMasivos(
        List<ErrorProcesoMasivo> errorProcesoMasivos) {
        this.errorProcesoMasivos = errorProcesoMasivos;
    }

    // bi-directional many-to-one association to DocumentoArchivoAnexo
    @OneToMany(mappedBy = "documento", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<DocumentoArchivoAnexo> getDocumentoArchivoAnexos() {
        return this.documentoArchivoAnexos;
    }

    public void setDocumentoArchivoAnexos(
        List<DocumentoArchivoAnexo> documentoArchivoAnexos) {
        this.documentoArchivoAnexos = documentoArchivoAnexos;
    }

    @Column(name = "CANTIDAD_FOLIOS", precision = 6, scale = 0)
    public Integer getCantidadFolios() {
        return this.cantidadFolios;
    }

    public void setCantidadFolios(Integer cantidadFolios) {
        this.cantidadFolios = cantidadFolios;
    }

    // ------------------------------------------------------------------------
    @Column(name = "FECHA_DOCUMENTO", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaDocumento() {
        return this.fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    // -------------------------------------------------------------------------
    @Transient
    public String getNombreDeArchivoCargado() {
        return this.nombreDeArchivoCargado;
    }

    public void setNombreDeArchivoCargado(String nombreDeArchivoCargado) {
        this.nombreDeArchivoCargado = nombreDeArchivoCargado;
    }

    // -------------------------------------------------------------------------
    @Transient
    public boolean isArchivoADescargar() {

        archivoADescargar = false;

        if (this.archivo != null && !this.archivo.isEmpty()) {

            String extDocTemporal;
            extDocTemporal = FilenameUtils.getExtension(this.archivo);

            if (extDocTemporal.equals(EDocumentoMimeTipo.DOC.getExtension()) ||
                extDocTemporal.equals(EDocumentoMimeTipo.DOCX
                    .getExtension()) ||
                extDocTemporal.equals(EDocumentoMimeTipo.XLS
                    .getExtension()) ||
                extDocTemporal.equals(EDocumentoMimeTipo.XLSX
                    .getExtension())) {

                archivoADescargar = true;
            }
        }

        return archivoADescargar;
    }

    @Override
    public String toString() {
        return "Documento [id=" + id + ", tipoDocumento=" + tipoDocumento +
            ", archivo=" + archivo + ", estado=" + estado +
            ", estructuraOrganizacionalCod=" +
            estructuraOrganizacionalCod + ", predioId=" + predioId +
            ", tramiteId=" + tramiteId + ", procesoId=" + procesoId +
            ", descripcion=" + descripcion + ", observaciones=" +
            observaciones + ", bloqueado=" + bloqueado +
            ", usuarioCreador=" + usuarioCreador +
            ", nombreEntidadExterna=" + nombreEntidadExterna +
            ", numeroRadicacion=" + numeroRadicacion +
            ", numeroDocumento=" + numeroDocumento +
            ", idRepositorioDocumentos=" + idRepositorioDocumentos +
            ", usuarioLog=" + usuarioLog + ", fechaRadicacion=" +
            fechaRadicacion + ", fechaLog=" + fechaLog +
            ", fechaDocumento=" + fechaDocumento +
            ", errorProcesoMasivos=" + errorProcesoMasivos +
            ", documentoArchivoAnexos=" + documentoArchivoAnexos +
            ", cantidadFolios=" + cantidadFolios +
            ", nombreDeArchivoCargado=" + nombreDeArchivoCargado +
            ", archivoADescargar=" + archivoADescargar + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Documento other = (Documento) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

//--------------------------------------------------------------------------------------------------
    @Transient
    public String getRutaArchivoLocal() {
        return this.rutaArchivoLocal;
    }

    public void setRutaArchivoLocal(String rutaArchivoLocal) {
        this.rutaArchivoLocal = rutaArchivoLocal;
    }

    @Transient
    public String getRutaArchivoWeb() {
        return this.rutaArchivoWeb;
    }

    public void setRutaArchivoWeb(String rutaArchivoWeb) {
        this.rutaArchivoWeb = rutaArchivoWeb;
    }

    /**
     * @author leidy.gonzalez Método para poder hacer un clon de documentación
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

    /**
     * @author leidy.gonzalez Relaciona la Unidad de Construccion cuando el documento es una Foto
     * @return
     */
    @Transient
    public PUnidadConstruccion getpUnidadConstruccion() {
        return pUnidadConstruccion;
    }

    public void setpUnidadConstruccion(PUnidadConstruccion pUnidadConstruccion) {
        this.pUnidadConstruccion = pUnidadConstruccion;
    }

//end of class    
}
