package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the GTT_OFERTA_INMOBILIARIA database table.
 *
 * @modified by javier.aponte: -> Se cambió el tipo de dato del id de long a Long -> Se cambió el
 * tipo de dato del atributo ofertaInmobiliariaId de BigDecimal a Long -> Se cambió el tipo de dato
 * del atributo secuencia de BigDecimal a Long
 *
 * Este entity sirve para almacenar temporalmente los id's de las ofertas inmobiliarias que sean
 * seleccionadas en las distintas pantallas de la aplicación, para luego consultarlas desde el
 * reporte, por cada vez que se seleccionen un grupo de ofertas se guardan los registros con una
 * secuencia respectiva, al terminar de generar el reporte se eliminan estos datos
 */
@Entity
@Table(name = "GTT_OFERTA_INMOBILIARIA")
public class GttOfertaInmobiliaria implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long ofertaInmobiliariaId;
    private Long secuencia;

    public GttOfertaInmobiliaria() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GTT_OFERTA_INMOBILIARIA_ID_SEQ")
    @SequenceGenerator(name = "GTT_OFERTA_INMOBILIARIA_ID_SEQ", sequenceName =
        "GTT_OFERTA_INMOBILIARIA_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "OFERTA_INMOBILIARIA_ID")
    public Long getOfertaInmobiliariaId() {
        return this.ofertaInmobiliariaId;
    }

    public void setOfertaInmobiliariaId(Long ofertaInmobiliariaId) {
        this.ofertaInmobiliariaId = ofertaInmobiliariaId;
    }

    @Column(name = "SECUENCIA", precision = 10, scale = 0)
    public Long getSecuencia() {
        return this.secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

}
