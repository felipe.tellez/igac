package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.snc.persistence.util.IProyeccionObject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the P_FM_CONSTRUCCION_COMPONENTE database table.
 *
 * @modified david.cifuentes :: Adición de la secuencia FM_CONSTRUCCION_COMPONE_ID_SEQ :: 26/04/2012
 */
@Entity
@Table(name = "P_FM_CONSTRUCCION_COMPONENTE")
public class PFmConstruccionComponente implements Serializable,
    IProyeccionObject, IModeloUnidadConstruccionComponente {

    private static final long serialVersionUID = 1L;
    private Long id;
    private PFmModeloConstruccion PFmModeloConstruccion;
    private String componente;
    private String elementoCalificacion;
    private String detalleCalificacion;
    private Double puntos;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;

    public PFmConstruccionComponente() {
    }

    @Override
    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FM_CONSTRUCCION_COMPONE_ID_SEQ")
    @SequenceGenerator(name = "FM_CONSTRUCCION_COMPONE_ID_SEQ", sequenceName =
        "FM_CONSTRUCCION_COMPONE_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    @Override
    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Override
    public String getComponente() {
        return this.componente;
    }

    @Override
    public void setComponente(String componente) {
        this.componente = componente;
    }

    @Override
    @Column(name = "DETALLE_CALIFICACION")
    public String getDetalleCalificacion() {
        return this.detalleCalificacion;
    }

    @Override
    public void setDetalleCalificacion(String detalleCalificacion) {
        this.detalleCalificacion = detalleCalificacion;
    }

    @Override
    @Column(name = "ELEMENTO_CALIFICACION")
    public String getElementoCalificacion() {
        return this.elementoCalificacion;
    }

    @Override
    public void setElementoCalificacion(String elementoCalificacion) {
        this.elementoCalificacion = elementoCalificacion;
    }

    @Override
    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    @Override
    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Override
    public Double getPuntos() {
        return this.puntos;
    }

    @Override
    public void setPuntos(Double puntos) {
        this.puntos = puntos;
    }

    @Override
    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    @Override
    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to PFmModeloConstruccion
    @ManyToOne
    @JoinColumn(name = "FM_MODELO_CONSTRUCCION_ID")
    public PFmModeloConstruccion getPFmModeloConstruccion() {
        return this.PFmModeloConstruccion;
    }

    public void setPFmModeloConstruccion(
        PFmModeloConstruccion PFmModeloConstruccion) {
        this.PFmModeloConstruccion = PFmModeloConstruccion;
    }

    @Override
    @Transient
    public IModeloUnidadConstruccion getPModeloUnidadConstruccion() {
        return new PFichaMatrizModeloUnidadConstruccion(
            this.PFmModeloConstruccion, null);
    }

    @Override
    @Transient
    public void setPModeloUnidadConstruccion(
        IModeloUnidadConstruccion PUnidadConstruccion) {
        // TODO Auto-generated method stub

    }
}
