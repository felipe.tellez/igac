package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.IInscripcionCatastral;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * PPredioServidumbre entity. @author MyEclipse Persistence Tools
 *
 * @author fabio.navarrete Se agrega serialVersionUID
 * @author fabio.navarrete Se reemplazan los tipos de dato Timestamp
 * @author fabio.navarrete Se agregó el método y atributo transient para consultar el valor de la
 * propiedad cancelaInscribe
 * @author javier.aponte Se agregó la anotación @GeneratedValue y
 * @SequenceGenerator para el atributo id
 */
@Entity
@Table(name = "P_PREDIO_SERVIDUMBRE", schema = "SNC_CONSERVACION")
public class PPredioServidumbre implements java.io.Serializable, IProyeccionObject,
    IInscripcionCatastral, Cloneable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = 2359728343526099911L;

    private Long id;
    private PPredio PPredio;
    private String servidumbre;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;
    private Date fechaInscripcionCatastral;
    private String cancelaInscribeValor;

    // Constructors
    /** default constructor */
    public PPredioServidumbre() {
    }

    /** minimal constructor */
    public PPredioServidumbre(Long id, PPredio PPredio, String servidumbre,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.servidumbre = servidumbre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PPredioServidumbre(Long id, PPredio PPredio, String servidumbre,
        Date fechaInscripcionCatastral,
        String cancelaInscribe, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.servidumbre = servidumbre;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PREDIO_SERVIDUMBRE_ID_SEQ")
    @SequenceGenerator(name = "PREDIO_SERVIDUMBRE_ID_SEQ", sequenceName =
        "PREDIO_SERVIDUMBRE_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    @Column(name = "SERVIDUMBRE", nullable = false, length = 30)
    public String getServidumbre() {
        return this.servidumbre;
    }

    public void setServidumbre(String servidumbre) {
        this.servidumbre = servidumbre;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    /**
     * @author fabio.navarrete
     * @return
     */
    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    /**
     * @author fabio.navarrete
     * @param cancelaInscribeValor
     */
    @Transient
    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    /**
     * @author leidy.gonzalez Método que realiza un clone del objeto pPredioServidumbre.
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

}
