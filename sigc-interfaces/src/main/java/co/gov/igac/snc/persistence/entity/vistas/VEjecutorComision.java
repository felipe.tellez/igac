package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the V_EJECUTOR_COMISION database table.
 */
/*
 * @modified by pedro.garcia what: - adición de anotación del id - cambio de tipo de dato para los
 * atributos id y tramites: del BigDecimal a Long - adición de namedQueries - cambio de tipo de dato
 * para el atributo duración: del BigDecimal a Double
 *
 * N: 'numero' es el numero de comisión 'tramites' es el número de trámites 'duracion' duración de
 * los trámites de ese ejecutor para esa comisión
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "findByNumeroComision",
        query = "from VEjecutorComision ec where ec.numero = :numComisionP")
})
@Table(name = "V_EJECUTOR_COMISION")
public class VEjecutorComision implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * es la suma de las duraciones de los trámites del ejecutor 'idFuncionarioEjecutor' en la
     * comisión 'id'
     */
    private Double duracion;

    /**
     * corresponde al id de la comisión
     */
    private Long id;
    private String municipio;
    private String nombreFuncionarioEjecutor;
    private String idFuncionarioEjecutor;

    /**
     * corresponde al número de la comisión
     */
    private String numero;
    private Long tramites;

    private VEjecutorComisionPK idVEC;

    //--------------------      methods   ---------
    public VEjecutorComision() {
    }

    @EmbeddedId
    public VEjecutorComisionPK getIdVEC() {
        return this.idVEC;
    }

    public void setIdVEC(VEjecutorComisionPK idVEC) {
        this.idVEC = idVEC;
    }

    public Double getDuracion() {
        return this.duracion;
    }

    public void setDuracion(Double duracion) {
        this.duracion = duracion;
    }

    @Column(name = "ID", insertable = false, updatable = false)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "MUNICIPIO", insertable = false, updatable = false)
    public String getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    @Column(name = "NOMBRE_FUNCIONARIO_EJECUTOR")
    public String getNombreFuncionarioEjecutor() {
        return this.nombreFuncionarioEjecutor;
    }

    public void setNombreFuncionarioEjecutor(String nombreFuncionarioEjecutor) {
        this.nombreFuncionarioEjecutor = nombreFuncionarioEjecutor;
    }

    @Column(name = "ID_FUNCIONARIO_EJECUTOR", insertable = false, updatable = false)
    public String getIdFuncionarioEjecutor() {
        return this.idFuncionarioEjecutor;
    }

    public void setIdFuncionarioEjecutor(String idFuncionarioEjecutor) {
        this.idFuncionarioEjecutor = idFuncionarioEjecutor;
    }

    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getTramites() {
        return this.tramites;
    }

    public void setTramites(Long tramites) {
        this.tramites = tramites;
    }

}
