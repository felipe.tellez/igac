package co.gov.igac.snc.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MunicipioManzanaDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1390218831495919541L;

    private String divisionAdministrativa;

    private List<TotalManzanaDTO> datos = new ArrayList<TotalManzanaDTO>();

    public MunicipioManzanaDTO(String divisionAdministrativa) {
        super();
        this.divisionAdministrativa = divisionAdministrativa;
    }

    public String getDivisionAdministrativa() {
        return divisionAdministrativa;
    }

    public void setDivisionAdministrativa(String divisionAdministrativa) {
        this.divisionAdministrativa = divisionAdministrativa;
    }

    public List<TotalManzanaDTO> getDatos() {
        return datos;
    }

    public TotalManzanaDTO getDato(String zona) {
        for (TotalManzanaDTO d : this.datos) {
            if (d.getZona().equals(zona)) {
                return d;
            }
        }
        return null;
    }

    public void setDatos(List<TotalManzanaDTO> datos) {
        this.datos = datos;
    }

    public BigDecimal getTotalCantidadPredios() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getCantidadPredios());
        }
        return p;
    }

    public BigDecimal getTotalCantidadPropietarios() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getCantidadPropietarios());
        }
        return p;
    }

    public BigDecimal getTotalAreaTerreno() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getAreaTerreno());
        }
        return p;
    }

    public BigDecimal getTotalAreaConstruccion() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getAreaConstruccion());
        }
        return p;
    }

    public BigDecimal getTotalAvaluos() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getValorAvaluo());
        }
        return p;
    }

    public BigDecimal getTotalCantidadManzanasVeredas() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getCantidadManzanasVeredas());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion0() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion0());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion1() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion1());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion2() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion2());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion3() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion3());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion4() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion4());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion5() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion5());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion6() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion6());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion7() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion7());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion8() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion8());
        }
        return p;
    }

    public BigDecimal getTotalPrediosCondicion9() {
        BigDecimal p = new BigDecimal(0);
        for (TotalManzanaDTO d : this.datos) {
            p = p.add(d.getPrediosCondicion9());
        }
        return p;
    }

}
