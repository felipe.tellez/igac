package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the TRAMITE_DETALLE_PREDIO database table.
 *
 */
/*
 * @modified pedro.garcia 16-08-2012 Cambio de predioId por Predio
 */
@Entity
@NamedQueries({
    @NamedQuery(
        name = "findTramiteDetallePredioByTramite",
        query = "from TramiteDetallePredio tp where tp.tramite= :tramite")})
@Table(name = "TRAMITE_DETALLE_PREDIO", schema = "SNC_TRAMITE")
public class TramiteDetallePredio implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String englobePrincipal;
    private Date fechaLog;
    private Predio predio;
    private String usuarioLog;
    private Tramite tramite;

    public TramiteDetallePredio() {
    }

    /**
     * Constructor para homologar las entidades TramiteDetallePredio y TramitePredioEnglobe
     *
     * @author franz.gamba
     * @param tpe
     */
    public TramiteDetallePredio(TramitePredioEnglobe tpe) {
        this.id = tpe.getId();
        this.englobePrincipal = tpe.getEnglobePrincipal();
        this.fechaLog = tpe.getFechaLog();
        this.predio = tpe.getPredio();
        this.usuarioLog = tpe.getUsuarioLog();
        this.tramite = tpe.getTramite();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_DETALLE_PREDIO_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_DETALLE_PREDIO_ID_SEQ", sequenceName =
        "TRAMITE_DETALLE_PREDIO_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "ENGLOBE_PRINCIPAL", nullable = false, length = 2)
    public String getEnglobePrincipal() {
        return this.englobePrincipal;
    }

    public void setEnglobePrincipal(String principal) {
        this.englobePrincipal = principal;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
