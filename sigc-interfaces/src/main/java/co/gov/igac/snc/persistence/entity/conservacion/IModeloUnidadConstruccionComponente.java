package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;

import javax.persistence.Transient;

public interface IModeloUnidadConstruccionComponente {

    // Property accessors
    public abstract Long getId();

    public abstract void setId(Long id);

    @Transient
    public abstract IModeloUnidadConstruccion getPModeloUnidadConstruccion();

    @Transient
    public abstract void setPModeloUnidadConstruccion(
        IModeloUnidadConstruccion PUnidadConstruccion);

    public abstract String getComponente();

    public abstract void setComponente(String componente);

    public abstract String getElementoCalificacion();

    public abstract void setElementoCalificacion(String elementoCalificacion);

    public abstract String getDetalleCalificacion();

    public abstract void setDetalleCalificacion(String detalleCalificacion);

    public abstract Double getPuntos();

    public abstract void setPuntos(Double puntos);

    public abstract String getCancelaInscribe();

    public abstract void setCancelaInscribe(String cancelaInscribe);

    public abstract String getUsuarioLog();

    public abstract void setUsuarioLog(String usuarioLog);

    public abstract Date getFechaLog();

    public abstract void setFechaLog(Date fechaLog);

}
