package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * Objeto utilizado para agrupar los datos usados para realizar busquedas de ....
 *
 * @author christian.rodriguez
 */
public class FiltroDatosConsultaControlCalidad implements Serializable {

    private static final long serialVersionUID = 1782269734324639678L;

    private String secRadicado;
    private String departamentoCodigo;
    private String municipioCodigo;
    private String numeroPredial;
    private String tipoAvaluo;
    private String tipoTramite;
    private String estadoTramite;
    private String nombreSolicitante;
    private String avaluadorId;
    private String profesionalCalidadId;

    public String getSecRadicado() {
        return this.secRadicado;
    }

    public void setSecRadicado(String secRadicado) {
        this.secRadicado = secRadicado;
    }

    public String getDepartamentoCodigo() {
        return this.departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String muicipioCodigo) {
        this.municipioCodigo = muicipioCodigo;
    }

    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getTipoAvaluo() {
        return this.tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getEstadoTramite() {
        return this.estadoTramite;
    }

    public void setEstadoTramite(String estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    public String getNombreSolicitante() {
        return this.nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getAvaluadorId() {
        return this.avaluadorId;
    }

    public void setAvaluadorId(String avaluadorId) {
        this.avaluadorId = avaluadorId;
    }

    public String getProfesionalCalidadId() {
        return this.profesionalCalidadId;
    }

    public void setProfesionalCalidadId(String profesionalCalidadId) {
        this.profesionalCalidadId = profesionalCalidadId;
    }

}
