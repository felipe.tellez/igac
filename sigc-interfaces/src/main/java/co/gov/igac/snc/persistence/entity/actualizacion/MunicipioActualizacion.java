/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.persistence.entity.actualizacion;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 * Entidad relacionada a los procesos de actualaizacion por municipio
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "AX_MUNICIPIO_ACTUALIZACION")
public class MunicipioActualizacion implements Serializable {

    private static final long serialVersionUID = -2219934966617987967L;
    private Long id;
    private Departamento departamento;
    private Municipio municipio;
    private String archivoAlfanumerico;
    private String archivoGeografico;
    private String dirDocsSoporte;
    private String estado;
    private String activo;
    private Solicitud solicitud;
    private Date fechaLog;
    private Date fechaInicio;
    private Date fechaFin;
    private String usuarioLog;
    private String observaciones;
    private Long predios;
    private Integer vigencia;

    //tramites asociados a la solicitud del proceso de actualizacion
    private List<Tramite> tramitesAsociados;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AX_MUNICIPIO_ACT_ID_SEQ")
    @SequenceGenerator(name = "AX_MUNICIPIO_ACT_ID_SEQ", sequenceName = "AX_MUNICIPIO_ACT_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "UBICACION_ALFANUMERICO")
    public String getArchivoAlfanumerico() {
        return archivoAlfanumerico;
    }

    public void setArchivoAlfanumerico(String archivoAlfanumerico) {
        this.archivoAlfanumerico = archivoAlfanumerico;
    }

    @Column(name = "UBICACION_GEOGRAFICO")
    public String getArchivoGeografico() {
        return archivoGeografico;
    }

    public void setArchivoGeografico(String archivoGeografico) {
        this.archivoGeografico = archivoGeografico;
    }

    @Column(name = "UBICACION_DOCUMENTOS")
    public String getDirDocsSoporte() {
        return dirDocsSoporte;
    }

    public void setDirDocsSoporte(String dirDocsSoporte) {
        this.dirDocsSoporte = dirDocsSoporte;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_ID")
    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @Column(name = "OBSERVACIONES")
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "FECHA_FIN")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Column(name = "FECHA_INICIO")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "PREDIOS")
    public Long getPredios() {
        return predios;
    }

    public void setPredios(Long predios) {
        this.predios = predios;
    }

    @Column(name = "VIGENCIA")
    public Integer getVigencia() {
        return vigencia;
    }

    public void setVigencia(Integer vigencia) {
        this.vigencia = vigencia;
    }

    @Transient
    public List<Tramite> getTramitesAsociados() {
        return tramitesAsociados;
    }

    public void setTramitesAsociados(List<Tramite> tramitesAsociados) {
        this.tramitesAsociados = tramitesAsociados;
    }

}
