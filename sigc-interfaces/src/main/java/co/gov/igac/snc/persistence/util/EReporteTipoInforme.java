/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;

/**
 * Enumeración con los tipos de inforque asociados a un {@link RepReporte}
 *
 * @author david.cifuentes
 */
public enum EReporteTipoInforme {

    CONSOLIDADO("Consolidado"),
    DETALLADO("Detallado");

    private String nombre;

    private EReporteTipoInforme(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

}
