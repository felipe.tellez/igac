package co.gov.igac.snc.persistence.util;

public enum EDocumentoEstado {

    //D: valor ("codigo")
    CARGADO("2"),
    MIGRADO("7"),
    RECIBIDO("1"),
    RECIBIDO_PRUEBAS("6"),
    RESOLUCION_EN_FIRME("9"),
    RESOLUCION_EN_PROYECCION("10"),
    SIN_CARGAR("3"),
    SIN_REGISTRAR_SOLICITUD("4"),
    SIN_VALIDAR_RESOLUCION("8"),
    SOLICITUD_DOCS_REGISTRADA("5");

    private String codigo;

    EDocumentoEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
