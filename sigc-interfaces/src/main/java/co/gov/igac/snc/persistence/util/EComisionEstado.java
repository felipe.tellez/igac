/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los estados de una comisión, correspondientes al dominio COMISION_ESTADO
 *
 * @author pedro.garcia
 */
public enum EComisionEstado {

    //D: código (valor)
    APLAZADA("Aplazada"),
    CANCELADA("Cancelada"),
    CREADA("Creada"),
    EN_EJECUCION("En ejecución"),
    FINALIZADA("Finalizada"),
    POR_APROBAR("Por aprobar"),
    //D: nuevos estados intermedios entre la acción de administración del responsable de conservación
    //  y la del director territorial
    POR_APROBAR_AMPLIACION("Por aprobar ampliación"),
    POR_APROBAR_APLAZAMIENTO("Por aprobar aplazamiento"),
    POR_APROBAR_CANCELACION("Por aprobar cancelación"),
    POR_APROBAR_SUSPENSION("Por aprobar suspensión"),
    POR_APROBAR_REACTIVACION("Por aprobar reactivación"),
    POR_EJECUTAR("Por ejecutar"),
    SUSPENDIDA("Suspendida");

    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private EComisionEstado(String valor) {
        this.valor = valor;
    }

    public String getCodigo() {
        return this.toString();
    }

}
