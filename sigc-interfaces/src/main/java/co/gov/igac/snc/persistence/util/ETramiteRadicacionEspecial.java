package co.gov.igac.snc.persistence.util;

/**
 * Dominio de tramites de inscripcion
 *
 * @author felipe.cadena
 */
public enum ETramiteRadicacionEspecial {

    POR_ETAPAS("POR_ETAPAS"),
    QUINTA_MASIVO("QUINTA_MASIVO"),
    RECTIFICACION_MATRIZ("RECTIFICACION_MATRIZ"),
    TERCERA_MASIVA("TERCERA_MASIVA"),
    CANCELACION_MASIVA("CANCELACION_MASIVA"),
    ENGLOBE_PH("ENGLOBE PH");

    private final String codigo;

    private ETramiteRadicacionEspecial(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
