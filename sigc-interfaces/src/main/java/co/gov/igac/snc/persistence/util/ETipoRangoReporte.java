/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * @author felipe.cadena
 */
public enum ETipoRangoReporte {

    SUPERFICIE("SUPERFICIE"),
    AVALUO("AVALUO");

    private final String codigo;

    private ETipoRangoReporte(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
