package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the LEVANTAMIENTO_ASIGNACION_INF database table.
 *
 */
@Entity
@Table(name = "LEVANTAMIENTO_ASIGNACION_INF")
public class LevantamientoAsignacionInf implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private String inconvenientesAdministrativos;
    private String inconvenientesHumanos;
    private String inconvenientesLogisticos;
    private String inconvenientesSociales;
    private String inconvenientesTecnicos;
    private String resumenActividades;
    private String tiempos;
    private String trabajos;
    private String usuarioLog;
    private LevantamientoAsignacion levantamientoAsignacion;

    public LevantamientoAsignacionInf() {
    }

    @Id
    @SequenceGenerator(name = "LEVANTAMIENTO_ASIGN_INF_ID_GENERATOR", sequenceName =
        "LEVANTAMIENTO_ASIGN_INF_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "LEVANTAMIENTO_ASIGN_INF_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "INCONVENIENTES_ADMINISTRATIVOS", length = 600)
    public String getInconvenientesAdministrativos() {
        return this.inconvenientesAdministrativos;
    }

    public void setInconvenientesAdministrativos(
        String inconvenientesAdministrativos) {
        this.inconvenientesAdministrativos = inconvenientesAdministrativos;
    }

    @Column(name = "INCONVENIENTES_HUMANOS", length = 600)
    public String getInconvenientesHumanos() {
        return this.inconvenientesHumanos;
    }

    public void setInconvenientesHumanos(String inconvenientesHumanos) {
        this.inconvenientesHumanos = inconvenientesHumanos;
    }

    @Column(name = "INCONVENIENTES_LOGISTICOS", length = 600)
    public String getInconvenientesLogisticos() {
        return this.inconvenientesLogisticos;
    }

    public void setInconvenientesLogisticos(String inconvenientesLogisticos) {
        this.inconvenientesLogisticos = inconvenientesLogisticos;
    }

    @Column(name = "INCONVENIENTES_SOCIALES", length = 600)
    public String getInconvenientesSociales() {
        return this.inconvenientesSociales;
    }

    public void setInconvenientesSociales(String inconvenientesSociales) {
        this.inconvenientesSociales = inconvenientesSociales;
    }

    @Column(name = "INCONVENIENTES_TECNICOS", length = 600)
    public String getInconvenientesTecnicos() {
        return this.inconvenientesTecnicos;
    }

    public void setInconvenientesTecnicos(String inconvenientesTecnicos) {
        this.inconvenientesTecnicos = inconvenientesTecnicos;
    }

    @Column(name = "RESUMEN_ACTIVIDADES", length = 1000)
    public String getResumenActividades() {
        return this.resumenActividades;
    }

    public void setResumenActividades(String resumenActividades) {
        this.resumenActividades = resumenActividades;
    }

    @Column(length = 600)
    public String getTiempos() {
        return this.tiempos;
    }

    public void setTiempos(String tiempos) {
        this.tiempos = tiempos;
    }

    @Column(length = 1000)
    public String getTrabajos() {
        return this.trabajos;
    }

    public void setTrabajos(String trabajos) {
        this.trabajos = trabajos;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to LevantamientoAsignacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LEVANTAMIENTO_ASIGNACION_ID", nullable = false)
    public LevantamientoAsignacion getLevantamientoAsignacion() {
        return this.levantamientoAsignacion;
    }

    public void setLevantamientoAsignacion(
        LevantamientoAsignacion levantamientoAsignacion) {
        this.levantamientoAsignacion = levantamientoAsignacion;
    }

}
