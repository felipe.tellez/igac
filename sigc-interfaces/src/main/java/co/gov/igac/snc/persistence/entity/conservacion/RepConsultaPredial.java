package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the REP_CONSULTA_PREDIAL database table.
 *
 */
@Entity
@Table(name = "REP_CONSULTA_PREDIAL")
public class RepConsultaPredial implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String departamento;
    private String fechaCorte;
    private String anio;
    private String numeroPredialManzana;
    private String numeroPredial;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String propietarios;
    private String sigla;
    private String porcParticipacion;
    private String modoAdquisicion;
    private Long repReporte;
    private String valorCompra;
    private String tipoTitulo;
    private String entidadEmisora;
    private String numeroTitulo;
    private String fechaTitulo;
    private String fechaRegistro;
    private String direccion;
    private String nombrePhCondominio;
    private String matriculaInmobiliaria;
    private String tipoPredio;
    private String tipificacion;
    private String desinoEconomico;
    private String areaTerreno;
    private String areaTerrenoComun;
    private String areaTerrenoPrivada;
    private String avaluoTerreno;
    private String coeficienteCopropiedad;
    private String zonaFisica;
    private String zonaGeoeconomica;
    private String areaZona;
    private String avaluoZona;
    private String usoConstruccion;
    private String areaConstruida;
    private String areaConstruidaComun;
    private String areaConstruidaPrivada;
    private String avaluoAreaConstruccion;
    private String numeroHabitaciones;
    private String numeroBanios;
    private String numeroLocales;
    private String numeroPisos;
    private String puntaje;
    private String anioConstruccion;
    private String anioAutoestimacion;
    private String avaluoTotal;
    private String totalTorres;
    private String totalPisoTorre;
    private String totalUnidadesPrivadas;
    private String totalSotanos;
    private String totalUnidadesSotanos;
    private String fechaInicioBloqueo;
    private String fechaDesbloqueo;
    private String entidad;
    private String municipio;

    public RepConsultaPredial() {
    }

    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "REPORTE_ID", length = 10)
    public Long getRepReporte() {
        return this.repReporte;
    }

    public void setRepReporte(Long repReporte) {
        this.repReporte = repReporte;
    }

    @Column(name = "DEPARTAMENTO")
    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @Column(name = "FECHA_CORTE")
    public String getFechaCorte() {
        return fechaCorte;
    }

    public void setFechaCorte(String fechaCorte) {
        this.fechaCorte = fechaCorte;
    }

    @Column(name = "ANIO")
    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    @Column(name = "NUMERO_PREDIAL_MANZANA")
    public String getNumeroPredialManzana() {
        return numeroPredialManzana;
    }

    public void setNumeroPredialManzana(String numeroPredialManzana) {
        this.numeroPredialManzana = numeroPredialManzana;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "TIPO_IDENTIFICACION")
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "NUMERO_IDENTIFICACION")
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "PROPIETARIOS")
    public String getPropietarios() {
        return propietarios;
    }

    public void setPropietarios(String propietarios) {
        this.propietarios = propietarios;
    }

    @Column(name = "SIGLA")
    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Column(name = "PORC_PARTICIPACION")
    public String getPorcParticipacion() {
        return porcParticipacion;
    }

    public void setPorcParticipacion(String porcParticipacion) {
        this.porcParticipacion = porcParticipacion;
    }

    @Column(name = "MODO_ADQUISICION")
    public String getModoAdquisicion() {
        return modoAdquisicion;
    }

    public void setModoAdquisicion(String modoAdquisicion) {
        this.modoAdquisicion = modoAdquisicion;
    }

    @Column(name = "VALOR_COMPRA")
    public String getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(String valorCompra) {
        this.valorCompra = valorCompra;
    }

    @Column(name = "TIPO_TITULO")
    public String getTipoTitulo() {
        return tipoTitulo;
    }

    public void setTipoTitulo(String tipoTitulo) {
        this.tipoTitulo = tipoTitulo;
    }

    @Column(name = "ENTIDAD_EMISORA")
    public String getEntidadEmisora() {
        return entidadEmisora;
    }

    public void setEntidadEmisora(String entidadEmisora) {
        this.entidadEmisora = entidadEmisora;
    }

    @Column(name = "NUMERO_TITULO")
    public String getNumeroTitulo() {
        return numeroTitulo;
    }

    public void setNumeroTitulo(String numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }

    @Column(name = "FECHA_TITULO")
    public String getFechaTitulo() {
        return fechaTitulo;
    }

    public void setFechaTitulo(String fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }

    @Column(name = "FECHA_REGISTRO")
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Column(name = "DIRECCION")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "NOMBRE_PH_CONDOMINIO")
    public String getNombrePhCondominio() {
        return nombrePhCondominio;
    }

    public void setNombrePhCondominio(String nombrePhCondominio) {
        this.nombrePhCondominio = nombrePhCondominio;
    }

    @Column(name = "MATRICULA_INMOBILIARIA")
    public String getMatriculaInmobiliaria() {
        return matriculaInmobiliaria;
    }

    public void setMatriculaInmobiliaria(String matriculaInmobiliaria) {
        this.matriculaInmobiliaria = matriculaInmobiliaria;
    }

    @Column(name = "TIPO_PREDIO")
    public String getTipoPredio() {
        return tipoPredio;
    }

    public void setTipoPredio(String tipoPredio) {
        this.tipoPredio = tipoPredio;
    }

    @Column(name = "TIPIFICACION")
    public String getTipificacion() {
        return tipificacion;
    }

    public void setTipificacion(String tipificacion) {
        this.tipificacion = tipificacion;
    }

    @Column(name = "DESTINO_ECONOMICO")
    public String getDesinoEconomico() {
        return desinoEconomico;
    }

    public void setDesinoEconomico(String desinoEconomico) {
        this.desinoEconomico = desinoEconomico;
    }

    @Column(name = "AREA_TERRENO")
    public String getAreaTerreno() {
        return areaTerreno;
    }

    public void setAreaTerreno(String areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "AREA_TERRENO_COMUN")
    public String getAreaTerrenoComun() {
        return areaTerrenoComun;
    }

    public void setAreaTerrenoComun(String areaTerrenoComun) {
        this.areaTerrenoComun = areaTerrenoComun;
    }

    @Column(name = "AREA_TERRENO_PRIVADA")
    public String getAreaTerrenoPrivada() {
        return areaTerrenoPrivada;
    }

    public void setAreaTerrenoPrivada(String areaTerrenoPrivada) {
        this.areaTerrenoPrivada = areaTerrenoPrivada;
    }

    @Column(name = "AVALUO_TERRENO")
    public String getAvaluoTerreno() {
        return avaluoTerreno;
    }

    public void setAvaluoTerreno(String avaluoTerreno) {
        this.avaluoTerreno = avaluoTerreno;
    }

    @Column(name = "COEFICIENTE_COPROPIEDAD")
    public String getCoeficienteCopropiedad() {
        return coeficienteCopropiedad;
    }

    public void setCoeficienteCopropiedad(String coeficienteCopropiedad) {
        this.coeficienteCopropiedad = coeficienteCopropiedad;
    }

    @Column(name = "ZONA_FISICA")
    public String getZonaFisica() {
        return zonaFisica;
    }

    public void setZonaFisica(String zonaFisica) {
        this.zonaFisica = zonaFisica;
    }

    @Column(name = "ZONA_GEOECONOMICA")
    public String getZonaGeoeconomica() {
        return zonaGeoeconomica;
    }

    public void setZonaGeoeconomica(String zonaGeoeconomica) {
        this.zonaGeoeconomica = zonaGeoeconomica;
    }

    @Column(name = "AREA_ZONA")
    public String getAreaZona() {
        return areaZona;
    }

    public void setAreaZona(String areaZona) {
        this.areaZona = areaZona;
    }

    @Column(name = "AVALUO_ZONA")
    public String getAvaluoZona() {
        return avaluoZona;
    }

    public void setAvaluoZona(String avaluoZona) {
        this.avaluoZona = avaluoZona;
    }

    @Column(name = "USO_CONSTRUCCION")
    public String getUsoConstruccion() {
        return usoConstruccion;
    }

    public void setUsoConstruccion(String usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    @Column(name = "AREA_CONSTRUIDA")
    public String getAreaConstruida() {
        return areaConstruida;
    }

    public void setAreaConstruida(String areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "AREA_CONSTRUIDA_COMUN")
    public String getAreaConstruidaComun() {
        return areaConstruidaComun;
    }

    public void setAreaConstruidaComun(String areaConstruidaComun) {
        this.areaConstruidaComun = areaConstruidaComun;
    }

    @Column(name = "AREA_CONSTRUIDA_PRIVADA")
    public String getAreaConstruidaPrivada() {
        return areaConstruidaPrivada;
    }

    public void setAreaConstruidaPrivada(String areaConstruidaPrivada) {
        this.areaConstruidaPrivada = areaConstruidaPrivada;
    }

    @Column(name = "AVALUO_AREA_CONSTRUCCION")
    public String getAvaluoAreaConstruccion() {
        return avaluoAreaConstruccion;
    }

    public void setAvaluoAreaConstruccion(String avaluoAreaConstruccion) {
        this.avaluoAreaConstruccion = avaluoAreaConstruccion;
    }

    @Column(name = "NUMERO_HABITACIONES")
    public String getNumeroHabitaciones() {
        return numeroHabitaciones;
    }

    public void setNumeroHabitaciones(String numeroHabitaciones) {
        this.numeroHabitaciones = numeroHabitaciones;
    }

    @Column(name = "NUMERO_BANIOS")
    public String getNumeroBanios() {
        return numeroBanios;
    }

    public void setNumeroBanios(String numeroBanios) {
        this.numeroBanios = numeroBanios;
    }

    @Column(name = "NUMERO_LOCALES")
    public String getNumeroLocales() {
        return numeroLocales;
    }

    public void setNumeroLocales(String numeroLocales) {
        this.numeroLocales = numeroLocales;
    }

    @Column(name = "NUMERO_PISOS")
    public String getNumeroPisos() {
        return numeroPisos;
    }

    public void setNumeroPisos(String numeroPisos) {
        this.numeroPisos = numeroPisos;
    }

    @Column(name = "PUNTAJE")
    public String getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(String puntaje) {
        this.puntaje = puntaje;
    }

    @Column(name = "ANIO_CONSTRUCCION")
    public String getAnioConstruccion() {
        return anioConstruccion;
    }

    public void setAnioConstruccion(String anioConstruccion) {
        this.anioConstruccion = anioConstruccion;
    }

    @Column(name = "ANIO_AUTOESTIMACION")
    public String getAnioAutoestimacion() {
        return anioAutoestimacion;
    }

    public void setAnioAutoestimacion(String anioAutoestimacion) {
        this.anioAutoestimacion = anioAutoestimacion;
    }

    @Column(name = "AVALUO_TOTAL")
    public String getAvaluoTotal() {
        return avaluoTotal;
    }

    public void setAvaluoTotal(String avaluoTotal) {
        this.avaluoTotal = avaluoTotal;
    }

    @Column(name = "TOTAL_TORRES")
    public String getTotalTorres() {
        return totalTorres;
    }

    public void setTotalTorres(String totalTorres) {
        this.totalTorres = totalTorres;
    }

    @Column(name = "TOTAL_PISOS_TORRE")
    public String getTotalPisoTorre() {
        return totalPisoTorre;
    }

    public void setTotalPisoTorre(String totalPisoTorre) {
        this.totalPisoTorre = totalPisoTorre;
    }

    @Column(name = "TOTAL_UNIDADES_PRIVADAS")
    public String getTotalUnidadesPrivadas() {
        return totalUnidadesPrivadas;
    }

    public void setTotalUnidadesPrivadas(String totalUnidadesPrivadas) {
        this.totalUnidadesPrivadas = totalUnidadesPrivadas;
    }

    @Column(name = "TOTAL_SOTANOS")
    public String getTotalSotanos() {
        return totalSotanos;
    }

    public void setTotalSotanos(String totalSotanos) {
        this.totalSotanos = totalSotanos;
    }

    @Column(name = "TOTAL_UNIDADES_SOTANOS")
    public String getTotalUnidadesSotanos() {
        return totalUnidadesSotanos;
    }

    public void setTotalUnidadesSotanos(String totalUnidadesSotanos) {
        this.totalUnidadesSotanos = totalUnidadesSotanos;
    }

    @Column(name = "FECHA_INICIO_BLOQUEO")
    public String getFechaInicioBloqueo() {
        return fechaInicioBloqueo;
    }

    public void setFechaInicioBloqueo(String fechaInicioBloqueo) {
        this.fechaInicioBloqueo = fechaInicioBloqueo;
    }

    @Column(name = "FECHA_DESBLOQUEO")
    public String getFechaDesbloqueo() {
        return fechaDesbloqueo;
    }

    public void setFechaDesbloqueo(String fechaDesbloqueo) {
        this.fechaDesbloqueo = fechaDesbloqueo;
    }

    @Column(name = "ENTIDAD")
    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    @Column(name = "MUNICIPIO")
    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

}
