package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;

/**
 * The persistent class for the SOLICITUD_PREDIO database table.
 *
 */
@Entity
@Table(name = "SOLICITUD_PREDIO", schema = "SNC_TRAMITE")
public class SolicitudPredio implements Serializable {

    private static final long serialVersionUID = -5304326680343740383L;

    private Long id;
    private Long anio;
    private Double areaConstruccion;
    private Double areaTerreno;
    private Long avaluoPredioId;
    private String circuloRegistral;
    private String condicionJuridica;
    private SolicitanteSolicitud contacto;
    private String destino;
    private String direccion;
    private Date fechaLog;
    private String libro;
    private String numeroPredial;
    private String numeroRegistro;
    private String numeroRegistroAnterior;
    private String oficina;
    private String pagina;
    private Long predioId;

    private String tipoAvaluoPredio;
    private String tipoInmueble;
    private String tomo;
    private String usuarioLog;
    private Solicitud solicitud;

    private Municipio municipio;
    private Departamento departamento;

    public SolicitudPredio() {
    }

    @Id
    @SequenceGenerator(name = "SOLICITUD_PREDIO_ID_GENERATOR", sequenceName =
        "SOLICITUD_PREDIO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SOLICITUD_PREDIO_ID_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ANIO", precision = 4)
    public Long getAnio() {
        return this.anio;
    }

    public void setAnio(Long anio) {
        this.anio = anio;
    }

    @Column(name = "AREA_CONSTRUCCION", nullable = false, precision = 18, scale = 2)
    public Double getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "AREA_TERRENO", nullable = false, precision = 18, scale = 2)
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "AVALUO_PREDIO_ID", precision = 10)
    public Long getAvaluoPredioId() {
        return this.avaluoPredioId;
    }

    public void setAvaluoPredioId(Long avaluoPredioId) {
        this.avaluoPredioId = avaluoPredioId;
    }

    @Column(name = "CIRCULO_REGISTRAL", length = 10)
    public String getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(String circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "CONDICION_JURIDICA", length = 30)
    public String getCondicionJuridica() {
        return this.condicionJuridica;
    }

    public void setCondicionJuridica(String condicionJuridica) {
        this.condicionJuridica = condicionJuridica;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTA_SOLICITANTE_SOLICITUD_ID")
    public SolicitanteSolicitud getContacto() {
        return this.contacto;
    }

    public void setContacto(SolicitanteSolicitud contacto) {
        this.contacto = contacto;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO")
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @Column(name = "DESTINO", length = 30)
    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Column(name = "DIRECCION", nullable = false, length = 250)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "LIBRO", length = 20)
    public String getLibro() {
        return this.libro;
    }

    public void setLibro(String libro) {
        this.libro = libro;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO")
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "NUMERO_PREDIAL", nullable = false, length = 30)
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_REGISTRO", length = 20)
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "NUMERO_REGISTRO_ANTERIOR", length = 20)
    public String getNumeroRegistroAnterior() {
        return this.numeroRegistroAnterior;
    }

    public void setNumeroRegistroAnterior(String numeroRegistroAnterior) {
        this.numeroRegistroAnterior = numeroRegistroAnterior;
    }

    @Column(name = "OFICINA", length = 20)
    public String getOficina() {
        return this.oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    @Column(name = "PAGINA", length = 20)
    public String getPagina() {
        return this.pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    @Column(name = "PREDIO_ID", precision = 10)
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    @Column(name = "TIPO_AVALUO_PREDIO", nullable = false, length = 30)
    public String getTipoAvaluoPredio() {
        return this.tipoAvaluoPredio;
    }

    public void setTipoAvaluoPredio(String tipoAvaluoPredio) {
        this.tipoAvaluoPredio = tipoAvaluoPredio;
    }

    @Column(name = "TIPO_INMUEBLE", length = 30)
    public String getTipoInmueble() {
        return this.tipoInmueble;
    }

    public void setTipoInmueble(String tipoInmueble) {
        this.tipoInmueble = tipoInmueble;
    }

    @Column(name = "TOMO", length = 20)
    public String getTomo() {
        return this.tomo;
    }

    public void setTomo(String tomo) {
        this.tomo = tomo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_ID", nullable = false)
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SolicitudPredio other = (SolicitudPredio) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (id.compareTo(other.id) != 0) {
            return false;
        }
        return true;
    }

}
