package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CONEXION_USUARIO database table.
 *
 */
@Entity
@Table(name = "CONEXION_USUARIO")
public class ConexionUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private String usuario;
    private Date fechaInicioSesion;
    private String maquinaCliente;
    private String maquinaServidor;
    private String sesion;

    public ConexionUsuario() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONEXION_USUARIO_ID_SEQ")
    @SequenceGenerator(name = "CONEXION_USUARIO_ID_SEQ", sequenceName = "CONEXION_USUARIO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INICIO_SESION")
    public Date getFechaInicioSesion() {
        return fechaInicioSesion;
    }

    public void setFechaInicioSesion(Date fechaInicioSesion) {
        this.fechaInicioSesion = fechaInicioSesion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String login) {
        this.usuario = login;
    }

    @Column(name = "MAQ_CLIENTE")
    public String getMaquinaCliente() {
        return maquinaCliente;
    }

    public void setMaquinaCliente(String maquinaCliente) {
        this.maquinaCliente = maquinaCliente;
    }

    @Column(name = "MAQ_SERVIDOR")
    public String getMaquinaServidor() {
        return maquinaServidor;
    }

    public void setMaquinaServidor(String maquinaServidor) {
        this.maquinaServidor = maquinaServidor;
    }

    @Column(name = "SESION_JBOSS")
    public String getSesion() {
        return sesion;
    }

    public void setSesion(String sesionId) {
        this.sesion = sesionId;
    }

}
