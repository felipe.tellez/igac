package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the ESTADISTICA_GERENCIAL_MANZANA database table.
 *
 */
@Entity
@Table(name = "ESTADISTICA_GERENCIAL_MANZANA")
public class EstadisticaGerencialManzana implements Serializable {

    private static final long serialVersionUID = 1L;
    private EstadisticaGerencialManzanaPK id;
    private BigDecimal areaConstruccion;
    private BigDecimal areaTerreno;
    private BigDecimal cantidadManzanasVeredas;
    private BigDecimal cantidadPredios;
    private BigDecimal cantidadPropietarios;
    private BigDecimal prediosCondicion0;
    private BigDecimal prediosCondicion1;
    private BigDecimal prediosCondicion2;
    private BigDecimal prediosCondicion3;
    private BigDecimal prediosCondicion4;
    private BigDecimal prediosCondicion5;
    private BigDecimal prediosCondicion6;
    private BigDecimal prediosCondicion7;
    private BigDecimal prediosCondicion8;
    private BigDecimal prediosCondicion9;
    private BigDecimal valorAvaluo;

    public EstadisticaGerencialManzana() {
    }

    @EmbeddedId
    public EstadisticaGerencialManzanaPK getId() {
        return this.id;
    }

    public void setId(EstadisticaGerencialManzanaPK id) {
        this.id = id;
    }

    @Column(name = "AREA_CONSTRUCCION")
    public BigDecimal getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(BigDecimal areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "AREA_TERRENO")
    public BigDecimal getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(BigDecimal areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "CANTIDAD_MANZANAS_VEREDAS")
    public BigDecimal getCantidadManzanasVeredas() {
        return this.cantidadManzanasVeredas;
    }

    public void setCantidadManzanasVeredas(BigDecimal cantidadManzanasVeredas) {
        this.cantidadManzanasVeredas = cantidadManzanasVeredas;
    }

    @Column(name = "CANTIDAD_PREDIOS")
    public BigDecimal getCantidadPredios() {
        return this.cantidadPredios;
    }

    public void setCantidadPredios(BigDecimal cantidadPredios) {
        this.cantidadPredios = cantidadPredios;
    }

    @Column(name = "CANTIDAD_PROPIETARIOS")
    public BigDecimal getCantidadPropietarios() {
        return this.cantidadPropietarios;
    }

    public void setCantidadPropietarios(BigDecimal cantidadPropietarios) {
        this.cantidadPropietarios = cantidadPropietarios;
    }

    @Column(name = "PREDIOS_CONDICION_0")
    public BigDecimal getPrediosCondicion0() {
        return this.prediosCondicion0;
    }

    public void setPrediosCondicion0(BigDecimal prediosCondicion0) {
        this.prediosCondicion0 = prediosCondicion0;
    }

    @Column(name = "PREDIOS_CONDICION_1")
    public BigDecimal getPrediosCondicion1() {
        return this.prediosCondicion1;
    }

    public void setPrediosCondicion1(BigDecimal prediosCondicion1) {
        this.prediosCondicion1 = prediosCondicion1;
    }

    @Column(name = "PREDIOS_CONDICION_2")
    public BigDecimal getPrediosCondicion2() {
        return this.prediosCondicion2;
    }

    public void setPrediosCondicion2(BigDecimal prediosCondicion2) {
        this.prediosCondicion2 = prediosCondicion2;
    }

    @Column(name = "PREDIOS_CONDICION_3")
    public BigDecimal getPrediosCondicion3() {
        return this.prediosCondicion3;
    }

    public void setPrediosCondicion3(BigDecimal prediosCondicion3) {
        this.prediosCondicion3 = prediosCondicion3;
    }

    @Column(name = "PREDIOS_CONDICION_4")
    public BigDecimal getPrediosCondicion4() {
        return this.prediosCondicion4;
    }

    public void setPrediosCondicion4(BigDecimal prediosCondicion4) {
        this.prediosCondicion4 = prediosCondicion4;
    }

    @Column(name = "PREDIOS_CONDICION_5")
    public BigDecimal getPrediosCondicion5() {
        return this.prediosCondicion5;
    }

    public void setPrediosCondicion5(BigDecimal prediosCondicion5) {
        this.prediosCondicion5 = prediosCondicion5;
    }

    @Column(name = "PREDIOS_CONDICION_6")
    public BigDecimal getPrediosCondicion6() {
        return this.prediosCondicion6;
    }

    public void setPrediosCondicion6(BigDecimal prediosCondicion6) {
        this.prediosCondicion6 = prediosCondicion6;
    }

    @Column(name = "PREDIOS_CONDICION_7")
    public BigDecimal getPrediosCondicion7() {
        return this.prediosCondicion7;
    }

    public void setPrediosCondicion7(BigDecimal prediosCondicion7) {
        this.prediosCondicion7 = prediosCondicion7;
    }

    @Column(name = "PREDIOS_CONDICION_8")
    public BigDecimal getPrediosCondicion8() {
        return this.prediosCondicion8;
    }

    public void setPrediosCondicion8(BigDecimal prediosCondicion8) {
        this.prediosCondicion8 = prediosCondicion8;
    }

    @Column(name = "PREDIOS_CONDICION_9")
    public BigDecimal getPrediosCondicion9() {
        return this.prediosCondicion9;
    }

    public void setPrediosCondicion9(BigDecimal prediosCondicion9) {
        this.prediosCondicion9 = prediosCondicion9;
    }

    @Column(name = "VALOR_AVALUO")
    public BigDecimal getValorAvaluo() {
        return this.valorAvaluo;
    }

    public void setValorAvaluo(BigDecimal valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

}
