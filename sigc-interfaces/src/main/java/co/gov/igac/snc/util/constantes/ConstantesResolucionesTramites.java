/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util.constantes;

/**
 * Clase que contiene constantes que se usan para el texto de las resoluciones de los trámites
 *
 * @author javier.aponte
 */
public class ConstantesResolucionesTramites {

    public static final String CONDICION =
        "EN SU CONDICIÓN DE ";

    public static final String CONDICIONES =
        " RESPECTIVAMENTE, EN SUS CONDICIONES DE ";

    public static final String DOCUMENTOS_JUSTIFICATIVOS =
        "<!--DOCUMENTOS JUSTIFICATIVOS-->";

    public static final String DIRECCION_TERRITORIAL =
        "LA DIRECCIÓN TERRITORIAL DE ";

    public static final String ESCRITURAS_PUBLICAS =
        "ESCRITURA(S) PÚBLICA(S)";

    public static final String IDENTIFICADO =
        " IDENTIFICADO(A) CON ";

    public static final String IDENTIFICADOS =
        " IDENTIFICADOS(AS) CON ";

    public static final String MATRICULA_INMOBILIARIA =
        "DEBIDAMENTE REGISTRADA CON LA MATRICULA INMOBILIARIA";

    public static final String NOTARIA =
        "DE LA NOTARIA";

    public static final String NUMERO_RADICADO =
        "CON EL NÚMERO ";
    
    public static final String NUMERO_RADICADO_TRAMITE_CATASTRAL =
        "TRAMITE CATASTRAL N° ";

    public static final String RADICO =
        "RADICÓ";

    public static final String RADICARON =
        "RADICARON";

    public static final String RESPECTIVAMENTE =
        " RESPECTIVAMENTE";

    public static final String RESPONSABLE_DIRECCION_TERRITORIAL =
        "EL RESPONSABLE DE CONSERVACIÓN DE LA TERRITORIAL";

    public static final String RESPONSABLE_UNIDAD_OPERATIVA =
        "EL RESPONSABLE DE LA UNIDAD OPERATIVA DE CATASTRO";

    public static final String SIN_PLANTILLA =
        "SIN PLANTILLA";

    public static final String SENIOR =
        "EL(LA) SEÑOR(A) ";

    public static final String SENIORES =
        "LOS(LAS) SEÑORES(AS): ";

    public static final String UNIDAD_OPERATIVA =
        "LA UNIDAD OPERATIVA DE CATASTRO DE ";

}
