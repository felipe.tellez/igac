package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;

/**
 * Objeto utilizado para serializar la información a enviar por REST / XML
 *
 * @author franz.gamba
 *
 */
public class DetallesDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6906638577341260655L;

    // datos del predio
    // --------------------------------------------
    private String codigoDepartamentoDetalle;
    private String nombreDepartamentoDetalle;
    private String codigoMunicipioDetalle;
    private String nombreMunicipioDetalle;
    private String tipoAvaluoDetalle;
    private String sectorCodigoDetalle;
    private String barrioCodigoDetalle;
    private String manzanaCodigoDetalle;
    private String predioDetalle;
    private String condicionPropiedadDetalle;
    private String edificioDetalle;
    private String pisoDetalle;
    private String numeroPredialDetalle;
    private String numeroPredialAnteriorDetalle;
    // aqui van los datos de la territorial -> estructuraOrganizacional
    // ------------------------------------------------------------------
    private List<String> codigoTerritorial = new ArrayList<String>(0);
    private List<String> tipoTerritorial = new ArrayList<String>(0);
    private List<String> nombreTerritorial = new ArrayList<String>(0);
    // ------------------------------------------------------------------
    private String corregimientoCodigoDetalle;
    private String nombrePredioDetalle;
    private String destinoDetalle;
    private String tipoDetalle;
    private Double areaTerrenoDetalle;
    private String tipoCatastro;
    private String estratoDetalle;
    private String chipDetalle;
    // private List<UnidadConstruccion> unidadConstruccionsDetalles = new
    // ArrayList<UnidadConstruccion>();
    // datos de la unidad de construccion
    // --------------------------------------------
    private List<String> unidadDetalle = new ArrayList<String>(0);
    private List<Long> calificacionAnexoIdDetalle = new ArrayList<Long>(0);
    private List<String> tipoConstruccionDetalle = new ArrayList<String>(0);
    private List<String> descripcionDetalle = new ArrayList<String>(0);
    private List<String> observacionesDetalle = new ArrayList<String>(0);
    private List<String> tipoCalificacionDetalle = new ArrayList<String>(0);
    private List<Double> totalPuntajeDetalle = new ArrayList<Double>(0);
    private List<String> tipificacionDetalle = new ArrayList<String>(0);
    private List<Double> areaConstruidaDetalle = new ArrayList<Double>(0);
    private List<Double> avaluoDetalle = new ArrayList<Double>(0);
    private List<Integer> totalBaniosDetalle = new ArrayList<Integer>(0);
    private List<Integer> totalHabitacionesDetalle = new ArrayList<Integer>(0);
    private List<Integer> totalLocalesDetalle = new ArrayList<Integer>(0);
    private List<Integer> totalPisosUnidadDetalle = new ArrayList<Integer>(0);
    private List<Integer> totalPisosConstruccionDetalle = new ArrayList<Integer>(0);
    private List<String> pisoUbicacionDetalle = new ArrayList<String>(0);
    // datos de usoConstruccion
    // --------------------------------------------
    private List<String> tipoUsoDetalle = new ArrayList<String>();
    private List<String> destinoEconomicoDetalle = new ArrayList<String>();
    // datos de predioAvaluoCatastral
    // --------------------------------------------
    private List<Double> valorTotalAvaluoCatastralDetalle = new ArrayList<Double>();
    private List<Double> valorTerrenoDetalle = new ArrayList<Double>();
    private List<Double> valorTotalConstruccionDetalle = new ArrayList<Double>();
    // datos de predioZona
    // --------------------------------------------
    private List<String> zonaFisicaDetalle = new ArrayList<String>();
    private List<String> zonaGeoeconomicaDetalle = new ArrayList<String>();
    private List<Double> areaDetalle = new ArrayList<Double>();
    // datos de predioDireccion
    // --------------------------------------------
    private List<String> direccionDetalle = new ArrayList<String>();
    private List<String> principalDetalle = new ArrayList<String>();
    private List<String> referenciaDetalle = new ArrayList<String>();
    // datos de personaPredio -> persona
    // --------------------------------------------
    private List<String> tipoPersonaDetalle = new ArrayList<String>();
    private List<String> tipoIdentificacionDetalle = new ArrayList<String>();
    private List<String> numeroIdentificacionDetalle = new ArrayList<String>();
    private List<String> primerNombreDetalle = new ArrayList<String>();
    private List<String> segundoNombreDetalle = new ArrayList<String>();
    private List<String> primerApellidoDetalle = new ArrayList<String>();
    private List<String> segundoApellidoDetalle = new ArrayList<String>();
    private List<String> razonSocialDetalle = new ArrayList<String>();
    private List<String> direccionPersonaDetalle = new ArrayList<String>();
    private List<String> estadoCivilDetalle = new ArrayList<String>();

    /*
     * Constructor a partir de un predio dado
     */
    public DetallesDTO(Predio p) {

        // departamentoDetalle;
        // Municipio municipioDetalle;
        Predio predioD = p;
        Departamento departamentoDetalle = predioD.getDepartamento();
        Municipio municipioDetalle = predioD.getMunicipio();
        List<UnidadConstruccion> unidadConstruccionsDetalles = new ArrayList<UnidadConstruccion>();
        unidadConstruccionsDetalles = predioD.getUnidadConstruccions();

        /*
         * Inicializacion de los valores del predio
         */
        this.setNumeroPredialDetalle(predioD.getNumeroPredial());
        this.setCodigoDepartamentoDetalle(departamentoDetalle.getCodigo());
        this.setNombreDepartamentoDetalle(departamentoDetalle.getNombre());
        this.setCodigoMunicipioDetalle(municipioDetalle.getCodigo());
        this.setNombreMunicipioDetalle(municipioDetalle.getNombre());
        this.setTipoAvaluoDetalle(predioD.getTipoAvaluo());
        this.setSectorCodigoDetalle(predioD.getSectorCodigo());
        this.setBarrioCodigoDetalle(predioD.getBarrioCodigo());
        this.setManzanaCodigoDetalle(predioD.getManzanaCodigo());
        this.setPredioDetalle(predioD.getPredio());
        this.setCondicionPropiedadDetalle(predioD.getCondicionPropiedad());
        this.setEdificioDetalle(predioD.getEdificio());
        this.setPisoDetalle(predioD.getPiso());
        this.setNumeroPredialDetalle(predioD.getNumeroPredial());
        this.setNumeroPredialAnteriorDetalle(predioD.getNumeroPredialAnterior());

        for (EstructuraOrganizacional eo : municipioDetalle
            .getEstructuraOrganizacionals()) {
            this.codigoTerritorial.add(eo.getCodigo());
            this.tipoTerritorial.add(eo.getTipo());
            this.nombreTerritorial.add(eo.getNombre());
        }

        this.setCorregimientoCodigoDetalle(predioD.getCorregimientoCodigo());
        this.setDestinoDetalle(predioD.getDestino());
        this.setTipoDetalle(predioD.getTipo());
        this.setAreaTerrenoDetalle(predioD.getAreaTerreno());
        this.setTipoCatastro(predioD.getTipoCatastro());
        this.setEstratoDetalle(predioD.getEstrato());
        this.setChipDetalle(predioD.getChip());
        /*
         * inicializacion de los valores correspondientes a las unidades de construccion
         */
        for (UnidadConstruccion uc : unidadConstruccionsDetalles) {
            this.unidadDetalle.add(uc.getUnidad());
            this.calificacionAnexoIdDetalle.add(uc.getCalificacionAnexoId());
            this.tipoConstruccionDetalle.add(uc.getTipoConstruccion());
            this.descripcionDetalle.add(uc.getDescripcion());
            this.observacionesDetalle.add(uc.getObservaciones());
            this.tipoCalificacionDetalle.add(uc.getTipoCalificacion());
            this.totalPuntajeDetalle.add(uc.getTotalPuntaje());
            this.tipificacionDetalle.add(uc.getTipificacion());
            this.areaConstruidaDetalle.add(uc.getAreaConstruida());
            this.avaluoDetalle.add(uc.getAvaluo());
            this.totalBaniosDetalle.add(uc.getTotalBanios());
            this.totalHabitacionesDetalle.add(uc.getTotalHabitaciones());
            this.totalLocalesDetalle.add(uc.getTotalLocales());
            this.totalPisosUnidadDetalle.add(uc.getTotalPisosUnidad());
            this.totalPisosConstruccionDetalle.add(uc
                .getTotalPisosConstruccion());
            this.pisoUbicacionDetalle.add(uc.getPisoUbicacion());
            /*
             * inicializacion de los valores de usoConstruccion
             */
            this.tipoUsoDetalle.add(uc.getUsoConstruccion().getTipoUso());
            this.descripcionDetalle.add(uc.getUsoConstruccion()
                .getDestinoEconomico());
        }
        for (PredioAvaluoCatastral pac : predioD.getPredioAvaluoCatastrals()) {
            this.valorTotalAvaluoCatastralDetalle.add(pac
                .getValorTotalAvaluoCatastral());
            this.valorTerrenoDetalle.add(pac.getValorTerreno());
            this.valorTotalConstruccionDetalle.add(pac
                .getValorTotalConstruccion());
        }
        for (PredioZona pz : predioD.getPredioZonas()) {
            this.zonaFisicaDetalle.add(pz.getZonaFisica());
            this.zonaGeoeconomicaDetalle.add(pz.getZonaGeoeconomica());
            this.areaDetalle.add(pz.getArea());
        }
        for (PredioDireccion pd : predioD.getPredioDireccions()) {
            this.direccionDetalle.add(pd.getDireccion());
            this.principalDetalle.add(pd.getPrincipal());
            this.referenciaDetalle.add(pd.getReferencia());
        }
        for (PersonaPredio pp : predioD.getPersonaPredios()) {
            this.tipoPersonaDetalle.add(pp.getPersona().getTipoPersona());
            this.tipoIdentificacionDetalle.add(pp.getPersona()
                .getTipoIdentificacion());
            this.numeroIdentificacionDetalle.add(pp.getPersona()
                .getNumeroIdentificacion());
            this.primerNombreDetalle.add(pp.getPersona().getPrimerNombre());
            this.segundoNombreDetalle.add(pp.getPersona().getSegundoNombre());
            this.primerApellidoDetalle.add(pp.getPersona().getPrimerApellido());
            this.segundoApellidoDetalle.add(pp.getPersona()
                .getSegundoApellido());
            this.razonSocialDetalle.add(pp.getPersona().getRazonSocial());
            this.direccionPersonaDetalle.add(pp.getPersona().getDireccion());
            this.estadoCivilDetalle.add(pp.getPersona().getEstadoCivil());
        }
    }

    /*
     * Metodos de Acceso
     */
    public String getCodigoDepartamentoDetalle() {
        return codigoDepartamentoDetalle;
    }

    public void setCodigoDepartamentoDetalle(String codigoDepartamentoDetalle) {
        this.codigoDepartamentoDetalle = codigoDepartamentoDetalle;
    }

    public String getNombreDepartamentoDetalle() {
        return nombreDepartamentoDetalle;
    }

    public void setNombreDepartamentoDetalle(String nombreDepartamentoDetalle) {
        this.nombreDepartamentoDetalle = nombreDepartamentoDetalle;
    }

    public String getCodigoMunicipioDetalle() {
        return codigoMunicipioDetalle;
    }

    public void setCodigoMunicipioDetalle(String codigoMunicipioDetalle) {
        this.codigoMunicipioDetalle = codigoMunicipioDetalle;
    }

    public String getNombreMunicipioDetalle() {
        return nombreMunicipioDetalle;
    }

    public void setNombreMunicipioDetalle(String nombreMunicipioDetalle) {
        this.nombreMunicipioDetalle = nombreMunicipioDetalle;
    }

    public String getTipoAvaluoDetalle() {
        return tipoAvaluoDetalle;
    }

    public void setTipoAvaluoDetalle(String tipoAvaluoDetalle) {
        this.tipoAvaluoDetalle = tipoAvaluoDetalle;
    }

    public List<String> getTipoTerritorial() {
        return tipoTerritorial;
    }

    public void setTipoTerritorial(List<String> tipoTerritorial) {
        this.tipoTerritorial = tipoTerritorial;
    }

    public String getSectorCodigoDetalle() {
        return sectorCodigoDetalle;
    }

    public void setSectorCodigoDetalle(String sectorCodigoDetalle) {
        this.sectorCodigoDetalle = sectorCodigoDetalle;
    }

    public String getBarrioCodigoDetalle() {
        return barrioCodigoDetalle;
    }

    public void setBarrioCodigoDetalle(String barrioCodigoDetalle) {
        this.barrioCodigoDetalle = barrioCodigoDetalle;
    }

    public String getManzanaCodigoDetalle() {
        return manzanaCodigoDetalle;
    }

    public void setManzanaCodigoDetalle(String manzanaCodigoDetalle) {
        this.manzanaCodigoDetalle = manzanaCodigoDetalle;
    }

    public String getPredioDetalle() {
        return predioDetalle;
    }

    public void setPredioDetalle(String predioDetalle) {
        this.predioDetalle = predioDetalle;
    }

    public String getCondicionPropiedadDetalle() {
        return condicionPropiedadDetalle;
    }

    public void setCondicionPropiedadDetalle(String condicionPropiedadDetalle) {
        this.condicionPropiedadDetalle = condicionPropiedadDetalle;
    }

    public String getEdificioDetalle() {
        return edificioDetalle;
    }

    public void setEdificioDetalle(String edificioDetalle) {
        this.edificioDetalle = edificioDetalle;
    }

    public String getPisoDetalle() {
        return pisoDetalle;
    }

    public void setPisoDetalle(String pisoDetalle) {
        this.pisoDetalle = pisoDetalle;
    }

    public String getNumeroPredialDetalle() {
        return numeroPredialDetalle;
    }

    public void setNumeroPredialDetalle(String numeroPredialDetalle) {
        this.numeroPredialDetalle = numeroPredialDetalle;
    }

    public String getNumeroPredialAnteriorDetalle() {
        return numeroPredialAnteriorDetalle;
    }

    public void setNumeroPredialAnteriorDetalle(
        String numeroPredialAnteriorDetalle) {
        this.numeroPredialAnteriorDetalle = numeroPredialAnteriorDetalle;
    }

    public List<String> getCodigoTerritorial() {
        return codigoTerritorial;
    }

    public void setCodigoTerritorial(List<String> codigoTerritorial) {
        this.codigoTerritorial = codigoTerritorial;
    }

    public List<String> getNombreTerritorial() {
        return nombreTerritorial;
    }

    public void setNombreTerritorial(List<String> nombreTerritorial) {
        this.nombreTerritorial = nombreTerritorial;
    }

    public String getCorregimientoCodigoDetalle() {
        return corregimientoCodigoDetalle;
    }

    public void setCorregimientoCodigoDetalle(String corregimientoCodigoDetalle) {
        this.corregimientoCodigoDetalle = corregimientoCodigoDetalle;
    }

    public String getNombrePredioDetalle() {
        return nombrePredioDetalle;
    }

    public void setNombrePredioDetalle(String nombrePredioDetalle) {
        this.nombrePredioDetalle = nombrePredioDetalle;
    }

    public String getDestinoDetalle() {
        return destinoDetalle;
    }

    public void setDestinoDetalle(String destinoDetalle) {
        this.destinoDetalle = destinoDetalle;
    }

    public String getTipoDetalle() {
        return tipoDetalle;
    }

    public void setTipoDetalle(String tipoDetalle) {
        this.tipoDetalle = tipoDetalle;
    }

    public Double getAreaTerrenoDetalle() {
        return areaTerrenoDetalle;
    }

    public void setAreaTerrenoDetalle(Double areaTerrenoDetalle) {
        this.areaTerrenoDetalle = areaTerrenoDetalle;
    }

    public String getEstratoDetalle() {
        return estratoDetalle;
    }

    public void setEstratoDetalle(String estratoDetalle) {
        this.estratoDetalle = estratoDetalle;
    }

    public String getChipDetalle() {
        return chipDetalle;
    }

    public void setChipDetalle(String chipDetalle) {
        this.chipDetalle = chipDetalle;
    }

    public List<String> getUnidadDetalle() {
        return unidadDetalle;
    }

    public void setUnidadDetalle(List<String> unidadDetalle) {
        this.unidadDetalle = unidadDetalle;
    }

    public List<Long> getCalificacionAnexoIdDetalle() {
        return calificacionAnexoIdDetalle;
    }

    public void setCalificacionAnexoIdDetalle(
        List<Long> calificacionAnexoIdDetalle) {
        this.calificacionAnexoIdDetalle = calificacionAnexoIdDetalle;
    }

    public List<String> getTipoConstruccionDetalle() {
        return tipoConstruccionDetalle;
    }

    public void setTipoConstruccionDetalle(List<String> tipoConstruccionDetalle) {
        this.tipoConstruccionDetalle = tipoConstruccionDetalle;
    }

    public List<String> getDescripcionDetalle() {
        return descripcionDetalle;
    }

    public void setDescripcionDetalle(List<String> descripcionDetalle) {
        this.descripcionDetalle = descripcionDetalle;
    }

    public List<String> getObservacionesDetalle() {
        return observacionesDetalle;
    }

    public void setObservacionesDetalle(List<String> observacionesDetalle) {
        this.observacionesDetalle = observacionesDetalle;
    }

    public List<String> getTipoCalificacionDetalle() {
        return tipoCalificacionDetalle;
    }

    public void setTipoCalificacionDetalle(List<String> tipoCalificacionDetalle) {
        this.tipoCalificacionDetalle = tipoCalificacionDetalle;
    }

    public List<Double> getTotalPuntajeDetalle() {
        return totalPuntajeDetalle;
    }

    public void setTotalPuntajeDetalle(List<Double> totalPuntajeDetalle) {
        this.totalPuntajeDetalle = totalPuntajeDetalle;
    }

    public List<String> getTipificacionDetalle() {
        return tipificacionDetalle;
    }

    public void setTipificacionDetalle(List<String> tipificacionDetalle) {
        this.tipificacionDetalle = tipificacionDetalle;
    }

    public List<Double> getAreaConstruidaDetalle() {
        return areaConstruidaDetalle;
    }

    public void setAreaConstruidaDetalle(List<Double> areaConstruidaDetalle) {
        this.areaConstruidaDetalle = areaConstruidaDetalle;
    }

    public List<Double> getAvaluoDetalle() {
        return avaluoDetalle;
    }

    public void setAvaluoDetalle(List<Double> avaluoDetalle) {
        this.avaluoDetalle = avaluoDetalle;
    }

    public List<Integer> getTotalBaniosDetalle() {
        return totalBaniosDetalle;
    }

    public void setTotalBaniosDetalle(List<Integer> totalBaniosDetalle) {
        this.totalBaniosDetalle = totalBaniosDetalle;
    }

    public List<Integer> getTotalHabitacionesDetalle() {
        return totalHabitacionesDetalle;
    }

    public void setTotalHabitacionesDetalle(List<Integer> totalHabitacionesDetalle) {
        this.totalHabitacionesDetalle = totalHabitacionesDetalle;
    }

    public List<Integer> getTotalLocalesDetalle() {
        return totalLocalesDetalle;
    }

    public void setTotalLocalesDetalle(List<Integer> totalLocalesDetalle) {
        this.totalLocalesDetalle = totalLocalesDetalle;
    }

    public List<Integer> getTotalPisosUnidadDetalle() {
        return totalPisosUnidadDetalle;
    }

    public void setTotalPisosUnidadDetalle(List<Integer> totalPisosUnidadDetalle) {
        this.totalPisosUnidadDetalle = totalPisosUnidadDetalle;
    }

    public List<Integer> getTotalPisosConstruccionDetalle() {
        return totalPisosConstruccionDetalle;
    }

    public void setTotalPisosConstruccionDetalle(
        List<Integer> totalPisosConstruccionDetalle) {
        this.totalPisosConstruccionDetalle = totalPisosConstruccionDetalle;
    }

    public List<String> getPisoUbicacionDetalle() {
        return pisoUbicacionDetalle;
    }

    public void setPisoUbicacionDetalle(List<String> pisoUbicacionDetalle) {
        this.pisoUbicacionDetalle = pisoUbicacionDetalle;
    }

    public List<String> getTipoUsoDetalle() {
        return tipoUsoDetalle;
    }

    public void setTipoUsoDetalle(List<String> tipoUsoDetalle) {
        this.tipoUsoDetalle = tipoUsoDetalle;
    }

    public List<String> getDestinoEconomicoDetalle() {
        return destinoEconomicoDetalle;
    }

    public void setDestinoEconomicoDetalle(List<String> destinoEconomicoDetalle) {
        this.destinoEconomicoDetalle = destinoEconomicoDetalle;
    }

    public List<Double> getValorTotalAvaluoCatastralDetalle() {
        return valorTotalAvaluoCatastralDetalle;
    }

    public void setValorTotalAvaluoCatastralDetalle(
        List<Double> valorTotalAvaluoCatastralDetalle) {
        this.valorTotalAvaluoCatastralDetalle = valorTotalAvaluoCatastralDetalle;
    }

    public List<Double> getValorTerrenoDetalle() {
        return valorTerrenoDetalle;
    }

    public void setValorTerrenoDetalle(List<Double> valorTerrenoDetalle) {
        this.valorTerrenoDetalle = valorTerrenoDetalle;
    }

    public List<Double> getValorTotalConstruccionDetalle() {
        return valorTotalConstruccionDetalle;
    }

    public void setValorTotalConstruccionDetalle(
        List<Double> valorTotalConstruccionDetalle) {
        this.valorTotalConstruccionDetalle = valorTotalConstruccionDetalle;
    }

    public List<String> getZonaFisicaDetalle() {
        return zonaFisicaDetalle;
    }

    public void setZonaFisicaDetalle(List<String> zonaFisicaDetalle) {
        this.zonaFisicaDetalle = zonaFisicaDetalle;
    }

    public List<String> getZonaGeoeconomicaDetalle() {
        return zonaGeoeconomicaDetalle;
    }

    public void setZonaGeoeconomicaDetalle(List<String> zonaGeoeconomicaDetalle) {
        this.zonaGeoeconomicaDetalle = zonaGeoeconomicaDetalle;
    }

    public List<Double> getAreaDetalle() {
        return areaDetalle;
    }

    public void setAreaDetalle(List<Double> areaDetalle) {
        this.areaDetalle = areaDetalle;
    }

    public List<String> getDireccionDetalle() {
        return direccionDetalle;
    }

    public void setDireccionDetalle(List<String> direccionDetalle) {
        this.direccionDetalle = direccionDetalle;
    }

    public List<String> getPrincipalDetalle() {
        return principalDetalle;
    }

    public void setPrincipalDetalle(List<String> principalDetalle) {
        this.principalDetalle = principalDetalle;
    }

    public List<String> getReferenciaDetalle() {
        return referenciaDetalle;
    }

    public void setReferenciaDetalle(List<String> referenciaDetalle) {
        this.referenciaDetalle = referenciaDetalle;
    }

    public List<String> getTipoPersonaDetalle() {
        return tipoPersonaDetalle;
    }

    public void setTipoPersonaDetalle(List<String> tipoPersonaDetalle) {
        this.tipoPersonaDetalle = tipoPersonaDetalle;
    }

    public List<String> getTipoIdentificacionDetalle() {
        return tipoIdentificacionDetalle;
    }

    public void setTipoIdentificacionDetalle(
        List<String> tipoIdentificacionDetalle) {
        this.tipoIdentificacionDetalle = tipoIdentificacionDetalle;
    }

    public List<String> getNumeroIdentificacionDetalle() {
        return numeroIdentificacionDetalle;
    }

    public void setNumeroIdentificacionDetalle(
        List<String> numeroIdentificacionDetalle) {
        this.numeroIdentificacionDetalle = numeroIdentificacionDetalle;
    }

    public List<String> getPrimerNombreDetalle() {
        return primerNombreDetalle;
    }

    public void setPrimerNombreDetalle(List<String> primerNombreDetalle) {
        this.primerNombreDetalle = primerNombreDetalle;
    }

    public List<String> getSegundoNombreDetalle() {
        return segundoNombreDetalle;
    }

    public void setSegundoNombreDetalle(List<String> segundoNombreDetalle) {
        this.segundoNombreDetalle = segundoNombreDetalle;
    }

    public List<String> getPrimerApellidoDetalle() {
        return primerApellidoDetalle;
    }

    public void setPrimerApellidoDetalle(List<String> primerApellidoDetalle) {
        this.primerApellidoDetalle = primerApellidoDetalle;
    }

    public List<String> getSegundoApellidoDetalle() {
        return segundoApellidoDetalle;
    }

    public void setSegundoApellidoDetalle(List<String> segundoApellidoDetalle) {
        this.segundoApellidoDetalle = segundoApellidoDetalle;
    }

    public List<String> getRazonSocialDetalle() {
        return razonSocialDetalle;
    }

    public void setRazonSocialDetalle(List<String> razonSocialDetalle) {
        this.razonSocialDetalle = razonSocialDetalle;
    }

    public List<String> getDireccionPersonaDetalle() {
        return direccionPersonaDetalle;
    }

    public void setDireccionPersonaDetalle(List<String> direccionPersonaDetalle) {
        this.direccionPersonaDetalle = direccionPersonaDetalle;
    }

    public List<String> getEstadoCivilDetalle() {
        return estadoCivilDetalle;
    }

    public void setEstadoCivilDetalle(List<String> estadoCivilDetalle) {
        this.estadoCivilDetalle = estadoCivilDetalle;
    }

    public String getTipoCatastro() {
        return tipoCatastro;
    }

    public void setTipoCatastro(String tipoCatastro) {
        this.tipoCatastro = tipoCatastro;
    }

}
