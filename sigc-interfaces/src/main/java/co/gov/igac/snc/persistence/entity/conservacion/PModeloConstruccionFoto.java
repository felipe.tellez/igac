package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.snc.persistence.entity.generales.Documento;

import java.util.Date;

/**
 * The persistent class for the P_MODELO_CONSTRUCCION_FOTO database table.
 *
 * @modified david.cifuentes :: Cambio del tipo de dato y nombre de la variable de fotoDocumentoId
 * (Long) a fotoDocumento (Documento) :: 23/04/2012
 *
 * :: Adicion de nameQuery findByUnidadDelModeloIdFetchDocumento :: 23/04/2012 :: Adición de la
 * secuencia MODELO_CONSTRUCCION_FOT_ID_SEQ :: 26/04/2012 :: Adicion de nameQuery
 * findByUnidadDelModeloConstruccionId :: 26/04/2012 :: Adición de los campos descripcion y fecha ::
 * 04/05/2012
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "findByUnidadDelModeloIdFetchDocumento",
        query = "from PModeloConstruccionFoto pmcf join fetch pmcf.fotoDocumento " +
        "where pmcf.PFmModeloConstruccion.id = :pFmModeloConstruccionIdParam"),
    @NamedQuery(name = "findByUnidadDelModeloConstruccionId",
        query = "from PModeloConstruccionFoto pmcf " +
        "where pmcf.PFmModeloConstruccion.id = :pFmModeloConstruccionIdParam")
})
@Table(name = "P_MODELO_CONSTRUCCION_FOTO")
public class PModeloConstruccionFoto implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String cancelaInscribe;
    private Date fechaLog;
    private Documento fotoDocumento;
    private String tipo;
    private String usuarioLog;
    private PFmModeloConstruccion PFmModeloConstruccion;

    private String descripcion;
    private Date fecha;

    public PModeloConstruccionFoto() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MODELO_CONSTRUCCION_FOT_ID_SEQ")
    @SequenceGenerator(name = "MODELO_CONSTRUCCION_FOT_ID_SEQ", sequenceName =
        "MODELO_CONSTRUCCION_FOT_ID_SEQ", allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "FOTO_DOCUMENTO_ID", nullable = false)
    public Documento getFotoDocumento() {
        return this.fotoDocumento;
    }

    public void setFotoDocumento(Documento fotoDocumento) {
        this.fotoDocumento = fotoDocumento;
    }

    @Column(length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to PFmModeloConstruccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FM_MODELO_CONSTRUCCION_ID", nullable = false)
    public PFmModeloConstruccion getPFmModeloConstruccion() {
        return this.PFmModeloConstruccion;
    }

    public void setPFmModeloConstruccion(PFmModeloConstruccion PFmModeloConstruccion) {
        this.PFmModeloConstruccion = PFmModeloConstruccion;
    }

    @Column(name = "DESCRIPCION", length = 2000)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA", nullable = true, length = 7)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
