/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import java.util.Date;

/**
 *
 * Clase creada para ser el wrapper de SolicitanteSolicitud y SolicitanteTramite. En el momento de
 * creación se hizo específicamente para evitar tener que hacer introspección de clases en el método
 * que me devuelve el solicitante a quien se envía la comunicación de notificación, ya que debía
 * recibir ambos tipos de solicitantes -como un genérico- y por introspección ejecutar algunos
 * métodos.
 *
 *
 * @author pedro.garcia
 */
public class SolicitanteSolicitudOTramite {

    private Long id;

    private Solicitante solicitante;
    private Solicitud solicitud;
    private Tramite tramite;

    private String tipoPersona;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String digitoVerificacion;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String razonSocial;
    private String sigla;
    private Pais direccionPais;
    private Departamento direccionDepartamento;
    private Municipio direccionMunicipio;
    private String direccion;
    private String telefonoPrincipal;
    private String telefonoPrincipalExt;
    private String telefonoSecundario;
    private String telefonoSecundarioExt;
    private String telefonoCelular;
    private String fax;
    private String faxExt;
    private String correoElectronico;
    private String correoElectronicoSecundario;
    private String notificacionEmail;
    private String usuarioLog;
    private Date fechaLog;
    private String tipoSolicitante;
    private String relacion;

    //--------------   methods   ---------------
    //----   constructores ------
    public SolicitanteSolicitudOTramite(SolicitanteSolicitud solicitanteSolicitud) {
        this.id = solicitanteSolicitud.getId();
        this.solicitud = solicitanteSolicitud.getSolicitud();
        this.solicitante = solicitanteSolicitud.getSolicitante();

        this.tipoPersona = solicitanteSolicitud.getTipoPersona();
        this.tipoIdentificacion = solicitanteSolicitud.getTipoIdentificacion();
        this.numeroIdentificacion = solicitanteSolicitud.getNumeroIdentificacion();
        this.digitoVerificacion = solicitanteSolicitud.getDigitoVerificacion();
        this.primerNombre = solicitanteSolicitud.getPrimerNombre();
        this.segundoNombre = solicitanteSolicitud.getSegundoNombre();
        this.primerApellido = solicitanteSolicitud.getPrimerApellido();
        this.segundoApellido = solicitanteSolicitud.getSegundoApellido();
        this.razonSocial = solicitanteSolicitud.getRazonSocial();
        this.sigla = solicitanteSolicitud.getSigla();
        this.direccionPais = solicitanteSolicitud.getDireccionPais();
        this.direccionDepartamento = solicitanteSolicitud.getDireccionDepartamento();
        this.direccionMunicipio = solicitanteSolicitud.getDireccionMunicipio();
        this.direccion = solicitanteSolicitud.getDireccion();
        this.telefonoPrincipal = solicitanteSolicitud.getTelefonoPrincipal();
        this.telefonoPrincipalExt = solicitanteSolicitud.getTelefonoPrincipalExt();
        this.telefonoSecundario = solicitanteSolicitud.getTelefonoSecundario();
        this.telefonoSecundarioExt = solicitanteSolicitud.getTelefonoSecundarioExt();
        this.telefonoCelular = solicitanteSolicitud.getTelefonoCelular();
        this.fax = solicitanteSolicitud.getFax();
        this.faxExt = solicitanteSolicitud.getFaxExt();
        this.correoElectronico = solicitanteSolicitud.getCorreoElectronico();
        this.correoElectronicoSecundario = solicitanteSolicitud.getCorreoElectronicoSecundario();
        this.notificacionEmail = solicitanteSolicitud.getNotificacionEmail();
        this.usuarioLog = solicitanteSolicitud.getUsuarioLog();
        this.fechaLog = solicitanteSolicitud.getFechaLog();
        this.tipoSolicitante = solicitanteSolicitud.getTipoSolicitante();
        this.relacion = solicitanteSolicitud.getRelacion();

    }

    public SolicitanteSolicitudOTramite(SolicitanteTramite solicitanteTramite) {

        this.id = solicitanteTramite.getId();
        this.tramite = solicitanteTramite.getTramite();
        this.solicitante = solicitanteTramite.getSolicitante();

        this.tipoPersona = solicitanteTramite.getTipoPersona();
        this.tipoIdentificacion = solicitanteTramite.getTipoIdentificacion();
        this.numeroIdentificacion = solicitanteTramite.getNumeroIdentificacion();
        this.digitoVerificacion = solicitanteTramite.getDigitoVerificacion();
        this.primerNombre = solicitanteTramite.getPrimerNombre();
        this.segundoNombre = solicitanteTramite.getSegundoNombre();
        this.primerApellido = solicitanteTramite.getPrimerApellido();
        this.segundoApellido = solicitanteTramite.getSegundoApellido();
        this.razonSocial = solicitanteTramite.getRazonSocial();
        this.sigla = solicitanteTramite.getSigla();
        this.direccionPais = solicitanteTramite.getDireccionPais();
        this.direccionDepartamento = solicitanteTramite.getDireccionDepartamento();
        this.direccionMunicipio = solicitanteTramite.getDireccionMunicipio();
        this.direccion = solicitanteTramite.getDireccion();
        this.telefonoPrincipal = solicitanteTramite.getTelefonoPrincipal();
        this.telefonoPrincipalExt = solicitanteTramite.getTelefonoPrincipalExt();
        this.telefonoSecundario = solicitanteTramite.getTelefonoSecundario();
        this.telefonoSecundarioExt = solicitanteTramite.getTelefonoSecundarioExt();
        this.telefonoCelular = solicitanteTramite.getTelefonoCelular();
        this.fax = solicitanteTramite.getFax();
        this.faxExt = solicitanteTramite.getFaxExt();
        this.correoElectronico = solicitanteTramite.getCorreoElectronico();
        this.correoElectronicoSecundario = solicitanteTramite.getCorreoElectronicoSecundario();
        this.notificacionEmail = solicitanteTramite.getNotificacionEmail();
        this.usuarioLog = solicitanteTramite.getUsuarioLog();
        this.fechaLog = solicitanteTramite.getFechaLog();
        this.tipoSolicitante = solicitanteTramite.getTipoSolicitante();
        this.relacion = solicitanteTramite.getRelacion();

    }

//--------------------------------------------------------------------------------------------------
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Solicitante getSolicitante() {
        return this.solicitante;
    }

    public void setSolicitante(Solicitante solicitante) {
        this.solicitante = solicitante;
    }

    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public String getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public Pais getDireccionPais() {
        return this.direccionPais;
    }

    public void setDireccionPais(Pais direccionPais) {
        this.direccionPais = direccionPais;
    }

    public Departamento getDireccionDepartamento() {
        return this.direccionDepartamento;
    }

    public void setDireccionDepartamento(Departamento direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    public Municipio getDireccionMunicipio() {
        return this.direccionMunicipio;
    }

    public void setDireccionMunicipio(Municipio direccionMunicipio) {
        this.direccionMunicipio = direccionMunicipio;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefonoPrincipal() {
        return this.telefonoPrincipal;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    public String getTelefonoPrincipalExt() {
        return this.telefonoPrincipalExt;
    }

    public void setTelefonoPrincipalExt(String telefonoPrincipalExt) {
        this.telefonoPrincipalExt = telefonoPrincipalExt;
    }

    public String getTelefonoSecundario() {
        return this.telefonoSecundario;
    }

    public void setTelefonoSecundario(String telefonoSecundario) {
        this.telefonoSecundario = telefonoSecundario;
    }

    public String getTelefonoSecundarioExt() {
        return this.telefonoSecundarioExt;
    }

    public void setTelefonoSecundarioExt(String telefonoSecundarioExt) {
        this.telefonoSecundarioExt = telefonoSecundarioExt;
    }

    public String getTelefonoCelular() {
        return this.telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFaxExt() {
        return this.faxExt;
    }

    public void setFaxExt(String faxExt) {
        this.faxExt = faxExt;
    }

    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getCorreoElectronicoSecundario() {
        return this.correoElectronicoSecundario;
    }

    public void setCorreoElectronicoSecundario(String correoElectronicoSecundario) {
        this.correoElectronicoSecundario = correoElectronicoSecundario;
    }

    public String getNotificacionEmail() {
        return this.notificacionEmail;
    }

    public void setNotificacionEmail(String notificacionEmail) {
        this.notificacionEmail = notificacionEmail;
    }

    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getTipoSolicitante() {
        return this.tipoSolicitante;
    }

    public void setTipoSolicitante(String tipoSolicitante) {
        this.tipoSolicitante = tipoSolicitante;
    }

    public String getRelacion() {
        return this.relacion;
    }

    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }

}
