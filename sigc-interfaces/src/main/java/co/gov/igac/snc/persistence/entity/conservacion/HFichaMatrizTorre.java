package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the H_FICHA_MATRIZ_TORRE database table.
 *
 * MODIFICACIONES:
 *
 * @modified	david.cifuentes :: Adición de la variable unidadesSotanos, :: Cambio del name de la
 * columna "UNIDADES" a "UNIDADES_PRIVADAS" :: Cambio de tipo de las variables pisos, sotanos,
 * torre, unidades a Long :: Cambio del nombre y tipo de la variable fichaMatrizId a HFichaMatriz ::
 * 19/07/12 david.cifuentes :: Adición de la variable estado :: 08/09/15
 */
@Entity
@Table(name = "H_FICHA_MATRIZ_TORRE")
public class HFichaMatrizTorre implements Serializable {

    private static final long serialVersionUID = 1L;

    private HFichaMatrizTorrePK id;
    private String cancelaInscribe;
    private Date fechaLog;
    private HFichaMatriz HFichaMatriz;
    private Long pisos;
    private Long sotanos;
    private Long torre;
    private Long unidades;
    private String usuarioLog;
    private HPredio HPredio;
    private Long unidadesSotanos;
    private String estado;

    public HFichaMatrizTorre() {
    }

    @EmbeddedId
    public HFichaMatrizTorrePK getId() {
        return this.id;
    }

    public void setId(HFichaMatrizTorrePK id) {
        this.id = id;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "H_PREDIO_ID", referencedColumnName = "H_PREDIO_ID", nullable = false,
            insertable = false, updatable = false),
        @JoinColumn(name = "FICHA_MATRIZ_ID", referencedColumnName = "ID", nullable = false,
            insertable = false, updatable = false)})
    public HFichaMatriz getHFichaMatriz() {
        return this.HFichaMatriz;
    }

    public void setHFichaMatriz(HFichaMatriz HFichaMatriz) {
        this.HFichaMatriz = HFichaMatriz;
    }

    public Long getPisos() {
        return this.pisos;
    }

    public void setPisos(Long pisos) {
        this.pisos = pisos;
    }

    public Long getSotanos() {
        return this.sotanos;
    }

    public void setSotanos(Long sotanos) {
        this.sotanos = sotanos;
    }

    public Long getTorre() {
        return this.torre;
    }

    public void setTorre(Long torre) {
        this.torre = torre;
    }

    @Column(name = "UNIDADES_PRIVADAS", nullable = false, precision = 4)
    public Long getUnidades() {
        return this.unidades;
    }

    public void setUnidades(Long unidades) {
        this.unidades = unidades;
    }

    @Column(name = "UNIDADES_SOTANOS", nullable = false, precision = 4)
    public Long getUnidadesSotanos() {
        return this.unidadesSotanos;
    }

    public void setUnidadesSotanos(Long unidadesSotanos) {
        this.unidadesSotanos = unidadesSotanos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to HPredio
    @ManyToOne
    @JoinColumn(name = "H_PREDIO_ID", insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
