package co.gov.igac.snc.persistence.entity.conservacion;

import java.sql.Timestamp;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * HPredioDireccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "H_PREDIO_DIRECCION", schema = "SNC_CONSERVACION")
public class HPredioDireccion implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -5054464602926635510L;
    private HPredioDireccionId id;
    private HPredio HPredio;
    private Long predioId;
    private String direccion;
    private String principal;
    private String referencia;
    private Timestamp fechaInscripcionCatastral;
    private String cancelaInscribe;
    private String usuarioLog;
    private Timestamp fechaLog;

    // Constructors
    /** default constructor */
    public HPredioDireccion() {
    }

    /** minimal constructor */
    public HPredioDireccion(HPredioDireccionId id, HPredio HPredio,
        Long predioId, String direccion, String principal,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predioId = predioId;
        this.direccion = direccion;
        this.principal = principal;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public HPredioDireccion(HPredioDireccionId id, HPredio HPredio,
        Long predioId, String direccion, String principal,
        String referencia, Timestamp fechaInscripcionCatastral,
        String cancelaInscribe, String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predioId = predioId;
        this.direccion = direccion;
        this.principal = principal;
        this.referencia = referencia;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "HPredioId", column = @Column(name = "H_PREDIO_ID", nullable =
            false, precision = 10, scale = 0)),
        @AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false, precision =
            10, scale = 0))})
    public HPredioDireccionId getId() {
        return this.id;
    }

    public void setId(HPredioDireccionId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "H_PREDIO_ID", nullable = false, insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Column(name = "PREDIO_ID", nullable = false, precision = 10, scale = 0)
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    @Column(name = "DIRECCION", nullable = false, length = 250)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "PRINCIPAL", nullable = false, length = 2)
    public String getPrincipal() {
        return this.principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    @Column(name = "REFERENCIA", length = 100)
    public String getReferencia() {
        return this.referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    public Timestamp getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Timestamp fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

}
