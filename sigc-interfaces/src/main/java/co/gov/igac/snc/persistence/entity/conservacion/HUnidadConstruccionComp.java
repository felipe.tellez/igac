package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * HUnidadConstruccionComp entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "H_UNIDAD_CONSTRUCCION_COMP", schema = "SNC_CONSERVACION")
public class HUnidadConstruccionComp implements Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -8713577613030667721L;
    private HUnidadConstruccionCompId id;
    private HPredio HPredio;
    private HUnidadConstruccion HUnidadConstruccion;
    private String componente;
    private String elementoCalificacion;
    private String detalleCalificacion;
    private Double puntos;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public HUnidadConstruccionComp() {
    }

    /** minimal constructor */
    public HUnidadConstruccionComp(HUnidadConstruccionCompId id,
        HPredio HPredio, HUnidadConstruccion HUnidadConstruccion, Double puntos,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.HUnidadConstruccion = HUnidadConstruccion;
        this.puntos = puntos;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public HUnidadConstruccionComp(HUnidadConstruccionCompId id,
        HPredio HPredio, HUnidadConstruccion HUnidadConstruccion, String componente,
        String elementoCalificacion, String detalleCalificacion,
        Double puntos, String cancelaInscribe, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.HUnidadConstruccion = HUnidadConstruccion;
        this.componente = componente;
        this.elementoCalificacion = elementoCalificacion;
        this.detalleCalificacion = detalleCalificacion;
        this.puntos = puntos;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "HPredioId", column = @Column(name = "H_PREDIO_ID", nullable =
            false, precision = 10, scale = 0)),
        @AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false, precision =
            10, scale = 0))})
    public HUnidadConstruccionCompId getId() {
        return this.id;
    }

    public void setId(HUnidadConstruccionCompId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "H_PREDIO_ID", nullable = false, insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "H_PREDIO_ID", referencedColumnName = "H_PREDIO_ID", nullable = false,
            insertable = false, updatable = false),
        @JoinColumn(name = "UNIDAD_CONSTRUCCION_ID", referencedColumnName = "ID", nullable = false,
            insertable = false, updatable = false)})
    public HUnidadConstruccion getHUnidadConstruccion() {
        return this.HUnidadConstruccion;
    }

    public void setHUnidadConstruccion(HUnidadConstruccion unidadConstruccion) {
        this.HUnidadConstruccion = unidadConstruccion;
    }

    @Column(name = "COMPONENTE", length = 30)
    public String getComponente() {
        return this.componente;
    }

    public void setComponente(String componente) {
        this.componente = componente;
    }

    @Column(name = "ELEMENTO_CALIFICACION", length = 30)
    public String getElementoCalificacion() {
        return this.elementoCalificacion;
    }

    public void setElementoCalificacion(String elementoCalificacion) {
        this.elementoCalificacion = elementoCalificacion;
    }

    @Column(name = "DETALLE_CALIFICACION", length = 250)
    public String getDetalleCalificacion() {
        return this.detalleCalificacion;
    }

    public void setDetalleCalificacion(String detalleCalificacion) {
        this.detalleCalificacion = detalleCalificacion;
    }

    @Column(name = "PUNTOS", nullable = false, precision = 5)
    public Double getPuntos() {
        return this.puntos;
    }

    public void setPuntos(Double puntos) {
        this.puntos = puntos;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30, nullable = true)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
