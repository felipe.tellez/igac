package co.gov.igac.snc.persistence.util;

import co.gov.igac.snc.util.Constantes;

/**
 * Enumeración con los roles que pueden tener los usuarios que se autenticn en el sistema. Se
 * enumeran como deben aparecer en el LDAP, con el rol y su distinguished name.
 *
 *
 * @documented pedro.garcia
 */
public enum ERol {
    ABOGADO("Profesional_Abogado", "CN=Profesional_Abogado,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    ADMINISTRADOR_SNC("Administrador_SNC", "CN=Administrador_SNC,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    AVALUADOR("Avaluador", "CN=Avaluador,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    CONSULTA_TRAMITE("Consulta_Tramite", "CN=Consulta_Tramite,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    CONTROL_CALIDAD_PRODUCTOS("Control_Calidad_Productos",
        "CN=Control_Calidad_Productos,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    CONTROL_DIGITALIZACION("Control_Digitalizacion",
        "CN=Control_Digitalizacion,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    CONSULTA_PREDIO("Consulta_Predio", "CN=Consulta_Predio,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    CONSULTA_SNC("Consulta_SNC", "CN=Consulta_SNC,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    COORDINADOR("Coordinador", "CN=Coordinador,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    COORDINADOR_GIT_AVALUOS("Coordinador_GIT_Avaluos",
        "CN=Coordinador_GIT_Avaluos,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    DIGITALIZADOR("Digitalizadores", "CN=Digitalizadores,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    DIGITALIZADORES("Digitalizadores", "CN=Digitalizadores,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    DIGITALIZADOR_DEPURACION("Digitalizador_Depuracion",
        "CN=Digitalizador_Depuracion,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    DIRECTOR_TERRITORIAL("Director_Territorial",
        "CN=Director_Territorial,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    EJECUTOR_TRAMITE("Ejecutor_Tramite", "CN=Ejecutor_Tramite,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    FUNCIONARIO_RADICADOR("Funcionario_Radicador",
        "CN=Funcionario_Radicador,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    INVESTIGADOR_MERCADO("Investigador_Mercado",
        "CN=Investigador_Mercado,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    LIDER_TECNICO("Lider_Tecnico", "CN=Lider_Tecnico,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    PROFESIONAL_ABOGADO("Profesional_Abogado", "CN=Profesional_Abogado,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    RECOLECTOR_OFERTA_INMOBILIARIA("Recolector_Oferta_Inmobiliaria",
        "CN=Recolector_Oferta_inmobiliaria,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    RESPONSABLE_ATENCION_USUARIO("Responsable_Atencion_Usuario",
        "CN=Responsable_Atencion_Usuario,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    RESPONSABLE_ACTUALIZACION("Responsable_Actualizacion",
        "CN=Responsable_Actualizacion,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    RESPONSABLE_CONSERVACION("Responsable_Conservacion",
        "CN=Responsable_Conservacion,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    RESPONSABLE_RADICACION_CONSERVACION("Responsable_Radicacion_Conservacion",
        "CN=Responsable_Radicacion_Conservacion,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    RESPONSABLE_SIG("Responsable_SIG", "CN=Responsable_SIG,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    RESPONSABLE_SISTEMAS_SUBDIRECCION_CATASTRO("Responsable_Sistemas_Subdireccion_Catastro",
        "CN=Responsable_Sistemas_Subdireccion_Catastro,OU=SIGC,OU=APLICACIONES," +
        Constantes.LDAP_DC),
    RESPONSABLE_TOPOGRAFIA("Responsable_Topografia",
        "CN=Responsable_Topografia,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    RESPONSABLE_UNIDAD_OPERATIVA("Responsable_Unidad_Operativa",
        "CN=Responsable_Unidad_Operativa,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    SECRETARIA_CONSERVACION("Secretaria_Conservacion",
        "CN=Secretaria_Conservacion,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    SUBDIRECTOR_CATASTRO("Subdirector_Catastro",
        "CN=Subdirector_Catastro,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC),
    TOPOGRAFO("Topografo", "CN=Topografo,OU=SIGC,OU=APLICACIONES," + Constantes.LDAP_DC);

    private String rol;
    private String distinguishedName;

    ERol(String rol) {
        this.rol = rol;
    }

    ERol(String rol, String distinguishedName) {
        this.rol = rol;
        this.distinguishedName = distinguishedName;
    }

    public String getRol() {
        return rol;
    }

    public String getDistinguishedName() {
        return distinguishedName;
    }

}
