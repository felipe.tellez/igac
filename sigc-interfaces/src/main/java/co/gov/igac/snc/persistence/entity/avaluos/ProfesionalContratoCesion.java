package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the PROFESIONAL_CONTRATO_CESION database table.
 */
/*
 * @modified christian.rodriguez agregado atributo profesionalAvaluosContrato
 */
@Entity
@Table(name = "PROFESIONAL_CONTRATO_CESION")
public class ProfesionalContratoCesion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5038696321515458457L;

    private Long id;
    private ProfesionalAvaluo cedeProfesional;
    private Date fecha;
    private Date fechaLog;
    private String motivo;
    private ProfesionalAvaluo recibeProfesional;
    private String usuarioLog;

    private ProfesionalAvaluosContrato profesionalAvaluosContrato;

    public ProfesionalContratoCesion() {
    }

    @Id
    @SequenceGenerator(name = "PROFESIONAL_CONTRATO_CESION_ID_GENERATOR", sequenceName =
        "PROFESIONAL_CONTRATO_CE_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "PROFESIONAL_CONTRATO_CESION_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CEDE_PROFESIONAL_ID")
    public ProfesionalAvaluo getCedeProfesional() {
        return this.cedeProfesional;
    }

    public void setCedeProfesional(ProfesionalAvaluo cedeProfesional) {
        this.cedeProfesional = cedeProfesional;
    }

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(nullable = false, length = 600)
    public String getMotivo() {
        return this.motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    // bi-directional many-to-one association to ProfesionalAvaluosContrato
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESIONAL_AVALUOS_CONTRAC_ID", nullable = false)
    public ProfesionalAvaluosContrato getProfesionalAvaluosContrato() {
        return this.profesionalAvaluosContrato;
    }

    public void setProfesionalAvaluosContrato(ProfesionalAvaluosContrato profesionalAvaluosContrato) {
        this.profesionalAvaluosContrato = profesionalAvaluosContrato;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECIBE_PROFESIONAL_ID")
    public ProfesionalAvaluo getRecibeProfesional() {
        return this.recibeProfesional;
    }

    public void setRecibeProfesional(ProfesionalAvaluo recibeProfesional) {
        this.recibeProfesional = recibeProfesional;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
