package co.gov.igac.snc.util;

public class Estadistica {

//--------------------------------------------------------------------------------------------------
    /**
     * Método que calcula la media de un conjunto de datos
     *
     * @param datos
     * @return media
     * @author javier.aponte
     */
    public static double media(double[] datos) {
        double suma = 0.0;
        for (int i = 0; i < datos.length; i++) {
            suma += datos[i];
        }
        return suma / datos.length;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que calcula la varianza de un conjunto de datos
     *
     * @param datos
     * @param media
     * @return varianza
     * @author javier.aponte
     */
    public static double varianza(double[] datos, double media) {
        double suma = 0.0;
        for (int i = 0; i < datos.length; i++) {
            suma += Math.pow(datos[i] - media, 2);
        }
        return suma / datos.length;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que calcula la asimetria (tercer momento) de un conjunto de datos
     *
     * @param datos
     * @param media
     * @return asimetria
     * @author javier.aponte
     */
    public static double asimetria(double[] datos, double media) {
        double suma = 0.0;
        for (int i = 0; i < datos.length; i++) {
            suma += Math.pow(datos[i] - media, 3);
        }
        return suma / datos.length;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que calcula el coeficiente de variación de Pearson de un conjunto de datos
     *
     * @param datos
     * @return coeficienteVariacion
     * @author javier.aponte
     */
    public static double coeficienteVariacion(double[] datos) {
        double media = Estadistica.media(datos);
        double desviacionEstandar = Math.sqrt(Estadistica.varianza(datos, media));
        return desviacionEstandar / media;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que calcula el coeficiente de asimetria de Fisher Es la medida de asimetría más
     * utilizada, parte del uso del tercer momento estándar γ1 = μ3 / σ3 μ3 asimetria tercer momento
     * en torno a la media σ desviacionEstandar
     *
     * @param datos
     * @param media
     * @return coeficienteAsimetria
     * @author javier.aponte
     */
    public static double coeficienteAsimetria(double[] datos, double media) {
        double asimetria = Estadistica.asimetria(datos, media);
        double desviacionEstandar = Math.sqrt(Estadistica.varianza(datos, media));
        return asimetria / Math.pow(desviacionEstandar, 3);
    }

}
