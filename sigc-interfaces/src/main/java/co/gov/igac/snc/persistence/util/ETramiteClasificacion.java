/**
 * < * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con las posibles clasificaciones de un trámite
 *
 * @author pedro.garcia
 */
public enum ETramiteClasificacion {
    OFICINA,
    TERRENO
}
