package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los código del dominio PREDIO_ESTADO
 *
 * @documented pedro.garcia
 */
public enum EPredioEstado {

    ACTIVO("ACTIVO"),
    //D: Este estado ya no se debe usar porque el bloqueo se maneja por las fechas de la tabla predio_bloqueo
    //BLOQUEADO ("BLOQUEADO"),
    CANCELADO("CANCELADO"),
    //lorena.salamanca :: 21-09-2015 :: Se agrega este estado para dar manejo a los predios cancelados que se reactivan en los tramites quinta omitido
    REACTIVADO("REACTIVADO");

    private String codigo;

    private EPredioEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
