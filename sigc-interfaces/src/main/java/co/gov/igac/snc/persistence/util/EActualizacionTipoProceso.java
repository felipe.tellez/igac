package co.gov.igac.snc.persistence.util;

/**
 * Enumeraciòn que identifica los tipos de procesos de actualización: FORMACION, ACTUALIZACION
 *
 * @author jamir.avila.
 *
 * @modified by franz.gamba -> se cambio la descripción de la actualizacion de "Actualización de la
 * formación catastral" a "Actualización catastral" debido al tamanio de la columna donde se
 * almacena la informacion.
 */
public enum EActualizacionTipoProceso {
    ACTUALIZACION("Actualización catastral"),
    FORMACION("Formación catastral");

    private String descripcion;

    EActualizacionTipoProceso(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
