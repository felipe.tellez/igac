package co.gov.igac.snc.persistence.util;

public enum EAvaluoClaseSuelo {

    CLASE_SUELO_1("Clase suelo 1", "1"),
    CLASE_SUELO_2("Clase suelo 2", "2"),
    CLASE_SUELO_3("Clase suelo 3", "3"),
    CLASE_SUELO_4("Clase suelo 4", "4"),
    CLASE_SUELO_5("Clase suelo 5", "5");

    private String valor;
    private String codigo;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    private EAvaluoClaseSuelo(String valor, String codigo) {
        this.valor = valor;
        this.codigo = codigo;
    }

}
