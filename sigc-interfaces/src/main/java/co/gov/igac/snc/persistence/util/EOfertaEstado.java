/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los estados de oferta inmobiliaria, corresponde al dominio
 * OFERTA_INMOBILIARIA_ESTADO
 *
 * @author christian.rodriguez
 */
public enum EOfertaEstado {

    // D: código (valor)
    APROBACION_CONDICIONADA("Aprobación condicionada"),
    APROBADA("Aprobada"),
    CONTROL_CALIDAD("En control de calidad"),
    CREADA("Creada"),
    FINALIZADA("Finalizada"),
    RECHAZADA("Rechazada");

    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private EOfertaEstado(String valor) {
        this.valor = valor;
    }

    public String getCodigo() {
        return this.toString();
    }

}
