package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles motivos de devolución de un trámite
 *
 * @author javier.aponte
 */
public enum ETramiteMotivoDevolucion {

    OTRO("Otro"),
    POR_DOCUMENTOS("Por documentos"),
    POR_RECLASIFICAR("Por reclasificar");

    private String motivo;

    private ETramiteMotivoDevolucion(String motivo) {
        this.motivo = motivo;
    }

    public String getMotivo() {
        return this.motivo;
    }
}
