/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los estados de un área captura de ofertas inmobiliarias, corresponde al dominio
 * AREA_CAPTURA_OFERTA_ESTADO
 *
 * @author christian.rodriguez
 */
public enum EOfertaAreaCapturaOfertaEstado {

    // D: código (valor)
    FINALIZADA("Finalizada exitosamente"),
    VIGENTE("Vigente para ofertas");

    private String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private EOfertaAreaCapturaOfertaEstado(String valor) {
        this.valor = valor;
    }

    public String getCodigo() {
        return this.toString();
    }

}
