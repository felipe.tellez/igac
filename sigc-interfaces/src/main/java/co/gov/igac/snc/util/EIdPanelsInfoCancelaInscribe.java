/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con los identificadores de los paneles de datos para el Subproceso de Validación -
 * Revisar Proyección - Información de cancelación inscripción
 *
 * @author juan.agudelo
 */
public enum EIdPanelsInfoCancelaInscribe {

    CONDICION_DE_PROPIEDAD("condicionPropiedadId"),
    DETALLE_DEL_AVALUO("detalleAvaluoId"),
    FICHA_MATRIZ("fichaMatrizId"),
    INSCRIPCIONES_O_DECRETOS("inscripcionesDecretosId"),
    PROPIETARIOS_Y_O_POSEEDORES("propietariosPoseedoresId"),
    UBICACION_DEL_PREDIO("ubicacionDelPredioId");

    private String id;

    private EIdPanelsInfoCancelaInscribe(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
