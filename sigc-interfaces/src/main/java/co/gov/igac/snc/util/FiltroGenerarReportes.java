/*
 * IGAC proyecto SNC
 */

package co.gov.igac.snc.util;


import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Filtro con los campos de los predios para la consulta de reportes prediales
 * @modify javier.barajas -> Creacion de una nueva clase llamada FiltroGenerarReportes para ajustes en la consulta
 * @modify javier.aponte  -> Cambios varios
 * @modified by leidy.gonzalez Se acoplo clase de acuerdo al nuevo diseño de base de datos
 * para reportes prediales.
 */
public class FiltroGenerarReportes implements Serializable {

  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FiltroGenerarReportes.class);
	
	/**
	 * Código de la tipo del reporte
	 */
	private String codigoTipoReporte;
	
	/**
	 * Código de la categoria del reporte
	 */
	private Long codigoTipoInforme;
	
	/**
	 * Código de la categoria del reporte
	*/
	private String tipoDeFechaCorte;
	
	/**
     * Determina el año en el que se va generar el reporte
     */
    private String anoReporte;
	
	/**
	 * Código de la categoria del reporte
	*/
	private String tipoPersona;
	
	private String territorialId;
	private String departamentoId;
	private String municipioId;

    /**
     * Segmentos del número predial inicial
     */
	private NumeroPredialPartes numeroPredialPartesInicial;
	
    /**
     * Segmentos del número predial final
     */
	private NumeroPredialPartes numeroPredialPartesFinal;
	
	
	 /**
     * segmentos del número predial inicial
     */
    private String numeroPredialS1;
    private String numeroPredialS2;
    private String numeroPredialS3;
    private String numeroPredialS4;
    private String numeroPredialS5;
    private String numeroPredialS6;
    private String numeroPredialS7;
    private String numeroPredialS8;
    private String numeroPredialS9;
    private String numeroPredialS10;
    private String numeroPredialS11;
    private String numeroPredialS12;
    
    /**
     * segmentos del número predial final
     */
    private String numeroPredialFS1;
    private String numeroPredialFS2;
    private String numeroPredialFS3;
    private String numeroPredialFS4;
    private String numeroPredialFS5;
    private String numeroPredialFS6;
    private String numeroPredialFS7;
    private String numeroPredialFS8;
    private String numeroPredialFS9;
    private String numeroPredialFS10;
    private String numeroPredialFS11;
    private String numeroPredialFS12;
	

    private String direccionPredio;
	private String numeroPredial;
	private String numeroPredialFinal;
	

	private String nombrePropietario;
	private String identificacionPropietario;
	private String digitoVerificacion;

   
	/**
     * datos de la búsqueda avanzada
     */
    private String numeroRegistro;
    private String numeroRegistroFinal;
    private String circuloRegistral;
    
    private String formato;
    
    private BigDecimal consecutivoCatastral;

	private List<String> usoConstruccionId;

	private List<String> zonaEconomicaId;
	private List<String> zonaFisicaId;
	private List<String> destinoPredioId;
	private List<String> tipoPredioId;
	private List<String> modoAdquisicionId;
	private List<String> tipificacionId;
	private List<String> tipoTitulo;
	private List<String> tipoPredioSelect;
	private List<String> destinoPredioSelect;
	private List<String> modoAdquisicionSelect;
	
	private List<String> usoConstruccionSelect;
	private List<String> tipoCalificacionSelect;
	private List<String> tipificacionConstruccionSelect;
   

	private BigDecimal areaTerrenoMin;
	private BigDecimal areaTerrenoMax;
    private BigDecimal areaConstruidaMin;
    private BigDecimal areaConstruidaMax;


    /**
     * adicionados por cambios en el procedimiento de búsqueda
     */
    private String tipoPersonaPropietario;
    private String tipoIdentificacionPropietario;

    

	
	private BigDecimal avaluoDesde;
    private BigDecimal avaluoHasta;

    /**
     * adicionados para manejo de matricula antigua
     */
    private String matriculaAntigua;
    private String libro;
    private String tomo;
    private String pagina;
    private String numero;
    private String año;
    
    /**
     * Determina la direccion
     */
    private String direccion;
    
    /**
     * Determina la entidad que realizo el bloqueo del predio
     */
    private String entidad;
    
    /**
     * Determina la fecha en la que el predio fue bloqueado
     */
    private Date fechaInicioBloqueo;
    
    /**
     * Determina la fecha en la que el predio fue bloqueado
     */
    private Date fechaDesbloqueo;
    
    /**
     * Determina si si el predio esta bloqueado
     */
    private String predioBloqueado;
    
    /**
     * Determina el tipo de bloqueo realizado al predio
     */
    private String tipoBloqueo;
    
    /**
     * Determina la fecha en la que fue enviado el bloqueo
     */
    private Date fechaEnvio;
    
    /**
     * Determina la fecha en la que el fue recibido el bloqueo
     */
    private Date fechaRecibido;
    
    /**
     * Determina el numero de radicacion que realizo el bloqueo del predio
     */
    private String numeroRadicacionBloqueo;


    //parametros estadisticos
	private String nacional;
	private String incluirUocs;
	private String consolidado;

    //--------    methods     ------------------------------------------------------------
    
   
//-------------------------------------------------------------------------------------------------
	public FiltroGenerarReportes() {
		this.tipoPredioSelect = new ArrayList<String>();
		this.destinoPredioSelect = new ArrayList<String>();
		this.modoAdquisicionSelect = new ArrayList<String>();
		this.usoConstruccionSelect = new ArrayList<String>();
		this.tipoCalificacionSelect = new ArrayList<String>();
		this.tipificacionConstruccionSelect = new ArrayList<String>();
		
	}
    
    /**
     * @author javier.barajas
     * Metodo para la consulta segun la lista de tipos de predio
     */
    
    public void addTipoPredio(String tipoPredio){
    	this.tipoPredioSelect.add(tipoPredio);
    	
    }
    
    /**
     * @author javier.barajas
     * Metodo para la consulta segun la lista de destinos de predio
     */
    
    public void addDestinoPredio(String destinoPredio){
    	this.destinoPredioSelect.add(destinoPredio);
    	
    }
    
    public void addModoAdquisicion(String modoAdquisicion){
    	this.modoAdquisicionSelect.add(modoAdquisicion);
    	
    }

	public String getNacional() {
		return nacional;
	}

	public void setNacional(String nacional) {
		this.nacional = nacional;
	}

	public String getIncluirUocs() {
		return incluirUocs;
	}

	public void setIncluirUocs(String incluirUocs) {
		this.incluirUocs = incluirUocs;
	}

	public String getConsolidado() {
		return consolidado;
	}

	public void setConsolidado(String consolidado) {
		this.consolidado = consolidado;
	}

	public void addUsoConstruccion(String usoConstruccion){
    	this.usoConstruccionSelect.add(usoConstruccion);
    	
    }
    public void addTipoCalificacion(String tipoCalificacion){
    	this.tipoCalificacionSelect.add(tipoCalificacion);
    	
    }
    public void addTipificacionConstruccion(String tipificacionConstruccion){
    	this.tipificacionConstruccionSelect.add(tipificacionConstruccion);
    	
    }
    
    /**
     * @author javier.barajas
     * Método que valida si hay algun criterio de busqueda
     * 
     */
    
    public boolean validaCriterios(){
    	boolean criteriosNoNulos=false;
    	if(this.getDepartamentoId()==null || this.getDepartamentoId().isEmpty()){
    		criteriosNoNulos=true;
    	}
    	if(this.getMunicipioId()==null || this.getMunicipioId().isEmpty()){
    		criteriosNoNulos=true;
    	}
    	if(this.numeroPredial==null || this.numeroPredial.isEmpty()){
    		criteriosNoNulos=true;
    	}
    	if(this.numeroPredialFinal==null || this.numeroPredialFinal.isEmpty()){
    		criteriosNoNulos=true;
    	}
    	if(this.getCirculoRegistral()==null || this.getCirculoRegistral().isEmpty()){
    		criteriosNoNulos=true;
    	}
    	if(this.getNumeroRegistro()==null || this.getNumeroRegistro().isEmpty()){
    		criteriosNoNulos=true;
    	}
    	if(this.getNumeroRegistro()==null || this.getNumeroRegistro().isEmpty()){
    		criteriosNoNulos=true;
    	}
    	return criteriosNoNulos;
    }
    
    
//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     * @return
     */
    public boolean isPersonaNatural() {
        boolean answer = false;
        if(this.tipoPersonaPropietario != null) {
            answer =
                (this.tipoPersonaPropietario.equals(EPersonaTipoPersona.NATURAL.getCodigo())) ?
                true : false;
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     * @return
     */
    public boolean isPersonaJuridica() {
        boolean answer = false;
        if(this.tipoPersonaPropietario != null) {
            answer =
                (this.tipoPersonaPropietario.equals(EPersonaTipoPersona.JURIDICA.getCodigo())) ?
                true : false;
        }

        return answer;
    }
    
    public boolean isEsNit() {
        boolean answer = false;
        if(this.tipoIdentificacionPropietario != null) {
            answer =
                (this.tipoIdentificacionPropietario.equals(EPersonaTipoIdentificacion.NIT.getCodigo())) ?
                true : false;
        }

        return answer;
    }
    
//---------gets y sets-----------------------------------------------------------------------------------------
       
   public List<String> getZonaEconomicaId() {
		return zonaEconomicaId;
	}

	public void setZonaEconomicaId(List<String> zonaEconomicaId) {
		this.zonaEconomicaId = zonaEconomicaId;
	}

	public List<String> getZonaFisicaId() {
		return zonaFisicaId;
	}

	public void setZonaFisicaId(List<String> zonaFisicaId) {
		this.zonaFisicaId = zonaFisicaId;
	}

	public String getNumeroPredialFS1() {
		return numeroPredialFS1;
	}

	public void setNumeroPredialFS1(String numeroPredialFS1) {
		this.numeroPredialFS1 = numeroPredialFS1;
	}

	public String getNumeroPredialFS2() {
		return numeroPredialFS2;
	}

	public void setNumeroPredialFS2(String numeroPredialFS2) {
		this.numeroPredialFS2 = numeroPredialFS2;
	}

	public String getNumeroPredialFS3() {
		return numeroPredialFS3;
	}

	public void setNumeroPredialFS3(String numeroPredialFS3) {
		this.numeroPredialFS3 = numeroPredialFS3;
	}

	public String getNumeroPredialFS4() {
		return numeroPredialFS4;
	}

	public void setNumeroPredialFS4(String numeroPredialFS4) {
		this.numeroPredialFS4 = numeroPredialFS4;
	}

	public String getNumeroPredialFS5() {
		return numeroPredialFS5;
	}

	public void setNumeroPredialFS5(String numeroPredialFS5) {
		this.numeroPredialFS5 = numeroPredialFS5;
	}

	public String getNumeroPredialFS6() {
		return numeroPredialFS6;
	}

	public void setNumeroPredialFS6(String numeroPredialFS6) {
		this.numeroPredialFS6 = numeroPredialFS6;
	}

	public String getNumeroPredialFS7() {
		return numeroPredialFS7;
	}

	public void setNumeroPredialFS7(String numeroPredialFS7) {
		this.numeroPredialFS7 = numeroPredialFS7;
	}

	public String getNumeroPredialFS8() {
		return numeroPredialFS8;
	}

	public void setNumeroPredialFS8(String numeroPredialFS8) {
		this.numeroPredialFS8 = numeroPredialFS8;
	}

	public String getNumeroPredialFS9() {
		return numeroPredialFS9;
	}

	public void setNumeroPredialFS9(String numeroPredialFS9) {
		this.numeroPredialFS9 = numeroPredialFS9;
	}

	public String getNumeroPredialFS10() {
		return numeroPredialFS10;
	}

	public void setNumeroPredialFS10(String numeroPredialFS10) {
		this.numeroPredialFS10 = numeroPredialFS10;
	}

	public String getNumeroPredialFS11() {
		return numeroPredialFS11;
	}

	public void setNumeroPredialFS11(String numeroPredialFS11) {
		this.numeroPredialFS11 = numeroPredialFS11;
	}

	public String getNumeroPredialFS12() {
		return numeroPredialFS12;
	}

	public void setNumeroPredialFS12(String numeroPredialFS12) {
		this.numeroPredialFS12 = numeroPredialFS12;
	}

	public String getNumeroPredialS1() {
		return numeroPredialS1;
	}

	public void setNumeroPredialS1(String numeroPredialS1) {
		this.numeroPredialS1 = numeroPredialS1;
	}

	public String getNumeroPredialS2() {
		return numeroPredialS2;
	}

	public void setNumeroPredialS2(String numeroPredialS2) {
		this.numeroPredialS2 = numeroPredialS2;
	}

	public String getNumeroPredialS3() {
		return numeroPredialS3;
	}

	public void setNumeroPredialS3(String numeroPredialS3) {
		this.numeroPredialS3 = numeroPredialS3;
	}

	public String getNumeroPredialS4() {
		return numeroPredialS4;
	}

	public void setNumeroPredialS4(String numeroPredialS4) {
		this.numeroPredialS4 = numeroPredialS4;
	}

	public String getNumeroPredialS5() {
		return numeroPredialS5;
	}

	public void setNumeroPredialS5(String numeroPredialS5) {
		this.numeroPredialS5 = numeroPredialS5;
	}

	public String getNumeroPredialS6() {
		return numeroPredialS6;
	}

	public void setNumeroPredialS6(String numeroPredialS6) {
		this.numeroPredialS6 = numeroPredialS6;
	}

	public String getNumeroPredialS7() {
		return numeroPredialS7;
	}

	public void setNumeroPredialS7(String numeroPredialS7) {
		this.numeroPredialS7 = numeroPredialS7;
	}

	public String getNumeroPredialS8() {
		return numeroPredialS8;
	}

	public void setNumeroPredialS8(String numeroPredialS8) {
		this.numeroPredialS8 = numeroPredialS8;
	}

	public String getNumeroPredialS9() {
		return numeroPredialS9;
	}

	public void setNumeroPredialS9(String numeroPredialS9) {
		this.numeroPredialS9 = numeroPredialS9;
	}

	public String getNumeroPredialS10() {
		return numeroPredialS10;
	}

	public void setNumeroPredialS10(String numeroPredialS10) {
		this.numeroPredialS10 = numeroPredialS10;
	}

	public String getNumeroPredialS11() {
		return numeroPredialS11;
	}

	public void setNumeroPredialS11(String numeroPredialS11) {
		this.numeroPredialS11 = numeroPredialS11;
	}

	public String getNumeroPredialS12() {
		return numeroPredialS12;
	}

	public void setNumeroPredialS12(String numeroPredialS12) {
		this.numeroPredialS12 = numeroPredialS12;
	}

	public String getTerritorialId() {
		return territorialId;
	}
	public void setTerritorialId(String territorialId) {
		this.territorialId = territorialId;
	}
	public String getDepartamentoId() {
		return departamentoId;
	}
	public void setDepartamentoId(String departamentoId) {
		this.departamentoId = departamentoId;
	}
	public String getMunicipioId() {
		return municipioId;
	}
	public void setMunicipioId(String municipioId) {
		this.municipioId = municipioId;
	}
	public String getDireccionPredio() {
		return direccionPredio;
	}
	public void setDireccionPredio(String direccionPredio) {
		this.direccionPredio = direccionPredio;
	}
	public String getNumeroPredial() {
		return numeroPredial;
	}
	public void setNumeroPredial(String numeroPredial) {
		this.numeroPredial = numeroPredial;
	}
	public String getNumeroPredialFinal() {
		return numeroPredialFinal;
	}
	public void setNumeroPredialFinal(String numeroPredialFinal) {
		this.numeroPredialFinal = numeroPredialFinal;
	}
	public String getNombrePropietario() {
		return nombrePropietario;
	}
	public void setNombrePropietario(String nombrePropietario) {
		this.nombrePropietario = nombrePropietario;
	}
	public String getIdentificacionPropietario() {
		return identificacionPropietario;
	}
	public void setIdentificacionPropietario(String identificacionPropietario) {
		this.identificacionPropietario = identificacionPropietario;
	}
	public String getDigitoVerificacion() {
		return digitoVerificacion;
	}
	public void setDigitoVerificacion(String digitoVerificacion) {
		this.digitoVerificacion = digitoVerificacion;
	}
	public String getNumeroRegistro() {
		return numeroRegistro;
	}
	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	public String getCirculoRegistral() {
		return circuloRegistral;
	}
	public void setCirculoRegistral(String circuloRegistral) {
		this.circuloRegistral = circuloRegistral;
	}
	public BigDecimal getConsecutivoCatastral() {
		return consecutivoCatastral;
	}
	public void setConsecutivoCatastral(BigDecimal consecutivoCatastral) {
		this.consecutivoCatastral = consecutivoCatastral;
	}
	public List<String> getUsoConstruccionId() {
		return usoConstruccionId;
	}
	public void setUsoConstruccionId(List<String> usoConstruccionId) {
		this.usoConstruccionId = usoConstruccionId;
	}
	public List<String> getDestinoPredioId() {
		return destinoPredioId;
	}
	public void setDestinoPredioId(List<String> destinoPredioId) {
		this.destinoPredioId = destinoPredioId;
	}
	public List<String> getTipoPredioId() {
		return tipoPredioId;
	}
	public void setTipoPredioId(List<String> tipoPredioId) {
		this.tipoPredioId = tipoPredioId;
	}
	public List<String> getModoAdquisicionId() {
		return modoAdquisicionId;
	}
	public void setModoAdquisicionId(List<String> modoAdquisicionId) {
		this.modoAdquisicionId = modoAdquisicionId;
	}
	public List<String> getTipificacionId() {
		return tipificacionId;
	}
	public void setTipificacionId(List<String> tipificacionId) {
		this.tipificacionId = tipificacionId;
	}

	public List<String> getTipoTitulo() {
		return tipoTitulo;
	}

	public void setTipoTitulo(List<String> tipoTitulo) {
		this.tipoTitulo = tipoTitulo;
	}

	public List<String> getTipoPredioSelect() {
		return tipoPredioSelect;
	}
	public void setTipoPredioSelect(List<String> tipoPredioSelect) {
		this.tipoPredioSelect = tipoPredioSelect;
	}
	public List<String> getDestinoPredioSelect() {
		return destinoPredioSelect;
	}
	public void setDestinoPredioSelect(List<String> destinoPredioSelect) {
		this.destinoPredioSelect = destinoPredioSelect;
	}
	public List<String> getModoAdquisicionSelect() {
		return modoAdquisicionSelect;
	}
	public void setModoAdquisicionSelect(List<String> modoAdquisicionSelect) {
		this.modoAdquisicionSelect = modoAdquisicionSelect;
	}
	public List<String> getUsoConstruccionSelect() {
		return usoConstruccionSelect;
	}
	public void setUsoConstruccionSelect(List<String> usoConstruccionSelect) {
		this.usoConstruccionSelect = usoConstruccionSelect;
	}
	public List<String> getTipoCalificacionSelect() {
		return tipoCalificacionSelect;
	}
	public void setTipoCalificacionSelect(List<String> tipoCalificacionSelect) {
		this.tipoCalificacionSelect = tipoCalificacionSelect;
	}
	public List<String> getTipificacionConstruccionSelect() {
		return tipificacionConstruccionSelect;
	}
	public void setTipificacionConstruccionSelect(
			List<String> tipificacionConstruccionSelect) {
		this.tipificacionConstruccionSelect = tipificacionConstruccionSelect;
	}
	public String getTipoPersonaPropietario() {
		return tipoPersonaPropietario;
	}
	public void setTipoPersonaPropietario(String tipoPersonaPropietario) {
		this.tipoPersonaPropietario = tipoPersonaPropietario;
	}
	public String getTipoIdentificacionPropietario() {
		return tipoIdentificacionPropietario;
	}
	public void setTipoIdentificacionPropietario(
			String tipoIdentificacionPropietario) {
		this.tipoIdentificacionPropietario = tipoIdentificacionPropietario;
	}
	
	public BigDecimal getAreaTerrenoMin() {
		return areaTerrenoMin;
	}

	public void setAreaTerrenoMin(BigDecimal areaTerrenoMin) {
		this.areaTerrenoMin = areaTerrenoMin;
	}

	public BigDecimal getAreaTerrenoMax() {
		return areaTerrenoMax;
	}

	public void setAreaTerrenoMax(BigDecimal areaTerrenoMax) {
		this.areaTerrenoMax = areaTerrenoMax;
	}

	public BigDecimal getAreaConstruidaMin() {
		return areaConstruidaMin;
	}

	public void setAreaConstruidaMin(BigDecimal areaConstruidaMin) {
		this.areaConstruidaMin = areaConstruidaMin;
	}

	public BigDecimal getAreaConstruidaMax() {
		return areaConstruidaMax;
	}

	public void setAreaConstruidaMax(BigDecimal areaConstruidaMax) {
		this.areaConstruidaMax = areaConstruidaMax;
	}

	public BigDecimal getAvaluoDesde() {
		return avaluoDesde;
	}

	public void setAvaluoDesde(BigDecimal avaluoDesde) {
		this.avaluoDesde = avaluoDesde;
	}

	public BigDecimal getAvaluoHasta() {
		return avaluoHasta;
	}

	public void setAvaluoHasta(BigDecimal avaluoHasta) {
		this.avaluoHasta = avaluoHasta;
	}

	public String getMatriculaAntigua() {
		return matriculaAntigua;
	}

	public void setMatriculaAntigua() {
		if(this.libro!= null){
			this.matriculaAntigua=this.libro;
			if(this.tomo != null){
				this.matriculaAntigua=this.libro+this.tomo;
			}
			if(this.pagina != null){
				this.matriculaAntigua=this.libro+this.tomo+this.pagina;
			}
			if(this.numero!= null){
				this.matriculaAntigua=this.libro+this.tomo+this.pagina+this.numero;
			}
			if(this.año!= null){
				this.matriculaAntigua=this.libro+this.tomo+this.pagina+this.numero+this.año;
			}
		}
	}

	public String getLibro() {
		return libro;
	}

	public void setLibro(String libro) {
		this.libro = libro;
	}

	public String getTomo() {
		return tomo;
	}

	public void setTomo(String tomo) {
		this.tomo = tomo;
	}

	public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getAño() {
		return año;
	}

	public void setAño(String año) {
		this.año = año;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	//----fin ---gets y sets-------------------------------------------------------
	public String todosDestinos() {
    	//LOGGER.debug("on FiltroGenerarReportes#getTodoDestinos....");
        String destinos = new String();
        int temp=0;
        for(String d:this.destinoPredioSelect){
        	if(temp==1){
        		temp=1;
        		destinos = destinos + ",'" + d + "'";
        		}
        	else {
        	destinos = destinos + "'" + d + "'";
        	temp=1;
        	}
         }
       
       
        //LOGGER.debug("Destinos: "+destinos);
        return destinos;
    }

    public String todosTipos() {
    	//LOGGER.debug("on FiltroGenerarReportes#getTodoTipos....");
        String tipos = new String();
        int temp=0;
        for(String t:this.tipoPredioSelect){
        	
        	if(temp==1){
        		temp=1;
        		tipos = tipos + ",'" + t + "'";
        		}
        	else {
        		tipos = tipos + "'" + t + "'";
        	temp=1;
        	}
         }
        //LOGGER.debug("Tipos: "+tipos);
        return tipos;
    }

    public String todosModoAdquision() {
    	//LOGGER.debug("on FiltroGenerarReportes#getTodoModoAdquision....");
        String modos = new String();
        int temp=0;
        for(String m:this.modoAdquisicionSelect){
        	if(temp==1){
        		temp=1;
        		modos = modos + ",'" + m + "'";
        		}
        	else {
        		modos = modos + "'" + m + "'";
        	temp=1;
        	}
         }
        //LOGGER.debug("Modos: "+modos);
        return modos;
    }
    public String todosUsoConstruccion() {
    	//LOGGER.debug("on FiltroGenerarReportes#getTodoUsoConstruccion....");
        String usos = new String();
        int temp=0;
        for(String u:this.usoConstruccionSelect){
        	if(temp==1){
        		temp=1;
        		usos = usos + ",'" + u + "'";
        		}
        	else {
        		usos = usos + "'" + u + "'";
        		temp=1;
        	}
         }
        //LOGGER.debug("Usos: "+usos);
        return usos;
    }
    public String todosTipoCalificacion() {
    	//LOGGER.debug("on FiltroGenerarReportes#getTodoTipoCalificacion....");
        String tiposca = new String();
        int temp=0;
        for(String ti:this.tipoCalificacionSelect){
        	if(temp==1){
        		temp=1;
        		tiposca = tiposca + ",'" + ti + "'";
        		}
        	else {
        		tiposca = tiposca + "'" + ti + "'";
        		temp=1;
        	}
         }
        //LOGGER.debug("Tipos Calificacion: "+tiposca);
        return tiposca;
    }
    public String todosTipificacion() {
    	//LOGGER.debug("on FiltroGenerarReportes#getTodoTipificacion....");
        String tipificaciones = new String();
        int temp=0;
        for(String tpf:this.tipificacionConstruccionSelect){
        	if(temp==1){
        		temp=1;
        		tipificaciones = tipificaciones + ",'" + tpf + "'";
        		}
        	else {
        		tipificaciones = tipificaciones + "'" + tpf + "'";
        		temp=1;
        	}
         }
        //LOGGER.debug("Tipificaciones: "+tipificaciones);
        return tipificaciones;
    }

	public NumeroPredialPartes getNumeroPredialPartesInicial() {
		return numeroPredialPartesInicial;
	}

	public void setNumeroPredialPartesInicial(
			NumeroPredialPartes numeroPredialPartesInicial) {
		this.numeroPredialPartesInicial = numeroPredialPartesInicial;
		this.numeroPredialPartesInicial.assembleNumeroPredialFromSegments();
	}

	public NumeroPredialPartes getNumeroPredialPartesFinal() {
		return numeroPredialPartesFinal;
	}

	public void setNumeroPredialPartesFinal(
			NumeroPredialPartes numeroPredialPartesFinal) {
		this.numeroPredialPartesFinal = numeroPredialPartesFinal;
		this.numeroPredialPartesFinal.assembleNumeroPredialFromSegments();
	}
	
	/**
	 * Permite la lectura de un numero predial en sus componentes
	 * 
	 * @author leidy.gonzalez
	 * @param numeroPredial
	 *            cadena con el numero predial completo
	 */
	public void assembleNumeroPredialFromSegments() {
        StringBuilder numeroPredialAssembled;
        numeroPredialAssembled = new StringBuilder();
        if (this.numeroPredialS1 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS1);
        }
        if (this.numeroPredialS2 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS2);
        }
        if (this.numeroPredialS3 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS3);
        }
        if (this.numeroPredialS4 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS4);
        }
        if (this.numeroPredialS5 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS5);
        }
        if (this.numeroPredialS6 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS6);
        }
        if (this.numeroPredialS7 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS7);
        }
        if (this.numeroPredialS8 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS8);
        }
        if (this.numeroPredialS9 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS9);
        }
        if (this.numeroPredialS10 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS10);
        }
        if (this.numeroPredialS11 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS11);
        }
        if (this.numeroPredialS12 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS12);
        }
        this.numeroPredial = numeroPredialAssembled.toString();
        
        
        StringBuilder numeroPredialFinalAssembled;
        numeroPredialFinalAssembled = new StringBuilder();
        if (this.numeroPredialFS1 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS1);
        }
        if (this.numeroPredialFS2 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS2);
        }
        if (this.numeroPredialFS3 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS3);
        }
        if (this.numeroPredialFS4 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS4);
        }
        if (this.numeroPredialFS5 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS5);
        }
        if (this.numeroPredialFS6 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS6);
        }
        if (this.numeroPredialFS7 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS7);
        }
        if (this.numeroPredialFS8 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS8);
        }
        if (this.numeroPredialFS9 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS9);
        }
        if (this.numeroPredialFS10 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS10);
        }
        if (this.numeroPredialFS11 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS11);
        }
        if (this.numeroPredialFS12 == null) {
            numeroPredialFinalAssembled.append("");
        } else {
            numeroPredialFinalAssembled.append(this.numeroPredialFS12);
        }
        this.numeroPredialFinal = numeroPredialFinalAssembled.toString();
    }
    
	// --------------------------------------------------------------------------------------------------
    /**
	 * Separa un numero predial en sus componentes
	 * 
	 * @author leidy.gonzalez
	 * @param numeroPredial
	 *            cadena con el numero predial completo
	 */
	public void disassembleNumeroPredialInSegments(String numeroPredial) {
		
		this.numeroPredialS1 = numeroPredial.substring(0,2);
		this.numeroPredialS2 = numeroPredial.substring(2,5);
		this.numeroPredialS3 = numeroPredial.substring(5,7);
		this.numeroPredialS4 = numeroPredial.substring(7,9);
		this.numeroPredialS5 = numeroPredial.substring(9,11);
		this.numeroPredialS6 = numeroPredial.substring(11,13);
		this.numeroPredialS7 = numeroPredial.substring(13,17);
		this.numeroPredialS8 = numeroPredial.substring(17,21);
		this.numeroPredialS9 = numeroPredial.substring(21,22);
		this.numeroPredialS10 = numeroPredial.substring(22,24);
		this.numeroPredialS11 = numeroPredial.substring(24,26);
		this.numeroPredialS12 = numeroPredial.substring(26,30);
		
		this.numeroPredial = numeroPredial;
	}
    
  // --------------------------------------------------------------------------------------------------
	/**
     * Método que valida que todos los segmentos del número predial sean válidos
     * Éste método se debe ejecutar antes del assembleNumeroPredialFromSegments
     * para que no concatene null por error.
     * @author leidy.gonzalez
     */
    public boolean validateSegmentsNumeroPredial() {
    	
    	if(this.numeroPredialS1 == null || this.numeroPredialS1.trim().isEmpty()) return false;
    	if(this.numeroPredialS2 == null || this.numeroPredialS2.trim().isEmpty()) return false;
    	if(this.numeroPredialS3 == null || this.numeroPredialS3.trim().isEmpty()) return false;
    	if(this.numeroPredialS4 == null || this.numeroPredialS4.trim().isEmpty()) return false;
    	if(this.numeroPredialS5 == null || this.numeroPredialS5.trim().isEmpty()) return false;
    	if(this.numeroPredialS6 == null || this.numeroPredialS6.trim().isEmpty()) return false;
    	if(this.numeroPredialS7 == null || this.numeroPredialS7.trim().isEmpty()) return false;
    	if(this.numeroPredialS8 == null || this.numeroPredialS8.trim().isEmpty()) return false;
    	if(this.numeroPredialS9 == null || this.numeroPredialS9.trim().isEmpty()) return false;
    	if(this.numeroPredialS10 == null || this.numeroPredialS10.trim().isEmpty()) return false;
    	if(this.numeroPredialS11 == null || this.numeroPredialS11.trim().isEmpty()) return false;
    	if(this.numeroPredialS12 == null || this.numeroPredialS12.trim().isEmpty()) return false;
    	
    	if(this.numeroPredialFS1 == null || this.numeroPredialFS1.trim().isEmpty()) return false;
    	if(this.numeroPredialFS2 == null || this.numeroPredialFS2.trim().isEmpty()) return false;
    	if(this.numeroPredialFS3 == null || this.numeroPredialFS3.trim().isEmpty()) return false;
    	if(this.numeroPredialFS4 == null || this.numeroPredialFS4.trim().isEmpty()) return false;
    	if(this.numeroPredialFS5 == null || this.numeroPredialFS5.trim().isEmpty()) return false;
    	if(this.numeroPredialFS6 == null || this.numeroPredialFS6.trim().isEmpty()) return false;
    	if(this.numeroPredialFS7 == null || this.numeroPredialFS7.trim().isEmpty()) return false;
    	if(this.numeroPredialFS8 == null || this.numeroPredialFS8.trim().isEmpty()) return false;
    	if(this.numeroPredialFS9 == null || this.numeroPredialFS9.trim().isEmpty()) return false;
    	if(this.numeroPredialFS10 == null || this.numeroPredialFS10.trim().isEmpty()) return false;
    	if(this.numeroPredialFS11 == null || this.numeroPredialFS11.trim().isEmpty()) return false;
    	if(this.numeroPredialFS12 == null || this.numeroPredialFS12.trim().isEmpty()) return false;
    	return true;
    }

  //--------------------------------------------------------------------------------------------------
    public String getCodigoTipoReporte() {
		return codigoTipoReporte;
	}

	public void setCodigoTipoReporte(String codigoTipoReporte) {
		this.codigoTipoReporte = codigoTipoReporte;
	}
    

	public Long getCodigoTipoInforme() {
		return codigoTipoInforme;
	}

	public void setCodigoTipoInforme(Long codigoTipoInforme) {
		this.codigoTipoInforme = codigoTipoInforme;
	}

	public String getTipoDeFechaCorte() {
		return tipoDeFechaCorte;
	}

	public void setTipoDeFechaCorte(String tipoDeFechaCorte) {
		this.tipoDeFechaCorte = tipoDeFechaCorte;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getAnoReporte() {
		return anoReporte;
	}

	public void setAnoReporte(String anoReporte) {
		this.anoReporte = anoReporte;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public Date getFechaInicioBloqueo() {
		return fechaInicioBloqueo;
	}

	public void setFechaInicioBloqueo(Date fechaInicioBloqueo) {
		this.fechaInicioBloqueo = fechaInicioBloqueo;
	}

	public Date getFechaDesbloqueo() {
		return fechaDesbloqueo;
	}

	public void setFechaDesbloqueo(Date fechaDesbloqueo) {
		this.fechaDesbloqueo = fechaDesbloqueo;
	}

	public String getPredioBloqueado() {
		return predioBloqueado;
	}

	public void setPredioBloqueado(String predioBloqueado) {
		this.predioBloqueado = predioBloqueado;
	}

	public String getTipoBloqueo() {
		return tipoBloqueo;
	}

	public void setTipoBloqueo(String tipoBloqueo) {
		this.tipoBloqueo = tipoBloqueo;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Date getFechaRecibido() {
		return fechaRecibido;
	}

	public void setFechaRecibido(Date fechaRecibido) {
		this.fechaRecibido = fechaRecibido;
	}

	public String getNumeroRegistroFinal() {
		return numeroRegistroFinal;
	}

	public void setNumeroRegistroFinal(String numeroRegistroFinal) {
		this.numeroRegistroFinal = numeroRegistroFinal;
	}

	public String getNumeroRadicacionBloqueo() {
		return numeroRadicacionBloqueo;
	}

	public void setNumeroRadicacionBloqueo(String numeroRadicacionBloqueo) {
		this.numeroRadicacionBloqueo = numeroRadicacionBloqueo;
	} 
	
	
//end of class
}
