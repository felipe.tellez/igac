package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.persistence.entity.generales.Municipio;

/**
 * The primary key class for the ESTADISTICA_GERENCIAL_MANZANA database table.
 *
 */
@Embeddable
public class EstadisticaGerencialManzanaPK implements Serializable {
    //default serial version id, required for serializable classes.

    private static final long serialVersionUID = 1L;
    private Municipio municipio;
    private Integer anio;
    private String zona;
    private String sector;

    public EstadisticaGerencialManzanaPK() {
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public Integer getAnio() {
        return this.anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public String getZona() {
        return this.zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getSector() {
        return this.sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof EstadisticaGerencialManzanaPK)) {
            return false;
        }
        EstadisticaGerencialManzanaPK castOther = (EstadisticaGerencialManzanaPK) other;
        return this.municipio.equals(castOther.municipio) &&
            (this.anio == castOther.anio) &&
            this.zona.equals(castOther.zona) &&
            this.sector.equals(castOther.sector);
    }

    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.municipio.hashCode();
        hash = hash * prime + ((int) (this.anio ^ (this.anio >>> 32)));
        hash = hash * prime + this.zona.hashCode();
        hash = hash * prime + this.sector.hashCode();

        return hash;
    }
}
