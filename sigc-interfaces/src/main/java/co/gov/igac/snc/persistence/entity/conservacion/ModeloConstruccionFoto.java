package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the MODELO_CONSTRUCCION_FOTO database table.
 *
 * @modified juan.agudelo 21-03-12 Adición de la secuencia MODELO_CONSTRUCCION_FOT_ID_SEQ, creación
 * de la clase cambio del objeto fotoDocumento a tipo Foto
 */
/*
 * @modified felipe.cadena::22-07-2015::La relacion del campo fotoDocumento esta definida en la base
 * de datos con latabla documento no contra la tabla foto, por esto se ajusta el mapeo y se agrega
 * la relacion explicita
 */
@Entity
@Table(name = "MODELO_CONSTRUCCION_FOTO")
public class ModeloConstruccionFoto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8800874891912003605L;

    private long id;
    private Date fechaLog;
    private Documento fotoDocumento;
    private String tipo;
    private String usuarioLog;
    private FmModeloConstruccion fmModeloConstruccion;

    public ModeloConstruccionFoto() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MODELO_CONSTRUCCION_FOT_ID_SEQ")
    @SequenceGenerator(name = "MODELO_CONSTRUCCION_FOT_ID_SEQ", sequenceName =
        "MODELO_CONSTRUCCION_FOT_ID_SEQ", allocationSize = 1)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FOTO_DOCUMENTO_ID")
    public Documento getFotoDocumento() {
        return this.fotoDocumento;
    }

    public void setFotoDocumento(Documento fotoDocumento) {
        this.fotoDocumento = fotoDocumento;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to FmModeloConstruccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FM_MODELO_CONSTRUCCION_ID")
    public FmModeloConstruccion getFmModeloConstruccion() {
        return this.fmModeloConstruccion;
    }

    public void setFmModeloConstruccion(
        FmModeloConstruccion fmModeloConstruccion) {
        this.fmModeloConstruccion = fmModeloConstruccion;
    }

    /**
     * Retorna una entidad tipo PModeloConstruccionFoto a partir del objeto actual se usa para hacer
     * operaciones definidas sobre proyecciones, pero que no existe proyeccion en BD
     *
     *
     * @author felipe.cadena
     *
     * @return
     */
    public PModeloConstruccionFoto obtenerProyectadoDesdeOriginal() {

        PModeloConstruccionFoto resultado = new PModeloConstruccionFoto();

        //resultado.setDescripcion(tipo);
        resultado.setFechaLog(this.fechaLog);
        resultado.setId(this.id);
        if (this.fotoDocumento != null) {
            resultado.setFotoDocumento(this.fotoDocumento);
        }
        resultado.setTipo(this.tipo);
        resultado.setUsuarioLog(this.usuarioLog);

        return resultado;

    }

}
