package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * The persistent class for the SOLICITUD_DOCUMENTACION database table.
 *
 * @modified felipe.cadena 09-08-2012 se agrega atributo transient documentosSolicitud para asociar
 * varios documentos a una solicitud. Sequencia del ID
 *
 * rodrigo.hernandez 30-08-2012 ->	cambio de long a Long en el atributo id -> adicion de metodo
 * equals
 *
 *
 */
@Entity
@Table(name = "SOLICITUD_DOCUMENTACION")
public class SolicitudDocumentacion implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String adicional;
    private String aportado;
    private String detalle;
    private Date fechaAporte;
    private Date fechaLog;
    private String requerido;
    private Solicitud solicitud;
    private Documento soporteDocumentoId;
    private String usuarioLog;
    private TipoDocumento tipoDocumento;

    private List<Documento> documentosSolicitud;

    public SolicitudDocumentacion() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SolicitudDocumentacion_ID_SEQ")
    @SequenceGenerator(name = "SolicitudDocumentacion_ID_SEQ", sequenceName =
        "SOLICITUD_DOCUMENTACION_ID_SEQ", allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ADICIONAL", length = 2)
    public String getAdicional() {
        return this.adicional;
    }

    public void setAdicional(String adicional) {
        this.adicional = adicional;
    }

    @Column(name = "APORTADO", nullable = false, length = 2)
    public String getAportado() {
        return this.aportado;
    }

    public void setAportado(String aportado) {
        this.aportado = aportado;
    }

    @Column(name = "DETALLE", length = 2000)
    public String getDetalle() {
        return this.detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_APORTE")
    public Date getFechaAporte() {
        return this.fechaAporte;
    }

    public void setFechaAporte(Date fechaAporte) {
        this.fechaAporte = fechaAporte;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "REQUERIDO", length = 2)
    public String getRequerido() {
        return this.requerido;
    }

    public void setRequerido(String requerido) {
        this.requerido = requerido;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_ID", nullable = false)
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID")
    @OneToOne(fetch = FetchType.LAZY)
    public Documento getSoporteDocumentoId() {
        return this.soporteDocumentoId;
    }

    public void setSoporteDocumentoId(Documento soporteDocumentoId) {
        this.soporteDocumentoId = soporteDocumentoId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_DOCUMENTO_ID", nullable = false)
    public TipoDocumento getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    // -------------------------------------------------------------------------
    @Transient
    public List<Documento> getDocumentosSolicitud() {
        return documentosSolicitud;
    }

    public void setDocumentosSolicitud(List<Documento> documentosSolicitud) {
        this.documentosSolicitud = documentosSolicitud;
    }

    // -------------------------------------------------------------------------
    @Transient
    public String getEstaSolicitado() {
        String result = "";

        if (this.id != null) {
            result = ESiNo.SI.getCodigo();
        } else {
            result = ESiNo.NO.getCodigo();
        }

        return result;
    }

    //--------------------------------------------------------------------------
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        SolicitudDocumentacion other = (SolicitudDocumentacion) obj;

        if (id == null) {
            if (other.id != null) {
                return false;
            } else {
                if (this.getTipoDocumento().getId().equals(other.getTipoDocumento().getId())) {
                    return true;
                } else {
                    return false;
                }
            }

        } else if (!id.equals(other.id)) {
            return false;
        }

        return true;
    }
}
