package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the REP_REPORTE_EJECUCION_DATOS database table.
 *
 */
@Entity
@Table(name = "REP_REPORTE_EJECUCION_DATOS")
public class RepReporteEjecucionDato implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private BigDecimal areaConstruccion;
    private BigDecimal areaConstruccionConvComun;
    private BigDecimal areaConstruccionConvPrivada;
    private BigDecimal areaConstruccionNoconvComun;
    private BigDecimal areaConstruccionNoconvPriv;
    private BigDecimal areaTerreno;
    private BigDecimal areaTerrenoComun;
    private BigDecimal areaTerrenoPrivado;
    private String circuloRegistral;
    private BigDecimal coeficienteCopropiedad;
    private String departamentoCodigo;
    private String destino;
    private String direccion;
    private String municipioCodigo;
    private String nombre;
    private String numeroIdentificacion;
    private String numeroPredial20;
    private String numeroPredial30;
    private String numeroRegistro;
    private String numeroRegistroAnterior;
    private String perSigla;
    private BigDecimal ppParticipacion;
    private String pppDepartamentoCodigo;
    private String pppEntidadEmisora;
    private Date pppFechaRegistro;
    private Date pppFechaTitulo;
    private String pppModoAdquisicion;
    private String pppMunicipioCodigo;
    private String pppNumeroTitulo;
    private String pppTipoTitulo;
    private BigDecimal pppValor;
    private BigDecimal predioId;
    private String tipo;
    private String tipoIdentificacion;
    private BigDecimal ucAreaConstruida;
    private BigDecimal ucAvaluo;
    private BigDecimal ucCalificacionAnexoId;
    private String ucTipificacion;
    private BigDecimal ucTotalBanios;
    private BigDecimal ucTotalHabitaciones;
    private BigDecimal ucTotalLocales;
    private BigDecimal ucTotalPisosUnidad;
    private BigDecimal ucTotalPuntaje;
    private String ucUnidad;
    private BigDecimal ucUsoId;
    private BigDecimal valorAvaluo;
    private BigDecimal valorConsConvencional;
    private BigDecimal valorConsNoConvencional;
    private BigDecimal valorConstruccionComun;
    private BigDecimal valorTerrenoComun;
    private BigDecimal valorTerrenoPrivado;
    private BigDecimal valorTotalAvaluoCatastral;
    private BigDecimal valorTotalAvaluoVigencia;
    private BigDecimal valorTotalConsPrivada;
    private BigDecimal valorTotalConstruccion;
    private BigDecimal valorTotalTerreno;
    private Date vigencia;
    private BigDecimal zonaArea;
    private String zonaFisica;
    private String zonaGeoeconomica;
    private RepReporteEjecucion repReporteEjecucion;

    public RepReporteEjecucionDato() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REP_REPORTE_EJECUCI_DAT_ID_SEQ")
    @SequenceGenerator(name = "REP_REPORTE_EJECUCI_DAT_ID_SEQ", sequenceName =
        "REP_REPORTE_EJECUCI_DAT_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA_CONSTRUCCION")
    public BigDecimal getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(BigDecimal areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "AREA_CONSTRUCCION_CONV_COMUN")
    public BigDecimal getAreaConstruccionConvComun() {
        return this.areaConstruccionConvComun;
    }

    public void setAreaConstruccionConvComun(BigDecimal areaConstruccionConvComun) {
        this.areaConstruccionConvComun = areaConstruccionConvComun;
    }

    @Column(name = "AREA_CONSTRUCCION_CONV_PRIVADA")
    public BigDecimal getAreaConstruccionConvPrivada() {
        return this.areaConstruccionConvPrivada;
    }

    public void setAreaConstruccionConvPrivada(BigDecimal areaConstruccionConvPrivada) {
        this.areaConstruccionConvPrivada = areaConstruccionConvPrivada;
    }

    @Column(name = "AREA_CONSTRUCCION_NOCONV_COMUN")
    public BigDecimal getAreaConstruccionNoconvComun() {
        return this.areaConstruccionNoconvComun;
    }

    public void setAreaConstruccionNoconvComun(BigDecimal areaConstruccionNoconvComun) {
        this.areaConstruccionNoconvComun = areaConstruccionNoconvComun;
    }

    @Column(name = "AREA_CONSTRUCCION_NOCONV_PRIV")
    public BigDecimal getAreaConstruccionNoconvPriv() {
        return this.areaConstruccionNoconvPriv;
    }

    public void setAreaConstruccionNoconvPriv(BigDecimal areaConstruccionNoconvPriv) {
        this.areaConstruccionNoconvPriv = areaConstruccionNoconvPriv;
    }

    @Column(name = "AREA_TERRENO")
    public BigDecimal getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(BigDecimal areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "AREA_TERRENO_COMUN")
    public BigDecimal getAreaTerrenoComun() {
        return this.areaTerrenoComun;
    }

    public void setAreaTerrenoComun(BigDecimal areaTerrenoComun) {
        this.areaTerrenoComun = areaTerrenoComun;
    }

    @Column(name = "AREA_TERRENO_PRIVADO")
    public BigDecimal getAreaTerrenoPrivado() {
        return this.areaTerrenoPrivado;
    }

    public void setAreaTerrenoPrivado(BigDecimal areaTerrenoPrivado) {
        this.areaTerrenoPrivado = areaTerrenoPrivado;
    }

    @Column(name = "CIRCULO_REGISTRAL")
    public String getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(String circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "COEFICIENTE_COPROPIEDAD")
    public BigDecimal getCoeficienteCopropiedad() {
        return this.coeficienteCopropiedad;
    }

    public void setCoeficienteCopropiedad(BigDecimal coeficienteCopropiedad) {
        this.coeficienteCopropiedad = coeficienteCopropiedad;
    }

    @Column(name = "DEPARTAMENTO_CODIGO")
    public String getDepartamentoCodigo() {
        return this.departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "MUNICIPIO_CODIGO")
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "NUMERO_IDENTIFICACION")
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "NUMERO_PREDIAL_20")
    public String getNumeroPredial20() {
        return this.numeroPredial20;
    }

    public void setNumeroPredial20(String numeroPredial20) {
        this.numeroPredial20 = numeroPredial20;
    }

    @Column(name = "NUMERO_PREDIAL_30")
    public String getNumeroPredial30() {
        return this.numeroPredial30;
    }

    public void setNumeroPredial30(String numeroPredial30) {
        this.numeroPredial30 = numeroPredial30;
    }

    @Column(name = "NUMERO_REGISTRO")
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "NUMERO_REGISTRO_ANTERIOR")
    public String getNumeroRegistroAnterior() {
        return this.numeroRegistroAnterior;
    }

    public void setNumeroRegistroAnterior(String numeroRegistroAnterior) {
        this.numeroRegistroAnterior = numeroRegistroAnterior;
    }

    @Column(name = "PER_SIGLA")
    public String getPerSigla() {
        return this.perSigla;
    }

    public void setPerSigla(String perSigla) {
        this.perSigla = perSigla;
    }

    @Column(name = "PP_PARTICIPACION")
    public BigDecimal getPpParticipacion() {
        return this.ppParticipacion;
    }

    public void setPpParticipacion(BigDecimal ppParticipacion) {
        this.ppParticipacion = ppParticipacion;
    }

    @Column(name = "PPP_DEPARTAMENTO_CODIGO")
    public String getPppDepartamentoCodigo() {
        return this.pppDepartamentoCodigo;
    }

    public void setPppDepartamentoCodigo(String pppDepartamentoCodigo) {
        this.pppDepartamentoCodigo = pppDepartamentoCodigo;
    }

    @Column(name = "PPP_ENTIDAD_EMISORA")
    public String getPppEntidadEmisora() {
        return this.pppEntidadEmisora;
    }

    public void setPppEntidadEmisora(String pppEntidadEmisora) {
        this.pppEntidadEmisora = pppEntidadEmisora;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "PPP_FECHA_REGISTRO")
    public Date getPppFechaRegistro() {
        return this.pppFechaRegistro;
    }

    public void setPppFechaRegistro(Date pppFechaRegistro) {
        this.pppFechaRegistro = pppFechaRegistro;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "PPP_FECHA_TITULO")
    public Date getPppFechaTitulo() {
        return this.pppFechaTitulo;
    }

    public void setPppFechaTitulo(Date pppFechaTitulo) {
        this.pppFechaTitulo = pppFechaTitulo;
    }

    @Column(name = "PPP_MODO_ADQUISICION")
    public String getPppModoAdquisicion() {
        return this.pppModoAdquisicion;
    }

    public void setPppModoAdquisicion(String pppModoAdquisicion) {
        this.pppModoAdquisicion = pppModoAdquisicion;
    }

    @Column(name = "PPP_MUNICIPIO_CODIGO")
    public String getPppMunicipioCodigo() {
        return this.pppMunicipioCodigo;
    }

    public void setPppMunicipioCodigo(String pppMunicipioCodigo) {
        this.pppMunicipioCodigo = pppMunicipioCodigo;
    }

    @Column(name = "PPP_NUMERO_TITULO")
    public String getPppNumeroTitulo() {
        return this.pppNumeroTitulo;
    }

    public void setPppNumeroTitulo(String pppNumeroTitulo) {
        this.pppNumeroTitulo = pppNumeroTitulo;
    }

    @Column(name = "PPP_TIPO_TITULO")
    public String getPppTipoTitulo() {
        return this.pppTipoTitulo;
    }

    public void setPppTipoTitulo(String pppTipoTitulo) {
        this.pppTipoTitulo = pppTipoTitulo;
    }

    @Column(name = "PPP_VALOR")
    public BigDecimal getPppValor() {
        return this.pppValor;
    }

    public void setPppValor(BigDecimal pppValor) {
        this.pppValor = pppValor;
    }

    @Column(name = "PREDIO_ID")
    public BigDecimal getPredioId() {
        return this.predioId;
    }

    public void setPredioId(BigDecimal predioId) {
        this.predioId = predioId;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "TIPO_IDENTIFICACION")
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "UC_AREA_CONSTRUIDA")
    public BigDecimal getUcAreaConstruida() {
        return this.ucAreaConstruida;
    }

    public void setUcAreaConstruida(BigDecimal ucAreaConstruida) {
        this.ucAreaConstruida = ucAreaConstruida;
    }

    @Column(name = "UC_AVALUO")
    public BigDecimal getUcAvaluo() {
        return this.ucAvaluo;
    }

    public void setUcAvaluo(BigDecimal ucAvaluo) {
        this.ucAvaluo = ucAvaluo;
    }

    @Column(name = "UC_CALIFICACION_ANEXO_ID")
    public BigDecimal getUcCalificacionAnexoId() {
        return this.ucCalificacionAnexoId;
    }

    public void setUcCalificacionAnexoId(BigDecimal ucCalificacionAnexoId) {
        this.ucCalificacionAnexoId = ucCalificacionAnexoId;
    }

    @Column(name = "UC_TIPIFICACION")
    public String getUcTipificacion() {
        return this.ucTipificacion;
    }

    public void setUcTipificacion(String ucTipificacion) {
        this.ucTipificacion = ucTipificacion;
    }

    @Column(name = "UC_TOTAL_BANIOS")
    public BigDecimal getUcTotalBanios() {
        return this.ucTotalBanios;
    }

    public void setUcTotalBanios(BigDecimal ucTotalBanios) {
        this.ucTotalBanios = ucTotalBanios;
    }

    @Column(name = "UC_TOTAL_HABITACIONES")
    public BigDecimal getUcTotalHabitaciones() {
        return this.ucTotalHabitaciones;
    }

    public void setUcTotalHabitaciones(BigDecimal ucTotalHabitaciones) {
        this.ucTotalHabitaciones = ucTotalHabitaciones;
    }

    @Column(name = "UC_TOTAL_LOCALES")
    public BigDecimal getUcTotalLocales() {
        return this.ucTotalLocales;
    }

    public void setUcTotalLocales(BigDecimal ucTotalLocales) {
        this.ucTotalLocales = ucTotalLocales;
    }

    @Column(name = "UC_TOTAL_PISOS_UNIDAD")
    public BigDecimal getUcTotalPisosUnidad() {
        return this.ucTotalPisosUnidad;
    }

    public void setUcTotalPisosUnidad(BigDecimal ucTotalPisosUnidad) {
        this.ucTotalPisosUnidad = ucTotalPisosUnidad;
    }

    @Column(name = "UC_TOTAL_PUNTAJE")
    public BigDecimal getUcTotalPuntaje() {
        return this.ucTotalPuntaje;
    }

    public void setUcTotalPuntaje(BigDecimal ucTotalPuntaje) {
        this.ucTotalPuntaje = ucTotalPuntaje;
    }

    @Column(name = "UC_UNIDAD")
    public String getUcUnidad() {
        return this.ucUnidad;
    }

    public void setUcUnidad(String ucUnidad) {
        this.ucUnidad = ucUnidad;
    }

    @Column(name = "UC_USO_ID")
    public BigDecimal getUcUsoId() {
        return this.ucUsoId;
    }

    public void setUcUsoId(BigDecimal ucUsoId) {
        this.ucUsoId = ucUsoId;
    }

    @Column(name = "VALOR_AVALUO")
    public BigDecimal getValorAvaluo() {
        return this.valorAvaluo;
    }

    public void setValorAvaluo(BigDecimal valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

    @Column(name = "VALOR_CONS_CONVENCIONAL")
    public BigDecimal getValorConsConvencional() {
        return this.valorConsConvencional;
    }

    public void setValorConsConvencional(BigDecimal valorConsConvencional) {
        this.valorConsConvencional = valorConsConvencional;
    }

    @Column(name = "VALOR_CONS_NO_CONVENCIONAL")
    public BigDecimal getValorConsNoConvencional() {
        return this.valorConsNoConvencional;
    }

    public void setValorConsNoConvencional(BigDecimal valorConsNoConvencional) {
        this.valorConsNoConvencional = valorConsNoConvencional;
    }

    @Column(name = "VALOR_CONSTRUCCION_COMUN")
    public BigDecimal getValorConstruccionComun() {
        return this.valorConstruccionComun;
    }

    public void setValorConstruccionComun(BigDecimal valorConstruccionComun) {
        this.valorConstruccionComun = valorConstruccionComun;
    }

    @Column(name = "VALOR_TERRENO_COMUN")
    public BigDecimal getValorTerrenoComun() {
        return this.valorTerrenoComun;
    }

    public void setValorTerrenoComun(BigDecimal valorTerrenoComun) {
        this.valorTerrenoComun = valorTerrenoComun;
    }

    @Column(name = "VALOR_TERRENO_PRIVADO")
    public BigDecimal getValorTerrenoPrivado() {
        return this.valorTerrenoPrivado;
    }

    public void setValorTerrenoPrivado(BigDecimal valorTerrenoPrivado) {
        this.valorTerrenoPrivado = valorTerrenoPrivado;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_CATASTRAL")
    public BigDecimal getValorTotalAvaluoCatastral() {
        return this.valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(BigDecimal valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_VIGENCIA")
    public BigDecimal getValorTotalAvaluoVigencia() {
        return this.valorTotalAvaluoVigencia;
    }

    public void setValorTotalAvaluoVigencia(BigDecimal valorTotalAvaluoVigencia) {
        this.valorTotalAvaluoVigencia = valorTotalAvaluoVigencia;
    }

    @Column(name = "VALOR_TOTAL_CONS_PRIVADA")
    public BigDecimal getValorTotalConsPrivada() {
        return this.valorTotalConsPrivada;
    }

    public void setValorTotalConsPrivada(BigDecimal valorTotalConsPrivada) {
        this.valorTotalConsPrivada = valorTotalConsPrivada;
    }

    @Column(name = "VALOR_TOTAL_CONSTRUCCION")
    public BigDecimal getValorTotalConstruccion() {
        return this.valorTotalConstruccion;
    }

    public void setValorTotalConstruccion(BigDecimal valorTotalConstruccion) {
        this.valorTotalConstruccion = valorTotalConstruccion;
    }

    @Column(name = "VALOR_TOTAL_TERRENO")
    public BigDecimal getValorTotalTerreno() {
        return this.valorTotalTerreno;
    }

    public void setValorTotalTerreno(BigDecimal valorTotalTerreno) {
        this.valorTotalTerreno = valorTotalTerreno;
    }

    @Temporal(TemporalType.DATE)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "ZONA_AREA")
    public BigDecimal getZonaArea() {
        return this.zonaArea;
    }

    public void setZonaArea(BigDecimal zonaArea) {
        this.zonaArea = zonaArea;
    }

    @Column(name = "ZONA_FISICA")
    public String getZonaFisica() {
        return this.zonaFisica;
    }

    public void setZonaFisica(String zonaFisica) {
        this.zonaFisica = zonaFisica;
    }

    @Column(name = "ZONA_GEOECONOMICA")
    public String getZonaGeoeconomica() {
        return this.zonaGeoeconomica;
    }

    public void setZonaGeoeconomica(String zonaGeoeconomica) {
        this.zonaGeoeconomica = zonaGeoeconomica;
    }

    //bi-directional many-to-one association to RepReporteEjecucion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REPORTE_EJECUCION_ID")
    public RepReporteEjecucion getRepReporteEjecucion() {
        return this.repReporteEjecucion;
    }

    public void setRepReporteEjecucion(RepReporteEjecucion repReporteEjecucion) {
        this.repReporteEjecucion = repReporteEjecucion;
    }

}
