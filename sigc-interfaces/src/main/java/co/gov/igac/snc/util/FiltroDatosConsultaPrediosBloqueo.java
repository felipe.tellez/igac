package co.gov.igac.snc.util;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;

/**
 * Datos basicos para la busqueda de predios bloqueados
 *
 * @author juan.agudelo
 *
 */
public class FiltroDatosConsultaPrediosBloqueo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9049882450821999340L;

    private Departamento departamento;
    private Municipio municipio;
    private String departamentoCodigo;
    private String municipioCodigo;
    private String tipoAvaluo;
    private String sectorCodigo;
    private String comunaCodigo;
    private String barrioCodigo;
    private String manzanaVeredaCodigo;
    private String predio;
    private String condicionPropiedad;
    private String edificio;
    private String piso;
    private String unidad;

    public FiltroDatosConsultaPrediosBloqueo() {

    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {

        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public String getTipoAvaluo() {
        return tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    public String getSectorCodigo() {
        return sectorCodigo;
    }

    public void setSectorCodigo(String sectorCodigo) {
        this.sectorCodigo = sectorCodigo;
    }

    public String getComunaCodigo() {
        return comunaCodigo;
    }

    public void setComunaCodigo(String comunaCodigo) {
        this.comunaCodigo = comunaCodigo;
    }

    public String getBarrioCodigo() {
        return barrioCodigo;
    }

    public void setBarrioCodigo(String barrioCodigo) {
        this.barrioCodigo = barrioCodigo;
    }

    public String getManzanaVeredaCodigo() {
        return manzanaVeredaCodigo;
    }

    public void setManzanaVeredaCodigo(String manzanaCodigo) {
        this.manzanaVeredaCodigo = manzanaCodigo;
    }

    public String getPredio() {
        return predio;
    }

    public void setPredio(String predio) {
        this.predio = predio;
    }

    public String getCondicionPropiedad() {
        return condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

}
