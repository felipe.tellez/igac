package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the TMP_BLOQUEO_MASIVO database table.
 *
 */
@Entity
@Table(name = "TMP_BLOQUEO_MASIVO")
public class TmpBloqueoMasivo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String bloqueado;
    private String descripcion;
    private Date fechaLog;
    private String identificador;
    private BigDecimal sessionId;

    public TmpBloqueoMasivo() {
    }

    public String getBloqueado() {
        return this.bloqueado;
    }

    public void setBloqueado(String bloqueado) {
        this.bloqueado = bloqueado;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Id
    public String getIdentificador() {
        return this.identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    @Column(name = "SESSION_ID", nullable = false, precision = 10, scale = 0)
    public BigDecimal getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(BigDecimal sessionId) {
        this.sessionId = sessionId;
    }

}
