package co.gov.igac.snc.persistence.util;

public enum ESolicitudEstado {
    RECIBIDA("1"),
    EN_ESTUDIO("2"),
    TRAMITES_CONSOLIDADOS("3"),
    /**
     * Estado que indica que el ingreso de datos de la solicitud aun esta en proceso.</br>
     * <i><b>Usado en Avaluos Comerciales</i></b>
     *
     * @author rodrigo.hernandez
     */
    BORRADOR("4"),
    /**
     * Estado que indica que el ingreso de datos de la solicitud ya finalizó.</br>
     * <i><b>Usado en Avaluos Comerciales</i></b>
     *
     * @author rodrigo.hernandez
     */
    INGRESADA("5");

    private String codigo;

    private ESolicitudEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
