package co.gov.igac.snc.util;

import co.gov.igac.generales.dto.UsuarioDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.RepTerritorialReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepUsuarioRolReporte;

/**
 * @description Clase usada para realizar agrupaciones de la información de
 * {@link RepUsuarioRolReporte}
 *
 * @version 1.0
 * @author david.cifuentes
 */
public class RepUsuarioRolReporteAgrupadoDTO implements Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -7587081082770043166L;

    /** ---------------------------------- */
    /** ------ VARIABLES SIMPLES --------- */
    /** ---------------------------------- */
    private Long id;
    private String usuarioAsignado;
    private String categoria;
    private Long tipoReporteId;
    private boolean consultar;
    private String estado;
    private boolean generar;
    private String usuarioLog;
    private Date fechaLog;
    private String rolAsignado;
    private EstructuraOrganizacional territorial;
    private EstructuraOrganizacional UOC;
    private Departamento departamento;
    private Municipio municipio;

    private String nombreTipoReporte;
    private String nombreTerritorial;
    private String nombreUOC;
    private String departamentoCodigo;
    private String municipioCodigo;

    /** ---------------------------------- */
    /** ------- OBJETOS AGRUPADOS -------- */
    /** ---------------------------------- */
    private List<String> rolesAsignados;
    private List<EstructuraOrganizacional> territoriales;
    private List<EstructuraOrganizacional> UOCs;
    private List<Departamento> departamentos;
    private List<Municipio> municipios;

    /** ---------------------------------- */
    /** --- IDS DE LAS PARAMETRIZACIONES-- */
    /** ---------------------------------- */
    private List<Long> idsParametrizaciones;
    private boolean editable;

    /**
     * Default constructor
     */
    public RepUsuarioRolReporteAgrupadoDTO() {
        this.editable = true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isConsultar() {
        return consultar;
    }

    public void setConsultar(boolean consultar) {
        this.consultar = consultar;
    }

    public String getNombreTerritorial() {
        return nombreTerritorial;
    }

    public void setNombreTerritorial(String nombreTerritorial) {
        this.nombreTerritorial = nombreTerritorial;
    }

    public String getNombreUOC() {
        return nombreUOC;
    }

    public void setNombreUOC(String nombreUOC) {
        this.nombreUOC = nombreUOC;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public String getNombreTipoReporte() {
        return nombreTipoReporte;
    }

    public void setNombreTipoReporte(String nombreTipoReporte) {
        this.nombreTipoReporte = nombreTipoReporte;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public boolean isGenerar() {
        return generar;
    }

    public void setGenerar(boolean generar) {
        this.generar = generar;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Long getTipoReporteId() {
        return tipoReporteId;
    }

    public void setTipoReporteId(Long tipoReporteId) {
        this.tipoReporteId = tipoReporteId;
    }

    public EstructuraOrganizacional getTerritorial() {
        return territorial;
    }

    public void setTerritorial(EstructuraOrganizacional territorial) {
        this.territorial = territorial;
    }

    public EstructuraOrganizacional getUOC() {
        return UOC;
    }

    public void setUOC(EstructuraOrganizacional uOC) {
        UOC = uOC;
    }

    public List<Long> getIdsParametrizaciones() {
        return idsParametrizaciones;
    }

    public void setIdsParametrizaciones(List<Long> idsParametrizaciones) {
        this.idsParametrizaciones = idsParametrizaciones;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public List<String> getRolesAsignados() {
        return rolesAsignados;
    }

    public void setRolesAsignados(List<String> rolesAsignados) {
        this.rolesAsignados = rolesAsignados;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public List<EstructuraOrganizacional> getTerritoriales() {
        return territoriales;
    }

    public void setTerritoriales(List<EstructuraOrganizacional> territoriales) {
        this.territoriales = territoriales;
    }

    public List<EstructuraOrganizacional> getUOCs() {
        return UOCs;
    }

    public void setUOCs(List<EstructuraOrganizacional> uOCs) {
        UOCs = uOCs;
    }

    public List<Departamento> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(List<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public List<Municipio> getMunicipios() {
        return municipios;
    }

    public void setMunicipios(List<Municipio> municipios) {
        this.municipios = municipios;
    }

    public String getRolAsignado() {
        return rolAsignado;
    }

    public void setRolAsignado(String rolAsignado) {
        this.rolAsignado = rolAsignado;
    }

    public String getUsuarioAsignado() {
        return usuarioAsignado;
    }

    public void setUsuarioAsignado(String usuarioAsignado) {
        this.usuarioAsignado = usuarioAsignado;
    }

    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    /**
     * Método que verifica si existen o no roles asignados
     *
     * @return
     */
    public boolean isExistenRolesAsignados() {
        if (this.rolesAsignados != null && this.rolesAsignados.size() > 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Método que verifica si existen o no territorial<es
     *
     * @return
     */
    public boolean isExistenTerritoriales() {
        if (this.territoriales != null && this.territoriales.size() > 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Método que verifica si existen o no UOCs
     *
     * @return
     */
    public boolean isExistenUOCs() {
        if (this.UOCs != null && this.UOCs.size() > 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Método que verifica si existen o no departamentos
     *
     * @return
     */
    public boolean isExistenDepartamentos() {
        if (this.departamentos != null && this.departamentos.size() > 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Método que verifica si existen o no municipios
     *
     * @return
     */
    public boolean isExistenMunicipios() {
        if (this.municipios != null && this.municipios.size() > 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Método que realiza una agrupación de todos los roles asignados
     *
     * @param rol
     */
    public void registrarRolAsignado(String rol) {
        if (this.rolesAsignados == null) {
            this.rolesAsignados = new ArrayList<String>();
        }
        if (rol != null && !this.rolesAsignados.contains(rol)) {
            this.rolesAsignados.add(rol);
        }
    }

    /**
     * Método que realiza una agrupación de todos los departamentos asignados.
     *
     * @param repTerritorialReportes
     */
    public void registrarDepartamentos(
        List<RepTerritorialReporte> repTerritorialReportes) {

        List<String> codigosDepartamentos = new ArrayList<String>();
        this.departamentos = new ArrayList<Departamento>();

        if (repTerritorialReportes != null) {
            for (RepTerritorialReporte rtr : repTerritorialReportes) {
                if (rtr.getDepartamento() != null) {
                    if (!codigosDepartamentos.contains(rtr.getDepartamento().getCodigo())) {
                        codigosDepartamentos.add(rtr.getDepartamento().getCodigo());
                        this.departamentos.add(rtr.getDepartamento());
                    }
                }
            }
        }

        if (!this.departamentos.isEmpty()) {
            this.departamento = this.departamentos.get(0);
            this.departamentoCodigo = this.departamentos.get(0).getCodigo();
        }
    }

    /**
     * Método que realiza una agrupación de todos los municipios asignados.
     *
     * @param repTerritorialReportes
     */
    public void registrarMunicipios(
        List<RepTerritorialReporte> repTerritorialReportes) {

        List<String> codigosMunicipios = new ArrayList<String>();
        this.municipios = new ArrayList<Municipio>();

        if (repTerritorialReportes != null) {
            for (RepTerritorialReporte rtr : repTerritorialReportes) {
                if (rtr.getMunicipio() != null) {
                    if (!codigosMunicipios.contains(rtr.getMunicipio().getCodigo())) {
                        codigosMunicipios.add(rtr.getMunicipio().getCodigo());
                        this.municipios.add(rtr.getMunicipio());
                    }
                }
            }
        }

        if (!this.municipios.isEmpty()) {
            this.municipio = this.municipios.get(0);
            this.municipioCodigo = this.municipios.get(0).getCodigo();
        }
    }

    /**
     * Método que realiza una agrupación de todos las territoriales asignadas.
     *
     * @param repTerritorialReportes
     */
    public void registrarTerritoriales(
        List<RepTerritorialReporte> repTerritorialReportes) {

        List<String> codigosTerritoriales = new ArrayList<String>();
        this.territoriales = new ArrayList<EstructuraOrganizacional>();

        if (repTerritorialReportes != null) {
            for (RepTerritorialReporte rtr : repTerritorialReportes) {
                if (rtr.getTerritorial() != null) {
                    if (!codigosTerritoriales.contains(rtr.getTerritorial().getCodigo())) {
                        codigosTerritoriales.add(rtr.getTerritorial().getCodigo());
                        this.territoriales.add(rtr.getTerritorial());
                    }
                }
            }
        }

        if (!this.territoriales.isEmpty()) {
            this.territorial = this.territoriales.get(0);
            this.nombreTerritorial = this.territoriales.get(0).getNombre();
        }
    }

    /**
     * Método que realiza una agrupación de todos las territoriales asignadas.
     *
     * @param repTerritorialReportes
     */
    public void registrarUOCs(
        List<RepTerritorialReporte> repTerritorialReportes) {

        List<String> codigosUOCs = new ArrayList<String>();
        this.UOCs = new ArrayList<EstructuraOrganizacional>();

        if (repTerritorialReportes != null) {
            for (RepTerritorialReporte rtr : repTerritorialReportes) {
                if (rtr.getUOC() != null) {
                    if (!codigosUOCs.contains(rtr.getUOC().getCodigo())) {
                        codigosUOCs.add(rtr.getUOC().getCodigo());
                        this.UOCs.add(rtr.getUOC());
                    }
                }
            }
        }

        if (!this.UOCs.isEmpty()) {
            this.UOC = this.UOCs.get(0);
            this.nombreUOC = this.UOCs.get(0).getNombre();
        }
    }

    /**
     * Método que realiza el registro de los ids de las parametrizaciones que forman la agrupación.
     *
     * @param Long
     */
    public void registrarIds(Long id) {

        if (this.idsParametrizaciones == null) {
            this.idsParametrizaciones = new ArrayList<Long>();
        }
        if (this.idsParametrizaciones == null) {
            this.idsParametrizaciones = new ArrayList<Long>();
        }
        if (id != null && !this.idsParametrizaciones.contains(id)) {
            this.idsParametrizaciones.add(id);
        }
    }

    /**
     * Método que determina si el registro es editable.
     *
     * @author felipe.cadena
     *
     * @param entidadCreacion
     * @param usuario
     */
    public void determinarEdicion(String entidadCreacion, UsuarioDTO usuario) {

        String entidad;

        if (usuario.getCodigoUOC() != null && !usuario.getCodigoUOC().isEmpty()) {
            entidad = usuario.getCodigoUOC();
        } else {
            entidad = usuario.getCodigoTerritorial();
        }

        this.editable = entidad.equals(entidadCreacion) && this.editable;

    }

}
