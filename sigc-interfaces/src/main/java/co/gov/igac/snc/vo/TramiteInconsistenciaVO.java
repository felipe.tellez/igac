/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.vo;

import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import java.io.Serializable;

/**
 * Clase que representa el tramite inconsistencia para inclusion dentro de las respuesta de los
 * servicios web
 *
 * @author andres.eslava
 */
public class TramiteInconsistenciaVO implements Serializable {

    private static final long serialVersionUID = -7312830130461751925L;

    private Long id;
    private String inconsistencia;
    private String tipo;
    private Long tramiteDepuracion;
    private String numeroPredial;

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public TramiteInconsistenciaVO(TramiteInconsistencia unaInconsistencia) {
        this.id = unaInconsistencia.getId();
        this.inconsistencia = unaInconsistencia.getInconsistencia();
        this.tipo = unaInconsistencia.getTipo();
        this.tramiteDepuracion = unaInconsistencia.getTramiteDepuracion().getId();
        this.numeroPredial = unaInconsistencia.getNumeroPredial();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInconsistencia() {
        return inconsistencia;
    }

    public void setInconsistencia(String inconsistencia) {
        this.inconsistencia = inconsistencia;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getTramiteDepuracion() {
        return tramiteDepuracion;
    }

    public void setTramiteDepuracion(Long tramiteDepuracion) {
        this.tramiteDepuracion = tramiteDepuracion;
    }

}
