package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the ESTADISTICA_GERENCIAL_PREDIO database table.
 *
 */
@Entity
@Table(name = "ESTADISTICA_GERENCIAL_PREDIO")
public class EstadisticaGerencialPredio implements Serializable {

    private static final long serialVersionUID = 1L;
    private EstadisticaGerencialPredioPK id;
    private BigDecimal areaConstruccion;
    private BigDecimal areaTerreno;
    private BigDecimal cantidadPredios;
    private BigDecimal cantidadPropietarios;
    private BigDecimal valorAvaluo;

    public EstadisticaGerencialPredio() {
    }

    @EmbeddedId
    public EstadisticaGerencialPredioPK getId() {
        return this.id;
    }

    public void setId(EstadisticaGerencialPredioPK id) {
        this.id = id;
    }

    @Column(name = "AREA_CONSTRUCCION")
    public BigDecimal getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(BigDecimal areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "AREA_TERRENO")
    public BigDecimal getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(BigDecimal areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "CANTIDAD_PREDIOS")
    public BigDecimal getCantidadPredios() {
        return this.cantidadPredios;
    }

    public void setCantidadPredios(BigDecimal cantidadPredios) {
        this.cantidadPredios = cantidadPredios;
    }

    @Column(name = "CANTIDAD_PROPIETARIOS")
    public BigDecimal getCantidadPropietarios() {
        return this.cantidadPropietarios;
    }

    public void setCantidadPropietarios(BigDecimal cantidadPropietarios) {
        this.cantidadPropietarios = cantidadPropietarios;
    }

    @Column(name = "VALOR_AVALUO")
    public BigDecimal getValorAvaluo() {
        return this.valorAvaluo;
    }

    public void setValorAvaluo(BigDecimal valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

}
