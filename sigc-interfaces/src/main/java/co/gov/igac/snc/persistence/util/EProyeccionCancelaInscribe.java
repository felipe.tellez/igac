package co.gov.igac.snc.persistence.util;

public enum EProyeccionCancelaInscribe {
    CANCELA("C"),
    INSCRIBE("I"),
    MODIFICA("M"),
    //Se utiliza para los avaluos manuales que se pueden modificar mientras
    //se mantenga en la actividad de modificar
    TEMP("T");

    private String codigo;

    EProyeccionCancelaInscribe(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
