package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;

import java.util.Date;

/**
 * The persistent class for the PREDIO_FORMULARIO_SBC database table.
 *
 */
@Entity
@Table(name = "PREDIO_FORMULARIO_SBC")
public class PredioFormularioSbc implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private Predio predio;
    private String usuarioLog;
    private GeneracionFormularioSbc generacionFormularioSbc;

    public PredioFormularioSbc() {
    }

    @Id
    @SequenceGenerator(name = "PREDIO_FORMULARIO_SBC_ID_GENERATOR", sequenceName =
        "PREDIO_FORMULARIO_SBC_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "PREDIO_FORMULARIO_SBC_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID")
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to GeneracionFormularioSbc
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GENERACION_FORMULARIO_SBC_ID")
    public GeneracionFormularioSbc getGeneracionFormularioSbc() {
        return this.generacionFormularioSbc;
    }

    public void setGeneracionFormularioSbc(GeneracionFormularioSbc generacionFormularioSbc) {
        this.generacionFormularioSbc = generacionFormularioSbc;
    }

}
