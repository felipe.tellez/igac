/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que contiene los campos que sirven de filtro para hacer la consulta de ofertas
 * inmobiliarias
 *
 * @author pedro.garcia
 * @modified juan.agudelo
 */
public class FiltroDatosConsultaOfertasInmob implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5984093959422008935L;

    private String codigoOferta;
    private String departamentoCodigo;
    private String municipioCodigo;
    private String tipoPredio;
    private String numeroPredial;
    private String tipoInmueble;
    private String sector;
    private String manzanaVereda;
    private String barrio;
    private String destino;
    private String estrato;
    private String estado;
    private Integer numeroHabitaciones;
    private Integer numeroBanios;
    private String tipoInvestigacion;
    private String condicionJuridica;
    private String investigacionIngreso;
    private String vetustez;
    private String responsableInformacion;
    private Long regionCapturaOfertaId;

    private Double precioOfertaDesde;
    private Double precioOfertaHasta;

    private Double areaOfertaConstruidaDesde;
    private Double areaOfertaConstruidaHasta;

    private Double areaOfertaTerrenoDesde;
    private Double areaOfertaTerrenoHasta;

    private Date fechaOfertaDesde;
    private Date fechaOfertaHasta;

    private Date fechaIngresoOfertaDesde;
    private Date fechaIngresoOfertaHasta;

    //-----------  methods   ---------------------------------------
    public Date getFechaOfertaDesde() {
        return this.fechaOfertaDesde;
    }

    public void setFechaOfertaDesde(Date fechaOfertaDesde) {
        this.fechaOfertaDesde = fechaOfertaDesde;
    }

    public Date getFechaOfertaHasta() {
        return this.fechaOfertaHasta;
    }

    public void setFechaOfertaHasta(Date fechaOfertaHasta) {
        this.fechaOfertaHasta = fechaOfertaHasta;
    }

    public String getCodigoOferta() {
        return this.codigoOferta;
    }

    public void setCodigoOferta(String codigoOferta) {
        this.codigoOferta = codigoOferta;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public String getTipoPredio() {
        return this.tipoPredio;
    }

    public void setTipoPredio(String tipoPredio) {
        this.tipoPredio = tipoPredio;
    }

    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getTipoInmueble() {
        return this.tipoInmueble;
    }

    public void setTipoInmueble(String tipoInmueble) {
        this.tipoInmueble = tipoInmueble;
    }

    public String getSector() {
        return this.sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getManzanaVereda() {
        return this.manzanaVereda;
    }

    public void setManzanaVereda(String manzanaVereda) {
        this.manzanaVereda = manzanaVereda;
    }

    public String getBarrio() {
        return this.barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstrato() {
        return this.estrato;
    }

    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    public Integer getNumeroHabitaciones() {
        return this.numeroHabitaciones;
    }

    public void setNumeroHabitaciones(Integer numeroHabitaciones) {
        this.numeroHabitaciones = numeroHabitaciones;
    }

    public String getTipoInvestigacion() {
        return this.tipoInvestigacion;
    }

    public Integer getNumeroBanios() {
        return this.numeroBanios;
    }

    public void setNumeroBanios(Integer numeroBanios) {
        this.numeroBanios = numeroBanios;
    }

    public void setTipoInvestigacion(String tipoInvestigacion) {
        this.tipoInvestigacion = tipoInvestigacion;
    }

    public String getCondicionJuridica() {
        return this.condicionJuridica;
    }

    public void setCondicionJuridica(String condicionJuridica) {
        this.condicionJuridica = condicionJuridica;
    }

    public String getInvestigacionIngreso() {
        return this.investigacionIngreso;
    }

    public void setInvestigacionIngreso(String investigacion) {
        this.investigacionIngreso = investigacion;
    }

    public String getVetustez() {
        return this.vetustez;
    }

    public void setVetustez(String vetustez) {
        this.vetustez = vetustez;
    }

    public String getResponsableInformacion() {
        return responsableInformacion;
    }

    public void setResponsableInformacion(String responsableInformacion) {
        this.responsableInformacion = responsableInformacion;
    }

    public Long getRegionCapturaOfertaId() {
        return regionCapturaOfertaId;
    }

    public void setRegionCapturaOfertaId(Long regionCapturaOfertaId) {
        this.regionCapturaOfertaId = regionCapturaOfertaId;
    }

    public Double getPrecioOfertaDesde() {
        return this.precioOfertaDesde;
    }

    public void setPrecioOfertaDesde(Double precioOfertaDesde) {
        this.precioOfertaDesde = precioOfertaDesde;
    }

    public Double getPrecioOfertaHasta() {
        return this.precioOfertaHasta;
    }

    public void setPrecioOfertaHasta(Double precioOfertaHasta) {
        this.precioOfertaHasta = precioOfertaHasta;
    }

    public Double getAreaOfertaConstruidaDesde() {
        return this.areaOfertaConstruidaDesde;
    }

    public void setAreaOfertaConstruidaDesde(Double areaOfertaConstruidaDesde) {
        this.areaOfertaConstruidaDesde = areaOfertaConstruidaDesde;
    }

    public Double getAreaOfertaConstruidaHasta() {
        return this.areaOfertaConstruidaHasta;
    }

    public void setAreaOfertaConstruidaHasta(Double areaOfertaConstruidaHasta) {
        this.areaOfertaConstruidaHasta = areaOfertaConstruidaHasta;
    }

    public Double getAreaOfertaTerrenoDesde() {
        return this.areaOfertaTerrenoDesde;
    }

    public void setAreaOfertaTerrenoDesde(Double areaOfertaTerrenoDesde) {
        this.areaOfertaTerrenoDesde = areaOfertaTerrenoDesde;
    }

    public Double getAreaOfertaTerrenoHasta() {
        return this.areaOfertaTerrenoHasta;
    }

    public void setAreaOfertaTerrenoHasta(Double areaOfertaTerrenoHasta) {
        this.areaOfertaTerrenoHasta = areaOfertaTerrenoHasta;
    }

    public Date getFechaIngresoOfertaDesde() {
        return fechaIngresoOfertaDesde;
    }

    public void setFechaIngresoOfertaDesde(Date fechaIngresoOfertaDesde) {
        this.fechaIngresoOfertaDesde = fechaIngresoOfertaDesde;
    }

    public Date getFechaIngresoOfertaHasta() {
        return fechaIngresoOfertaHasta;
    }

    public void setFechaIngresoOfertaHasta(Date fechaIngresoOfertaHasta) {
        this.fechaIngresoOfertaHasta = fechaIngresoOfertaHasta;
    }

//end of class
}
