/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los reportes que en parametrización deben ocultar ciertos menus de consulta
 *
 * @author david.cifuentes
 */
public enum ERepReporteConsultaEspecialParametrizacion {

    RADICACIONES_CONSOLIDADO(221L, "REPORTES RADICACION"),
    RADICACIONES_DE_PREDIOS_CON_MATRICULAS_REGISTRALES(235L, "REPORTES RADICACION"),
    RADICACIONES_DETALLADO(222L, "REPORTES RADICACION"),
    RADICACIONES_TIPO_TRAMITE(223L, "REPORTES RADICACION");

    private Long id;
    private String categoria;

    private ERepReporteConsultaEspecialParametrizacion(Long id, String categoria) {
        this.id = id;
        this.categoria = categoria;
    }

    public Long getId() {
        return this.id;
    }

    public String getCategoria() {
        return categoria;
    }

}
