package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "V_RADICACION")
public class VRadicacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "DIGITO_VERIFICACION")
    private String digitoVerificacion;

    private String direccion;

    @Column(name = "DIRECCION_DEPARTAMENTO_CODIGO")
    private String direccionDepartamentoCodigo;

    @Column(name = "DIRECCION_DEPARTAMENTO_NOMBRE")
    private String direccionDepartamentoNombre;

    @Column(name = "DIRECCION_MUNICIPIO_CODIGO")
    private String direccionMunicipioCodigo;

    @Column(name = "DIRECCION_MUNICIPIO_NOMBRE")
    private String direccionMunicipioNombre;

    @Column(name = "DIRECCION_PAIS_CODIGO")
    private String direccionPaisCodigo;

    @Column(name = "DIRECCION_PAIS_NOMBRE")
    private String direccionPaisNombre;

    @Column(name = "DOCUMENTO_ID")
    private BigDecimal documentoId;

    private String estado;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RADICACION")
    private Date fechaRadicacion;

    @Id
    private BigDecimal id;

    @Column(name = "NUMERO_IDENTIFICACION")
    private String numeroIdentificacion;

    @Column(name = "NUMERO_RADICACION")
    private String numeroRadicacion;

    @Column(name = "NUMERO_RADICACION_DEFINITIVA")
    private String numeroRadicacionDefinitiva;

    @Column(name = "NUMERO_RADICACION_INICIAL")
    private String numeroRadicacionInicial;

    private String observaciones;

    @Column(name = "PERSONA_SOLICITANTE_ID")
    private BigDecimal personaSolicitanteId;

    @Column(name = "PRIMER_APELLIDO")
    private String primerApellido;

    @Column(name = "PRIMER_NOMBRE")
    private String primerNombre;

    @Column(name = "RAZON_SOCIAL")
    private String razonSocial;

    @Column(name = "SEGUNDO_APELLIDO")
    private String segundoApellido;

    @Column(name = "SEGUNDO_NOMBRE")
    private String segundoNombre;

    private String sigla;

    @Column(name = "SOLICITUD_ID")
    private BigDecimal solicitudId;

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD")
    private String estructuraOrganizacional;

    private String tipo;

    @Column(name = "TIPO_IDENTIFICACION")
    private String tipoIdentificacion;

    @Column(name = "TIPO_PERSONA")
    private String tipoPersona;

    public VRadicacion() {
    }

    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccionDepartamentoCodigo() {
        return this.direccionDepartamentoCodigo;
    }

    public void setDireccionDepartamentoCodigo(String direccionDepartamentoCodigo) {
        this.direccionDepartamentoCodigo = direccionDepartamentoCodigo;
    }

    public String getDireccionMunicipioCodigo() {
        return this.direccionMunicipioCodigo;
    }

    public void setDireccionMunicipioCodigo(String direccionMunicipioCodigo) {
        this.direccionMunicipioCodigo = direccionMunicipioCodigo;
    }

    public String getDireccionPaisCodigo() {
        return this.direccionPaisCodigo;
    }

    public void setDireccionPaisCodigo(String direccionPaisCodigo) {
        this.direccionPaisCodigo = direccionPaisCodigo;
    }

    public BigDecimal getDocumentoId() {
        return this.documentoId;
    }

    public void setDocumentoId(BigDecimal documentoId) {
        this.documentoId = documentoId;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaRadicacion() {
        return this.fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    public BigDecimal getId() {
        return this.id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getNumeroRadicacion() {
        return this.numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public String getNumeroRadicacionDefinitiva() {
        return this.numeroRadicacionDefinitiva;
    }

    public void setNumeroRadicacionDefinitiva(String numeroRadicacionDefinitiva) {
        this.numeroRadicacionDefinitiva = numeroRadicacionDefinitiva;
    }

    public String getNumeroRadicacionInicial() {
        return this.numeroRadicacionInicial;
    }

    public void setNumeroRadicacionInicial(String numeroRadicacionInicial) {
        this.numeroRadicacionInicial = numeroRadicacionInicial;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public BigDecimal getPersonaSolicitanteId() {
        return this.personaSolicitanteId;
    }

    public void setPersonaSolicitanteId(BigDecimal personaSolicitanteId) {
        this.personaSolicitanteId = personaSolicitanteId;
    }

    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public BigDecimal getSolicitudId() {
        return this.solicitudId;
    }

    public void setSolicitudId(BigDecimal solicitudId) {
        this.solicitudId = solicitudId;
    }

    public String getTerritorialCodigo() {
        return this.estructuraOrganizacional;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.estructuraOrganizacional = territorialCodigo;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getDireccionDepartamentoNombre() {
        return direccionDepartamentoNombre;
    }

    public void setDireccionDepartamentoNombre(String direccionDepartamentoNombre) {
        this.direccionDepartamentoNombre = direccionDepartamentoNombre;
    }

    public String getDireccionMunicipioNombre() {
        return direccionMunicipioNombre;
    }

    public void setDireccionMunicipioNombre(String direccionMunicipioNombre) {
        this.direccionMunicipioNombre = direccionMunicipioNombre;
    }

    public String getDireccionPaisNombre() {
        return direccionPaisNombre;
    }

    public void setDireccionPaisNombre(String direccionPaisNombre) {
        this.direccionPaisNombre = direccionPaisNombre;
    }

    @Transient
    public String getNombreCompleto() {
        StringBuilder resultado = new StringBuilder();
        if (primerNombre != null) {
            resultado.append(" " + primerNombre);
        }
        if (segundoNombre != null) {
            resultado.append(" " + segundoNombre);
        }
        if (primerApellido != null) {
            resultado.append(" " + primerApellido);
        }
        if (segundoApellido != null) {
            resultado.append(" " + segundoApellido);
        }
        if (razonSocial != null) {
            resultado.append(" " + razonSocial);
        }

        System.out.println("JAMIR: " + resultado.toString().trim());
        return resultado.toString().trim();
    }
}
