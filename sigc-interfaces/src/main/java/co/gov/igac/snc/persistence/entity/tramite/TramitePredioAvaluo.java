package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TramitePredioAvaluo entity. @author MyEclipse Persistence Tools
 *
 * @modified ariel.ortiz - se elimina el atributo Long numeroPredialNacional debido a que se elimina
 * la columna en la tabla de la base de datos.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TRAMITE_PREDIO_AVALUO", schema = "SNC_TRAMITE")
public class TramitePredioAvaluo implements java.io.Serializable {

    /**
     *
     */
    // comentado para despliegue en jetty
    // juan.agudelo
    // private static final long serialVersionUID = -7144421393258525868L;
    // Fields
    private Long id;
    private Tramite tramite;
    private Long predioId;
    // private Long numeroPredialNacional;
    private String departamentoCodigo;
    private String municipioCodigo;
    private String numeroPredial;
    private String numeroRegistro;
    private String numeroRegistroAnterior;
    private String circuloRegistral;
    private String oficina;
    private String libro;
    private String tomo;
    private String pagina;
    private Short anio;
    private Double areaTerreno;
    private Double areaConstruccion;
    private String direccion;
    private String condicionJuridica;
    private String destino;
    private String tipoInmueble;
    private String contactoNombre;
    private String contactoTelefono;
    private String contactoTelefonoExt;
    private String contactoCorreoElectronico;
    private String tipoAvaluoPredio;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public TramitePredioAvaluo() {
    }

    /** minimal constructor */
    public TramitePredioAvaluo(Long id, Tramite tramite,
        Long numeroPredialNacional, String departamentoCodigo,
        String municipioCodigo, String numeroPredial, Double areaTerreno,
        Double areaConstruccion, String direccion, String tipoAvaluoPredio,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        // this.numeroPredialNacional = numeroPredialNacional;
        this.departamentoCodigo = departamentoCodigo;
        this.municipioCodigo = municipioCodigo;
        this.numeroPredial = numeroPredial;
        this.areaTerreno = areaTerreno;
        this.areaConstruccion = areaConstruccion;
        this.direccion = direccion;
        this.tipoAvaluoPredio = tipoAvaluoPredio;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramitePredioAvaluo(Long id, Tramite tramite, Long predioId,
        Long numeroPredialNacional, String departamentoCodigo,
        String municipioCodigo, String numeroPredial,
        String numeroRegistro, String numeroRegistroAnterior,
        String circuloRegistral, String oficina, String libro, String tomo,
        String pagina, Short anio, Double areaTerreno,
        Double areaConstruccion, String direccion,
        String condicionJuridica, String destino, String tipoInmueble,
        String contactoNombre, String contactoTelefono,
        String contactoTelefonoExt, String contactoCorreoElectronico,
        String tipoAvaluoPredio, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.predioId = predioId;
        // this.numeroPredialNacional = numeroPredialNacional;
        this.departamentoCodigo = departamentoCodigo;
        this.municipioCodigo = municipioCodigo;
        this.numeroPredial = numeroPredial;
        this.numeroRegistro = numeroRegistro;
        this.numeroRegistroAnterior = numeroRegistroAnterior;
        this.circuloRegistral = circuloRegistral;
        this.oficina = oficina;
        this.libro = libro;
        this.tomo = tomo;
        this.pagina = pagina;
        this.anio = anio;
        this.areaTerreno = areaTerreno;
        this.areaConstruccion = areaConstruccion;
        this.direccion = direccion;
        this.condicionJuridica = condicionJuridica;
        this.destino = destino;
        this.tipoInmueble = tipoInmueble;
        this.contactoNombre = contactoNombre;
        this.contactoTelefono = contactoTelefono;
        this.contactoTelefonoExt = contactoTelefonoExt;
        this.contactoCorreoElectronico = contactoCorreoElectronico;
        this.tipoAvaluoPredio = tipoAvaluoPredio;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_PREDIO_AVALUO_Id_SEQ")
    @SequenceGenerator(name = "TRAMITE_PREDIO_AVALUO_Id_SEQ", sequenceName =
        "TRAMITE_PREDIO_AVALUO_Id_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "PREDIO_ID", precision = 10, scale = 0)
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    // @Column(name = "NUMERO_PREDIAL_NACIONAL", nullable = false, precision =
    // 10, scale = 0)
    // public Long getNumeroPredialNacional() {
    // return this.numeroPredialNacional;
    // }
    //
    // public void setNumeroPredialNacional(Long numeroPredialNacional) {
    // this.numeroPredialNacional = numeroPredialNacional;
    // }
    @Column(name = "DEPARTAMENTO_CODIGO", nullable = false, length = 2)
    public String getDepartamentoCodigo() {
        return this.departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    @Column(name = "MUNICIPIO_CODIGO", nullable = false, length = 5)
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    @Column(name = "NUMERO_PREDIAL", nullable = false, length = 30)
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_REGISTRO", length = 20)
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "NUMERO_REGISTRO_ANTERIOR", length = 20)
    public String getNumeroRegistroAnterior() {
        return this.numeroRegistroAnterior;
    }

    public void setNumeroRegistroAnterior(String numeroRegistroAnterior) {
        this.numeroRegistroAnterior = numeroRegistroAnterior;
    }

    @Column(name = "CIRCULO_REGISTRAL", length = 10)
    public String getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(String circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "OFICINA", length = 20)
    public String getOficina() {
        return this.oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    @Column(name = "LIBRO", length = 20)
    public String getLibro() {
        return this.libro;
    }

    public void setLibro(String libro) {
        this.libro = libro;
    }

    @Column(name = "TOMO", length = 20)
    public String getTomo() {
        return this.tomo;
    }

    public void setTomo(String tomo) {
        this.tomo = tomo;
    }

    @Column(name = "PAGINA", length = 20)
    public String getPagina() {
        return this.pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    @Column(name = "ANIO", precision = 4, scale = 0)
    public Short getAnio() {
        return this.anio;
    }

    public void setAnio(Short anio) {
        this.anio = anio;
    }

    @Column(name = "AREA_TERRENO", nullable = false, precision = 18)
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "AREA_CONSTRUCCION", nullable = false, precision = 18)
    public Double getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "DIRECCION", nullable = false, length = 250)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "CONDICION_JURIDICA", length = 30)
    public String getCondicionJuridica() {
        return this.condicionJuridica;
    }

    public void setCondicionJuridica(String condicionJuridica) {
        this.condicionJuridica = condicionJuridica;
    }

    @Column(name = "DESTINO", length = 30)
    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Column(name = "TIPO_INMUEBLE", length = 30)
    public String getTipoInmueble() {
        return this.tipoInmueble;
    }

    public void setTipoInmueble(String tipoInmueble) {
        this.tipoInmueble = tipoInmueble;
    }

    @Column(name = "CONTACTO_NOMBRE", length = 250)
    public String getContactoNombre() {
        return this.contactoNombre;
    }

    public void setContactoNombre(String contactoNombre) {
        this.contactoNombre = contactoNombre;
    }

    @Column(name = "CONTACTO_TELEFONO", length = 50)
    public String getContactoTelefono() {
        return this.contactoTelefono;
    }

    public void setContactoTelefono(String contactoTelefono) {
        this.contactoTelefono = contactoTelefono;
    }

    @Column(name = "CONTACTO_CORREO_ELECTRONICO", length = 100)
    public String getContactoCorreoElectronico() {
        return this.contactoCorreoElectronico;
    }

    public void setContactoCorreoElectronico(String contactoCorreoElectronico) {
        this.contactoCorreoElectronico = contactoCorreoElectronico;
    }

    @Column(name = "TIPO_AVALUO_PREDIO", nullable = false, length = 30)
    public String getTipoAvaluoPredio() {
        return this.tipoAvaluoPredio;
    }

    public void setTipoAvaluoPredio(String tipoAvaluoPredio) {
        this.tipoAvaluoPredio = tipoAvaluoPredio;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "CONTACTO_TELEFONO_EXT", length = 6)
    public String getContactoTelefonoExt() {
        return contactoTelefonoExt;
    }

    public void setContactoTelefonoExt(String contactoTelefonoExt) {
        this.contactoTelefonoExt = contactoTelefonoExt;
    }

}
