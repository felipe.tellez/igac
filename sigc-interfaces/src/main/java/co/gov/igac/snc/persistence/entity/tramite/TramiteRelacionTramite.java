package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the TRAMITE_RELACION_TRAMITE database table.
 *
 * @modified juan.agudelo adición de la secuencia TRAMITE_RELACION_TRAMIT_ID_SEQ
 */
@Entity
@Table(name = "TRAMITE_RELACION_TRAMITE")
public class TramiteRelacionTramite implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5296308309174351097L;

    private long id;
    private Date fechaLog;
    private String usuarioLog;
    private Tramite tramite1;
    private Tramite tramiteRelacionado;

    public TramiteRelacionTramite() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_RELACION_TRAMIT_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_RELACION_TRAMIT_ID_SEQ", sequenceName =
        "TRAMITE_RELACION_TRAMIT_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Tramite
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID")
    public Tramite getTramite1() {
        return this.tramite1;
    }

    public void setTramite1(Tramite tramite1) {
        this.tramite1 = tramite1;
    }

    //bi-directional many-to-one association to Tramite
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RELACIONADO_TRAMITE_ID")
    public Tramite getTramiteRelacionado() {
        return this.tramiteRelacionado;
    }

    public void setTramiteRelacionado(Tramite tramite2) {
        this.tramiteRelacionado = tramite2;
    }

}
