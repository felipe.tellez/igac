/*
 * Proyecto SNC 2017
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * @author felipe.cadena
 */
public enum EDistribucionRangoReporte {

    RURAL("RURAL"),
    URBANO("URBANO"),
    CORREGIMIENTO("CORREGIMIENTO");

    private final String codigo;

    private EDistribucionRangoReporte(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
