/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con los componentes de una unidad de construcción
 *
 * @author juan.agudelo
 */
public enum EComponenteUnidadConstruccion {

    ACABADOS_PRINCIPALES("ACABADOS PRINCIPALES"),
    BANIO("BAÑO"),
    COCINA("COCINA"),
    ESTRUCTURA("ESTRUCTURA"),
    COCINA2("COCINA");

    private String nombre;

    private EComponenteUnidadConstruccion(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String id) {
        this.nombre = id;
    }

}
