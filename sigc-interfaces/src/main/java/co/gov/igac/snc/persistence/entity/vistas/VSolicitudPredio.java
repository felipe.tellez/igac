package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;

import java.util.Date;

/**
 * @author david.cifuentes The persistent class for the V_SOLICITUD_PREDIO database table.
 * @author fabio.navarrete -> Se agrega método para obtener el nombre completo del filtro de
 * búsqueda -> Se modificó el campo tramiteId para mapearlo como un objeto trámite
 *
 */
@Entity
@Table(name = "V_SOLICITUD_PREDIO")
public class VSolicitudPredio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CONSECUTIVO")
    private Integer consecutivo;

    @Column(name = "DIGITO_VERIFICACION")
    private String digitoVerificacion;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RADICACION_SOLICITUD")
    private Date fechaRadicacionSolicitud;

    @Column(name = "NUMERO_IDENTIFICACION")
    private String numeroIdentificacion;

    @Column(name = "NUMERO_PREDIAL")
    private String numeroPredial;

    @Column(name = "NUMERO_RADICACION_SOLICITUD")
    private String numeroRadicacionSolicitud;

    @Column(name = "NUMERO_RADICACION_TRAMITE")
    private String numeroRadicacionTramite;

    @Column(name = "PREDIO_ID")
    private Long predioId;

    @Column(name = "PRIMER_APELLIDO")
    private String primerApellido;

    @Column(name = "PRIMER_NOMBRE")
    private String primerNombre;

    @Column(name = "RAZON_SOCIAL")
    private String razonSocial;

    @Column(name = "SEGUNDO_APELLIDO")
    private String segundoApellido;

    @Column(name = "SEGUNDO_NOMBRE")
    private String segundoNombre;

    @Column(name = "SOLICITANTE_ID")
    private Long solicitanteId;

    @Column(name = "SOLICITUD_ID")
    private Long solicitudId;

    @Column(name = "TIPO_IDENTIFICACION")
    private String tipoIdentificacion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID")
    private Tramite tramite;

    public VSolicitudPredio() {
    }

    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    public Date getFechaRadicacionSolicitud() {
        return this.fechaRadicacionSolicitud;
    }

    public void setFechaRadicacionSolicitud(Date fechaRadicacionSolicitud) {
        this.fechaRadicacionSolicitud = fechaRadicacionSolicitud;
    }

    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getNumeroRadicacionSolicitud() {
        return this.numeroRadicacionSolicitud;
    }

    public void setNumeroRadicacionSolicitud(String numeroRadicacionSolicitud) {
        this.numeroRadicacionSolicitud = numeroRadicacionSolicitud;
    }

    public String getNumeroRadicacionTramite() {
        return this.numeroRadicacionTramite;
    }

    public void setNumeroRadicacionTramite(String numeroRadicacionTramite) {
        this.numeroRadicacionTramite = numeroRadicacionTramite;
    }

    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public Long getSolicitanteId() {
        return this.solicitanteId;
    }

    public void setSolicitanteId(Long solicitanteId) {
        this.solicitanteId = solicitanteId;
    }

    public Long getSolicitudId() {
        return this.solicitudId;
    }

    public void setSolicitudId(Long solicitudId) {
        this.solicitudId = solicitudId;
    }

    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Transient
    public String getNombreCompleto() {
        StringBuffer sb = new StringBuffer();
        sb.append("");
        if (this.tipoIdentificacion != null && this.tipoIdentificacion.equals(
            EPersonaTipoIdentificacion.NIT
                .getCodigo())) {
            if (this.razonSocial != null) {
                sb.append(this.razonSocial).append(" ");
            }
        } else {
            if (this.primerNombre != null) {
                sb.append(this.primerNombre).append(" ");
            }
            if (this.segundoNombre != null) {
                sb.append(this.segundoNombre).append(" ");
            }
            if (this.primerApellido != null) {
                sb.append(this.primerApellido).append(" ");
            }
            if (this.segundoApellido != null) {
                sb.append(this.segundoApellido).append(" ");
            }
        }
        return sb.toString();
    }

}
