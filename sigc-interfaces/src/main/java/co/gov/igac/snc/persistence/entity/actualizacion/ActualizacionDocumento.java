package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.generales.Documento;

import java.util.Date;

/**
 * The persistent class for the ACTUALIZACION_DOCUMENTO database table.
 *
 *
 * @modified by: franz.gamba -> se cambio el ID del documento de tipo BigDecimal a Long para
 * concordar con el tipo de ID de documento; Seagrego el atributo transient fueGenerado.05/12/2011.
 */
@Entity
@Table(name = "ACTUALIZACION_DOCUMENTO")
@NamedQueries(@NamedQuery(name = "findByActualizacionId", query =
    "SELECT ad FROM ActualizacionDocumento ad WHERE ad.actualizacion.id = :actualizacionId"))
public class ActualizacionDocumento implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7577182398669950621L;

    private Long id;
    private Date fechaLog;
    private String usuarioLog;
    private Actualizacion actualizacion;
    private Documento documento;

    /**
     * Permite ver si el Documento de la actualizacion fu egenerado o no
     */
    private boolean fueGenerado = false;

    /**
     * Indica el documento en el orden en que aparecen en las pantallas de Actualización
     */
    private String opcionDocumentoActualizacion;

    public ActualizacionDocumento() {
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_DOCUMENTO_ID_GENERATOR", sequenceName =
        "ACTUALIZACION_DOCUMENTO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_DOCUMENTO_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(TemporalType.DATE)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = false)
    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID")
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    @Transient
    public boolean isFueGenerado() {
        return fueGenerado;
    }

    public void setFueGenerado(boolean fueGenerado) {
        this.fueGenerado = fueGenerado;
    }

    @Transient
    public String getOpcionDocumentoActualizacion() {
        return opcionDocumentoActualizacion;
    }

    public void setOpcionDocumentoActualizacion(
        String opcionDocumentoActualizacion) {
        this.opcionDocumentoActualizacion = opcionDocumentoActualizacion;
    }

}
