package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the TRAMITE_DEPURACION database table.
 *
 * @modified felipe.cadena -- 15-10-2013 -- Se agrega el campo transient para la estructura
 * organizacional
 */
@Entity
@Table(name = "TRAMITE_DEPURACION", schema = "SNC_TRAMITE")
public class TramiteDepuracion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long id;
    private String campo;
    private String digitalizador;
    private String ejecutor;
    private Date fechaLog;
    private String inconsistencias;
    private String levantamiento;
    private String nombreDigitalizador;
    private String nombreEjecutor;
    private String nombreTopografo;
    private String nuevosTramites;
    private String razonesRechazo;
    private String razonesSolicitudDepuracion;
    private String tareasRealizar;
    private String topografo;
    private String usuarioLog;
    private String viable;
    private Tramite tramite;
    private List<TramiteDepuracionObservacion> tramiteDepuracionObservacions;

    private EstructuraOrganizacional estructuraOrganizacional;

    public TramiteDepuracion() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRAMITE_DEPURACION_ID_SEQ")
    @SequenceGenerator(name = "TRAMITE_DEPURACION_ID_SEQ", sequenceName =
        "TRAMITE_DEPURACION_ID_SEQ", allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(length = 2)
    public String getCampo() {
        return this.campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    @Column(length = 100)
    public String getDigitalizador() {
        return this.digitalizador;
    }

    public void setDigitalizador(String digitalizador) {
        this.digitalizador = digitalizador;
    }

    @Column(length = 100)
    public String getEjecutor() {
        return this.ejecutor;
    }

    public void setEjecutor(String ejecutor) {
        this.ejecutor = ejecutor;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(length = 2000)
    public String getInconsistencias() {
        return this.inconsistencias;
    }

    public void setInconsistencias(String inconsistencias) {
        this.inconsistencias = inconsistencias;
    }

    @Column(length = 2)
    public String getLevantamiento() {
        return this.levantamiento;
    }

    public void setLevantamiento(String levantamiento) {
        this.levantamiento = levantamiento;
    }

    @Column(name = "NOMBRE_DIGITALIZADOR", length = 100)
    public String getNombreDigitalizador() {
        return this.nombreDigitalizador;
    }

    public void setNombreDigitalizador(String nombreDigitalizador) {
        this.nombreDigitalizador = nombreDigitalizador;
    }

    @Column(name = "NOMBRE_EJECUTOR", length = 100)
    public String getNombreEjecutor() {
        return this.nombreEjecutor;
    }

    public void setNombreEjecutor(String nombreEjecutor) {
        this.nombreEjecutor = nombreEjecutor;
    }

    @Column(name = "NOMBRE_TOPOGRAFO", length = 100)
    public String getNombreTopografo() {
        return this.nombreTopografo;
    }

    public void setNombreTopografo(String nombreTopografo) {
        this.nombreTopografo = nombreTopografo;
    }

    @Column(name = "NUEVOS_TRAMITES", length = 2)
    public String getNuevosTramites() {
        return this.nuevosTramites;
    }

    public void setNuevosTramites(String nuevosTramites) {
        this.nuevosTramites = nuevosTramites;
    }

    @Column(name = "RAZONES_RECHAZO", length = 2000)
    public String getRazonesRechazo() {
        return this.razonesRechazo;
    }

    public void setRazonesRechazo(String razonesRechazo) {
        this.razonesRechazo = razonesRechazo;
    }

    @Column(name = "RAZONES_SOLICITUD_DEPURACION", length = 2000)
    public String getRazonesSolicitudDepuracion() {
        return this.razonesSolicitudDepuracion;
    }

    public void setRazonesSolicitudDepuracion(String razonesSolicitudDepuracion) {
        this.razonesSolicitudDepuracion = razonesSolicitudDepuracion;
    }

    @Column(name = "TAREAS_REALIZAR", length = 2000)
    public String getTareasRealizar() {
        return this.tareasRealizar;
    }

    public void setTareasRealizar(String tareasRealizar) {
        this.tareasRealizar = tareasRealizar;
    }

    @Column(length = 100)
    public String getTopografo() {
        return this.topografo;
    }

    public void setTopografo(String topografo) {
        this.topografo = topografo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(length = 2)
    public String getViable() {
        return this.viable;
    }

    public void setViable(String viable) {
        this.viable = viable;
    }

    // bi-directional many-to-one association to Tramite
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    // bi-directional many-to-one association to TramiteDepuracionObservacion
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tramiteDepuracion")
    public List<TramiteDepuracionObservacion> getTramiteDepuracionObservacions() {
        return this.tramiteDepuracionObservacions;
    }

    public void setTramiteDepuracionObservacions(
        List<TramiteDepuracionObservacion> tramiteDepuracionObservacions) {
        this.tramiteDepuracionObservacions = tramiteDepuracionObservacions;
    }

    //Este dato solo se utiliza en la vista asi que debe inicializarse explicitamente
    @Transient
    public EstructuraOrganizacional getEstructuraOrganizacional() {
        return estructuraOrganizacional;
    }

    public void setEstructuraOrganizacional(EstructuraOrganizacional estructuraOrganizacional) {
        this.estructuraOrganizacional = estructuraOrganizacional;
    }

}
