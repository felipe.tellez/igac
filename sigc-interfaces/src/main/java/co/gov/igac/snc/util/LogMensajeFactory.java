package co.gov.igac.snc.util;

import java.util.Date;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.generales.LogMensaje;

/**
 * Clase encargada de crear objetos de tipo LogMensaje
 *
 * @author juan.mendez
 *
 */
public class LogMensajeFactory {

    private final static String INFO = "INFO";

    /**
     * Mêtodo para crear instancias de LogMensaje para el servicio de reportes
     *
     * @return
     */
    public static LogMensaje getLogMensaje(UsuarioDTO usuario, String tipoMensaje) {
        LogMensaje mensaje = new LogMensaje();
        mensaje.setFechaLog(new Date());
        mensaje.setNivel(INFO);
        mensaje.setPrograma(tipoMensaje);
        mensaje.setUsuarioLog(usuario.getLogin());
        return mensaje;
    }

}
