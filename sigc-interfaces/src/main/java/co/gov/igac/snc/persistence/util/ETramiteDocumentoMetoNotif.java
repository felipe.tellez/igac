package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion de los metodos de notificar un TrámiteDocumento
 *
 * @author franz.gamba
 *
 */
public enum ETramiteDocumentoMetoNotif {
    PERSONAL("6", "Personal"),
    CORREO_CERTIFICADO("8", "Correo certificado"),
    EDICTO("7", "Edicto");

    private String codigo;
    private String valor;

    private ETramiteDocumentoMetoNotif(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
