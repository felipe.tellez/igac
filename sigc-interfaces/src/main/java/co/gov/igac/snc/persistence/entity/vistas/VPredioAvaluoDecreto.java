package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * The persistent class for the V_PREDIO_AVALUO_DECRETO database table.
 *
 * @author david.cifuentes
 *
 */
@Entity
@Table(name = "V_PREDIO_AVALUO_DECRETO")
public class VPredioAvaluoDecreto implements Serializable {

    private static final long serialVersionUID = 1L;
    private String autoavaluo;
    private Date decretoFecha;
    private String decretoNumero;
    private Date fechaInscripcionCatastral;
    private Long id;
    private String justificacionAvaluo;
    private String notaAvaluo;
    private String observaciones;
    private Predio predio;
    private Documento soporteDocumento;
    private Double valorTerreno;
    private Double valorTotalAvaluoCatastral;
    private Double valorTotalConsConvencional;
    private Double valorTotalConsNconvencional;
    private Double valorTotalConstruccion;
    private Date vigencia;

    public VPredioAvaluoDecreto() {
    }

    @Id
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID")
    public Documento getSoporteDocumento() {
        return this.soporteDocumento;
    }

    public void setSoporteDocumento(Documento soporteDocumento) {
        this.soporteDocumento = soporteDocumento;
    }

    public String getAutoavaluo() {
        return this.autoavaluo;
    }

    public void setAutoavaluo(String autoavaluo) {
        this.autoavaluo = autoavaluo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "DECRETO_FECHA")
    public Date getDecretoFecha() {
        return this.decretoFecha;
    }

    public void setDecretoFecha(Date decretoFecha) {
        this.decretoFecha = decretoFecha;
    }

    @Column(name = "DECRETO_NUMERO")
    public String getDecretoNumero() {
        return this.decretoNumero;
    }

    public void setDecretoNumero(String decretoNumero) {
        this.decretoNumero = decretoNumero;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL")
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "JUSTIFICACION_AVALUO")
    public String getJustificacionAvaluo() {
        return this.justificacionAvaluo;
    }

    public void setJustificacionAvaluo(String justificacionAvaluo) {
        this.justificacionAvaluo = justificacionAvaluo;
    }

    @Column(name = "NOTA_AVALUO")
    public String getNotaAvaluo() {
        return this.notaAvaluo;
    }

    public void setNotaAvaluo(String notaAvaluo) {
        this.notaAvaluo = notaAvaluo;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "VALOR_TERRENO")
    public Double getValorTerreno() {
        return this.valorTerreno;
    }

    public void setValorTerreno(Double valorTerreno) {
        this.valorTerreno = valorTerreno;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_CATASTRAL")
    public Double getValorTotalAvaluoCatastral() {
        return this.valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(Double valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    @Column(name = "VALOR_TOTAL_CONS_CONVENCIONAL")
    public Double getValorTotalConsConvencional() {
        return this.valorTotalConsConvencional;
    }

    public void setValorTotalConsConvencional(Double valorTotalConsConvencional) {
        this.valorTotalConsConvencional = valorTotalConsConvencional;
    }

    @Column(name = "VALOR_TOTAL_CONS_NCONVENCIONAL")
    public Double getValorTotalConsNconvencional() {
        return this.valorTotalConsNconvencional;
    }

    public void setValorTotalConsNconvencional(Double valorTotalConsNconvencional) {
        this.valorTotalConsNconvencional = valorTotalConsNconvencional;
    }

    @Column(name = "VALOR_TOTAL_CONSTRUCCION")
    public Double getValorTotalConstruccion() {
        return this.valorTotalConstruccion;
    }

    public void setValorTotalConstruccion(Double valorTotalConstruccion) {
        this.valorTotalConstruccion = valorTotalConstruccion;
    }

    @Temporal(TemporalType.DATE)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

}
