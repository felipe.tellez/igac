package co.gov.igac.snc.persistence.entity.generales;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * NumeracionDocumento entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "NUMERACION", schema = "SNC_GENERALES")
public class Numeracion implements java.io.Serializable {

    // Fields
    private Long id;
    private String descripcion;
    private Byte estructuraOrganizacional;
    private Byte departamento;
    private Byte municipio;
    private Byte tipoDocumento;
    private Byte anio;
    private Byte consecutivo;
    private String usuarioLog;
    private Date fechaLog;
    private Set<TipoDocumento> tipoDocumentos = new HashSet<TipoDocumento>(0);

    // Constructors
    /** default constructor */
    public Numeracion() {
    }

    /** minimal constructor */
    public Numeracion(Long id, String descripcion,
        Byte estructuraOrganizacional, Byte departamento, Byte municipio,
        Byte tipoDocumento, Byte anio, Byte consecutivo, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.descripcion = descripcion;
        this.estructuraOrganizacional = estructuraOrganizacional;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoDocumento = tipoDocumento;
        this.anio = anio;
        this.consecutivo = consecutivo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Numeracion(Long id, String descripcion,
        Byte estructuraOrganizacional, Byte departamento, Byte municipio,
        Byte tipoDocumento, Byte anio, Byte consecutivo, String usuarioLog,
        Date fechaLog, Set<TipoDocumento> tipoDocumentos) {
        this.id = id;
        this.descripcion = descripcion;
        this.estructuraOrganizacional = estructuraOrganizacional;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoDocumento = tipoDocumento;
        this.anio = anio;
        this.consecutivo = consecutivo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.tipoDocumentos = tipoDocumentos;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Numeracion_id_seq")
    @SequenceGenerator(name = "Numeracion_id_seq", sequenceName = "NUMERACION_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DESCRIPCION", nullable = false, length = 250)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL", nullable = false, precision = 2, scale = 0)
    public Byte getEstructuraOrganizacional() {
        return this.estructuraOrganizacional;
    }

    public void setEstructuraOrganizacional(Byte estructuraOrganizacional) {
        this.estructuraOrganizacional = estructuraOrganizacional;
    }

    @Column(name = "DEPARTAMENTO", nullable = false, precision = 2, scale = 0)
    public Byte getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Byte departamento) {
        this.departamento = departamento;
    }

    @Column(name = "MUNICIPIO", nullable = false, precision = 2, scale = 0)
    public Byte getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Byte municipio) {
        this.municipio = municipio;
    }

    @Column(name = "TIPO_DOCUMENTO", nullable = false, precision = 2, scale = 0)
    public Byte getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(Byte tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Column(name = "ANIO", nullable = false, precision = 2, scale = 0)
    public Byte getAnio() {
        return this.anio;
    }

    public void setAnio(Byte anio) {
        this.anio = anio;
    }

    @Column(name = "CONSECUTIVO", nullable = false, precision = 2, scale = 0)
    public Byte getConsecutivo() {
        return this.consecutivo;
    }

    public void setConsecutivo(Byte consecutivo) {
        this.consecutivo = consecutivo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "numeracionDocumento")
    public Set<TipoDocumento> getTipoDocumentos() {
        return this.tipoDocumentos;
    }

    public void setTipoDocumentos(Set<TipoDocumento> tipoDocumentos) {
        this.tipoDocumentos = tipoDocumentos;
    }

}
