package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the V_COMISION database table
 */
/*
 * @modified by: pedro.garcia what: - el maldito eclipse genera los entities con las anotaciones a
 * nivel del atributo, y el pendejo jboss chilla (no traduce bien los queries (los nombres de las
 * columnas quedan con el mismo nombre del atributo), no reconoce el id, etc) - adición de
 * namedQueries - cambio de tipo de datos para los atributos duracion, duracionTotal id, tramites;
 * de BigDecimal a Double - adición de anotación @Id al getId() - adición de atributos transient
 * razonAjuste y observaciones para facilitar la modificación de una comisión
 *
 * @modified by: javier.aponte se agrega condición en el named query de findByTerritorialAndEstado
 * que el número de la comisión no se null porque en el caso de uso de elaborar informe sustentando
 * el concepto fue necesario crear comisiones ficticias para asociarlas con el trámite cuando éste
 * tenía clasificación de oficina
 *
 *  * OJO: el campo 'id' se refiere al id de la comisión no al id del registro en la tabla
 *
 * @modified by: felipe.cadena Se agrega una condición para que solo consulte las comisiones
 * relacionadas a los tramites que vienen del proceso.
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "findByTerritorialAndEstado",
        query = "from VComision c where c.estado = :estadoComision and " +
        " c.territorialCodigo = :idTerritorial and c.numero IS NOT NULL" +
        " and c.id in :idComisiones")
})
@Table(name = "V_COMISION")
public class VComision implements Serializable {

    private static final long serialVersionUID = 1L;

    private String conductor;

    private Double duracion;
    private Double duracionTotal;
    private String estado;
    private String objeto;
    private Date fechaFin;
    private Date fechaInicio;

    /**
     * ids de los ejecutores relacionados con la comisión
     */
    private String funcionarioEjecutor;

    private Long id;
    private String municipio;
    private String nombreFuncionarioEjecutor;
    private String numero;
    private String rolFuncionarioEjecutor;
    private String placaVehiculo;
    private String territorialCodigo;
    private String tipoComision;
    private Long tramites;
    private String observaciones;

    /**
     * campos transient adicionados para la modificación de una comisión
     */
    private Double duracionAjuste;
    private String razonAjuste;
    private Long memorandoDocumentoId;

    /**
     * campos adicionados para lo del histórico
     */
    private Date nuevaFechaInicio;
    private Date nuevaFechaFin;
    private String motivo;

    /*
     * Atributo que visualiza si alguno de los tramites asociados a la comision tienen o un derecho
     * de peticion o tutela asociado. @modified by: leidy.gonzalez
     */
    private String formaPeticion;

//--------------------------------------------------------------------------------------------------	
    public VComision() {
    }

    public String getConductor() {
        return this.conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public Double getDuracion() {
        return this.duracion;
    }

    public void setDuracion(Double duracion) {
        this.duracion = duracion;
    }

    @Column(name = "DURACION_TOTAL")
    public Double getDuracionTotal() {
        return this.duracionTotal;
    }

    public void setDuracionTotal(Double duracionTotal) {
        this.duracionTotal = duracionTotal;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObjeto() {
        return this.objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIN")
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO")
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Id
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    @Column(name = "FUNCIONARIO_EJECUTOR")
    public String getFuncionarioEjecutor() {
        return this.funcionarioEjecutor;
    }

    public void setFuncionarioEjecutor(String uncionarioEjecutor) {
        this.funcionarioEjecutor = uncionarioEjecutor;
    }

    @Column(name = "NOMBRE_FUNCIONARIO_EJECUTOR")
    public String getNombreFuncionarioEjecutor() {
        return this.nombreFuncionarioEjecutor;
    }

    public void setNombreFuncionarioEjecutor(String nombreFuncionarioEjecutor) {
        this.nombreFuncionarioEjecutor = nombreFuncionarioEjecutor;
    }

    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "PLACA_VEHICULO")
    public String getPlacaVehiculo() {
        return this.placaVehiculo;
    }

    public void setPlacaVehiculo(String placaVehiculo) {
        this.placaVehiculo = placaVehiculo;
    }

    @Column(name = "TERRITORIAL_CODIGO")
    public String getTerritorialCodigo() {
        return this.territorialCodigo;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.territorialCodigo = territorialCodigo;
    }

    @Column(name = "TIPO_COMISION")
    public String getTipoComision() {
        return this.tipoComision;
    }

    public void setTipoComision(String tipoC) {
        this.tipoComision = tipoC;
    }

    public Long getTramites() {
        return this.tramites;
    }

    public void setTramites(Long tramites) {
        this.tramites = tramites;
    }

    @Column(name = "DURACION_AJUSTE")
    public Double getDuracionAjuste() {
        return this.duracionAjuste;
    }

    public void setDuracionAjuste(Double duracionAjuste) {
        this.duracionAjuste = duracionAjuste;
    }

    @Transient
    public String getRazonAjuste() {
        return this.razonAjuste;
    }

    public void setRazonAjuste(String razonAjuste) {
        this.razonAjuste = razonAjuste;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "NUEVA_FECHA_INICIO")
    public Date getNuevaFechaInicio() {
        return nuevaFechaInicio;
    }

    public void setNuevaFechaInicio(Date nuevaFechaInicio) {
        this.nuevaFechaInicio = nuevaFechaInicio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "NUEVA_FECHA_FIN")
    public Date getNuevaFechaFin() {
        return nuevaFechaFin;
    }

    public void setNuevaFechaFin(Date nuevaFechaFin) {
        this.nuevaFechaFin = nuevaFechaFin;
    }

    public String getMotivo() {
        return this.motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Column(name = "ROL_FUNCIONARIO_EJECUTOR")
    public String getRolFuncionarioEjecutor() {
        return this.rolFuncionarioEjecutor;
    }

    public void setRolFuncionarioEjecutor(String rFuncionarioEjecutor) {
        this.rolFuncionarioEjecutor = rFuncionarioEjecutor;
    }

    @Transient
    public Long getMemorandoDocumentoId() {
        return this.memorandoDocumentoId;
    }

    public void setMemorandoDocumentoId(Long memorandoDocumentoId) {
        this.memorandoDocumentoId = memorandoDocumentoId;
    }

    /*
     * Metodo que visualiza si alguno de los tramites asociados a la comision tienen o un derecho de
     * peticion o tutela asociado. @modified by: leidy.gonzalez
     */
    @Column(name = "FORMA_PETICION")
    public String getFormaPeticion() {
        return formaPeticion;
    }

    public void setFormaPeticion(String formaPeticion) {
        this.formaPeticion = formaPeticion;
    }
}
