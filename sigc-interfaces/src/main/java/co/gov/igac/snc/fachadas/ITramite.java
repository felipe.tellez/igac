package co.gov.igac.snc.fachadas;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import java.util.List;
import javax.ejb.Remote;

/**
 * Interfaz Local para el acceso a los servicios de ITramite
 *
 * Nota: Los Métodos se declaran en la interfaz Local (ITramiteLocal)
 *
 * @author juan.mendez
 * @version 2.0
 */
@Remote
public interface ITramite extends ITramiteLocal {

}
