package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the CONTROL_CALIDAD_AVALUO database table.
 *
 */
@Entity
@Table(name = "CONTROL_CALIDAD_AVALUO")
public class ControlCalidadAvaluo implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String actividad;
    private String aprobo;
    private Long avaluoId;
    private String comiteAvaluos;
    private Date fechaLog;
    private ProfesionalAvaluo profesionalAvaluos;
    private Asignacion asignacion;
    private String territorialCodigo;
    private String usuarioLog;
    private List<ControlCalidadAvaluoRev> controlCalidadAvaluoRevs;

    public ControlCalidadAvaluo() {
    }

    @Id
    @SequenceGenerator(name = "CONTROL_CALIDAD_AVALUO_ID_GENERATOR", sequenceName =
        "CONTROL_CALIDAD_AVALUO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "CONTROL_CALIDAD_AVALUO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActividad() {
        return this.actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getAprobo() {
        return this.aprobo;
    }

    public void setAprobo(String aprobo) {
        this.aprobo = aprobo;
    }

    @Column(name = "AVALUO_ID")
    public Long getAvaluoId() {
        return this.avaluoId;
    }

    public void setAvaluoId(Long avaluoId) {
        this.avaluoId = avaluoId;
    }

    @Column(name = "COMITE_AVALUOS")
    public String getComiteAvaluos() {
        return this.comiteAvaluos;
    }

    public void setComiteAvaluos(String comiteAvaluos) {
        this.comiteAvaluos = comiteAvaluos;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    // bi-directional many-to-one association to ProfesionalAvaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESIONAL_AVALUOS_ID", nullable = false)
    public ProfesionalAvaluo getProfesionalAvaluos() {
        return this.profesionalAvaluos;
    }

    public void setProfesionalAvaluos(ProfesionalAvaluo profesionalAvaluos) {
        this.profesionalAvaluos = profesionalAvaluos;
    }

    // bi-directional many-to-one association to AsignacionControlCalidad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASIGNACION_ID", nullable = false)
    public Asignacion getAsignacion() {
        return this.asignacion;
    }

    public void setAsignacion(Asignacion asignacion) {
        this.asignacion = asignacion;
    }

    @Column(name = "TERRITORIAL_CODIGO")
    public String getTerritorialCodigo() {
        return this.territorialCodigo;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.territorialCodigo = territorialCodigo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ControlCalidadAvaluoRev
    @OneToMany(mappedBy = "controlCalidadAvaluo", cascade = CascadeType.ALL)
    public List<ControlCalidadAvaluoRev> getControlCalidadAvaluoRevs() {
        return this.controlCalidadAvaluoRevs;
    }

    public void setControlCalidadAvaluoRevs(List<ControlCalidadAvaluoRev> controlCalidadAvaluoRevs) {
        this.controlCalidadAvaluoRevs = controlCalidadAvaluoRevs;
    }

    @Transient
    public Integer getNumeroRevisiones() {
        if (this.controlCalidadAvaluoRevs != null) {
            return this.controlCalidadAvaluoRevs.size();
        } else {
            return null;
        }
    }

}
