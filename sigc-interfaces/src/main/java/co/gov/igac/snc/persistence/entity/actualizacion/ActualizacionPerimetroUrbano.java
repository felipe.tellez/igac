package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * Entidad Actualizacion de Perimetro Urbano
 *
 * @author andres.eslava
 *
 */
@Entity
@Table(name = "ACTUALIZACION_PERIMETRO_URBANO")
public class ActualizacionPerimetroUrbano implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long actualizacionId;
    private String nombreAlcalde;
    private Documento acuerdoDocumento;
    private Documento oficioDocumento;
    private String usuarioLog;
    private Date fechaLog;

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_PERIMETRO_URBANO_ID_GENERATOR", sequenceName =
        "ACTUALIZACION_PERIMETRO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_PERIMETRO_URBANO_ID_GENERATOR")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACTUALIZACION_ID")
    public Long getActualizacionId() {
        return actualizacionId;
    }

    public void setActualizacionId(Long actualizacionId) {
        this.actualizacionId = actualizacionId;
    }

    @Column(name = "NOMBRE_ALCALDE")
    public String getNombreAlcalde() {
        return nombreAlcalde;
    }

    public void setNombreAlcalde(String nombreAlcalde) {
        this.nombreAlcalde = nombreAlcalde;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @JoinColumn(name = "ACUERDO_DOCUMENTO_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    public Documento getAcuerdoDocumento() {
        return acuerdoDocumento;
    }

    public void setAcuerdoDocumento(Documento acuerdoDocumento) {
        this.acuerdoDocumento = acuerdoDocumento;
    }

    @JoinColumn(name = "OFICIO_DOCUMENTO_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    public Documento getOficioDocumento() {
        return oficioDocumento;
    }

    public void setOficioDocumento(Documento oficioDocumento) {
        this.oficioDocumento = oficioDocumento;
    }

}
