package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the ACTUALIZACION_RESPONSABLE database table.
 *
 * @author franz.gamba
 */
@Entity
@Table(name = "ACTUALIZACION_RESPONSABLE")
public class ActualizacionResponsable implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4106378592113526846L;

    private Long id;
    private Date fechaLog;
    private String identificacion;
    private String nombre;
    private String usuarioLog;
    private Actualizacion actualizacion;
    private List<RecursoHumano> recursoHumanos;

    public ActualizacionResponsable() {
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_RESPONSABLE_ID_GENERATOR",
        sequenceName = "ACTUALIZACION_RESPONSAB_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_RESPONSABLE_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getIdentificacion() {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID")
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    //bi-directional many-to-one association to RecursoHumano
    @OneToMany(mappedBy = "actualizacionResponsable")
    public List<RecursoHumano> getRecursoHumanos() {
        return this.recursoHumanos;
    }

    public void setRecursoHumanos(List<RecursoHumano> recursoHumanos) {
        this.recursoHumanos = recursoHumanos;
    }

}
