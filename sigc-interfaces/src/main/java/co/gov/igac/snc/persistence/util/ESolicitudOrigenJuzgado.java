package co.gov.igac.snc.persistence.util;

/**
 * Enumeración que representa los datos contenidos en el Dominio SOLICITUD_ORIGEN_JUZGADO
 *
 * Datos de posibles tipos de solicitud segun el tipo de solicitante JUZGADO
 *
 *
 * @author rodrigo.hernandez
 *
 */
public enum ESolicitudOrigenJuzgado {

    ADMINISTRATIVO("JA", "Administrativo"),
    PROCESO_PENAL("JP", "Proceso penal"),
    PROCESO_CIVIL("JC", "Proceso civil");

    private String codigo;
    private String valor;

    private ESolicitudOrigenJuzgado(String codigo, String valor) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
