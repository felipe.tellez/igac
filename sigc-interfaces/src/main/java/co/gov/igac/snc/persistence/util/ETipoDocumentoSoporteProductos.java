package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para los Tipos de documentos de Soporte para los casos de uso del servidor de
 * productos. Las enumeraciones corresponden a los datos de la tabla Tipo_Documento
 *
 * @author javier.barajas
 */
public enum ETipoDocumentoSoporteProductos {

    FACTURA_DE_VENTA(Long.valueOf(2007), "Factura de Venta"),
    MEMORANDO(Long.valueOf(2008), "Memorando"),
    OFICIO(Long.valueOf(2009), "Oficio"),
    CIRCULAR(Long.valueOf(2010), "Circular"),
    RESOLUCION(Long.valueOf(2011), "Resolución"),
    ACTA(Long.valueOf(2012), "Acta");

    private Long id;
    private String nombre;

    private ETipoDocumentoSoporteProductos(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    /**
     * retorna el id del tipo de documento
     *
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     * retorna el nombre del tipo de documento
     *
     * @return
     */
    public String nombre() {
        return this.nombre;
    }

}
