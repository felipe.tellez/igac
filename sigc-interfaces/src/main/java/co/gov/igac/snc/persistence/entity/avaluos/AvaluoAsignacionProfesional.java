package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the AVALUO_ASIGNACION_PROFESIONAL database table.
 */
/*
 * @modified rodrigo.hernandez - 25/09/2012 - Adicion de campo profesionalAvaluoContratoId para
 * establecer relacion con la tabla PROFESIONAL_AVALUO_CONTRATO
 *
 */
@Entity
@Table(name = "AVALUO_ASIGNACION_PROFESIONAL", schema = "SNC_AVALUOS")
public class AvaluoAsignacionProfesional implements Serializable {

    private static final long serialVersionUID = -4399269523314382586L;
    private Long id;
    private String aceptada;
    private String actividad;
    private Date fechaDesde;
    private Date fechaHasta;
    private Date fechaLog;
    private String usuarioLog;
    private Long profesionalAvaluoContratoId;

    private Avaluo avaluo;
    private ProfesionalAvaluo profesionalAvaluo;
    private OrdenPractica ordenPractica;
    private Asignacion asignacion;

    public AvaluoAsignacionProfesional() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_ASIGNACION_PROFE_SEQ_GEN", sequenceName =
        "AVALUO_ASIGNACION_PROFE_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_ASIGNACION_PROFE_SEQ_GEN")
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false, length = 2)
    public String getAceptada() {
        return this.aceptada;
    }

    public void setAceptada(String aceptada) {
        this.aceptada = aceptada;
    }

    @Column(length = 30)
    public String getActividad() {
        return this.actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASIGNACION_ID", nullable = false)
    public Asignacion getAsignacion() {
        return this.asignacion;
    }

    public void setAsignacion(Asignacion asignacion) {
        this.asignacion = asignacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DESDE", nullable = false)
    public Date getFechaDesde() {
        return this.fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_HASTA")
    public Date getFechaHasta() {
        return this.fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to Avaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    // bi-directional many-to-one association to ProfesionalAvaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESIONAL_AVALUOS_ID", nullable = false)
    public ProfesionalAvaluo getProfesionalAvaluo() {
        return this.profesionalAvaluo;
    }

    public void setProfesionalAvaluo(ProfesionalAvaluo profesionalAvaluo) {
        this.profesionalAvaluo = profesionalAvaluo;
    }

    @Column(name = "PROFESIONAL_AVALUOS_CONTRAT_ID", precision = 10)
    public Long getProfesionalAvaluoContratoId() {
        return profesionalAvaluoContratoId;
    }

    public void setProfesionalAvaluoContratoId(
        Long profesionalAvaluoContratoId) {
        this.profesionalAvaluoContratoId = profesionalAvaluoContratoId;
    }

    // bi-directional many-to-one association to OrdenPractica
    @ManyToOne
    @JoinColumn(name = "ORDEN_PRACTICA_ID")
    public OrdenPractica getOrdenPractica() {
        return this.ordenPractica;
    }

    public void setOrdenPractica(OrdenPractica ordenPractica) {
        this.ordenPractica = ordenPractica;
    }

}
