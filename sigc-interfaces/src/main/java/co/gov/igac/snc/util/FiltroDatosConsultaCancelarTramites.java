/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.List;

/**
 * Filtro para los campos de la consulta de trámites vigentes
 *
 * @author pedro.garcia
 */
public class FiltroDatosConsultaCancelarTramites implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 476049427734912124L;

    /** La estructura organizacional de los tramites a cancelar */
    private List<String> municipiosJurisdiccion;

    private String numeroSolicitud;
    private String numeroRadicacion;
    private String numeroIdentificacion;
    private String tipoTramite;
    private String claseMutacion;
    private String subtipo;
    private String departamentoNumPredial;
    private String municipioNumPredial;
    private String zonaNumPredial;
    private String sectorNumPredial;
    private String comunaNumPredial;
    private String barrioNumPredial;
    private String manzanaNumPredial;
    private String terrenoNumPredial;
    private String condicionPropiedadNumPredial;
    private String edificioNumPredial;
    private String pisoNumPredial;
    private String unidadNumPredial;
    private boolean esVisibleMnpios;

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getClaseMutacion() {
        return claseMutacion;
    }

    public void setClaseMutacion(String claseMutacion) {
        this.claseMutacion = claseMutacion;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    public String getDepartamentoNumPredial() {
        return departamentoNumPredial;
    }

    public void setDepartamentoNumPredial(String departamentoNumPredial) {
        if (departamentoNumPredial != null && !departamentoNumPredial.isEmpty()) {
            while (departamentoNumPredial.length() < 2) {
                departamentoNumPredial = "0" + departamentoNumPredial;
            }
        }
        this.departamentoNumPredial = departamentoNumPredial;
    }

    public String getMunicipioNumPredial() {
        return municipioNumPredial;
    }

    public void setMunicipioNumPredial(String municipioNumPredial) {
        if (municipioNumPredial != null && !municipioNumPredial.isEmpty()) {
            while (municipioNumPredial.length() < 3) {
                municipioNumPredial = "0" + municipioNumPredial;
            }
        }
        this.municipioNumPredial = municipioNumPredial;
    }

    public String getZonaNumPredial() {
        return zonaNumPredial;
    }

    public void setZonaNumPredial(String zonaNumPredial) {
        if (zonaNumPredial != null && !zonaNumPredial.isEmpty()) {
            while (zonaNumPredial.length() < 2) {
                zonaNumPredial = "0" + zonaNumPredial;
            }
        }
        this.zonaNumPredial = zonaNumPredial;
    }

    public String getSectorNumPredial() {
        return sectorNumPredial;
    }

    public void setSectorNumPredial(String sectorNumPredial) {
        if (sectorNumPredial != null && !sectorNumPredial.isEmpty()) {
            while (sectorNumPredial.length() < 2) {
                sectorNumPredial = "0" + sectorNumPredial;
            }
        }
        this.sectorNumPredial = sectorNumPredial;
    }

    public String getComunaNumPredial() {
        return comunaNumPredial;
    }

    public void setComunaNumPredial(String comunaNumPredial) {
        if (comunaNumPredial != null && !comunaNumPredial.isEmpty()) {
            while (comunaNumPredial.length() < 2) {
                comunaNumPredial = "0" + comunaNumPredial;
            }
        }
        this.comunaNumPredial = comunaNumPredial;
    }

    public String getBarrioNumPredial() {
        return barrioNumPredial;
    }

    public void setBarrioNumPredial(String barrioNumPredial) {
        if (barrioNumPredial != null && !barrioNumPredial.isEmpty()) {
            while (barrioNumPredial.length() < 2) {
                barrioNumPredial = "0" + barrioNumPredial;
            }
        }
        this.barrioNumPredial = barrioNumPredial;
    }

    public String getManzanaNumPredial() {
        return manzanaNumPredial;
    }

    public void setManzanaNumPredial(String manzanaNumPredial) {
        if (manzanaNumPredial != null && !manzanaNumPredial.isEmpty()) {
            while (manzanaNumPredial.length() < 4) {
                manzanaNumPredial = "0" + manzanaNumPredial;
            }
        }
        this.manzanaNumPredial = manzanaNumPredial;
    }

    public String getTerrenoNumPredial() {
        return terrenoNumPredial;
    }

    public void setTerrenoNumPredial(String terrenoNumPredial) {
        if (terrenoNumPredial != null && !terrenoNumPredial.isEmpty()) {
            while (terrenoNumPredial.length() < 4) {
                terrenoNumPredial = "0" + terrenoNumPredial;
            }
        }
        this.terrenoNumPredial = terrenoNumPredial;
    }

    public String getCondicionPropiedadNumPredial() {
        return condicionPropiedadNumPredial;
    }

    public void setCondicionPropiedadNumPredial(
        String condicionPropiedadNumPredial) {
        if (condicionPropiedadNumPredial != null && !condicionPropiedadNumPredial.isEmpty()) {
            while (condicionPropiedadNumPredial.length() < 1) {
                condicionPropiedadNumPredial = "0" + condicionPropiedadNumPredial;
            }
        }
        this.condicionPropiedadNumPredial = condicionPropiedadNumPredial;
    }

    public String getEdificioNumPredial() {
        return edificioNumPredial;
    }

    public void setEdificioNumPredial(String edificioNumPredial) {
        if (edificioNumPredial != null && !edificioNumPredial.isEmpty()) {
            while (edificioNumPredial.length() < 2) {
                edificioNumPredial = "0" + edificioNumPredial;
            }
        }
        this.edificioNumPredial = edificioNumPredial;
    }

    public String getPisoNumPredial() {
        return pisoNumPredial;
    }

    public void setPisoNumPredial(String pisoNumPredial) {
        if (pisoNumPredial != null && !pisoNumPredial.isEmpty()) {
            while (pisoNumPredial.length() < 2) {
                pisoNumPredial = "0" + pisoNumPredial;
            }
        }
        this.pisoNumPredial = pisoNumPredial;
    }

    public String getUnidadNumPredial() {
        return unidadNumPredial;
    }

    public void setUnidadNumPredial(String unidadNumPredial) {
        if (unidadNumPredial != null && !unidadNumPredial.isEmpty()) {
            while (unidadNumPredial.length() < 4) {
                unidadNumPredial = "0" + unidadNumPredial;
            }
        }
        this.unidadNumPredial = unidadNumPredial;
    }

    public List<String> getMunicipiosJurisdiccion() {
        return municipiosJurisdiccion;
    }

    public void setMunicipiosJurisdiccion(List<String> municipiosJurisdiccion) {
        this.municipiosJurisdiccion = municipiosJurisdiccion;
    }

    public boolean isEsVisibleMnpios() {
        return esVisibleMnpios;
    }

    public void setEsVisibleMnpios(boolean esVisibleMnpios) {
        this.esVisibleMnpios = esVisibleMnpios;
    }
}
