package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.generales.Documento;

/**
 * The persistent class for the SOLICITUD_DOCUMENTO database table.
 *
 * @modified rodrigo.hernandez 20-04-2012 Cambio de tipo de dato del id: de Long a Long Cambio de
 * tipo de dato del id: de BigDecimal a Long Generación de Serial ID
 *
 * @modified rodrigo.hernandez 13-11-2012 Adicion de atributo detalleDocumentoFaltantes Eliminacion
 * de atributo solicitudId Adicion de atributo solicitud Eliminacion de atributo soporteDocumentoId
 * Adicion de atributo soporteDocumento
 *
 *
 */
@Entity
@Table(name = "SOLICITUD_DOCUMENTO", schema = "SNC_TRAMITE")
public class SolicitudDocumento implements Serializable {

    /**
     * Serial ID generado
     */
    private static final long serialVersionUID = -3967011695335666878L;

    private Long id;
    private Long autorizacionDocumentoId;
    private Date fecha;
    private Date fechaDesfijacionEdicto;
    private Date fechaFijacionEdicto;
    private Date fechaLog;
    private Date fechaNotificacion;
    private String idPersonaAutorizacion;
    private String idPersonaNotificacion;
    private String idRepositorioDocumentos;
    private String metodoNotificacion;
    private Long notificacionDocumentoId;
    private String numeroDocumento;
    private String observacionNotificacion;
    private String personaAutrorizacion;
    private String personaNotificacion;
    private String renunciaRecursos;
    private Solicitud solicitud;
    private Documento soporteDocumento;
    private String tipoIdPersonaAutorizacion;
    private String tipoIdPersonaNotificacion;
    private String usuarioLog;
    private List<DetalleDocumentoFaltante> detalleDocumentoFaltantes =
        new ArrayList<DetalleDocumentoFaltante>();

    public SolicitudDocumento() {
    }

    @Id
    @SequenceGenerator(name = "SOLICITUD_DOCUMENTO_ID_GEN", sequenceName =
        "SOLICITUD_DOCUMENTO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SOLICITUD_DOCUMENTO_ID_GEN")
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AUTORIZACION_DOCUMENTO_ID")
    public Long getAutorizacionDocumentoId() {
        return this.autorizacionDocumentoId;
    }

    public void setAutorizacionDocumentoId(Long autorizacionDocumentoId) {
        this.autorizacionDocumentoId = autorizacionDocumentoId;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DESFIJACION_EDICTO")
    public Date getFechaDesfijacionEdicto() {
        return this.fechaDesfijacionEdicto;
    }

    public void setFechaDesfijacionEdicto(Date fechaDesfijacionEdicto) {
        this.fechaDesfijacionEdicto = fechaDesfijacionEdicto;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIJACION_EDICTO")
    public Date getFechaFijacionEdicto() {
        return this.fechaFijacionEdicto;
    }

    public void setFechaFijacionEdicto(Date fechaFijacionEdicto) {
        this.fechaFijacionEdicto = fechaFijacionEdicto;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_NOTIFICACION")
    public Date getFechaNotificacion() {
        return this.fechaNotificacion;
    }

    public void setFechaNotificacion(Date fechaNotificacion) {
        this.fechaNotificacion = fechaNotificacion;
    }

    @Column(name = "ID_PERSONA_AUTORIZACION")
    public String getIdPersonaAutorizacion() {
        return this.idPersonaAutorizacion;
    }

    public void setIdPersonaAutorizacion(String idPersonaAutorizacion) {
        this.idPersonaAutorizacion = idPersonaAutorizacion;
    }

    @Column(name = "ID_PERSONA_NOTIFICACION")
    public String getIdPersonaNotificacion() {
        return this.idPersonaNotificacion;
    }

    public void setIdPersonaNotificacion(String idPersonaNotificacion) {
        this.idPersonaNotificacion = idPersonaNotificacion;
    }

    @Column(name = "ID_REPOSITORIO_DOCUMENTOS")
    public String getIdRepositorioDocumentos() {
        return this.idRepositorioDocumentos;
    }

    public void setIdRepositorioDocumentos(String idRepositorioDocumentos) {
        this.idRepositorioDocumentos = idRepositorioDocumentos;
    }

    @Column(name = "METODO_NOTIFICACION")
    public String getMetodoNotificacion() {
        return this.metodoNotificacion;
    }

    public void setMetodoNotificacion(String metodoNotificacion) {
        this.metodoNotificacion = metodoNotificacion;
    }

    @Column(name = "NOTIFICACION_DOCUMENTO_ID")
    public Long getNotificacionDocumentoId() {
        return this.notificacionDocumentoId;
    }

    public void setNotificacionDocumentoId(Long notificacionDocumentoId) {
        this.notificacionDocumentoId = notificacionDocumentoId;
    }

    @Column(name = "NUMERO_DOCUMENTO")
    public String getNumeroDocumento() {
        return this.numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    @Column(name = "OBSERVACION_NOTIFICACION")
    public String getObservacionNotificacion() {
        return this.observacionNotificacion;
    }

    public void setObservacionNotificacion(String observacionNotificacion) {
        this.observacionNotificacion = observacionNotificacion;
    }

    @Column(name = "PERSONA_AUTORIZACION")
    public String getPersonaAutrorizacion() {
        return this.personaAutrorizacion;
    }

    public void setPersonaAutrorizacion(String personaAutrorizacion) {
        this.personaAutrorizacion = personaAutrorizacion;
    }

    @Column(name = "PERSONA_NOTIFICACION")
    public String getPersonaNotificacion() {
        return this.personaNotificacion;
    }

    public void setPersonaNotificacion(String personaNotificacion) {
        this.personaNotificacion = personaNotificacion;
    }

    @Column(name = "RENUNCIA_RECURSOS")
    public String getRenunciaRecursos() {
        return this.renunciaRecursos;
    }

    public void setRenunciaRecursos(String renunciaRecursos) {
        this.renunciaRecursos = renunciaRecursos;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_ID", nullable = false)
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = false)
    public Documento getSoporteDocumento() {
        return this.soporteDocumento;
    }

    public void setSoporteDocumento(Documento soporteDocumento) {
        this.soporteDocumento = soporteDocumento;
    }

    @Column(name = "TIPO_ID_PERSONA_AUTORIZACION")
    public String getTipoIdPersonaAutorizacion() {
        return this.tipoIdPersonaAutorizacion;
    }

    public void setTipoIdPersonaAutorizacion(String tipoIdPersonaAutorizacion) {
        this.tipoIdPersonaAutorizacion = tipoIdPersonaAutorizacion;
    }

    @Column(name = "TIPO_ID_PERSONA_NOTIFICACION")
    public String getTipoIdPersonaNotificacion() {
        return this.tipoIdPersonaNotificacion;
    }

    public void setTipoIdPersonaNotificacion(String tipoIdPersonaNotificacion) {
        this.tipoIdPersonaNotificacion = tipoIdPersonaNotificacion;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitudDocumento")
    public List<DetalleDocumentoFaltante> getDetalleDocumentoFaltantes() {
        return detalleDocumentoFaltantes;
    }

    public void setDetalleDocumentoFaltantes(
        List<DetalleDocumentoFaltante> detalleDocumentoFaltantes) {
        this.detalleDocumentoFaltantes = detalleDocumentoFaltantes;
    }

}
