package co.gov.igac.snc.util;

import java.io.Serializable;
import java.math.BigDecimal;

public class TotalManzanaDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4980018078523310430L;
    private Integer ano;
    private String zona;
    private BigDecimal cantidadManzanasVeredas;
    private BigDecimal cantidadPredios;
    private BigDecimal cantidadPropietarios;
    private BigDecimal areaTerreno;
    private BigDecimal areaConstruccion;
    private BigDecimal valorAvaluo;
    private BigDecimal prediosCondicion0;
    private BigDecimal prediosCondicion1;
    private BigDecimal prediosCondicion2;
    private BigDecimal prediosCondicion3;
    private BigDecimal prediosCondicion4;
    private BigDecimal prediosCondicion5;
    private BigDecimal prediosCondicion6;
    private BigDecimal prediosCondicion7;
    private BigDecimal prediosCondicion8;
    private BigDecimal prediosCondicion9;

    public TotalManzanaDTO(Integer ano, String zona,
        BigDecimal cantidadManzanasVeredas, BigDecimal cantidadPredios,
        BigDecimal cantidadPropietarios, BigDecimal areaTerreno,
        BigDecimal areaConstruccion, BigDecimal valorAvaluo,
        BigDecimal prediosCondicion0, BigDecimal prediosCondicion1,
        BigDecimal prediosCondicion2, BigDecimal prediosCondicion3,
        BigDecimal prediosCondicion4, BigDecimal prediosCondicion5,
        BigDecimal prediosCondicion6, BigDecimal prediosCondicion7,
        BigDecimal prediosCondicion8, BigDecimal prediosCondicion9) {
        super();
        this.ano = ano;
        this.zona = zona;
        this.cantidadManzanasVeredas = cantidadManzanasVeredas;
        this.cantidadPredios = cantidadPredios;
        this.cantidadPropietarios = cantidadPropietarios;
        this.areaTerreno = areaTerreno;
        this.areaConstruccion = areaConstruccion;
        this.valorAvaluo = valorAvaluo;
        this.prediosCondicion0 = prediosCondicion0;
        this.prediosCondicion1 = prediosCondicion1;
        this.prediosCondicion2 = prediosCondicion2;
        this.prediosCondicion3 = prediosCondicion3;
        this.prediosCondicion4 = prediosCondicion4;
        this.prediosCondicion5 = prediosCondicion5;
        this.prediosCondicion6 = prediosCondicion6;
        this.prediosCondicion7 = prediosCondicion7;
        this.prediosCondicion8 = prediosCondicion8;
        this.prediosCondicion9 = prediosCondicion9;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public BigDecimal getCantidadManzanasVeredas() {
        return cantidadManzanasVeredas;
    }

    public void setCantidadManzanasVeredas(BigDecimal cantidadManzanasVeredas) {
        this.cantidadManzanasVeredas = cantidadManzanasVeredas;
    }

    public BigDecimal getCantidadPredios() {
        return cantidadPredios;
    }

    public void setCantidadPredios(BigDecimal cantidadPredios) {
        this.cantidadPredios = cantidadPredios;
    }

    public BigDecimal getCantidadPropietarios() {
        return cantidadPropietarios;
    }

    public void setCantidadPropietarios(BigDecimal cantidadPropietarios) {
        this.cantidadPropietarios = cantidadPropietarios;
    }

    public BigDecimal getAreaTerreno() {
        return areaTerreno;
    }

    public void setAreaTerreno(BigDecimal areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    public BigDecimal getAreaConstruccion() {
        return areaConstruccion;
    }

    public void setAreaConstruccion(BigDecimal areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    public BigDecimal getValorAvaluo() {
        return valorAvaluo;
    }

    public void setValorAvaluo(BigDecimal valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

    public BigDecimal getPrediosCondicion0() {
        return prediosCondicion0;
    }

    public void setPrediosCondicion0(BigDecimal prediosCondicion0) {
        this.prediosCondicion0 = prediosCondicion0;
    }

    public BigDecimal getPrediosCondicion1() {
        return prediosCondicion1;
    }

    public void setPrediosCondicion1(BigDecimal prediosCondicion1) {
        this.prediosCondicion1 = prediosCondicion1;
    }

    public BigDecimal getPrediosCondicion2() {
        return prediosCondicion2;
    }

    public void setPrediosCondicion2(BigDecimal prediosCondicion2) {
        this.prediosCondicion2 = prediosCondicion2;
    }

    public BigDecimal getPrediosCondicion3() {
        return prediosCondicion3;
    }

    public void setPrediosCondicion3(BigDecimal prediosCondicion3) {
        this.prediosCondicion3 = prediosCondicion3;
    }

    public BigDecimal getPrediosCondicion4() {
        return prediosCondicion4;
    }

    public void setPrediosCondicion4(BigDecimal prediosCondicion4) {
        this.prediosCondicion4 = prediosCondicion4;
    }

    public BigDecimal getPrediosCondicion5() {
        return prediosCondicion5;
    }

    public void setPrediosCondicion5(BigDecimal prediosCondicion5) {
        this.prediosCondicion5 = prediosCondicion5;
    }

    public BigDecimal getPrediosCondicion6() {
        return prediosCondicion6;
    }

    public void setPrediosCondicion6(BigDecimal prediosCondicion6) {
        this.prediosCondicion6 = prediosCondicion6;
    }

    public BigDecimal getPrediosCondicion7() {
        return prediosCondicion7;
    }

    public void setPrediosCondicion7(BigDecimal prediosCondicion7) {
        this.prediosCondicion7 = prediosCondicion7;
    }

    public BigDecimal getPrediosCondicion8() {
        return prediosCondicion8;
    }

    public void setPrediosCondicion8(BigDecimal prediosCondicion8) {
        this.prediosCondicion8 = prediosCondicion8;
    }

    public BigDecimal getPrediosCondicion9() {
        return prediosCondicion9;
    }

    public void setPrediosCondicion9(BigDecimal prediosCondicion9) {
        this.prediosCondicion9 = prediosCondicion9;
    }

    public String getZonaCompleta() {
        if (zona != null) {
            if (zona.equals("R")) {
                return "Rural";
            }
            if (zona.equals("U")) {
                return "Urbano";
            }
            if (zona.equals("C")) {
                return "Corregimiento";
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

}
