package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * MODIFICACIONES PROPIAS A LA CLASE PFichaMatriz:
 *
 * @modified fabio.navarrete -> Se agregó serialVersionUID -> Se agregó el método y atributo
 * transient para consultar el valor de la propiedad cancelaInscribe -> Se agrega método transient
 * getTotalTorres
 *
 * @modified david.cifuentes :: Adición de la variable totalUnidadesSotanos :: 19/07/12
 * david.cifuentes :: Implementación de la interfaz Cloneable :: 19/05/15 david.cifuentes :: Adición
 * del campo totalUnidadesNuevas :: 12/06/2015
 */
/**
 * The persistent class for the P_FICHA_MATRIZ database table.
 */
@Entity
@Table(name = "P_FICHA_MATRIZ")
public class PFichaMatriz implements Serializable, IProyeccionObject, Cloneable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Double areaTotalConstruidaComun;
    private Double areaTotalConstruidaPrivada;
    private Double areaTotalTerrenoComun;
    private Double areaTotalTerrenoPrivada;
    private String cancelaInscribe;
    private Date fechaLog;
    private Long totalUnidadesPrivadas;
    private String usuarioLog;
    private Double valorTotalAvaluoCatastral;
    private List<PFichaMatrizModelo> PFichaMatrizModelos;
    private List<PFichaMatrizPredio> PFichaMatrizPredios;
    private List<PFichaMatrizTorre> PFichaMatrizTorres;
    private List<PFichaMatrizPredioTerreno> pFichaMatrizPredioTerrenos;
    private PPredio PPredio;
    private String cancelaInscribeValor;
    private Long totalUnidadesSotanos;
    private Long totalUnidadesNuevas;
    private Long totalUnidadesPh;
    private Long totalUnidadesCondominio;
    private String estado;

    public PFichaMatriz() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FICHA_MATRIZ_ID_SEQ")
    @SequenceGenerator(name = "FICHA_MATRIZ_ID_SEQ", sequenceName = "FICHA_MATRIZ_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA_TOTAL_CONSTRUIDA_COMUN")
    public Double getAreaTotalConstruidaComun() {
        return this.areaTotalConstruidaComun;
    }

    public void setAreaTotalConstruidaComun(Double areaTotalConstruidaComun) {
        this.areaTotalConstruidaComun = areaTotalConstruidaComun;
    }

    @Column(name = "AREA_TOTAL_CONSTRUIDA_PRIVADA")
    public Double getAreaTotalConstruidaPrivada() {
        return this.areaTotalConstruidaPrivada;
    }

    public void setAreaTotalConstruidaPrivada(
        Double areaTotalConstruidaPrivada) {
        this.areaTotalConstruidaPrivada = areaTotalConstruidaPrivada;
    }

    @Column(name = "AREA_TOTAL_TERRENO_COMUN", precision = 12, scale = 2)
    public Double getAreaTotalTerrenoComun() {
        return this.areaTotalTerrenoComun;
    }

    public void setAreaTotalTerrenoComun(Double areaTotalTerrenoComun) {
        this.areaTotalTerrenoComun = areaTotalTerrenoComun;
    }

    @Column(name = "AREA_TOTAL_TERRENO_PRIVADA", precision = 12, scale = 2)
    public Double getAreaTotalTerrenoPrivada() {
        return this.areaTotalTerrenoPrivada;
    }

    public void setAreaTotalTerrenoPrivada(Double areaTotalTerrenoPrivada) {
        this.areaTotalTerrenoPrivada = areaTotalTerrenoPrivada;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "TOTAL_UNIDADES_PRIVADAS")
    public Long getTotalUnidadesPrivadas() {
        return this.totalUnidadesPrivadas;
    }

    public void setTotalUnidadesPrivadas(Long totalUnidadesPrivadas) {
        this.totalUnidadesPrivadas = totalUnidadesPrivadas;
    }

    @Column(name = "TOTAL_UNIDADES_SOTANOS", nullable = false, precision = 6)
    public Long getTotalUnidadesSotanos() {
        return this.totalUnidadesSotanos;
    }

    public void setTotalUnidadesSotanos(Long totalUnidadesSotanos) {
        this.totalUnidadesSotanos = totalUnidadesSotanos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_CATASTRAL")
    public Double getValorTotalAvaluoCatastral() {
        return this.valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(
        Double valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    // bi-directional many-to-one association to PFichaMatrizModelo
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PFichaMatriz")
    public List<PFichaMatrizModelo> getPFichaMatrizModelos() {
        return this.PFichaMatrizModelos;
    }

    public void setPFichaMatrizModelos(
        List<PFichaMatrizModelo> PFichaMatrizModelos) {
        this.PFichaMatrizModelos = PFichaMatrizModelos;
    }

    // bi-directional many-to-one association to PFichaMatrizPredio
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PFichaMatriz")
    public List<PFichaMatrizPredio> getPFichaMatrizPredios() {
        return this.PFichaMatrizPredios;
    }

    public void setPFichaMatrizPredios(
        List<PFichaMatrizPredio> PFichaMatrizPredios) {
        this.PFichaMatrizPredios = PFichaMatrizPredios;
    }

    // bi-directional many-to-one association to PFichaMatrizTorre
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "PFichaMatriz")
    public List<PFichaMatrizTorre> getPFichaMatrizTorres() {
        return this.PFichaMatrizTorres;
    }

    public void setPFichaMatrizTorres(List<PFichaMatrizTorre> PFichaMatrizTorres) {
        this.PFichaMatrizTorres = PFichaMatrizTorres;
    }

    // bi-directional many-to-one association to PFichaMatrizTorre
    @OneToMany(mappedBy = "fichaMatriz")
    public List<PFichaMatrizPredioTerreno> getPFichaMatrizPredioTerrenos() {
        return this.pFichaMatrizPredioTerrenos;
    }

    public void setPFichaMatrizPredioTerrenos(
        List<PFichaMatrizPredioTerreno> PFichaMatrizPredioTerrenos) {
        this.pFichaMatrizPredioTerrenos = PFichaMatrizPredioTerrenos;
    }

    // bi-directional many-to-one association to PPredio
    @ManyToOne
    @JoinColumn(name = "PREDIO_ID")
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    @Column(name = "TOTAL_UNIDADES_NUEVAS")
    public Long getTotalUnidadesNuevas() {
        return this.totalUnidadesNuevas;
    }

    public void setTotalUnidadesNuevas(Long totalUnidadesNuevas) {
        this.totalUnidadesNuevas = totalUnidadesNuevas;
    }

    @Column(name = "TOTAL_UNIDADES_PH")
    public Long getTotalUnidadesPh() {
        return this.totalUnidadesPh;
    }

    public void setTotalUnidadesPh(Long totalUnidadesPh) {
        this.totalUnidadesPh = totalUnidadesPh;
    }

    @Column(name = "TOTAL_UNIDADES_CONDOM")
    public Long getTotalUnidadesCondominio() {
        return this.totalUnidadesCondominio;
    }

    public void setTotalUnidadesCondominio(Long totalUnidadesCondominio) {
        this.totalUnidadesCondominio = totalUnidadesCondominio;
    }

    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    @Transient
    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

    @Transient
    public Integer getTotalTorres() {
        if (this.PFichaMatrizTorres != null) {
            return this.PFichaMatrizTorres.size();
        }
        return 0;
    }

    /**
     * Método que me retorna el área total de terreno
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public Double getAreaTotalTerreno() {
        if (this.areaTotalTerrenoComun != null && this.areaTotalTerrenoPrivada != null) {
            return Double.valueOf(this.areaTotalTerrenoComun + this.areaTotalTerrenoPrivada);
        }
        return Double.valueOf(0);
    }

    /**
     * Método que me retorna el área total de construida
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public Double getAreaTotalConstruida() {
        if (this.areaTotalConstruidaComun != null && this.areaTotalConstruidaPrivada != null) {
            return Double.valueOf(this.areaTotalConstruidaPrivada + this.areaTotalConstruidaComun);
        }
        return Double.valueOf(0);
    }

    // ------------------------------------------//	
    /**
     * Método que realiza el clone de una {@link PFichaMatriz}.
     *
     * @author david.cifuentes
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
