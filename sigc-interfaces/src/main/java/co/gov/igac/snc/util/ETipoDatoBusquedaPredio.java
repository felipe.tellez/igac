package co.gov.igac.snc.util;

/**
 * Contiene los posibles tipos de datos por los que se puede hacer una búsqueda de predios
 *
 * @author pedro.garcia
 *
 */
public enum ETipoDatoBusquedaPredio {

    AREA_TERRENO,
    CONSECUTIVO_CATASTRAL,
    DESTINO,
    DIRECCION,
    IDENTIFICACION_PROPIETARIO,
    NIP,
    NOMBRE_PROPIETARIO,
    NUMERO_PREDIAL_ANTERIOR,
    NUMERO_PREDIAL

}
