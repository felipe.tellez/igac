package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los asuntos de los correos electrónicos en el proceso de actualización.
 *
 * @author david.cifuentes
 */
public enum EAsuntoCorreoElectronicoActualizacion {

    ASUNTO_SOLICITUD_DIGITALIZACION_NOMENCLATURA_VIAL(
        "Solicitud digitalización proyecto nomenclatura vial municipio de {0}"),
    ASUNTO_SOLICITUD_DIGITALIZACION_PERIMETRO_URBANO(
        "digitalización proyecto perímetro urbano municipio de {0}"),
    ASUNTO_SOLICITUD_ORDEN_AJUSTES_NOMENCLATURA_VIAL(
        "Ajustes a plano de nomenclatura vial para fines catastrales"),
    ASUNTO_SOLICITUD_ORDEN_AJUSTES_PERIMETRO_URBANO(
        "Ajustes a plano de perímetro urbano con fines catastrales"),
    ASUNTO_SOLICITUD_ORDEN_AJUSTE(
        "Ajustes a manzanas e identificación de areas"),
    ASUNTO_AJUSTES_CONTROL_CALIDAD_LEVATOPOGRAFICO(
        "Ajustes de Control de Calidad Levantamiento Topográfico.");

    private String asunto;

    private EAsuntoCorreoElectronicoActualizacion(String asunto) {
        this.asunto = asunto;
    }

    public String getAsunto() {
        return asunto;
    }

}
