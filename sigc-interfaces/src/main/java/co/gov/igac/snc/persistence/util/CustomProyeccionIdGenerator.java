package co.gov.igac.snc.persistence.util;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.SequenceGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Esta clase se usa como generador de secuencia para el id de las clases entity de las tablas de
 * proyección (P_*), para poder usar el mismo id del objeto correspondiente no proyectado como id
 * del objeto proyectado (cuando ese id no es null)
 *
 * @author Fabio
 */
/*
 * @documented pedro.garcia
 */
public class CustomProyeccionIdGenerator extends SequenceGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomProyeccionIdGenerator.class);

    @Override
    public Serializable generate(SessionImplementor session, Object obj)
        throws HibernateException {

        IProyeccionObject myObject = (IProyeccionObject) obj;

        if (myObject.getId() != null && myObject.getId() > 0) {
            return myObject.getId();
        } else {
            return super.generate(session, obj);
        }

    }
}
