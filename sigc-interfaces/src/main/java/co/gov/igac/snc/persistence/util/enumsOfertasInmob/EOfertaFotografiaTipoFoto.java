/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util.enumsOfertasInmob;

/**
 * Enumeración con los posibles valores del dominio de OFERTA_FOTOGRAFIA_TIPO_FOTO
 *
 * @author juan.agudelo
 */
public enum EOfertaFotografiaTipoFoto {

    BANIO("BANIO"),
    COCINA("COCINA"),
    FACHADA("FACHADA"),
    FOLLETO("FOLLETO"),
    OTRO("OTRO"),
    SALA("SALA");

    private String codigo;

    private EOfertaFotografiaTipoFoto(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
