package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;

/**
 * The persistent class for the DETALLE_DOCUMENTO_FALTANTE database table.
 *
 * @modified rodrigo.hernandez - Se elimina el atributo tipoDocumentoId Se agrega el atributo
 * tipoDocumento
 *
 * @modified rodrigo.hernandez - 13-11-2012 - Se elimina el atributo solicitudDocumentoId Se agrega
 * el atributo solicitudDocumento
 *
 */
@Entity
@Table(name = "DETALLE_DOCUMENTO_FALTANTE")
public class DetalleDocumentoFaltante implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7925654970754374817L;

    private Long id;
    private Long cantidadEntrega;
    private Long cantidadSolicitud;
    private Date fechaLog;
    private String observacionesEntrega;
    private String observacionesSolicitud;
    private SolicitudDocumento solicitudDocumento;
    private TipoDocumento tipoDocumento;
    private String usuarioLog;

    public DetalleDocumentoFaltante() {
    }

    @Id
    @SequenceGenerator(name = "DETALLE_DOCUMENTO_FALTANTE_ID_GENERATOR", sequenceName =
        "DETALLE_DOCUMENTO_FALTA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "DETALLE_DOCUMENTO_FALTANTE_ID_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CANTIDAD_ENTREGA")
    public Long getCantidadEntrega() {
        return this.cantidadEntrega;
    }

    public void setCantidadEntrega(Long cantidadEntrega) {
        this.cantidadEntrega = cantidadEntrega;
    }

    @Column(name = "CANTIDAD_SOLICITUD")
    public Long getCantidadSolicitud() {
        return this.cantidadSolicitud;
    }

    public void setCantidadSolicitud(Long cantidadSolicitud) {
        this.cantidadSolicitud = cantidadSolicitud;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "OBSERVACIONES_ENTREGA")
    public String getObservacionesEntrega() {
        return this.observacionesEntrega;
    }

    public void setObservacionesEntrega(String observacionesEntrega) {
        this.observacionesEntrega = observacionesEntrega;
    }

    @Column(name = "OBSERVACIONES_SOLICITUD")
    public String getObservacionesSolicitud() {
        return this.observacionesSolicitud;
    }

    public void setObservacionesSolicitud(String observacionesSolicitud) {
        this.observacionesSolicitud = observacionesSolicitud;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_DOCUMENTO_ID", nullable = false)
    public SolicitudDocumento getSolicitudDocumento() {
        return this.solicitudDocumento;
    }

    public void setSolicitudDocumento(SolicitudDocumento solicitudDocumento) {
        this.solicitudDocumento = solicitudDocumento;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_DOCUMENTO_ID")
    public TipoDocumento getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
