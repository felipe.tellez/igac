package co.gov.igac.snc.persistence.util;

/**
 * @author juan.agudelo
 */
public enum ETipoTramiteCaducidad {
    SOLICITUD_DE_REVISION_DE_AVALUO("2"),
    SOLICITUD_DE_RECURSO_DE_REPOSICION("3"),
    SOLICITUD_DE_RECURSO_DE_REPOSICION_Y_EN_SUBSIDIO_EL_DE_APELACION("4"),
    SOLICITUD_DE_RECURSO_DE_APELACION("5"),
    SOLICITUD_DE_AUTOAVALUO("7"),
    SOLICITUD_DE_DERECHO_DE_PETICION("15");

    private String codigo;

    private ETipoTramiteCaducidad(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
