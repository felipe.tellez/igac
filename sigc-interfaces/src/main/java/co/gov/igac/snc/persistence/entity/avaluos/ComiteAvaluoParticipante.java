package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * The persistent class for the COMITE_AVALUO_PARTICIPANTE database table.
 *
 * @modified christian.rodriguez agregado métodos transient isAsisteBoolean / setAsisteBoolean
 */
@Entity
@Table(name = "COMITE_AVALUO_PARTICIPANTE")
public class ComiteAvaluoParticipante implements Serializable {

    private static final long serialVersionUID = 7975136265257511534L;

    private Long id;
    private String actividad;
    private String asiste;
    private Date fechaLog;
    private String nombre;
    private ProfesionalAvaluo profesionalAvaluos;
    private String usuarioLog;
    private ComiteAvaluo comiteAvaluo;

    public ComiteAvaluoParticipante() {
        this.asiste = ESiNo.NO.getCodigo();
    }

    @Id
    @SequenceGenerator(name = "COMITE_AVALUO_PARTICIPANTE_ID_GENERATOR", sequenceName =
        "COMITE_AVALUO_PARTICIPA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "COMITE_AVALUO_PARTICIPANTE_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActividad() {
        return this.actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getAsiste() {
        return this.asiste;
    }

    public void setAsiste(String asiste) {
        this.asiste = asiste;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESIONAL_AVALUOS_ID")
    public ProfesionalAvaluo getProfesionalAvaluos() {
        return this.profesionalAvaluos;
    }

    public void setProfesionalAvaluos(ProfesionalAvaluo profesionalAvaluos) {
        this.profesionalAvaluos = profesionalAvaluos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to ComiteAvaluo
    @ManyToOne
    @JoinColumn(name = "COMITE_AVALUO_ID")
    public ComiteAvaluo getComiteAvaluo() {
        return this.comiteAvaluo;
    }

    public void setComiteAvaluo(ComiteAvaluo comiteAvaluo) {
        this.comiteAvaluo = comiteAvaluo;
    }

    // -----------------------------------------------------------------------------------
    /**
     * Método utilizado para acceder al campo {@link #asiste} como si fuera un boolean, se creo con
     * el fin de poder editar este campo desde una tabla
     *
     * @author christian.rodriguez
     * @return true si el campo {@link #asiste} es SI, false en otro caso
     */
    @Transient
    public boolean isAsisteBoolean() {
        if (this.asiste != null && this.asiste.equals(ESiNo.SI.getCodigo())) {
            return true;
        }
        return false;
    }

    // -----------------------------------------------------------------------------------
    /**
     * Método utilizado para asignar al campo {@link #asiste} como si fuera un boolean, se creo con
     * el fin de poder editar este campo desde una tabla
     *
     * @author christian.rodriguez
     */
    @Transient
    public void setAsisteBoolean(boolean reservadoBoolean) {
        if (reservadoBoolean) {
            this.asiste = ESiNo.SI.getCodigo();
        } else {
            this.asiste = ESiNo.NO.getCodigo();
        }
    }

}
