package co.gov.igac.snc.persistence.entity.generales;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.dao.sig.vo.ProductoCatastralResultado;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;

/**
 * Entidad utilizada para realizar la programación de la ejecución de los jobs para los productos
 * catastrales y reconciliación de los datos geográficos.
 *
 * @modified pedro.garcia 24-06-2013 cambio de anotaciones a los getter 14-09-2013 logger, métodos
 * transient getLoginUsuarioDeCodigo, getIdTramiteDeCodigo
 *
 * @modified juan.mendez 23-09-2013 Eliminación de atributos que ya no se utilizan
 *
 * @modified andres.eslava 15-07-2015 se agregan constantes para tipos dejob asincronicos
 * inconsistencias y zonas.
 * @modified javier.aponte 20-05-2015 Se elimina del método prePersist que se ponga el estado en
 * 'AGS_ESPERANDO' para poder usar esta tabla para jobs que no van a ArcGIS, como por ejemplo los
 * jobs que tienen que ir a JasperServer.
 * @author juan.mendez
 */
@Entity
@XmlRootElement
@Table(name = "PRODUCTO_CATASTRAL_JOB")
public class ProductoCatastralJob implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 390133258395793334L;

    private static final org.slf4j.Logger LOGGER =
        LoggerFactory.getLogger(ProductoCatastralJob.class);

    /**
     * Tipos de productos Catastrales
     */
    //public static final String TIPO_FICHA_PREDIAL_DIGITAL 		= "FPD";
    public static final String TIPO_COLINDANCIA = "CDC";
    //public static final String TIPO_CARTA_CATASTRAL_URBANA 		= "CCU";
    public static final String TIPO_MANZANA_FICHA_PREDIAL = "MFP";
    public static final String TIPO_PLANO_PREDIAL_RURAL = "PPR";
    public static final String TIPO_OFERTA_INMOBILIARIA_URBANO = "OIU";
    public static final String TIPO_OFERTA_INMOBILIARIA_RURAL = "OIR";
    //public static final String TIPO_CERTIFICADO_PLANO_PREDIAL 	= "CPC";
    public static final String TIPO_CERTIFICADO_DIGITAL_MANZANA = "CPCM";
    //public static final String TIPO_FICHA_PREDIAL_DIGITAL_PRODUCTOS = "FPD_PRODUCTOS";
    //public static final String TIPO_CARTA_CATASTRAL_URBANA_PRODUCTOS = "CCU_PRODUCTOS";

    /**
     * Tipos de jobs SIG asincrónicos única instancia
     */
    public static final String SIG_JOB_GENERAR_DATOS_EDICION = "SIG_JOB_GENERAR_DATOS_EDICION";
    public static final String SIG_JOB_FINALIZAR_EDICION = "SIG_JOB_FINALIZAR_EDICION";
    //public static final String SIG_JOB_CARGA_DATOS_HISTORICOS = "SIG_JOB_CARGA_DATOS_HISTORICOS";
    public static final String SIG_JOB_EXPORTAR_DATOS = "SIG_JOB_EXPORTAR_DATOS";
    public static final String SIG_JOB_ELIMINAR_VERSION_SDE = "SIG_JOB_ELIMINAR_VERSION_SDE";
    public static final String SIG_JOB_GENERAR_PRODUCTOS_DATOS = "SIG_JOB_GENERAR_PRODUCTO_DATO";
    public static final String SIG_JOB_EXPORTAR_IMAGEN = "SIG_JOB_EXPORTAR_IMAGEN";
    public static final String SIG_JOB_VALIDAR_INCONSISTENCIAS = "SIG_JOB_VALIDAR_GEOMETRIA";
    public static final String SIG_JOB_OBTENER_ZONAS = "SIG_JOB_OBTENER_ZONAS";
    public static final String SIG_JOB_ACTUALIZAR_COLINDANTES = "SIG_JOB_ACTUALIZAR_COLINDANTES";
    public static final String SIG_JOB_CARGUE_ACTUALIZACION = "SIG_JOB_CARGUE_ACTUALIZACION";

    /**
     * Tipos de jobs Resoluciones Jasper
     */
    public static final String TIPO_RESOLUCIONES_CONSERVACION = "RES_CONSERVACION";
    public static final String TIPO_RESOLUCIONES_ACTUALIZACION = "RES_ACTUALIZACION";

    /**
     * Tipos de jobs de reportes que se generan cuando se cargan las tablas en el proceso de
     * actualizacion express
     *
     * @author javier.aponte
     */
    public static final String TIPO_REPORTES_ACTUALIZACION = "REPORTES_ACTUALIZACION";

    /**
     * Tipos de Job para Generación de imágenes (Cartografía de "calidad producción" con Arcgis
     * Server 10.3.1 )
     */
    public static final String SIG_JOB_IMAGEN_CARTA_CATASTRAL_URBANA = "SIG_JOB_IMG_CCU";
    public static final String SIG_JOB_IMAGEN_FICHA_PREDIAL_DIGITAL = "SIG_JOB_IMG_FPD";
    public static final String SIG_JOB_IMAGEN_CERTIFICADO_PLANO_PREDIAL = "SIG_JOB_IMG_CPP";
    public static final String SIG_JOB_IMAGEN_PREDIAL	= "SIG_JOB_IMG_PREDIAL";

    /**
     * Formatos para exportación de datos
     */
    public static String FORMATO_DATOS_EXPORTADOS_SHAPE = "SHP";
    public static String FORMATO_DATOS_EXPORTADOS_FILEGEODB = "FILEGEODB";
    public static String FORMATO_DATOS_EXPORTADOS_CAD = "CAD";

    /**
     * Listado de productos de datos a Generar
     */
    public static String PRODUCTO_DATOS_ZONA_URBANA_MUNICIPIO = "1216";
    public static String PRODUCTO_DATOS_MANZANAS_CATASTRALES_MENOS_DE_20 = "1278";
    public static String PRODUCTO_DATOS_MANZANAS_CATASTRALES_MAS_DE_20 = "1279";

    /**
     * Estados de ejecución de los jobs en ArcGis Server
     */
    public static final String AGS_ESPERANDO = "AGS_ESPERANDO";
    public static final String AGS_EN_EJECUCION = "AGS_EN_EJECUCION";
    public static final String AGS_ERROR = "AGS_ERROR";
    public static final String AGS_TERMINADO = "AGS_TERMINADO";
    public static final String AGS_ERROR_CANCELADO = "AGS_ERROR_CANCELADO";
        public static final String AGS_GEN_IMG 	= "AGS_GEN_IMG";
        public static final String AGS_ERROR_GEN_IMG 	= "AGS_ERROR_GEN_IMG";

    /**
     * Estados de ejecución de los jobs en SNC
     */
    public static final String SNC_EN_EJECUCION = "SNC_EN_EJECUCION";
    public static final String SNC_ERROR = "SNC_ERROR";
    public static final String SNC_TERMINADO = "SNC_TERMINADO";

    /**
     * Estados de ejecución de los jobs en JasperServer
     */
    public static final String JPS_ESPERANDO = "JPS_ESPERANDO";
    public static final String JPS_EN_EJECUCION = "JPS_EN_EJECUCION";
    public static final String JPS_ERROR = "JPS_ERROR";
    public static final String JPS_TERMINADO = "JPS_TERMINADO";

    /**
     * Nombres de los parámetros almacenados en el xml del job
     */
    public static final String PARAMETRO_CODIGO_PREDIO = "codigoPredio";
    public static final String PARAMETRO_CODIGO_MANZANA = "codigoManzana";

    /**
     * Tipos de imágenes generadas por el servicio SIG para productos
     */
    public static final String TIPO_IMAGEN_FPD_PRINCIPAL = "Imagen_FPD";
    public static final String TIPO_IMAGEN_FPD_MANZANA = "Imagen_FPD_Manzana";
    public static final String TIPO_IMAGEN_CPP_PRINCIPAL = "Imagen_CPP";
    public static final String TIPO_IMAGEN_CPP_MANZANA = "Imagen_CPP_Manzana";
    public static final String TIPO_IMAGEN_CIP              =  "Imagen_ConsultaPredio";
    public static final String TIPO_IMAGEN_CIP_DET          =  "Imagen_ConsultaPredioDetalle";

    /**
     * Tipos de Colindancia
     */
    public static final String TIPO_COLINDANCIA_PREDIO = "COLINDANCIA_PREDIO";
    public static final String TIPO_COLINDANCIA_PREDIOS_MANZANA = "COLINDANCIA_PREDIOS_MANZANA";
    public static final String TIPO_COLINDANCIA_CONSTRUCCIONES = "COLINDANCIA_CONSTRUCCIONES";
    public static final String TIPO_COLINDANCIA_ACTUALIZAR = "COLINDANCIA_ACTUALIZAR";

    //-----    campos de la tabla --------
    private Long id;

    private String tipoProducto;

    private String usuarioLog;

    private String codigo;

    private String estado;

    private String resultado;

    private Date fechaLog;

    private Date fechaActualizadoLog;

    private Long tramiteId;

    private Integer numeroEnvio;

    private Long productoCatastralDetalleId;

    public ProductoCatastralJob() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCTO_CATASTRAL_JOB_ID_SEQ")
    @SequenceGenerator(name = "PRODUCTO_CATASTRAL_JOB_ID_SEQ", sequenceName =
        "PRODUCTO_CATASTRAL_JOB_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(final Long idToSet) {
        this.id = idToSet;
    }

    @Column(name = "RESULTADO")
    public String getResultado() {
        return this.resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_LOG", nullable = true, length = 7)
    public Date getFechaActualizadoLog() {
        return this.fechaActualizadoLog;
    }

    public void setFechaActualizadoLog(Date fechaActualizadoLog) {
        this.fechaActualizadoLog = fechaActualizadoLog;
    }

    @Column(name = "ESTADO", nullable = false, length = 16)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "CODIGO", nullable = false)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "TIPO_PRODUCTO", nullable = false, length = 16)
    public String getTipoProducto() {
        return this.tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    @Column(name = "TRAMITE_ID", nullable = true, precision = 10, scale = 0)
    public Long getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    @Column(name = "PRODUCTO_CATASTRAL_DETALLE_ID", nullable = true, precision = 10, scale = 0)
    public Long getProductoCatastralDetalleId() {
        return productoCatastralDetalleId;
    }

    public void setProductoCatastralDetalleId(Long productoCatastralDetalleId) {
        this.productoCatastralDetalleId = productoCatastralDetalleId;
    }

    /**
     *
     * @param tempDir
     * @return
     * @throws IOException
     */
    public String generateFile(File tempDir) throws IOException {
        String filename = this.tipoProducto + "_" + this.id + ".txt";
        String codigos[] = this.codigo.trim().split(",");
        File tempFile = new File(tempDir, filename);

        BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
            tempFile),
            Charset.forName("UTF-8")));
        for (int i = 0; i < codigos.length; i++) {
            String codigoT = codigos[i].trim();
            if (!"".equals(codigoT)) {
                output.write(codigoT);
                output.write("\r\n");
            }
        }
        output.close();
        return tempFile.getAbsolutePath();
    }

    /**
     * Retorna si un job está ejecutándose
     *
     * @return
     */
    public boolean jobRunning() {
        boolean running = this.estado.equals(AGS_EN_EJECUCION) ||
            this.estado.equals(AGS_ESPERANDO);
        return running;
    }

    @SuppressWarnings("unused")
    @PrePersist
    private void prePersist() {
        this.fechaLog = new Date();
        this.fechaActualizadoLog = new Date();
    }

    @SuppressWarnings("unused")
    @PreUpdate
    private void preUpdate() {
        this.fechaActualizadoLog = new Date();
    }

    /**
     *
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     *
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     *
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna el login de usuario que va como parámetro en las entradas de la tabla que son de
     * sigjob; es decir, donde el campo código es un xml que tiene como root <SigJob>
     *
     * PRE: el campo código, cuando es para lo de sig tiene la siguiente estructura:
     *
     * <SigJob>
     * <parameters>
     * <parameter>
     * <name>numeroTramite</name>
     * <value>516</value></parameter>
     * <parameter><name>identificadoresPredios</name><value>520010101000000060004000000000</value></parameter>
     * <parameter><name>tipoMutacion</name><value>2</value></parameter>
     * <parameter><name>usuario</name><value>pruatlantico16</value></parameter>
     * </parameters>
     * </SigJob>
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public String getLoginUsuarioDeCodigo() {

        String answer = "";
        Document document;

        if (!this.codigo.contains("SigJob")) {
            return answer;
        }
        try {
            document = DocumentHelper.parseText(this.codigo);
            answer = document.selectSingleNode("//parameters//parameter[name='usuario']//value").
                getText();
        } catch (DocumentException ex) {
            LOGGER.error(
                "Error en ProductoCatastralJob#getLoginUsuarioDeCodigo tratando de armar " +
                "un Document: " + ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("Error en ProductoCatastralJob#getLoginUsuarioDeCodigo tratando de " +
                "obtener el valor de un nodo" + ex.getMessage());
        }

        return answer;
    }
//-----------------------------------------------------    

    /**
     * Retorna el id del trámite que va como parámetro en las entradas de la tabla que son de
     * sigjob; es decir, donde el campo código es un xml que tiene como root <SigJob>
     *
     * PRE: el campo código, cuando es para lo de sig tiene la estructura descrita en el método 
     * {@link #getLoginUsuarioDeCodigo() }
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public Long getIdTramiteUsuarioDeCodigo() {

        Long answer = 0l;
        String valor;
        Document document;

        if (!this.codigo.contains("SigJob")) {
            return answer;
        }

        try {
            document = DocumentHelper.parseText(this.codigo);
            valor = document.
                selectSingleNode("//parameters//parameter[name='numeroTramite']//value").getText();
            if (valor != null) {
                answer = new Long(valor);
            }
        } catch (DocumentException ex) {
            LOGGER.error(
                "Error en ProductoCatastralJob#getLoginUsuarioDeCodigo tratando de armar " +
                "un Document: " + ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("Error en ProductoCatastralJob#getLoginUsuarioDeCodigo tratando de " +
                "obtener el valor de un nodo" + ex.getMessage());
        }

        return answer;
    }

    /**
     * Retorna el nombre de la actividad de la cual proviene para jobs de finalizar edición
     *
     * PRE: en el campo código, el XML debe contener el parametro actividadProcess
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public String getNombreActividadDeCodigo() {

        String valor = null;
        Document document;

        if (!this.codigo.contains("SigJob") || !this.codigo.contains("actividadProceso")) {
            return valor;
        }

        try {
            document = DocumentHelper.parseText(this.codigo);
            valor = document.selectSingleNode(
                "//parameters//parameter[name='actividadProceso']//value").getText();
        } catch (DocumentException ex) {
            LOGGER.error(
                "Error en ProductoCatastralJob#getNombreActividadDeCodigo tratando de armar " +
                "un Document: " + ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("Error en ProductoCatastralJob#getNombreActividadDeCodigo tratando de " +
                "obtener el valor de un nodo" + ex.getMessage());
        }

        return valor;
    }

    /**
     * Retorna el taskId de la tarea asociada al tramite asociado al job
     *
     * PRE: en el campo código, el XML debe contener el parametro taskIdProcess
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public String getTaskIdProcesoDeCodigo() {

        String valor = null;
        Document document;

        if (!this.codigo.contains("SigJob") || !this.codigo.contains("taskIdProceso")) {
            return valor;
        }

        try {
            document = DocumentHelper.parseText(this.codigo);
            valor = document.
                selectSingleNode("//parameters//parameter[name='taskIdProceso']//value").getText();
        } catch (DocumentException ex) {
            LOGGER.error(
                "Error en ProductoCatastralJob#getNombreActividadDeCodigo tratando de armar " +
                "un Document: " + ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("Error en ProductoCatastralJob#getNombreActividadDeCodigo tratando de " +
                "obtener el valor de un nodo" + ex.getMessage());
        }

        return valor;
    }

    @Column(name = "NUMERO_ENVIO", nullable = true, length = 1024)
    public Integer getNumeroEnvio() {
        return this.numeroEnvio;
    }

    public void setNumeroEnvio(Integer numeroEnvio) {
        this.numeroEnvio = numeroEnvio;
    }

    /**
     * Extrae los resultados del xml generado para productos catastrales Si ocurre un error, lanza
     * una excepción de tipo SncBusinessServiceExceptions.EXCEPCION_100016
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<ProductoCatastralResultado> obtenerResultadosParaProductosCatatrales() {
        List<ProductoCatastralResultado> resultados = new ArrayList<ProductoCatastralResultado>();
        try {
            if (this.resultado != null && this.resultado.startsWith("<")) {
                LOGGER.debug(resultado);
                Document document = DocumentHelper.parseText(resultado);
                List productos = document.selectNodes("//Imagen");
                for (Iterator iter = productos.iterator(); iter.hasNext();) {
                    Element productoXml = (Element) iter.next();

                    String idDocumento = productoXml.attributeValue("documentServerId");
                    String nombreArchivo = FilenameUtils.getName(idDocumento);
                    String tipoArchivo = productoXml.attributeValue("type");
                    String codigo = productoXml.attributeValue("code");
                    String escala = productoXml.attributeValue("scale");

                    ProductoCatastralResultado resultado = new ProductoCatastralResultado(
                        idDocumento, nombreArchivo, tipoArchivo, codigo, escala);
                    resultados.add(resultado);
                    LOGGER.debug("resultado:" + resultado);
                }
                validateErrorsFromProductoJob(document);
            } else {
                String message =
                    "Ocurrió un error durante la ejecución del Servicio Geográfico.  No se obtuvieron Resultados";
                throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null,
                    message);
            }
        } catch (DocumentException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return resultados;
    }

    /**
     * Validación de errores que llegan del servidor SIG
     *
     * @param document
     */
    @SuppressWarnings("rawtypes")
    private void validateErrorsFromProductoJob(Document document) {
        List errores = document.selectNodes("//Exception");
        for (Iterator iter = errores.iterator(); iter.hasNext();) {
            Element exception = (Element) iter.next();
            String type = exception.attributeValue("node_type");
            String detail = exception.getText();
            LOGGER.error(type + " : " + detail);
            if (!"".equals(detail)) {
                throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, null,
                    detail);
            }
        }
    }

    /**
     * Obtiene los números prediales del xml ingresado como parámetro
     *
     * @return
     */
    public List<String> obtenerParametroNumerosPrediales() {
        return obtenerValorParametro(PARAMETRO_CODIGO_PREDIO);
    }

    /**
     * Obtiene los códigos de manzanas del xml ingresado como parámetro
     *
     * @return
     */
    public List<String> obtenerParametroCodigoManzana() {
        return obtenerValorParametro(PARAMETRO_CODIGO_MANZANA);
    }

    /**
     * Extracción de parámetros del xml de entrada
     *
     * @param nombreParametro
     * @return
     */
    @SuppressWarnings("rawtypes")
    private List<String> obtenerValorParametro(String nombreParametro) {
        List<String> resultados = new ArrayList<String>();
        try {
            if (this.codigo != null && this.codigo.startsWith("<")) {
                LOGGER.debug(codigo);
                Document document = DocumentHelper.parseText(codigo);
                List productos = document.selectNodes("//parameter");
                for (Iterator iter = productos.iterator(); iter.hasNext();) {
                    Element parameter = (Element) iter.next();
                    Node name = parameter.selectSingleNode("name");
                    String nodeName = name.getText();
                    if (nodeName.equals(nombreParametro)) {
                        Node value = parameter.selectSingleNode("value");
                        String nodeValue = value.getText();
                        String[] numerosPrediales = nodeValue.split(",");
                        for (String numeroPredial : numerosPrediales) {
                            resultados.add(numeroPredial);
                        }
                    }
                }
            }
        } catch (DocumentException e) {
            throw SncBusinessServiceExceptions.EXCEPCION_100016.getExcepcion(LOGGER, e, e.
                getMessage());
        }
        return resultados;
    }

}
