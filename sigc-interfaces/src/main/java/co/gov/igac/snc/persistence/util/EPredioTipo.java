//TODO :: felipe.cadena :: usar encabezado de clase definido :: pedro.garcia
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para los valores del dominio PREDIO_TIPO
 *
 * @author felipe.cadena
 */
public enum EPredioTipo {

//TODO :: felipe.cadena :: deben ir ordenados alfabéticamente :: pedro.garcia    
    VALDIO("B"),
    DEPARTAMENTAL("D"),
    EJIDO("E"),
    MUNICIPAL("M"),
    NACIONAL("N"),
    PARTICULAR("P"),
    RESGUARDO_INDIGENA("R"),
    TIERRA_COMUNIDADES_NEGRAS("T"),
    RESERVAS_NATURALES("V"),
    VACANTE("X");

    private String codigo;

    private EPredioTipo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

//TODO :: felipe.cadena :: ¿cómo se obtiene el 'valor' del dominio ? :: pedro.garcia        
}
