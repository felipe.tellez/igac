package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/*
 * Proyecto SNC 2017
 */
/**
 * Entidad paa atributos adicionales relativos a la tabla {@link Tramite}
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "TRAMITE_INFO_ADICIONAL")
public class TramiteInfoAdicional implements java.io.Serializable, Cloneable {

    private static final long serialVersionUID = 4262970936756309656L;

// Campos bd
    private Long id;

    private Tramite tramite;

    private String campo;

    private String valor;

    private String usuarioLog;

    private Date fechaLog;

// Campos transient(Deben tener autor y descripcion)
    /**
     * default constructor
     */
    public TramiteInfoAdicional() {
    }
// Metodos

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "TRAMITE_INFO_ADICIONAL_ID_SEQ_GEN")
    @SequenceGenerator(name = "TRAMITE_INFO_ADICIONAL_ID_SEQ_GEN", sequenceName =
        "TRAMITE_INFO_ADICIONAL_ID_SEQ", allocationSize = 1)
    @Column(name = "ID")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "CAMPO")
    public String getCampo() {
        return this.campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    @Column(name = "VALOR")
    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

// Metodos transient(Deben tener autor y descripcion)
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TramiteInfoAdicional other = (TramiteInfoAdicional) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
