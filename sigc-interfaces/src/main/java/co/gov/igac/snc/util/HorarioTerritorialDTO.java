package co.gov.igac.snc.util;

import co.gov.igac.persistence.entity.generales.HorarioAtencionTerritorial;
import co.gov.igac.snc.persistence.util.EDias;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Data Transfer Object para Horario de territorial o UOC
 *
 * @author lorena.salamanca
 *
 */
public class HorarioTerritorialDTO implements Serializable {

    private static final long serialVersionUID = 6906638577341260655L;

    private String dia;
    private Date horaInicialManhana;
    private Date horaFinalManhana;
    private Date horaInicialTarde;
    private Date horaFinalTarde;

    public HorarioTerritorialDTO() {
    }

    public HorarioTerritorialDTO(String dia) {
        this.dia = dia;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public Date getHoraInicialManhana() {
        return horaInicialManhana;
    }

    public void setHoraInicialManhana(Date horaInicialManhana) {
        this.horaInicialManhana = horaInicialManhana;
    }

    public Date getHoraFinalManhana() {
        return horaFinalManhana;
    }

    public void setHoraFinalManhana(Date horaFinalManhana) {
        this.horaFinalManhana = horaFinalManhana;
    }

    public Date getHoraInicialTarde() {
        return horaInicialTarde;
    }

    public void setHoraInicialTarde(Date horaInicialTarde) {
        this.horaInicialTarde = horaInicialTarde;
    }

    public Date getHoraFinalTarde() {
        return horaFinalTarde;
    }

    public void setHoraFinalTarde(Date horaFinalTarde) {
        this.horaFinalTarde = horaFinalTarde;
    }

    @Override
    public String toString() {
        return "HorarioTerritorialDTO{" + "dia=" + dia + ", horaInicialManhana=" +
            horaInicialManhana + ", horaFinalManhana=" +
            horaFinalManhana + ", horaInicialTarde=" + horaInicialTarde + ", horaFinalTarde=" +
            horaFinalTarde + '}';
    }
}
