/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util.constantes;

/**
 * Clase que contiene constantes que se usan para las comunicaciones vía correo electrónico
 *
 * @author pedro.garcia
 */
public class ConstantesComunicacionesCorreoElectronico {

    @Deprecated
    //No usar, usar el método getEmail() del objeto UsuarioDTO
    public static final String EXTENSION_EMAIL_INSTITUCIONAL_IGAC = "@igac.gov.co";

    public static final String ARCHIVO_COMUNICACION_AUTO_DE_PRUEBAS =
        "Comunicacion_auto_de_pruebas_IGAC.pdf";

    public static final String ARCHIVO_AUTO_DE_PRUEBAS =
        "Auto_de_pruebas_IGAC.pdf";

    public static final String ARCHIVO_COMUNICACION_DE_NOTIFICACION =
        "comunicacion_notificacion_de_resolucion_IGAC.pdf";

    public static final String ARCHIVO_ADJUNTO_SOLICITUD_DOCUMENTOS =
        "Solicitud_de_documentos_catastrales_IGAC.pdf";

    public static final String ARCHIVO_ADJUNTO_APROBACION_COMISION =
        "Aprobación_de_comisión_IGAC.pdf";

    public static final String ARCHIVO_DERECHO_PETICION_O_TUTELA =
        "Archivo_derecho_peticion_o_tutela.pdf";

    public static final String ARCHIVO_ADJUNTO_RETIRO_TRAMITE_DE_COMISION =
        "Retiro_trámite_de_comisión_IGAC.pdf";

    public static final String ARCHIVO_ADJUNTO_RECHAZO_COMISION =
        "Rechazo_de_comisión_IGAC.pdf";

    public static final String ARCHIVO_MEMORANDO_CAMBIOESTADO_COMISION =
        "memorando_cambioEstado_de_comision_IGAC.pdf";

    public static final String ASUNTO_SOLICITUD_DOCUMENTOS = "Solicitud de documentos para IGAC";

    public static final String ASUNTO_CANCELACION_ASIGNACION_TRAMITE =
        "Cancelación de asignación de trámite";

    public static final String ASUNTO_COMUNICACION_AUTO_DE_PRUEBAS =
        "Comunicación del IGAC informando el auto de pruebas";

    public static final String ASUNTO_COMUNICACION_DE_NOTIFICACION =
        "Comunicación de notificación del IGAC";

    /*
     * de comisiones
     */
    public static final String ASUNTO_CAMBIO_ESTADO_COMISION = "Cambio de estado de la comisión {p}";
    public static final String ASUNTO_CANCELACION_COMISION = "Cancelación de la comisión {p}";
    public static final String ASUNTO_SUSPENSION_COMISION = "Suspensión de la comisión {p}";
    public static final String ASUNTO_APLAZAMIENTO_COMISION = "Aplazamiento de la comisión {p}";
    public static final String ASUNTO_REACTIVACION_COMISION = "Reactivación de la comisión {p}";
    public static final String ASUNTO_AMPLIACION_COMISION = "Ampliación de la comisión {p}";
    public static final String ASUNTO_RETIROTRAMITES_COMISION =
        "Retiro de trámites de la comisión {p}";
    public static final String ASUNTO_RETIROTRAMITE_COMISION_POR_ENVIO_DEPURACION =
        "Retiro de trámite de comisión por envío a subproceso de depuración";

    public static final String CONTENIDO_CANCELACION_ASIGNACION_TRAMITE =
        "Cordial saludo</br></br>" +
        "Para informar que el trámite con radicación No. {0}. Ha sido retirado de sus asignaciones.</br></br>" +
        "Atentamente</br></br>" +
        "{1}</br>" +
        "Responsable de conservación {2}";

    public static final String CONTENIDO_COMUNICACION_DE_AUTO_DE_PRUEBAS =
        " Cordial saludo<br/><br/>" +
        "Apreciado ciudadano, usted está " +
        "siendo comunicado por el Instituo Geográfico Agustín Codazzi, para informarle sobre el auto de pruebas " +
        "del trámite con número de radicación {p}. <br/><br/>" +
        "Atentamente,<br/><br/>" +
        "{p}<br/>" +
        "Responsable de conservación";

    public static final String CONTENIDO_COMUNICACION_DE_NOTIFICACION =
        "Apreciado ciudadano usted está " +
        "siendo comunicado de la notificación de la resolución de un trámite.";

    public static final String CONTENIDO_SOLICITUD_DOCUMENTOS = "Apreciado ciudadano usted debe " +
        "presentar los documentos referidos en el archivo adjunto.";

    public static final String CONTENIDO_MODIFICACION_COMISION = "Cordial saludo</br>" +
        "Se le informa que la comisión número {0} ha sido {1}.</br></br>" +
        "Atentamente,</br></br> {2}</br>" +
        "Responsable de conservación";

    public static final String CONTENIDO_APROBACION_MODIFICACION_COMISION = "Cordial saludo</br>" +
        "Se le informa que la comisión número {0} ha sido aprobada para ser {1}.</br></br>" +
        "Atentamente,</br></br> {2}</br>" +
        "Director territorial";

    public static final String CONTENIDO_RETIROTRAMITES_COMISION = "Cordial saludo</br>" +
        "Se le informa que de la comisión número {p} han sido retirados los siguientes trámites.</br></br>" +
        "{p}</br></br>" +
        "Atentamente</br> {p}</br>" +
        "Responsable de conservación";

    public static final String CONTENIDO_RETIROTRAMITE_COMISION_POR_ENVIO_DEPURACION =
        "Cordial saludo</br>" +
        "Se le informa de la necesidad de realizar el retiro del trámite radicado con el número {0} " +
        "asociado a la comisión de conservación {1}, ya que dicho trámite ha sido enviado al subproceso de depuración.";

    //----------------      correo de orden de práctica de avalúos -----
    public static final String ASUNTO_CORREO_ORDEN_PRACTICA = "Entrega de orden de práctica No. {0}";

    public static final String CUERPO_CORREO_ORDEN_PRACTICA = "Cordial saludo. <br />" +
        "Adjunto a la presente la orden de práctica de avalúo No {0} <br /><br />";

    public static final String DESPEDIDA_CORREO_ORDEN_PRACTICA = "Atentamente, <br />" +
        "{0} <br /> {1}";

    //-----------      correo de asignación de profesionales de avaluós   --------
    public static final String ASUNTO_CORREO_ASIGNACION_AVALUOS = "Asignación de {0}";

    public static final String CUERPO_CORREO_ASIGNACION_AVALUOS = "Cordial Saludo.<br /> " +
        "Se le informa que ha sido asignado para dar respuesta a la {0}, " +
        "que se encuentra identificado con el sec. Radicado {1} para la actividad de {2}.";

    public static final String CUERPO_OBSERVACIONES_CORREO_ASIGNACION_AVALUOS =
        "<br /><br />Para este trámite debe tener  en cuenta lo siguiente: {0}";

    public static final String DESPEDIDA_CORREO_ASIGNACION_AVALUOS =
        "<br /><br />Cualquier duda por favor ponerse en contacto con el Grupo de Avalúos de la " +
        "territorial {0}. <br /><br />" +
        "Atentamente,<br /> {1} <br /> {2}";

    // --- Correo de comunicación de aprobación y rechazo de comisiones. -----//
    public static final String ASUNTO_COMUNICACION_APROBACION_DE_COMISON =
        "Comunicación de aprobación de la comisión No. {0}";

    public static final String ASUNTO_COMUNICACION_RECHAZO_DE_COMISON =
        "Comunicación de rechazo de la comisión No. {0}";

    // --- Correo de comunicación de retiro de trámite de comisión. -----//
    public static final String ASUNTO_COMUNICACION_RETIRO_TRAMITE_DE_COMISON =
        "Comunicación de retiro de trámite con número de radicación {0} de la comisión No. {1}";

    // --- Correo de comunicación de  cancelación y/o creación de trámites. -----//
    public static final String COMUNICACION_CANCELACION_CREACION_TRAMITE =
        "Comunicación de  cancelación y/o creación de trámites";

    //-----------      correo de Trámite Catastral   --------
    public static final String ASUNTO_CORREO_REGISTRO_TRAMITE_CATASTRAL_PRODUCTOS =
        "Trámite catastral";

    //-----------      correo de Reportes Prediales  --------
    public static final String ASUNTO_CORREO_REGISTRO__REPORTES_PREDIALES = "Reportes prediales";

    //-----------      correo de Reportes estadísticos  --------
    public static final String ASUNTO_CORREO_REGISTRO_REPORTES_ESTADISTICOS = "Reportes estadísticos";

    // --- Correo de comunicación de  bloqueo de predio por solicitud de entidad. -----//
    public static final String ASUNTO_COMUNICACION_BLOQUEO_PREDIO =
        "Comunicación de  bloqueo de predio";

    //-----------      correo de tramites no enviados por ser reclamados previamente  --------
    public static final String ASUNTO_CORREO_TRAMITES_RECLAMADOS =
        "Tramites no avanzados por concurrencia en la actividad {0}";
    //-----------      Correo recalculo avaluos actualizacion  --------
    public static final String ASUNTO_CORREO_RECALCULO_AVALUOS_ACTUALIZACION =
        "Generación de reportes de avalúos de pruebas ";
}
