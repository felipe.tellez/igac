package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para estandarizar los formatos de la documentacion de la actualizacion
 *
 * @author franz.gamba
 *
 */
public enum EActualizacionDocumentacionFormato {

    DIGITAL("Digital"),
    ANALOGO("Analogo"),
    DIGITAL_ANALOGO("Digital y Analogo"),
    NO("No");

    private String descripcion;

    EActualizacionDocumentacionFormato(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
