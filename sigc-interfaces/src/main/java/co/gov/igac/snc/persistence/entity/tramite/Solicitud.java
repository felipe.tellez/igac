package co.gov.igac.snc.persistence.entity.tramite;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;

import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;

/**
 * Solicitud entity. @author MyEclipse Persistence Tools
 */
/*
 * FGWL se adicionó al getId lo necesario para manejo de la secuencia FGWL adicionó get y set para
 * finalizadoJSF boolean para usar en jsf -> Generado serialVersionUID -> Campo direccionPais cambio
 * de tipo de dato a Pais -> Generados getters y setters para modificación de campo direccionPais ->
 * Campo direccionDepartamento cambio de tipo de dato a Departamento -> Generados getters y setters
 * para modificación de campo direccionDepartamento -> Campo direccionMunicipio cambio de tipo de
 * dato a Municipio -> Generados getters y setters para modificación de campo direccionMunicipio
 *
 * @modified by pedro.garcia - merge por cambios en campos de teléfono - método transient
 * isEsSolicitudAvisos
 *
 * @modified by fabio.navarrete -> se ajusta al nuevo modelo de solicitud-solicitante-tramite
 * (08-Jul-2011) -> método isSolicitudAvisos -> Se agrega método Transient isSolicitudDeTesoreria
 * que indica si contiene un solicitante tesorería -> Se agrega método Transient
 * isSolicitudAvisosFinalizada. -> Se agrega método Transient
 * isSolicitudAutoavaluoTramitesCompletos. -> Se agrega método Transient
 * isSolicitudRevisionAvaluoTramitesCompletos. @modified by david.cifuentes -> Se agrega método
 * Transient isRevocatoriaDirecta. -> Se agrega método Transient isViaGubernativa @modified by
 * rodrigo.hernandez -> Se agrega atributo, getter y setter para columna
 * TRAMITE_NUMERO_RADICACION_REF -> Se agrega atributo, getter y setter para columna
 * SUBSIDIO_IMPUGNACION -> Se agrega atributo, getter y setter para columna ORIGEN -> Se agrega
 * atributo, getter y setter para columna MARCO_JURIDICO
 *
 * @modified by rodrigo.hernandez 03-09-2012 -> Se agrega implementacion de interface Cloneable ->
 * Creacion de metodo clone
 *
 * @modified javier.aponte se agrega campo formaPeticion para los solicitudes que se van a marcar
 * como derecho de petición o tutela 13/08/2013
 *
 * -> Se agrega método Transient isSolicitudConTesoreria que indica si contiene un solicitante
 * tesorería 12/09/2013
 *
 */
@Entity
@Table(name = "SOLICITUD", schema = "SNC_TRAMITE", uniqueConstraints = @UniqueConstraint(
    columnNames = "NUMERO"))
public class Solicitud implements java.io.Serializable, Cloneable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -7910853977164236027L;

    private Long id;
    private String tipo;
    private String numero;
    private Date fecha;
    private String nombreAsignadoA;
    private String asunto;
    private String observaciones;
    private String estado;
    private String tipoDocumentoCorrespondencia;
    private String destinoTerritorialId;
    private String destinoDependenciaId;
    private String destinoFuncionarioId;
    private String asignadoAFuncionarioId;
    private String numeroGuia;
    private String sistemaEnvio;
    private String finalizado = ESiNo.NO.getCodigo();
    private Integer folios;
    private Integer anexos;
    private String generaPago;
    private String tipoPago;
    private Long contratoInteradministrativId;
    private String asociadaSolicitudNumero;
    private String estructuraOrganizacional;
    private String numeroRadicacionInicial;
    private String numeroRadicacionDefinitiva;
    private Date fechaRadicacionDefinitiva;
    private String usuarioLog;
    private Date fechaLog;
    private List<SolicitanteSolicitud> solicitanteSolicituds = new ArrayList<SolicitanteSolicitud>();

    private SolicitudAvisoRegistro solicitudAvisoRegistro;
    private List<Tramite> tramites = new ArrayList<Tramite>();
    private List<SolicitudDocumentacion> solicitudDocumentos =
        new ArrayList<SolicitudDocumentacion>();
    private Set<SolicitudPago> solicitudPagos = new HashSet<SolicitudPago>(0);
    private Set<ProductoCatastral> productoCatastrals = new HashSet<ProductoCatastral>(
        0);

    private boolean finalizadoJSF;
    private String tramiteNumeroRadicacionRef;
    private String origen;
    private String subsidioImpugnacion;
    private String marcoJuridico;

    private String formaPeticion;

    private String procesoInstanciaId;

    private List<SolicitudPredio> solicitudPredios = new ArrayList<SolicitudPredio>();

    // Constructors
    /** default constructor */
    public Solicitud() {
    }

    /** minimal constructor */
    public Solicitud(Long id, String tipo, String numero, Date fecha,
        String estado, String finalizado, Integer folios, Integer anexos,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tipo = tipo;
        this.numero = numero;
        this.fecha = fecha;
        this.estado = estado;
        this.finalizado = finalizado;
        this.folios = folios;
        this.anexos = anexos;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Solicitud(Long id, String tipo,
        String numero, Date fecha, String nombreAsignadoA,
        String asunto, String observaciones, String estado,
        String tipoDocumentoCorrespondencia, String destinoTerritorialId,
        String destinoDependenciaId, String destinoFuncionarioId,
        String asignadoAFuncionarioId, String numeroGuia,
        String sistemaEnvio, String finalizado, Integer folios, Integer anexos,
        String generaPago, String tipoPago,
        Long contratoInteradministrativId, String asociadaSolicitudNumero,
        String territorialCodigo, String numeroRadicacionInicial,
        String numeroRadicacionDefinitiva, String usuarioLog,
        Date fechaLog, List<Tramite> tramites,
        List<SolicitanteSolicitud> solicitanteSolicituds,
        SolicitudAvisoRegistro solicitudAvisoRegistro,
        Set<SolicitudPago> solicitudPagos,
        Set<ProductoCatastral> productoCatastrals,
        String tramiteNumeroRadicacionRef) {
        this.id = id;
        this.tipo = tipo;
        this.numero = numero;
        this.fecha = fecha;
        this.nombreAsignadoA = nombreAsignadoA;
        this.asunto = asunto;
        this.observaciones = observaciones;
        this.estado = estado;
        this.tipoDocumentoCorrespondencia = tipoDocumentoCorrespondencia;
        this.destinoTerritorialId = destinoTerritorialId;
        this.destinoDependenciaId = destinoDependenciaId;
        this.destinoFuncionarioId = destinoFuncionarioId;
        this.asignadoAFuncionarioId = asignadoAFuncionarioId;
        this.numeroGuia = numeroGuia;
        this.sistemaEnvio = sistemaEnvio;
        this.finalizado = finalizado;
        this.folios = folios;
        this.anexos = anexos;
        this.generaPago = generaPago;
        this.tipoPago = tipoPago;
        this.contratoInteradministrativId = contratoInteradministrativId;
        this.asociadaSolicitudNumero = asociadaSolicitudNumero;
        this.estructuraOrganizacional = territorialCodigo;
        this.numeroRadicacionInicial = numeroRadicacionInicial;
        this.numeroRadicacionDefinitiva = numeroRadicacionDefinitiva;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.solicitanteSolicituds = solicitanteSolicituds;
        this.solicitudAvisoRegistro = solicitudAvisoRegistro;
        this.tramites = tramites;
        this.solicitudPagos = solicitudPagos;
        this.productoCatastrals = productoCatastrals;
        this.setTramiteNumeroRadicacionRef(tramiteNumeroRadicacionRef);
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SOLICITUD_ID_SEQ")
    @SequenceGenerator(name = "SOLICITUD_ID_SEQ", sequenceName = "SOLICITUD_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "NUMERO", unique = true, nullable = false, length = 20)
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "FECHA", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Column(name = "NOMBRE_ASIGNADO_A", length = 100)
    public String getNombreAsignadoA() {
        return this.nombreAsignadoA;
    }

    public void setNombreAsignadoA(String nombreAsignadoA) {
        this.nombreAsignadoA = nombreAsignadoA;
    }

    @Column(name = "ASUNTO", length = 600)
    public String getAsunto() {
        return this.asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "ESTADO", nullable = false, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "TIPO_DOCUMENTO_CORRESPONDENCIA", length = 30)
    public String getTipoDocumentoCorrespondencia() {
        return this.tipoDocumentoCorrespondencia;
    }

    public void setTipoDocumentoCorrespondencia(
        String tipoDocumentoCorrespondencia) {
        this.tipoDocumentoCorrespondencia = tipoDocumentoCorrespondencia;
    }

    @Column(name = "DESTINO_TERRITORIAL_ID", length = 10)
    public String getDestinoTerritorialId() {
        return this.destinoTerritorialId;
    }

    public void setDestinoTerritorialId(String destinoTerritorialId) {
        this.destinoTerritorialId = destinoTerritorialId;
    }

    @Column(name = "DESTINO_DEPENDENCIA_ID", length = 10)
    public String getDestinoDependenciaId() {
        return this.destinoDependenciaId;
    }

    public void setDestinoDependenciaId(String destinoDependenciaId) {
        this.destinoDependenciaId = destinoDependenciaId;
    }

    @Column(name = "DESTINO_FUNCIONARIO_ID", length = 20)
    public String getDestinoFuncionarioId() {
        return this.destinoFuncionarioId;
    }

    public void setDestinoFuncionarioId(String destinoFuncionarioId) {
        this.destinoFuncionarioId = destinoFuncionarioId;
    }

    @Column(name = "ASIGNADO_A_FUNCIONARIO_ID", length = 100)
    public String getAsignadoAFuncionarioId() {
        return this.asignadoAFuncionarioId;
    }

    public void setAsignadoAFuncionarioId(String asignadoAFuncionarioId) {
        this.asignadoAFuncionarioId = asignadoAFuncionarioId;
    }

    @Column(name = "NUMERO_GUIA", length = 20)
    public String getNumeroGuia() {
        return this.numeroGuia;
    }

    public void setNumeroGuia(String numeroGuia) {
        this.numeroGuia = numeroGuia;
    }

    @Column(name = "SISTEMA_ENVIO", length = 30)
    public String getSistemaEnvio() {
        return this.sistemaEnvio;
    }

    public void setSistemaEnvio(String sistemaEnvio) {
        this.sistemaEnvio = sistemaEnvio;
    }

    @Column(name = "FINALIZADO", nullable = false, length = 2)
    public String getFinalizado() {
        return this.finalizado;
    }

    public void setFinalizado(String finalizado) {
        this.finalizado = finalizado;
    }

    @Column(name = "FOLIOS", nullable = false, precision = 4, scale = 0)
    public Integer getFolios() {
        return this.folios;
    }

    public void setFolios(Integer folios) {
        this.folios = folios;
    }

    @Column(name = "ANEXOS", nullable = false, precision = 4, scale = 0)
    public Integer getAnexos() {
        return this.anexos;
    }

    public void setAnexos(Integer anexos) {
        this.anexos = anexos;
    }

    @Column(name = "GENERA_PAGO", length = 2)
    public String getGeneraPago() {
        return this.generaPago;
    }

    public void setGeneraPago(String generaPago) {
        this.generaPago = generaPago;
    }

    @Column(name = "TIPO_PAGO", length = 30)
    public String getTipoPago() {
        return this.tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    @Column(name = "CONTRATO_INTERADMINISTRATIV_ID", precision = 10, scale = 0)
    public Long getContratoInteradministrativId() {
        return this.contratoInteradministrativId;
    }

    public void setContratoInteradministrativId(
        Long contratoInteradministrativId) {
        this.contratoInteradministrativId = contratoInteradministrativId;
    }

    @Column(name = "ASOCIADA_SOLICITUD_NUMERO", length = 20)
    public String getAsociadaSolicitudNumero() {
        return this.asociadaSolicitudNumero;
    }

    public void setAsociadaSolicitudNumero(String asociadaSolicitudNumero) {
        this.asociadaSolicitudNumero = asociadaSolicitudNumero;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD", length = 2)
    public String getTerritorialCodigo() {
        return this.estructuraOrganizacional;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.estructuraOrganizacional = territorialCodigo;
    }

    @Column(name = "NUMERO_RADICACION_INICIAL", length = 20)
    public String getNumeroRadicacionInicial() {
        return this.numeroRadicacionInicial;
    }

    public void setNumeroRadicacionInicial(String numeroRadicacionInicial) {
        this.numeroRadicacionInicial = numeroRadicacionInicial;
    }

    @Column(name = "NUMERO_RADICACION_DEFINITIVA", length = 20)
    public String getNumeroRadicacionDefinitiva() {
        return this.numeroRadicacionDefinitiva;
    }

    public void setNumeroRadicacionDefinitiva(String numeroRadicacionDefinitiva) {
        this.numeroRadicacionDefinitiva = numeroRadicacionDefinitiva;
    }

    @Column(name = "FECHA_RADICACION_DEFINITIVA", length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaRadicacionDefinitiva() {
        return this.fechaRadicacionDefinitiva;
    }

    public void setFechaRadicacionDefinitiva(Date fechaRadicacionDefinitiva) {
        this.fechaRadicacionDefinitiva = fechaRadicacionDefinitiva;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitud")
    public List<Tramite> getTramites() {
        return this.tramites;
    }

    public void setTramites(List<Tramite> tramites) {
        this.tramites = tramites;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitud")
    public List<SolicitudDocumentacion> getSolicitudDocumentos() {
        return solicitudDocumentos;
    }

    public void setSolicitudDocumentos(List<SolicitudDocumentacion> solicitudDocumentos) {
        this.solicitudDocumentos = solicitudDocumentos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitud")
    public Set<SolicitudPago> getSolicitudPagos() {
        return this.solicitudPagos;
    }

    public void setSolicitudPagos(Set<SolicitudPago> solicitudPagos) {
        this.solicitudPagos = solicitudPagos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitud")
    public Set<ProductoCatastral> getProductoCatastrals() {
        return this.productoCatastrals;
    }

    public void setProductoCatastrals(Set<ProductoCatastral> productoCatastrals) {
        this.productoCatastrals = productoCatastrals;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitud")
    public List<SolicitanteSolicitud> getSolicitanteSolicituds() {
        return solicitanteSolicituds;
    }

    public void setSolicitanteSolicituds(List<SolicitanteSolicitud> solicitanteSolicituds) {
        this.solicitanteSolicituds = solicitanteSolicituds;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitud")
    public SolicitudAvisoRegistro getSolicitudAvisoRegistro() {
        return solicitudAvisoRegistro;
    }

    public void setSolicitudAvisoRegistro(SolicitudAvisoRegistro solicitudAvisoRegistro) {
        this.solicitudAvisoRegistro = solicitudAvisoRegistro;
    }

    @Column(name = "TRAMITE_NUMERO_RADICACION_REF", length = 7)
    public String getTramiteNumeroRadicacionRef() {
        return tramiteNumeroRadicacionRef;
    }

    public void setTramiteNumeroRadicacionRef(String tramiteNumeroRadicacionRef) {
        this.tramiteNumeroRadicacionRef = tramiteNumeroRadicacionRef;
    }

    @Column(name = "ORIGEN", length = 7)
    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    @Column(name = "SUBSIDIO_IMPUGNACION", length = 7)
    public String getSubsidioImpugnacion() {
        return subsidioImpugnacion;
    }

    public void setSubsidioImpugnacion(String subsidioImpugnacion) {
        this.subsidioImpugnacion = subsidioImpugnacion;
    }

    @Column(name = "MARCO_JURIDICO", length = 7)
    public String getMarcoJuridico() {
        return marcoJuridico;
    }

    public void setMarcoJuridico(String marcoJuridico) {
        this.marcoJuridico = marcoJuridico;
    }

    @Column(name = "FORMA_PETICION")
    public String getFormaPeticion() {
        return formaPeticion;
    }

    public void setFormaPeticion(String formaPeticion) {
        this.formaPeticion = formaPeticion;
    }

    @Column(name = "PROCESO_INSTANCIA_ID", length = 100)
    public String getProcesoInstanciaId() {
        return this.procesoInstanciaId;
    }

    public void setProcesoInstanciaId(String procesoInstanciaId) {
        this.procesoInstanciaId = procesoInstanciaId;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitud")
    public List<SolicitudPredio> getSolicitudPredios() {
        return solicitudPredios;
    }

    public void setSolicitudPredios(List<SolicitudPredio> solicitudPredios) {
        this.solicitudPredios = solicitudPredios;
    }

    @Transient
    public boolean isFinalizadoJSF() {
        if (finalizado != null && finalizado.equals(ESiNo.SI.getCodigo())) {
            return true;
        } else {
            return false;
        }
    }

    public void setFinalizadoJSF(boolean finalizadoJSF) {
        this.finalizadoJSF = finalizadoJSF;
        if (this.finalizadoJSF) {
            this.finalizado = ESiNo.SI.getCodigo();
        } else {
            this.finalizado = ESiNo.NO.getCodigo();
        }
    }

    /**
     * Método encargado de traducir el SINO por un booleano para determinar si se asigna valor a la
     * variable que indica el envío por correo electrónico de información.
     *
     * @param value
     * @author fabio.navarrete
     */
    public void setNotificacionesCorreoElectronico(boolean value) {
        // TODO :: fabio.navarrete :: 03-08-2011 :: Asignar el valor a la
        // variable que va a crear Julio :: fabio.navarrete
    }

    /**
     * Retorna verdadero si el tipo de solicitud es Avisos.
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isSolicitudAvisos() {
        if (this.tipo != null) {
            return this.tipo.equals(ESolicitudTipo.AVISOS.getCodigo());
        }
        return false;
    }

    /**
     * Retorna verdadero si el tipo de solicitud es Avisos. Creé uno que hace lo mismo pero con otro
     * nombre porque cuando se usa en los jsf queda x.solicitudAviso, lo cual no me indica que está
     * preguntando por un booleano. No se usa el estándar de java que para esto es una chimbada
     * porque muchas variables booleanas son de nombre isXXX (o esXXX)
     *
     * @author pedro.garcia
     * @return
     */
    // COMENTADO POR FGWL. Porque no debe haber dos metodos similares y el anterior tiene el estandar java
    /* @Transient public boolean getEsSolicitudAvisos(){ if(this.tipo != null){ return
     * this.tipo.equals(ESolicitudTipo.AVISOS.getCodigo()); } return false; } */
    /**
     * Retorna verdadero si el tipo de solicitud es de tipo Revisión de Avalúo.
     *
     * @return
     */
    @Transient
    public boolean isRevisionAvaluo() {
        if (this.tipo != null) {
            return this.tipo.equals(ESolicitudTipo.REVISION_AVALUO.getCodigo());
        }
        return false;
    }

    /**
     * Retorna verdadero si el tipo de solicitud es de tipo Via gubernatica
     *
     * @return
     */
    @Transient
    public boolean isViaGubernativa() {
        if (this.tipo != null) {
            return this.tipo.equals(ESolicitudTipo.VIA_GUBERNATIVA.getCodigo());
        }
        return false;
    }

    /**
     * Retorna verdadero si el tipo de solicitud es de tipo RevocatoriaDirecta
     *
     * @return
     */
    @Transient
    public boolean isRevocatoriaDirecta() {
        if (this.tipo != null) {
            return this.tipo.equals(ESolicitudTipo.REVOCATORIA_DIRECTA.getCodigo());
        }
        return false;
    }

    /**
     * Indica si el objeto Silicitud contiene Intermediarios en sus solicitantes
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isSolicitudConIntermediario() {
        if (this.solicitanteSolicituds != null) {
            for (SolicitanteSolicitud solSol : this.solicitanteSolicituds) {
                if (solSol.isIntermediario()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Indica si el objeto Solicitud contiene Tesoreros en sus solicitantes
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isSolicitudConTesoreria() {
        if (this.solicitanteSolicituds != null) {
            for (SolicitanteSolicitud solSol : this.solicitanteSolicituds) {
                if (solSol.isTesoreria()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Retorna verdadero si el tipo de solicitud es Autoavalúo.
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isSolicitudAutoavaluo() {
        if (this.tipo != null) {
            return this.tipo.equals(ESolicitudTipo.AUTOAVALUO.getCodigo());
        }
        return false;
    }

    /**
     * Retorna verdadero si la solicitud es de avisos y no quedan avisos pendientes
     *
     * @return
     */
    @Transient
    public boolean isSolicitudAvisosFinalizada() {
        if (this.isSolicitudAvisos() && this.solicitudAvisoRegistro != null) {
            return this.solicitudAvisoRegistro.getAvisosPendientes() == 0;
        }
        return false;
    }

    /**
     * Sirve para determinar si una solicitud de autoavalúo ya tiene el máximo de trámites que
     * permite diligenciar. Debería validar sólo un trámite pero está implementado para más de uno
     * en caso de datos preexistentes que tengan más de uno
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isSolicitudAutoavaluoTramitesCompletos() {
        if (this.isSolicitudAutoavaluo()) {
            if (this.tramites != null && !this.tramites.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sirve para determinar si una solicitud de revisión de avalúo ya tiene el máximo de trámites
     * que permite diligenciar. Debería validar sólo un trámite pero está implementado para más de
     * uno, en caso de datos preexistentes que tengan más de uno.
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isSolicitudRevisionAvaluoTramitesCompletos() {
        if (this.isRevisionAvaluo()) {
            if (this.tramites != null && !this.tramites.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    @Transient
    public boolean isTramitesConsolidados() {
        if (this.estado != null) {
            return this.estado.equals(ESolicitudEstado.TRAMITES_CONSOLIDADOS.getCodigo());
        }
        return false;
    }

    /**
     * Método para consultar el solicitante de la solicitud
     *
     * @author rodrigo.hernandez
     *
     */
    @Transient
    public SolicitanteSolicitud getSolicitante() {
        if (Hibernate.isInitialized(this.solicitanteSolicituds)) {
            for (SolicitanteSolicitud sol : this.solicitanteSolicituds) {
                if (sol.getRelacion().equals(
                    ESolicitanteSolicitudRelac.PROPIETARIO.toString())) {
                    return sol;
                }
            }
        }
        return null;
    }

    /**
     * Método para consultar el solicitante de la solicitud
     *
     * @author felipe.cadena
     *
     */
    @Transient
    public SolicitanteSolicitud getRepresentanteLegal() {
        if (Hibernate.isInitialized(this.solicitanteSolicituds)) {
            for (SolicitanteSolicitud sol : this.solicitanteSolicituds) {
                if (sol.getRelacion().equals(
                    ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL.getValor().toUpperCase())) {
                    return sol;
                }
            }
        }
        return null;
    }

    /**
     * @author rodrigo.hernandez Método para poder hacer un clon de trámite documentación
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

    /**
     * Retorna verdadero si el tipo de solicitud es trámite catastral.
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isSolicitudTramiteCatastral() {
        return ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo().equals(this.tipo);
    }

    /**
     * Retorna verdadero si el tipo de solicitud es de productos catastrales.
     *
     * @return
     * @author javier.aponte
     */
    @Transient
    public boolean isSolicitudProductosCatastrales() {
        if (this.tipo != null) {
            return this.tipo.equals(ESolicitudTipo.PRODUCTOS_CATASTRALES.getCodigo());
        }
        return false;
    }
}
