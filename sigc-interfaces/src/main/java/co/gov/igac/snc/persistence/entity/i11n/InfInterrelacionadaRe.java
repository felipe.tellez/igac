package co.gov.igac.snc.persistence.entity.i11n;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the INF_INTERRELACIONADA_RES database table.
 *
 */
@Entity
@Table(name = "INF_INTERRELACIONADA_RES", schema = "SNC_INTERRELACION")
public class InfInterrelacionadaRe implements Serializable {

    private static final long serialVersionUID = 1L;
    private String anotacionReal;
    private Double areaTerreno;
    private String automatica;
    private String circuloRegistral;
    private String ciudadOrigenInstrumento;
    //private String codigoDepartamento;
    private Departamento departamento;
    //private String codigoMunicipio;
    private Municipio municipio;
    private String direccion;
    private String estado;
    private String estadoFolio;
    private String estadoInterrelacion;
    private Date fechaInstrumento;
    private Date fechaRadicacion;
    private Date fechaRecibido;
    private Date fechaResolucion;
    private BigDecimal idEnvioRegistro;
    private String instrumento;
    private String matricula;
    private String modoAdquisicion;
    private String naturalezaJuridica;
    private Long nciId;
    private String numeroAnotacion;
    private String numeroCatastro;
    private String numeroInstrumento;
    private String numeroResolucion;
    private String observaciones;
    private String oficinaOrigenInstrumento;
    private String resultado;
    private Long solicitudId;
    private String solicitudNumero;
    private Long tramiteId;
    private String tramiteNumeroRadicacion;
    private String url;
    private boolean seleccionado;

    public InfInterrelacionadaRe() {
    }

    public InfInterrelacionadaRe(InfInterrelacionada i, Solicitud s, Tramite t, Predio p,
        boolean automatico, boolean procesar) {

        this.nciId = i.getNciId();
        this.idEnvioRegistro = i.getIdEnvioRegistro();
        this.circuloRegistral = i.getCirculoRegistral();
        this.matricula = i.getMatricula();
        if (departamento == null) {
            this.departamento = new Departamento();
        }
        this.departamento.setCodigo(i.getCodigoDepartamento());
        if (municipio == null) {
            this.municipio = new Municipio();
        }
        this.municipio.setCodigo(i.getCodigoDepartamento() + i.getCodigoMunicipio());
        this.numeroAnotacion = i.getNumeroAnotacion();
        this.anotacionReal = i.getAnotacionReal();
        this.direccion = i.getDireccion();
        this.naturalezaJuridica = i.getNaturalezaJuridica();
        this.modoAdquisicion = i.getModoAdquisicion();
        this.numeroCatastro = i.getNumeroCatastro();
        this.estadoFolio = i.getEstadoFolio();
        this.instrumento = i.getInstrumento();
        this.numeroInstrumento = i.getNumeroInstrumento();
        this.fechaInstrumento = i.getFechaInstrumento();
        this.oficinaOrigenInstrumento = i.getOficinaOrigenInstrumento();
        this.ciudadOrigenInstrumento = i.getCiudadOrigenInstrumento();
        this.estadoInterrelacion = i.getEstadoInterrelacion();
        this.estado = i.getEstado();
        this.automatica = i.getAutomatica();
        this.fechaRecibido = i.getFechaRecibido();

        this.solicitudId = s.getId();
        this.solicitudNumero = s.getNumero();

        this.tramiteId = t.getId();
        this.tramiteNumeroRadicacion = t.getNumeroRadicacion();
        this.fechaRadicacion = t.getFechaRadicacion();
        this.numeroResolucion = t.getNumeroResolucion();

        //this.fechaResolucion = t.getResultadoDocumento().getFechaRadicacion();
        this.areaTerreno = p.getAreaTerreno();
        //this.url=
        this.resultado = automatico && procesar ? "ACEPTADO" : "RECHAZADO";
        this.observaciones = t.getObservacionesRadicacion();

    }

    @Column(name = "ANOTACION_REAL")
    public String getAnotacionReal() {
        return this.anotacionReal;
    }

    public void setAnotacionReal(String anotacionReal) {
        this.anotacionReal = anotacionReal;
    }

    @Column(name = "AREA_TERRENO")
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    public String getAutomatica() {
        return this.automatica;
    }

    public void setAutomatica(String automatica) {
        this.automatica = automatica;
    }

    @Column(name = "CIRCULO_REGISTRAL")
    public String getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(String circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "CIUDAD_ORIGEN_INSTRUMENTO")
    public String getCiudadOrigenInstrumento() {
        return this.ciudadOrigenInstrumento;
    }

    public void setCiudadOrigenInstrumento(String ciudadOrigenInstrumento) {
        this.ciudadOrigenInstrumento = ciudadOrigenInstrumento;
    }


    /* @Column(name="CODIGO_DEPARTAMENTO") public String getCodigoDepartamento() { return
     * this.codigoDepartamento; }
     *
     * public void setCodigoDepartamento(String codigoDepartamento) { this.codigoDepartamento =
     * codigoDepartamento; }
     *
     *
     * @Column(name="CODIGO_MUNICIPIO") public String getCodigoMunicipio() { return
     * this.codigoMunicipio; }
     *
     * public void setCodigoMunicipio(String codigoMunicipio) { this.codigoMunicipio =
     * codigoMunicipio; } */
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "ESTADO_FOLIO")
    public String getEstadoFolio() {
        return this.estadoFolio;
    }

    public void setEstadoFolio(String estadoFolio) {
        this.estadoFolio = estadoFolio;
    }

    @Column(name = "ESTADO_INTERRELACION")
    public String getEstadoInterrelacion() {
        return this.estadoInterrelacion;
    }

    public void setEstadoInterrelacion(String estadoInterrelacion) {
        this.estadoInterrelacion = estadoInterrelacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSTRUMENTO")
    public Date getFechaInstrumento() {
        return this.fechaInstrumento;
    }

    public void setFechaInstrumento(Date fechaInstrumento) {
        this.fechaInstrumento = fechaInstrumento;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RADICACION")
    public Date getFechaRadicacion() {
        return this.fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RECIBIDO")
    public Date getFechaRecibido() {
        return this.fechaRecibido;
    }

    public void setFechaRecibido(Date fechaRecibido) {
        this.fechaRecibido = fechaRecibido;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RESOLUCION")
    public Date getFechaResolucion() {
        return this.fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    @Column(name = "ID_ENVIO_REGISTRO")
    public BigDecimal getIdEnvioRegistro() {
        return this.idEnvioRegistro;
    }

    public void setIdEnvioRegistro(BigDecimal idEnvioRegistro) {
        this.idEnvioRegistro = idEnvioRegistro;
    }

    public String getInstrumento() {
        return this.instrumento;
    }

    public void setInstrumento(String instrumento) {
        this.instrumento = instrumento;
    }

    public String getMatricula() {
        return this.matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Column(name = "MODO_ADQUISICION")
    public String getModoAdquisicion() {
        return this.modoAdquisicion;
    }

    public void setModoAdquisicion(String modoAdquisicion) {
        this.modoAdquisicion = modoAdquisicion;
    }

    @Column(name = "NATURALEZA_JURIDICA")
    public String getNaturalezaJuridica() {
        return this.naturalezaJuridica;
    }

    public void setNaturalezaJuridica(String naturalezaJuridica) {
        this.naturalezaJuridica = naturalezaJuridica;
    }

    @Id
    @Column(name = "NCI_ID")
    public Long getNciId() {
        return this.nciId;
    }

    public void setNciId(Long nciId) {
        this.nciId = nciId;
    }

    @Column(name = "NUMERO_ANOTACION")
    public String getNumeroAnotacion() {
        return this.numeroAnotacion;
    }

    public void setNumeroAnotacion(String numeroAnotacion) {
        this.numeroAnotacion = numeroAnotacion;
    }

    @Column(name = "NUMERO_CATASTRO")
    public String getNumeroCatastro() {
        return this.numeroCatastro;
    }

    public void setNumeroCatastro(String numeroCatastro) {
        this.numeroCatastro = numeroCatastro;
    }

    @Column(name = "NUMERO_INSTRUMENTO")
    public String getNumeroInstrumento() {
        return this.numeroInstrumento;
    }

    public void setNumeroInstrumento(String numeroInstrumento) {
        this.numeroInstrumento = numeroInstrumento;
    }

    @Column(name = "NUMERO_RESOLUCION")
    public String getNumeroResolucion() {
        return this.numeroResolucion;
    }

    public void setNumeroResolucion(String numeroResolucion) {
        this.numeroResolucion = numeroResolucion;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "OFICINA_ORIGEN_INSTRUMENTO")
    public String getOficinaOrigenInstrumento() {
        return this.oficinaOrigenInstrumento;
    }

    public void setOficinaOrigenInstrumento(String oficinaOrigenInstrumento) {
        this.oficinaOrigenInstrumento = oficinaOrigenInstrumento;
    }

    public String getResultado() {
        return this.resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Column(name = "SOLICITUD_ID")
    public Long getSolicitudId() {
        return this.solicitudId;
    }

    public void setSolicitudId(Long solicitudId) {
        this.solicitudId = solicitudId;
    }

    @Column(name = "SOLICITUD_NUMERO")
    public String getSolicitudNumero() {
        return this.solicitudNumero;
    }

    public void setSolicitudNumero(String solicitudNumero) {
        this.solicitudNumero = solicitudNumero;
    }

    @Column(name = "TRAMITE_ID")
    public Long getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    @Column(name = "TRAMITE_NUMERO_RADICACION")
    public String getTramiteNumeroRadicacion() {
        return this.tramiteNumeroRadicacion;
    }

    public void setTramiteNumeroRadicacion(String tramiteNumeroRadicacion) {
        this.tramiteNumeroRadicacion = tramiteNumeroRadicacion;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Transient
    public boolean isSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO_DEPARTAMENTO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO_MUNICIPIO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

}
