package co.gov.igac.snc.persistence.entity.formacion;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the V_USO_CONSTRUCCION_ZONA database table.
 *
 */
@Entity
@Table(name = "V_USO_CONSTRUCCION_ZONA", schema = "SNC_FORMACION")
public class VUsoConstruccionZona implements Serializable {

    private static final long serialVersionUID = 1L;

    private String destinoEconomico;
    private Long id;
    private String nombre;
    private String tipoUso;
    private Date vigencia;
    private String zonaCodigo;

    public VUsoConstruccionZona() {
    }

    @Column(name = "DESTINO_ECONOMICO")
    public String getDestinoEconomico() {
        return this.destinoEconomico;
    }

    public void setDestinoEconomico(String destinoEconomico) {
        this.destinoEconomico = destinoEconomico;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "TIPO_USO")
    public String getTipoUso() {
        return this.tipoUso;
    }

    public void setTipoUso(String tipoUso) {
        this.tipoUso = tipoUso;
    }

    @Temporal(TemporalType.DATE)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "ZONA_CODIGO")
    public String getZonaCodigo() {
        return this.zonaCodigo;
    }

    public void setZonaCodigo(String zonaCodigo) {
        this.zonaCodigo = zonaCodigo;
    }

}
