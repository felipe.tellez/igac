/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Enumeración con los posibles valores del dominio de OFERTA_TIPO_INMUEBLE
 *
 * @author juan.agudelo
 */
public enum EWVVentanasOfertaTipoInmueble {

    CASA_APARTAMENTO_SH("proyectoCasaAptoSHWV"),
    LOTE_CASALOTE("proyectoCasaLoteWV"),
    LOCAL("proyectoLocalWV"),
    OFICINA_CONSULTORIO("proyectoOficinaConsultorioWV"),
    BODEGA("proyectoBodegaWV"),
    FINCA("proyectoFincaWV"),
    EDIFICIO("proyectoEdificioWV"),
    PARQUEADERO("proyectoParqueaderoWV");

    private String widgetVar;

    private EWVVentanasOfertaTipoInmueble(String widgetVar) {
        this.widgetVar = widgetVar;
    }

    public String getWidgetVar() {
        return this.widgetVar;
    }

}
