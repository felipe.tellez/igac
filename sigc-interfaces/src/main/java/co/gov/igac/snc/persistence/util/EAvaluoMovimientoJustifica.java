package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para dominio AVALUO_MOVIMIENTO_JUSTIFICA
 *
 * @author rodrigo.hernandez
 */
public enum EAvaluoMovimientoJustifica {

    DESASTRE_NATURAL("DESASTRE NATURAL", "Desastre natural"),
    ORDEN_PUBLICO("ORDEN PUBLICO", "Orden público"),
    OTRA_JUSTIFICACION("OTRA JUSTIFICACION", "Otra justificación");

    private String codigo;
    private String valor;

    private EAvaluoMovimientoJustifica(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }
}
