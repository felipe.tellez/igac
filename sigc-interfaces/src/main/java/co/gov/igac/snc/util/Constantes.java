package co.gov.igac.snc.util;

import co.gov.igac.snc.persistence.util.EPredioInconsistenciaTipo;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Constantes {

    /** separador de cadenas usado en el método toString() de los objetos para los que se definen
     * converters */
    public static final String CONVERTERS_TOSTRING_STRINGSEPARATOR = ":;:";

    public static final String CANCELA_INSCRIBE_NULO = "Nulo";
    public static final String COLOMBIA = "140";
    public static final List<String> CONDICIONES_PROPIEDAD_DESENGLOBE = Arrays.asList(new String[]{
        "0", "8", "9"});
    public static final List<String> CONDICIONES_PROPIEDAD_ENGLOBE = Arrays.asList(new String[]{"0",
        "2", "3", "4", "5", "6", "7", "8", "9"});
    public static final String CONSTANTE_CADENA_VACIA_DB = " ";
    public static final String DATO_RECTIFICAR_ANIO_CONSTRUCCION = "ANIO_CONSTRUCCION";
    public static final List<String> DATOS_RECTIFICAR_AREA = Arrays
        .asList(new String[]{"AREA_TOTAL_TERRENO_PRIVADA",
        "AREA_TOTAL_TERRENO_COMUN",
        "AREA_TOTAL_CONSTRUIDA_PRIVADA",
        "AREA_TOTAL_CONSTRUIDA_COMUN", "AREA_TERRENO",
        "AREA_CONSTRUCCION", "AREA_CONSTRUIDA"});

    public static final String SUBTIPO_RECTIFICACION_GEOGRAFICA = "Area total terreno con linderos ";
    public static final List<String> DATOS_RECTIFICAR_ZONA = Arrays
        .asList(new String[]{"ZONA_FISICA",
        "ZONA_GEOECONOMICA"});
    public static final String AREA_CONSTRUIDA = "AREA_CONSTRUIDA";
    public static final String ANIO_CONSTRUCCION = "ANIO_CONSTRUCCION";
    public static final String MATRICULA_INMOBILIARIA = "NUMERO_REGISTRO";
    public static final String DETALLE_CALIFICACION = "DETALLE_CALIFICACION";
    public static final String DATO_RECTIFICAR_CANCELACION_DOBLE_INSCRIPCION =
        "Cancelación doble inscripción";
    public static final String DATO_RECTIFICAR_DETALLE_CALIFICACION = "DETALLE_CALIFICACION";
    public static final List<String> DIRECCION = Arrays
        .asList(new String[]{"DIRECCION, PRINCIPAL"});
    public static final String TRAMITE_SUBTIPO_CANCELACION = "CANCELACION";
    public static final List<String> DATOS_RECTIFICAR_USO = Arrays
        .asList(new String[]{"USO_ID"});
    public static final List<Long> DATOS_MODIFICAN_AVALUO = Arrays.asList(new Long[]{12l, 14l, 32l,
        35l, 54l, 47l, 48l, 49l, 24l});
    public static final String DOMINIO_DOCUMENTO_ESTADO_RECIBIDO = "Recibido";
    public static final String DOMINIO_IGAC = "igac.gov.co";
    public static final String DOCUMENTACION_ESTADO_INCOMPLETO = "INCOMPLETA";
    public static final String DOCUMENTACION_ESTADO_COMPLETO = "COMPLETA";
    public static final String DOCUMENTO_ESTADO_RECIBIDO = "RECIBIDO";
    //public static final String ENGLOBE = "ENGLOBE";
    public static final String FORMATO_FECHA = "dd/MM/yyyy";
    public static final String FORMATO_FECHA_HORA = "dd/MM/yyyy hh:mm:ss";
    public static final String FORMATO_FECHA_HORA_12 = "dd/MM/yyyy hh:mm a";
    public static final String FORMATO_FECHA_HORA_24 = "dd/MM/yyyy HH:mm:ss";
    public static final String FORMATO_FECHA_EDICTO = "dd 'de' MM 'de' yyyy 'a las' h:mm a";
    public static final String FORMATO_FECHA_DILIGENCIA_NOTIFICACION =
        "dd 'd&iacute;as del mes de' MMMMM 'del' yyyy 'siendo las' HH:mm a";
    public static final String FORMATO_FECHA_RESOLUCION = "EEEEE dd 'DE' MMMMM 'DEL' yyyy";
    public static final String FORMATO_FECHA_TITULO_RESOLUCION = "dd 'de' MMMM 'de' yyyy";
    public static final String FORMATO_NUMERO_MONEDA = "$#,###";
    public static final String FORMATO_NUMERO_DECIMAL = "######.###"; //redmine #7491 version 1.1.4 - felipe.cadena_7286

    /**
     * tiene que ser con MM porque si se usa mm con '/' el componente se comporta raro. Con '-' sí
     * se puede usar mm
     */
    public static final String FORMATO_FECHA_CALENDAR = "dd/MM/yyyy";
    public static final String FORMATO_FECHA_USUARIO_2 = "dd/MM/yyyy";

    public static final String FORMATO_FECHA_USUARIO = "dd/MM/yyyy";
    public static final Double HECTAREA = 10000D;

    /**
     * valor del IVA actual (como factor)
     */
    public static final Double IVA = 0.16D;

    public static final String NO_APLICA = "N/A";
    public static final String NO_DATA_FOUND = "No se encontraron datos";
    public static final String NO_EXISTEN_FILTROS_DISPONIBLES = "No existen filtros disponibles";
    public static final int NUMERACION_AVALUO_TIPO_DOCUMENTO = 18;
    public static final String OBSERVACIONES_PROCESO = "observaciones";
    public static final String OTRO = "OTRO";
    public static final String OFICIO = "OFICIO";
    public static final String REASIGNAR = "REASIGNAR";
    public static final String RESOLUCION = "RESOLUCION";
    public static final String PALABRA_CLAVE_TMP_HISTORICO_OBS = "PALABRA_CLAVE_TMP_HISTORICO_OBS";
    public static final String PREFIJO_JOB_ID = "JOB_ID:";
    public static final String PROYECCION_CANCELA = "C";
    public static final String PROYECCION_INSCRIBE = "I";
    public static final String REPRESENTANTE_LEGAL = "REPRESENTANTE LEGAL";
    public static final String DIRECCION_GENERAL_CODIGO = "1000";
    public static final String SEPARADOR_VACIO_DOCUMENTO_BLOQUEO = "-";
    public static final String SEPARADOR_CADENAS_DOCUMENTO_NOMBRE = "_";
    public static final CharSequence TESORERIA = "tesoreria";
    public static final String TEXTO_BOTON_COMPLEMENTACION = "Seleccinar datos a complementar";
    public static final String TEXTO_BOTON_RECTIFICACION = "Seleccionar datos a rectificar";
    public static final String TIPO_DOCUMENTO_NIT = "NIT";
    public static final String TIPO_DOCUMENTO_NIT_MInicial = "Nit";
    public static final String TIPO_DOCUMENTO_NIT_Min = "nit";
    public static final String TIPO_MIME_PDF = "application/pdf";
    public static final String TIPO_MIME_ZIP = "application/zip";

    public static final String TODAS_LAS_ZONAS = "Todas las zonas";

    public static final String TIPO_PREDIO_COLINDANTE_VIA = "VIA";
    public static final String TIPO_PREDIO_COLINDANTE_PREDIO = "PREDIO";
    public static final Long TIPO_DOCUMENTO_FOTOGRAFIA = 9L;
    public static final Long TIPO_IMG_PREDIAL = 111L;
    public static final Long TIPO_IMG_PREDIAL_DETALLE = 112L;

    public static final Long TIPO_DATO_RECTIFICACION_CDI_GEO = 83L;
    /**
     * Cadena de los estados de un tramite para graficar
     */
    public static final String[] ESTADO_TRAMITE_GRAFICA = {"1", "8", "12", "21", "30", "31", "32"};
    /**
     * Hoja del Excel donde estan los formatos para el estudio de Costos para Actualizacion
     */
    public static final String ESTUDIO_DE_COSTOS = "F12000-10-10.V5";
    public static final String FORMATOS_ESTUDIO_DE_COSTOS =
        "Formatos calculo costos para Convenios2 2010.xls";
    public static final String FORMATOS_ESTUDIO_DE_COSTOS_FTP =
        "Formatos calculo costos para Convenios2 2010 Ajustados Junio-2012.xls";

    /**
     * Variable utilizada para suspender tramites cuando no se tiene definida una fecha.
     */
    public static final int ANIO_SUSPENSION = 20000;

    /**
     * Tabla de condiciones de propiedad predominantes.
     */
    public static final String[][] MATRIZ_CONDICION_PROPIEDAD_PREDOMINANTE = {
        {"0", "0", "3", "4", "5", null, "7", "8", "9"},
        {"0", "2", null, null, null, null, null, null, null},
        {"3", null, "3", "?", null, null, null, "?", "?"},
        {"4", null, "?", "4", null, null, "4", "4", null},
        {"5", null, null, null, null, null, null, null, null},
        {null, null, null, null, null, null, null, null, null},
        {"7", null, null, "4", null, null, "7", null, null},
        {"8", null, "?", "4", null, null, null, null, null},
        {"9", null, "?", null, null, null, null, null, null}};

    /**
     * Valores para el tratamiento de un destinatario en un documento
     */
    public static final String[] TRATAMIENTOS_DESTINATARIOS = {"Doctor(a)", "Señor(a)",
        "Ingeniero(a)", "Dr(a)", "Sr(a)", "Ing."};

    public static final String NULL_TEXT = "null";

//TODO :: fredy.wilches :: 27-04-12 :: Esta cantidad de folios debe mapearse
    // desde el archivo cargado, no todos los valores por defecto se están
    // mapeando desde esta constate se debe actualizar :: juan.agudelo
    public static final Integer CANTIDAD_FOLIOS_POR_DEFECTO = 1;

    public static final String REPORTE_DILIGENCIA_NOTIFICACION_NOMBRE_POR_DEFECTO =
        "diligencia_notificacion.pdf";

    public static final int TAMANO_MOTIVO_TRAMITE_ESTADO = 2000;

    /**
     * Cadena que separa los archivos de texto planos de bloqueo masivo
     */
    public static final String CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO = ";";

    /**
     * Cadena que separa los campos en una rchivod e cargue de predios para avaluos (Alt 190)
     */
    public static final String CADENA_SEPARADOR_ARCHIVO_CARGUE_PREDIOS_AVALUOS = "|";

    /**
     * Cadena que indica que la persona para la que se genero el documento de notificacion
     */
    public static final String CADENA_DOCUMENTO_NOTIFICACION_GENERADO = "NOTIFICADO:";

    /**
     * Cadena que indica que la persona es un autorizado en la notificacion
     */
    public static final String CADENA_DOCUMENTO_NOTIFICACION_AUTORIZADO = "AUTORIZADO:";

    /**
     * Valor por defecto para la relación del solicitante en la solicitud, en la radicación de
     * trámites
     */
    public static final String RELACION_DEFECTO = "PROPIETARIO";

    /**
     * Valor por defecto para la fuente en la radicación de trámites
     */
    public static final String FUENTE_DEFECTO = "P";

    /**
     * Valor por defecto para el tipo de predio = PARTICULAR
     */
    public static final String TIPO_PREDIO_DEFECTO = "P";

    /**
     * Tipo de justificación por defecto
     */
    public static final String TIPO_JUSTIFICACION_DEFECTO = "DERECHO";

    /**
     * Modo Adquisicion Tradicion usado en la justificacion de propiedad Ejecucion
     */
    public static final String MODO_ADQUISICION_TRADICION = "Tradición";

    /**
     * Modo Adquisicion Ocupacion usado en la justificacion de propiedad Ejecucion
     */
    public static final String MODO_ADQUISICION_OCUPACION = "Ocupación";

    /**
     * Modo Adquisicion Sucesion usado en la justificacion de propiedad Ejecucion
     */
    public static final String MODO_ADQUISICION_SUCESION = "Sucesión";

    /**
     * Modo Adquisicion Prescripcion usado en la justificacion de propiedad Ejecucion
     */
    public static final String MODO_ADQUISICION_PRESCRIPCION = "Prescripción";

    /**
     * Modo Adquisicion Accesion usado en la justificacion de propiedad Ejecucion
     */
    public static final String MODO_ADQUISICION_ACCESION = "Accesión";

    /**
     * Si a un combo de selección se le deja "" o null como valor, no se da cuenta del cambio de
     * selección; por eso se usa esta constante como valor por defecto de los combos si se necesita
     * que al escoger la opción de no selección por defecto ("Selecionar ...") se haga algo (que
     * vaya a un listener u otra cosa)
     */
    public static final String VALOR_DUMMY_COMBOS = "_";

    /**
     * Constante de condición verdadera.
     */
    public static final String CONDICIONAL_VERDADERA = " 1=1 ";

    /**
     * Constante usada en el caso de uso Registrar decreto de incremento anual que se muestra en
     * caso de que exista una condicional 1=1 que contempla todos los predios nacionales.
     */
    public static final String CONDICIONAL_NACIONAL =
        "Condicional que contempla todos los predios nacionales.";

    //public static final VALOR
    /**
     * Constante usada cuando se requiere almacenar algún valor como código de territorial para la
     * sede central
     *
     * @author christian.rodriguez
     */
    public static final String CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL = "1000";
    public static final String NOMBRE_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL = "SEDE CENTRAL IGAC";

    /**
     * Nombre equivalente de la sede central para el ldap
     *
     * @author christian.rodriguez
     */
    public static final String NOMBRE_LDAP_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL = "SUB_CATASTRO";

    /**
     * Constante para indicar que la información de una solicitud es de tipo temporal (Usada en
     * Avaluos Comerciales)
     *
     * @author rodrigo.hernandez
     */
    public static final int AC_TIPO_INFORMACION_SOLICITUD_TEMPORAL = 1;

    /**
     * Constante para indicar que la información de una solicitud es de tipo definitiva (Usada en
     * Avaluos Comerciales)
     *
     * @author rodrigo.hernandez
     */
    public static final int AC_TIPO_INFORMACION_SOLICITUD_DEFINITIVA = 2;

    /**
     * Constante para indicar que la información de una solicitud no existe (Usada en Avaluos
     * Comerciales)
     *
     * @author rodrigo.hernandez
     */
    public static final int AC_TIPO_INFORMACION_SOLICITUD_NO_EXISTENTE = 3;

    /**
     * Constantes para definir los años minimo y maximo para el selector de años
     *
     * @author felipe.cadena
     */
    public static final int ANIO_MINIMO_SEL = 1980;
    public static final int ANIO_MAXIMO_SEL = 2020;
    public static final int ANIO_ACTUAL = 2012;

    /**
     * Constante para tipo de justificación de cancelacion OTRA JUSTIFICACION
     */
    public static final String OTRA_JUSTIFICACION = "OTRA JUSTIFICACION";

    /**
     * Constantes de formatos de fechas
     */
    public static final SimpleDateFormat FORMAT_FECHA_DILIGENCIA_NOTIFICACION_PERSONAL =
        new SimpleDateFormat(Constantes.FORMATO_FECHA_DILIGENCIA_NOTIFICACION, new Locale("ES",
            "es_CO"));
    public static final SimpleDateFormat FORMAT_FECHA_RESOLUCION =
        new SimpleDateFormat(Constantes.FORMATO_FECHA, new Locale("ES", "es_CO"));
    public static final SimpleDateFormat FORMAT_FECHA_RESOLUCION_2 =
        new SimpleDateFormat(Constantes.FORMATO_FECHA_RESOLUCION, new Locale("ES", "es_CO"));

    /**
     * Milisegundos al dia
     */
    public static final Long MILISEG_POR_DIA = 86400000L;

    /**
     * zona horaria para usar en los componentes jsf
     */
    public static final String ZONA_GMT = "GMT-5";

    /**
     * Tipo de persona defecto para IPER
     */
    public static final String TIPO_PERSONA_IPER = "JURIDICA";
    public static final String TIPO_PERSONA_JURIDICA = "JURIDICA";

    /**
     * Tipo de identificacion defecto para IPER
     */
    //public static final String tipoIdentificacionIper="X";
    /**
     * Nombres de las imagenes de los colores para el semáforo de trámites
     */
    public static final String SEMAFORO_EN_ROJO = "boton-rojo.png";
    public static final String SEMAFORO_EN_VERDE = "boton-verde.png";
    public static final String SEMAFORO_EN_AMARILLO = "boton-amarillo.png";

    /**
     * Para setear estado_gdb cuando debe obligarse ir a lo geográfico
     */
    public static final String ACT_MODIFICAR_INFORMACION_GEOGRAFICA =
        "ModificarInformacionGeografica";

    /**
     * Para setear estado_gdb cuando está en Aplicar cambios asincrónico
     */
    public static final String ACT_APLICAR_CAMBIOS = "AplicarCambios";

    /**
     * Determina el valor máximo de caracteres a insertar en el campo mensaje de la tabla
     * LOG_MENSAJE
     *
     * @author javier.aponte
     */
    public static final int TAMANIO_MENSAJE_LOG_MENSAJE = 3980;

    /**
     * Mensaje que aparece cuando no se puede generar el reporte
     *
     * @author javier.aponte
     */
    public static final String MENSAJE_ERROR_REPORTE = "Ocurrió un error al generar el reporte, " +
        "por favor intente más tarde o comuníquese con el administrador";

    /**
     * Utilizada en radicacion para obligar este tipo de documento cuando solicitan mediante
     * apoderado
     */
    public static final Long TIPO_DOCUMENTO_OBLIGATORIO_PARA_APODERADO = 104L;

    /**
     * Utilizada en radicacion para obligar este tipo de documento cuando solicitan mediante
     * representante legal
     */
    public static final Long TIPO_DOCUMENTO_OBLIGATORIO_PARA_REPRESENTANTE_LEGAL = 15L;

    /**
     * cadena que se usa para separar las dos partes del nombre de un archivo al que se le ha
     * antepuesto una cadena para identificarlo como temporal. Ej: nombre archivo original =
     * cosa.jpg nombre archivo temporal = xxx[este separador]cosa.jpg
     */
    public static final String SEPARADOR_NOMBRE_TEMPORAL_ARCHIVO_TEMPORAL = "_-t-_";

    // nit la nacion
    public static final String NIT_LA_NACION = "999";
    public static final String RAZON_SOCIAL_LA_NACION = "LA NACION";

    /**
     * Números utilizados para calcular el digito de verificación
     */
    public static final int[] NUMEROS_DIGITO_VERIFICACION = {3, 7, 13, 17, 19, 23, 29, 37, 41, 43,
        47, 53, 59, 67, 71};

    /**
     * Funcionario ejecutor IPER
     *
     * @author juanfelipe.garcia
     */
    public static final String FUNCIONARIO_EJECUTOR_IPER = "Ejecutor IPER";

    /**
     * separador con que las funciones de oracle retornan la concatenación de campos (ej: V_Comision
     * tiene el campo 'ejecutor' que es la concatenación de los ids de los ejecutores de los
     * trámites) Definida por julio.mendoza
     *
     * @autor pedro.garcia
     */
    public static final String SEPARADOR_CAMPOS_CONCATENADOS_BD = ",";

    /**
     * Utilizada en validar geometría de predios temporal para guardar el estado de correcto en la
     * columna INCONSISTENCIAS de la tabla TramiteDepuracion
     *
     * @author javier.aponte
     *
     * @deprecated usar {@link EInconsistenciasGeograficas#NO_INCONSISTENCIA} o
     * {@link EPredioInconsistenciaTipo#NO_INCONSISTENCIA}
     */
    @Deprecated
    public static final String NO_HAY_INCONSISTENCIAS_GEOGRAFICAS =
        "No se encuentran inconsistencias geográficas";

    /**
     * Constante utilizada como equivalente del nombre de la actividad Registrar notificación para
     * el menú del caso de uso registrar notificación.
     *
     * @author david.cifuentes
     */
    public static final String POR_NOTIFICAR = "Por notificar";

    /**
     * Constante utilizada como equivalente del nombre de la actividad Radicar recurso para el menú
     * del caso de uso registrar notificación.
     *
     * @author david.cifuentes
     */
    public static final String RENUNCIA_RECURSO = "Renuncia recurso";

    /**
     * Prefijo de los ids que se les da a los documentos en el gestor documental.
     *
     * @author pedro.garcia
     */
    public static final String PREFIJO_IDS_DOCUMENTOS_REPOSITORIO_DOCS = "Workspace/";

    /**
     * Extensión de la imagen de la ficha predial digital
     *
     * @author javier.aponte
     */
    public static final String CARACTER_EXTENSION_FICHA_PREDIAL = "\\.";

    /**
     * Extensión de la imagen de la ficha predial digital
     *
     * @author javier.aponte
     */
    public static final String INCONSISTENCIA_GEOGRAFICA_PRIORITARIA = "IGP";

    /**
     * texto que viene contenido en la excepción que lanza el método que valida la geometría de los
     * predios y que marca los códigos de error que se generaron allí
     *
     * @author pedro.garcia
     */
    public static final String TEXTO_IDENTIFICADOR_CODIGOS_ERROR_INCONSISTENCIA =
        "CODIGO_VALIDACION_SIG:";

    /**
     * texto que separa los diferentes códigos de error que vienen contenidos en la excepción que
     * lanza el método que valida la geometría de los predios
     *
     * @author pedro.garcia
     */
    public static final String TEXTO_SEPARADOR_CODIGOS_ERROR_INCONSISTENCIA = ",";

    /**
     * posiciones en el número predial hasta donde se busca por determinado dato. </>
     * EJ: para manzana se toma hasta la posición 17
     */
    public static int NUMPREDIAL_MANZANA_VEREDA_INDICE = 17;
    public static int NUMPREDIAL_PREDIO_INDICE = 21;

    /**
     * Dominio de LDAP
     */
    public static final String LDAP_DC = SNCPropertiesUtil.getInstance().getProperty("dn");
    //public static final String LDAP_DC = "DC=PRUDCIGAC,DC=LOCAL";

    /**
     * Constante para usuarios log migrados
     */
    public static final String USUARIO_LOG_MIGRADOS = "MIG";

    /**
     * Constante para usuarios log migrados en el 2015
     */
    public static final String USUARIO_LOG_MIGRADOS_2015 = "MIG2015";

    /**
     * Variable para generar el producto en pdf
     */
    public static final String GENERAR_PRODUCTO_EN_PDF = "pdf";

    /**
     * Variable para generar el producto en un formato diferente de pdf
     */
    public static final String NO_GENERAR_PRODUCTO_EN_PDF = "nopdf";

//-------------------------------------
    /**
     * Variable para validar que el número de identificación en productos no sea 0
     */
    public static final String CERO = "0";

    /**
     * Constante que se utiliza para verificar si una actividad del process es de menu
     *
     * @author javier.aponte
     */
    public static final String ACTIVIDAD_PROCESO_MENU = "MENU";

    /**
     * Constante que se utiliza para determinar el nombre del subproceso de depuracion
     *
     * @author felipe.cadena
     */
    public static final String DEPURACION = "Depuracion";

    /**
     * Constante determina si un predio esta habilitado para un tramite en particular
     *
     * @author felipe.cadena
     */
    public static final String HABILITADO = "H";

    /**
     * Constante determina si un predio es condicion de propiedad Ficha Matriz
     *
     * @author felipe.cadena
     */
    public static final String FM = "F";

    /**
     * Constante que representa la cadena final de una ficha matriz condominio
     *
     * @author felipe.cadena
     */
    public static final String FM_CONDOMINIO_END = "800000000";

    /**
     * Constante que representa la cadena final de una ficha matriz PH
     *
     * @author felipe.cadena
     */
    public static final String FM_PH_END = "900000000";

    /**
     * Constante que representa el nombre la tabla de direcciones de predios proyectados
     *
     * @author felipe.cadena
     */
    public static final String TABLA_DIRECCION_PROYECTADA = "P_PREDIO_DIRECCION";

    /**
     * Constante que representa el nombre la tabla de unidades de proyección de predios proyectados
     *
     * @author javier.aponte
     */
    public static final String TABLA_UNIDAD_CONSTRUCCION_PROYECTADA = "P_UNIDAD_CONSTRUCCION";

    /**
     * Texto de las observaciones para la notificacion de aviso con direccion
     *
     * @author felipe.cadena
     */
    public static final String NOTIFICADO_CON_DIRECCION = "Notificado por aviso con dirección";

    /**
     * Tipo documento solicitente actualizacion
     *
     * @author felipe.cadena
     */
    public static final String TIPO_DOC_SOLICITANTE_ACTUALIZACION = "NIT-IGAC";

    /**
     * Numero documento solicitente actualizacion
     *
     * @author felipe.cadena
     */
    public static final String NUMERO_DOC_SOLICITANTE_ACTUALIZACION = "899999004";

    /**
     * Asunto radicaciones de actualizacion
     *
     * @author felipe.cadena
     */
    public static final String TEXTO_ASUNTO_SOLICITUD_ACT = "Trámites de actualización";

    /**
     * Asunto radicaciones de actualizacion
     *
     * @author felipe.cadena
     */
    public static final int NUMERO_COLUMNAS_REGLAS = 6;
    public static final int NUMERO_COLUMNAS_TABLAS = 6;

    /**
     * Constante de error que se agrega al objeto de la clase RequestContext cuando se usa el método
     * addCallbackParam
     *
     * @javier.aponte
     */
    public static final String ERROR_REQUEST_CONTEXT = "error";

    /**
     * Tamaño máximo del motivo de cancelación del trámite
     *
     * @javier.aponte
     */
    public static final int TAMANIO_MAXIMO_MOTIVO_CANCELACION = 250;

    /**
     * Mensaje que aparece cuando no se genera un error para un predio particular.
     *
     * @author javier.aponte
     */
    public static final String NO_HAY_RESULTADOS_ENTIDADES =
        "La consulta de entidades no devolvió resultados";

    /**
     * Texto que aparece al usuario en la actividad de cancelar trámites
     *
     * @author javier.aponte
     */
    public static final String TRAMITE_CON_NUMERO_RADICACION =
        "El Trámite con número de radicación: ";

    /**
     * Texto que aparece cuando el usuario ingresa a modificar información alfanumérica y el predio
     * está proyectado en un trámite asociado al trámite original
     *
     * @author javier.aponte
     */
    public static final String PREDIO_EN_TRAMITE_ASOCIADO =
        ", no se puede tramitar se requiere que el trámite asociado finalice para continuar";

    /**
     * Constantes menejo de entidades delegadas
     */
    public static final String DN_LDAP_DELEGADOS = "OU=EXTERNOS";
    public static final String PREFIJO_DELEGADOS = "DLG_";
    public static final String PREFIJO_HABILITADOS = "HAB_";

    /**
     * Textos para el envio de correos cuando se trata avanzar un tramite que ya se reclamo por otro
     * usuario.
     */
    public static final String TEXTO_AUTOESTIMACION_TRAMITES_RECLAMADOS =
        "Las siguientes solicitudes fueron avanzadas previamente por otro usuario: ";
    public static final String TEXTO_OTROS_TRAMITES_RECLAMADOS =
        "Los siguientes trámites fueron avanzados previamente por otro usuario:";

    /**
     * Mensaje que aparece cuando no se genera un error para un predio particular.
     *
     * @author javier.aponte
     */
    public static final String MENSAJE_ERROR_PREDIO =
        "El predio con número predial {0}, presenta el siguiente error: {1}";
    public static final String BOGOTA_CODIGO = "11";

    public static final int USO_CONSTRUCCION_CIMIENTOS = 99;

    /**
     * Constante Codigo Reporte Prediales
     *
     * @jleidy.gonzalez
     */
    public static final String CODIGO_REPORTES_PREDIALES = "REP-PRED";

    /**
     * Constante Campos Opcionales Reportes
     *
     * @jleidy.gonzalez
     */
    public static final String OPCIONAL = "OP";

    /**
     * Constante Campo generacion Reporte NOCTURNO
     *
     * @jleidy.gonzalez
     */
    public static final String NOCTURNO = "NOCTURNO";

    /**
     * Constante Campo generacion Reporte DIURNO
     *
     * @jleidy.gonzalez
     */
    public static final String DIURNO = "DIURNO";

    /**
     * Constante Campos Obligatorios Reportes
     *
     * @jleidy.gonzalez
     */
    public static final String OBLIGATORIO = "O";

    /**
     * Constante para consultar historico en el editor geográfico
     */
    public static final String CONSULTA_HISTORICO = "CONSULTA_HISTORICO";

    /**
     * Constante para denotar un reporte (RepReporteEjecucion) como vacio, sin datos
     */
    public static final String REPORTE_VACIO = "S";

    /**
     * Constante para definir el formato de área de construcción
     */
    public static final String FORMATO_AREA_CONSTRUCCION = "######.####";
}
