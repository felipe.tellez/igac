package co.gov.igac.snc.persistence.util;

/**
 * Enumeraciòn que contiene la estructura del archivo de cargue de detalles del predio se usa en la
 * carga de Zonas, Construcciones, Cultivos, Anexos y Maquinaria
 *
 * @author felipe.cadena
 */
public enum EArchivoCargueDetallesAvaluo {
    //generales
    DEPARTAMENTO("Departamento", 0, 2),
    MUNICIPIO("Municipio", 1, 3),
    NUMERO_PREDIAL("Número Predial", 2, 30),
    //avaluo_predio_zona
    DESCRIPCION_ZONA("Descripcion zona", 3, 50),
    AREA("Área en metros cuadrados", 4, 30),
    VALOR_UNITARIO_ZONA("Valor unitario", 5, 30),
    //avaluo_predio_cultivo
    TIPO_CULTIVO("Tipo", 3, 50),
    EDAD_CULTIVO("Edad", 4, 30),
    DESCRIPCION_CULTIVO("Descripción", 5, 100),
    ASPECTO_TECNICO_CULTIVO("Aspecto técnico", 6, 30),
    MEDIDA_CULTIVO("Medida / Cantidad", 7, 30),
    UNIDAD_MEDIDA_CULTIVO("Unidad medida", 8, 30),
    VALOR_UNIDAD_CULTIVO("Valor unidad", 9, 30),
    //avaluo_predio_maquinaria
    DESCRIPCION_MAQUINARIA("Descripción", 3, 50),
    ESPECIFICACION_MAQUINARIA("Especificación", 4, 30),
    CANTIDAD_MAQUINARIA("Cantidad", 5, 100),
    VALOR_UNIDAD_MAQUINARIA("Valor unidad", 6, 30),
    //avaluo_predio_anexo
    TIPO_ANEXO("Descripción", 3, 50),
    DECRIPCION_ANEXO("Especificación", 4, 30),
    FUENTE_ANEXO("Fuente", 5, 100),
    UNIDAD_MEDIDA_ANEXO("Unidad medida", 6, 100),
    MEDIDA_ANEXO("Medida", 7, 100),
    VALOR_UNIDAD_ANEXO("Valor unitario", 8, 30),
    //avaluo_predio
    TIPO_AVALUO_PREDIO("Tipo avalúo", 2, 10),
    NUMERO_PREDIAL_PREDIO("Número Predial", 3, 30),
    MATRICULA_ANTIGUA_PREDIO("Matricula antigua", 4, 1),
    CIRCULO_REGISTRAL_PREDIO("Circulo registral", 5, 3),
    NUMERO_MATRICULA_PREDIO("Número matricula", 6, 12),
    DIRECCION_PREDIO("Dirección", 7, 300),
    DESTINO_PREDIO("Destino", 8, 100),
    TIPO_INMUEBLE_PREDIO("Tipo inmueble", 9, 100);

    private String descripcion;
    private int columna;
    private int longitud;

    private EArchivoCargueDetallesAvaluo(String descripcion, int columna, int longitud) {
        this.descripcion = descripcion;
        this.columna = columna;
        this.longitud = longitud;
    }

    /**
     * Corresponde al nombre de la columna
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Corresponde al tamanio maximo de la columna
     */
    public int getLongitud() {
        return longitud;
    }

    /**
     * Corresponde al contenido de la columna
     */
    public int getColumna() {
        return columna;
    }

}
