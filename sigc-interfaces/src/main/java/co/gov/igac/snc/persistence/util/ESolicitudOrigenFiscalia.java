package co.gov.igac.snc.persistence.util;

/**
 * Enumeración que representa los datos contenidos en el Dominio SOLICITUD_ORIGEN_FISCALIA
 *
 * Datos de posibles tipos de solicitud segun el tipo de solicitante FISCALIA
 *
 * @author rodrigo.hernandez
 *
 */
public enum ESolicitudOrigenFiscalia {

    ADMINISTRATIVO("FA", "Administrativo"),
    INVESTIGACION("FI", "Investigación");

    private String codigo;
    private String valor;

    private ESolicitudOrigenFiscalia(String codigo, String valor) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
