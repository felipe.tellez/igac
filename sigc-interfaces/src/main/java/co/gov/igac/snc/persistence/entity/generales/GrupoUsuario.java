package co.gov.igac.snc.persistence.entity.generales;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * GrupoUsuario entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "GRUPO_USUARIO", schema = "SNC_GENERALES", uniqueConstraints = @UniqueConstraint(
    columnNames = {
        "USUARIO_ID", "GRUPO_ID"}))
public class GrupoUsuario implements java.io.Serializable {

    // Fields
    private Long id;
    private Usuario usuario;
    private Grupo grupo;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public GrupoUsuario() {
    }

    /** full constructor */
    public GrupoUsuario(Long id, Usuario usuario, Grupo grupo,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.usuario = usuario;
        this.grupo = grupo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USUARIO_ID", nullable = false)
    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GRUPO_ID", nullable = false)
    public Grupo getGrupo() {
        return this.grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
