package co.gov.igac.snc.persistence.util;

/**
 * Determina la relacion para la tabla Solicitante al momento de notificar los involucrados en un
 * tramite masivo
 *
 * @author felipe.cadena
 */
public enum ESolicitanteRelacionNoti {

    AFECTADO("Afectado"),
    PROPIETARIO("Propietario");

    private final String valor;

    ESolicitanteRelacionNoti(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return this.valor;
    }

}
