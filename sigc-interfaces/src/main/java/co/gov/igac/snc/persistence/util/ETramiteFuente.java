/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles estados de la fuente de un trámite
 *
 * @author juan.agudelo
 */
public enum ETramiteFuente {

    DE_OFICIO("O"),
    DE_PARTE("P");

    private String codigo;

    private ETramiteFuente(String codigoP) {
        this.codigo = codigoP;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
