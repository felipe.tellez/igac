package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the DOTACION database table.
 *
 * @author franz.gamba
 */
@Entity
public class Dotacion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 9130649528558877551L;
    private Long id;
    private String descripcion;
    private Date fecha;
    private Date fechaLog;
    private Long soporteDocumentoId;
    private String usuarioLog;
    private Actualizacion actualizacion;
    private RecursoHumano recursoHumano;

    public Dotacion() {
    }

    @Id
    @SequenceGenerator(name = "DOTACION_ID_GENERATOR",
        sequenceName = "DOTACION_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOTACION_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "SOPORTE_DOCUMENTO_ID", precision = 10)
    public Long getSoporteDocumentoId() {
        return this.soporteDocumentoId;
    }

    public void setSoporteDocumentoId(Long soporteDocumentoId) {
        this.soporteDocumentoId = soporteDocumentoId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    //bi-directional many-to-one association to RecursoHumano
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECURSO_HUMANO_ID", nullable = false)
    public RecursoHumano getRecursoHumano() {
        return this.recursoHumano;
    }

    public void setRecursoHumano(RecursoHumano recursoHumano) {
        this.recursoHumano = recursoHumano;
    }

}
