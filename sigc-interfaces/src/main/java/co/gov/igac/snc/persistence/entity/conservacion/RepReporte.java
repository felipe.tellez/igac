package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.snc.util.Constantes;
import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the REP_REPORTE database table.
 *
 */
@Entity
@Table(name = "REP_REPORTE")
public class RepReporte implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String categoria;
    private String consultaGrafica1;
    private String consultaGrafica2;
    private String consultaGrafica3;
    private String consultaResultados;
    private String descripcion;
    private String ejecucionNombrePrograma;
    private String ejecucionTipoPrograma;
    private String enLinea;
    private Date fechaLog;
    private String nombre;
    private String usuarioLog;
    private List<RepReporteEjecucion> repReporteEjecucions;
    private String urlReporteJasper;
    private String urlPrevisualizacion;
    private String tipoReporte;
    private String tipoInforme;
    private List<RepUsuarioRolReporte> repUsuarioRolReportes;

    public RepReporte() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REP_REPORTE_ID_SEQ")
    @SequenceGenerator(name = "REP_REPORTE_ID_SEQ", sequenceName = "REP_REPORTE_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoria() {
        return this.categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Column(name = "CONSULTA_GRAFICA_1")
    public String getConsultaGrafica1() {
        return this.consultaGrafica1;
    }

    public void setConsultaGrafica1(String consultaGrafica1) {
        this.consultaGrafica1 = consultaGrafica1;
    }

    @Column(name = "CONSULTA_GRAFICA_2")
    public String getConsultaGrafica2() {
        return this.consultaGrafica2;
    }

    public void setConsultaGrafica2(String consultaGrafica2) {
        this.consultaGrafica2 = consultaGrafica2;
    }

    @Column(name = "CONSULTA_GRAFICA_3")
    public String getConsultaGrafica3() {
        return this.consultaGrafica3;
    }

    public void setConsultaGrafica3(String consultaGrafica3) {
        this.consultaGrafica3 = consultaGrafica3;
    }

    @Column(name = "CONSULTA_RESULTADOS")
    public String getConsultaResultados() {
        return this.consultaResultados;
    }

    public void setConsultaResultados(String consultaResultados) {
        this.consultaResultados = consultaResultados;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "EJECUCION_NOMBRE_PROGRAMA")
    public String getEjecucionNombrePrograma() {
        return this.ejecucionNombrePrograma;
    }

    public void setEjecucionNombrePrograma(String ejecucionNombrePrograma) {
        this.ejecucionNombrePrograma = ejecucionNombrePrograma;
    }

    @Column(name = "EJECUCION_TIPO_PROGRAMA")
    public String getEjecucionTipoPrograma() {
        return this.ejecucionTipoPrograma;
    }

    public void setEjecucionTipoPrograma(String ejecucionTipoPrograma) {
        this.ejecucionTipoPrograma = ejecucionTipoPrograma;
    }

    @Column(name = "EN_LINEA")
    public String getEnLinea() {
        return this.enLinea;
    }

    public void setEnLinea(String enLinea) {
        this.enLinea = enLinea;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to RepReporteEjecucion
    @OneToMany(mappedBy = "repReporte")
    public List<RepReporteEjecucion> getRepReporteEjecucions() {
        return this.repReporteEjecucions;
    }

    public void setRepReporteEjecucions(List<RepReporteEjecucion> repReporteEjecucions) {
        this.repReporteEjecucions = repReporteEjecucions;
    }

    @Column(name = "URL_REPORTE_JASPER")
    public String getUrlReporteJasper() {
        return urlReporteJasper;
    }

    public void setUrlReporteJasper(String urlReporteJasper) {
        this.urlReporteJasper = urlReporteJasper;
    }

    @Column(name = "URL_PREVISUALIZACION")
    public String getUrlPrevisualizacion() {
        return urlPrevisualizacion;
    }

    public void setUrlPrevisualizacion(String urlPrevisualizacion) {
        this.urlPrevisualizacion = urlPrevisualizacion;
    }

    @Column(name = "TIPO_REPORTE")
    public String getTipoReporte() {
        return tipoReporte;
    }

    public void setTipoReporte(String tipoReporte) {
        this.tipoReporte = tipoReporte;
    }

    @Column(name = "TIPO_INFORME")
    public String getTipoInforme() {
        return tipoInforme;
    }

    public void setTipoInforme(String tipoInforme) {
        this.tipoInforme = tipoInforme;
    }

    //bi-directional many-to-one association to RepReporteEjecucion
    @OneToMany(mappedBy = "repReporte")
    public List<RepUsuarioRolReporte> getRepUsuarioRolReportes() {
        return this.repUsuarioRolReportes;
    }

    public void setRepUsuarioRolReportes(List<RepUsuarioRolReporte> repUsuarioRolReportes) {
        this.repUsuarioRolReportes = repUsuarioRolReportes;
    }

    /**
     * Retorna este objeto como un string. No tiene en cuenta los atributos que son listas de otros
     * objetos, ni los campos de log
     *
     * @author david.cifuentes
     */
    @Override
    public String toString() {

        StringBuilder objectAsString;
        String stringSeparator = Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR;

        objectAsString = new StringBuilder();
        objectAsString.append((this.id != null) ? this.id : "").append(stringSeparator);
        objectAsString.append((this.categoria != null) ? this.categoria : "").
            append(stringSeparator);
        objectAsString.append((this.nombre != null) ? this.nombre : "").append(stringSeparator);
        objectAsString.append((this.descripcion != null) ? this.descripcion : "").append(
            stringSeparator);

        return objectAsString.toString();
    }

}
