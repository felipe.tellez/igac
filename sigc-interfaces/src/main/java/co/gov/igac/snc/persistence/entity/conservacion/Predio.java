package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.ETipoAvaluo;
import co.gov.igac.snc.util.Utilidades;
import java.util.Calendar;
import org.apache.commons.lang.time.DateUtils;

/*
 * MODIFICACIONES PROPIAS A LA CLASE Predio: -> Se agregó el serialVersionUID. -> Se agregó el Named
 * Query: findPredioByNumeroPredial -> Se agregó el método Transient getFormatedNumeroPredial para
 * obtener el número predial con el formato requerido en la GUI. -> Se modificó el tipo del campo
 * personaPredios de Set a List. -> Se modificó el tipo del campo predioBloqueos de Set a List. ->
 * Se modificó el tipo del campo predioAvaluoCatastrals de Set a List. -> Se modificó el tipo del
 * campo predioZonas de Set a List. -> Se modificó el tipo del campo predioDireccions de Set a List.
 * -> Se modificó el tipo del campo predioServidumbres de Set a List. -> Se modificó el tipo del
 * campo referenciaCartograficas de Set a List. -> Se modificó el tipo del campo
 * predioInconsistencias de Set a List. -> Se modificó la variable de departamento para convertirla
 * en un objeto de tipo departamento. -> Se modificó la variable de municipio para convertirla en un
 * objeto de tipo municipio. -> Se agregó el campo Transient estadoValor para muestra en interfaz
 * gráfica -> Se agregó el campo Transient tipoValor para muestra en interfaz gráfica -> Se agregó
 * el campo Transient destinoValor para muestra en interfaz gráfica @modified by fabio.navarrete ->
 * Se agregó el método Transient getTotalUnidades -> Se agregó el método Transient
 * getUnidadesConstruccionNoConvencional -> Se agregó el método Transient
 * getUnidadesConstruccionConvencional -> Se agregó el método Transient
 * getAreaTotalConsNoConvencional -> Se agregó el método Transient getAreaTotalConsConvencional ->
 * Se agregó el método Transient getAreaTotalConstrucción -> Se agregó el método Transient
 * getAreaTotalTerreno -> Se agregó el método Transient getTotalUnidadesConstruccion -> Se agregó el
 * método Transient getDerechosPropiedad -> Se agrega getter transiente isPredioUrbano -> Se agregan
 * getter y stter Transient para Ley14 -> Se agrega propiedad transient imprimirFichaPredial. -> Se
 * agrega propiedad transient imprimirCartaCatastral.
 *
 * @modified by pedro.garcia what - adición de anotaciones para la secuencia del id - adición método
 * getTipoAvaluoPredio - adición método isEstaBloqueado
 *
 * @modified juan.agudelo 06-10-11 Se agregaron los métodos transient para ley14 y numero predial
 * formateado juan.agudelo 19-10-11 Se agregaron los métodos transient
 * avaluoUnidadesConstruccionConvencional y avaluoUnidadesConstruccionNoConvencional
 *
 * @modified david.cifuentes 31-10-11 Se agregó el atributo Date fechaRegistro, y tambien sus
 * getters y setters.
 */

 /*
 * Predio entity. @author MyEclipse Persistence Tools
 *
 * @modified juan.agudelo 27-01-2012 Adición del método transient esPredioEnPH que consulta si el
 * predio se encuentra en PH o Condominio 08-05-2012 Adición del atributo transient
 * numeroPredialAsociadoPadre 31-05-2012 Adición del atributo transient coeficienteCopropiedad
 *
 * @modified pedro.garcia 24-04-2012 cambio del atributo circuloRegistral para que sea del tipo
 * CirculoRegistral (no String) 02-12-2013 adición de método isCondicionPropiedadRara
 *
 * @modified pedro.garcia 04-09-2012 Adición de métodos transient getMatriculaInmobiliaria,
 * isEsMatriculaAntigua, getPredioTipoAvaluoValor
 *
 * @modified franz.gamba 24-09-2012 se agrego el fetch de departamento y municipio para los predios
 * en el namedQuery
 *
 * @modified :: david.cifuentes :: Adición del método transient isEsPredioFichaMatriz :: 13/07/2016
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "findPredioByNumeroPredial",
        query =
        "from Predio p join fetch p.departamento join fetch p.municipio where p.numeroPredial = :numeroPredial")})
@Table(name = "PREDIO", schema = "SNC_CONSERVACION")
public class Predio implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3118635164693737896L;

    // Fields
    private Long id;
    private Departamento departamento;
    private Municipio municipio;
    private String tipoAvaluo;
    private String sectorCodigo;
    private String barrioCodigo;
    private String manzanaCodigo;
    private String predio;
    private String condicionPropiedad;
    private String edificio;
    private String piso;
    private String unidad;
    private String numeroPredial;
    private String numeroPredialAnterior;
    private String nip;
    private BigDecimal consecutivoCatastral;
    private BigDecimal consecutivoCatastralAnterior;
    private String zonaUnidadOrganica;
    private String localidadCodigo;
    private String corregimientoCodigo;
    private String nombre;
    private String destino;
    private String tipo;
    private Double areaTerreno;
    private CirculoRegistral circuloRegistral;
    private String numeroRegistro;
    private String numeroRegistroAnterior;
    private String tipoCatastro;
    private String este;
    private String norte;
    private String estado;
    private String estrato;
    private String chip;
    private String numeroUltimaResolucion;
    private Short anioUltimaResolucion;
    private String usuarioLog;
    private Date fechaLog;
    private List<PredioAvaluoCatastral> predioAvaluoCatastrals =
        new ArrayList<PredioAvaluoCatastral>();
    private List<PredioDireccion> predioDireccions = new ArrayList<PredioDireccion>();
    private List<ReferenciaCartografica> referenciaCartograficas =
        new ArrayList<ReferenciaCartografica>();
    private List<PredioInconsistencia> predioInconsistencias = new ArrayList<PredioInconsistencia>();
    private List<PersonaPredio> personaPredios = new ArrayList<PersonaPredio>();
    private List<PredioBloqueo> predioBloqueos = new ArrayList<PredioBloqueo>();
    private List<PredioServidumbre> predioServidumbres = new ArrayList<PredioServidumbre>();
    private List<FichaMatriz> fichaMatrizs = new ArrayList<FichaMatriz>();
    private List<PredioZona> predioZonas = new ArrayList<PredioZona>();
    private List<UnidadConstruccion> unidadConstruccions = new ArrayList<UnidadConstruccion>();
    //private Set<CoordenadaNodo> coordenadaNodos = new HashSet<CoordenadaNodo>(0);
    private List<Foto> fotos = new ArrayList<Foto>();

    private String estadoValor;
    private String tipoValor;
    private String destinoValor;
    private String zonaUnidadOrganicaValor;
    private Double areaConstruccion;

    private Date fechaInscripcionCatastral;
    private Date fechaRegistro;

    private boolean imprimirFichaPredial;
    private boolean imprimirCartaCatastral;
    private boolean descargarCapasGeograficas;

    // Transient
    @SuppressWarnings("unused")
    private boolean esPredioEnPH;
    private String numeroPredialAsociadoPadre;
    private Double coeficienteCopropiedad;

    private Double avaluoCatastral;

    // Constructors
    /** default constructor */
    public Predio() {
    }

    /** minimal constructor */
    public Predio(Long id, Departamento departamento, Municipio municipio,
        String tipoAvaluo, String sectorCodigo, String barrioCodigo,
        String manzanaCodigo, String predio, String condicionPropiedad,
        String edificio, String piso, String unidad, String numeroPredial,
        String destino, String tipo, Double areaTerreno,
        String tipoCatastro, String estado, String usuarioLog,
        Date fechaLog, Double areaConstruccion) {
        this.id = id;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoAvaluo = tipoAvaluo;
        this.sectorCodigo = sectorCodigo;
        this.barrioCodigo = barrioCodigo;
        this.manzanaCodigo = manzanaCodigo;
        this.predio = predio;
        this.condicionPropiedad = condicionPropiedad;
        this.edificio = edificio;
        this.piso = piso;
        this.unidad = unidad;
        this.numeroPredial = numeroPredial;
        this.destino = destino;
        this.tipo = tipo;
        this.areaTerreno = areaTerreno;
        this.tipoCatastro = tipoCatastro;
        this.estado = estado;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.areaConstruccion = areaConstruccion;
    }

    /** full constructor */
    public Predio(Long id, Departamento departamento, Municipio municipio,
        String tipoAvaluo, String sectorCodigo, String barrioCodigo,
        String manzanaCodigo, String predio, String condicionPropiedad,
        String edificio, String piso, String unidad, String numeroPredial,
        String numeroPredialAnterior, String nip,
        BigDecimal consecutivoCatastral,
        BigDecimal consecutivoCatastralAnterior, String zonaUnidadOrganica,
        String localidadCodigo, String corregimientoCodigo, String nombre,
        String destino, String tipo, Double areaTerreno,
        CirculoRegistral circuloRegistral, String numeroRegistro,
        String numeroRegistroAnterior, String tipoCatastro, String este,
        String norte, String estado, String estrato, String chip,
        String usuarioLog, Date fechaLog,
        List<PredioAvaluoCatastral> predioAvaluoCatastrals,
        List<PredioDireccion> predioDireccions,
        List<ReferenciaCartografica> referenciaCartograficas,
        List<PredioInconsistencia> predioInconsistencias,
        List<PersonaPredio> personaPredios,
        List<PredioBloqueo> predioBloqueos,
        List<PredioServidumbre> predioServidumbres,
        List<FichaMatriz> fichaMatrizs, List<PredioZona> predioZonas,
        List<UnidadConstruccion> unidadConstruccions,
        List<Foto> fotos,
        Double areaConstruccion, String numeroUltimaResolucion,
        Short anioUltimaResolucion) {
        this.id = id;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoAvaluo = tipoAvaluo;
        this.sectorCodigo = sectorCodigo;
        this.barrioCodigo = barrioCodigo;
        this.manzanaCodigo = manzanaCodigo;
        this.predio = predio;
        this.condicionPropiedad = condicionPropiedad;
        this.edificio = edificio;
        this.piso = piso;
        this.unidad = unidad;
        this.numeroPredial = numeroPredial;
        this.numeroPredialAnterior = numeroPredialAnterior;
        this.nip = nip;
        this.consecutivoCatastral = consecutivoCatastral;
        this.consecutivoCatastralAnterior = consecutivoCatastralAnterior;
        this.zonaUnidadOrganica = zonaUnidadOrganica;
        this.localidadCodigo = localidadCodigo;
        this.corregimientoCodigo = corregimientoCodigo;
        this.nombre = nombre;
        this.destino = destino;
        this.tipo = tipo;
        this.areaTerreno = areaTerreno;
        this.circuloRegistral = circuloRegistral;
        this.numeroRegistro = numeroRegistro;
        this.numeroRegistroAnterior = numeroRegistroAnterior;
        this.tipoCatastro = tipoCatastro;
        this.este = este;
        this.norte = norte;
        this.estado = estado;
        this.estrato = estrato;
        this.chip = chip;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.predioAvaluoCatastrals = predioAvaluoCatastrals;
        this.predioDireccions = predioDireccions;
        this.referenciaCartograficas = referenciaCartograficas;
        this.predioInconsistencias = predioInconsistencias;
        this.personaPredios = personaPredios;
        this.predioBloqueos = predioBloqueos;
        this.predioServidumbres = predioServidumbres;
        this.fichaMatrizs = fichaMatrizs;
        this.predioZonas = predioZonas;
        this.unidadConstruccions = unidadConstruccions;
        this.fotos = fotos;
        this.areaConstruccion = areaConstruccion;
        this.numeroUltimaResolucion = numeroUltimaResolucion;
        this.anioUltimaResolucion = anioUltimaResolucion;

    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Predio_ID_SEQ")
    @SequenceGenerator(name = "Predio_ID_SEQ", sequenceName = "PREDIO_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_AVALUO", nullable = false, length = 2)
    public String getTipoAvaluo() {
        return this.tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    @Column(name = "SECTOR_CODIGO", nullable = false, length = 2)
    public String getSectorCodigo() {
        return this.sectorCodigo;
    }

    public void setSectorCodigo(String sectorCodigo) {
        this.sectorCodigo = sectorCodigo;
    }

    @Column(name = "BARRIO_CODIGO", nullable = false, length = 4)
    public String getBarrioCodigo() {
        return this.barrioCodigo;
    }

    public void setBarrioCodigo(String barrioCodigo) {
        this.barrioCodigo = barrioCodigo;
    }

    @Column(name = "MANZANA_CODIGO", nullable = false, length = 4)
    public String getManzanaCodigo() {
        return this.manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    @Column(name = "PREDIO", nullable = false, length = 4)
    public String getPredio() {
        return this.predio;
    }

    public void setPredio(String predio) {
        this.predio = predio;
    }

    @Column(name = "CONDICION_PROPIEDAD", nullable = false, length = 1)
    public String getCondicionPropiedad() {
        return this.condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    @Column(name = "EDIFICIO", nullable = false, length = 2)
    public String getEdificio() {
        return this.edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    @Column(name = "PISO", nullable = false, length = 2)
    public String getPiso() {
        return this.piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    @Column(name = "UNIDAD", nullable = false, length = 4)
    public String getUnidad() {
        return this.unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    @Column(name = "NUMERO_PREDIAL", nullable = false, length = 30)
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_PREDIAL_ANTERIOR", length = 20)
    public String getNumeroPredialAnterior() {
        return this.numeroPredialAnterior;
    }

    public void setNumeroPredialAnterior(String numeroPredialAnterior) {
        this.numeroPredialAnterior = numeroPredialAnterior;
    }

    @Column(name = "NIP", length = 20)
    public String getNip() {
        return this.nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    @Column(name = "CONSECUTIVO_CATASTRAL", precision = 20, scale = 0)
    public BigDecimal getConsecutivoCatastral() {
        return this.consecutivoCatastral;
    }

    public void setConsecutivoCatastral(BigDecimal consecutivoCatastral) {
        this.consecutivoCatastral = consecutivoCatastral;
    }

    @Column(name = "CONSECUTIVO_CATASTRAL_ANTERIOR", precision = 20, scale = 0)
    public BigDecimal getConsecutivoCatastralAnterior() {
        return this.consecutivoCatastralAnterior;
    }

    public void setConsecutivoCatastralAnterior(
        BigDecimal consecutivoCatastralAnterior) {
        this.consecutivoCatastralAnterior = consecutivoCatastralAnterior;
    }

    @Column(name = "ZONA_UNIDAD_ORGANICA", length = 30)
    public String getZonaUnidadOrganica() {
        return this.zonaUnidadOrganica;
    }

    public void setZonaUnidadOrganica(String zonaUnidadOrganica) {
        this.zonaUnidadOrganica = zonaUnidadOrganica;
    }

    @Column(name = "LOCALIDAD_CODIGO", length = 7)
    public String getLocalidadCodigo() {
        return this.localidadCodigo;
    }

    public void setLocalidadCodigo(String localidadCodigo) {
        this.localidadCodigo = localidadCodigo;
    }

    @Column(name = "CORREGIMIENTO_CODIGO", length = 7)
    public String getCorregimientoCodigo() {
        return this.corregimientoCodigo;
    }

    public void setCorregimientoCodigo(String corregimientoCodigo) {
        this.corregimientoCodigo = corregimientoCodigo;
    }

    @Column(name = "NOMBRE", length = 250)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "DESTINO", nullable = false, length = 30)
    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "AREA_TERRENO", nullable = false, precision = 18, scale = 2)
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    // ------------------
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CIRCULO_REGISTRAL", nullable = true)
    public CirculoRegistral getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(CirculoRegistral circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    // ------------------
    @Column(name = "NUMERO_REGISTRO", length = 20)
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "NUMERO_REGISTRO_ANTERIOR", length = 20)
    public String getNumeroRegistroAnterior() {
        return this.numeroRegistroAnterior;
    }

    public void setNumeroRegistroAnterior(String numeroRegistroAnterior) {
        this.numeroRegistroAnterior = numeroRegistroAnterior;
    }

    @Column(name = "TIPO_CATASTRO", nullable = false, length = 2)
    public String getTipoCatastro() {
        return this.tipoCatastro;
    }

    public void setTipoCatastro(String tipoCatastro) {
        this.tipoCatastro = tipoCatastro;
    }

    @Column(name = "ESTE", length = 20)
    public String getEste() {
        return this.este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    @Column(name = "NORTE", length = 20)
    public String getNorte() {
        return this.norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    @Column(name = "ESTADO", nullable = false, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "ESTRATO", length = 30)
    public String getEstrato() {
        return this.estrato;
    }

    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    @Column(name = "CHIP", length = 14)
    public String getChip() {
        return this.chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    @Column(name = "NUMERO_ULTIMA_RESOLUCION", length = 30)
    public String getNumeroUltimaResolucion() {
        return this.numeroUltimaResolucion;
    }

    public void setNumeroUltimaResolucion(String numeroUltimaResolucion) {
        this.numeroUltimaResolucion = numeroUltimaResolucion;
    }

    @Column(name = "ANIO_ULTIMA_RESOLUCION", precision = 4, scale = 0)
    public Short getAnioUltimaResolucion() {
        return this.anioUltimaResolucion;
    }

    public void setAnioUltimaResolucion(Short anioUltimaResolucion) {
        this.anioUltimaResolucion = anioUltimaResolucion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<PredioAvaluoCatastral> getPredioAvaluoCatastrals() {
        return this.predioAvaluoCatastrals;
    }

    public void setPredioAvaluoCatastrals(
        List<PredioAvaluoCatastral> predioAvaluoCatastrals) {
        this.predioAvaluoCatastrals = predioAvaluoCatastrals;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<PredioDireccion> getPredioDireccions() {
        return this.predioDireccions;
    }

    public void setPredioDireccions(List<PredioDireccion> predioDireccions) {
        this.predioDireccions = predioDireccions;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<ReferenciaCartografica> getReferenciaCartograficas() {
        return this.referenciaCartograficas;
    }

    public void setReferenciaCartograficas(
        List<ReferenciaCartografica> referenciaCartograficas) {
        this.referenciaCartograficas = referenciaCartograficas;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<PredioInconsistencia> getPredioInconsistencias() {
        return this.predioInconsistencias;
    }

    public void setPredioInconsistencias(
        List<PredioInconsistencia> predioInconsistencias) {
        this.predioInconsistencias = predioInconsistencias;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<PersonaPredio> getPersonaPredios() {
        return this.personaPredios;
    }

    public void setPersonaPredios(List<PersonaPredio> personaPredios) {
        this.personaPredios = personaPredios;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<PredioBloqueo> getPredioBloqueos() {
        return this.predioBloqueos;
    }

    public void setPredioBloqueos(List<PredioBloqueo> predioBloqueos) {
        this.predioBloqueos = predioBloqueos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<PredioServidumbre> getPredioServidumbres() {
        return this.predioServidumbres;
    }

    public void setPredioServidumbres(List<PredioServidumbre> predioServidumbres) {
        this.predioServidumbres = predioServidumbres;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<FichaMatriz> getFichaMatrizs() {
        return this.fichaMatrizs;
    }

    public void setFichaMatrizs(List<FichaMatriz> fichaMatrizs) {
        this.fichaMatrizs = fichaMatrizs;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<PredioZona> getPredioZonas() {
        return this.predioZonas;
    }

    public void setPredioZonas(List<PredioZona> predioZonas) {
        this.predioZonas = predioZonas;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<UnidadConstruccion> getUnidadConstruccions() {
        return this.unidadConstruccions;
    }

    public void setUnidadConstruccions(
        List<UnidadConstruccion> unidadConstruccions) {
        this.unidadConstruccions = unidadConstruccions;
    }

    /* @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio") public
     * Set<CoordenadaNodo> getCoordenadaNodos() { return this.coordenadaNodos; }
     *
     * public void setCoordenadaNodos(Set<CoordenadaNodo> coordenadaNodos) { this.coordenadaNodos =
     * coordenadaNodos; } */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "predio")
    public List<Foto> getFotos() {
        return this.fotos;
    }

    public void setFotos(List<Foto> fotos) {
        this.fotos = fotos;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL")
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_REGISTRO", length = 7)
    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "AVALUO_CATASTRAL")
    public Double getAvaluoCatastral() {
        return this.avaluoCatastral;
    }

    public void setAvaluoCatastral(Double avaluoCatastral) {
        this.avaluoCatastral = avaluoCatastral;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que determina el número total de unidades de construcción del predio
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public int getTotalUnidadesConstruccion() {
        if (this.unidadConstruccions != null) {
            return this.unidadConstruccions.size();
        }
        return 0;
    }

    /**
     * Método que retorna el área total del terreno del predio.
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getAreaTotalTerreno() {
        Double areaTotal = 0.0;
        for (PredioZona predZona : this.getPredioZonas()) {
            areaTotal += predZona.getArea();
        }
        return areaTotal.toString();
    }

    /**
     * Método para obtener el area de terreno distribuida en hectareas y metros
     *
     * @author felipe.cadena
     *
     * @return - arreglo de dos posiciones con el area en Hectareas y Metros respectivamente
     *
     */
    @Transient
    public int[] getAreaTerrenoHectareasMetros() {
        return Utilidades.convertirAreaAformatoHectareas(this.areaTerreno);
    }

    /**
     * Método para obtener el area de construccion distribuida en hectareas y metros
     *
     * @author felipe.cadena
     *
     * @return - arreglo de dos posiciones con el area en Hectareas y Metros respectivamente
     *
     */
    @Transient
    public int[] getAreaTotalConstruccionPrivadaHectareasMetros() {
        return Utilidades.convertirAreaAformatoHectareas(Double.valueOf(this.
            getAreaTotalConstruccion()));
    }

    /**
     * Método que retorna el área total de construcción del predio a partir del subconjunto de
     * construcción convencional y no convencional
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getAreaTotalConstruccion() {
        Double areaTotal = new Double(this.getAreaTotalConsConvencional());
        areaTotal += new Double(this.getAreaTotalConsNoConvencional());
        return areaTotal.toString();
    }

    /**
     * Método que retorna el área de construcción convencional del predio convencional
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getAreaTotalConsConvencional() {
        Double area = 0.0;
        for (UnidadConstruccion unidad1 : this
            .getUnidadesConstruccionConvencional()) {
            area += unidad1.getAreaConstruida();
        }
        return area.toString();
    }

    /**
     * Método que retorna el área de construcción NO convencional del predio
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getAreaTotalConsNoConvencional() {
        Double area = 0.0;
        for (UnidadConstruccion unidad1 : this
            .getUnidadesConstruccionNoConvencional()) {
            area += unidad1.getAreaConstruida();
        }
        return area.toString();
    }

    /**
     * Método que retorna las unidades de construcción convencionales
     *
     * @return
     * @author fabio.navarrete
     * @modified by leidy.gonzalez 24/09/2014 Se agrega validación para verificar que las unidades
     * de construccion convencional no sean nulas
     */
    @Transient
    public List<UnidadConstruccion> getUnidadesConstruccionConvencional() {
        ArrayList<UnidadConstruccion> unidadesConst = new ArrayList<UnidadConstruccion>();
        if (this.getUnidadConstruccions() != null) {
            for (UnidadConstruccion uni : this.getUnidadConstruccions()) {
                if (uni.getTipoConstruccion().trim()
                    .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                    unidadesConst.add(uni);
                }
            }
        }
        return unidadesConst;
    }

    /**
     * Método que retorna el avaluo de las unidades de construcción convencionales
     *
     * @return
     * @author juan.agudelo
     */
    @Transient
    public Double getAvaluoUnidadesConstruccionConvencional() {
        Double avaluoConstruccionConverncional = 0.0;
        for (UnidadConstruccion uni : this.getUnidadConstruccions()) {
            if (uni.getTipoConstruccion().trim()
                .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                avaluoConstruccionConverncional += uni.getAvaluo();
            }
        }
        return avaluoConstruccionConverncional;
    }

    /**
     * Método que retorna el avaluo de las unidades de construcción no convencionales
     *
     * @return
     * @author juan.agudelo
     */
    @Transient
    public Double getAvaluoUnidadesConstruccionNoConvencional() {
        Double avaluoConstruccionConverncional = 0.0;
        for (UnidadConstruccion uni : this.getUnidadConstruccions()) {
            if (uni.getTipoConstruccion()
                .trim()
                .equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
                avaluoConstruccionConverncional += uni.getAvaluo();
            }
        }
        return avaluoConstruccionConverncional;
    }

    /**
     * Método que retorna las unidades de construcción no convencionales
     *
     * @return
     * @author fabio.navarrete
     * @modified by leidy.gonzalez 24/09/2014 Se agrega validación para verificar que las unidades
     * de construccion no convencional no sean nulas
     */
    @Transient
    public List<UnidadConstruccion> getUnidadesConstruccionNoConvencional() {
        ArrayList<UnidadConstruccion> unidadesConst = new ArrayList<UnidadConstruccion>();
        if (this.getUnidadConstruccions() != null) {
            for (UnidadConstruccion uni : this.getUnidadConstruccions()) {
                if (!uni.getTipoConstruccion().trim()
                    .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                    unidadesConst.add(uni);
                }
            }
        }
        return unidadesConst;
    }

    @Column(name = "AREA_CONSTRUCCION", nullable = false, precision = 18)
    public Double getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    /**
     * Método que retorna TODOS los Derechos de propiedad del Predio.
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public List<PersonaPredioPropiedad> getDerechosPropiedad() {
        List<PersonaPredioPropiedad> derechosPropiedad = new ArrayList<PersonaPredioPropiedad>();
        if (this.getPersonaPredios() != null) {
            for (PersonaPredio perPre : this.getPersonaPredios()) {
                List<PersonaPredioPropiedad> personaPredioPropiedades = perPre
                    .getPersonaPredioPropiedads();
                for (PersonaPredioPropiedad perPrePro : personaPredioPropiedades) {
                    derechosPropiedad.add(perPrePro);
                }
            }
        }
        return derechosPropiedad;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete
     */
    @Transient
    public String getEstadoValor() {
        return estadoValor;
    }

    /**
     * @author fabio.navarrete
     * @param estadoValor
     */
    public void setEstadoValor(String estadoValor) {
        this.estadoValor = estadoValor;
    }

    /**
     * @author fabio.navarrete
     * @return
     */
    @Transient
    public String getTipoValor() {
        return tipoValor;
    }

    /**
     * @author fabio.navarrete
     * @param tipoValor
     */
    public void setTipoValor(String tipoValor) {
        this.tipoValor = tipoValor;
    }

    /**
     * @author fabio.navarrete
     * @return
     */
    @Transient
    public String getDestinoValor() {
        return destinoValor;
    }

    /**
     * @author fabio.navarrete
     * @param destinoValor
     */
    public void setDestinoValor(String destinoValor) {
        this.destinoValor = destinoValor;
    }

    /**
     * @author fabio.navarrete
     * @return
     */
    @Transient
    public String getZonaUnidadOrganicaValor() {
        return zonaUnidadOrganicaValor;
    }

    /**
     * @author fabio.navarrete
     * @param zonaUnidadOrganicaValor
     */
    public void setZonaUnidadOrganicaValor(String zonaUnidadOrganicaValor) {
        this.zonaUnidadOrganicaValor = zonaUnidadOrganicaValor;
    }

    /**
     * Método que arma el número predial con el formato usado en los caos de uso de consulta para la
     * interfaz gráfica del sistema. Retorna el dato sin códigos de departamento y municipio.
     *
     * @author fabio.navarrete
     * @return Número predial con el formato especificado.
     */
    @Transient
    public String getFormatedNumeroPredial() {
        String formatedNumPredial = new String();
        /*
         * //Departamento formatedNumPredial = numeroPredial.substring(0, 2).concat("-");
         * //Municipio formatedNumPredial = formatedNumPredial.concat(numeroPredial.substring(2, 5))
         * .concat("-");
         */
        // Tipo Avalúo
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(5, 7)).concat("-");
        // Sector
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(7, 9)).concat("-");
        // Comuna
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(9, 11)).concat("-");
        // Barrio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(11, 13)).concat("-");
        // Manzana
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(13, 17)).concat("-");
        // Predio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(17, 21)).concat("-");
        // Condición Propiedad
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(21, 22)).concat("-");
        // Edificio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(22, 24)).concat("-");
        // Piso
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(24, 26)).concat("-");
        // Unidad
        formatedNumPredial = formatedNumPredial.concat(numeroPredial.substring(
            26, 30));
        return formatedNumPredial;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete Método que arma el número predial
     * @return Número predial completo con el formato especificado.
     */
    @Transient
    public String getFormattedNumeroPredialCompleto() {
        String formatedNumPredial = new String();

        // Departamento
        formatedNumPredial = numeroPredial.substring(0, 2).concat("-");
        // Municipio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(2, 5)).concat("-");
        // Tipo Avalúo
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(5, 7)).concat("-");
        // Sector
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(7, 9)).concat("-");
        // Comuna
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(9, 11)).concat("-");
        // Barrio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(11, 13)).concat("-");
        // Manzana
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(13, 17)).concat("-");
        // Predio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(17, 21)).concat("-");
        // Condición Propiedad
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(21, 22)).concat("-");
        // Edificio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(22, 24)).concat("-");
        // Piso
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(24, 26)).concat("-");
        // Unidad
        formatedNumPredial = formatedNumPredial.concat(numeroPredial.substring(
            26, 30));
        return formatedNumPredial;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * retorna el substring del número predial que indica el tipo de avalúo del predio: caracteres 6
     * y 7
     *
     * OJO: se puede usar simplemente la función getTipoAvaluo. Ese dato está bien definido en la
     * tabla.
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public String getTipoAvaluoPredio() {

        String answer = "";
        answer = this.numeroPredial.substring(5, 7);

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna el substring del número predial que indica el municipio: caracteres 1 al 5
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public String getCodigoMunicipio() {
        String answer = "";
        answer = this.numeroPredial.substring(0, 5);

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    public void setLey14(boolean ley14) {
        if (ley14) {
            this.tipoCatastro = EPredioTipoCatastro.LEY_14.getCodigo();
        }
    }

    @Transient
    public boolean getLey14() {
        return this.tipoCatastro.equals(EPredioTipoCatastro.LEY_14.getCodigo());
    }

    /**
     * Determina si un predio es urbano o rural. Utiliza la propiedad de tipoAvaluo
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isPredioRural() {
        if (this.tipoAvaluo != null) {
            return this.tipoAvaluo.equals(ETipoAvaluo.RURAL.getCodigo());
        }
        return false;
    }

    /**
     * Variable definida para la interfaz gráfica, indica si se debe imprimir la ficha predial del
     * predio
     *
     * @return
     */
    @Transient
    public boolean isImprimirFichaPredial() {
        return imprimirFichaPredial;
    }

    public void setImprimirFichaPredial(boolean imprimirFichaPredial) {
        this.imprimirFichaPredial = imprimirFichaPredial;
    }

    /**
     * Variable definida para la interfaz gráfica, indica si se debe imprimir la carta catastral del
     * predio
     *
     * @return
     */
    @Transient
    public boolean isImprimirCartaCatastral() {
        return imprimirCartaCatastral;
    }

    public void setImprimirCartaCatastral(boolean imprimirCartaCatastral) {
        this.imprimirCartaCatastral = imprimirCartaCatastral;
    }

    /**
     * Variable definida para la interfaz gráfica, indica si se debe descargar las capas geográficas
     *
     * @return
     */
    @Transient
    public boolean isDescargarCapasGeograficas() {
        return descargarCapasGeograficas;
    }

    public void setDescargarCapasGeograficas(boolean descargarCapasGeograficas) {
        this.descargarCapasGeograficas = descargarCapasGeograficas;
    }

    @Transient
    public String getDireccionPrincipal() {
        for (PredioDireccion pd : this.predioDireccions) {
            if (pd.getPrincipal().equals(ESiNo.SI.toString())) {
                return pd.getDireccion();
            }
        }
        return "";
    }

    /**
     * Método que retorna la zona como una cadena de texto obtenida a partir del número predial y su
     * estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getZona() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(5, 7);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de una zona.
     *
     * @param zona
     * @author fabio.navarrete
     */
    public void setZona(String zona) {
        if (zona.length() < 3) {
            while (zona.length() < 2) {
                zona = "0" + zona;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(5, 7, zona);
            this.numeroPredial = sb.toString();
            this.tipoAvaluo = zona;
        }
    }

    /**
     * Método que retorna el sector como una cadena de texto obtenida a partir del número predial y
     * su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getSector() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(7, 9);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de un sector.
     *
     * @param sector
     * @author fabio.navarrete
     */
    public void setSector(String sector) {
        if (sector.length() < 3) {
            while (sector.length() < 2) {
                sector = "0" + sector;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(7, 9, sector);
            this.numeroPredial = sb.toString();
            this.sectorCodigo = sector;
        }
    }

    /**
     * Método que retorna la comuna como una cadena de texto obtenida a partir del número predial y
     * su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getComuna() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(9, 11);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de una comuna.
     *
     * @param comuna
     * @author fabio.navarrete
     */
    public void setComuna(String comuna) {
        if (comuna.length() < 3) {
            while (comuna.length() < 2) {
                comuna = "0" + comuna;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(9, 11, comuna);
            this.numeroPredial = sb.toString();
            this.barrioCodigo = sb.substring(9, 13);
        }
    }

    /**
     * Método que retorna el barrio como una cadena de texto obtenida a partir del número predial y
     * su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getBarrio() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(11, 13);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de un barrio.
     *
     * @param barrio
     * @author fabio.navarrete
     */
    public void setBarrio(String barrio) {
        if (barrio.length() < 3) {
            while (barrio.length() < 2) {
                barrio = "0" + barrio;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(11, 13, barrio);
            this.numeroPredial = sb.toString();
            this.barrioCodigo = sb.substring(9, 13);
        }
    }

    /**
     * Método que retorna la manzana o vereda como una cadena de texto obtenida a partir del número
     * predial y su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getManzanaVereda() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(13, 17);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de una manzana o vereda.
     *
     * @param manzanaVereda
     * @author fabio.navarrete
     */
    public void setManzanaVereda(String manzanaVereda) {
        if (manzanaVereda.length() < 5) {
            while (manzanaVereda.length() < 4) {
                manzanaVereda = "0" + manzanaVereda;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(13, 17, manzanaVereda);
            this.numeroPredial = sb.toString();
            this.manzanaCodigo = manzanaVereda;
        }
    }

    /**
     * Método que retorna el terreno como una cadena de texto obtenida a partir del número predial y
     * su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getTerreno() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(17, 21);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial del terreno.
     *
     * @param terreno
     * @author fabio.navarrete
     */
    public void setTerreno(String terreno) {
        if (terreno.length() < 5) {
            while (terreno.length() < 4) {
                terreno = "0" + terreno;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(17, 21, terreno);
            this.numeroPredial = sb.toString();
            this.predio = terreno;
        }
    }

    /**
     * Método que retorna la condición de propiedad como una cadena de texto obtenida a partir del
     * número predial y su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getCondicionPropiedadNumeroPredial() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(21, 22);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial de la condición de propiedad.
     *
     * @param condicionPropiedad
     * @author fabio.navarrete
     */
    public void setCondicionPropiedadNumeroPredial(String condicionPropiedad) {
        if (condicionPropiedad.length() < 2) {
            while (condicionPropiedad.length() < 1) {
                condicionPropiedad = "0" + condicionPropiedad;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(21, 22, condicionPropiedad);
            this.numeroPredial = sb.toString();
            this.condicionPropiedad = condicionPropiedad;
        }
    }

    /**
     * Método que retorna el número de edificio o torre como una cadena de texto obtenida a partir
     * del número predial y su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getNumeroEdificioTorre() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(22, 24);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial del número de edificio o torre.
     *
     * @param numeroEdificioTorre
     * @author fabio.navarrete
     */
    public void setNumeroEdificioTorre(String numeroEdificioTorre) {
        if (numeroEdificioTorre.length() < 3) {
            while (numeroEdificioTorre.length() < 2) {
                numeroEdificioTorre = "0" + numeroEdificioTorre;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(22, 24, numeroEdificioTorre);
            this.numeroPredial = sb.toString();
            this.edificio = numeroEdificioTorre;
        }
    }

    /**
     * Método que retorna el piso como una cadena de texto obtenida a partir del número predial y su
     * estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getPisoNumeroPredial() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(24, 26);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial del piso.
     *
     * @param pisoNumeroPredial
     * @author fabio.navarrete
     */
    public void setPisoNumeroPredial(String pisoNumeroPredial) {
        if (pisoNumeroPredial.length() < 3) {
            while (pisoNumeroPredial.length() < 2) {
                pisoNumeroPredial = "0" + pisoNumeroPredial;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(24, 26, pisoNumeroPredial);
            this.numeroPredial = sb.toString();
            this.piso = pisoNumeroPredial;
        }
    }

    /**
     * Método que retorna el número de unidad como una cadena de texto obtenida a partir del número
     * predial y su estructura predefinida
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public String getNumeroUnidad() {
        if (this.numeroPredial != null) {
            return this.numeroPredial.substring(26, 30);
        }
        return "";
    }

    /**
     * Método que realiza la inserción en el número predial del número de unidad.
     *
     * @param numeroUnidad
     * @author fabio.navarrete
     */
    public void setNumeroUnidad(String numeroUnidad) {
        if (numeroUnidad.length() < 5) {
            while (numeroUnidad.length() < 4) {
                numeroUnidad = "0" + numeroUnidad;
            }
            StringBuilder sb = new StringBuilder(this.numeroPredial);
            sb.replace(26, 30, numeroUnidad);
            this.numeroPredial = sb.toString();
            this.unidad = numeroUnidad;
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que revisa si un predio se encuentra en propiedad horizontal o condominio
     *
     * @author juan.agudelo
     */
    @Transient
    public boolean isEsPredioEnPH() {

        if (condicionPropiedad != null &&
            !condicionPropiedad.isEmpty() &&
            (condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_6
                    .getCodigo()) ||
            condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_7
                    .getCodigo()) ||
            condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_8
                    .getCodigo()) ||
            condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_9
                    .getCodigo()) ||
            condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_1
                    .getCodigo()))) {

            return true;
        } else {
            return false;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Retorna true si el estado del predio es igual a BLOQUEADO (código en el dominio
     * PREDIO_ESTADO)
     *
     * PRE: la lista de Predio_Bloqueo se cargó con el predio al hacer la consulta del mismo.
     *
     * PRE: se supone aquí que loos bloqueos se han guardado con la fecha y hora. De esta forma el
     * before de fechas es suficiente.
     *
     * Regla: (la misma que se definió en las funciones de la bd) bloqueado si fecha actual ge fecha
     * inicio bloqueo y ((fecha actual le fecha desbloqueo o (fecha desbloqueo is null y fecha
     * actual le fecha termina bloqueo) o (fecha desbloqueo is null o fecha termina bloqueo is
     * null))
     *
     * @author pedro.garcia
     * @return true si se cumple la regla definida para saber que un predio está bloqueado. false si
     * la lista de bloqueos del predio es nula, es vacía, o no se cumple la regla
     */
    /*
     * @modified by leidy.gonzalez:: #12497:: 27/05/2015:: Se elimina validacion que se estaba
     * realizando anteriormente para la fecha de desbloqueo e impedia que se desbloquearan los
     * predios.
     */
    @Transient
    public boolean isEstaBloqueado() {
        boolean answer = false;
        Calendar calFechaActual = Calendar.getInstance();
        if (this.predioBloqueos != null && !this.predioBloqueos.isEmpty()) {
            for (PredioBloqueo pb : this.predioBloqueos) {
                if (pb.getFechaDesbloqueo() == null) {
                    answer = true;
                    break;
                } else {
                    Calendar calFechaDesbloqueo = Calendar.getInstance();
                    calFechaDesbloqueo.setTime(pb.getFechaDesbloqueo());
                    answer = calFechaActual.before(calFechaDesbloqueo);
                }
            }
        }
        return answer;
    }

    // -------------------------------------------------------------------------
    /**
     * Atributo que contiene temporalmente el número predial del predio padre
     */
    @Transient
    public String getNumeroPredialAsociadoPadre() {
        return numeroPredialAsociadoPadre;
    }

    public void setNumeroPredialAsociadoPadre(String numeroPredialAsociadoPadre) {
        this.numeroPredialAsociadoPadre = numeroPredialAsociadoPadre;
    }

    // -------------------------------------------------------------------------
    /**
     * Atributo que contiene temporalmente el coeficiente de copropiedad para el detalle del avalúo
     * del predio, este atributo se seteado manualmente desde la consulta
     */
    @Transient
    public Double getCoeficienteCopropiedad() {
        return coeficienteCopropiedad;
    }

    public void setCoeficienteCopropiedad(Double coeficienteCopropiedad) {
        this.coeficienteCopropiedad = coeficienteCopropiedad;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna el dato 'matrícula inmobiliaria'. Este es la concatenación de los campos 'cículo
     * registral' y 'número de registro' separados por '-'.
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public String getMatriculaInmobiliaria() {
        StringBuilder mi;

        mi = new StringBuilder();
        if (this.circuloRegistral != null && this.circuloRegistral.getCodigo() != null &&
            !this.circuloRegistral.getCodigo().isEmpty()) {
            mi.append(this.circuloRegistral.getCodigo()).append("-");
        }

        if (this.numeroRegistro != null && !this.numeroRegistro.isEmpty()) {
            mi.append(this.numeroRegistro);
        } else {
            mi.append("");
        }

        return mi.toString();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Un predio tiene matrícula antigua si no tiene círculo registral y tiene -opcionalmente-
     * número de registro. Un predio NO tiene matrícula antigua si tiene -obligatoriamente- círculo
     * registral y número de registro.
     *
     * @author pedro.garcia
     * @return true si cumple las condiciones para tener matrícula antigua
     */
    @Transient
    public boolean isEsMatriculaAntigua() {

        boolean answer;

        //D: condiciones para NO tener matrícula antigua
        if (this.circuloRegistral != null && this.circuloRegistral.getCodigo() != null &&
            !this.circuloRegistral.getCodigo().isEmpty() && this.numeroRegistro != null &&
            !this.numeroRegistro.isEmpty()) {
            answer = false;
        } else if (this.circuloRegistral == null || this.circuloRegistral.getCodigo() == null ||
            this.circuloRegistral.getCodigo().isEmpty()) {
            answer = true;
        } //D: caso default
        else {
            answer = true;
        }

        return answer;
    }

    /**
     * Devuelve el tipo de avalúo de un predio como un tetto
     *
     * @author pedro.garcia
     */
    @Transient
    public String getPredioTipoAvaluoValor() {
        String answer = "";

        if (this.tipoAvaluo != null) {
            if (this.tipoAvaluo.equals(ETipoAvaluo.RURAL.getCodigo())) {
                answer = ETipoAvaluo.RURAL.getNombre();
            } else {
                answer = ETipoAvaluo.URBANO.getNombre();
            }
        }
        return answer;
    }

    /**
     * Condicion 5 o 6 definen si es mejora
     *
     * @author fredy.wilches
     * @return
     */
    @Transient
    public boolean isMejora() {
        if (this.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo()) ||
            this.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_6.getCodigo())) {
            return true;
        }
        return false;
    }
    //-------------------------------------------------------------------------------------------------   

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Predio other = (Predio) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    //-------------------------------------------------------------------------------------------------   

    /**
     * Retorna el segmento del número predial de este predio que indica el número de predio: desde
     * la posición 1 hasta la {@link Constantes#NUMPREDIAL_PREDIO_INDICE } (incluida).
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public String getNumeroPredioDeNumeroPredial() {

        String answer = null;

        if (this.numeroPredial != null && this.numeroPredial.length() == 30) {
            answer = this.numeroPredial.substring(0, Constantes.NUMPREDIAL_PREDIO_INDICE);//#7925 felipe.cadena
        }

        return answer;
    }
    //-------------------------------------------------------------------------------------------------   

    /**
     * Determina si la condición de propiedad del predio es uno de los valores para los cuales a la
     * fecha (diciembre de 2013) aún no hay acciones definidas
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public boolean isCondicionPropiedadRara() {

        boolean answer = false;

        if (this.condicionPropiedad.equals(EPredioCondicionPropiedad.CP_7.getCodigo()) ||
            this.condicionPropiedad.equals(EPredioCondicionPropiedad.CP_8.getCodigo()) ||
            this.condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {
            answer = true;
        }

        return answer;
    }

    // ---------------------------------------- //
    /**
     * Método que retorna si el Predio hace referencia a una ficha matriz.
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isEsPredioFichaMatriz() {
        boolean answer = false;
        if (this.fichaMatrizs != null && !this.fichaMatrizs.isEmpty()) {
            for (FichaMatriz fm : this.fichaMatrizs) {
                if (fm.getPredio() != null &&
                    fm.getPredio().getId() != null &&
                    this.id.longValue() == fm.getPredio().getId()
                    .longValue()) {
                    answer = true;
                    break;
                }
            }
        }
        return answer;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static Predio parsePredio(HPredio hPredio) {
        Predio elPredio = new Predio();

        elPredio.setId(hPredio.getPredioId().getId());
        elPredio.setDepartamento(hPredio.getDepartamento());
        elPredio.setMunicipio(hPredio.getMunicipio());
        elPredio.setTipoAvaluo(hPredio.getTipoAvaluo());
        elPredio.setSectorCodigo(hPredio.getSectorCodigo());
        elPredio.setBarrioCodigo(hPredio.getBarrioCodigo());
        elPredio.setManzanaCodigo(hPredio.getManzanaCodigo());
        elPredio.setPredio(hPredio.getPredio());
        elPredio.setCondicionPropiedad(hPredio.getCondicionPropiedad());
        elPredio.setEdificio(hPredio.getEdificio());
        elPredio.setPiso(hPredio.getPiso());
        elPredio.setUnidad(hPredio.getUnidad());
        elPredio.setNumeroPredial(hPredio.getNumeroPredial());
        elPredio.setNumeroPredialAnterior(hPredio.getNumeroPredialAnterior());
        elPredio.setNip(hPredio.getNip());
        elPredio.setConsecutivoCatastral(hPredio.getConsecutivoCatastral());
        elPredio.setConsecutivoCatastralAnterior(hPredio.getConsecutivoCatastralAnterior());
        elPredio.setZonaUnidadOrganica(hPredio.getZonaUnidadOrganica());
        elPredio.setLocalidadCodigo(hPredio.getLocalidadCodigo());
        elPredio.setCorregimientoCodigo(hPredio.getCorregimientoCodigo());
        elPredio.setNombre(hPredio.getNombre());
        elPredio.setDestino(hPredio.getDestino());
        elPredio.setTipo(hPredio.getTipo());
        elPredio.setAreaTerreno(hPredio.getAreaTerreno());
        elPredio.setCirculoRegistral(hPredio.getCirculoRegistral());
        elPredio.setNumeroRegistro(hPredio.getNumeroRegistro());
        elPredio.setNumeroRegistroAnterior(hPredio.getNumeroRegistroAnterior());
        elPredio.setTipoCatastro(hPredio.getTipoCatastro());
        elPredio.setEste(hPredio.getEste());
        elPredio.setNorte(hPredio.getNorte());
        elPredio.setEstado(hPredio.getEstado());
        elPredio.setEstrato(hPredio.getEstrato());
        elPredio.setChip(hPredio.getChip());
        elPredio.setUsuarioLog(hPredio.getUsuarioLog());
        elPredio.setFechaLog(hPredio.getFechaLog());

        List<HPredioAvaluoCatastral> h_prediosAvaluoCatastral = hPredio.getHPredioAvaluoCatastrals();
        List<PredioAvaluoCatastral> prediosAvaluoCatastral = new ArrayList<PredioAvaluoCatastral>();
        for (HPredioAvaluoCatastral unHPredioAvaluoCatastral : h_prediosAvaluoCatastral) {
            prediosAvaluoCatastral.add(PredioAvaluoCatastral.parsePredioAvaluoCatastral(
                unHPredioAvaluoCatastral));
        }
        elPredio.setPredioAvaluoCatastrals(prediosAvaluoCatastral);

        //POR SETEAR --elPredio.setPredioDireccions(hPredio.getHPredioDireccions());
        //NO  -- elPredio.setReferenciaCartograficas(hPredio.getReferenciaCartograficas());
        //NO   -- elPredio.setPredioInconsistencias(hPredio.getPredioInconsistencias());
        List<HPersonaPredio> h_personasPredios = hPredio.getHPersonaPredios();
        List<PersonaPredio> personasPredios = new ArrayList<PersonaPredio>();
        for (HPersonaPredio unHPersonaPredio : h_personasPredios) {
            personasPredios.add(PersonaPredio.parsePersonaPredio(unHPersonaPredio));
        }
        elPredio.setPersonaPredios(personasPredios);

        //NO  --elPredio.setPredioBloqueos(hPredio.getprediosBloqueos());
        //POR SETEAR --elPredio.setPredioServidumbres(hPredio.getHPredioServidumbres());
//            List<HFichaMatriz> h_fichaMatrizs = hPredio.getHFichaMatrizs();
//            List<FichaMatriz> fichaMatrizs = new ArrayList<FichaMatriz>();
//            
//            for(HFichaMatriz unHFichaMatriz : h_fichaMatrizs){
//                fichaMatrizs.add(FichaMatriz.parseFichaMatriz(unHFichaMatriz));
//            }
//            elPredio.setFichaMatrizs(fichaMatrizs);
        //POR SETEAR --elPredio.setPredioZonas(hPredio.getHPredioZonas());
        List<HUnidadConstruccion> h_unidadConstruccions = hPredio.getHUnidadConstruccions();
        List<UnidadConstruccion> unidadConstruccions = new ArrayList<UnidadConstruccion>();
        for (HUnidadConstruccion unHUnidadConstruccion : h_unidadConstruccions) {
            unidadConstruccions.add(UnidadConstruccion.
                parseUnidadConstruccion(unHUnidadConstruccion));
        }
        elPredio.setUnidadConstruccions(unidadConstruccions);

        List<Foto> fotos = new ArrayList<Foto>();
        for (HFoto aHFoto : hPredio.getHFotos()) {
            fotos.add(Foto.parseFoto(aHFoto));
        }
        elPredio.setFotos(fotos);

        elPredio.setAreaConstruccion(hPredio.getAreaConstruccion());
        elPredio.setNumeroUltimaResolucion(hPredio.getNumeroUltimaResolucion());
        elPredio.setAnioUltimaResolucion(hPredio.getAnioUltimaResolucion());

        return elPredio;
    }
    // -------------------------------------------------------------------------

    /**
     * Condicion 1 define predio de ficha matriz mixta (Unidades de condicion 8 y 9)
     *
     * @author felipe.cadena
     * @return
     */
    @Transient
    public boolean isMixto() {
        if (this.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_1.getCodigo())) {
            return true;
        }
        return false;
    }

}
