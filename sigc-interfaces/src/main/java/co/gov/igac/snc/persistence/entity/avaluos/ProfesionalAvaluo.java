package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Profesion;
import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * The persistent class for the PROFESIONAL_AVALUOS database table.
 */
/*
 * @modified christian.rodriguez se agrega relacion de los campos direccion municipio y direccion
 * departamento @modified christian.rodriguez se agrega relacion del campo territorial @modified
 * pedro.garcia - cambio del atributo 'profesionId': antes era Long profesionId - adición de método
 * getContratoActivo @modified christian.rodriguez se agrega el campo usuarioSistema
 */
@Entity
@Table(name = "PROFESIONAL_AVALUOS", schema = "SNC_AVALUOS")
public class ProfesionalAvaluo implements Serializable {

    private static final long serialVersionUID = -8472560029108023992L;

    private Long id;
    private String activo;
    private Long calificacionPromedio;
    private String celular;
    private String correoElectronico;
    private String direccion;
    private Date fechaLog;
    private String numeroIdentificacion;
    private String primerApellido;
    private String primerNombre;
    private Profesion profesion;
    private String segundoApellido;
    private String segundoNombre;
    private String telefono;
    private String tipoIdentificacion;
    private String tipoVinculacion;
    private String usuarioLog;
    private String usuarioSistema;

    private EstructuraOrganizacional territorial;
    private Departamento direccionDepartamento;
    private Municipio direccionMunicipio;

    private List<AvaluoAsignacionProfesional> avaluoAsignacionProfesionals;
    private List<ProfesionalAvaluosContrato> profesionalAvaluosContratos;
    private List<ControlCalidadAvaluo> controlCalidadAvaluos;
    private List<ContratoInteradministrativo> contratoInteradministrativos;

    public ProfesionalAvaluo() {
    }

    @Id
    @SequenceGenerator(name = "PROFESIONAL_AVALUOS_SEQ_GEN", sequenceName =
        "PROFESIONAL_AVALUOS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROFESIONAL_AVALUOS_SEQ_GEN")
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false, length = 2)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "CALIFICACION_PROMEDIO", nullable = false, precision = 6, scale = 2)
    public Long getCalificacionPromedio() {
        return this.calificacionPromedio;
    }

    public void setCalificacionPromedio(Long calificacionPromedio) {
        this.calificacionPromedio = calificacionPromedio;
    }

    @Column(length = 50)
    public String getCelular() {
        return this.celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Column(name = "CORREO_ELECTRONICO", length = 100)
    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Column(length = 100)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_DEPARTAMENTO_CODIGO")
    public Departamento getDireccionDepartamento() {
        return this.direccionDepartamento;
    }

    public void setDireccionDepartamento(Departamento direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_MUNICIPIO_CODIGO")
    public Municipio getDireccionMunicipio() {
        return this.direccionMunicipio;
    }

    public void setDireccionMunicipio(Municipio direccionMunicipio) {
        this.direccionMunicipio = direccionMunicipio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_SISTEMA")
    public String getUsuarioSistema() {
        return this.usuarioSistema;
    }

    public void setUsuarioSistema(String usuarioSistema) {
        this.usuarioSistema = usuarioSistema;
    }

    @Column(name = "NUMERO_IDENTIFICACION", nullable = false, length = 30)
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "PRIMER_APELLIDO", nullable = false, length = 100)
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "PRIMER_NOMBRE", nullable = false, length = 100)
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESION_ID")
    public Profesion getProfesion() {
        return this.profesion;
    }

    public void setProfesion(Profesion profesion) {
        this.profesion = profesion;
    }

    @Column(name = "SEGUNDO_APELLIDO", length = 100)
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "SEGUNDO_NOMBRE", length = 100)
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Column(length = 50)
    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TERRITORIAL_CODIGO")
    public EstructuraOrganizacional getTerritorial() {
        return this.territorial;
    }

    public void setTerritorial(EstructuraOrganizacional territorial) {
        this.territorial = territorial;
    }

    @Column(name = "TIPO_IDENTIFICACION", nullable = false, length = 30)
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "TIPO_VINCULACION", nullable = false, length = 30)
    public String getTipoVinculacion() {
        return this.tipoVinculacion;
    }

    public void setTipoVinculacion(String tipoVinculacion) {
        this.tipoVinculacion = tipoVinculacion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to AvaluoAsignacionProfesional
    @OneToMany(mappedBy = "profesionalAvaluo")
    public List<AvaluoAsignacionProfesional> getAvaluoAsignacionProfesionals() {
        return this.avaluoAsignacionProfesionals;
    }

    public void setAvaluoAsignacionProfesionals(
        List<AvaluoAsignacionProfesional> avaluoAsignacionProfesionals) {
        this.avaluoAsignacionProfesionals = avaluoAsignacionProfesionals;
    }

    // bi-directional many-to-one association to ProfesionalAvaluosContrato
    @OneToMany(mappedBy = "profesionalAvaluo")
    public List<ProfesionalAvaluosContrato> getProfesionalAvaluosContratos() {
        return this.profesionalAvaluosContratos;
    }

    public void setProfesionalAvaluosContratos(
        List<ProfesionalAvaluosContrato> profesionalAvaluosContratos) {
        this.profesionalAvaluosContratos = profesionalAvaluosContratos;
    }

    // bi-directional many-to-one association to ProfesionalAvaluosContrato
    @OneToMany(mappedBy = "profesionalAvaluos")
    public List<ControlCalidadAvaluo> getControlCalidadAvaluos() {
        return this.controlCalidadAvaluos;
    }

    public void setControlCalidadAvaluos(
        List<ControlCalidadAvaluo> controlCalidadAvaluos) {
        this.controlCalidadAvaluos = controlCalidadAvaluos;
    }

    // bi-directional many-to-one association to ContratoInteradministrativo
    @OneToMany(mappedBy = "interventor")
    public List<ContratoInteradministrativo> getContratoInteradministrativos() {
        return this.contratoInteradministrativos;
    }

    public void setContratoInteradministrativos(
        List<ContratoInteradministrativo> contratoInteradministrativos) {
        this.contratoInteradministrativos = contratoInteradministrativos;
    }

    /**
     * Método que retorna el nombre completo del avaluador.
     *
     * @author christian.rodriguez
     */
    @Transient
    public String getNombreCompleto() {
        StringBuilder sb = new StringBuilder();
        sb.append("");

        if (this.primerNombre != null) {
            sb.append(this.primerNombre).append(" ");
        }
        if (this.segundoNombre != null) {
            sb.append(this.segundoNombre).append(" ");
        }
        if (this.primerApellido != null) {
            sb.append(this.primerApellido).append(" ");
        }
        if (this.segundoApellido != null) {
            sb.append(this.segundoApellido).append(" ");
        }

        return sb.toString();
    }

    /**
     * Método que consulta los contratos asociados al profesional, si tiene un contrato activo
     * entonces retorna el interventor Requiere que se hayan cargado los
     * {@link ProfesionalAvaluosContrato} asociados al profesional
     *
     * @author christian.rodriguez
     * @return {@link ProfesionalAvaluo} interventor del contrato activo, null si no tiene contrato
     * activo
     */
    @Transient
    public ProfesionalAvaluo getInterventor() {
        if (Hibernate.isInitialized(this.getProfesionalAvaluosContratos())) {
            for (ProfesionalAvaluosContrato contrato : this
                .getProfesionalAvaluosContratos()) {
                if (contrato.getActivo().equalsIgnoreCase(ESiNo.SI.getCodigo())) {
                    if (Hibernate.isInitialized(contrato.getInterventor())) {
                        return contrato.getInterventor();
                    }
                }
            }
        }

        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Override
    public boolean equals(Object another) {

        if (another == null) {
            return false;
        }
        if (this.getClass() != another.getClass()) {
            return false;
        }

        ProfesionalAvaluo pa = (ProfesionalAvaluo) another;
        if (pa.getId().compareTo(this.id) != 0 ||
            pa.getNumeroIdentificacion().compareToIgnoreCase(this.numeroIdentificacion) != 0) {
            return false;
        }

        return true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * autogenerado
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 43 * hash +
            (this.numeroIdentificacion != null ? this.numeroIdentificacion.hashCode() : 0);
        return hash;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Devuelve el contrato del profesional que esté marcado como activo.
     *
     * PRE: se debe haber cargado la lista de contratos del profesional de avalúos
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public ProfesionalAvaluosContrato getContratoActivo() {

        ProfesionalAvaluosContrato answer = null;

        if (this.profesionalAvaluosContratos != null && !this.profesionalAvaluosContratos.isEmpty()) {
            for (ProfesionalAvaluosContrato contrato : this.profesionalAvaluosContratos) {
                if (contrato.isContratoActivo()) {
                    answer = contrato;
                    break;
                }
            }
        }
        return answer;
    }

}
