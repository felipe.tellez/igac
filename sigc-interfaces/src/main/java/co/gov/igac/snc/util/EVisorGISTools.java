package co.gov.igac.snc.util;

/**
 * Enumeración con los posibles juegos de herramientas que puede tener el visor GIS
 *
 * @author christian.rodriguez
 */
public enum EVisorGISTools {

    // D: valor ("codigo")
    ACTUALIZACION("ac"),
    CONSERVACION("co"),
    NO_HERRAMIENTAS("no"),
    ACTUALIZACION_CU302("ac302"),
    OFERTAS_INMOBILIARIAS("of");

    private String codigo;

    EVisorGISTools(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
