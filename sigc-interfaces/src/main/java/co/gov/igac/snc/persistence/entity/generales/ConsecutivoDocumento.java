package co.gov.igac.snc.persistence.entity.generales;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ConsecutivoDocumento entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CONSECUTIVO_DOCUMENTO", schema = "SNC_GENERALES")
public class ConsecutivoDocumento implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private String estructuraOrganizacionalId;
    private String departamentoCodigo;
    private String municipioCodigo;
    private Long tipoDocumentoId;
    private Short anio;
    private Long ultimoConsecutivo;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public ConsecutivoDocumento() {
    }

    /** full constructor */
    public ConsecutivoDocumento(Long id, String estructuraOrganizacionalId,
        String departamentoCodigo, String municipioCodigo,
        Long tipoDocumentoId, Short anio, Long ultimoConsecutivo,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.estructuraOrganizacionalId = estructuraOrganizacionalId;
        this.departamentoCodigo = departamentoCodigo;
        this.municipioCodigo = municipioCodigo;
        this.tipoDocumentoId = tipoDocumentoId;
        this.anio = anio;
        this.ultimoConsecutivo = ultimoConsecutivo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_ID", nullable = false, length = 10)
    public String getEstructuraOrganizacionalId() {
        return this.estructuraOrganizacionalId;
    }

    public void setEstructuraOrganizacionalId(String estructuraOrganizacionalId) {
        this.estructuraOrganizacionalId = estructuraOrganizacionalId;
    }

    @Column(name = "DEPARTAMENTO_CODIGO", nullable = false, length = 2)
    public String getDepartamentoCodigo() {
        return this.departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    @Column(name = "MUNICIPIO_CODIGO", nullable = false, length = 5)
    public String getMunicipioCodigo() {
        return this.municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    @Column(name = "TIPO_DOCUMENTO_ID", nullable = false, precision = 10, scale = 0)
    public Long getTipoDocumentoId() {
        return this.tipoDocumentoId;
    }

    public void setTipoDocumentoId(Long tipoDocumentoId) {
        this.tipoDocumentoId = tipoDocumentoId;
    }

    @Column(name = "ANIO", nullable = false, precision = 4, scale = 0)
    public Short getAnio() {
        return this.anio;
    }

    public void setAnio(Short anio) {
        this.anio = anio;
    }

    @Column(name = "ULTIMO_CONSECUTIVO", nullable = false, precision = 14, scale = 0)
    public Long getUltimoConsecutivo() {
        return this.ultimoConsecutivo;
    }

    public void setUltimoConsecutivo(Long ultimoConsecutivo) {
        this.ultimoConsecutivo = ultimoConsecutivo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
