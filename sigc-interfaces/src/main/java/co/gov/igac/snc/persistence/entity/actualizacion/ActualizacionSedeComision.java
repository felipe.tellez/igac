package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ACTUALIZACION_SEDE_COMISION database table.
 *
 * @author franz.gamba
 */
@Entity
@Table(name = "ACTUALIZACION_SEDE_COMISION")
public class ActualizacionSedeComision implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5105106015898860338L;
    private Long id;
    private String direccion;
    private Date fechaLog;
    private Long soporteDocumentoId;
    private String tipoSoporte;
    private String usuarioLog;
    private Actualizacion actualizacion;

    public ActualizacionSedeComision() {
    }

    @Id
    @SequenceGenerator(name = "ACTUALIZACION_SEDE_COMISION_ID_GENERATOR",
        sequenceName = "ACTUALIZACION_SEDE_COMI_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_SEDE_COMISION_ID_GENERATOR")
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "SOPORTE_DOCUMENTO_ID")
    public Long getSoporteDocumentoId() {
        return this.soporteDocumentoId;
    }

    public void setSoporteDocumentoId(Long soporteDocumentoId) {
        this.soporteDocumentoId = soporteDocumentoId;
    }

    @Column(name = "TIPO_SOPORTE")
    public String getTipoSoporte() {
        return this.tipoSoporte;
    }

    public void setTipoSoporte(String tipoSoporte) {
        this.tipoSoporte = tipoSoporte;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

}
