package co.gov.igac.snc.persistence.util;

/**
 * Enumeraciòn que identifica los tipos de recursos humanos para asignar en una actualización:
 * CONTRATISTA, FUNCIONARIO.
 *
 * @author franz.gamba
 *
 */
public enum ETipoRecursoHumano {
    CONTRATISTA("Contratista"),
    FUNCIONARIO("Funcionario");

    private String codigo;

    ETipoRecursoHumano(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
