package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import java.util.Calendar;
import org.apache.commons.lang.time.DateUtils;

/**
 * MODIFICACIONES PROPIAS A LA CLASE Persona: -> Se agregó el serialVersionUID. -> Se agregó el
 * método Transient getBloqueada para saber si una persona tiene bloqueos asociados. -> Se agregó el
 * método Transient getNombre para obtener el nombre de una persona sin importar si es persona
 * Natural o Jurídica. Luego se reemplazo por un campo virtual de la BD -> Se modificó el tipo del
 * campo personaBloqueos de Set a List. -> Se agregó el campo tipoIdentificacionValor para almacenar
 * el valor del dominio.
 *
 * @modified by pedro.garcia merge por cambios en campos de teléfono
 */
/**
 * Persona entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "PERSONA", schema = "SNC_CONSERVACION")
public class Persona implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7215487417793734731L;

    // Fields
    /*
     * TODO: Validar si los datos para la anotación en el comentario son correctos
     *
     * @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONA_ID_SEQ")
     */
    private Long id;
    private String tipoPersona;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String digitoVerificacion;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String razonSocial;
    private String sigla;
    private String direccion;
    private Pais direccionPais;
    private Departamento direccionDepartamento;
    private Municipio direccionMunicipio;
    private String telefonoPrincipal;
    private String telefonoPrincipalExt;
    private String telefonoSecundario;
    private String telefonoSecundarioExt;
    private String telefonoCelular;
    private String fax;
    private String faxExt;
    private String correoElectronico;
    private String correoElectronicoSecundario;
    private String estadoCivil;
    private String nombre;
    private String usuarioLog;
    private Date fechaLog;
    private List<PersonaPredio> personaPredios = new ArrayList<PersonaPredio>();
    private List<PersonaBloqueo> personaBloqueos = new ArrayList<PersonaBloqueo>();

    private String tipoIdentificacionValor;

    @Transient
    public String getTipoIdentificacionValor() {
        return tipoIdentificacionValor;
    }

    public void setTipoIdentificacionValor(String tipoIdentificacionValor) {
        this.tipoIdentificacionValor = tipoIdentificacionValor;
    }

    /**
     * Comentareado por FGWL porque Julio incluyó campo virtual de oracle Método encargado de
     * concatenar el nombre de una persona si es natural y retornar la razón social de la persona si
     * es jurídica.
     *
     * @return Se retornal el nombre o razón social
     */
    //TODO revisar el de julio
    @Deprecated
    @Transient
    public String getNombreCompleto() {
        String name = "";

        if (this.tipoPersona.equals(EPersonaTipoPersona.NATURAL.getCodigo()) || this.razonSocial ==
            null || this.razonSocial.equals("")) {
            name = this.primerNombre;
            if (this.segundoNombre != null) {
                name = name.concat(" ").concat(this.segundoNombre);
            }
            if (this.primerApellido != null) {
                name = name.concat(" ").concat(this.primerApellido);
            }
            if (this.segundoApellido != null) {
                name = name.concat(" ").concat(this.segundoApellido);
            }
        } else if (this.tipoPersona.equals(EPersonaTipoPersona.JURIDICA
            .getCodigo()) || (this.razonSocial != null && !this.razonSocial.equals(""))) {
            name = this.razonSocial;
        }
        return name;
    }

    // Constructors
    /** default constructor */
    public Persona() {
    }

    /** minimal constructor */
    public Persona(Long id, String tipoPersona, String tipoIdentificacion,
        String numeroIdentificacion, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tipoPersona = tipoPersona;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /**
     * Usado en IPER
     *
     * @author fredy.wilches
     * @param id
     * @param tipoPersona
     * @param tipoIdentificacion
     * @param numeroIdentificacion
     * @param usuarioLog
     * @param fechaLog
     */
    public Persona(String tipoIdentificacion,
        String numeroIdentificacion, String primerNombre, String segundoNombre,
        String primerApellido,
        String segundoApellido, String razonSocial) {
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.razonSocial = razonSocial;
    }

    /** full constructor */
    public Persona(Long id, String tipoPersona, String tipoIdentificacion,
        String numeroIdentificacion, String digitoVerificacion,
        String primerNombre, String segundoNombre, String primerApellido,
        String segundoApellido, String razonSocial, String sigla,
        String direccion, Pais direccionPais,
        Departamento direccionDepartamento,
        Municipio direccionMunicipio,
        String telefonoPrincipal,
        String telefonoPrincipalExt, String telefonoSecundario,
        String telefonoSecundarioExt, String telefonoCelular, String fax, String faxExt,
        String correoElectronico, String correoElectronicoSecundario,
        String estadoCivil, String nombre, String usuarioLog, Date fechaLog,
        List<PersonaPredio> personaPredios,
        List<PersonaBloqueo> personaBloqueos) {
        this.id = id;
        this.tipoPersona = tipoPersona;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.digitoVerificacion = digitoVerificacion;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.razonSocial = razonSocial;
        this.sigla = sigla;
        this.direccion = direccion;
        this.direccionPais = direccionPais;
        this.direccionDepartamento = direccionDepartamento;
        this.direccionMunicipio = direccionMunicipio;
        this.telefonoPrincipal = telefonoPrincipal;
        this.telefonoPrincipalExt = telefonoPrincipalExt;
        this.telefonoSecundario = telefonoSecundario;
        this.telefonoSecundarioExt = telefonoSecundarioExt;
        this.telefonoCelular = telefonoCelular;
        this.fax = fax;
        this.faxExt = faxExt;
        this.correoElectronico = correoElectronico;
        this.correoElectronicoSecundario = correoElectronicoSecundario;
        this.estadoCivil = estadoCivil;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.personaPredios = personaPredios;
        this.personaBloqueos = personaBloqueos;
        this.nombre = nombre;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_PERSONA", nullable = false, length = 30)
    public String getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    @Column(name = "TIPO_IDENTIFICACION", nullable = false, length = 30)
    public String getTipoIdentificacion() {
        return this.tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Column(name = "NUMERO_IDENTIFICACION", nullable = false, length = 50)
    public String getNumeroIdentificacion() {
        return this.numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @Column(name = "DIGITO_VERIFICACION", length = 2)
    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    @Column(name = "PRIMER_NOMBRE", length = 100)
    public String getPrimerNombre() {
        return this.primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Column(name = "SEGUNDO_NOMBRE", length = 100)
    public String getSegundoNombre() {
        return this.segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Column(name = "PRIMER_APELLIDO", length = 100)
    public String getPrimerApellido() {
        return this.primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "SEGUNDO_APELLIDO", length = 100)
    public String getSegundoApellido() {
        return this.segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "RAZON_SOCIAL", length = 100)
    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Column(name = "SIGLA", length = 50)
    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Column(name = "DIRECCION", length = 100)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_PAIS_CODIGO", nullable = true)
    public Pais getDireccionPais() {
        return this.direccionPais;
    }

    public void setDireccionPais(Pais direccionPais) {
        this.direccionPais = direccionPais;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_DEPARTAMENTO_CODIGO", nullable = true)
    public Departamento getDireccionDepartamento() {
        return this.direccionDepartamento;
    }

    public void setDireccionDepartamento(
        Departamento direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECCION_MUNICIPIO_CODIGO", nullable = true)
    public Municipio getDireccionMunicipio() {
        return this.direccionMunicipio;
    }

    public void setDireccionMunicipio(Municipio direccionMunicipio) {
        this.direccionMunicipio = direccionMunicipio;
    }

    @Column(name = "TELEFONO_PRINCIPAL", length = 15)
    public String getTelefonoPrincipal() {
        return this.telefonoPrincipal;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    @Column(name = "TELEFONO_PRINCIPAL_EXT", length = 6)
    public String getTelefonoPrincipalExt() {
        return this.telefonoPrincipalExt;
    }

    public void setTelefonoPrincipalExt(String telefonoPrincipalExt) {
        this.telefonoPrincipalExt = telefonoPrincipalExt;
    }

    @Column(name = "TELEFONO_SECUNDARIO", length = 15)
    public String getTelefonoSecundario() {
        return this.telefonoSecundario;
    }

    public void setTelefonoSecundario(String telefonoSecundario) {
        this.telefonoSecundario = telefonoSecundario;
    }

    @Column(name = "TELEFONO_SECUNDARIO_EXT", length = 6)
    public String getTelefonoSecundarioExt() {
        return this.telefonoSecundarioExt;
    }

    public void setTelefonoSecundarioExt(String telefonoSecundarioExt) {
        this.telefonoSecundarioExt = telefonoSecundarioExt;
    }

    @Column(name = "TELEFONO_CELULAR", length = 15)
    public String getTelefonoCelular() {
        return this.telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    @Column(name = "FAX", length = 15)
    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Column(name = "FAX_EXT", length = 6)
    public String getFaxExt() {
        return this.faxExt;
    }

    public void setFaxExt(String faxExt) {
        this.faxExt = faxExt;
    }

    @Column(name = "CORREO_ELECTRONICO", length = 100)
    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Column(name = "CORREO_ELECTRONICO_SECUNDARIO", length = 100)
    public String getCorreoElectronicoSecundario() {
        return this.correoElectronicoSecundario;
    }

    public void setCorreoElectronicoSecundario(
        String correoElectronicoSecundario) {
        this.correoElectronicoSecundario = correoElectronicoSecundario;
    }

    @Column(name = "ESTADO_CIVIL", length = 30)
    public String getEstadoCivil() {
        return this.estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "persona")
    public List<PersonaPredio> getPersonaPredios() {
        return this.personaPredios;
    }

    public void setPersonaPredios(List<PersonaPredio> personaPredios) {
        this.personaPredios = personaPredios;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "persona")
    public List<PersonaBloqueo> getPersonaBloqueos() {
        return this.personaBloqueos;
    }

    public void setPersonaBloqueos(List<PersonaBloqueo> personaBloqueos) {
        this.personaBloqueos = personaBloqueos;
    }

    @Column(name = "NOMBRE", length = 600)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método creado para saber cuándo una persona tiene bloqueos activo o no.
     *
     * @modified pedro.garcia debe revisar no solo si tiene algún registro de bloqueos, sino además
     * si ese bloqueo está activo
     * @modified pedro.garcia 17-04-2012 se valida teniendo en cuenta la fecha actual
     * @modified pedro.garcia 16-05-2012 cambio en la regla para determinar si hay un bloqueo actual
     *
     * Regla: (la misma que se definió en las funciones de la bd) bloqueado si fecha actual GE fecha
     * inicio bloqueo y ((fecha actual LE fecha desbloqueo o (fecha desbloqueo is null y fecha
     * actual LE fecha termina bloqueo) o (fecha desbloqueo is null o fecha termina bloqueo is
     * null))
     *
     * @return
     */
    /*
     * @modified by leidy.gonzalez:: #12497:: 27/05/2015:: Se elimina validacion que se estaba
     * realizando anteriormente para la fecha de desbloqueo e impedia que se desbloquearan las
     * personas.
     */
    @Transient
    public Boolean getBloqueada() {

        boolean answer = false;
        Calendar calFechaActual, calFechaDesbloqueo, calFechaInicioBloqueo, calFechaTerminaBloqueo;

        if (this.personaBloqueos.size() > 0) {
            calFechaActual = Calendar.getInstance();
            calFechaDesbloqueo = Calendar.getInstance();
            calFechaInicioBloqueo = Calendar.getInstance();
            calFechaTerminaBloqueo = Calendar.getInstance();
            calFechaActual.setTime(new Date(System.currentTimeMillis()));

            for (PersonaBloqueo pb : this.personaBloqueos) {

                if (pb.getFechaInicioBloqueo() != null) {
                    calFechaInicioBloqueo.setTime(pb.getFechaInicioBloqueo());
                } else {
                    continue;
                }

                if (pb.getFechaDesbloqueo() != null) {
                    calFechaDesbloqueo.setTime(pb.getFechaDesbloqueo());
                }
                if (pb.getFechaTerminaBloqueo() != null) {
                    calFechaTerminaBloqueo.setTime(pb.getFechaTerminaBloqueo());
                }

                // si fecha actual GE fecha inicio bloqueo
                if ((calFechaActual.after(calFechaInicioBloqueo) ||
                    DateUtils.isSameDay(calFechaActual, calFechaInicioBloqueo)) &&
                    // y
                    //     fecha actual LE fecha desbloqueo
                    ((pb.getFechaDesbloqueo() != null &&
                    (calFechaActual.before(calFechaDesbloqueo) || DateUtils.
                    isSameDay(calFechaActual, calFechaDesbloqueo))) ||
                    // o fecha desbloqueo is null y fecha actual LE fecha termina bloqueo
                    (pb.getFechaDesbloqueo() == null && pb.getFechaTerminaBloqueo() != null &&
                    (calFechaActual.before(calFechaTerminaBloqueo) || DateUtils.isSameDay(
                    calFechaActual, calFechaTerminaBloqueo))))) {

                    answer = true;
                    break;
                }
            }
        }

        return answer;
    }

    /**
     * Implementado para comparar propietarios en el proceso de cargue de IPER
     */
    public boolean equals(Object o) {
        if (o instanceof Persona) {
            Persona p = (Persona) o;
            return this.tipoIdentificacion.equals(p.getTipoIdentificacion()) &&
                this.numeroIdentificacion.equals(p.getNumeroIdentificacion()) && this.
                getNombreCompleto().equals(p.getNombreCompleto());
        }
        return false;
    }

//end of class
}
