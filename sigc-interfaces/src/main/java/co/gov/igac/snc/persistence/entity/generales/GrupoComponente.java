package co.gov.igac.snc.persistence.entity.generales;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * GrupoComponente entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "GRUPO_COMPONENTE", schema = "SNC_GENERALES", uniqueConstraints = @UniqueConstraint(
    columnNames = {
        "GRUPO_ID", "COMPONENTE_ID"}))
public class GrupoComponente implements java.io.Serializable {

    // Fields
    private Long id;
    private Grupo grupo;
    private Componente componente;
    private String consulta;
    private String crea;
    private String edita;
    private String elimina;
    private String ejecuta;
    private String usuarioLog;
    private Date fechaLog;
    private String ambito;

    // Constructors
    /** default constructor */
    public GrupoComponente() {
    }

    /** full constructor */
    public GrupoComponente(Long id, Grupo grupo, Componente componente,
        String consulta, String crea, String edita, String elimina,
        String ejecuta, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.grupo = grupo;
        this.componente = componente;
        this.consulta = consulta;
        this.crea = crea;
        this.edita = edita;
        this.elimina = elimina;
        this.ejecuta = ejecuta;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GRUPO_ID", nullable = false)
    public Grupo getGrupo() {
        return this.grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPONENTE_ID", nullable = false)
    public Componente getComponente() {
        return this.componente;
    }

    public void setComponente(Componente componente) {
        this.componente = componente;
    }

    @Column(name = "CONSULTA", nullable = false, length = 2)
    public String getConsulta() {
        return this.consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    @Column(name = "CREA", nullable = false, length = 2)
    public String getCrea() {
        return this.crea;
    }

    public void setCrea(String crea) {
        this.crea = crea;
    }

    @Column(name = "EDITA", nullable = false, length = 2)
    public String getEdita() {
        return this.edita;
    }

    public void setEdita(String edita) {
        this.edita = edita;
    }

    @Column(name = "ELIMINA", nullable = false, length = 2)
    public String getElimina() {
        return this.elimina;
    }

    public void setElimina(String elimina) {
        this.elimina = elimina;
    }

    @Column(name = "EJECUTA", nullable = false, length = 2)
    public String getEjecuta() {
        return this.ejecuta;
    }

    public void setEjecuta(String ejecuta) {
        this.ejecuta = ejecuta;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "AMBITO", nullable = false, length = 30)
    public String getAmbito() {
        return this.ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

}
