package co.gov.igac.snc.fachadas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Profesion;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAreaRegistrada;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoCapitulo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoMovimiento;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoObservacion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPermiso;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioAnexo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioConstruccion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioCultivo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioMaquinaria;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioZona;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoRequisitoDocumentacion;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoParticipante;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoPonente;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradminisAdicion;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidad;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOfertaOb;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadRecolector;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.GttOfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaAreaUsoExclusivo;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaServicioComunal;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoAdicion;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoCesion;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoPago;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.VariableEvaluacionAvaluo;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.DetalleDocumentoFaltante;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioAvaluo;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosCarga;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.EOrdenPracticaEstado;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.FiltroDatosConsultaContratoInteradministrativo;
import co.gov.igac.snc.util.FiltroDatosConsultaOfertasInmob;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.util.FiltroDatosConsultaRadicacionesCorrespondencia;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosSolicitud;
import co.gov.igac.snc.util.FuncionarioRecolector;
import co.gov.igac.snc.util.RadicacionNuevaAvaluoComercialDTO;

/**
 *
 *
 * @author juan.mendez
 * @version 2.0
 */
@Local
public interface IAvaluosLocal {

    /**
     * Método que guarda masivamente una lista de Ofertas de Areas de Uso Exclusivo
     *
     * @param ofertaAreaUsoExclusivo
     * @return
     * @author javier.aponte
     */
    public void guardarOfertasAreasUsoExclusivo(
        List<OfertaAreaUsoExclusivo> ofertaAreaUsoExclusivo);

    /**
     * Método que guarda masivamente una lista de Ofertas de Servicios Comunales
     *
     * @param ofertaServicioComunal
     * @return
     * @author javier.aponte
     */
    public void guardarOfertasServiciosComunales(
        List<OfertaServicioComunal> ofertaServicioComunal);

    /**
     * Método que permite el guardar o actualizar una oferta inmobiliaria
     *
     * @author juan.agudelo
     * @param ofertainmobiliaria
     * @param listaOfertasEliminadas
     * @param usuario
     * @return
     */
    public OfertaInmobiliaria guardarActualizarOfertaInmobiliaria(
        OfertaInmobiliaria ofertainmobiliaria,
        List<OfertaInmobiliaria> listaOfertasEliminadas, UsuarioDTO usuario);

    /**
     * Método que permite el actualizar multiples ofertas inmobiliarias
     *
     * @author christian.rodriguez
     * @param ofertasInmobiliaria lista de ofertas a actualizar
     */
    public void guardarActualizarListaOfertaInmobiliaria(
        List<OfertaInmobiliaria> ofertasInmobiliaria);

    /**
     * Método que permite el actualizar multiples ofertas inmobiliarias
     *
     * @author christian.rodriguez
     * @param ofertasInmobiliaria lista de ofertas a actualizar
     */
    public void guardarActualizarListaControlCalidadOferta(
        List<ControlCalidadOferta> controlCalidadOferta);

    /**
     * Método que permite buscar ofertas inmobiliarias dependiendo de algun parámtero de consulta
     * contenido en FiltroDatosConsultaOfertasInmob
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaOfertasInmob
     * @param contadoresDesdeYCuantos
     * @param sortOrder nombre del ordenamiento (UNSORTED, DESCENDING, DESCENDING)
     * @param contadoresDesdeYCuantos Parámetros opcionales para tomar solo un segmento de las filas
     * resultado
     *
     * @return
     */
    public List<OfertaInmobiliaria> buscarOfertasInmobiliariasPorFiltro(
        FiltroDatosConsultaOfertasInmob filtroDatosConsultaOfertasInmob,
        String sortField, String sortOrder, int... contadoresDesdeYCuantos);

    /**
     * Método que permite contar ofertas inmobiliarias dependiendo de algun parámtero de consulta
     * contenido en FiltroDatosConsultaOfertasInmob
     *
     * @author juan.agudelo
     * @param filtroDatosConsultaOfertasInmob
     * @return
     */
    public Long contarOfertasInmobiliariasPorFiltro(
        FiltroDatosConsultaOfertasInmob filtroDatosConsultaOfertasInmob);

    /**
     * Método que permite buscar una oferta inmobiliaria con las ofertas inmobiliarias asociadas por
     * el id de la oferta inmobiliaria
     *
     * @author pedro.garcia
     * @param ofertaInmobiliariaId id de la oferta a buscar
     */
    public OfertaInmobiliaria buscarOfertaInmobiliariaPorId(
        Long ofertaInmobiliariaId);

    /**
     * Método que permite buscar una oferta inmobiliaria con las oferta inmobiliaria padre y las
     * listas inicializadas para visualización de detalles por el id de la oferta inmobiliaria
     *
     * @author juan.agudelo
     * @param id
     * @return
     */
    public OfertaInmobiliaria buscarOfertaInmobiliariaOfertaPadreByOfertaId(
        Long ofertaInmobiliariaId);

    /**
     * Método que genera un número de secuencia de la tabla GttOfertaInmobiliaria
     *
     * @author javier.aponte
     */
    public Long generarSecuenciaGttOfertaInmobiliaria();

    /**
     * Método que permite insertar masivamente en la tabla GttOfertaInmobiliaria los ids de las
     * ofertas seleccionadas
     *
     * @param
     * @author javier.aponte
     */
    public List<GttOfertaInmobiliaria> guardarActualizarIdsOfertasInmobiliarias(
        List<GttOfertaInmobiliaria> gttsOfertasInmobiliarias);

    /**
     * Método que permite borrar masivamente en la tabla GttOfertaInmobiliaria los ids de las
     * ofertas seleccionadas
     *
     * @param
     * @author javier.aponte
     */
    public void borrarIdsOfertasInmobiliarias(
        List<GttOfertaInmobiliaria> gttsOfertasInmobiliarias);

    /**
     * inserta una nueva área de captura de ofertas
     *
     * @author pedro.garcia
     * @cu CU-TV-0F-001
     * @version 2.0
     *
     * @param nuevaAreaCapturaOferta
     * @param usuario
     */
    public AreaCapturaOferta guardarAreaCapturaOferta(
        AreaCapturaOferta nuevaAreaCapturaOferta, UsuarioDTO usuario);

    /**
     * Borra el área de captura de ofertas indicada siempre y cuando no tenga recolectores asociados
     *
     * @author christian.rodriguez
     * @cu CU-TV-0F-001
     * @version 2.0
     * @modified pedro.garcia 06-06-2012 cambio de nombre y parámetro que recibe. NO es necesario
     * enviar el objeto completo porque se consulta dentro del método
     *
     * @param idAreaCapturaOferta id del área a eliminar
     */
    public boolean borrarAreaCapturaOferta(Long idAreaCapturaOferta);

    /**
     * Busca las áreas de captura para ofertas asignadas a un recolector y sin comisiones asociadas
     *
     * @author christian.rodriguez
     * @param departamentoCodigo departamento asociado al área
     * @param municipioCodigo municipio asociado al área
     * @return lista con las áreas de captura que están asignadas a un recolector pero que no se han
     * comisionado
     */
    public List<AreaCapturaOferta> buscarAreasCapturaOfertaSinComisionar(
        String departamentoCodigo, String municipioCodigo);

    /**
     * Busca las RegionesCapturaOferta del áreas de captura que tengan recolector asociado y no
     * esten comisionadas
     *
     * @author pedro.garcia
     * @param areaCapturaId id del área de captura de la que se buscan las regiones
     * @param estados estados de las comisiones que se desean traer
     * @return
     */
    public List<RegionCapturaOferta> buscarRegionesCapturaOfertaPorEstadosYIds(
        List<Long> regionesCapturaIds, List<String> estados);

    /**
     * Actualizar o guarda una lista de comisiones de ofertas
     *
     * @author christian.rodriguez
     * @param listaComisiones lsita de comisiones a actualizar
     */
    public void guardarActualizarComisionesOfertas(ArrayList<ComisionOferta> listaComisiones);

    /**
     * Actualiza o guarda una comisión de ofertas
     *
     * @author christian.rodriguez
     * @param comisionOferta comisión a guardar o actualizar
     * @param usuario usuario logeado en el sistema
     * @param regionesAEliminar regiones a eliminar de la comision. si no se quiere eliminar ninguna
     * region, se debe enviar una lista, en ningún caos debe enviarse un valor nulo
     * @return el entity guardado o actualizado
     */
    public ComisionOferta guardarActualizarComisionOferta(ComisionOferta comisionOferta,
        UsuarioDTO usuario, List<RegionCapturaOferta> regionesAEliminar);

    /**
     * Busca el área de ofertas que tenga el ID seleccionado
     *
     * @author christian.rodriguez
     * @param areaId ide del área
     * @return área asociada al id seleccionado
     */
    public AreaCapturaOferta buscarAreaCapturaOfertaPorId(Long areaId);

    /**
     * Busca las áreas de captura de ofertas que están en estado VIGENTE
     *
     * @author christian.rodriguez
     * @return lista con las áreas de captura de ofertas que están en estado VIGENTE
     */
    public List<AreaCapturaOferta> buscarAreasCapturaVigentesPorEstrucOrg(
        String estrucOrgCod);

    /**
     * Metodo para cargar la informacion de los recolectores a partir de sus ids
     *
     * @author rodrigo.hernandez
     * @param listaRecolectores
     * @param idAreaRecoleccion identificador del {@link AreaCapturaOferta} sobre la cual se quiere
     * consultar información de recolectores
     * @return
     */
    public List<FuncionarioRecolector> getInformacionRecolectores(
        List<String> listaRecolectores, Long idAreaRecoleccion);

    /**
     * Método para consultar las areas asignadas a un recolector, buscando por el id del recolector
     *
     * @author ariel.ortiz
     * @param codigosManzana - lista de codigos de las manzanas de las que se quiere recuperar las
     * planchas
     * @param valorEscala - Valor de la escala a la que se quieren recuperar las planchas
     * @param isPredioRural - variable que controla si los municipios seleccionados son ruales o
     * urbanos.
     * @return Lista de manzanas/veredas asignadas a un recolector
     */
    public List<String> obtenerCodigosPlanchas(List<String> codigosManzanas,
        String valorEscala, boolean isPredioRural, UsuarioDTO usuario);

    /**
     * Método para cargar las regiones pertenecientes al área asignada a un recolector
     *
     * @author ariel.ortiz
     * @param idAreaAsignada - id del área asignada.
     * @param usuario - Login del recolector.
     * @return
     */
    public List<RegionCapturaOferta> buscarRegionCapturaOfertaPorIdAreaYRecolector(
        Long idAreaAsignada, String idRecolector);

    /**
     * Método para cargar lista de ofertas de un recolector seleccionadas en la muestra para control
     * de calidad
     *
     * @author rodrigo.hernandez
     *
     * @param recolectorId - Id del Recolector
     * @param controlCalidadId - Id del Control de Calidad
     * @param regionId - Id de la region de captura de las ofertas
     */
    public List<ControlCalidadOferta> buscarOfertasSeleccionadasPorIdRecolectorIdControlCalidadIdRegion(
        String recolectorId, Long controlCalidadId, Long regionId);

    /**
     * Método para cargar lista de ofertas devueltas de un recolector seleccionadas en la muestra
     * para control de calidad
     *
     * @author christian.rodriguez
     *
     * @param recolectorId - Id del Recolector
     * @param controlCalidadId - Id del Control de Calidad
     * @param regionId - Id de la region de captura de las ofertas
     */
    public List<OfertaInmobiliaria> buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion(
        String recolectorId, Long controlCalidadId, Long regionId);

    /**
     * Método para cargar lista de ofertas devueltas de un recolector seleccionadas en la muestra
     * para control de calidad
     *
     * @author christian.rodriguez
     *
     * @param recolectorId - Id del Recolector
     * @param controlCalidadId - Id del Control de Calidad
     */
    public List<ControlCalidadOferta> buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad(
        String recolectorId, Long controlCalidadId);

    /**
     *
     * 	 * Método usado en el caso de uso de determinar muestras control calidad, retorna los
     * controles de calidad de Recolector (tabla CONTROL_CALIDAD_RECOLECTOR) de un control de caliad
     * especifico.
     *
     * @param idControl
     * @author ariel.ortiz
     * @return
     */
    public List<ControlCalidadRecolector> buscarControlCalidadRecolectorPorIdControl(Long idControl);

    /**
     * Método que carga las observaciones asociadas a una oferta inmobiliaria
     *
     * @author christian.rodriguez
     * @param idOferta Id de la oferta inmobiliaria
     * @return Lista con las observaciones asociadas a la oferta
     */
    public List<ControlCalidadOfertaOb> buscarObservacionesPorOfertaId(Long idOferta);

    /**
     * Método que calcula el número de ofertas de muestra que se van a utilizar para el control de
     * calidad
     *
     * @author ariel.ortiz
     * @param municipio - es el id del municipio sobre el cual se quiere realizar el control de
     * calidad.
     * @param valorPedidoD - Variable que almacena el valor de d en caso de que sea del tipo Valor
     * Pedido
     * @param areaTerrenoD - Variable que almacena el valor de d en caso de que sea del tipo área
     * Terreno
     * @param areaConstruidaD - Variable que almacena el valor de d en caso de que sea del tipo área
     * Construida
     * @param valorCalculadoD - Variable que almacena el valor de d en caso de que sea del tipo
     * Valor Calculado
     * @param login - Login del Recolector.
     * @return Lista de Controles de calidad asociados a un recolector.
     */
    public Object[] calcularMuestras(String municipioSeleccionadoCodigo,
        int valorPedidoD, int areaTerrenoD, int areaConstruidaD,
        int valorCalculadoD, String login);

    /**
     * Método para que descarta las muestras seleccionadas para un control de calidad mediante un
     * procedimiento almacenado.
     *
     * @param controlCalidadId - Id del control de Calidad a borrar
     * @author ariel.ortiz
     */
    public void borrarCalculoMuestras(Long controlCalidadId);

    /**
     * Método para guardar en la bd las ofertas seleccionadas para control de calidad.
     *
     * @param controlId - id del control de calidad a guardar
     * @author ariel.ortiz
     *
     */
    public void guardarOfertasMuestraControl(Long controlId);

    /**
     * Método para guardar las modificaciones realizadas durante el control de calidad de la oferta
     *
     * @author rodrigo.hernandez
     *
     * @param controlCalidadOfertaOb - Objeto con datos de la observación creada para una oferta
     * revisada por el control de calidad
     *
     * @param controlCalidadOferta - Objeto con datos del control de calidad realizado sobre una
     * oferta
     *
     */
    public void guardarActualizarControlCalidadOferta(ControlCalidadOfertaOb controlCalidadOfertaOb,
        ControlCalidadOferta controlCalidadOferta);

    /**
     * Método que guarda o actualiza un contorl de calidad de ofertas
     *
     * @author christian.rodriguez
     * @param controlCalidad control de calidad que se desea actualizar
     */
    public void guardarActualizarControlCalidad(ControlCalidad controlCalidad);

    /**
     * Método que cargar la lista de observaciones asociadas a una oferta a la que se le está
     * realizando control de calidad
     *
     * @author rodrigo.hernandez
     *
     * @param controlCalidadOfertaId
     *
     * @return
     */
    public List<ControlCalidadOfertaOb> buscarObservacionesPorControlCalidadOfertaId(
        Long controlCalidadOfertaId);

    /**
     * Método que carga los códigos de las manzanas/veredas asociadas a un recolector determinado.
     *
     *
     * @author ariel.ortiz
     * @param idRecolector - id del recolector del cual se quieren extraer las manzanas veredas.
     * @return
     */
    public List<String> buscarDetallesAreasPorRecolector(String idRecolector, Long idRegion);

    /**
     * Obtiene la lista de regiones captura oferta asociadas a una comisión de ofertas
     *
     * @author pedro.garcia
     *
     * @param comisionId id de la comisión
     * @return
     */
    public List<RegionCapturaOferta> obtenerRegionesCapturaOfertaPorComision(Long comisionId);

    /**
     * Carga un AreaCapturaOferta con los datos de DetalleCapturaOferta, RegionCapturaOferta,
     * Municipio y Departamento
     *
     * @author rodrigo.hernandez
     *
     * @param areaCapturaOfertaId - Id del AreaCapturaOferta
     * @return AreaCapturaOferta con todos sus datos
     */
    public AreaCapturaOferta obtenerAreaCapturaOfertaConDatosPorId(Long areaCapturaOfertaId);

    /**
     * Almacena una RegionCapturaOferta creada desde el visor GIS
     *
     * @param regionCapturaOferta RegionCapturaOferta creada desde el visor
     *
     * @return Region creada
     */
//TODO :: rodrigo.hernandez :: usar parámetro usuario loggeado como (por estándar; en los métodos
//    de guardar y modificar se us como parámetro, aunque no se use aún)
    public RegionCapturaOferta crearRegionCapturaOferta(RegionCapturaOferta regionCapturaOferta);

    /**
     * Método para asociar una
     *
     * @author rodrigo.hernandez
     *
     * @param areaCapturaOferta - AreaCapturaOferta seleccionada
     * @param region - Región creada
     * @param cadenaManzanasVeredas - Cadena con codigos de las manzanas para asignarlas a una
     * región
     *
     */
    public void asociarManzanasVeredasARegionCreada(
        AreaCapturaOferta areaCapturaOferta, RegionCapturaOferta region,
        String cadenaManzanasVeredas);

    /**
     * Método para desasignar una region de un area centralizada de un recolector
     *
     * @author rodrigo.hernandez
     *
     * @param listaRegionesSeleccionadas
     *
     */
    public void desasignarRegionRecolector(RegionCapturaOferta[] listaRegionesSeleccionadas);

    /**
     * Método para desasignar una region de una area descentralizada de un recolector
     *
     * @author rodrigo.hernandez
     *
     */
    public void desasignarRegionAreaDescentralizadaRecolector(
        RegionCapturaOferta[] listaRegionesSeleccionadas);

    /**
     * Método para cargar lista de ofertas de un recolector para control de calidad
     *
     * @author rodrigo.hernandez
     *
     * @param idRecolector - Id del Recolector
     * @param idControlCalidad - Id del Control de Calidad
     */
    public List<ControlCalidadOferta> buscarOfertasPorIdRecolectorEIdControlCalidad(
        String recolectorId, Long controlCalidadId);

    /**
     *
     * Metodo para mover proceso en control de calidad
     *
     * @author rodrigo.hernandez
     *
     * @param listaOfertasControlCalidad - lista de ofertas
     * @param estadoOfertas - estado para actualizar las ofertas
     */
    public void moverProcesoControlCalidadOferta(
        List<ControlCalidadOferta> listaOfertasControlCalidad,
        String estadoOfertas);

    /**
     * Método para consultar el id de la última región guardada en la tabla
     *
     * @author rodrigo.hernandez
     *
     * @return Id de la ultima region guardada en la tabla
     */
    public Long consultarIdUltimaRegionRegistrada();

    /**
     * Busca las comisiones de ofertas que estén en determinado estado y que pertenezcan a las
     * regiones captura oferta dadas
     *
     * @author pedro.garcia
     * @version 2.0
     * @param ofertaComisionEstado estado de las comisiones
     * @param regionesCapturaOfertaIds id de las regiones captura oferta
     * @return
     */
    public List<ComisionOferta> buscarComisionesOfertaPorEstadoYRCO(String codigo,
        List<Long> regionesCapturaOfertaIds);

    /**
     *
     * Método que consulta la lista de ofertas inmobiliarias de un recolector
     *
     * @author rodrigo.hernandez
     *
     * @param recolectorId - Id del recolector
     * @param regionId - Id de la región
     * @param estado - Estado en el que se encuentran las ofertas
     *
     * @return - Lista de ofertas inmobiliarias de un recolector
     */
    public List<OfertaInmobiliaria> buscarListaOfertasInmobiliariasPorRecolector(
        String recolectorId, Long regionId, String estado);

    /**
     * Método que carga una RegionCapturaOferta a partir de su id
     *
     * @author rodrigo.hernandez
     *
     * @param regionCapturaOfertaid - Id de la región
     *
     * @return
     */
    public RegionCapturaOferta buscarRegionCapturaOfertaPorId(Long regionCapturaOfertaid);

    /**
     * Método para actualizar un objeto RegionCapturaOferta
     *
     * @author rodrigo.hernandez
     *
     * @param regionCapturaOferta
     */
    public RegionCapturaOferta guardarActualizarRegionCapturaOferta(
        RegionCapturaOferta regionCapturaOferta);

    /**
     * Método que devuelve los predios asociados a una región de captura para ofertas inmobiliarias
     *
     * @author christian.rodriguez
     * @param regionCapturaOferta region sobre la cual buscar los predios
     * @param idRecolector recolector asociado a la region
     * @return una lista con lso predios asociados a la region de captura de ofertas
     */
    public List<Predio> consultarPrediosPorRegionCapturaOferta(
        RegionCapturaOferta regionCapturaOferta, String idRecolector);

    /**
     * Método que crea una lista de detalleCapturaOferta
     *
     * @author rodrigo.hernandez
     *
     * @return Lista de DetalleCapturaOferta
     */
    public List<DetalleCapturaOferta> crearListaDetallesCapturaOferta(
        List<DetalleCapturaOferta> listaDetallesCapturaOferta);

    /**
     * Busca las regiones que estén asociadas a una comisión de ofertas. OJO: NO hace fetch de nada
     * (y no debe modificarse)
     *
     * @author pedro.garcia
     * @version 2.0
     * @param idComision
     * @return
     */
    public List<RegionCapturaOferta> obtenerRegionesCapturaOfertaDeComisionNF(Long idComision);

    /**
     * Método que carga los predios asociados a un trámite de avaúos especÃ­fico.
     *
     * @author ariel.ortiz
     * @param tramiteId
     * @return
     */
    public List<TramitePredioAvaluo> buscarPrediosPorTramiteId(Long tramiteId);

    /**
     * Método que adiciona una lista de predios a un trámite de avaúos especÃ­fico
     *
     * @author ariel.ortiz
     * @param tramiteId
     * @param prediosActualizar
     */
    public void adicionarPrediosTramite(TramitePredioAvaluo prediosActualizar);

    /**
     *
     * Método que elimina una lista de predios a un trámite de avaúos especÃ­fico
     *
     * @author ariel.ortiz
     * @param tramiteId
     * @param prediosActualizar
     */
    public void eliminarPrediosTramite(Long tramitePredioId);

    /**
     * Método que modifica la información de un predio asociado a un trámite
     *
     * @author ariel.ortiz
     * @param prediosActualizar
     */
    public void modificarPrediosTramite(TramitePredioAvaluo prediosActualizar);

    /**
     * Busca las regionesCapturasOferta asociadas a un AreaCapturaOferta y a un recolector
     *
     * @param idAreaCapturaOferta
     * @param recolectorLogin
     * @return
     */
    public List<RegionCapturaOferta> obtenerRegionesCapturaOfertaDeAreaCapturaOfertaYRecolector(
        Long idAreaCapturaOferta, String recolectorLogin);

    /**
     * Busca las regionesCapturasOferta asociadas a un AreaCapturaOferta
     *
     * @author christian.rodriguez
     * @param idAreaCapturaOferta id del area de captura oferta
     * @return lista con las regiones asociadas al área de captura
     */
    public List<RegionCapturaOferta> obtenerRegionesCapturaOfertaPorAreaCapturaOferta(
        Long idAreaCapturaOferta);

    /**
     * Carga la lista de DetalleCapturaOferta por un Area
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public List<DetalleCapturaOferta> obtenerListaDetallesCapturaOfertaPorArea(Long idArea);

    /**
     * Método que busca los ids de las regiones de captura asociadas a una muestra de control de
     * calidad
     *
     * @author christian.rodriguez
     * @param controlCalidadId ID del control de calidad
     * @return Lista con los ids de las regiones asociadas al control de calidad
     */
    public List<Long> obtenerIdsRegionesAsociadasAControlCalidad(Long controlCalidadId);

    /**
     * Método que trae el control de calidad de ofertas que corresponda al Id solicitado
     *
     * @author christian.rodriguez
     * @param idControlCalidad id del control de caldiad que se desea consultar
     * @return control de calidad con el Id indicado
     */
    public ControlCalidad obtenerControlCalidadPorId(Long idControlCalidad);

    /**
     * Método que consulta la lista de Sec. Radicados asociadas a una solicitud de avaluo de un
     * solicitante que se va a revisar o impugnar
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitante - Id del Solicitante
     * @param banderaParaRevision - Bandera que indica si la solicitud de avaluo es para revisar o
     * impugnar<br/>
     * <b>true:</b> La solicitud de avaluo es para revisar<br/>
     * <b>false:</b> La solicitud de avaluo es para impugnar<br/>
     * @return Lista de Sec. Radicados asociados
     */
    public List<String> consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion(
        Long idSolicitante, boolean banderaSolicitudRevision);

    /**
     * Método que consulta la lista de Sec. Radicados asociados a una solicitud de cotizacion de un
     * solicitante, para asociar el Sec.Radicado seleccionado, a una solicitud de avaluo
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitante - Id del Solicitante
     *
     * @return Lista de Sec. Radicados asociados
     */
    public List<String> consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo(
        Long idSolicitante);

    /**
     * Método que guarda una SolicitudPredio
     *
     * @author christian.rodriguez
     * @param solicitudPredio solicitudPredio a guardar
     * @return solicitudPredio guardad o null si ocurrio algún error
     */
    public SolicitudPredio guardarActualizarSolicitudPredio(SolicitudPredio solicitudPredio);

    /**
     * Método que guarda una SolicitudPredio
     *
     * @author christian.rodriguez
     * @param solicitudPredio solicitudPredio a guardar
     * @return solicitudPredio guarda o null si ocurrio algún error
     */
    public List<SolicitudPredio> guardarActualizarSolicitudesPredio(
        List<SolicitudPredio> solicitudPredio);

    /**
     * Método para borrar un solicitudPredio por su id
     *
     * @author christian.rodriguez
     * @param solicitudPredio solicitud predio que se desea borrar
     * @return true si se encontró y se borro satisfactoriamente, false en otro caso
     */
    public boolean borrarSolicitudPredioPorId(SolicitudPredio solicitudPredio);

    /**
     * Método para borrar multiples solicitudPredio
     *
     * @author christian.rodriguez
     * @param solicitudesPredio lista de solicitudes predio que se desean borrar
     * @return true si se encontró y se borro satisfactoriamente, false en otro caso
     */
    public boolean borrarSolicitudesPredio(List<SolicitudPredio> solicitudesPredio);

    /**
     * Metodo para recuperar las solicitudes de documentación relacionadas a una solicitud
     *
     * @author felipe.cadena
     *
     * @param idSolicitud id de la solicitud
     * @param aportado - si es verdadero recupera los documentos aportados, de lo contrario solo los
     * no aportados
     * @return Lista de solicitudDocumentacion con los documentos asociados
     */
    public List<SolicitudDocumentacion> obtenerSolicitudDocumentacionPorIdSolicitud(Long idSolicitud,
        Boolean aportado);

    /**
     * Metodo para recuperar las solicitudes de documentación relacionadas a una solicitud agrupadas
     * por detalle
     *
     * @author felipe.cadena
     *
     * @param idSolicitud id de la solicitud
     * @return Lista de solicitudDocumentacion con los documentos asociados
     */
    public List<SolicitudDocumentacion> obtenerSolicitudDocumentacionPorIdSolicitudAgrupadas(
        Long idSolicitud);

    /**
     * Método para consultar las radicaciones nuevas
     * <i><b>(Usado en avaluos comerciales)</b></i>
     *
     * @author rodrigo.hernandez
     *
     * @param fechaDesde - Fecha inicial del rango para buscar las radicaciones
     * @param fechaHasta - Fecha final del rango para buscar las radicaciones
     * @param solicitante - Según el tipo de empresa:</br>
     * <b><i>Para persona natural: </i></b> Concatenacion de Primer Nombre, Segundo Nombre, Primer
     * Apellido, Segundo Apellido</br>
     * <b><i>Para persona jurÃ­dica: </i></b> Razón Social</br></br>
     * @param codigoTipoTramite - Código que identifica el tipo de tramite
     * @param numeroRadicacion - Número de radicacion de correspondencia (Avaúo, Revisión,
     * Cotización, Impugnación)
     * @param codigoTipoEmpresa - Código que identifica el tipo de empresa (Natural o JurÃ­dica)
     * @param codigoTerritorial - Código de la territorial a la que pertenece el usuario que realiza
     * la búsqueda
     * @param codigoDepartamentoOrigen - Código del departamento
     * @param codigoMunicipioOrigen - Código del municipio
     *
     * @return - Lista de radicaciones nuevas.
     */
    public List<RadicacionNuevaAvaluoComercialDTO> buscarRadicacionesNuevasAvaluosComerciales(
        FiltroDatosConsultaRadicacionesCorrespondencia filtroDatos, UsuarioDTO usuario);

    /**
     * Método para realizar la reclasificación de una radicacion
     * <i><b>(Usado en avaluos comerciales)</b></i>
     *
     * @author rodrigo.hernandez
     *
     * @param numeroRadicacion - Numero de la radicacion a reclasificar
     * @param codigoTipoTramite - Codigo del tramite nuevo
     * @param usuario - usuario autenticado en el sistema
     *
     * @return bandera que indica si se reclasificó el trámite exitosamente</br>
     * <b><i>True: </i></b> El tramite fue reclasificado exitosamente</br>
     * <b><i>False: </i></b> El tramite <b>NO</b> fue reclasificado exitosamente</br></br>
     *
     */
    public boolean reclasificarTramiteDocumentoODocumento(String numeroRadicacion,
        String codigoTipoTramite, UsuarioDTO usuario);

    /**
     * Método para realizar búsqueda de una radicacion de correspondencia en correspondencia a
     * partir de su numero de radicacion
     *
     * @author rodrigo.hernandez
     *
     * @param numeroRadicacion - Numero de la radicacion
     * @param usuario - Usuario autenticado en el sistema
     * @return
     */
    public RadicacionNuevaAvaluoComercialDTO buscarRadicacion(String numeroRadicacion,
        UsuarioDTO usuario);

    /**
     * Método que consulta los avaúos asociados a una solicitud
     *
     * @author christian.rodriguez
     * @param idSolicitud Identificador del solicitud
     * @return lista con los avaúos asociados a la solicitud
     */
    public List<Avaluo> consultarAvaluosPorSolicitudId(Long idSolicitud);

    /**
     * Método que crea un avaúo comercial a partir de una solicitud y una lista de predios. Se
     * encarga de generar el tramite correspondiente y de crear los avaluos predios
     *
     * @author christian.rodriguez
     * @param avaluo avaúo con los datos básicos
     * @param solicitud solicitud a la que se va a asociar el avaúo
     * @param solicitante solicitante de la solicitud
     * @param predios predios seleccionados par aasociar al avaúo
     * @param usuario usuario que crea el avaúo
     * @return avaluo creado
     */
    public Avaluo crearAvaluoRadicacion(Avaluo avaluo, Solicitud solicitud,
        SolicitanteSolicitud solicitante, List<SolicitudPredio> predios, UsuarioDTO usuario);

    /**
     * Método que actualizar un avaúo comercial. Modifica los predios de la tabla SOLICITUD_PREDIO y
     * AVALUO_PREDIO
     *
     * @author christian.rodriguez
     * @param avaluo avaluo existente en la bd
     * @param solicitud solicitud asociada al avaluo
     * @param prediosAAsociar predios que se van a asociar al avaúo
     * @param prediosADesasociar predios que se van a desasociar del avaúo
     * @param usuario usuario que crea el avaúo
     * @return avaluo modificado
     */
    public Avaluo actualizarAvaluoRadicacion(Avaluo avaluo, Solicitud solicitud,
        List<SolicitudPredio> prediosAAsociar, List<AvaluoPredio> prediosADesasociar,
        UsuarioDTO usuario);

    /**
     * Método para obterner un avaluo correspondiente a un número de sec_radicado determinado.
     *
     *
     * @author felipe.cadena
     *
     * @param secRadicado
     * @return
     */
    public Avaluo buscarAvaluoPorSecRadicado(String secRadicado);

    /**
     *
     * Método para obtener las solicitud documentación asociadas a un sec_radicado
     *
     * @author felipe.cadena
     *
     * @param idSolicitud - id de la solicitud del sec_radicado
     * @param idTramite - id del tramite asociado el sec_radicado
     * @return
     */
    public List<SolicitudDocumentacion> obtenerSolicitudDocumentacionPorSecRadicadoAgrupadas(
        Long idSolicitud, Long idTramite);

    /**
     * Método que busca un avaúo predio en la db
     *
     * @author christian.rodriguez
     * @param idAvaluoPredio id del avaúo predio a buscar
     * @return avaúo predio encontrado o null si no encontró
     */
    public AvaluoPredio obtenerAvaluoPredioPorId(Long idAvaluoPredio);

    /**
     * Método que guarda o actualiza un avaúo predio en la db
     *
     * @author christian.rodriguez
     * @param avaluoPredio avaúo predio a modificar
     * @return avaúo predio modificado
     */
    public AvaluoPredio guardarActualizarAvaluoPredio(AvaluoPredio avaluoPredio);

    /**
     * Métedo para consultar los predios asociados a un avaluo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @return
     */
    public List<AvaluoPredio> buscarAvaluoPredioPorIdAvaluo(Long idAvaluo);

    /**
     * Método que elimina un avaúo de la bd. Borra los avaúo predios asociados y los desvincula de
     * la tabla de solicitud predio para que puedan ser usados en otro avaúo
     *
     * @author christian.rodriguez
     * @param avaluoAEliminar avaúo a eliminar
     * @param solicitudId id de la solicitud a la que está asociado el avaúo
     */
    public boolean eliminarAvaluoRadicacion(Avaluo avaluoAEliminar, Long solicitudId);

    /**
     * Método que avanza avaluos. Crea el proceso asociado al tramite, asigna el id del proceso al
     * tramite y reasigna la numeración de los secradicados que no se han avanzado
     *
     * @author christian.rodriguez
     * @param avaluosAAvanzar lista con los avaúos a avanzar
     * @return true si se pudieron avanzar los avaúos, false en otro caso
     */
    public boolean avanzarAvaluosRadicacion(List<Avaluo> avaluosAAvanzar, Long solicitudId);

    /**
     * Método que consulta los documentos faltantes de una solicitud de avaluo
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitud - Id de la Solicitud
     * @param banderaComGenerado - Bandera que indica si el comunicado de documentos faltantes ya
     * fue generado </br>
     * <b>true: </b>Si el comunicado ya fue generado</br>
     * <b>false: </b> Si el comunicado no ha sido generado
     *
     *
     * @return - Lista de documentos faltantes
     */
    public List<SolicitudDocumentacion> buscarDocsFaltantesSolicitudAvaluo(
        Long idSolicitud, boolean banderaComGenerado);

    /**
     * Método que consulta los documentos faltantes de una solicitud asociados a un comunicado
     * generado
     *
     * @author rodrigo.hernandez
     *
     * @return Lista de documentos faltantes asociados a un comunicado
     */
    public List<SolicitudDocumentacion> buscarDocsFaltantesSolicitudAvaluoComGenerado(
        Long idSolicitud);

    /**
     * Método que consulta los documentos faltantes de una solicitud cuando aun no ha sido generado
     * un comunicado
     *
     * @author rodrigo.hernandez
     *
     * @return Lista de documentos faltantes
     */
    public List<SolicitudDocumentacion> buscarDocsFaltantesSolicitudAvaluoSinComGenerado(
        Long idSolicitud);

    /**
     *
     * Método que busca una solicitud por su número
     *
     * @author rodrigo.hernandez
     *
     * @param numero - Numero de la solicitud
     * @return Solicitud
     */
    public Solicitud buscarSolicitudPorNumero(String numero);

    /**
     * Método para actualizar la informacion de una solicitud en avaluos comerciales
     *
     * @author rodrigo.hernandez
     *
     * @param solicitud
     * @return
     */
    public Solicitud guardarActualizarSolicitudAvaluoComercial(Solicitud solicitud);

    /**
     * Método para actualizar/guardar la lista de solicitanteSolicituds de una solicitud de avaluo
     *
     * @param solicitanteSolicituds - Lista de solicitanteSolicituds
     * @param solicitudId - Id de la solicitud
     * @return Lista de solicitanteSolicituds actualizada
     */
    public List<SolicitanteSolicitud> guardarActualizarSolicitanteSolicituds(
        List<SolicitanteSolicitud> listaSolicitanteSolicituds, Long solicitudId);

    /**
     * Método para consultar los departamentos y municipios relacionados con avaluo. La consulta se
     * hace a partir del sec radicado del avaluo.
     *
     * @author rodrigo.hernandez
     *
     * @param idAvaluo
     * @return
     */
    public List<Municipio> buscarDepartamentosMunicipiosAvaluo(
        Long idAvaluo);

    /**
     * Obtiene un Avaluo dado su id. Hace fetch del trámite relacionado, de la solicitud, y de los
     * solicitantes (que en el caso de un avaúo es uno solo)
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return null si no lo encuentra
     */
    public Avaluo obtenerInfoEncabezadoAvaluoPorId(Long avaluoId);

    /**
     * Obtiene los Predio asociados a un Avaluo.
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return null o lista con al menos un Predio
     */
    public List<Predio> obtenerPrediosDeAvaluo(Long avaluoId);

    /**
     * Método para consultar avaluos basado en un filtro de datos relacionados
     *
     * @author felipe.cadena
     *
     * @param filtro - Conjunto de datos para definir la busqueda
     * @return
     */
    public List<Avaluo> buscarAvaluosPorFiltro(FiltroDatosConsultaAvaluo filtro);

    /**
     * Método para consultar conttratos administrativos basado en un filtro de informacion del
     * solicitante relacionado al contrato y en algunos datos relacionados directamente al contrato.
     *
     * @author felipe.cadena
     *
     * @param filtroSolicitante - Conjunto de datos relacionados al solicitante
     * @param filtroContrato - Conjunto de datos relacionados al {@link ContratoInteradministrativo}
     * @return
     */
    public List<ContratoInteradministrativo> buscarContratosInteradministrativosPorFiltros(
        FiltroDatosConsultaSolicitante filtroSolicitante,
        FiltroDatosConsultaContratoInteradministrativo filtroContrato);

    /**
     * Método que guarda o actualiza un contrato interadministrativo
     *
     * @author christian.rodriguez
     * @param contrato {@link ContratoInteradministrativo} contrato a modificar/guardar
     * @param entidadSolicitante {@link Solicitante} entidad que solicitó el contrato, no se tendrá
     * en cuenta este parametro cuando se está actualizando
     * @param usuario {@link UsuarioDTO} usuario que creo el contrato, no se tendrá en cuenta este
     * parametro cuando se está actualizando
     * @return
     */
    public ContratoInteradministrativo guardarActualizarContratoInteradminsitrativo(
        ContratoInteradministrativo contrato, Solicitante entidadSolicitante, UsuarioDTO usuario);

    /**
     * Método que busca el {@link ContratoInteradministrativo} con el id asociado
     *
     * @author christian.rodriguez
     * @param idContrato id del contrato a buscar
     * @return contrato encontrado o null si no se encuentran resultados
     */
    public ContratoInteradministrativo obtenerContratoInteradministrativoPorIdConEntidad(
        Long idContrato);

    /**
     * Método que busca el {@link ContratoInteradministrativo} con el id asociado. Trae las
     * consignaciónes asociadas
     *
     * @author christian.rodriguez
     * @param idContrato id del contrato a buscar
     * @return contrato encontrado con las consignaciones asociadas o null si no se encuentran
     * resultados
     */
    public ContratoInteradministrativo obtenerContratoInteradministrativoPorIdConConsignaciones(
        Long idContrato);

    /**
     * Busca los avaúos pertenezcan al contrato dado, aplicando los filtros de datos del parámetro.
     *
     * @author pedro.garcia
     * @param idContrato
     * @param filtroConsulta Filtro para la consulta
     * @return
     */
    public List<Avaluo> consultarAvaluosDeContrato(Long idContrato,
        FiltroDatosConsultaAvaluo filtroConsulta);

    /**
     * Busca las solicitudes que pertenezcan al contrato dado
     *
     * @author pedro.garcia
     * @param idContrato
     * @return
     */
    public List<Solicitud> consultarSolicitudesDeContrato(Long idContrato);

    /**
     * Método para guardar o actualizar la entidad {@link ContratoInteradminisAdicion}
     *
     * @author felipe.cadena
     *
     * @param adicion
     * @return - retorna la entidad actualizada en base de datos
     */
    public ContratoInteradminisAdicion guardarActualizarContratoAdicion(
        ContratoInteradminisAdicion adicion);

    /**
     * Método para buscar adiciones relacionadas a un {@link ContratoInteradministrativo}
     *
     * @author felipe.cadena
     *
     * @param idContrato
     * @return
     */
    public List<ContratoInteradminisAdicion> buscarAdicionesPorIdContrato(Long idContrato);

    /**
     * Método para buscar avaluadores
     *
     * @author rodrigo.hernandez
     *
     * @param filtroDatos
     * @return
     */
    public List<VProfesionalAvaluosContrato> buscarAvaluadoresPorFiltro(
        FiltroDatosConsultaProfesionalAvaluos filtroDatos);

    /**
     * Obtiene el resultado de la sumatoria de los valores de las adiciones echas en un contrato
     *
     * @author christian.rodriguez
     * @param idContrato id del contrato del cual se quiere obtener las adiciones
     * @return valor double con el total de la sumatoria de las adiciones realizadas a un contrato
     */
    public Double calcularTotalAdicionesPorIdContratoInteradministrativo(Long idContrato);

    /**
     * Obtiene el valor comprometido en ejecución para un contrato administrativo
     *
     * @author christian.rodriguez
     * @param idContrato id del contrato del cual se quiere obtener el valor total de adiciones
     * @return valor double con el total adiciones del contrato especificado
     */
    public Double calcularValorComprometidoEjecucionPorIdContratoInteradministrativo(Long idContrato);

    /**
     * Actualiza un avaúo
     *
     * @author pedro.garcia
     * @param avaluo
     */
    public void actualizarAvaluo(Avaluo avaluo);

    /**
     * Busca las adiciones relacionadas a un contrato relacionado a un avaluador
     *
     * @author felipe.cadena
     * @param idContrato
     * @return
     */
    public List<ProfesionalContratoAdicion> buscarAdicionesPorIdProfesionalContrato(Long idContrato);

    /**
     * Obtiene el valor total de las adiciones relacionadas a un contrato
     *
     * @param idContrato
     * @return - El total de las adiciones del contrato
     */
    public Double obtenerTotalAdicionesPorIdProfesionalContrato(Long idContrato);

    /**
     * Método para buscar un contrato de un avaluador
     *
     * @author rodrigo.hernandez
     *
     * @param idContrato - Id del contrato
     *
     * @return Contrato de avaluador
     */
    public ProfesionalAvaluosContrato obtenerContratoAvaluadorPorId(Long idContrato);

    /**
     * obtiene un ProfesionalAvaluo dado su id
     *
     * @author pedro.garcia
     * @param profesionalAvaluoId
     * @return
     */
    public ProfesionalAvaluo obtenerProfesionalAvaluoPorId(Long profesionalAvaluoId);

    /**
     * obtiene un ProfesionalAvaluo dado su id. Carga el departamento, municipio y territorial
     * asociados
     *
     * @author christian.rodriguez
     * @param profesionalAvaluoId id del profesional a consultar
     * @return {@link ProfesionalAvaluo} encontrado o null si no se obtuvieron resultados
     */
    public ProfesionalAvaluo obtenerProfesionalAvaluoPorIdConAtributos(Long profesionalAvaluoId);

    /**
     * obtiene un ProfesionalAvaluosContrato dado su id. Carga el profesional, lista de adiciones,
     * lista de pagos, lsita de cesiones asocaidas
     *
     * @author christian.rodriguez
     * @param contratoId id del contrato del avaluaador
     * @return {@link ProfesionalAvaluosContrato} encontrado o null si no se obtuvieron resultados
     */
    public ProfesionalAvaluosContrato obtenerProfesionalAvaluoContratoPorIdConAtributos(
        Long contratoId);

    /**
     * Método para calcular el valor ejecutado del contrato
     *
     * @author rodrigo.hernandez
     *
     * @param idContrato - Id del contrato del profesional avaluador
     * @return
     */
    public Double calcularValorEjecutadoPAContrato(Long idContrato);

    /**
     * Método para calcular la fecha final del contrato teniendo en cuenta sus adiciones
     *
     * @author rodrigo.hernandez
     *
     * @param idContrato - Id del contrato del profesional avaluador
     * @return
     */
    public Date calcularFechaFinalPAContratoAdiciones(Long idContrato);

    /**
     * Método para consultar la lista de cesiones asociadas a un contrato activo
     *
     * @author rodrigo.hernandez
     *
     * @param idContrato - Id del contrato activo
     * @return
     */
    public List<ProfesionalContratoCesion> obtenerCesionesDeContratoActivo(Long idContrato);

    /**
     * Método para consultar los avaluos en proceso relacionados a un avaluador
     *
     * @author felipe.cadena
     *
     * @param idContrato - Id del profesional de avaluos.
     * @return
     */
    public List<Avaluo> consultarAvaluosEnProcesoAvaluador(Long idContrato);

    /**
     * Método que consulta los pagos parafiscales {@link ProfesionalContratoPago} asociados a un
     * contrato {@link ProfesionalAvaluosContrato}
     *
     * @author christian.rodriguez
     * @param idProfesionalAvaluoContrato Identificar del contrato del profesional de avaúos
     * @return
     */
    public List<ProfesionalContratoPago> consultarPagosParafiscalesPorProfesionalAvaluoContratoId(
        Long idProfesionalAvaluoContrato);

    /**
     * Actualiza los datos de un registro de tipo ProfesionalAvaluo
     *
     * @author pedro.garcia
     * @param avaluador
     */
    public void actualizarProfesionalAvaluos(ProfesionalAvaluo avaluador);

    /**
     * Actualiza los datos de un {@link ProfesionalAvaluo}.
     *
     * @author christian.rodriguez
     * @param avaluador profesional de avaluos que se desea actualizar
     * @param idProfesion identificador de la {@link Profesion}
     * @param usuario usuario que crea el registro
     */
    public ProfesionalAvaluo actualizarProfesionalAvaluos(ProfesionalAvaluo avaluador,
        String idProfesion, UsuarioDTO usuario);

    /**
     * Obtiene los contratos anteriores para un avaluador {@link ProfesionalAvaluo}
     *
     * @author christian.rodriguez
     * @param idProfesional id del avaluador
     * @return contratos anteriores del avaluador seleccionado
     */
    public List<ProfesionalAvaluosContrato> obtenerProfesionalAvaluosContratoAnterioresPorIdProfesional(
        Long idProfesional);

    /**
     * Método para cancelar el avaúo comercial, cambia el estado del tramite relacionado al avaúo a
     * CANCELADO y crea un registro AvaluoMovimiento con la información acerca de la cancelación.
     *
     * @author felipe.cadena
     * @param avaluo - Avaúo a cancelar
     * @param justificacion - Justificación de la cancelación
     * @param usuario - usuario que realiza la cancelación
     * @return
     */
    public Avaluo cancelarAvaluo(Avaluo avaluo, String justificacion, String otraJustificacion,
        UsuarioDTO usuario);

    /**
     * Consulta los pagos causados realizados a un contrato de avaluador.
     *
     * @author christian.rodriguez
     * @param idContrato Identificador del contrato
     * @return Lista de {@link ProfesionalContratoPago} causados asociados al contrato
     */
    public List<ProfesionalContratoPago> consultarProfesionalAvaluoContratoPagosCausadosPorIdContrato(
        Long idContrato);

    /**
     * Consulta los pagos proyectados realizados a un contrato de avaluador.
     *
     * @author christian.rodriguez
     * @param idContrato Identificador del contrato
     * @return Lista de {@link ProfesionalContratoPago} proyectados asociados al contrato
     */
    public List<ProfesionalContratoPago> consultarProfesionalAvaluoContratoPagosProyectadosPorIdContrato(
        Long idContrato);

    /**
     * Consulta los avaluadores asociados al sec radicado
     *
     * @author christian.rodriguez
     * @param secRadicado Cadena de texto con el sec radicado del avaluo a consultar
     * @return Lista con los avaluadores asocaidos al avaluo
     */
    public List<ProfesionalAvaluo> consultarAvaluadoresPorSecRadicado(String secRadicado);

    /**
     * Método para buscar los avaluadores relacionados al patron de busqueda nombreProfesional
     *
     * @author felipe.cadena
     *
     * @param nombreProfesional
     * @return
     */
    public List<VProfesionalAvaluosCarga> buscarProfesionalesAvaluosPorNombre(
        String nombreProfesional,
        String codigoTerritorial);

    /**
     * Método para consultar avaúos realizados asociados a un contrato de profesional de avaúos. si
     * el tipo de actividad es null retorna los avaúos relacionados a todas las actividades.
     *
     * @author felipe.cadena
     *
     * @param idContrato
     * @param tipoActividad
     * @param estadoTramite
     *
     * @return
     */
    public List<Avaluo> consultarAvaluosRealizadosContratoPA(Long idContrato, String tipoActividad,
        String estadoTramite);

    /**
     * Consulta los avaúos asociados a un {@link ProfesionalAvaluosContrato} y a un
     * {@link ProfesionalAvaluo} que no esten asociados a ningún {@link ProfesionalContratoPago}
     *
     * @author christian.rodriguez
     * @param idContrato id del {@link ProfesionalAvaluosContrato}
     * @param idAvaluador id del avaluador {@link ProfesionalAvaluo}
     * @return Lista con los {@link Avaluo} que cumplan las condiciones dadas
     */
    public List<Avaluo> consultarAvaluosSinPagoPorContratoYAvaluador(
        Long idContrato, Long idAvaluador);

    /**
     * Guarda o acutaliza un {@link ProfesionalContratoPago}
     *
     * @author christian.rodriguez
     * @param pago pago a modificar
     * @param contrato contrato al que se va a asociar el pago
     * @param avaluo avaluo que se asociaran al pago
     * @param usuario activo en el sistema
     * @return pago actualziado/guardado o null si no se pudo compeltar la transacción
     */
    public ProfesionalContratoPago guardarActualizarProfesionalContratoPago(
        ProfesionalContratoPago pago, ProfesionalAvaluosContrato contrato,
        List<Avaluo> avaluo, UsuarioDTO usuario);

    /**
     * método que consulta los avaluos que están asociados a un pago de un profesional
     * {@link ProfesionalContratoPago}
     *
     * @author christian.rodriguez
     * @param idContratoPago id del pago {@link ProfesionalContratoPago}
     * @return Lista con los {@link Avaluo} asociados al pago
     */
    public List<Avaluo> consultarAvaluosPorProfesionalContratoPagoId(
        Long profesionalContratoPagoId);

    /**
     * Método para consultar avaúos realizados asociados profesional de avaúos. en un aÃ±o
     * particular, no toma en cuenta si los avaúos estan relacionados a un contrato de profesional
     * de avaúos o no.
     *
     * @author felipe.cadena
     *
     * @param idContrato
     * @param tipoActividad
     * @param estadoTramite
     * @param anio
     *
     * @return
     */
    public List<Avaluo> consultarAvaluosProfesionalPorAnio(Long idAvaluador, String tipoActividad,
        String estadoTramite, Integer anio);

    /**
     * Método para consultar el historico de los avaúos realizados por un profesional agrupados por
     * aÃ±o con información acerca del estado de los avaúos y su calificación.
     *
     * @author felipe.cadena
     *
     * @param idAvaluador
     * @return Lista de Object[] donde cada item del arreglo contiene un campo de la consulta de
     * historico.  <br/>
     * Object[0] - AÃ±o<br/>
     * Object[1] - Total avaúos<br/>
     * Object[2] - Total avaúos aprobados<br/>
     * Object[3] - Total avaúos no aprobados<br/>
     * Object[4] - Total avaúos control de calidad<br/>
     * Object[5] - Promedio de calificación<br/>
     * Object[6] - Promedio de calificación control de calidad<br/>
     */
    public List<Object[]> consultarHistoricoAvaluosProfesional(Long idAvaluador);

    /**
     * Obtiene los avaúos que tienen relacionados los trámites cuyos ids vienen en la lista. Se
     * cargan los avaluadores y los predios de cada avaúo.}
     *
     * @author pedro.garcia
     * @param idsTramites Lista de ids de trámites
     * @return
     */
    public List<Avaluo> obtenerAvaluosPorIdsTramite(List<Long> idsTramites);

    /**
     * consulta las {@link AvaluoAsignacionProfesional} asociadas a un número de radicado de una
     * solicitud
     *
     * @author christian.rodriguez
     * @param radicado String con el número de radicado de una solicitud
     * @return Lista con las {@link AvaluoAsignacionProfesional} si se encontraron coincidencias,
     * null en otro caso
     */
    public List<AvaluoAsignacionProfesional> consultarAvaluoAsignacionProfesionalPorSolicitudConOrdenPractica(
        String radicado);

    /**
     *
     * Método para buscar solicitudes a partir de los datos de un solicitante
     *
     * @author rodrigo.hernandez
     *
     * @param filtroDatosSolicitud - Filtros de datos para búsqueda de solicitud
     * @return
     */
    public List<Solicitud> buscarSolicitudesPorSolicitante(
        FiltroDatosSolicitud filtroDatosSolicitud);

    /**
     * Método para consultar avaúos relacionados a un profesional y un sec_radicado que tengan
     * asignación y aun no tengan orden de practica.
     *
     * @author felipe.cadena
     *
     * @param idProfesional
     * @param secRadicado
     * @return
     */
    public List<Avaluo> consultarAvaluosAsignacion(Long idProfesional, String secRadicado);

    /**
     * Método para desasignar un avaluador de un avaúo determinado
     *
     * @author felipe.cadena
     *
     * @param avaluo - el avaluo debe tener asociado la lista de asignaciones y los profesionales de
     * cada asignación
     * @param idAvaluador - id del avaluador que se desea eliminar la asignación
     * @return
     */
    public AvaluoAsignacionProfesional desasignarAvaluador(Avaluo avaluo, Long idAvaluador);

    /**
     * Método para buscar los avaluadores relacionados a una territorial
     *
     * @author felipe.cadena
     * @param codigoTerritorial
     * @return
     */
    public List<ProfesionalAvaluo> buscarAvaluadoresPorTerritorial(String codigoTerritorial);

    /**
     * Método para buscar los {@link ProfesionalAvaluo} por un filtro de tipo
     * {@link FiltroDatosConsultaProfesionalAvaluos}
     *
     * @author christian.rodriguez
     * @param filtro {@link FiltroDatosConsultaAvaluo} filtro con los parametros de busqueda
     * @return lista con los {@link ProfesionalAvaluo} que cumplan con las condiciones del filtro
     * parametro o null si no se encuentra nada
     */
    public List<ProfesionalAvaluo> buscarProfesionalesAvaluosPorFiltro(
        FiltroDatosConsultaProfesionalAvaluos filtro);

    /**
     * Método que elimina un {@link ProfesionalAvaluo} siempre y cuando este no tenga asociadas
     * {@link AvaluoAsignacionProfesional}
     *
     * @param profesional {@link ProfesionalAvaluo} a eliminar
     */
    public boolean eliminarProfesionalAvaluos(ProfesionalAvaluo profesional);

    /**
     * Método para guardar o actualizar controles de calidad de los avaluos
     *
     * @param ccAvaluo {@link ControlCalidadAvaluo} a guardar/actualziar
     * @return {@link ControlCalidadAvaluo} modificado
     */
    public ControlCalidadAvaluo crearActualizarControlCalidadAvaluo(
        ControlCalidadAvaluo ccAvaluo, UsuarioDTO usuario);

    /**
     * Busca el {@link ControlCalidadAvaluoRev} que tenga el id especificado
     *
     * @author christian.rodriguez
     * @param idControlCalidadAvaluoRev id del {@link ControlCalidadAvaluoRev}
     * @return {@link ControlCalidadAvaluoRev} encontrado o null si no se encontró
     */
    public ControlCalidadAvaluoRev obtenerControlCalidadAvaluoRevPorId(
        Long idControlCalidadAvaluoRev);

    /**
     * Busca el {@link Avaluo} que tenga el id especificado.
     *
     * @author christian.rodriguez
     * @param idAvaluo id del {@link Avaluo}
     * @return {@link Avaluo} encontrado o null si no se encontró
     */
    public Avaluo obtenerAvaluoPorIdConAtributos(Long idAvaluo);

    /**
     * Busca el {@link Avaluo} que tenga el secradicado especificado.
     *
     * @author christian.rodriguez
     * @param secRadicado secradicado del {@link Avaluo}
     * @return {@link Avaluo} encontrado o null si no se encontró
     */
    public Avaluo consultarAvaluoPorSecRadicadoConAtributos(String secRadicado);

    /**
     * Método para obtener un profesional de avaúos por medio de su númeo de identificación
     *
     * @author felipe.cadena
     * @param identificacion
     * @return
     */
    public ProfesionalAvaluo obtenerProfesionaAvaluosPorIdentificacion(String identificacion);

    /**
     * M[etodo para obtener control calidad por medio de su Id
     *
     * @author felipe.cadena
     *
     * @param IdControlCalidad
     * @return
     */
    public ControlCalidadAvaluo obtenerControlCalidadAvaluoPorId(
        Long IdControlCalidad);

    /**
     * Guarda o actualiza un {@link AvaluoMovimiento}
     *
     * @author christian.rodriguez
     * @param movimiento {@link AvaluoMovimiento} que se va a guardar/actualizar
     * @param usuario usuario que crea el movimiento
     * @return objeto modificado
     */
    public AvaluoMovimiento guardarActualizarAvaluoMovimiento(
        AvaluoMovimiento movimiento, UsuarioDTO usuario);

    /**
     * Método que envia un correo electrónico cuando se realiza una solicitud de ampliación,
     * cancelación y suspención de un trámite de avaluo
     *
     * @author christian.rodriguez
     * @param destinatario destinatario de l correo, debe ser el coordinadro git avaluos cuando el
     * avaluo es sede central o el director de la territorial cuando el avaluo es de territorial
     * @param movimiento {@link AvaluoMovimiento} solicitud de movimiento
     * @param usuarioEjecutor usuario que ejecuta la solicitud
     * @return true si el correo se pudo enviar, false si no
     */
    public boolean enviarCorreoElectronicoSolicitudMovimientoAvaluo(
        UsuarioDTO destinatario, AvaluoMovimiento movimiento,
        UsuarioDTO usuarioEjecutor);

    /**
     * Método para cargar los detalles de los documentos faltantes de una solicitud a partir del id
     * de la solicitud
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitud
     * @return
     */
    public List<DetalleDocumentoFaltante> obtenerDetallesDocumentosFaltantesPorIdSolicitud(
        Long idSolicitud);

    /**
     * Método para guardar/actualizar un objeto de tipo DetalleDocumentoFaltante
     *
     * @author rodrigo.hernandez
     *
     * @param ddf - DetalleDocumentoFaltante a guardar/actualizar
     *
     * @return
     */
    public DetalleDocumentoFaltante guardarActualizarDetalleDocumentoFaltante(
        DetalleDocumentoFaltante ddf);

    /**
     * Método que busca la lista de anexos a partir del id de una solicitud y un tipo de documento
     *
     * @return
     */
    public List<SolicitudDocumentacion> obtenerListaSolDocsPorSolicitudYTipoDocumento(
        Long solicitudId, Long tipoDocumentoId);

    /**
     * Busca los avaúos cuyo trámite asociado tenga id igual a alguno de los contenidos en el
     * arreglo idsTramites. Hace fetch de los datos que se necesiten para las pantallas del CU.
     *
     * @author pedro.garcia
     * @cu CU-SA-AC-009
     *
     * @param idsTramites arreglo con los id de trámite
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters campos para filtrar
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaÃ±o del conjunto resultado
     * @return
     */
    public List<Avaluo> obtenerAvaluosPorIdsParaOrdenPractica(
        long[] idsTramites, String sortField, String sortOrder, Map<String, String> filters,
        int... contadoresDesdeYCuantos);

    /**
     * Obtiene -si existe- el contrato interadministrativo al que está asociado el avaúo
     *
     * @author pedro.garcia
     * @param idAvaluo
     * @return
     */
    public ContratoInteradministrativo obtenerContratoInteradministrativoPorAvaluo(Long idAvaluo);

    /**
     * Llama a al procedimiento almacendao para calcular el siguiente número en la numeración de
     * ordenes de práctica
     *
     * @pedro.garcia
     * @return Cadena que contiene el siguiente número en la numeración
     */
    public String obtenerNumeroOrdenPractica();

    /**
     * Obtiene un Avaluo dado su id, haciendo fetch del trámite asociado y la solicitud asociada al
     * trámite.
     *
     * @author pedro.garcia
     * @param idAvaluo
     * @return
     */
    public Avaluo obtenerAvaluoPorIdConTramiteYSolicitud(Long idAvaluo);

    /**
     * Método para obtener el ultimo documento radicado relacionado a una solicitud
     *
     * @author felipe.cadena
     * @param idSolicitud
     */
    public Documento obtenerDocumentoParaRadicacionDefinitiva(Long idSolicitud);

    /**
     * Método para eliminar un objeto de tipo DetalleDocumentoFaltante
     *
     * @author rodrigo.hernandez
     *
     * @param detalleDocumentoFaltante
     */
    public void borrarDetalleDocumentoFaltante(DetalleDocumentoFaltante detalleDocumentoFaltante);

    /**
     * Método para guardar/actualizar una lista de objetos de tipo DetalleDocumentoFaltante
     *
     * @author rodrigo.hernandez
     *
     * @param listaDetalleDocumentosFaltantes
     */
    public void guardarActualizarDetalleDocumentoFaltantes(
        List<DetalleDocumentoFaltante> listaDetalleDocumentosFaltantes);

    /**
     * guarda o actualiza una OrdenPractica
     *
     * @author pedro.garcia
     * @param ordenPracticaGuardada
     * @return el id de la OrdenPractica insertada o actualizada
     */
    public Long guardarActualizarOrdenPractica(OrdenPractica ordenPracticaGuardada);

    /**
     * Método que consulta el método de no aprobación de un avaúo
     *
     * @author christian.rodriguez
     * @param idavaluo identificador del avaúo
     * @return {@link Documento} encontrado o null si no se encuentra anda
     */
    public Documento consultarOficioDeNoAprobacionAvaluoPorIdAvaluo(Long idavaluo);

    /**
     * Método para consultar la lista de TramiteDocumentacions asociados a varios tramites
     *
     * @author rodrigo.hernandez
     *
     *
     * @return
     */
    public List<TramiteDocumentacion> consultarDocumentosTramitePorIdsTramites(
        List<Long> listasIdsTramites);

    /**
     * Método para buscar la lista de documentos relacionados con los TramiteDocumentacion de un
     * radicado/solicitud de Avaluo Comercial. </br>
     * <b>TENER EN CUENTA:</b></br>
     * Solo trae un documento sin importar el número de TramiteDocumentacions con los que se
     * encuentre asociado
     *
     * @author rodrigo.hernandez
     *
     * @param idSolicitud
     *
     * @return
     */
    public List<Documento> buscarDocsDeTramiteDocumentacionsPorSolicitudId(
        Long idSolicitud);

    /**
     * Método para consultar todos los controles de calidad asociados a un avaúo y a la asignación
     * actual del avaúo
     *
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @return
     *
     */
    public List<ControlCalidadAvaluo> consultarControlCalidadPorAvaluoId(Long idAvaluo);

    /**
     * Actualiza los registros de la tabla Avaluo_Asignacion_Profesional, cambiando el valor del id
     * de la orden de práctica. Se pasa como parámetro el número de la orden de práctica porque es
     * el dato de esta que se tiene disponible en el momento donde se usa este método.
     *
     * @author pedro.garcia
     * @param numeroOrdenPractica número de la orden de práctica
     * @param avaluosOrdenPractica lista de los id de los Avaluo que hacen parte de la orden de
     * práctica
     * @return false si ocurrió algún error
     */
    public boolean actualizarOrdenPracticaEnAvaluoAsignacionProfesional(
        String numeroOrdenPractica, List<Long> avaluosOrdenPractica);

    /**
     * Método para consultar la orden de practica asociada a un avaúo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @return
     */
    public OrdenPractica consultarOrdenPracticaPorAvaluoId(Long idAvaluo);

    /**
     * Método para obtener un objeto SolicitudDocumento a partir del id de la Solicitud
     *
     * @author rodrigo.hernandez
     *
     * @param solicitudId Id de la Solicitud
     * @param documentoId Id del documento
     * @return
     */
    public SolicitudDocumento obtenerSolicitudDocumentoPorSolicitudId(
        Long solicitudId);

    /**
     * Crea o actualiza un {@link AvaluoPermiso}.
     *
     * @author christian.rodriguez
     * @param permiso {@link AvaluoPermiso} que se va a crear/actualizar
     * @param usuario usuario que crea el objeto, si es actualización este atributo no será tomado
     * en cuenta (puede ser nulo)
     * @return {@link AvaluoPermiso} creado/modificado o null si no se pudo completar la tarea
     */
    public AvaluoPermiso guardarActualizarAvaluoPermiso(AvaluoPermiso permiso,
        UsuarioDTO usuario);

    /**
     * Borra el {@link AvaluoPermiso} especificado de la BD
     *
     * @author christian.rodriguez
     * @param avaluoPermisoId id del {@link AvaluoPermiso} que se desea borrar
     * @return true si se pudo borrar satisfactoriamente el objeto, false si no
     */
    public boolean borrarAvaluoPermisoPorId(Long avaluoPermisoId);

    /**
     * Método que consulta un {@link ProfesionalAvaluo} por su nombre de usuario ldap y su numero de
     * identificaicon ldap
     *
     * @author christian.rodriguez
     * @param usuarioSistema cadena de texto con el nombre de usuario del profesional en ldap
     * @param numeroIdentificacion cadena de texto con el numero de identificacion de usuario del
     * profesional en ldap
     * @return {@link ProfesionalAvaluo} encontrado o null si no se encuentra nada
     */
    public ProfesionalAvaluo consultarProfesionalAvaluosPorUsuarioSistemaYNumeroIdentificacion(
        String usuarioSistema, String numeroIdentificacion);

    /**
     * Método que crea/actualiza una {@link ControlCalidadAvaluoRev}
     *
     * @author christian.rodriguez
     * @param revision {@link ControlCalidadAvaluoRev} a crear/actualizar
     * @param usuario {@link UsuarioDTO} usuario que crea el registro, puede ser nulo cuando se esta
     * actualizando un objeto existente en db
     * @return objeto creado/actualizado o null si no se pudo realizar la operación
     */
    public ControlCalidadAvaluoRev guardarActualizarControlCalidadAValuoRev(
        ControlCalidadAvaluoRev revision, UsuarioDTO usuario);

    /**
     * Método que crea/actualiza una {@link AvaluoEvaluacion}
     *
     * @author felipe.cadena
     *
     * @param avaluoEvaluacion
     * @param usuario
     * @return
     */
    public AvaluoEvaluacion guardarActualizarAvaluoEvaluacion(
        AvaluoEvaluacion avaluoEvaluacion, UsuarioDTO usuario);

    /**
     * Método que consulta las {@link OrdenPractica} asociadas a un profesional de avaúos, si el
     * tipo de vinculación es prestación de servicios entonces se devuelven las ordenes con fecha
     * comprendida entre la duración del contrato activo. Si la vinculación es carrera
     * administrativa se consulta las ordenes con fecha comprendida en el aÃ±o vigente
     *
     * @author christian.rodriguez
     * @param profesionalId identificador del profesional de avaluos
     * @param tipoVinculacionProfesional cadena de texto con el codigo del tipo de la vinculacion
     * del profesional de avaluos. Corresponde a un valor del dominio PROFESIONAL_TIPO_VINCULA
     * @return Lista con las {@link OrdenPractica} o null si no se pudo realizar la consulta
     */
    public List<OrdenPractica> consultarOrdenesPracticaPorAvaluadorIdYTipoVinculacion(
        Long profesionalId, String tipoVinculacionProfesional);

    /**
     * Método que elimina una {@link OrdenPractica} por su id. Al borrar la {@link OrdenPractica} se
     * eliminara la asociación en el entity {@link AvaluoAsignacionProfesional}
     *
     * @author christian.rodriguez
     * @param ordenPracticaId identificador de la orden de practica
     * @return true si se pudo borrar correctamente l aorden, false si no
     */
    public boolean borrarOrdenPracticaPorId(Long ordenPracticaId);

    /**
     * Método que consulta los {@link Avaluo} que están asociados a una {@link OrdenPractica} y a un
     * {@link ProfesionalAvaluo}
     *
     * @author christian.rodriguez
     * @param profesionalId identificador del {@link ProfesionalAvaluo}
     * @param ordenPracticaId identificador de la {@link OrdenPractica}
     * @return lista con los {@link Avaluo} encontrados o null si no se obtuvieron resutlados
     */
    public List<Avaluo> consultarAvaluosPorOrdenPracticaIdYProfesionalId(
        Long profesionalId, Long ordenPracticaId);

    /**
     * Método que consulta los {@link Avaluo} que están asociados a una {@link OrdenPractica}
     *
     * @author christian.rodriguez
     * @param ordenPracticaId identificador de la {@link OrdenPractica}
     * @return lista con los {@link Avaluo} encontrados o null si no se obtuvieron resutlados
     */
    public List<Avaluo> consultarAvaluosPorOrdenPracticaId(Long ordenPracticaId);

    /**
     * Busca los avaúos cuyo trámite asociado tenga id igual a alguno de los contenidos en el
     * arreglo idsTramites. Descarta los avaúos para los que existe una asignación vigente para la
     * actividad para la que se va a hacer la asignación.
     *
     * Hace fetch de los datos que se necesiten para las pantallas del CU.
     *
     * @author pedro.garcia
     * @cu CU-SA-AC-003
     *
     * @param idsTramites arreglo con los id de trámite
     * @param actividadCodigo código de la actividad para la que se va a asignar
     * @param asignados determina si se buscan los que están sin asignación o ya asignados
     * @param sortField nombre del campo por el que se ordena la lista de resultados
     * @param sortOrder indica cómo se hace el ordenamiento
     * @param filters campos para filtrar
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaÃ±o del conjunto resultado
     * @return
     */
    public List<Avaluo> obtenerAvaluosPorIdsParaAsignacion(long[] idsTramites,
        String actividadCodigo, boolean asignados, String sortField, String sortOrder,
        Map<String, String> filters, int... contadoresDesdeYCuantos);

    /**
     * Método para consultar evaluaciones realacionadas a un avaúo y a una asignación
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @param idAsignacion
     * @return
     */
    public AvaluoEvaluacion consultarEvaluacionesPorAvaluoAsignacionConAtributos(
        Long idAvaluo, Long idAsignacion);

    /**
     * Método para consultar todos las variables de evaluacion de los avaluos.
     *
     * @author felipe.cadena
     *
     * @return
     */
    public List<VariableEvaluacionAvaluo> consultarTodasVariableEvaluacionAvaluo();

    /**
     * Método para consultar la asignación actual relacionada a un avaúo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @param actividad actividad asociada a la asociación, debe ser un codigo de
     * {@link EAvaluoAsignacionProfActi}
     * @return
     */
    public Asignacion consultarAsignacionActualAvaluo(Long idAvaluo, String actividad);

    /**
     * Método para calcular el número de devoluciones de un avaúo durante el control de calidad
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @param idAsignacion
     * @param actividad
     * @return
     */
    public Long calcularDevolucionesAvaluo(Long idAvaluo, Long idAsignacion, String actividad);

    /**
     * Método que desasocia trámites ({@link Avaluo}) de una {@link OrdenPractica}. Al desasociar
     * los avaluos, se les modificará el valor de plazo ejecución volviendolo 0
     *
     * @author christian.rodriguez
     * @param ordenAModificarid identificador de la {@link OrdenPractica} de la cual se van a
     * desasociar los trámites
     * @param tramitesADesasociar {@link Avaluo} a desasociar de la orden de practica
     * @return true si se realizó la desasignación correctamente, false en otro caso
     */
    public boolean desasociarTramitesDeOrdenDePractica(
        Long ordenAModificarId, Avaluo[] tramitesADesasociar);

    /**
     * Cuenta los avaúos cuyo trámite asociado tenga id igual a alguno de los contenidos en el
     * arreglo idsTramites. Descarta los avaúos para los que existe una asignación vigente para la
     * actividad para la que se va a hacer la asignación.
     *
     * @author pedro.garcia
     * @cu CU-SA-AC-003
     *
     * @param idsTramites arreglo con los id de trámite
     * @param actividadCodigo código de la actividad para la que se va a asignar
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaÃ±o del conjunto resultado
     * @return
     */
    public int contarAvaluosPorIdsParaAsignacion(long[] idsTramites,
        String actividadCodigo, int... contadoresDesdeYCuantos);

    /**
     * Método para calcular el número de dias que tardo realmente en ejecutarse un avaúo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @return
     */
    public Integer calcularTiempoEjecucionDias(Long idAvaluo);

    /**
     * Método para calcular los dias de atraso de un avaluo
     *
     * @author felipe.cadena
     *
     * @param idAvaluo
     * @return
     */
    public Integer calcularTiempoRetrasoDias(Long idAvaluo);

    /**
     * Cuenta los avaúos cuyo trámite asociado tenga id igual a alguno de los contenidos en el
     * arreglo idsTramites. Tiene en cuenta los que aparecen en la actividad de asignación
     * respectiva del proceso, ya se asignaron, pero aún no se mueven a la siguiente actividad.
     *
     *
     * @author pedro.garcia
     * @cu CU-SA-AC-003
     *
     * @param idsTramites arreglo con los id de trámite
     * @param actividadCodigo código de la actividad para la que se va a asignar
     * @param contadoresDesdeYCuantos opcionales. para determinar el tamaÃ±o del conjunto resultado
     * @return
     */
    public int contarAvaluosPorIdsConAsignacionSinMover(long[] idsTramites, String actividadCodigo,
        int... contadoresDesdeYCuantos);

    /**
     * Método que aprueba una lista de {@link Avaluo}.
     *
     * @author christian.rodriguez
     * @return true si no ocurrio ningún error el realizar la aprobación
     */
    public boolean aprobarAvaluos(List<Avaluo> avaluosAAprobar);

    /**
     * Método que consulta el {@link Avaluo} asociado a una impugnación o revisión ({@link Avaluo}
     * referenciado en el campo tramiteNumeroRadicacionRef de la solicitud)
     *
     * @author christian.rodriguez
     * @param avaluoId Identificador del avaúo del cual se quiere consultar el avaluo a impugnar o
     * revisar
     * @return {@link Avaluo} a impugnar o revisar o null si no se obtuvieron resultados
     */
    public Avaluo obtenerAvaluoPorIdParaImpugnacionRevision(Long avaluoId);

    /**
     * Hace la consulta sobre la vista VProfesionalAvaluosCarga de los que pertenezcan a la
     * territorial dada
     *
     * @author pedro.garcia
     * @param codigoTerritorial
     * @return
     */
    public List<VProfesionalAvaluosCarga> consultarProfesionalesAvaluosCargaPorTerritorial(
        String codigoTerritorial);

    /**
     * Retorna true si el profesional de avaluos con el id dado no fue asignado como avaluador de
     * alguno de los avaúos cuyo id esté en el arreglo.
     *
     * @author pedro.garcia
     * @cu CU-SA-AC-003
     *
     * @param profesionalAvaluosId
     * @param idsAvaluos
     * @return
     */
    public boolean validarAsignadoControlCalidadNoEsAvaluador(long profesionalAvaluosId,
        long[] idsAvaluos);

    /**
     * Retorna true si los profesionales de avaluos identificados con los ids en el arreglo son
     * quienes hicieron el avaúo
     *
     * @param idsProfesionalesAvaluos
     * @param idAvaluos
     * @return
     */
    public boolean validarAsignadosRevisionSonAvaluadores(long[] idsProfesionalesAvaluos,
        Long idAvaluos);

    /**
     * Inserta o modifica un objeto Asignacion en la BD
     *
     * @author pedro.garcia
     * @param asignacion
     * @return
     */
    public Asignacion guardarActualizarAsignacion(Asignacion asignacion);

    /**
     * Inserta o modifica los AvaluoAsignacionProfesional que vienen en la lista. No devuelve los
     * objetos.
     *
     * @author pedro.garcia
     * @param newAsignaciones
     * @return true si no hay error
     */
    public boolean guardarAvaluosAsignacionProfesional(
        List<AvaluoAsignacionProfesional> newAsignaciones);

    /**
     * Método que consulta el {@link ComiteAvaluo} asociado a un {@link Avaluo}
     *
     * @author christian.rodriguez
     * @param avaluoId Identificador del {@link Avaluo} del cual se quiere consultar el comite
     * @return {@link ComiteAvaluo} encontrado o null si no se obtuvieron resultados
     */
    public ComiteAvaluo consultarComiteAvaluoPorIdAvaluo(Long avaluoId);

    /**
     * Método para guardar y actualizar zonas del avaúo
     *
     * @author felipe.cadena
     *
     * @param detalle
     * @return
     */
    public AvaluoPredioZona guardarActualizarAvaluoPredioZona(AvaluoPredioZona detalle,
        UsuarioDTO usuario);

    /**
     * Método para guardar y actualizar cultivos del avaúo
     *
     * @author felipe.cadena
     *
     * @param detalle
     * @return
     */
    public AvaluoPredioCultivo guardarActualizarAvaluoPredioCultivo(AvaluoPredioCultivo detalle,
        UsuarioDTO usuario);

    /**
     * Método para guardar y actualizar construcciones del avaúo
     *
     * @author felipe.cadena
     *
     * @param detalle
     * @return
     */
    public AvaluoPredioConstruccion guardarActualizarAvaluoPredioConstruccion(
        AvaluoPredioConstruccion detalle, UsuarioDTO usuario);

    /**
     * Método para guardar y actualizar maquinaria del avaúo
     *
     * @author felipe.cadena
     *
     * @param detalle
     * @return
     */
    public AvaluoPredioMaquinaria guardarActualizarAvaluoPredioMaquinaria(
        AvaluoPredioMaquinaria detalle, UsuarioDTO usuario);

    /**
     * Método para guardar y actualizar anexos del avaúo
     *
     * @author felipe.cadena
     *
     * @param detalle
     * @return
     */
    public AvaluoPredioAnexo guardarActualizarAvaluoPredioAnexo(AvaluoPredioAnexo detalle,
        UsuarioDTO usuario);

    /**
     * Método que crea/actualiza un {@link ComiteAvaluo}. Si se está creando el comité las listas de
     * {@link ComiteAvaluoParticipante} y de {@link ComiteAvaluoPonente} se actualizan.
     *
     * @author christian.rodriguez
     * @param comiteAvaluo {@link ComiteAvaluo} que se desea guardar/actualizar
     * @param idAvaluo identificador del {@link Avaluo} asociado al comite
     * @param usuario {@link UsuarioDTO} que crea el registro. Puede ser nulo cuando se está
     * actualizando el comité
     * @return {@link ComiteAvaluo} modificado o null si ocurrio algún error
     */
    public ComiteAvaluo guardarActualizarComiteAvaluo(
        ComiteAvaluo comiteAvaluo, Long idAvaluo, UsuarioDTO usuario);

    /**
     * Método para obtener los avaluadores asociados a un interventor
     *
     * @author rodrigo.hernandez
     *
     * @param idInterventor - Id del interventor
     * @param banderaEsAvaluadorExterno - bandera que indica si se estan buscando avaluadores
     * externos o por nomina
     */
    public List<ProfesionalAvaluo> obtenerAvaluadoresPorInterventorId(
        Long idInterventor, boolean banderaEsAvaluadorExterno);

    /**
     * Método que retorna la lista de avaluos de un avaluador con sus respectivos movimientos
     *
     * @author rodrigo.hernandez
     *
     * @param filtro - filtro de datos
     * @return
     */
    public List<Avaluo> obtenerAvaluosConMovimientosPorFiltro(
        FiltroDatosConsultaAvaluo filtro);

    /**
     * Borra los registros de AvaluoAsignacionProfesional relacionados con un Avaluo para alguna
     * actividad, que estén vigentes.
     *
     * @author pedro.garcia
     * @param idAvaluo
     * @param actividad
     * @return
     */
    public boolean borrarAsignacionesVigentesDeAvaluo(Long idAvaluo, String actividad);

    /**
     * Llama a al procedimiento almacenado para calcular el siguiente número en la numeración de
     * {@link ENumeraciones#NUMERACION_ACTA_COMITE_AVALUOS}
     *
     * @author christian.rodriguez
     * @return Cadena que contiene el siguiente número en la numeración
     */
    public String obtenerNumeroActaComiteAvaluos();

    /**
     * Método para determinar si existen detalles asociados a un avaúo o a los predios del avaúo
     * sogun las restricciones de la inserción de los detalles un avaúo solo puede tener todos sus
     * detalles de un tipo asociados al a sec. radicado directo o todos a los predios, nunca ambos
     * casos.
     *
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @return - true:Si existen detalles asociados a los predios del avaúo <br/>
     * - false:Si existen detalles asociados solo al avaúo <br/>
     * - true:Si no existen detalles asociados al avaúo <br/>
     */
    public Boolean calcularDetallesPreviosAvaluo(Long idAvaluo, int tipoDetalle);

    /**
     * Guarda o modifica un objeto que pertenece a alguna de las clases de detalles del avaúo
     *
     * @author pedro.garcia
     *
     * @param tipoDetalleAvaluo se usa para identificar internamente de qué tipo de entidad se trata
     * </br>
     * 1 = AvaluoPredioAnexo 2 = AvaluoPredioConstruccion 3 = AvaluoPredioCultivo 4 =
     * AvaluoPredioMaquinaria 5 = AvaluoPredioZona
     * @param detalleAvaluoNuevoModificado objeto que se guarda o modifica
     * @return
     */
    public boolean guardarModificarDetalleAvaluo(final int tipoDetalleAvaluo,
        Object detalleAvaluoNuevoModificado);

    /**
     * Consulta los {@link AvaluoPredioAnexo} dado un id de Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredioAnexo> consultarAvaluoPredioAnexoPorAvaluo(long avaluoId);

    /**
     * Consulta los {@link AvaluoPredioConstruccion} dado un id de Avaluo
     *
     * @author ?
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredioConstruccion> consultarAvaluoPredioConstruccionPorAvaluo(long avaluoId);

    /**
     * Consulta los {@link AvaluoPredioCultivo} dado un id de Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredioCultivo> consultarAvaluoPredioCultivoPorAvaluo(long avaluoId);

    /**
     * Consulta los AvaluoPredioMaquinaria dado un id de Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredioMaquinaria> consultarAvaluoPredioMaquinariaPorAvaluo(long avaluoId);

    /**
     * Consulta los {@link AvaluoPredioZona} dado un id de Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredioZona> consultarAvaluoPredioZonaPorAvaluo(long avaluoId);

    /**
     * Borra un objeto de los de tipo de detalle de avaúo de la bd (AvaluoPredioX)
     *
     * @author pedro.garcia
     *
     * @param tipoDetalleAvaluo se usa para identificar internamente de qué tipo de entidad se trata
     * </br> 1 = {@link AvaluoPredioAnexo}
     * 2 = {@link AvaluoPredioConstruccion}
     * 3 = {@link AvaluoPredioCultivo}
     * 4 = {@link AvaluoPredioMaquinaria}
     * 5 = {@link AvaluoPredioZona}
     * 6 = {@link AvaluoPredio}
     *
     * @param idDetalleAvaluo
     * @return true si lo eliminó correctamente
     */
    public boolean eliminarDetalleAvaluo(final int tipoDetalleAvaluo, long idDetalleAvaluo);

    /**
     * Método para eliminar los detalles de un tipo asociados a un avalúo
     *
     * @param idAvaluo
     * @param tipoDetalle
     * @return
     */
    public Boolean eliminarDetallesPreviosAvaluo(Long idAvaluo, int tipoDetalle);

    /**
     * Método que consulta las {@link OrdenPractica} que tengan el estado
     * {@link EOrdenPracticaEstado#GENERADA}
     *
     * @author christian.rodriguez
     * @return Lista con las {@link OrdenPractica} encontradas o null si no
     */
    public List<OrdenPractica> consultarOrdenesPracticaPorEnviar();

    /**
     *
     * Metodo para obtener los capitulos de proyeccion de cotizacion asociados a un avaluo
     *
     * @author rodrigo.hernandez
     *
     * @param idAvaluo - Id del avaluo
     * @return
     */
    public List<AvaluoCapitulo> obtenerCapitulosPorAvaluoId(Long idAvaluo);

    /**
     * Metodo para guardar/actualizar los documentos requisito asociados a una proyeccion de
     * cotizacion
     *
     * @author rodrigo.hernandez
     *
     * @param listaDocsRequisito
     * @return
     */
    public boolean guardarActualizarDocsRequisitoProyeccionCotizacion(
        List<AvaluoRequisitoDocumentacion> listaDocsRequisito);

    /**
     * Método para consultar los observaciones relacionadas con un avalúo
     *
     * @author felipe.cadena
     * @param avaluoId
     * @return
     */
    public List<AvaluoObservacion> consultarObservacionesPorAvaluo(long avaluoId);

    /**
     * Método para para guardar o actualizar {@link AvaluoObservacion}
     *
     * @author felipe.cadena
     * @param observacion
     * @param usuario
     * @return
     */
    public AvaluoObservacion guardarActualizarObservacionesAvaluo(
        AvaluoObservacion observacion, UsuarioDTO usuario);

    /**
     * Consulta los {@link AvaluoPredio} dado un id de Avaluo
     *
     * @author pedro.garcia
     * @param avaluoId
     * @return
     */
    public List<AvaluoPredio> consultarAvaluoPredioPorAvaluo(long avaluoId);

    /**
     *
     * Meotodo para guardar/actualizar los capitulos asociados a una proyeccion de cotización
     *
     * @author rodrigo.hernandez
     *
     * @param listaCapitulos
     * @return
     */
    public boolean guardarActualizarCapitulosProyeccionCotizacion(
        List<AvaluoCapitulo> listaCapitulos);

    /**
     *
     * Metodo para obtener los documentos requisito de proyeccion de cotizacion asociados a un
     * avaluo
     *
     * @author rodrigo.hernandez
     *
     * @param idAvaluo - Id del avaluo
     * @return
     */
    public List<AvaluoRequisitoDocumentacion> obtenerDocsRequisitoProyeccionCotizacionPorAvaluoId(
        Long idAvaluo);

    /**
     * Metodo para eliminar de BD un {@link AvaluoCapitulo} asociado a una proyección de cotización
     *
     * @author rodrigo.hernandez
     *
     * @param docRequisito
     * @return
     */
    public boolean eliminarCapituloProyeccionCotizacion(
        AvaluoCapitulo capitulo);

    /**
     * Metodo para eliminar de BD un {@link AvaluoRequisitoDocumentacion} asociado a una proyección
     * de cotización
     *
     * @author rodrigo.hernandez
     *
     * @param docRequisito
     * @return
     */
    public boolean eliminarDocRequisitoProyeccionCotizacion(
        AvaluoRequisitoDocumentacion docRequisito);

    /**
     * Metodo para consultar de BD las {@link AvaluoAreaRegistrada} relacionadas a un avaúo
     *
     * @author felipe.cadena
     * @param avaluoId
     * @return
     */
    public List<AvaluoAreaRegistrada> consultarAreasRegistradasPorAvaluo(long avaluoId);

    /**
     * Metodo para guardar un {@link AvaluoAreaRegistrada}
     *
     * @author felipe.cadena
     * @param area
     * @param usuario
     * @return
     */
    public AvaluoAreaRegistrada guardarActualizarAvaluoAreaRegistradaAvaluo(
        AvaluoAreaRegistrada area, UsuarioDTO usuario);

    /**
     * Metodo para eliminar de BD un {@link AvaluoAreaRegistrada}
     *
     * @author felipe.cadena
     * @param area
     * @return
     */
    public AvaluoAreaRegistrada eliminarAvaluoAreaRegistradaAvaluo(AvaluoAreaRegistrada area);

    /**
     * Método para obtener AvaluoPredioConstruccion con el id
     *
     * @author felipe.cadena
     * @param idConstruccion
     * @return
     */
    public AvaluoPredioConstruccion consultarAvaluoPredioConstruccionPorId(Long idConstruccion);

    /**
     * Método para consultara un area adoptada con atributos.
     *
     * @author felipe.cadena
     * @param idArea
     * @return
     */
    public AvaluoAreaRegistrada consultarAreasRegistradasPorIdConAtributo(long idArea);

}
