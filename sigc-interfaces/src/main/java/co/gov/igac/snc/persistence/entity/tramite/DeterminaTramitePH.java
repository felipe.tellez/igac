/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the TRAMITE_DEPURACION database table.
 */
@Entity
@Table(name = "DETERMINA_TRAMITE_PH", schema = "SNC_TRAMITE")
public class DeterminaTramitePH implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String tipoTramite;
    private String claseMutacion;
    private String subtipo;
    private String condicionPropiedad;
    private String condicion;
    private String estado;
    private Date fechaRegistro;
    private Date fechaLog;
    private String usuarioLog;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DETERMINA_TRAMITE_PH_ID_SEQ")
    @SequenceGenerator(name = "DETERMINA_TRAMITE_PH_ID_SEQ", sequenceName =
        "DETERMINA_TRAMITE_PH_ID_SEQ", allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_TRAMITE", nullable = false)
    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @Column(name = "CLASE_MUTACION", nullable = false)
    public String getClaseMutacion() {
        return claseMutacion;
    }

    public void setClaseMutacion(String claseMutacion) {
        this.claseMutacion = claseMutacion;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    @Column(name = "CONDICION_PROPIEDAD", nullable = false)
    public String getCondicionPropiedad() {
        return condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_REGISTRO", nullable = false)
    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false)
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
