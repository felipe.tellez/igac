package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.*;

/**
 * PredioServidumbre entity. @author MyEclipse Persistence Tools
 */
@Entity
@NamedQueries({@NamedQuery(name = "findServidumbreByPredio", query =
        "from PredioServidumbre ps where ps.predio = :predio")})
@Table(name = "PREDIO_SERVIDUMBRE", schema = "SNC_CONSERVACION")
public class PredioServidumbre implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3370644488505639288L;

    // Fields
    /* TODO: Validar si los datos para la anotación en el comentario son correctos
     * @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PREDIO_SERVIDUMBRE_ID_SEQ")
     */
    @GeneratedValue
    private Long id;
    private Predio predio;
    private String servidumbre;
    private String usuarioLog;
    private Date fechaLog;

    private Date fechaInscripcionCatastral;

    // Constructors
    /** default constructor */
    public PredioServidumbre() {
    }

    /** full constructor */
    public PredioServidumbre(Long id, Predio predio, String servidumbre,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.servidumbre = servidumbre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "SERVIDUMBRE", nullable = false, length = 30)
    public String getServidumbre() {
        return this.servidumbre;
    }

    public void setServidumbre(String servidumbre) {
        this.servidumbre = servidumbre;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL")
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

}
