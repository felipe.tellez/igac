package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ComisionTramite entity.
 */
/*
 * @modified by: pedro.garcia. what: - adición de anotaciones para la secuencia del id - cambio de
 * tipo de dato BigDecimal por Double para el atributo 'duracion' - adición de anotaciones para la
 * generación del id - adición de anotación @Temporal para los campos de fechas
 *
 * @modified by: fredy.wilches tabla regenerada en la BD @modified by: - javier.aponte se cambia la
 * anotacion @Temporal de fecha informe de TemporalType.DATE a TemporalType.TIMESTAMP
 */
@Entity
@NamedQueries({@NamedQuery(name = "deleteByTramiteAndComision", query =
        "delete from ComisionTramite t where t.comision.id = :comisionIdParam " +
        " and t.tramite.id = :tramiteIdParam")})
@Table(name = "COMISION_TRAMITE", schema = "SNC_TRAMITE")
public class ComisionTramite implements java.io.Serializable {

    // Fields
    private static final long serialVersionUID = -4105520103809634493L;

    private Long id;
    private Tramite tramite;
    private Comision comision;
    private String detalleInformeVisita;

    /**
     * Estos datos son los del ejecutor del tramite y pueden ser los datos del topógrafo, del
     * digitalizador o del ejecutor de un trámite en depuración.
     */
    private String ejecutor;
    private String ejecutorNombre;
    private String ejecutorRol;

    private Date fechaInforme;
    private Date fechaVisita;
    private String realizada;

    /**
     * la comisi{on pudo ser realizada, pero pudo ser que no haya podido hacer la inspección ocular
     */
    private String realizoInspeccionOcular;

    /**
     * indica si como resultado de la visita se deben generar nuevos trámites
     */
    private String tramitesNuevosAdicionales;

    private String resultado;
    private String novedad;
    private Long informeVisitaDocumentoId;
    private String observaciones;
    private String observacionesVisita;
    private Double duracion;
    private String usuarioLog;
    private Date fechaLog;

    private List<ComisionTramiteDato> comisionTramiteDatos;

    // Constructors
    /** default constructor */
    public ComisionTramite() {
    }

    /** minimal constructor */
    public ComisionTramite(Long id, Tramite tramite, Comision comision,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.comision = comision;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public ComisionTramite(Long id, Tramite tramite, Comision comision,
        String realizada, String novedad, String detalleInformeVisita,
        Long informeVisitaDocumentoId, String observaciones,
        Double duracion, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.comision = comision;
        this.realizada = realizada;
        this.novedad = novedad;
        this.detalleInformeVisita = detalleInformeVisita;
        this.informeVisitaDocumentoId = informeVisitaDocumentoId;
        this.observaciones = observaciones;
        this.duracion = duracion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMISION_TRAMITE_ID_SEQ")
    @SequenceGenerator(name = "COMISION_TRAMITE_ID_SEQ", sequenceName = "COMISION_TRAMITE_ID_SEQ",
        allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMISION_ID", nullable = false)
    public Comision getComision() {
        return this.comision;
    }

    public void setComision(Comision comision) {
        this.comision = comision;
    }

    @Column(name = "REALIZADA", length = 2)
    public String getRealizada() {
        return this.realizada;
    }

    public void setRealizada(String realizada) {
        this.realizada = realizada;
    }

    @Column(name = "REALIZO_INSPECCION_OCULAR", length = 2)
    public String getRealizoInspeccionOcular() {
        return this.realizoInspeccionOcular;
    }

    public void setRealizoInspeccionOcular(String oculo) {
        this.realizoInspeccionOcular = oculo;
    }

    @Column(name = "TRAMITES_NUEVOS_ADICIONALES", length = 2)
    public String getTramitesNuevosAdicionales() {
        return this.tramitesNuevosAdicionales;
    }

    public void setTramitesNuevosAdicionales(String nuevosTrams) {
        this.tramitesNuevosAdicionales = nuevosTrams;
    }

    @Column(name = "NOVEDAD", length = 30)
    public String getNovedad() {
        return this.novedad;
    }

    public void setNovedad(String novedad) {
        this.novedad = novedad;
    }

    @Column(name = "DETALLE_INFORME_VISITA", length = 2000)
    public String getDetalleInformeVisita() {
        return this.detalleInformeVisita;
    }

    public void setDetalleInformeVisita(String detalleInformeVisita) {
        this.detalleInformeVisita = detalleInformeVisita;
    }

    public Double getDuracion() {
        return this.duracion;
    }

    public void setDuracion(Double duracion) {
        this.duracion = duracion;
    }

    @Column(name = "INFORME_VISITA_DOCUMENTO_ID", precision = 10, scale = 0)
    public Long getInformeVisitaDocumentoId() {
        return this.informeVisitaDocumentoId;
    }

    public void setInformeVisitaDocumentoId(Long informeVisitaDocumentoId) {
        this.informeVisitaDocumentoId = informeVisitaDocumentoId;
    }

    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "OBSERVACIONES_VISITA")
    public String getObservacionesVisita() {
        return this.observacionesVisita;
    }

    public void setObservacionesVisita(String observacionesVisita) {
        this.observacionesVisita = observacionesVisita;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    //bi-directional many-to-one association to ComisionTramiteDato
    @OneToMany(mappedBy = "comisionTramite")
    public List<ComisionTramiteDato> getComisionTramiteDatos() {
        return this.comisionTramiteDatos;
    }

    public void setComisionTramiteDatos(List<ComisionTramiteDato> comisionTramiteDatos) {
        this.comisionTramiteDatos = comisionTramiteDatos;
    }

    @Column(name = "FECHA_INFORME")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaInforme() {
        return this.fechaInforme;
    }

    public void setFechaInforme(Date fechaInforme) {
        this.fechaInforme = fechaInforme;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_VISITA")
    public Date getFechaVisita() {
        return this.fechaVisita;
    }

    public void setFechaVisita(Date fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public String getResultado() {
        return this.resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Column(name = "EJECUTOR", length = 100)
    public String getEjecutor() {
        return this.ejecutor;
    }

    public void setEjecutor(String ejecutorP) {
        this.ejecutor = ejecutorP;
    }

    @Column(name = "EJECUTOR_NOMBRE", length = 100)
    public String getEjecutorNombre() {
        return this.ejecutorNombre;
    }

    public void setEjecutorNombre(String ejecutorP) {
        this.ejecutorNombre = ejecutorP;
    }

    @Column(name = "EJECUTOR_ROL", length = 20)
    public String getEjecutorRol() {
        return this.ejecutorRol;
    }

    public void setEjecutorRol(String ejecutorR) {
        this.ejecutorRol = ejecutorR;
    }

}
