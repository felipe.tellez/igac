package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.snc.persistence.util.ESiNo;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * GrupoProducto entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "GRUPO_PRODUCTO", schema = "SNC_TRAMITE")
public class GrupoProducto implements java.io.Serializable {

    // Fields
    @GeneratedValue
    private Long id;
    private String codigo;
    private String descripcion;
    private String usuarioLog;
    private Date fechaLog;
    private List<Producto> productos;
    private String activo;

    // Constructors
    /** default constructor */
    public GrupoProducto() {
    }

    /** minimal constructor */
    public GrupoProducto(Long id, String descripcion, String tipo,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.descripcion = descripcion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public GrupoProducto(Long id, String descripcion, String codigo,
        String tipo, String usuarioLog, Date fechaLog,
        List<Producto> productos) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.productos = productos;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DESCRIPCION", nullable = false, length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "CODIGO", length = 10)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "grupoProducto")
    public List<Producto> getProductos() {
        return this.productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @Column(name = "ACTIVO", length = 100)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Transient
    public boolean isGrupoProductoActivo() {
        return ESiNo.SI.getCodigo().equals(activo);
    }

}
