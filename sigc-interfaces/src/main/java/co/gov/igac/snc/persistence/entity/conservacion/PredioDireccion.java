package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.util.ESiNo;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.*;

/**
 * PredioDireccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@NamedQueries(
    {
        @NamedQuery(name = "findPredioDireccionByPredio", query =
            "from PredioDireccion pd where pd.predio = :predio")
    }
)
@Table(name = "PREDIO_DIRECCION", schema = "SNC_CONSERVACION")
public class PredioDireccion implements java.io.Serializable, Comparable<PredioDireccion> {

    /**
     *
     */
    private static final long serialVersionUID = -2509924367289599285L;

    // Fields
    /* TODO: Validar si los datos para la anotación en el comentario son correctos
     * @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PREDIO_DIRECCION_ID_SEQ")
     */
    private Long id;
    private Predio predio;
    private String direccion;
    private String principal;
    private String referencia;
    private String usuarioLog;
    private Date fechaLog;
    private String direccionEstandar;
    private String codigoPostal;

    private Date fechaInscripcionCatastral;

    // Constructors
    /** default constructor */
    public PredioDireccion() {
    }

    /** minimal constructor */
    public PredioDireccion(Long id, Predio predio, String direccion,
        String principal, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.direccion = direccion;
        this.principal = principal;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PredioDireccion(Long id, Predio predio, String direccion,
        String principal, String referencia, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.predio = predio;
        this.direccion = direccion;
        this.principal = principal;
        this.referencia = referencia;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "predio_direccion_ID_SEQ")
    @SequenceGenerator(name = "predio_direccion_ID_SEQ", sequenceName = "PREDIO_DIRECCION_ID_SEQ",
        allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "DIRECCION", nullable = false, length = 250)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "PRINCIPAL", nullable = false, length = 2)
    public String getPrincipal() {
        return this.principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    @Column(name = "REFERENCIA", length = 100)
    public String getReferencia() {
        return this.referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL")
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "DIRECCION_ESTANDAR")
    public String getDireccionEstandar() {
        return direccionEstandar;
    }

    public void setDireccionEstandar(String direccionEstandar) {
        this.direccionEstandar = direccionEstandar;
    }

    @Column(name = "CODIGO_POSTAL")
    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Implementación del método compareTo para que esta sea Comparable. La comparación por defecto
     * es sobre la casdena que guarda la dirección.
     *
     * @author pedro.garcia
     */
    @Implement
    @Override
    public int compareTo(PredioDireccion pd) {

        int answer = this.direccion.compareTo(pd.direccion);
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que retorna un Comparator para comparar objetos de este tipo por el criterio de que la
     * dirección sea la principal del predio
     *
     * OJO: por defecto, cuando se usa un Comparator para ordenar Collections, el ordenamiento se
     * hace ascendente
     *
     * @author pedro.garcia
     *
     * @param ascending define si el ordenamiento es ascendente o descendente.
     * @return 0 por defecto. En el caso de que 'ascending' es true: gt 0 si pd1 está marcada como
     * principal (sin importar qué tenga pd2). lt 0 si pd1 no está marcada como principal y pd2 sí
     * En el casi de que 'ascending' es false: retorna lo contrario.
     */
    public static Comparator<PredioDireccion> getComparatorDirPrincipal(boolean ascending) {

        Comparator<PredioDireccion> answer = null;

        if (ascending) {
            answer = new Comparator<PredioDireccion>() {
                @Override
                public int compare(PredioDireccion pd1, PredioDireccion pd2) {
                    int answer = 0;
                    if (pd1.principal.compareToIgnoreCase(ESiNo.SI.getCodigo()) == 0) {
                        answer = 1;
                    } else if (pd1.principal.compareToIgnoreCase(ESiNo.SI.getCodigo()) != 0 &&
                        pd2.principal.compareToIgnoreCase(ESiNo.SI.getCodigo()) == 0) {
                        answer = -1;
                    }
                    return answer;
                }
            };
        } else {
            answer = new Comparator<PredioDireccion>() {
                @Override
                public int compare(PredioDireccion pd1, PredioDireccion pd2) {
                    int answer = 0;
                    if (pd1.principal.compareToIgnoreCase(ESiNo.SI.getCodigo()) == 0) {
                        answer = -1;
                    } else if (pd1.principal.compareToIgnoreCase(ESiNo.SI.getCodigo()) != 0 &&
                        pd2.principal.compareToIgnoreCase(ESiNo.SI.getCodigo()) == 0) {
                        answer = 1;
                    }
                    return answer;
                }
            };
        }

        return answer;
    }

}
