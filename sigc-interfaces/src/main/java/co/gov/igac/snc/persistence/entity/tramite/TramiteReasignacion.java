/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the TRAMITE_REASIGNACION database table.
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "TRAMITE_REASIGNACION", schema = "SNC_TRAMITE")
public class TramiteReasignacion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Tramite tramite;
    private String funcionarioOrigen;
    private String funcionarioDestino;
    private String motivo;
    private Date fechaLog;
    private String usuarioLog;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRMTE_REASIGNACION_ID_SEQ_GEN")
    @SequenceGenerator(name = "TRMTE_REASIGNACION_ID_SEQ_GEN", sequenceName =
        "TRMTE_REASIGNACION_ID_SEQ", allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // bi-directional many-to-one association to Tramite
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "FUNCIONARIO_ORIGEN")
    public String getFuncionarioOrigen() {
        return funcionarioOrigen;
    }

    public void setFuncionarioOrigen(String funcionarioOrigen) {
        this.funcionarioOrigen = funcionarioOrigen;
    }

    @Column(name = "FUNCIONARIO_DESTINO")
    public String getFuncionarioDestino() {
        return funcionarioDestino;
    }

    public void setFuncionarioDestino(String funcionarioDestino) {
        this.funcionarioDestino = funcionarioDestino;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false)
    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
