package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Objeto que almacena los valores registrados para una entidad que es la encargada de realizar
 * bloqueos a personas y predios.
 *
 * @author david.cifuentes
 */
@Entity
@Table(name = "ENTIDAD", schema = "SNC_CONSERVACION")
public class Entidad implements java.io.Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String nombre;
    private String estado;
    private Date fechaRegistro;
    private Date fechaLog;
    private String usuarioLog;
    private Departamento departamento;
    private Municipio municipio;
    private String direccion;
    private String correo;
    private EstructuraOrganizacional organizacion;

    public Entidad() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENTIDAD_ID_SEQ")
    @SequenceGenerator(name = "ENTIDAD_ID_SEQ", sequenceName = "ENTIDAD_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NOMBRE")
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "ESTADO")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_REGISTRO")
    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "DIRECCION")
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "CORREO_ELECTRONICO")
    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ESTRUCTURA_ORGANIZAC_CODIGO", nullable = false)
    public EstructuraOrganizacional getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(EstructuraOrganizacional organizacion) {
        this.organizacion = organizacion;
    }

    /**
     *
     * Método que realiza un clone del objeto {@link Entidad}
     *
     * @author david.cifuentes
     */
    @Override
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }
}
