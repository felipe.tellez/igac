/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los valores del digito predial (posición 22) de la condición juridica
 *
 * @author christian.rodriguez
 */
public enum ECondicionJuridicaNumPredial {

    NO_PROPIEDAD_HORIZONTAL("012345"), PROPIEDAD_HORIZONTAL("6789");

    private String codigo;

    private ECondicionJuridicaNumPredial(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
