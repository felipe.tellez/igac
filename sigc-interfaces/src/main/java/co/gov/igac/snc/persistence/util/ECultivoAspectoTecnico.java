package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los valores para el dominio AVALUO_PREDIO_CULT_ASPE_TEC
 *
 * @author felipe.cadena
 */
public enum ECultivoAspectoTecnico implements ECampoDetalleAvaluo {

    TECNIFICADO("1", "Tecnificado"),
    NO_TECNIFICADO("2", "No Tecnificado");

    private String codigo;
    private String valor;

    private ECultivoAspectoTecnico(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

    @Override
    public String getValor() {
        return valor;
    }

}
