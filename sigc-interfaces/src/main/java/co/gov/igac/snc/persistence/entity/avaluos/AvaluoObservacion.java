/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the AVALUO_OBSERVACION database table.
 *
 */
@Entity
@Table(name = "AVALUO_OBSERVACION", schema = "SNC_AVALUOS")
public class AvaluoObservacion implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String observacion;
    private Date fechaObservacion;
    private Date fechaEnvio;
    private Date fechaLog;
    private String usuarioLog;
    private Avaluo avaluo;

    public AvaluoObservacion() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_OBSERVACION_ID_GENERATOR", sequenceName =
        "AVALUO_OBSERVACION_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_OBSERVACION_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "OBSERVACION")
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_OBSERVACION")
    public Date getFechaObservacion() {
        return fechaObservacion;
    }

    public void setFechaObservacion(Date fechaObservacion) {
        this.fechaObservacion = fechaObservacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ENVIO")
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to Avaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    @Transient
    public String getObservacionCorta() {
        if (this.observacion.length() > 80) {
            return this.observacion.substring(0, 80) + " ...";
        } else {
            return this.observacion;
        }
    }
}
