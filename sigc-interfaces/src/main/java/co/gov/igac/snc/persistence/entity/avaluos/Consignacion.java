package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the CONSIGNACION database table.
 *
 */
@Entity
@Table(name = "CONSIGNACION")
public class Consignacion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String entidadFinanciera;
    private Date fecha;
    private Date fechaLog;
    private String numeroComprobanteDeposito;
    private String numeroCuenta;
    private String referencia1;
    private String referencia2;
    private String tipoCuenta;
    private String usuarioLog;
    private BigDecimal valor;
    private ContratoInteradministrativo contratoInteradministrativo;

    public Consignacion() {
    }

    @Id
    @SequenceGenerator(name = "CONSIGNACION_ID_GENERATOR", sequenceName = "CONSIGNACION_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONSIGNACION_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ENTIDAD_FINANCIERA", nullable = false, length = 100)
    public String getEntidadFinanciera() {
        return this.entidadFinanciera;
    }

    public void setEntidadFinanciera(String entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NUMERO_COMPROBANTE_DEPOSITO", nullable = false, length = 50)
    public String getNumeroComprobanteDeposito() {
        return this.numeroComprobanteDeposito;
    }

    public void setNumeroComprobanteDeposito(String numeroComprobanteDeposito) {
        this.numeroComprobanteDeposito = numeroComprobanteDeposito;
    }

    @Column(name = "NUMERO_CUENTA", nullable = false, length = 50)
    public String getNumeroCuenta() {
        return this.numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    @Column(length = 100)
    public String getReferencia1() {
        return this.referencia1;
    }

    public void setReferencia1(String referencia1) {
        this.referencia1 = referencia1;
    }

    @Column(length = 100)
    public String getReferencia2() {
        return this.referencia2;
    }

    public void setReferencia2(String referencia2) {
        this.referencia2 = referencia2;
    }

    @Column(name = "TIPO_CUENTA", nullable = false, length = 50)
    public String getTipoCuenta() {
        return this.tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(nullable = false, precision = 10, scale = 2)
    public BigDecimal getValor() {
        return this.valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    // bi-directional many-to-one association to ContratoInteradministrativo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTRATO_INTERADMINISTRATIV_ID")
    public ContratoInteradministrativo getContratoInteradministrativo() {
        return this.contratoInteradministrativo;
    }

    public void setContratoInteradministrativo(
        ContratoInteradministrativo contratoInteradministrativo) {
        this.contratoInteradministrativo = contratoInteradministrativo;
    }

}
