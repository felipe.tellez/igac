package co.gov.igac.snc.persistence.entity.conservacion;

import co.gov.igac.snc.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the FICHA_MATRIZ database table.
 *
 * @modified	juan.agudelo 21-03-12 Adición de la secuencia FICHA_MATRIZ_ID_SEQ juan.agudelo 17-04-12
 * Adición de el atributo transient totalUnidadesPrivadasSotano
 *
 * @modified david.cifuentes :: Adición de la variable totalUnidadesSotanos :: Cambio de la variable
 * id a tipo Long :: 19/07/12
 *
 */
@Entity
@Table(name = "FICHA_MATRIZ", schema = "SNC_CONSERVACION")
public class FichaMatriz implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2023396489012437977L;

    private Long id;
    private Double areaTotalConstruidaComun;
    private Double areaTotalConstruidaPrivada;
    private Double areaTotalTerrenoComun;
    private Double areaTotalTerrenoPrivada;
    private Date fechaLog;
    private Long totalUnidadesPrivadas;
    private String usuarioLog;
    private Double valorTotalAvaluoCatastral;
    private List<FichaMatrizModelo> fichaMatrizModelos;
    private List<FichaMatrizPredio> fichaMatrizPredios;
    private List<FichaMatrizTorre> fichaMatrizTorres;
    private List<FichaMatrizPredioTerreno> FichaMatrizPredioTerrenos;
    private Predio predio;

    private Long totalUnidadesSotanos;

    private String estado;

    public FichaMatriz() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FICHA_MATRIZ_ID_SEQ")
    @SequenceGenerator(name = "FICHA_MATRIZ_ID_SEQ", sequenceName = "FICHA_MATRIZ_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA_TOTAL_CONSTRUIDA_COMUN")
    public Double getAreaTotalConstruidaComun() {
        return this.areaTotalConstruidaComun;
    }

    public void setAreaTotalConstruidaComun(Double areaTotalConstruidaComun) {
        this.areaTotalConstruidaComun = areaTotalConstruidaComun;
    }

    @Column(name = "AREA_TOTAL_CONSTRUIDA_PRIVADA")
    public Double getAreaTotalConstruidaPrivada() {
        return this.areaTotalConstruidaPrivada;
    }

    public void setAreaTotalConstruidaPrivada(
        Double areaTotalConstruidaPrivada) {
        this.areaTotalConstruidaPrivada = areaTotalConstruidaPrivada;
    }

    @Column(name = "AREA_TOTAL_TERRENO_COMUN")
    public Double getAreaTotalTerrenoComun() {
        return this.areaTotalTerrenoComun;
    }

    public void setAreaTotalTerrenoComun(Double areaTotalTerrenoComun) {
        this.areaTotalTerrenoComun = areaTotalTerrenoComun;
    }

    @Column(name = "AREA_TOTAL_TERRENO_PRIVADA")
    public Double getAreaTotalTerrenoPrivada() {
        return this.areaTotalTerrenoPrivada;
    }

    public void setAreaTotalTerrenoPrivada(Double areaTotalTerrenoPrivada) {
        this.areaTotalTerrenoPrivada = areaTotalTerrenoPrivada;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "TOTAL_UNIDADES_PRIVADAS")
    public Long getTotalUnidadesPrivadas() {
        return this.totalUnidadesPrivadas;
    }

    public void setTotalUnidadesPrivadas(Long totalUnidadesPrivadas) {
        this.totalUnidadesPrivadas = totalUnidadesPrivadas;
    }

    @Column(name = "TOTAL_UNIDADES_SOTANOS", nullable = false, precision = 6)
    public Long getTotalUnidadesSotanos() {
        return this.totalUnidadesSotanos;
    }

    public void setTotalUnidadesSotanos(Long totalUnidadesSotanos) {
        this.totalUnidadesSotanos = totalUnidadesSotanos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_TOTAL_AVALUO_CATASTRAL")
    public Double getValorTotalAvaluoCatastral() {
        return this.valorTotalAvaluoCatastral;
    }

    public void setValorTotalAvaluoCatastral(
        Double valorTotalAvaluoCatastral) {
        this.valorTotalAvaluoCatastral = valorTotalAvaluoCatastral;
    }

    // bi-directional many-to-one association to FichaMatrizModelo
    @OneToMany(mappedBy = "fichaMatriz", fetch = FetchType.LAZY)
    public List<FichaMatrizModelo> getFichaMatrizModelos() {
        return this.fichaMatrizModelos;
    }

    public void setFichaMatrizModelos(List<FichaMatrizModelo> fichaMatrizModelos) {
        this.fichaMatrizModelos = fichaMatrizModelos;
    }

    // bi-directional many-to-one association to FichaMatrizPredio
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "fichaMatriz")
    public List<FichaMatrizPredio> getFichaMatrizPredios() {
        return this.fichaMatrizPredios;
    }

    public void setFichaMatrizPredios(List<FichaMatrizPredio> fichaMatrizPredios) {
        this.fichaMatrizPredios = fichaMatrizPredios;
    }

    // bi-directional many-to-one association to FichaMatrizTorre
    @OneToMany(mappedBy = "fichaMatriz", fetch = FetchType.LAZY)
    public List<FichaMatrizTorre> getFichaMatrizTorres() {
        return this.fichaMatrizTorres;
    }

    public void setFichaMatrizTorres(List<FichaMatrizTorre> fichaMatrizTorres) {
        this.fichaMatrizTorres = fichaMatrizTorres;
    }

    // bi-directional many-to-one association to Predio
    @ManyToOne(fetch = FetchType.LAZY)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    // bi-directional many-to-one association to FichaMatrizPredioTerrenos
    @OneToMany(mappedBy = "fichaMatriz")
    public List<FichaMatrizPredioTerreno> getFichaMatrizPredioTerrenos() {
        return this.FichaMatrizPredioTerrenos;
    }

    public void setFichaMatrizPredioTerrenos(
        List<FichaMatrizPredioTerreno> FichaMatrizPredioTerrenos) {
        this.FichaMatrizPredioTerrenos = FichaMatrizPredioTerrenos;
    }

    /**
     * Método para obtener el area de terreno privada distribuida en hectareas y metros
     *
     * @author felipe.cadena
     *
     * @return - arreglo de dos posiciones con el area en Hectareas y Metros respectivamente
     *
     */
    @Transient
    public int[] getAreaTerrenoPrivadaHectareasMetros() {
        return Utilidades.convertirAreaAformatoHectareas(this.areaTotalTerrenoPrivada);
    }

    /**
     * Método para obtener el area de terreno comun distribuida en hectareas y metros
     *
     * @author felipe.cadena
     *
     * @return - arreglo de dos posiciones con el area en Hectareas y Metros respectivamente
     *
     */
    @Transient
    public int[] getAreaTotalTerrenoComunHectareasMetros() {
        return Utilidades.convertirAreaAformatoHectareas(this.areaTotalTerrenoComun);
    }

    /**
     * Método para obtener el area construida privada distribuida en hectareas y metros
     *
     * @author felipe.cadena
     *
     * @return - arreglo de dos posiciones con el area en Hectareas y Metros respectivamente
     *
     */
    @Transient
    public int[] getAreaTotalConstruidaPrivadaHectareasMetros() {
        return Utilidades.convertirAreaAformatoHectareas(this.areaTotalConstruidaPrivada);
    }

    /**
     * Método para obtener el area construida comun distribuida en hectareas y metros
     *
     * @author felipe.cadena
     *
     * @return - arreglo de dos posiciones con el area en Hectareas y Metros respectivamente
     *
     */
    @Transient
    public int[] getAreaTotalConstruidaComunHectareasMetros() {
        return Utilidades.convertirAreaAformatoHectareas(this.areaTotalConstruidaComun);
    }

    @Column(name = "ESTADO", nullable = false, length = 30)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static FichaMatriz parseFichaMatriz(HFichaMatriz hFichaMatriz) {
        FichaMatriz fichaMatriz = new FichaMatriz();

        fichaMatriz.setId(hFichaMatriz.getId().getId());
        fichaMatriz.setAreaTotalConstruidaComun(hFichaMatriz.getAreaTotalConstruidaComun());
        fichaMatriz.setAreaTotalConstruidaPrivada(hFichaMatriz.getAreaTotalConstruidaPrivada());
        fichaMatriz.setAreaTotalTerrenoComun(hFichaMatriz.getAreaTotalTerrenoComun());
        fichaMatriz.setAreaTotalTerrenoPrivada(hFichaMatriz.getAreaTotalTerrenoPrivada());
        fichaMatriz.setFechaLog(hFichaMatriz.getFechaLog());
        fichaMatriz.setTotalUnidadesPrivadas(hFichaMatriz.getTotalUnidadesPrivadas());
        fichaMatriz.setUsuarioLog(hFichaMatriz.getUsuarioLog());
        fichaMatriz.setValorTotalAvaluoCatastral(hFichaMatriz.getValorTotalAvaluoCatastral());

        List<FichaMatrizModelo> fichaMatrizModelos = new ArrayList<FichaMatrizModelo>();
        for (HFichaMatrizModelo aHFichaMatrizModelo : hFichaMatriz.getHFichaMatrizModelos()) {
            fichaMatrizModelos.add(FichaMatrizModelo.parseFichaMatrizModelo(aHFichaMatrizModelo));
        }
        fichaMatriz.setFichaMatrizModelos(fichaMatrizModelos);

        List<FichaMatrizPredio> fichaMatrizPredios = new ArrayList<FichaMatrizPredio>();
        for (HFichaMatrizPredio aHFichaMatrizPredio : hFichaMatriz.getHFichaMatrizPredios()) {
            fichaMatrizPredios.add(FichaMatrizPredio.parseFichaMatrizPredio(aHFichaMatrizPredio));
        }
        fichaMatriz.setFichaMatrizPredios(fichaMatrizPredios);

        List<FichaMatrizTorre> fichaMatrizTorres = new ArrayList<FichaMatrizTorre>();
        for (HFichaMatrizTorre aHFichaMatrizTorre : hFichaMatriz.getHFichaMatrizTorres()) {
            fichaMatrizTorres.add(FichaMatrizTorre.parseFichaMatrizTorres(aHFichaMatrizTorre));
        }
        fichaMatriz.setFichaMatrizTorres(fichaMatrizTorres);

        fichaMatriz.setPredio(Predio.parsePredio(hFichaMatriz.getHPredio()));
        fichaMatriz.setTotalUnidadesSotanos(hFichaMatriz.getTotalUnidadesSotanos());
        fichaMatriz.setEstado(hFichaMatriz.getEstado());

        return fichaMatriz;
    }

}
