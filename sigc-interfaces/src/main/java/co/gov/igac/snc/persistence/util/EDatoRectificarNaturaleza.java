/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los valores de naturaleza para un dato rectificar. Correspondientes al dominio
 * DATO_RECTIFICAR_NATURALEZA
 *
 * @author juan.agudelo
 */
/*
 * @modified pedro.garcia 26-11-2013 se agrega el campo valor y se implementa el métogo getCodigo
 */
public enum EDatoRectificarNaturaleza {

    //D: nombre ("código", "valor")
    RECTIFICACION("RECTIFICA", "Rectifica"),
    COMPLEMENTACION("COMPLEMENTA", "Complementa");

    private String codigo;
    private String valor;

    private EDatoRectificarNaturaleza(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String geCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
