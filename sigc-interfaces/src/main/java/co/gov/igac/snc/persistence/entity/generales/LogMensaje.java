package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import javax.persistence.*;

import co.gov.igac.snc.util.Constantes;

import java.util.Date;

/**
 * The persistent class for the LOG_MENSAJE database table.
 *
 * @modified pedro.garcia - adición de secuencia para el id
 *
 */
@Entity
@Table(name = "LOG_MENSAJE")
public class LogMensaje implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private Date fechaLog;
    private String mensaje;
    private String nivel;
    private String programa;
    private String usuarioLog;

    public LogMensaje() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LogMensaje_ID_SEQ")
    @SequenceGenerator(name = "LogMensaje_ID_SEQ", sequenceName = "LOG_MENSAJE_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getMensaje() {
        return this.mensaje;
    }

    public void setMensaje(String mensaje) {
        if (mensaje.length() > Constantes.TAMANIO_MENSAJE_LOG_MENSAJE) {
            mensaje = mensaje.substring(0, Constantes.TAMANIO_MENSAJE_LOG_MENSAJE);
        }
        this.mensaje = mensaje;
    }

    public String getNivel() {
        return this.nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getPrograma() {
        return this.programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
