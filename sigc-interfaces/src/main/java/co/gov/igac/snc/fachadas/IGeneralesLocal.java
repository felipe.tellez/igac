package co.gov.igac.snc.fachadas;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.CirculoRegistralMunicipio;
import co.gov.igac.persistence.entity.generales.Comuna;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.persistence.entity.generales.Profesion;
import co.gov.igac.persistence.entity.generales.Sector;
import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoCapitulo;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.generales.Componente;
import co.gov.igac.snc.persistence.entity.generales.ConexionUsuario;
import co.gov.igac.snc.persistence.entity.generales.ConfiguracionReporte;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Grupo;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.persistence.entity.generales.LogAccion;
import co.gov.igac.snc.persistence.entity.generales.LogMensaje;
import co.gov.igac.snc.persistence.entity.generales.MunicipioComplemento;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.generales.PlantillaSeccionReporte;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.generales.VPermisoGrupo;
import co.gov.igac.snc.persistence.entity.tramite.GrupoProducto;
import co.gov.igac.snc.persistence.entity.tramite.Producto;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralError;
import co.gov.igac.snc.persistence.entity.tramite.ProductoFormato;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.ECodigoHomologado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESistema;
import co.gov.igac.snc.util.DocumentoResultadoPPRedios;
import co.gov.igac.snc.util.exceptions.SncBusinessServiceExceptions;
import co.gov.igac.snc.vo.ImagenPredioVO;
import co.gov.igac.snc.vo.ParametroEditorVO;

/**
 *
 * @author fredy.wilches
 *
 */
@Local
public interface IGeneralesLocal {

    /**
     * Método que retorna la lista de dominios por el campo nombre
     *
     * @param d Referirse a la enumeración EDominio para encontrar los valores de los dominios
     * @return
     * @author null
     * @documented fabio.navarrete
     * @modified pedro.garcia para que reciba un objeto de tipo Enum y no solo un EDominio.
     */
    @SuppressWarnings("rawtypes")
    public List<Dominio> getCacheDominioPorNombre(Enum d);

    /**
     * Retorna la lista completa de países
     *
     * @return
     * @author null
     * @documented fabio.navarrete
     */
    public List<Pais> getCachePaises();

    /**
     * Retorna un parámetro por su nombre.
     *
     * @param nombre nombre del parámetro
     * @return
     * @author null
     * @documented fabio.navarrete
     */
    public Parametro getCacheParametroPorNombre(String nombre);

    /**
     * Retorna los departamentos asociados a un país
     *
     * @param codigoPais código del país
     * @return
     * @author null
     * @documented fabio.navarrete
     */
    public List<Departamento> getCacheDepartamentosPorPais(String codigoPais);

    /**
     * Retorna los municipios asociados a un departamento
     *
     * @param codigoDepartamento código del departamento
     * @return
     * @author null
     * @documented fabio.navarrete
     */
    public List<Municipio> getCacheMunicipiosPorDepartamento(String codigoDepartamento);

    /**
     * Retorna las estructuras organizacionales asociadas a una estructura padre filtradas por
     * determinado tipo.
     *
     * @param tipo tipo de la estructura organizacional. Consultar enumeración
     * EEstructuraOrganizacional
     * @param codigoPadre código de la estructura organizacional padre
     * @return
     * @author null
     * @documented fabio.navarrete
     */
    public List<EstructuraOrganizacional> getCacheEstructuraOrganizacionalPorTipoYPadre(
        EEstructuraOrganizacional tipo, String codigoPadre);

    /**
     * Retorna la lista de todas las Territoriales
     *
     * @author pedro.garcia
     * @return
     */
    public List<EstructuraOrganizacional> getCacheTerritoriales();

    /**
     * Retorna la lista de todas las Territoriales incluyendo la dirección general
     *
     * @author fabio.navarrete
     * @return
     */
    public List<EstructuraOrganizacional> getCacheTerritorialesConDireccionGeneral();

    /**
     * Retorna la lista de los departamentos de una Territorial
     *
     * @author pedro.garcia
     * @param departamentoCode código del departamento
     * @return
     */
    public List<Departamento> getCacheDepartamentosPorTerritorial(String territorialCode);

    /**
     * Retorna la lista de los municipios de un departamento
     *
     * @author pedro.garcia
     * @param departamentoCode código del departamento
     * @return
     */
    public List<Municipio> getCacheMunicipiosByDepartamento(String departamentoCode);

    /**
     * Método encargado de obtener un departamento a partir de su código.
     *
     * @param codigo Código del departamento
     * @return Departamento con el código que se recibe como parámetro.
     */
    public Departamento getCacheDepartamentoByCodigo(String codigo);

    /**
     * Método encargado de obtener un circulo registral a partir de su código.
     *
     * @author christian.rodriguez
     * @param codigo Código del circulo registral
     * @return CirculoRegistral con el código que se recibe como parámetro.
     */
    public CirculoRegistral getCirculoRegistralByCodigo(String codigo);

    /**
     * Método encargado de obtener un municipio a partir de su código.
     *
     * @param codigo Código del municipio
     * @return Municipio con el código que se recibe como parámetro.
     */
    public Municipio getCacheMunicipioByCodigo(String codigo);

    /**
     * Retorna la lista de todos los departamentos
     *
     * @return
     */
    public List<Departamento> getCacheDepartamentos();

    /**
     * Retorna el listado de tipos de documentos que son requeridos para un tipo de tramite, clase
     * de mutacion y subtipo
     *
     * @param tipoTramite
     * @param claseMutacion
     * @param subtipo
     * @return
     */
    public List<TipoDocumento> getCacheTiposDocumentoPorTipoTramite(String tipoTramite,
        String claseMutacion, String subtipo);

    /**
     * @author fredy.wilches Ejecuta el SP para generacion de numeraciones y devuelve en dos
     * formatos
     * @param numeracion
     * @param estructuraOrganizacional
     * @param departamento
     * @param municipio
     * @param tipoDocumento
     * @return
     */
    public Object[] generarNumeracion(ENumeraciones numeracion, String estructuraOrganizacional,
        String departamento, String municipio, Integer tipoDocumento);

    /**
     * Retorna el tipo de documento por CLASE del dominio TIPO_DOCUMENTO_CLASE
     *
     * @author juan.agudelo
     */
    public TipoDocumento getCacheTipoDocumentoPorClase(String clase);

    /**
     * @author fredy.wilches Ejecuta el SP provisional que permite radicar en correspondencia
     * cualquier documento
     * @param parametros
     * @return
     */
    public Object[] radicarEnCorrespondencia(List<Object> parametros);

    /**
     * @author fabio.navarrete LLama al procedimiento almacenado que permite obtener los datos de
     * radicación asociados al número de radicación ingresado.
     * @param numeroRadicacionCorrespondencia
     * @param usuario
     * @return
     *
     * @deprecated pedro.garcia :: no se pudo determinar en dónde se usa o se debía usar
     */
    @Deprecated
    public Object[] consultarRadicadoCorrespondencia(
        String numeroRadicacionCorrespondencia, UsuarioDTO usuario);

    /**
     * Retorna las opciones del arbol del menú
     *
     * @param roles
     * @return
     */
    public List<VPermisoGrupo> getCacheMenu(String roles[]);

    /**
     * Obtiene la lista de círculos registrales dado un id de municipio
     *
     * @author pedro.garcia
     * @param idDepartamento
     * @param idMunicipio
     * @return
     */
    public List<CirculoRegistral> getCacheCirculosRegistralesPorMunicipio(String idMunicipio);

    /**
     * Obtiene la lista de círculos registrales dado un id de departamento
     *
     * @author fredy.wilches
     * @param idDepartamento
     * @return
     */
    public List<CirculoRegistral> getCacheCirculosRegistralesPorDepartamento(String idDepartamento);

    /**
     *
     * @param login
     * @return
     */
    public UsuarioDTO getCacheUsuario(String login);

    /**
     * Obtiene un código homologado
     *
     * @author fredy.wilches
     * @param codigo
     * @param origen
     * @param destino
     * @param valor
     * @param fecha
     * @return
     */
    public String getCacheCodigoHomologado(ECodigoHomologado codigo, ESistema origen,
        ESistema destino, String valor, Date fecha);

    /**
     * hace la consulta para saber si un código de municipio corresponde a una entidad territorial
     * lo cual lo definiría como sede -para los trámites-
     *
     * @author pedro.garcia
     * @param codigoMunicipio
     * @param idTerritorial
     * @return
     */
    public boolean averiguarSiMunicipioEsSede(String codigoMunicipio, String idTerritorial);

    /**
     * Retorna la lista de todos los {@link TipoDocumento} filtrados por clase o generado
     *
     * @author david.cifuentes
     * @param String claseDoc
     * @param String generado
     * @return List<TipoDocumentoDAOBean>
     */
    public List<TipoDocumento> buscarTiposDeDocumentos(String claseDoc, String generado);

    /**
     * @author fredy.wilches
     * @param adjuntos urls de los archivos que se van a adjuntar
     * @param titulos nombres de los archivos enviados como adjuntos Metodo usado para enviar
     * correos electrónicos
     */
    /*
     * @modified pedro.garcia 03-10-2012 Devuelve un booleano para poder saber en web si se envió el
     * correo
     */
    public boolean enviarCorreo(String destinatario, String objeto, String mensaje,
        String[] adjuntos, String[] titulos);

    /**
     * Método que inserta un nuevo {@link Documento}
     *
     * @author david.cifuentes
     * @param documentoSoporte
     * @return
     */
    public Documento guardarDocumento(Documento documentoSoporte);

    /**
     * Método que permite actualizar un documento y subirlo en alfresco
     *
     * @author juan.agudelo
     * @param usuario
     * @param documentoTramiteDTO
     * @param documento
     */
    public boolean actualizarDocumentoYAlfresco(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento);

    /**
     * Método que busca un {@link Documento} por su id
     *
     * @author david.cifuentes
     * @param documentoId
     * @return
     */
    public Documento buscarDocumentoPorId(Long documentoId);

    /**
     * @author christian.rodriguez Método que busca un documento por su id y carga el
     * {@link TipoDocumento} asociado
     * @param documentoId identificador del documento a cargar
     * @return {@link Documento} encontrado o null si no se obtuvieron resultados
     */
    public Documento buscarDocumentoPorIdConTipoDocumento(Long documentoId);

    /**
     * Método que busca un {@link TipoDocumento} por su id
     *
     * @author david.cifuentes
     * @param tipoDocumentoId
     * @return
     */
    public TipoDocumento buscarTipoDocumentoPorId(Long tipoDocumentoId);

    /**
     * Método que busca un {@link Documento} por su id del repositorio de documentos en Alfresco
     *
     * @author david.cifuentes
     * @param idRepositorioDocumentos
     * @return
     */
    /*
     * @modified by juanfelipe.garcia :: 25-11-2013 :: comentariado por cambio al gestor documental
     * (FTP), este método no se esta usando
     */
    //public DocumentoNombreMimeDTO buscarDocumentoAlfresco( String idRepositorioDocumentos );
    /**
     * Método que actualiza un {@link Documento} y sube el archivo a Alfresco
     *
     * @author david.cifuentes
     *
     * @param usuario
     * @param documentoTramiteDTO
     * @param documento
     * @return
     */
    public Documento actualizarDocumentoYSubirArchivoAlfresco(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento);

    /**
     * Método que sube un archivo a Alfresco
     *
     * @author david.cifuentes
     *
     * @param usuario
     * @param documentoTramiteDTO
     * @param documento
     */
    public void subirArchivoAlfresco(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento);

    /**
     * Método que retorna una lista de {@link Documento} con las resoluciones asociadas a un predio
     *
     * @author david.cifuentes
     *
     * @param numeroPredial
     * @return
     */
    public List<Documento> buscarResolucionesByNumeroPredialPredio(String numeroPredial);

    /**
     * Retorna las subdirecciones y oficinas (EstructuraOrganizacional) de tipo Subdireccion y
     * Oficina Apoyo asociadas a una EstructuraOrganizacional específica
     *
     * @param codigoPadre
     * @return
     * @author fabio.navarrete
     */
    public List<EstructuraOrganizacional> getCacheSubdireccionesYOficinas(
        String codigoPadre);

    /**
     * Retorna los grupos de trabajo (GIT) asociados a una estructura organizacional específica
     *
     * @param estructuraOrganizacionalCodigo
     * @return
     * @author fabio.navarrete
     */
    public List<EstructuraOrganizacional> getCacheGruposTrabajo(
        String estructuraOrganizacionalCodigo);

    /**
     * Obtiene documento resultado de un tramite especifico y la lista ppredio para revisar la
     * proyección en cancelación y contiene información sobre las cancelaciones en cuanto a areas y
     * avalúos
     *
     * @author juan.agudelo
     * @param tramiteId
     * @return
     */
    public DocumentoResultadoPPRedios buscarDocumentoResultadoPorTramiteId(Long tramiteId);

    /**
     * Método que recupera todos los tipos de documentos que no son generados por el sistema
     *
     * @author juan.agudelo
     * @return
     */
    public List<TipoDocumento> getAllTipoDocumento();

    /**
     * Método que retorna la direccion normalizada a partir de una direccion
     *
     * @param direccion
     * @return
     * @author franz.gamba
     */
    public String obtenerDireccionNormalizada(String direccion);

    /**
     * obtiene una fecha que es igual a la dada más el número de días hábiles
     *
     * @author pedro.garcia
     * @param fechaInicial
     * @param diasSumados
     * @return
     */
    public Date obtenerFechaConAdicionDiasHabiles(Date fechaInicial, int diasSumados);

    /**
     * Obtiene la entidad plantilla a partir de su código.
     *
     * @param codigo es el código de la plantilla a recuperar.
     * @return el objeto Plantilla correspondiente o null en caso de no encontrarlo.
     * @author jamir.avila.
     */
    public Plantilla recuperarPlantillaPorCodigo(String codigo);

    /**
     * Guarda una actualizacionDocumento en la base de datos
     *
     * @param actualizacionDoc
     * @return
     * @author franz.gamba
     */
    public ActualizacionDocumento guardarActualizacionDocumento(
        ActualizacionDocumento actualizacionDoc);

    /**
     * Método que guarda un memorando de comisión en la BD y en Alfresco
     *
     * @author javier.aponte
     * @param documento
     * @param usuario
     */
    public Documento guardarMemorandoConservacion(Documento documento, String numeroRadicado,
        UsuarioDTO usuario);

    /**
     * Método mueve un archivo identificado con el id dado a la carpeta de archivos temporales
     * Retorna la URL de la carpeta temporal donde se descargó el archivo
     *
     * @author david.cifuentes
     * @param idAlfresco
     * @return
     */
    /*
     * @modified pedro.garcia 14-05-2013 documentación, cambio de nombre (antes:
     * consultarURLDeArchivoEnAlfresco)
     */
    public String descargarArchivoDeAlfrescoATemp(String idAlfresco);

    /**
     * Retorna las zonas asociadas a un municipio
     *
     * @param municipioCodigo código del municipio
     * @return
     * @author lorena.salamanca
     */
//TODO :: lorena.salamanca :: 20-04-2012 :: el estándar definido dice que no se deben usar métodos llamados en otro idioma en estas interfaces  :: pedro.garcia
    public List<Zona> getZonasFromMunicipio(String municipioCodigo);

    /**
     * Retorna los sectores asociados a una zona
     *
     * @param zonaId código de la zona
     * @return
     * @author lorena.salamanca
     */
    public List<Sector> getSectoresFromZona(String zonaId);

    /**
     * Retorna las comunas asociadas a un sector
     *
     * @param sectorId código del sector
     * @return
     * @author lorena.salamanca
     */
    public List<Comuna> getComunasFromSector(String sectorId);

    /**
     * Retorna los barrios asociados a una comuna
     *
     * @param comunaId código de la comuna
     * @return
     * @author lorena.salamanca
     */
    public List<Barrio> getBarriosFromComuna(String comunaId);

    /**
     * Retorna las manzanas o veredas asociadas a un barrio
     *
     * @param barrioId código del barrio
     * @return
     * @author lorena.salamanca
     */
    public List<ManzanaVereda> getManzanaVeredaFromBarrio(String barrioId);

    /**
     * Borra un departamento
     *
     * @param departamento
     * @author lorena.salamanca
     */
    public void borrarDepartamento(Departamento departamento);

    /**
     * Borra un municipio
     *
     * @param municipio
     * @author lorena.salamanca
     */
    public void borrarMunicipio(Municipio municipio);

    /**
     * Borra una zona
     *
     * @param zona
     * @author lorena.salamanca
     */
    public void borrarZona(Zona zona);

    /**
     * Borra un sector
     *
     * @param sector
     * @author lorena.salamanca
     */
    public void borrarSector(Sector sector);

    /**
     * Borra una comuna
     *
     * @param comuna
     * @author lorena.salamanca
     */
    public void borrarComuna(Comuna comuna);

    /**
     * Borra un barrio
     *
     * @param barrio
     * @author lorena.salamanca
     */
    public void borrarBarrio(Barrio barrio);

    /**
     * Borra una manzana o vereda
     *
     * @param manzanaVereda
     * @author lorena.salamanca
     */
    public void borrarManzanaVereda(ManzanaVereda manzanaVereda);

    /**
     * Agrega un departamento
     *
     * @param departamento
     * @author lorena.salamanca
     */
    public void agregarDepartamento(Departamento departamento);

    /**
     * Agrega un municipio
     *
     * @param municipio
     * @author lorena.salamanca
     */
    public void agregarMunicipio(Municipio municipio);

    /**
     * Agrega una zona
     *
     * @param zona
     * @author lorena.salamanca
     */
    public void agregarZona(Zona zona);

    /**
     * Agrega un sector
     *
     * @param sector
     * @author lorena.salamanca
     */
    public void agregarSector(Sector sector);

    /**
     * Agrega una comuna
     *
     * @param comuna
     * @author lorena.salamanca
     */
    public void agregarComuna(Comuna comuna);

    /**
     * Agrega un barrio
     *
     * @param barrio
     * @author lorena.salamanca
     */
    public void agregarBarrio(Barrio barrio);

    /**
     * Adiciona un evento de Log
     *
     * @param mensaje
     * @author juan.mendez
     */
    public void agregarLogMensaje(LogMensaje mensaje);

    /**
     * Agrega una manzana o vereda
     *
     * @param manzanaVereda
     * @author lorena.salamanca
     */
    public void agregarManzanaVereda(ManzanaVereda manzanaVereda);

    /**
     * Modifica un departamento
     *
     * @param departamento
     * @author lorena.salamanca
     */
    public void modificarDepartamento(Departamento departamento);

    /**
     * Modifica un municipio
     *
     * @param municipio
     * @author lorena.salamanca
     */
    public void modificarMunicipio(Municipio municipio);

    /**
     * Modifica una zona
     *
     * @param zona
     * @author lorena.salamanca
     */
    public void modificarZona(Zona zona);

    /**
     * Modifica un sector
     *
     * @param sector
     * @author lorena.salamanca
     */
    public void modificarSector(Sector sector);

    /**
     * Modifica un comuna
     *
     * @param comuna
     * @author lorena.salamanca
     */
    public void modificarComuna(Comuna comuna);

    /**
     * Modifica un barrio
     *
     * @param barrio
     * @author lorena.salamanca
     */
    public void modificarBarrio(Barrio barrio);

    /**
     * Modifica una manzana o vereda
     *
     * @param manzanaVereda
     * @author lorena.salamanca
     */
    public void modificarManzanaVereda(ManzanaVereda manzanaVereda);

    /**
     * Método encargado de obtener una zona a partir de su código.
     *
     * @param codigo Código de zona
     * @return Zona con el código que se recibe como parámetro.
     */
    public Zona getCacheZonaByCodigo(String codigo);

    /**
     * Método encargado de obtener un Sector a partir de su código.
     *
     * @param codigo Código de Sector
     * @return Sector con el código que se recibe como parámetro.
     */
    public Sector getCacheSectorByCodigo(String codigo);

    /**
     * Método encargado de obtener una Comuna a partir de su código.
     *
     * @param codigo Código de Comuna
     * @return Comuna con el código que se recibe como parámetro.
     */
    public Comuna getCacheComunaByCodigo(String codigo);

    /**
     * Método encargado de obtener un Barrio a partir de su código.
     *
     * @param codigo Código de Barrio
     * @return Barrio con el código que se recibe como parámetro.
     */
    public Barrio getCacheBarrioByCodigo(String codigo);

    /**
     * Método encargado de obtener una ManzanaVereda a partir de su código.
     *
     * @param codigo Código de ManzanaVereda
     * @return ManzanaVereda con el código que se recibe como parámetro.
     */
    public ManzanaVereda getCacheManzanaVeredaByCodigo(String codigo);

    /**
     * Método que retorna todos los funcionarios correspondientes a una territorial el parametro de
     * "uo" es opcional
     *
     * @param territorial
     * @param uo
     * @return
     */
    public List<UsuarioDTO> getFuncionariosTerritorial(String territorial, String uo);

    /**
     * Determina si el predio con número predial 'numPredialPredio' es colindante con TODOS los
     * predios cuyos números prediales están en la lista 'numPredialesComparados'
     *
     * @author pedro.garcia
     *
     * @param numPredialPredio número predial del predio que se quiere saber si es colindante
     * @param numPredialesComparados números prediales de los predios con que se va a comparar
     */
    public boolean esColindante(String numPredialPredio, List<String> numPredialesComparados);

    /**
     * Obtiene una lista de usuarios que pertenecen a la territorial y rol indicados.
     *
     * @author franz.gamba
     * @param territorial código de la territorial.
     * @param rol rol del usuario.
     * @return una lista de objetos UsuarioDTO que coinciden con los criterios de búsqueda o una
     * lista vacía en caso de que no haya coincidencias.
     */
    public List<UsuarioDTO> obtenerFuncionarioTerritorialYRol(String territorial, ERol rol);

    /**
     * Método encargado de obtener un departamento a partir de su código.
     *
     * @author ...
     * @param codigo Código del departamento
     * @return Departamento con el código que se recibe como parámetro.
     */
    public Departamento getDepartamentoByCodigo(String codigo);

    /**
     * Retorna los municipios por código de estructura organizacional
     *
     * @param codigoEO
     * @return
     * @author juan.agudelo
     */
    public List<Municipio> getCacheMunicipiosPorCodigoEstructuraOrganizacional(
        String codigoEO);

    /**
     * Retorna los municipios para una territorial por un código de estructura organizacional
     *
     * @param codigoEO
     * @return
     * @author juan.agudelo
     */
    public List<Municipio> getCacheMunicipiosTerritorialPorDepartametoYCodigoEstructuraOrganizacional(
        String departamentoCodigo, String codigoEO);

    /**
     * Método encargado de obtener una lista de departamentos a partir del código de estructura
     * organizacional del usuarioDTO
     *
     * @author juan.agudelo
     * @param codigo Código de la estructura organizacional
     * @return Departamento
     */
    public List<Departamento> getDepartamentoByCodigoEstructuraOrganizacional(
        String codigo);

    /**
     * Método que retorna una lista de {@link Dominio} buscandolos por su nombre. Éste método es una
     * copia del método findByNombre, y retornando la lista ordenada por éste mismo parametro.
     *
     * @author david.cifuentes
     * @param d
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<Dominio> buscarDominioPorNombreOrderByNombre(Enum d);

    /**
     * Método que retorna la lista de tramitesEstado a partir del id del tramite
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    public List<TramiteEstado> buscarTramiteEstadosPorTramiteId(Long tramiteId);

    /**
     * Retorna una lista de {@link Departamentos} asociados a un {@link Pais} exceptuando los
     * departamentos con catastro centralizado (Antioquia, Bogotá)
     *
     * @param codigoPais {@link String} Código del país
     * @return
     * @author david.cifuentes
     */
    public List<Departamento> buscarDepartamentosConCatastroCentralizadoPorCodigoPais(
        String codigoPais);

    /**
     * Retorna los municipios asociados a un departamento exceptuando los municipios con catastro
     * centralizado (Bogotá, Cali y los municipios de Antioquia)
     *
     * @param codigoDepartamento
     * @return
     * @author david.cifuentes
     */
    public List<Municipio> buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
        String codigoDepartamento);

    /**
     * Retorna la descripcion de la estructura organizacional de un municipio
     *
     * @author franz.gamba
     * @param municipioCod
     * @return
     */
    public String getDescripcionEstructuraOrganizacionalPorMunicipioCod(String municipioCod);

    /**
     * Retorna la oficina jerarquicamente superior de la estructura organizacional qeu corresponda
     * al codigo del municipio
     *
     * @param municipioCod
     * @return
     */
    public String getEstructuraOrgSuperiorPorMunicipioCod(String municipioCod);

    /**
     * Método que permite actualizar un documento y subirlo en alfresco revisando si tiene o no
     * número predial.
     *
     * Éste método es una copia del método actualizarDocumentoYAlfresco con las validaciones
     * adicionales para verificar si se tiene o no un número predial.
     *
     * @author david.cifuentes
     * @param usuario
     * @param documentoTramiteDTO
     * @param documento
     * @param tramite
     */
    public boolean actualizarDocumentoYAlfrescoConValidacionNumeroPredial(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento, Tramite tramite);

    /**
     * Metodo que retorna el usuario subdirector de Catastro
     *
     * @return
     */
    public UsuarioDTO obtenerSubdirectorCatastro();

    /**
     * Método que retorna si el municipio enviado pertenece a un catastro descentralizado basandose
     * en el dominio CATASTRO_DESCENTRALIZADO
     *
     * @author christian.rodriguez
     * @param codMunicipio código del municipio
     * @return verdadero si el municipio pertenece a un catastro descentralizado, falso si no
     */
    public boolean municipioPerteneceACatastroDescentralizado(String codMunicipio);

    /**
     * Método que búsca una estructura organizacional por su código retornandola con su municipio,
     * departamento y pais.
     *
     * @author david.cifuentes
     */
    public EstructuraOrganizacional buscarEstructuraOrganizacionalPorCodigo(
        String codigoEstructuraOrganizacional);

    /**
     * Método que actualiza una lista de documentos.
     *
     * @author david.cifuentes
     */
    public boolean actualizarListaDeDocumentos(List<Documento> documentosActualizar);

    /**
     * Metodo que permite consultar los usos de la construccion por id
     *
     * @author fredy.wilches
     */
    public UsoConstruccion getCacheUsoConstruccionById(Long id);

    /**
     * Método que busca el código de un dominio por su dominio y su valor
     *
     * @author david.cifuentes
     */
    public Dominio buscarValorDelDominioPorDominioYCodigo(EDominio nombre, String codigo);

    /**
     * Método para almacenar documentos en alfresco relacionados a una solicitud de avaluo comercial
     *
     * @author felipe.cadena
     */
    public Documento guardarDocumentosSolicitudAvaluoAlfresco(UsuarioDTO usuario,
        DocumentoSolicitudDTO documentoSolicitudDTO, Documento documento);

    /**
     * Método para obtener una estructura organizacional con su codigo asociado
     *
     * @author felipe.cadena
     */
    public EstructuraOrganizacional obtenerTerritorialPorCodigo(String codigo);

    /**
     * Retorna una lista con las inconsistencias geográficas asociadas al predio si el predio no
     * tiene inconsistencias retorna la lista vacia.
     *
     * @param numeroPredial
     * @param tramite
     * @param usuario
     * @return
     * @author felipe.cadena
     */
    public List<TramiteInconsistencia> determinarInconsistenciasGeograficas(String numeroPredial,
        Tramite tramite, UsuarioDTO usuario) throws Exception;

    /**
     * Retorna la ruta del comprimido con datos geograficos del tramite en formato shape.
     *
     * @param numeroPredial
     * @param identificadoresPredios
     * @param tipoMutacion
     * @param usuario
     * @return
     * @author juanfelipe.garcia
     */
    public String obtenerRutaDatosGeograficosTramiteAsShape(String numeroTramite,
        String identificadoresPredios, String tipoMutacion, UsuarioDTO usuario);

    /**
     * Método para actualizar un documento en la base de datos
     *
     * @author felipe.cadena
     *
     * @param documento - documento a actualziar
     * @return
     */
    public Documento actualizarDocumento(Documento documento);

    /**
     * Método que busca un documento por su id de tramite
     *
     * @author lorena.salamanca
     * @param tramiteId, idTipoDocumento
     * @return
     */
    public Documento buscarDocumentoPorTramiteIdyTipoDocumento(Long tramiteId, Long idTipoDocumento);

    /**
     * Metodo que obtiene un archivo de Alfresco por su id y luego publica en el WorkspacePreview de
     * alfresco
     *
     * @author juanfelipe.garcia
     * @param idRepositorioDocumentos
     * @return url del archivo para verlo por http
     */
    public String descargarArchivoAlfrescoYSubirAPreview(String idRepositorioDocumentos);

    /**
     * Retorna la lista de registros de la tabla Profesion
     *
     * @author pedro.garcia
     * @return
     */
    public List<Profesion> getCacheProfesiones();

    /**
     * Retorna la capital de determinado departamento
     *
     * @author lorena.salamanca
     * @return
     */
    public Municipio getCapitalDepartamentoByCodigoDepartamento(String departamentoId);

    /**
     * Retorna el numero de barrios por municipio
     *
     * @author javier.barajas
     * @param String codigo municipio
     * @return numero de manzanas
     */
    public List<Barrio> getNumeroManzanasPorMunicipio(String codigoMunicipio);

    /**
     * Método que elimina un documento en base de datos y alfresco
     *
     * @author franz.gamba
     * @param documento
     * @return
     */
    public boolean borrarDocumentoEnBDyAlfresco(Documento documento);

    /**
     * Método que elimina un documento de alfresco
     *
     * @author christian.rodriguez
     * @param idDocumentoAlfresco Cadena con el id del documento en alfresco
     * @return true si no ocurrio error en el borrado
     */
    public boolean borrarDocumentoEnAlfresco(String idDocumentoAlfresco);

    /**
     * Método genérico que envia correos usando el método
     * {@link IGeneralesLocal#enviarCorreo(String, String, String, String[], String[])}
     *
     * @author christian.rodriguez
     * @param plantillaCorreo plantilla que contiene el cuerpo del correo, corresponde a un registro
     * de la enumeración {@link EPlantilla}
     * @param destinatariosTC lista de cadenas de texto con los correos a los que se le va a enviar
     * el correo
     * @param destinatariosCC lista de cadenas de texto con los correos a los que se le va a enviar
     * el correo como copia
     * @param destinatariosBCC lista de cadenas de texto con los correos a los que se le va a enviar
     * el correo como copia oculta
     * @param datosCorreo arreglo de {@link Object} con los parametros que se incluiran en la
     * plantilla del correo
     * @param adjuntos urls de los archivos adjuntos
     * @param titulos cadenas de texto con los nombres a mostrar de los adjuntos
     * @return true si el correo se envio correctamente, false si no
     */
    public boolean enviarCorreoElectronicoConCuerpo(EPlantilla plantillaCorreo,
        List<String> destinatariosTC, List<String> destinatariosCC,
        List<String> destinatariosBCC, Object[] datosCorreo,
        String[] adjuntos, String[] titulos);

    /**
     * Método que busca un formato en alfresco por su nombre, lo carga en la carpeta temporal del
     * usuario del sistema y retorna la ruta en la que quedó descargado.
     *
     * @author franz.gamba
     * @param nombreFormato
     * @return
     */
    /*
     * @modified pedro.garcia 23-05-2012 Cambio de nombre de método (ya no sube al ftp).
     * Documentación.
     */
 /*
     * @modified by juanfelipe.garcia :: 25-11-2013 :: comentariado por cambio al gestor documental
     * (FTP), este método no se esta usando
     */
    //public String buscarFormatoEnAlfresco(String nombreFormato);
    /**
     * Método que retorna un objeto de tipo configuración de reporte por id de éste objeto
     *
     * @param configuracionReporteId
     * @author javier.aponte
     * @return
     */
    public ConfiguracionReporte obtenerConfiguracionReportePorId(Long configuracionReporteId);

    /**
     * Método que devuelve un reporte dinamico
     *
     * @param parameters
     * @param configuracionReporteId
     * @param loggedInUser
     * @return File
     * @author javier.aponte
     */
    public File obtenerReporteDinamico(Map<String, String> parameters, Long configuracionReporteId,
        UsuarioDTO loggedInUser);

    /**
     * Método que publica un archivo en web para poder ser visualizados en la aplicación a partir de
     * la ruta donde está almacenado un archivo de tipo File
     *
     * @param path ruta donde está almacenado el archivo File
     * @return rutaWeb ruta donde se publica el archivo
     * @author javier.aponte
     */
    public String publicarArchivoEnWeb(String path);

    /**
     * Metodo usado para enviar correos electrónicos desde la capa web a varios destinatarios
     *
     * @author pedro.garcia
     *
     * @param destinatarios lista de correos electrónicos
     * @param tema tema u objeto del correo
     * @param mensaje cuerpo del correo
     * @param adjuntos url de los archivos que se van a adjuntar
     * @param titulos nombres de los archivos adjuntos
     */
    public boolean enviarCorreo(List<String> destinatarios, String tema, String mensaje,
        String[] adjuntos, String[] titulos);

    /**
     * Metodo usado para enviar correos electrónicos desde la capa web a varios destinatarios,
     * incluyendo los tres tipos posibles de destinatarios
     *
     * @author pedro.garcia
     *
     * @param destinatariosTO lista de correos electrónicos
     * @param destinatariosCC lista de correos electrónicos
     * @param destinatariosBCC lista de correos electrónicos
     * @param asunto asunto u objeto del correo
     * @param mensaje cuerpo del correo
     * @param adjuntos url de los archivos que se van a adjuntar
     * @param titulos nombres de los archivos adjuntos
     */
    public boolean enviarCorreo(List<String> destinatariosTO, List<String> destinatariosCC,
        List<String> destinatariosBCC, String asunto, String mensaje, String[] adjuntos,
        String[] titulos);

    /**
     * Retorna el listado de tipos de documentos adicionales para un tipo de tramite de avaluo
     *
     * @author felipe.cadena
     * @param tipoTramite
     * @return
     */
    public List<TipoDocumento> obtenerTipoDocumentoAvaluoAdicional(String tipoTramite);

    /**
     * Calcula el número de dias habiles que existen entre dos fechas, los parametros de fechas
     * Desde y Hasta no se incluyen en el conteo de los dias habiles.
     *
     * @author felipe.cadena
     *
     * @param fechaDesde
     * @param fechaHasta
     * @return
     */
    public Integer calcularDiasHabilesEntreDosFechas(Date fechaDesde, Date fechaHasta);

    /**
     * determina si una fecha dada corresponde a un dia habil o no
     *
     * @author felipe.cadena
     *
     * @param fecha
     * @return
     */
    public boolean esDiaHabil(Date fecha);

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @throws SncBusinessServiceExceptions en caso de que ocurra un error al radicar en
     * correspondencia
     * @param lista con los parametros necesarios para radicar en correspondencia
     *
     * List<Object> parametros = new ArrayList<Object>();
     *
     * parametros.add("800"); // Territorial=compania parametros.add("SC_SNC"); // Usuario
     * parametros.add("IE"); // tipo correspondencia parametros.add("1"); // tipo contenido
     * solicitud.getTipo() parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento
     * principal parametros.add(BigDecimal.valueOf(0)); //Número de anexos parametros.add(""); //
     * Asunto parametros.add(""); // Observaciones parametros.add(""); // Dependencia origen
     * parametros.add(""); // Funcionario origen parametros.add(""); // Cargo origen
     * parametros.add(""); // Publicacion origen parametros.add("01"); // Sistema envio
     * parametros.add(""); // Radicacion anterior parametros.add(""); // Radicacion responde
     * parametros.add("6"); // presentacion, 6 internet parametros.add("800"); // Destino compania =
     * territorial parametros.add("0"); // Destino copia parametros.add("5000"); // Destino lugar
     * parametros.add(""); // Destino persona parametros.add(""); // Destino cargo
     * parametros.add(""); // Direccion destino parametros.add(""); // Tipo identificacion destino
     * parametros.add(""); // Identificacion destino parametros.add(""); // Ciudad destino
     * parametros.add(""); // Solicitante tipo documento parametros.add(""); // Solicitante
     * identificacion parametros.add(""); // Solicitante nombre parametros.add(""); // Solicitante
     * teléfono parametros.add(""); // Solicitante email
     *
     * @return String con el número de radicado en correspondencia
     * @author javier.aponte
     */
    public String obtenerNumeroRadicado(List<Object> parametros);

    /**
     * Método que consulta la estructura organizaiocnl que tiene jurisdicción sobre un municipio
     *
     * @author christian.rodriguez
     * @param municipioCodigo código del municipio que se desea consultar
     * @return {@link EstructuraOrganizacional} encontrada o null si ocurrio algún error
     */
    public EstructuraOrganizacional obtenerEstructuraOrganizacionalPorMunicipioCodigo(
        String municipioCodigo);

    /**
     *
     * Metodo que carga los capitulos (por default) que se pueden seleccionar para la proyeccion de
     * una cotizacion de un avaluo comercial
     *
     * @author rodrigo.hernandez
     *
     * @return Lista de capitulos usados para la proyeccion de cotizacion de un avaluo
     *
     * Tener en cuenta que los datos: - id - avaluoId - fechaLog - usuarioLog - otraDescripcion de
     * cada elemento de la lista quedan en null porque se definen en el MB donde se use la lista
     */
    public List<AvaluoCapitulo> obtenerCapitulosAvaluo();

    /**
     * Metodo para obtener los tipos de documentos soporte para el servidor de productos
     *
     * @return List<TipoDocumento>
     * @cu CU-TV-PR-0001 INGRESAR SOLICITUD PRODUCTOS CATASTRALES
     * @version 1.0
     * @author javier.barajas
     *
     */
    public List<TipoDocumento> buscarTipoDocumentoSoporteProductos();

    /**
     * Método que retorna una lista de {@link String} con los códigos de los municipios asociados a
     * un departamento.
     *
     * @author david.cifuentes
     *
     * @param {@link String} codigoDepartamento
     *
     * @return Lista de {@link String} con los códigos de los municipios.
     */
    public List<String> obtenerCodigosMunicipiosPorCodDepartamentoSinCatastroDescentralizado(
        String codigoDepartamento);

    /**
     * Método que retorna una lista de los municipios asociados a un departamento.
     *
     * @author dumar.penuela
     *
     * @param {@link String} codigoDepartamento
     *
     * @return Lista de {@link String} con los municipios.
     */
    public List<Municipio> obteneMunicipiosDepartamentoSinCatastroDescentralizado(
        String codigoDepartamento);

    /**
     * Obtiene un Documento dado su id
     *
     * @author pedro.garcia
     * @param idDocumento
     * @return
     */
    public Documento obtenerDocumentoPorId(Long idDocumento);

    /**
     * Guarda un documento en la base de datos y en alfresco.
     *
     * @author felipe.cadena
     * @param idDocumento
     * @return
     */
    public Documento guardarDocumentoAlfresco(UsuarioDTO usuario,
        DocumentoTramiteDTO documentoTramiteDTO, Documento documento);

    /**
     * Método para validar la existencia de geometría para los predios del trámite indicado por el
     * parámetro (Forma asincrónica)
     *
     * @param tramite tramite que tiene asociados los predios que se quieren validar
     * @param usuario
     * @author fredy.wilches
     */
    /*
     * @modified by javier.aponte ajustes varios para validar la geometría de los predios 15/08/2013
     */
    public void validarGeometriaPredios(Tramite tramite, UsuarioDTO usuario);

    /**
     * Método que busca un documento por su numero de documento.
     *
     * @param numResolucion
     * @return
     */
    public Documento buscarDocumentoPorNumeroDocumento(String numResolucion);

    /**
     * Método para revalidar la existencia de geometría para los predios relacionados con el trámite
     * indicado por el parámetro, en el caso que no haya inconsistencias guarda un registro en la
     * tabla TRAMITE_INCONSISTENCIA, con el valor de la constante
     * NO_HAY_INCONSISTENCIAS_GEOGRAFICAS, de lo contrario guarda un registro en la misma tabla con
     * la inconsistencia geográfica que tiene el predio. Devuelve un String que si es null significa
     * que no hay inconsistencias geográficas, en caso contrario es el error que sucedió al
     * revalidar la geometría de los predios. Se debe usar este método cuando se requiera validar la
     * geometría de predios de forma sincronica, esa es la diferencia con el método que se llama
     * validarGeometriaPredios, que hace la misma validación pero de forma asincrónica
     *
     * @param tramite entidad tramite para validar los predios que éste tenga asociados
     * @param usuario usuario en sesión que invoca este método
     * @return String null si no hay error, en caso contrario el código de error al revalidar la
     * geometría de los predios
     * @author javier.aponte
     */
//REVIEW :: javier.aponte :: debería devolver algo diferente de null (ver implementación) :: pedro.garcia
    public String revalidarGeometriaPredios(Tramite tramite, UsuarioDTO usuario);

    /**
     * Envia un job de validacion de geometrias para los predios realcionados a un tramite
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @return
     */
    public String validarGeometriaPrediosAsincronico(Tramite tramite, UsuarioDTO usuario);
    
    /**
     * @author hector.arias
     * @param environment
     * @param ubicacionArchivo
     * @param validacionZonas
     * @param validacionPredios
     * @param usuario 
     */
    public String validarInconsistenciasActualizacion(String departamentoSeleccionado, String municipioSeleccionado, String ubicacionArchivo, String validacionZonas, String validacionPredios, UsuarioDTO usuario);

    /**
     * Método para validar la geometria de los predios asociados a un tramite, se pueden validar un
     * conjunto de predios en particular enviandolo en el parametro predios si el parametro es null
     * se validaran los predios asociados directamente al tramite.
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param predios
     * @param usuario
     * @return retorna la lista de inconsistencias relacionadas a los predios.
     */
    public List<TramiteInconsistencia> validarGeometriaPrediosTramite(Tramite tramite,
        List<Predio> predios, UsuarioDTO usuario);

    /**
     * Método para validar la geometria de las mejoras asociadas a un tramite
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @param mejoras - lista de predios que representan las mejoras
     * @param usuario
     * @return retorna la lista de inconsistencias relacionadas a los predios.
     */
    public List<TramiteInconsistencia> validarGeometriaMejorasTramite(Tramite tramite,
        List<Predio> mejoras, UsuarioDTO usuario);

    /**
     * Método para validar de nuevo la geometría de los predios
     *
     * @author javier.aponte
     */
    /*
     * @modified felipe.cadena :: 01-11-2013 :: se agregan validaciones para predios de mejoras
     * @modified pedro.garcia 03-12-2013 se revisa si es trámite es de cancelación de predio o de
     * rectificación por cancelación por doble inscripción para usar otro método para validar de
     * forma sincrónica
     */
    public String validarInconsistencias(Tramite tramite, UsuarioDTO usuario);

    /**
     * Método para validar cuando los predios del tramite son de tipo mejora.
     *
     * @author felipe.cadena
     * @return
     */
    public String validarGeometriaMejoras(Tramite tramite, UsuarioDTO usuario);

    /**
     * Método para validar cuando los predios del tramite son de tipo mejora y de tipo terreno.
     *
     * @author felipe.cadena
     * @return
     */
    public String validarGeometriaMejorasTerreno(Tramite tramite, UsuarioDTO usuario);

    /**
     * Mètodo para eliminar los números prediales repetidos que puedan existir en una lista
     *
     * @author felipe.cadena
     * @param numeros
     * @return
     */
    public List<String> eliminarNumerosPredialesDuplicados(List<String> numeros);

    /**
     * Metodo para crear una inconsistencia cuando una mejora no tiene predio de terreno
     * correspondiente
     *
     * @author felipe.cadena
     */
    public TramiteInconsistencia crearInconsistenciaTerreno(Tramite tramite, UsuarioDTO usuario);

    /**
     * Método que genera un archivo zip a partir de la ruta de los documentos que se envian como
     * parametro
     *
     * @param rutaArchivos
     * @author javier.aponte
     */
    public String crearZipAPartirDeRutaDeDocumentos(List<String> rutaArchivos);

    /**
     * Método para calcular el digito de verificación de un número de identificación para una
     * persona juridica.
     *
     * @param nit
     * @return
     */
    public int calcularDigitoVerificacion(String nit);

    /**
     * Método encargado de obtener una lista de circulo registral municipio a partir del código del
     * circulo registral.
     *
     * @author javier.aponte
     * @param codigoCirculoRegistral Código del circulo registral
     * @return List<CirculoRegistralMunicipio> lista de circulos registrales muncipio asociados con
     * el código de circulo registral que se recibe como parámetro.
     */
    public List<CirculoRegistralMunicipio> getCirculoRegistralMunicipioByCodigoCirculoRegistral(
        String codigoCirculoRegistral);

    /**
     * Verifica los jobs pendientes a completar su procesamiento en SNC
     *
     * Estos Jobs ya terminaron de ser procesados en el servidor geográfico (Arcgis Server) y están
     * pendientes de completar su ejecución según una serie de pasos a ser ejecutados en Java (Ejm:
     * Crear / Actualizar registros en Base de datos, Generar archivos, Mover el servidor de
     * procesos , etc )
     */
    public void verificarEjecucionJobsPendientesEnSNC();

    /**
     * Obtiene una lista de jobs pendientes por procesar en SNC (Los jobs están organizados por
     * orden de creación)
     *
     * @param cantidad de registros a obtener
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsPendientesEnSNC(Long cantidad);

    /**
     * Obtiene la lista de los jobs de arcgis server que están esperando hace más de n horas y una
     * cantidad determinada.
     *
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsGeograficosEsperando(int horas, int cantidadJobs,
        int numeroReintentos);

    /**
     * Obtiene la lista de los jobs de arcgis server que están en error geografico hace más de n
     * horas y una cantidad determinada
     *
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsGeograficosError(int horas, int cantidadJobs,
        int numeroReintentos);

    /**
	 * Obtiene la lista de los jobs de arcgis server que están en error geografico hace más de n horas y una cantidad determinada
	 * 
	 * @return
	 */
	public List<ProductoCatastralJob>  obtenerJobsGeograficosImagenPredialTerminado(int horas, int cantidadJobs,int numeroReintentos);
    
	/**
	 * Verifica UN jobs pendiente a completar su procesamiento en SNC, buscándoo por id.
     * Este método sirve para pruebas y control específicos de algún flujo.
     * Hace lo mismo que {@link #verificarEjecucionJobsPendientesEnSNC()} pero para el 
     * ProductoCatastralJob identificado con el id dado.
     *
     * @author pedro.garcia
     */
    public void verificarEjecucionJobPendienteEnSNC(Long idJob);

    /**
     * Reintenta el reenvío de un job geográfico que se quedó esperando por más de n horas
     * (parámetro del sistema
     *
     * @param idJob
     */
    public void reenviarJobGeograficoEsperando(Long idJob);

	/**
     * Realiza la creacion del documento referente a la imagen predial
     * @param idJob
     * @return 
     */
	public Boolean generarDocumentoImagenPredialTerminado(Long idJob);
    
    
    /**
     * Metodo que retorna los departamentos de una UOC
     *
     * @param codigoUOC
     * @return
     */
    public List<Departamento> getCacheDepartamentosByCodigoUOC(String codigoUOC);

    /**
     * Método para buscar un documento por id de solicitud y por el id del tipo de documento
     *
     * @param idSolicitud
     * @param idTipoDocumento
     * @author javier.aponte
     */
    public SolicitudDocumento buscarDocumentoPorSolicitudIdAndTipoDocumentoId(
        Long idSolicitud, Long idTipoDocumento);

    /**
     * Método para obtener el registro de acceso de un usuario.
     *
     *
     * @author felipe.cadena
     * @param usuario
     * @return
     */
    public LogAcceso obtenerLogAccesoPorUsuario(String usuario);

    /**
     * Método para registrar un acceso al sistema con procedimiento almacenado
     *
     *
     * @author felipe.cadena
     * @param logAcceso
     * @return -- idlog de acceso
     */
    public Long registrarAcceso(LogAcceso logAcceso);

    /**
     * Método para registrar una accion del usuario con procedimiento almacenado
     *
     *
     * @author felipe.cadena
     * @param logAccion
     */
    public void registrarlogAccion(LogAccion logAccion);

    /**
     * Método para registrar una salida del sistema, recibe el usuario y el motivo por el cual se
     * genero el logout, cierre de sesion, tiemout, etc.
     *
     *
     * @author felipe.cadena
     * @param usuario
     * @param motivo
     */
    public void registrarLogout(String usuario, String motivo);

    /**
     * Método encargado de obtener una lista de usuarios dto con los responsables de conservación o
     * directores territoriales asociados al trámite, según corresponda, dependiendo del tipo de
     * trámite
     *
     * @param tramite
     * @param usuario que mueve el proceso
     * @author javier.aponte
     * @return Lista de responsables de conservación o de directores territoriales
     */
    public UsuarioDTO obtenerResponsableConservacionODirectorTerritorialAsociadoATramite(
        Tramite tramite, UsuarioDTO usuario);

    /**
     * Método encargado de traer todos los grupo productos
     *
     * @author javier.aponte
     * @return
     */
    public List<GrupoProducto> obtenerTodosGrupoProductos();

    /**
     * Método que devuelve la lista de productos generados por id del grupo del producto
     *
     * @cu: CU-TV-PR-0001,CU-TV-PR-0002,CU-TV-PR-0003,CU-TV-PR-0004
     * @param codigoGrupo
     * @return List<Producto>
     * @version:1.0
     * @author javier.aponte
     */
    public List<Producto> obtenerListaProductosPorGrupoId(Long idGrupo);

    /**
     * Método encargado de obtener un producto por código
     *
     * @param codigo código del producto
     * @author javier.aponte
     * @return
     */
    public Producto obtenerProductoPorCodigo(String codigo);

    /**
     * Método que busca un {@link GrupoProducto} por su id
     *
     * @author javier.aponte
     * @param grupoProductoId
     * @return
     */
    public GrupoProducto buscarGrupoProductoPorId(Long grupoProductoId);

    /**
     * Método encargado de almacenar en la base de datos el producto catastral asociado a la
     * solicitud
     *
     * @author javier.aponte
     */
    public ProductoCatastral guardarActualizarProductoCatastralAsociadoASolicitud(
        ProductoCatastral productoCatastral);

    /**
     * Método encargado de buscar en la base de datos los productos catastrales asociados a una
     * solicitud
     *
     * @param solicitudId id de la solicitud
     * @return lista de productos catastrales asociados a la solicitud
     * @author javier.aponte
     */
    public List<ProductoCatastral> buscarProductosCatastralesPorSolicitudId(Long solicitudId);

    /**
     * Método para hacer las validaciones espaciales cuando el trámite es de cancelación de predio o
     * de rectificación de cancelación por doble inscripción. </br>
     * Se hace de forma <b>asincrónica</b>
     *
     * @param idTramite id del tramite que tiene asociado el predio que se quieren validar
     * @param usuario
     * @author pedro.garcia
     */
    public boolean validarGeometriaPrediosTramCancelPredioA(Long idTramite, UsuarioDTO usuario);

    /**
     * Método para hacer las validaciones espaciales cuando el trámite es de cancelación de predio o
     * de rectificación de cancelación por doble inscripción. </br>
     * Se hace de forma <b>sincrónica</b>.
     *
     * @param idTramite id del tramite que tiene asociado el predio que se quieren validar
     * @param usuario
     * @author pedro.garcia
     *
     */
    public boolean validarGeometriaPrediosTramCancelPredioS(Long idTramite, UsuarioDTO usuario)
        throws Exception;

    /**
     * Método encargado de almacenar en la base de datos los productos catastrales detalle en la
     * base de datos
     *
     * @author javier.aponte
     */
    public ProductoCatastralDetalle guardarProductoCatastralDetalle(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Método encargado de buscar en la base de datos los productos catastrales detalles asociados a
     * un producto catastral
     *
     * @param productoCatastralId id del producto catastral
     * @return lista de productos catastrales detalle asociados al producto catastral
     * @author javier.aponte
     */
    public List<ProductoCatastralDetalle> buscarProductosCatastralesDetallePorProductoCatastralId(
        Long productoCatastralId);

    /**
     * Metodo para obtener un registro de MunicipioComplemento por el codigo del municipio
     *
     * @author felipe.cadena
     * @param codigo
     * @return
     */
    public MunicipioComplemento obtenerMunicipioComplementoPorCodigo(String codigo);

    /**
     * Método encargado de guardar un producto generado en el gestor documental
     *
     * @author javier.aponte
     */
    public String guardarProductoGeneradoEnGestorDocumental(String path,
        String codigoEstructuraOrganizacional);

    /**
     * Método encargado de almacenar en la base de datos el producto catastral error
     *
     * @author javier.aponte
     */
    public ProductoCatastralError guardarActualizarProductoCatastralError(
        ProductoCatastralError productoCatastralError);

    /**
     * Obtiene el porcentaje de incremento de un predio en una vigencia en particular
     *
     * @author felipe.cadena
     * @param vigencia
     * @param idPredio
     * @return
     */
    public Double obtenerPorcentajeIncremento(Date vigencia, Long idPredio);

    /**
     * Método encargado de generar la ficha predial digital para productos a partir de los números
     * prediales separados por coma
     *
     * @param numerosPrediales
     * @param usuario
     * @return variable booleana que indica si el llamado a generar las imagenes de la ficha predial
     * se realizó correctamente
     * @author javier.aponte
     */
    public boolean generarFichaPredialDigitalProductos(
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario);

    /**
     * Método encargado de generar la imagen predial 
     *
     * @param numeroPredial
     * @param usuario
     * @return variable booleana que indica si el llamado a generar la imagene se realizó correctamente
     * @author carlos.ferro
     */
    public boolean generarImagenPredial(ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario);


	/**
     * Método para procesar el job de la ficha predial digital en el proceso de productos
     *
     * @author javier.aponte
     *
     * @param job job con la información del job de la ficha predial digital
     * @return boolean que indica si se generó exitosamente la ficha predial digital
     */
    public boolean procesarFichaPredialDigitalProductos(ProductoCatastralJob job);

    /**
     * Método encargado de generar el certificado plano predial catastral para productos a partir de
     * la información que está en producto catastral detalle
     *
     * @param productoCatastralDetalle
     * @param usuario
     * @return variable booleana que indica si el llamado a generar el certificado plano predial
     * catastral fue exitoso
     * @author javier.aponte
     */
    public boolean generarCertificadoPlanoPredialCatastralProductos(
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario);

    /**
     * Método para procesar el job de la ficha predial digital en el proceso de productos
     *
     * @author javier.aponte
     *
     * @param job job con la información del job de la ficha predial digital
     * @return boolean que indica si se generó exitosamente la ficha predial digital
     */
    public boolean procesarCertificadoPlanoPredialCatastralProductos(ProductoCatastralJob job);

    /**
     * Método encargado de generar la carta catastral urbana para productos a partir de la
     * información que está en producto catastral detalle
     *
     * @param productoCatastralDetalle
     * @param usuario
     * @return variable booleana que indica si el llamado a generar la carta catastral urbana fue
     * exitoso
     * @author javier.aponte
     */
    public boolean generarCartaCatastralUrbanaProductos(
        ProductoCatastralDetalle productoCatastralDetalle, UsuarioDTO usuario);

    /**
     * Método para procesar el job de la carta catastral urbana en el proceso de productos
     *
     * @author javier.aponte
     *
     * @param job job con la información del job de la carta catastral urbana
     * @return boolean que indica si se generó exitosamente la carta catastral urbana
     */
    public boolean procesarCartaCatastralUrbanaProductos(ProductoCatastralJob job);

    /**
     * Elimina un producto catastral detalle de la BD
     *
     * @author leidy.gonzalez
     * @param productoCatastralDetalle
     */
    public void eliminarProductoCatastralDetalle(ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * @author leidy.gonzalez Metodo encargado de contar la cantidad de registros prediales a
     * generar en producto catastral creado.
     * @param parametros
     * @return
     */
    public Object[] contarRegistrosPrediales(List<Object> parametros);

    /**
     * Método encargado de borrar un tràmite documento y el documento por tramite id y tipo de
     * documento id, en la base de datos y en el gestor documental, en caso que el borrado sea
     * exitoso retorna una variable booleana con el valor verdadero
     *
     * @param tramiteId
     * @param tipoDocumentoId
     * @return true en el caso que el borrado sea exitoso
     */
    public boolean borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(Long tramiteId,
        Long tipoDocumentoId);

    /**
     * Método encargado de guardar los predios encontrados entre un rango de numeros prediales
     * ingresados en la tabla Producto Catastral Detalle
     *
     * @author leidy.gonzalez
     * @param productoCatastralDetalle
     * @return true en el caso que el borrado sea exitoso
     */
    public void guardarNumerosPredialesEntreRangosEnProductoCatastralDetalle(
        ProductoCatastralDetalle productoCatastralDetalle);

    /**
     * Método que retorna la lista de formato de productos por el campo formato
     *
     * @param productoId Id del producto para encontrar los valores del formato
     * @author leidy.gonzalez
     */
    public List<ProductoFormato> getCacheFormatoPorProducto(Long productoId);

    /**
     * Metodo para obtener el numero de accesos simultaneos de un usuario
     *
     * @author felipe.cadena
     * @param usuario
     * @return
     */
    public Long obtenerNumeroAccesosIniciadosPorUsuario(String usuario);

    /**
     * Metodo para obtener el numeo de conexiones de una usuario en un momento especifico
     *
     * @author felipe.cadena
     * @param usuario
     * @return
     */
    public Long obtenerNumeroConexionesPorUsuario(String usuario);

    /**
     * Metodo para guardar una nueva conexion
     *
     * @author felipe.cadena
     * @param cu
     * @return
     */
    public ConexionUsuario guardarActualizarConexionUsuario(ConexionUsuario cu);

    /**
     * Metodo para eliminar una conexion existente
     *
     * @author felipe.cadena
     * @param cu
     */
    public void eliminarConexionUsuario(ConexionUsuario cu);

    // end of interface
    /**
     * Metodo para consultar una conexion por el id
     *
     * @author felipe.cadena
     * @param id
     * @return
     */
    public ConexionUsuario obtenerConexionUsuarioPorId(Long id);

    /**
     * Retorna la conexion asociada al id de sesion
     *
     * @author felipe.cadena
     * @param idSesion
     * @return
     */
    public ConexionUsuario obtenerConexionUsuarioPorSesionId(String idSesion);

    /**
     * Método encargado de procesar los registros prediales de productos
     *
     * @param productoCatastral
     * @author javier.aponte
     */
    public boolean procesarRegistrosPredialesProductosPDF(ProductoCatastral job);

    /**
     * Obtiene una lista de jobs pendientes por procesar en SNC (Los jobs están organizados por
     * orden de creación)
     *
     * @param cantidad de registros a obtener
     * @return Lista de productos catastrales de los jobs que están pendientes en el SNC
     * @author javier.aponte
     */
    public List<ProductoCatastral> obtenerJobsReportesPredialesPendientesEnSNC(Long cantidad);

    /**
     * Verifica UN job pendiente a completar su procesamiento en SNC, buscándolo por id. Este método
     * sirve para pruebas y control específicos de algún flujo. Hace lo mismo que
     * {@link #verificarEjecucionJobsPendientesEnSNC()} pero para el ProductoCatastralJob
     * identificado con el id dado.
     *
     * @author javier.aponte
     */
    public void verificarEjecucionJobReportesPredialesPendienteEnSNC(Long idJob);

    /**
     * Obtiene los jobs asociados a un tramite
     *
     * @author felipe.cadena
     * @param tramiteId
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsTramite(Long tramiteId);
    
    /**
     * 
     * @param tipoProducto
     * @param usuarioLog
     * @return 
     */
    public ProductoCatastralJob obtenerJobsCargueActualizacion(String tipoProducto, String usuarioLog);

    /**
     * Obtiene los jobs asociados a un tramite
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param tipoProducto
     * @return
     */
    public ProductoCatastralJob obtenerUltimoJobPorTramite(Long tramiteId, String tipoProducto);

    /**
     * Método que retorna las categorias de reporte a generar
     *
     * @return
     * @author leidy.gonzalez
     */
    public List<RepReporte> obtenerNombreCategoriaReporte();

    /**
     * Obtiene la lista de los jobs de BD en la tabla RepReporteEjecucion que estan esperando.
     *
     * @author leidy.gonzalez
     * @return
     */
    public List<RepReporteEjecucion> obtenerJobsReporteEjecucionTerminado(Long cantidad);

    /**
     * Reintenta el reenvio de un job en la tabla RepReporteEjecucion que se queda esperando
     * (idJob).
     *
     * @author leidy.gonzalez
     * @return
     */
    public void reenviarJobReportesTerminado(Long idJob);

    /**
     * Método que busca un {@link RepReporte} por su id
     *
     * @author david.cifuentes
     *
     * @param idRepReporte
     * @return
     */
    public RepReporte buscarRepReportePorId(Long idRepReporte);

    /**
     * Método que busca una lista de {@link RepReporte} por su categoría
     *
     * @author david.cifuentes
     *
     * @param categoria
     * @return
     */
    public List<RepReporte> buscarRepReportesPorCategoria(String categoria);

    /**
     * Metodo que verifica si un documento se encuentra almacenado en el gestor documental, retorna
     * null si courre un error en la consulta.
     *
     *
     * @author felipe.cadena
     * @param documento
     * @return
     */
    public Boolean validarExistenciaDocumentoGestorDocumental(Documento documento);

    /**
     * Metodo para obtener las UOC relacionadas a una territorial determinada
     *
     * @author felipe.cadena
     *
     * @param codigoTerritorial
     * @return
     */
    public List<EstructuraOrganizacional> buscarUOCporCodigoTerritorial(String codigoTerritorial);

    /**
     * Obtiene los municipios asociados a una oficina
     *
     * @author felipe.cadena
     *
     * @param codigoOficina
     * @return
     */
    public List<Municipio> buscarMunicipioPorIdOficina(String codigoOficina);

    /**
     * Retorna un mapa con los dominios de los tipos de solicitud, tipo de tramites y subtipos de
     * tramites agrupados, las llaves de los grupos son el nombre del dominio padre seguido del
     * codigo, en el caso de las solicitudes existe un grupo con todos los tipos que solo tiene el
     * nombre del dominio solicitud.
     *
     * @author felipe.cadena
     * @return
     */
    public Map<String, List<Dominio>> obtenerTiposSolicitudesTramites();

    /**
     * Obtiene los jobs que deben ser enviados a aplicar cambios
     *
     * @param cantidadJobs cantidad de jobs a recuperar en la consulta.
     * @author andres.eslava
     */
    public List<ProductoCatastralJob> obtenerJobsEsperandoUnoPorManzana(long cantidadJobs) throws
        Exception;

    /**
     * envia los job geograficos para procesamiento de aplicar cambios geograficos en arcgis server
     *
     * @param idJob identificar del job
     * @author andres.eslava
     */
    public void aplicarCambiosGeograficos(ProductoCatastralJob job) throws Exception;

    /**
     * Obtiene un parametro de la DB para confirmar que se desea ejecutar una tarea programada
     *
     * @author andres.eslava
     */
    public Boolean confirmarEjecucionJobAplicarCambios() throws Exception;

    /**
     * Obtiene un parametro de la DB para confirmar que se desea ejecutar una tarea programada
     *
     * @author andres.eslava
     */
    public Boolean confirmarEjecucionJobErrorGeografico() throws Exception;

    /**
     * Obtiene los jobs asociados a un tramite, si el parametros estado es null no realiza ningun
     * filtro
     *
     * @author felipe.cadena
     * @param estados
     * @param tramiteId
     * @return
     */
    public List<ProductoCatastralJob> obtenerJobsPorEstadoTramiteIdTipo(List<String> estados,
        Long tramiteId, String tipo);

    /**
     * Obtiene los parametros del editor a partir del prefijo editor
     *
     * @author andres.eslava
     */
    public List<ParametroEditorVO> obtenerParametrosEditor();

    /**
     * Procesa las inconsistencias obtenidas en el job
     *
     * @author felipe.cadena
     * @param job
     * @return
     */
    public Boolean guardarInconsistenciasAsincronicas(ProductoCatastralJob job);

    /**
     * Obtiene la lista de los jobs de jasper server que están esperando, una cantidad de jobs
     * especifica.
     *
     * @param cantidadJobs cantidad maxima de jobs a reenviar
     * @return lista de jobs resultado del query
     * @author javier.aponte
     */
    public List<ProductoCatastralJob> obtenerJobsJasperServerEsperando(int cantidadJobs);

    /**
     * Verifica UN job pendiente a completar su procesamiento en SNC, buscándolo por id. Este método
     * sirve para pruebas y control específicos de algún flujo. Hace lo mismo que
     * {@link #verificarEjecucionJobsPendientesEnSNC()} pero para el ProductoCatastralJob
     * identificado con el id dado.
     *
     * @author javier.aponte
     * @param idJob
     */
    public void verificarEjecucionJobJasperServerPendientesEnSNC(Long idJob);

    /**
     * Método encargado de almacenar el documento de Firma Mecanica de Usuarios
     *
     * @param documento
     * @author leidy.gonzalez
     */
    public Documento guardarDocumentoFirmaMecanicaUsuario(Documento documento);

    /**
     * Método encargado de almacenar la imagen de la Firma Mecanica de Usuarios en el servidor de la
     * documental
     *
     * @param path,codigoEstructuraOrganizacional
     * @author leidy.gonzalez
     */
    public String guardarFirmaMecanicaUsuarioEnGestorDocumental(String path,
        String codigoEstructuraOrganizacional);

    /**
     * Método encargado de eliminar un documento de firma mecanica de usuario
     *
     * @param documento
     * @author leidy.gonzalez
     */
    public void eliminarDocumentoFirmaMecanica(Documento documento);

    /**
     * Método encargado de eliminar una firma de usuario
     *
     * @param firmaUsuario
     * @author leidy.gonzalez
     */
    public FirmaUsuario guardarFirmaUsuario(FirmaUsuario firmaUsuario);

    /**
     * Método encargado de consultar un documento de firma de usuario
     *
     * @param idDocumento
     * @author leidy.gonzalez
     */
    public Documento buscarFirmaUsuario(Long idDocumento);

    /**
     * Método encargado de consultar una firma de usuario
     *
     * @param nombreUsuario
     * @author leidy.gonzalez
     */
    public FirmaUsuario consultarUsuarioFirma(String nombreUsuario);

    /**
     * Método encargado de eliminar una firma de usuario
     *
     * @param firmaUsuario
     * @author leidy.gonzalez
     */
    public void eliminarFirmaUsuario(FirmaUsuario firmaUsuario);

    /**
     * Obtiene una firma de usuario consultandolo por el usuario
     *
     * @param nombre
     * @author leidy.gonzalez
     */
    public FirmaUsuario buscarDocumentoFirmaPorUsuario(String nombre);

    /**
     * Obtiene el documento asociado a un numero de documento
     *
     * @author leidy.gonzalez:: #12528::26/05/2015 Se modifica por CU_187
     * @return
     */
    public Documento buscarDocumentoPorNumeroDocumentoAndTipoDocumento(String numDocumento);

    /**
     * Obtiene las UOC de una territorial siempre y cuando estas no tengan ya el rol que se esta
     * buscando Ejemplo: Si Soacha tiene un Lider Tecnico, al buscar las UOC de Cundinamarca con el
     * rol Lider Tecnico, no debe aparecer Soacha
     *
     * @author lorena.salamanca
     * @param codigoTerritorial
     * @param rol
     * @return
     */
    public List<EstructuraOrganizacional> buscarUOCporTerritorialSinRolEnUOC(
        String codigoTerritorial, ERol rol);

// end of interface
    /**
     * Obtiene los documentos asociados al tipo y a numero indicados en los parametros
     *
     * @author felipe.cadena
     * @param numeroDoc
     * @param idTipoDoc
     * @return
     */
    public List<Documento> buscarDocumentoPorNumeroDocumentoYTipo(String numeroDoc, Long idTipoDoc);
    
    /**
     * @author hector.arias
     * @param numeroDocumento
     * @param tipoDocumentoId
     * @return 
     */
    public List<Documento> buscarDocumentoPorTipoActualizacion(String numeroDocumento, Long tipoDocumentoId);
    
    /**
     * @author hector.arias
     * @param numeroDoc
     * @param idTipoDoc
     * @return 
     */
    public Documento buscarPorNumeroUltimoDocumentoYTipo(String numeroDoc, Long idTipoDoc);

    /**
     * Obtiene los jobs asociados a una lista de tramites
     *
     * @author leidy.gonzalez
     * @param idTramites
     * @param tipoProducto
     * @return
     */
    public List<ProductoCatastralJob> obtenerUltimoJobPorIdTramites(List<Long> idTramites,
        String tipoProducto);

    /**
     * Obtiene imagen simple de predios en cordenadas locales
     *
     * @author andres.eslava
     * @param predios
     * @param usuario
     * @return
     */
    public List<ImagenPredioVO> obtenerImagenSimplePredios(String predios, UsuarioDTO usuario);

    /**
     * Obtiene imagen avanzada de predios en cordenadas locales
     *
     * @author andres.eslava
     * @param predios
     * @param usuario
     * @return
     */
    public List<ImagenPredioVO> obtenerImageneAvanzadaPredios(String predios, UsuarioDTO usuario);

    /**
     * Obtiene un ProductoCatastralDetalle por id
     *
     * @author lorena.salamanca
     * @param productoCatastralDetalleId
     * @return
     */
    public ProductoCatastralDetalle buscarProductoCatastralesDetallePorId(
        Long productoCatastralDetalleId);

    /**
     * Consulta el ultimo {@link ProductoCatastralJob} asociado a un
     * {@link ProductoCatastralDetalle}
     *
     * @author david.cifuentes
     * @param idProductoCatastralDetalle
     * @return
     */
    public ProductoCatastralJob buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId(
        Long idProductoCatastralDetalle);

    /**
     * @author dumar.penuela
     * @param oUC
     * @param territorial
     * @return
     */
    List<EstructuraOrganizacional> buscarEstructuraOrganizacionalByNombre(String oUC,
        String territorial);

    /**
     * @author dumar.penuela
     * @param departamentoId
     * @param municipioId
     * @return
     */
    public Boolean validaMunicipioDepartamento(String departamentoId, String municipioId);

    /**
     * Método que realiza el cargue de la plantilla para una unidad operativa o territorial
     * seleccionada
     *
     * @author david.cifuentes
     * @param estructuraOrganizacionalCodigo
     * @return
     */
    public PlantillaSeccionReporte buscarPlantillaSeccionReportePorCodEstructuraOrganizacional(
        String estructuraOrganizacionalCodigo);

    /**
     * Método que guarda una plantilla sección reporte
     *
     * @param plantilla
     * @return
     */
    public PlantillaSeccionReporte guardarPlantillaSeccionReporte(
        PlantillaSeccionReporte plantilla);

    /**
     * Metodo que retorna la lista de numeros prediales ubicado en un rango, completos o solo los
     * que no son fichas ni cancelados dependiendo el parametro 'completo'
     *
     * @author felipe.cadena
     * @param nPredialInicial
     * @param nPredialFinal
     * @param completo
     * @return
     */
    public List<String> obtenerNumerosPredialesEntreRangoPredial(String nPredialInicial,
        String nPredialFinal, boolean completo);

    /**
     * Metodo para obtener la lista de todos los municipios asociados a entidades delegadas
     *
     * @author felipe.cadena
     * @return
     */
    public List<Municipio> obtenerMunicipiosDelegados();
    
    
    public List<Municipio> obtenerMunicipiosHabilitados();

    /* Consulta una lista de tramiteDocumentos asociados a un documento
     *
     * @author leidy.gonzalez @param documentoId @return
     */
    public List<TramiteDocumento> consultaTramiteDocumentosPorDocumentoId(
        Long documentoId);

    /**
     * Consulta las urls de los {@link PlantillaSeccionReporte} y si no existen debido al borrado
     * diario las crea nuevamente.
     *
     * @author david.cifuentes
     * @throws Exception
     */
    public void verificarUrlSeccionesPlantillaReportes() throws Exception;

    /* Método que realiza la busqueda de una estructura organizacional por un código de municipio
     *
     * @param codigo @return
     */
    public String buscarEstructuraOrgPorMunicipioCod(String codigo);

    /**
     * Método que realiza la búsqueda de los municipios de una estructura organizacional por su
     * jurisdicción
     *
     * @author david.cifuentes
     * @param codigo
     * @return
     */
    public List<Municipio> findMunicipiosbyCodigoEstructuraOrganizacional(
        String codigo);

    /**
     * Método que realiza la consulta de todos los roles registrados para el SNC según su estado.
     *
     * @author david.cifuentes
     * @param estado
     * @return
     */
    public List<Grupo> consultarGrupoPorEstadoActivo(String estado);

    /**
     * Método que realiza la consulta de todos los roles registrados para el SNC
     *
     * @author david.cifuentes
     * @return
     */
    public List<Grupo> buscarGrupos();

    /**
     * Retorna la lista de todas las estructuras organizacionales
     *
     * @author david.cifuentes
     * @return
     */
    public List<EstructuraOrganizacional> buscarTodasTerritoriales();

    /**
     * Método que realiza la actualización de un grupo
     *
     * @param rolSeleccionado
     */
    public void guardarGrupo(Grupo rolSeleccionado);

    public String guardarReporteGeneradoEnGestorDocumental(String rutaArchivoLocal, String tipo,
        String tipoReporte,
        String codigoOrganizacional, String departamento, String municipio, String codigoReporte,
        String anio);

    /**
     * Método que retorna el código homologado del nombre de una territorial
     *
     * @param territorial
     * @return
     */
    public String obtenerCodigoHomologadoNombreTerritorial(String territorial);

    /**
     * Metodo para obtener un componente por su nombre
     *
     * @param nombre
     * @return
     */
    public Componente obtenerComponentePorNombre(String nombre);

    /**
     * Método que retorna la lista de dominios por el campo nombre ordenados por id
     *
     * @param d Referirse a la enumeración EDominio para encontrar los valores de los dominios
     * @return
     */
    @SuppressWarnings("rawtypes")
    public List<Dominio> getCacheDominioPorNombreOrdenadoPorId(Enum d);

    /**
     * Método que retorna los tipos de reporte a generar por categoria
     *
     * @return
     * @author leidy.gonzalez
     */
    public List<RepReporte> obtenerListaTipoReportePorTipoReporte(String categoria);

    /**
     * Método que retorna la lista de circulos registrales
     *
     * @return
     * @author leidy.gonzalez
     */
    public List<CirculoRegistral> obtenerListaCirculoRegistral();

    /**
     * Método que retorna la lista de nombres de tipos de reportes prediales
     *
     * @return
     * @author leidy.gonzalez
     */
    public List<String> obtenerListaNombreTipoReporte();

    /**
     * Método que retorna la lista de tipos de reportes prediales
     *
     * @return
     * @author leidy.gonzalez
     */
    public List<RepReporte> consultarListaTipoReportePorNombre(List<String> nombresTiposReportes);

    /**
     * Método que retorna la lista de departamentos de RepTerritorialReporte por el codigo de la
     * Territorial
     *
     * @return
     * @author leidy.gonzalez
     */
    public List<Departamento> obtenerRepTerritorialReporte(String codigoTerritorial,
        String codigoUoc);

    /**
     * Método que busca lista documentos por su id de tramite y tipo de documentos
     *
     * @author leidy.gonzalez
     * @param tramiteId, idTipoDocumento
     * @return
     */
    public List<Documento> buscarDocumentosPorTramiteIdyTiposDocumento(Long tramiteId,
        List<Long> idTipoDocumentos);

    /**
     * Metodo para obtener una lista de dominios ordenados por su valor
     *
     * @author felipe.cadena
     * @param d
     * @return
     */
    public List<Dominio> findByNombreOrderByValor(Enum d);

    /**
     * Método que actualiza el estado de los jobs asociados a la solicitud
     *
     * @author hector.arias
     * @param job, nuevoEstado, nuevoResultado
     * @return
     */
    public void reenviarJobsProductosCatastrales(ProductoCatastralJob job, String nuevoEstado,
        String nuevoResultado);

    /**
     * Obtiene municipios con informacion en el SNC no delegados
     *
     * @author felipe.cadena
     * @return
     */
    public List<Municipio> buscarMunicipiosSNCNoDelegada();

    /**
     * Determina si un usuario tienen permisos nacionales para un reporte dado
     *
     * @author felipe.cadena
     * @return
     */
    public boolean determinarReporteNacional(String usuario, Long reporteId);
    
    /**
     * Método que realiza la consulta de todos los nombres de roles registrados para el SNC según su estado.
     *
     * @author juan.cruz
     * @param estado
     * @return
     */
    public List<String> obtenerListaNombresGrupo(String estado);

	/**
	 * Elimina las inconsistencias de un tramite
	 *
	 * @author vsocarras
	 * @return
	 */
	void eliminarInconsistenciasPorTramite(Long idTramite);
    
    // end of interface
}
