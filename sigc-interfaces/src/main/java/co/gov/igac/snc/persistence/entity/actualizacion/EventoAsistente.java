package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the EVENTO_ASISTENTE database table.
 *
 * @author franz.gamba
 */
@Entity
@Table(name = "EVENTO_ASISTENTE")
public class EventoAsistente implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8378529905072931790L;

    private Long id;
    private String cargo;
    private String documentoIdentificacion;
    private Date fechaLog;
    private String institucion;
    private String nombre;
    private String usuarioLog;
    private ActualizacionEvento actualizacionEvento;

    public EventoAsistente() {
    }

    @Id
    @SequenceGenerator(name = "EVENTO_ASISTENTE_ID_GENERATOR",
        sequenceName = "EVENTO_ASISTENTE_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENTO_ASISTENTE_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCargo() {
        return this.cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Column(name = "DOCUMENTO_IDENTIFICACION")
    public String getDocumentoIdentificacion() {
        return this.documentoIdentificacion;
    }

    public void setDocumentoIdentificacion(String documentoIdentificacion) {
        this.documentoIdentificacion = documentoIdentificacion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getInstitucion() {
        return this.institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ActualizacionEvento
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_EVENTO_ID")
    public ActualizacionEvento getActualizacionEvento() {
        return this.actualizacionEvento;
    }

    public void setActualizacionEvento(ActualizacionEvento actualizacionEvento) {
        this.actualizacionEvento = actualizacionEvento;
    }

}
