package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the LOG_ACCESO database table.
 *
 */
@Entity
@Table(name = "LOG_ACCESO")
public class LogAcceso implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private Date fechaInicioSesion;
    private Date fechaFinSesion;
    private String login;
    private String estadoSesion;
    private String maquinaCliente;
    private String exploradorCliente;

    public LogAcceso() {
    }

    public LogAcceso(String login, String estadoSesion, String maquinaCliente,
        String exploradorCliente) {
        this.login = login;
        this.estadoSesion = estadoSesion;
        this.maquinaCliente = maquinaCliente;
        this.exploradorCliente = exploradorCliente;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOG_ACCESO_ID_SEQ")
    @SequenceGenerator(name = "LOG_ACCESO_ID_SEQ", sequenceName = "LOG_ACCESO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INICIO_SESION")
    public Date getFechaInicioSesion() {
        return fechaInicioSesion;
    }

    public void setFechaInicioSesion(Date fechaInicioSesion) {
        this.fechaInicioSesion = fechaInicioSesion;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_FIN_SESION")
    public Date getFechaFinSesion() {
        return fechaFinSesion;
    }

    public void setFechaFinSesion(Date fechaFinSesion) {
        this.fechaFinSesion = fechaFinSesion;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Column(name = "ESTADO_SESION")
    public String getEstadoSesion() {
        return estadoSesion;
    }

    public void setEstadoSesion(String estadoSesion) {
        this.estadoSesion = estadoSesion;
    }

    @Column(name = "MAQUINA_CLIENTE")
    public String getMaquinaCliente() {
        return maquinaCliente;
    }

    public void setMaquinaCliente(String maquinaCliente) {
        this.maquinaCliente = maquinaCliente;
    }

    @Column(name = "EXPLORADOR_CLIENTE")
    public String getExploradorCliente() {
        return exploradorCliente;
    }

    public void setExploradorCliente(String exploradorCliente) {
        this.exploradorCliente = exploradorCliente;
    }

}
