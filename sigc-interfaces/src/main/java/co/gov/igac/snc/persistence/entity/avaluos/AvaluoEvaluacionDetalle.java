package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the AVALUO_EVALUACION_DETALLE database table.
 *
 */
@Entity
@Table(name = "AVALUO_EVALUACION_DETALLE")
public class AvaluoEvaluacionDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private String observaciones;
    private String usuarioLog;
    private Double valor;
    private AvaluoEvaluacion avaluoEvaluacion;
    private VariableEvaluacionAvaluo variableEvaluacionAvaluo;

    public AvaluoEvaluacionDetalle() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_EVALUACION_DETALLE_ID_GENERATOR", sequenceName =
        "AVALUO_EVALUACION_DETAL_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_EVALUACION_DETALLE_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    //bi-directional many-to-one association to AvaluoEvaluacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_EVALUACION_ID")
    public AvaluoEvaluacion getAvaluoEvaluacion() {
        return this.avaluoEvaluacion;
    }

    public void setAvaluoEvaluacion(AvaluoEvaluacion avaluoEvaluacion) {
        this.avaluoEvaluacion = avaluoEvaluacion;
    }

    //bi-directional many-to-one association to VariableEvaluacionAvaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VARIABLE_EVALUACION_ID")
    public VariableEvaluacionAvaluo getVariableEvaluacionAvaluo() {
        return this.variableEvaluacionAvaluo;
    }

    public void setVariableEvaluacionAvaluo(VariableEvaluacionAvaluo variableEvaluacionAvaluo) {
        this.variableEvaluacionAvaluo = variableEvaluacionAvaluo;
    }

    /**
     * Se usa para los AvaluoEvaluacionDetalle cuyo tipo de variable sea de EFICIENCIA y EFICACIA,
     * con el fin de permitir la modificación desde la vista
     *
     * @author rodrigo.hernandez
     */
    @Transient
    public boolean isEsEditable() {
        boolean result = false;

        /*
         * Se compara que el valor del detalle y el valor minimo de la variable sean iguales para
         * determinar si se puede editar o no el detalle
         */
        if (this.valor == this.variableEvaluacionAvaluo.getValorMinimo()) {
            result = true;
        }

        return result;
    }

}
