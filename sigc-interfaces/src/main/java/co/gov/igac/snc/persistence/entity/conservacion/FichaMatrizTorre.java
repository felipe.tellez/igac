package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the FICHA_MATRIZ_TORRE database table.
 *
 * MODIFICACIONES:
 *
 * @author fabio.navarrete -> Se agrega copy constructor
 *
 * @modified juan.agudelo 21-03-12 Adición de la secuencia FICHA_MATRIZ_TORRE_ID_GENERATOR
 *
 * @modified david.cifuentes :: Adición de la variable unidadesSotanos, :: Cambio del name de la
 * columna "UNIDADES" a "UNIDADES_PRIVADAS" :: Cambio de tipo de las variables pisos, sotanos,
 * torre, unidades a Long :: 19/07/12
 * @modified david.cifuentes :: Adición de la variable estado :: 08/09/15
 *
 */
@Entity
@Table(name = "FICHA_MATRIZ_TORRE")
public class FichaMatrizTorre implements Serializable {

    private static final long serialVersionUID = -274483626830632792L;

    private Long id;
    private Date fechaLog;
    private Long pisos;
    private Long sotanos;
    private Long torre;
    private Long unidades;
    private String usuarioLog;
    private FichaMatriz fichaMatriz;
    private Long unidadesSotanos;
    private String estado;

    public FichaMatrizTorre() {
    }

    /**
     * Copy Constructor
     *
     * @param fichaMatrizTorre
     * @author fabio.navarrete
     */
    public FichaMatrizTorre(FichaMatrizTorre fichaMatrizTorre) {
        this.fechaLog = fichaMatrizTorre.getFechaLog();
        this.fichaMatriz = fichaMatrizTorre.getFichaMatriz();
        this.id = fichaMatrizTorre.getId();
        this.pisos = fichaMatrizTorre.getPisos();
        this.sotanos = fichaMatrizTorre.getSotanos();
        this.torre = fichaMatrizTorre.getTorre();
        this.unidades = fichaMatrizTorre.getUnidades();
        this.usuarioLog = fichaMatrizTorre.getUsuarioLog();
        this.unidadesSotanos = fichaMatrizTorre.getUnidadesSotanos();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "FICHA_MATRIZ_TORRE_ID_GENERATOR")
    @SequenceGenerator(name = "FICHA_MATRIZ_TORRE_ID_GENERATOR", sequenceName =
        "FICHA_MATRIZ_TORRE_ID_GENERATOR", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public Long getPisos() {
        return this.pisos;
    }

    public void setPisos(Long pisos) {
        this.pisos = pisos;
    }

    public Long getSotanos() {
        return this.sotanos;
    }

    public void setSotanos(Long sotanos) {
        this.sotanos = sotanos;
    }

    public Long getTorre() {
        return this.torre;
    }

    public void setTorre(Long torre) {
        this.torre = torre;
    }

    @Column(name = "UNIDADES_PRIVADAS", nullable = false, precision = 4)
    public Long getUnidades() {
        return this.unidades;
    }

    public void setUnidades(Long unidades) {
        this.unidades = unidades;
    }

    @Column(name = "UNIDADES_SOTANOS", nullable = false, precision = 4)
    public Long getUnidadesSotanos() {
        return this.unidadesSotanos;
    }

    public void setUnidadesSotanos(Long unidadesSotanos) {
        this.unidadesSotanos = unidadesSotanos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to FichaMatriz
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FICHA_MATRIZ_ID")
    public FichaMatriz getFichaMatriz() {
        return this.fichaMatriz;
    }

    public void setFichaMatriz(FichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static FichaMatrizTorre parseFichaMatrizTorres(HFichaMatrizTorre hFichaMatrizTorre) {
        FichaMatrizTorre fichaMatrizTorre = new FichaMatrizTorre();

        fichaMatrizTorre.setId(hFichaMatrizTorre.getId().getId());
        fichaMatrizTorre.setFechaLog(hFichaMatrizTorre.getFechaLog());
        fichaMatrizTorre.setPisos(hFichaMatrizTorre.getPisos());
        fichaMatrizTorre.setSotanos(hFichaMatrizTorre.getSotanos());
        fichaMatrizTorre.setTorre(hFichaMatrizTorre.getTorre());
        fichaMatrizTorre.setUnidades(hFichaMatrizTorre.getUnidades());
        fichaMatrizTorre.setUsuarioLog(hFichaMatrizTorre.getUsuarioLog());
        fichaMatrizTorre.setFichaMatriz(FichaMatriz.parseFichaMatriz(hFichaMatrizTorre.
            getHFichaMatriz()));
        fichaMatrizTorre.setUnidadesSotanos(hFichaMatrizTorre.getUnidadesSotanos());
        fichaMatrizTorre.setEstado(hFichaMatrizTorre.getEstado());

        return fichaMatrizTorre;
    }

}
