package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the H_FM_CONSTRUCCION_COMPONENTE database table.
 *
 */
@Embeddable
public class HFmConstruccionComponentePK implements Serializable {
    //default serial version id, required for serializable classes.

    private static final long serialVersionUID = 1L;
    private long hPredioId;
    private long id;

    public HFmConstruccionComponentePK() {
    }

    @Column(name = "H_PREDIO_ID")
    public long getHPredioId() {
        return this.hPredioId;
    }

    public void setHPredioId(long hPredioId) {
        this.hPredioId = hPredioId;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof HFmConstruccionComponentePK)) {
            return false;
        }
        HFmConstruccionComponentePK castOther = (HFmConstruccionComponentePK) other;
        return (this.hPredioId == castOther.hPredioId) &&
            (this.id == castOther.id);

    }

    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.hPredioId ^ (this.hPredioId >>> 32)));
        hash = hash * prime + ((int) (this.id ^ (this.id >>> 32)));

        return hash;
    }
}
