package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import co.gov.igac.snc.persistence.util.IInscripcionCatastral;
import co.gov.igac.snc.persistence.util.IProyeccionObject;
import co.gov.igac.snc.util.Utilidades;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MODIFICACIONES PROPIAS A LA CLASE PPredioZona:
 *
 * @author fabio.navarrete Se agregó serialVersionUID
 * @author fabio.navarrete Se agregó el método y atributo transient para consultar el valor de la
 * propiedad cancelaInscribe
 */
/**
 * PPredioZona entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "P_PREDIO_ZONA", schema = "SNC_CONSERVACION")
public class PPredioZona implements java.io.Serializable, IProyeccionObject, IInscripcionCatastral,
    Cloneable {

    // Fields
    private Long id;
    private PPredio PPredio;
    private String zonaFisica;
    private String zonaGeoeconomica;
    private Double valorM2Terreno;
    private Double area;
    private Double avaluo;
    private Date vigencia;
    private String cancelaInscribe;
    private String usuarioLog;
    private Date fechaLog;
    private Date fechaInscripcionCatastral;
    private String cancelaInscribeValor;

    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

    // Constructors
    /** default constructor */
    public PPredioZona() {
    }

    /** minimal constructor */
    public PPredioZona(Long id, PPredio PPredio, String zonaFisica,
        Double valorM2Terreno, Double area, Double avaluo, Date vigencia,
        String cancelaInscribe, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.zonaFisica = zonaFisica;
        this.valorM2Terreno = valorM2Terreno;
        this.area = area;
        this.avaluo = avaluo;
        this.vigencia = vigencia;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public PPredioZona(Long id, PPredio PPredio, String zonaFisica,
        String zonaGeoeconomica, Double valorM2Terreno, Double area,
        Double avaluo, Date vigencia, String cancelaInscribe, Date fechaInscripcionCatastral,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.PPredio = PPredio;
        this.zonaFisica = zonaFisica;
        this.zonaGeoeconomica = zonaGeoeconomica;
        this.valorM2Terreno = valorM2Terreno;
        this.area = area;
        this.avaluo = avaluo;
        this.vigencia = vigencia;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    /**
     * @author fredy.wilches
     * @param ppz
     */
    public PPredioZona(PPredioZona ppz) {
        this.PPredio = ppz.getPPredio();
        this.zonaFisica = ppz.getZonaFisica();
        this.zonaGeoeconomica = ppz.getZonaGeoeconomica();
        this.valorM2Terreno = ppz.getValorM2Terreno();
        this.area = ppz.getArea();
        this.avaluo = ppz.getAvaluo();
        this.vigencia = ppz.getVigencia();
        this.cancelaInscribe = ppz.getCancelaInscribe();
        this.usuarioLog = ppz.getUsuarioLog();
        this.fechaLog = ppz.getFechaLog();
        this.fechaInscripcionCatastral = ppz.getFechaInscripcionCatastral();
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PREDIO_ZONA_ID_SEQ")
    @GenericGenerator(name = "PREDIO_ZONA_ID_SEQ", strategy =
        "co.gov.igac.snc.persistence.util.CustomProyeccionIdGenerator", parameters = {
            @Parameter(name = "sequence", value = "PREDIO_ZONA_ID_SEQ"),
            @Parameter(name = "allocationSize", value = "1")})
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public PPredio getPPredio() {
        return this.PPredio;
    }

    public void setPPredio(PPredio PPredio) {
        this.PPredio = PPredio;
    }

    @Column(name = "ZONA_FISICA", nullable = false, length = 4)
    public String getZonaFisica() {
        return this.zonaFisica;
    }

    public void setZonaFisica(String zonaFisica) {
        this.zonaFisica = zonaFisica;
    }

    @Column(name = "ZONA_GEOECONOMICA", length = 4)
    public String getZonaGeoeconomica() {
        return this.zonaGeoeconomica;
    }

    public void setZonaGeoeconomica(String zonaGeoeconomica) {
        this.zonaGeoeconomica = zonaGeoeconomica;
    }

    @Column(name = "VALOR_M2_TERRENO", nullable = false, precision = 12)
    public Double getValorM2Terreno() {
        return this.valorM2Terreno;
    }

    public void setValorM2Terreno(Double valorM2Terreno) {
        this.valorM2Terreno = valorM2Terreno;
    }

    @Column(name = "AREA", nullable = false, precision = 18, scale = 2)
    public Double getArea() {
        return this.area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    @Column(name = "AVALUO", nullable = false, precision = 18)
    public Double getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Double avaluo) {
        this.avaluo = avaluo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "VIGENCIA", nullable = false, length = 7)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "CANCELA_INSCRIBE", nullable = true, length = 30)
    @Override
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    @Override
    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    @Override
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    @Override
    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Override
    public Date getFechaLog() {
        return this.fechaLog;
    }

    @Override
    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    @Override
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    @Override
    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Transient
    public String getAreaUnidad() {

        if (this.PPredio.isPredioRural()) {
            Integer ha = (int) (this.area / 10000);
            double mts = this.area - ha * 10000;
            mts = Utilidades.redondearNumeroADosCifrasDecimales(mts);
            return (ha > 0 ? ha + " ha " : " ") + (mts > 0 ? mts + " m2 " : "");
        } else {
            this.area = Utilidades.redondearNumeroADosCifrasDecimales(this.area);
            return this.area.toString() + " m2";
        }
    }

    @Override
    public PPredioZona clone() {

        PPredioZona clon;
        try {
            clon = (PPredioZona) super.clone();
            clon.setPPredio(this.PPredio);
            return clon;
        } catch (CloneNotSupportedException ex) {
            return null;
        }

    }
    
}
