package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.snc.persistence.util.EDatoRectificarNaturaleza;
import co.gov.igac.snc.persistence.util.EDatoRectificarNombre;
import co.gov.igac.snc.persistence.util.EDatoRectificarTipo;
import co.gov.igac.snc.util.Constantes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * DatoRectificar entity. @author MyEclipse Persistence Tools
 *
 * @modified by pedro.garcia what: - adición de anotaciones para la secuencia del id - cambio de
 * tipo de dato del atrubuto 'rectificaciones' de Set a List - adición de método equals -
 * implementación de método toString
 *
 * @modified felipe.cadena --Se agragan campos transient para las validaciones de nuevo tramite.
 */
@Entity
@Table(name = "DATO_RECTIFICAR", schema = "SNC_TRAMITE")
public class DatoRectificar implements java.io.Serializable {

    // Fields
    private Long id;
    private String tipo;
    private String nombre;
    private String tabla;
    private String columna;
    private String usuarioLog;
    private Date fechaLog;
    private String naturaleza;
    private List<TramiteRectificacion> tramiteRectificacions = new ArrayList<TramiteRectificacion>();

    //transient
    private boolean rectificar;
    private boolean complementar;
    private boolean noConstruccion;

    // Constructors
    /** default constructor */
    public DatoRectificar() {
    }

    /**
     * @param id
     */
    public DatoRectificar(Long id) {
        this.id = id;
    }

    /** minimal constructor */
    public DatoRectificar(Long id, String nombre, String tabla, String columna,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.tabla = tabla;
        this.columna = columna;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public DatoRectificar(Long id, String tipo, String nombre, String tabla,
        String columna, String naturaleza, String usuarioLog, Date fechaLog,
        List<TramiteRectificacion> rectificacions) {
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.tabla = tabla;
        this.columna = columna;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.tramiteRectificacions = rectificacions;
        this.naturaleza = naturaleza;

    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DatoRectificar_ID_SEQ")
    @SequenceGenerator(name = "DatoRectificar_ID_SEQ", sequenceName = "DATO_RECTIFICAR_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO", length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "TABLA", nullable = false, length = 30)
    public String getTabla() {
        return this.tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }

    @Column(name = "COLUMNA", nullable = false, length = 30)
    public String getColumna() {
        return this.columna;
    }

    public void setColumna(String columna) {
        this.columna = columna;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "datoRectificar")
    public List<TramiteRectificacion> getTramiteRectificacions() {
        return this.tramiteRectificacions;
    }

    public void setTramiteRectificacions(List<TramiteRectificacion> tramiteRectificacions) {
        this.tramiteRectificacions = tramiteRectificacions;
    }

    @Transient
    public boolean isRectificar() {
        return rectificar;
    }

    public void setRectificar(boolean rectificar) {
        this.rectificar = rectificar;
    }

    @Transient
    public boolean isComplementar() {
        return complementar;
    }

    public void setComplementar(boolean complementar) {
        this.complementar = complementar;
    }

    @Transient
    public boolean isNoConstruccion() {
        return noConstruccion;
    }

    public void setNoConstruccion(boolean noConstruccion) {
        this.noConstruccion = noConstruccion;
    }

    @Column(name = "NATURALEZA", nullable = true, length = 30)
    public String getNaturaleza() {
        return naturaleza;
    }

    public void setNaturaleza(String naturaleza) {
        this.naturaleza = naturaleza;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * El único criterio que necesito para saber si son iguales es que su id sea el mismo, lo que
     * implica que en base de datos es la misma entidad
     *
     * OJO: para que haga override del método equals (entre otras cosas para que lo use cuando se
     * usa el 'contains' de una lista), debe recibir como parámetro un Object. El primer criterio de
     * comparaci+ón es que tengan la misma clase con this. Luego, como por estándar, se saca una
     * copia, haciéndole cast con la clase, al objeto que llega como parámetro y con ese se trabaja.
     *
     * @author pedro.garcia
     */
    @Override
    public boolean equals(Object comparable) {
        boolean answer = false;

        if (comparable == null) {
            return false;
        }
        if (getClass() != comparable.getClass()) {
            return false;
        }

        final DatoRectificar other = (DatoRectificar) comparable;

        //N: no sé si sobra porque tal vez si this es null no llega hasta aquí
        if (this == null && other == null) {
            answer = true;
        } else if (this != null) {
            if (this.id == other.id) {
                answer = true;
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna este objeto como un string. No tiene en cuenta los atributos que son listas de otros
     * objetos, ni los campos de log
     *
     * @author pedro.garcia
     */
    @Override
    public String toString() {

        StringBuilder objectAsString;
        String stringSeparator = Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR;

        objectAsString = new StringBuilder();
        objectAsString.append(this.id).append(stringSeparator);
        objectAsString.append(this.tipo).append(stringSeparator);
        objectAsString.append(this.nombre).append(stringSeparator);
        objectAsString.append(this.tabla).append(stringSeparator);
        objectAsString.append(this.columna).append(stringSeparator);
        objectAsString.append(this.naturaleza).append(stringSeparator);

        return objectAsString.toString();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Decide si este DatoRectidicar es del tipo 'Cancelación por doble inscripción'. </br>
     * Se usa porque hay una restricción sobre este tipo de rectificación
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public boolean isDatoRectifCDI() {

        boolean answer = false;

        if (this.getNombre().equals(EDatoRectificarNombre.CANCELACION_DOBLE_INSCRIPCION.getCodigo())) {
            answer = true;
        }

        return answer;

    }

//end of clas
}
