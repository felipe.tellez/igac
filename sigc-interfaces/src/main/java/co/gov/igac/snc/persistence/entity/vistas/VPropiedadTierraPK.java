package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Clase que se usa como primaryKey para la vista VPropiedadTierra para la consulta de estadisticas
 * del visor
 *
 * @author franz.gamba
 */
@Embeddable
public class VPropiedadTierraPK implements Serializable {

    private static final long serialVersionUID = -4543158876144258010L;

    private String municipio;
    private String zonaUnidadOrganica;
    private String tipoPropiedad;

    @Column(name = "MUNICIPIO_CODIGO")
    public String getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    @Column(name = "ZONA_UNIDAD_ORGANICA")
    public String getZonaUnidadOrganica() {
        return this.zonaUnidadOrganica;
    }

    public void setZonaUnidadOrganica(String zonaUnidadOrganica) {
        this.zonaUnidadOrganica = zonaUnidadOrganica;
    }

    @Column(name = "TIPO_PROPIEDAD")
    public String getTipoPropiedad() {
        return this.tipoPropiedad;
    }

    public void setTipoPropiedad(String tipoPropiedad) {
        this.tipoPropiedad = tipoPropiedad;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result +
            ((municipio == null) ? 0 : municipio.hashCode());
        result = prime * result +
            ((tipoPropiedad == null) ? 0 : tipoPropiedad.hashCode());
        result = prime *
            result +
            ((zonaUnidadOrganica == null) ? 0 : zonaUnidadOrganica
                    .hashCode());
        return result;
    }

    public boolean equals(VPropiedadTierraPK obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        VPropiedadTierraPK other = (VPropiedadTierraPK) obj;
        if (municipio == null) {
            if (other.municipio != null) {
                return false;
            }
        } else if (!municipio.equals(other.municipio)) {
            return false;
        }
        if (tipoPropiedad == null) {
            if (other.tipoPropiedad != null) {
                return false;
            }
        } else if (!tipoPropiedad.equals(other.tipoPropiedad)) {
            return false;
        }
        if (zonaUnidadOrganica == null) {
            if (other.zonaUnidadOrganica != null) {
                return false;
            }
        } else if (!zonaUnidadOrganica.equals(other.zonaUnidadOrganica)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object another) {
        return this.equals((VPropiedadTierraPK) another);
    }
}
