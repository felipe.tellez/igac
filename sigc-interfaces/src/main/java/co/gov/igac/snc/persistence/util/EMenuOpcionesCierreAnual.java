/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración para las opciones de menu asociadas al cierre anual
 *
 * @author david.cifuentes
 */
public enum EMenuOpcionesCierreAnual {

    CIERRE_ANUAL("Cierre anual para municipios"),
    REGISTRAR_DECRETO("Registrar decreto de incremento del avaluo anual");

    private String nombre;

    EMenuOpcionesCierreAnual(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}
