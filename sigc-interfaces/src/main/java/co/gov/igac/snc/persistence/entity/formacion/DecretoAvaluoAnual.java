package co.gov.igac.snc.persistence.entity.formacion;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioCierreVigencia;

/**
 * DecretoAvaluoAnual entity. @author MyEclipse Persistence Tools
 */
/*
 * @modified by: david.cifuentes Transient anioVigencia 27/12/2015
 */
@Entity
@Table(name = "DECRETO_AVALUO_ANUAL", schema = "SNC_FORMACION", uniqueConstraints =
    @UniqueConstraint(columnNames = "VIGENCIA"))
public class DecretoAvaluoAnual implements java.io.Serializable {

    private static final long serialVersionUID = -3984780494459583484L;

    // Fields
    private Long id;
    private Date vigencia;
    private String decretoNumero;
    private Date decretoFecha;
    private String observaciones;
    private Long soporteDocumentoId;
    private String usuarioLog;
    private Date fechaLog;

    private List<DecretoCondicion> decretoCondicions;
    private List<MunicipioCierreVigencia> municipioCierreVigencias;

    // Transient
    private Integer anioVigencia;

    // Constructors
    /** default constructor */
    public DecretoAvaluoAnual() {
    }

    /** minimal constructor */
    public DecretoAvaluoAnual(Long id, Date vigencia, Date decretoFecha,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.vigencia = vigencia;
        this.decretoFecha = decretoFecha;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public DecretoAvaluoAnual(Long id, Date vigencia, String decretoNumero,
        Date decretoFecha, String observaciones, Long soporteDocumentoId,
        String usuarioLog, Date fechaLog) {
        this.id = id;
        this.vigencia = vigencia;
        this.decretoNumero = decretoNumero;
        this.decretoFecha = decretoFecha;
        this.observaciones = observaciones;
        this.soporteDocumentoId = soporteDocumentoId;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DECRETO_AVALUO_ANUAL_Id_SEQ")
    @SequenceGenerator(name = "DECRETO_AVALUO_ANUAL_Id_SEQ", sequenceName =
        "DECRETO_AVALUO_ANUAL_Id_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "VIGENCIA", unique = true, nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Date vigencia) {
        this.vigencia = vigencia;
    }

    @Column(name = "DECRETO_NUMERO", length = 250)
    public String getDecretoNumero() {
        return this.decretoNumero;
    }

    public void setDecretoNumero(String decretoNumero) {
        this.decretoNumero = decretoNumero;
    }

    @Column(name = "DECRETO_FECHA", nullable = false, length = 7)
    public Date getDecretoFecha() {
        return this.decretoFecha;
    }

    public void setDecretoFecha(Date decretoFecha) {
        this.decretoFecha = decretoFecha;
    }

    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "SOPORTE_DOCUMENTO_ID", precision = 10, scale = 0)
    public Long getSoporteDocumentoId() {
        return this.soporteDocumentoId;
    }

    public void setSoporteDocumentoId(Long soporteDocumentoId) {
        this.soporteDocumentoId = soporteDocumentoId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    // bi-directional many-to-one association to DecretoCondicion
    @OneToMany(mappedBy = "decretoAvaluoAnual", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<DecretoCondicion> getDecretoCondicions() {
        return this.decretoCondicions;
    }

    public void setDecretoCondicions(List<DecretoCondicion> decretoCondicions) {
        this.decretoCondicions = decretoCondicions;
    }

    // bi-directional many-to-one association to DecretoCondicion
    @OneToMany(mappedBy = "decretoAvaluoAnual", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<MunicipioCierreVigencia> getMunicipioCierreVigencias() {
        return municipioCierreVigencias;
    }

    public void setMunicipioCierreVigencias(
        List<MunicipioCierreVigencia> municipioCierreVigencias) {
        this.municipioCierreVigencias = municipioCierreVigencias;
    }

    @Transient
    public Integer getAnioVigencia() {
        if (this.vigencia != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(this.vigencia);
            int year = calendar.get(Calendar.YEAR);
            this.anioVigencia = new Integer(year);
        }
        return this.anioVigencia;
    }

    public void setAnioVigencia(Integer anioVigencia) {
        this.anioVigencia = anioVigencia;
    }

}
