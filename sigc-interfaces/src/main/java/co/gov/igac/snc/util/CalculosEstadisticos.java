package co.gov.igac.snc.util;

/**
 * Objeto utilizado para almacenar la información de los calculos estadisticos como media, varianza
 * y demas de un conjunto de datos
 *
 * @author javier.aponte
 *
 */
public class CalculosEstadisticos {

    private double media;

    private double varianza;

    private double coeficienteAsimetria;

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }

    public double getVarianza() {
        return varianza;
    }

    public void setVarianza(double varianza) {
        this.varianza = varianza;
    }

    public double getCoeficienteAsimetria() {
        return coeficienteAsimetria;
    }

    public void setCoeficienteAsimetria(double coeficienteAsimetria) {
        this.coeficienteAsimetria = coeficienteAsimetria;
    }

    public double getDesviacionEstandar() {
        return Math.sqrt(this.varianza);
    }

    public double getCoeficienteVariacion() {
        return this.getDesviacionEstandar() / this.media;
    }

    public double getLimiteSuperior() {
        return this.media + this.varianza;
    }

    public double getLimiteInferior() {
        return this.media - this.varianza;
    }

}
