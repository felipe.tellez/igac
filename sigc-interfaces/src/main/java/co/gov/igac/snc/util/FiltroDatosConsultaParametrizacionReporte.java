package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * @description Filtro de datos para la consulta de parametrizaciones de reportes a usuarios
 *
 * @version 1.0
 * @author david.cifuentes
 */
public class FiltroDatosConsultaParametrizacionReporte implements Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -3015906031641774832L;

    private String categoria;
    private Long tipoReporteId;
    private String territorialCodigo;
    private String UOCCodigo;
    private String departamentoCodigo;
    private String municipioCodigo;
    private String rol;
    private String funcionario;
    private String estado;
    private String entidadCreacion;
    private String generar;
    private String consultar;

    /**
     * Default constructor
     */
    public FiltroDatosConsultaParametrizacionReporte() {
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Long getTipoReporteId() {
        return tipoReporteId;
    }

    public void setTipoReporteId(Long tipoReporteId) {
        this.tipoReporteId = tipoReporteId;
    }

    public String getUOCCodigo() {
        return UOCCodigo;
    }

    public void setUOCCodigo(String uOCCodigo) {
        UOCCodigo = uOCCodigo;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getTerritorialCodigo() {
        return territorialCodigo;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.territorialCodigo = territorialCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEntidadCreacion() {
        return entidadCreacion;
    }

    public void setEntidadCreacion(String entidadCreacion) {
        this.entidadCreacion = entidadCreacion;
    }

    public String getGenerar() {
        return generar;
    }

    public void setGenerar(String generar) {
        this.generar = generar;
    }

    public String getConsultar() {
        return consultar;
    }

    public void setConsultar(String consultar) {
        this.consultar = consultar;
    }

}
