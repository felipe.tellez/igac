package co.gov.igac.snc.dao.util;

public enum ENumeraciones {

    // Numeración para las actas de comités de avlaúos. No tiene en cuenta ningún parámetro (territorial,
    // depto, etc)
    NUMERACION_ACTA_COMITE_AVALUOS(14),
    //D: Numeración por Tipo Doc, Año, Departamento, Municipio (Certificado plano predial catastral)
    NUMERACION_CERTIFICADO_PLANO_PREDIAL_CATASTRAL(12),
    //D: numeración por Territorial, Departamento, Municipio  y Año (Ofertas inmobiliarias)
    NUMERACION_OFERTAS_INMOBILIARIAS(11),
    //D: numeración para las ordenes de práctica. No tiene en cuenta ningún parámetro (territorial,
    //   depto, etc)
    NUMERACION_ORDENES_PRACTICA(13),
    NUMERACION_RADICACION_CATASTRAL(4),
    NUMERACION_RADICACION_DE_REVISION_DE_AVALUO(5),
    //D: Numeración por Territorial, Departamento, Municipio  y Año (Resolucion)
    NUMERACION_RESOLUCION(10),
    //D: numeración por territorial y año
    NUMERACION_TYA(2),
    //D: numeración por territorial y año
    NUMERACION_T_A_CONS(16),
    //D: numeración por año
    NUMERACION_ANIO_CONS(17),
    //D: numeración por año reportes estadisticos
    NUMERACION_ANIO_CONS_REP_EST(18);

    private Integer id;

    ENumeraciones(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
