package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion relacionada al dominio TRAMITE_TAREA_RESULTADO
 *
 * @author felipe.cadena
 */
public enum ETramiteTareaResultado {

    INICIO,
    ERROR,
    FIN,
    RECUPERACION;

}
