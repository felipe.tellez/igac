/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * @author felipe.cadena
 */
public enum EFechaCorteReporte {

    ACTUAL("Fecha actual", "1"),
    DICIEMBRE_31("31 de diciembre", "2"),
    ENERO_1("1 de enero", "3");

    
    private final String nombre;
    private final String codigo;
	
	EFechaCorteReporte(String nombre, String codigo){
		this.codigo=codigo;
		this.nombre=nombre;
	}

	public String getCodigo() {
		return codigo;
	}

    public String getNombre() {
        return nombre;
    }
    
    
}
