package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the CONTROL_CALIDAD_OFERTA database table.
 *
 * @author rodrigo.hernandez
 *
 * @modified rodrigo.hernandez - cambio de serialVersionUID con valor autogenerado
 */
@Entity
@Table(name = "CONTROL_CALIDAD_OFERTA")
public class ControlCalidadOferta implements Serializable {

    private static final long serialVersionUID = 92610661272899422L;
    private Long id;
    private String aprobado;
    private Date fechaLog;
    private Double numeroOfertaRecolector;
    private OfertaInmobiliaria ofertaInmobiliariaId;
    private String ofertaSeleccionada;
    private String recolectorId;
    private String usuarioLog;
    private Double valorVariable;
    private ControlCalidad controlCalidad;
    private List<ControlCalidadOfertaOb> controlCalidadOfertaObs;

    public ControlCalidadOferta() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTROL_CALIDAD_OFERTA_ID_SEQ")
    @SequenceGenerator(name = "CONTROL_CALIDAD_OFERTA_ID_SEQ", sequenceName =
        "CONTROL_CALIDAD_OFERTA_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "APROBADO")
    public String getAprobado() {
        return this.aprobado;
    }

    public void setAprobado(String aprobado) {
        this.aprobado = aprobado;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NUMERO_OFERTA_RECOLECTOR")
    public Double getNumeroOfertaRecolector() {
        return this.numeroOfertaRecolector;
    }

    public void setNumeroOfertaRecolector(Double numeroOfertaRecolector) {
        this.numeroOfertaRecolector = numeroOfertaRecolector;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OFERTA_INMOBILIARIA_ID", nullable = false)
    public OfertaInmobiliaria getOfertaInmobiliariaId() {
        return this.ofertaInmobiliariaId;
    }

    public void setOfertaInmobiliariaId(OfertaInmobiliaria ofertaInmobiliariaId) {
        this.ofertaInmobiliariaId = ofertaInmobiliariaId;
    }

    @Column(name = "OFERTA_SELECCIONADA")
    public String getOfertaSeleccionada() {
        return this.ofertaSeleccionada;
    }

    public void setOfertaSeleccionada(String ofertaSeleccionada) {
        this.ofertaSeleccionada = ofertaSeleccionada;
    }

    @Column(name = "RECOLECTOR_ID")
    public String getRecolectorId() {
        return this.recolectorId;
    }

    public void setRecolectorId(String recolectorId) {
        this.recolectorId = recolectorId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_VARIABLE")
    public Double getValorVariable() {
        return this.valorVariable;
    }

    public void setValorVariable(Double valorVariable) {
        this.valorVariable = valorVariable;
    }

    // bi-directional many-to-one association to ControlCalidad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTROL_CALIDAD_ID")
    public ControlCalidad getControlCalidad() {
        return this.controlCalidad;
    }

    public void setControlCalidad(ControlCalidad controlCalidad) {
        this.controlCalidad = controlCalidad;
    }

    // bi-directional many-to-one association to ControlCalidadOfertaOb
    @OneToMany(mappedBy = "controlCalidadOferta")
    public List<ControlCalidadOfertaOb> getControlCalidadOfertaObs() {
        return this.controlCalidadOfertaObs;
    }

    public void setControlCalidadOfertaObs(
        List<ControlCalidadOfertaOb> controlCalidadOfertaObs) {
        this.controlCalidadOfertaObs = controlCalidadOfertaObs;
    }
}
