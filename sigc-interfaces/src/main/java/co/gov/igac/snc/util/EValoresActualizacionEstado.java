/*
 *Proyecto SNC
 */
package co.gov.igac.snc.util;

/**
 * Posibles estados para los registros de
 *
 * @author felipe.cadena
 */
public enum EValoresActualizacionEstado {

    CARGADO("CARGADO"),
    ELIMINADO("ELIMINADO"),
    FINALIZADO("FINALIZADO");

    private String codigo;

    EValoresActualizacionEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
