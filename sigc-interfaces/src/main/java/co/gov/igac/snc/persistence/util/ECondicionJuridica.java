/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los codigos de condición juridica para ofertas
 *
 * @author juan.agudelo
 */
public enum ECondicionJuridica {

    NO_PROPIEDAD_HORIZONTAL("NPH"), PROPIEDAD_HORIZONTAL("PH");

    private String codigo;

    private ECondicionJuridica(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
