package co.gov.igac.snc.persistence.entity.tramite;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * ModeloResolucion entity. @author MyEclipse Persistence Tools
 *
 * @modified by: javier.aponte 25-07-2012 se implementa la interfaz Cloneable y se agrega método
 * clone
 */
@Entity
@Table(name = "MODELO_RESOLUCION", schema = "SNC_TRAMITE")
public class ModeloResolucion implements java.io.Serializable, Cloneable {

    // Fields
    private Long id;
    private String tipoTramite;
    private String claseMutacion;
    private String subtipo;
    private String delegada;
    private String nombre;
    private String textoTitulo;
    private String textoConsiderando;
    private String textoResuelve;
    private Timestamp fechaDesde;
    private Timestamp fechaHasta;
    private String usuarioLog;
    private Timestamp fechaLog;
    private Set<TramiteTextoResolucion> tramiteTextoResolucions =
        new HashSet<TramiteTextoResolucion>(
            0);

    // Constructors
    /** default constructor */
    public ModeloResolucion() {
    }

    /** minimal constructor */
    public ModeloResolucion(Long id, String tipoTramite, String nombre,
        String textoTitulo, String textoConsiderando, Timestamp fechaDesde,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.tipoTramite = tipoTramite;
        this.nombre = nombre;
        this.textoTitulo = textoTitulo;
        this.textoConsiderando = textoConsiderando;
        this.fechaDesde = fechaDesde;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public ModeloResolucion(Long id, String tipoTramite, String claseMutacion,
        String subtipo, String nombre, String textoTitulo,
        String textoConsiderando, String textoResuelve,
        Timestamp fechaDesde, Timestamp fechaHasta, String usuarioLog,
        Timestamp fechaLog,
        Set<TramiteTextoResolucion> tramiteTextoResolucions) {
        this.id = id;
        this.tipoTramite = tipoTramite;
        this.claseMutacion = claseMutacion;
        this.subtipo = subtipo;
        this.nombre = nombre;
        this.textoTitulo = textoTitulo;
        this.textoConsiderando = textoConsiderando;
        this.textoResuelve = textoResuelve;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.tramiteTextoResolucions = tramiteTextoResolucions;
    }
    public ModeloResolucion(Long id, String tipoTramite, String claseMutacion,
        String subtipo,String delegada, String nombre, String textoTitulo,
        String textoConsiderando, String textoResuelve,
        Timestamp fechaDesde, Timestamp fechaHasta, String usuarioLog,
        Timestamp fechaLog,
        Set<TramiteTextoResolucion> tramiteTextoResolucions) {
        this.id = id;
        this.tipoTramite = tipoTramite;
        this.claseMutacion = claseMutacion;
        this.subtipo = subtipo;
        this.delegada = delegada;
        this.nombre = nombre;
        this.textoTitulo = textoTitulo;
        this.textoConsiderando = textoConsiderando;
        this.textoResuelve = textoResuelve;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.tramiteTextoResolucions = tramiteTextoResolucions;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MODELO_RESOLUCION_Id_SEQ")
    @SequenceGenerator(name = "MODELO_RESOLUCION_Id_SEQ", sequenceName = "MODELO_RESOLUCION_Id_SEQ",
        allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_TRAMITE", nullable = false, length = 30)
    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @Column(name = "CLASE_MUTACION", length = 30)
    public String getClaseMutacion() {
        return this.claseMutacion;
    }

    public void setClaseMutacion(String claseMutacion) {
        this.claseMutacion = claseMutacion;
    }

    @Column(name = "SUBTIPO", length = 30)
    public String getSubtipo() {
        return this.subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    @Column(name = "DELEGADA", length = 30)
    public String getDelegada() {
        return delegada;
    }

    public void setDelegada(String delegada) {
        this.delegada = delegada;
    }
    
    @Column(name = "NOMBRE", nullable = false, length = 600)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "TEXTO_TITULO", nullable = false, length = 600)
    public String getTextoTitulo() {
        return this.textoTitulo;
    }

    public void setTextoTitulo(String textoTitulo) {
        this.textoTitulo = textoTitulo;
    }

    @Column(name = "TEXTO_CONSIDERANDO", nullable = false)
    public String getTextoConsiderando() {
        return this.textoConsiderando;
    }

    public void setTextoConsiderando(String textoConsiderando) {
        this.textoConsiderando = textoConsiderando;
    }

    @Column(name = "TEXTO_RESUELVE")
    public String getTextoResuelve() {
        return this.textoResuelve;
    }

    public void setTextoResuelve(String textoResuelve) {
        this.textoResuelve = textoResuelve;
    }

    @Column(name = "FECHA_DESDE", nullable = false, length = 7)
    public Timestamp getFechaDesde() {
        return this.fechaDesde;
    }

    public void setFechaDesde(Timestamp fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    @Column(name = "FECHA_HASTA", length = 7)
    public Timestamp getFechaHasta() {
        return this.fechaHasta;
    }

    public void setFechaHasta(Timestamp fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "modeloResolucion")
    public Set<TramiteTextoResolucion> getTramiteTextoResolucions() {
        return this.tramiteTextoResolucions;
    }

    public void setTramiteTextoResolucions(
        Set<TramiteTextoResolucion> tramiteTextoResolucions) {
        this.tramiteTextoResolucions = tramiteTextoResolucions;
    }

    /**
     * @author javier.aponte Método para poder hacer un clon de modelo resolución
     */
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }
}
