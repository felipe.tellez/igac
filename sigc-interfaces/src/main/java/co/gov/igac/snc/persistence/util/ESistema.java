package co.gov.igac.snc.persistence.util;

public enum ESistema {
    SNC, CORDIS, LDAP;
}
