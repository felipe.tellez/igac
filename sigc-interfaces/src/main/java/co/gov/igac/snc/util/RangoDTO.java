package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * Define rango de datos
 *
 * @author felipe.cadena
 *
 */
public class RangoDTO implements Serializable {


    private Double valorInicial;
    private Double valorFinal;

    //limites
    private Double valorMinimo;
    private Double valorMaximo;


    /**
     * Void Constructor
     */
    public RangoDTO() {
    }

    public Double getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(Double valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public Double getValorMaximo() {
        return valorMaximo;
    }

    public void setValorMaximo(Double valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    public Double getValorInicial() {
        return valorInicial;
    }

    public void setValorInicial(Double valorInicial) {
        this.valorInicial = valorInicial;
    }

    public Double getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(Double valorFinal) {
        this.valorFinal = valorFinal;
    }

    public void limpiarValores(){
        this.setValorInicial(null);
        this.setValorFinal(null);
    }
}
