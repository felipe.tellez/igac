package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los valores del dominio PREDIO_INCONSISTENCIA_TIPO. <br/>
 *
 * Solo se definen los que se usan directamente en la aplicación; es decir, lo que se manejan en los
 * métodos que validan la geometría de Predios
 *
 * @author pedro.garcia
 */
public enum EPredioInconsistenciaTipo {

    //D: nombre (código, valor)
    MANZANA_NO_EXISTE("4", "Manzana no existente en el plano de  conjunto"),
    NO_EXISTE_FEATURE("300", "No existe el feature en la bd"),
    NUMERO_PREDIAL_REPETIDO("1", "Número de predio repetido"),
    NUMERO_PREDIAL_SIN_PLIGONO("39", "Numero predial sin polígono asociado"),
    PREDIO_SIN_ZONAS("204", "Predio sin zonas"),
    //valor que se registra en la tabla inconsistencias cuando no existe ninguna asociada al tramite
    NO_INCONSISTENCIA("0", "No se encuentran inconsistencias geográficas");

    private String codigo;

    private String valor;

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private EPredioInconsistenciaTipo(String cod, String val) {
        this.codigo = cod;
        this.valor = val;
    }

}
