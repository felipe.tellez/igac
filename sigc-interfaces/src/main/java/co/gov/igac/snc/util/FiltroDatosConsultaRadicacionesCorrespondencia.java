package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;

/**
 * Filtro para los campos de la consulta de radicaciones de correspondencia
 *
 * @author rodrigo.hernandez
 *
 */
public class FiltroDatosConsultaRadicacionesCorrespondencia implements Serializable {

    private static final long serialVersionUID = 5897204630729534716L;

    private Date fechaDesde;
    private Date fechaHasta;
    private String solicitante;
    private String codigoTipoTramite;
    private String numeroRadicacion;
    private String codigoTipoEmpresa;
    private String codigoTerritorial;
    private String codigoDepartamentoOrigen;
    private String codigoMunicipioOrigen;

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public String getCodigoTipoTramite() {
        return codigoTipoTramite;
    }

    public void setCodigoTipoTramite(String codigoTipoTramite) {
        this.codigoTipoTramite = codigoTipoTramite;
    }

    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public String getCodigoTipoEmpresa() {
        return codigoTipoEmpresa;
    }

    public void setCodigoTipoEmpresa(String codigoTipoEmpresa) {
        this.codigoTipoEmpresa = codigoTipoEmpresa;
    }

    public String getCodigoTerritorial() {
        return codigoTerritorial;
    }

    public void setCodigoTerritorial(String codigoTerritorial) {
        this.codigoTerritorial = codigoTerritorial;
    }

    public String getCodigoDepartamentoOrigen() {
        return codigoDepartamentoOrigen;
    }

    public void setCodigoDepartamentoOrigen(String codigoDepartamentoOrigen) {
        this.codigoDepartamentoOrigen = codigoDepartamentoOrigen;
    }

    public String getCodigoMunicipioOrigen() {
        return codigoMunicipioOrigen;
    }

    public void setCodigoMunicipioOrigen(String codigoMunicipioOrigen) {
        this.codigoMunicipioOrigen = codigoMunicipioOrigen;
    }

}
