package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio ERROR_PROC_MASIVO_USUARIO
 *
 * @author juan.agudelo
 */
public enum EErrorProcesoMasivoUsuario {

    //D: valor ("codigo")
    PERSONA_NO_ENCONTRADA("1"),
    PREDIO_CANCELADO("3"),
    PREDIO_NO_ENCONTRADO("2");

    private String codigo;

    EErrorProcesoMasivoUsuario(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
