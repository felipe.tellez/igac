package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the NOMENCLATURA_VIAL_ENTIDAD database table.
 */
/**
 * Modificaciones a la clase NomenclaturaVialEntidad:
 */
@Entity
@Table(name = "NOMENCLATURA_VIAL_ENTIDAD", schema = "SNC_ACTUALIZACION")
public class NomenclaturaVialEntidad implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1350761618663476628L;
    private Long id;
    private EntidadMunicipal entidadMunicipal;
    private Date fechaLog;
    private String usuarioLog;
    private ActualizacionNomenclaturaVia actualizacionNomenclaturaVia;

    public NomenclaturaVialEntidad() {
    }

    @Id
    @SequenceGenerator(name = "NOMENCLATURA_VIAL_ENTID_ID_GENERATOR", sequenceName =
        "NOMENCLATURA_VIAL_ENTID_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "NOMENCLATURA_VIAL_ENTID_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    //bi-directional many-to-one association to EntidadMunicipal
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ENTIDAD_MUNICIPAL_ID", nullable = false)
    public EntidadMunicipal getEntidadMunicipal() {
        return this.entidadMunicipal;
    }

    public void setEntidadMunicipal(EntidadMunicipal entidadMunicipal) {
        this.entidadMunicipal = entidadMunicipal;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    //bi-directional many-to-one association to ActualizacionNomenclaturaVia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_NOME_VIAL_ID", nullable = false)
    public ActualizacionNomenclaturaVia getActualizacionNomenclaturaVia() {
        return this.actualizacionNomenclaturaVia;
    }

    public void setActualizacionNomenclaturaVia(
        ActualizacionNomenclaturaVia actualizacionNomenclaturaVia) {
        this.actualizacionNomenclaturaVia = actualizacionNomenclaturaVia;
    }

}
