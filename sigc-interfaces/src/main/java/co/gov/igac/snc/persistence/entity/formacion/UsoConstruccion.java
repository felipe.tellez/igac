package co.gov.igac.snc.persistence.entity.formacion;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.util.Constantes;
import java.io.Serializable;

/**
 * UsoConstruccion entity. @author MyEclipse Persistence Tools
 *
 * @modified javier.aponte Se agrega el método transient getIdConFormato :: 20-12-2012
 */
@Entity
@Table(name = "USO_CONSTRUCCION", schema = "SNC_FORMACION")
public class UsoConstruccion implements Serializable {

    // Fields
    private Long id;
    private String nombre;
    private String tipoUso;
    private String destinoEconomico;
    private String usuarioLog;
    private Date fechaLog;
    private Set<UnidadConstruccion> unidadConstruccions = new HashSet<UnidadConstruccion>(
        0);

    // Constructors
    /** default constructor */
    public UsoConstruccion() {
    }

    /** minimal constructor */
    public UsoConstruccion(Long id, String nombre, String tipoUso,
        String destinoEconomico, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.tipoUso = tipoUso;
        this.destinoEconomico = destinoEconomico;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public UsoConstruccion(Long id, String nombre, String tipoUso,
        String destinoEconomico, String usuarioLog, Date fechaLog,
        Set<UnidadConstruccion> unidadConstruccions) {
        this.id = id;
        this.nombre = nombre;
        this.tipoUso = tipoUso;
        this.destinoEconomico = destinoEconomico;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.unidadConstruccions = unidadConstruccions;
    }

    // Property accessors
    @Id
    @GeneratedValue
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "TIPO_USO", nullable = false, length = 30)
    public String getTipoUso() {
        return this.tipoUso;
    }

    public void setTipoUso(String tipoUso) {
        this.tipoUso = tipoUso;
    }

    @Column(name = "DESTINO_ECONOMICO", nullable = false, length = 30)
    public String getDestinoEconomico() {
        return this.destinoEconomico;
    }

    public void setDestinoEconomico(String destinoEconomico) {
        this.destinoEconomico = destinoEconomico;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usoConstruccion")
    public Set<UnidadConstruccion> getUnidadConstruccions() {
        return this.unidadConstruccions;
    }

    public void setUnidadConstruccions(
        Set<UnidadConstruccion> unidadConstruccions) {
        this.unidadConstruccions = unidadConstruccions;
    }

    /**
     * Método que devuelve el id del uso de construcción concatenado con un 0 si el id es menor que
     * 10, en caso contrario devuelve el id sin hacerle ningún cambio
     *
     * @author javier.aponte
     */
    @Transient
    public String getIdConFormato() {
        String answer;
        if (this.id < 10) {
            answer = "0" + this.id;
        } else {
            answer = String.valueOf(this.id);
        }
        return answer;
    }

}
