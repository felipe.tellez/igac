package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;

/**
 * Almacena la informacion de los predios, de cada una de las unidades de costruccion asociadas y de
 * las zonas a las que pertenece el predio requeridas para mostrar en el visor web
 *
 * @author franz.gamba
 */
public class DetalleVisorVO implements Serializable {

    private static final long serialVersionUID = -4454228321375191080L;
    private Double areaConstruccion;
    private Double areaTerreno;
    private String destinoValor;
    private String departamento;
    private String municipio;
    private String matriculaInmobiliaria;
    private int numeroCostruccionesPredio;
    private List<UnidadConstruccionVO> unidadConstruccions;
    private List<String> direccion;
    private String numeroPredial;
    private String numeroPredialAterior;
    private List<ZonaVO> predioZonas;

    public DetalleVisorVO() {

    }

    public DetalleVisorVO(Predio p) {
        this.areaConstruccion = p.getAreaConstruccion();
        this.areaTerreno = p.getAreaTerreno();
        this.destinoValor = p.getDestino();
        this.departamento = p.getDepartamento().getNombre();
        this.municipio = p.getMunicipio().getNombre();
        this.numeroCostruccionesPredio = p.getUnidadConstruccions().size();
        this.matriculaInmobiliaria = p.getNumeroRegistro();
        if (!p.getUnidadConstruccions().isEmpty()) {
            this.unidadConstruccions = new ArrayList<UnidadConstruccionVO>();
            for (UnidadConstruccion uc : p.getUnidadConstruccions()) {
                UnidadConstruccionVO ucvo = new UnidadConstruccionVO(
                    uc.getAreaConstruida(),
                    uc.getTotalBanios(),
                    uc.getTotalHabitaciones(),
                    uc.getTotalLocales(),
                    uc.getTotalPisosConstruccion(),
                    uc.getTotalPuntaje(),
                    uc.getUsoConstruccion().getNombre());
                this.unidadConstruccions.add(ucvo);
            }

        }
        if (!p.getPredioDireccions().isEmpty()) {
            this.direccion = new ArrayList<String>();
            for (PredioDireccion dir : p.getPredioDireccions()) {
                this.direccion.add(dir.getDireccion());
            }
        }
        this.numeroPredial = p.getNumeroPredial();
        this.numeroPredialAterior = p.getNumeroPredialAnterior();
        if (!p.getPredioZonas().isEmpty()) {
            this.predioZonas = new ArrayList<ZonaVO>();
            for (PredioZona pz : p.getPredioZonas()) {
                ZonaVO zona = new ZonaVO(
                    pz.getZonaGeoeconomica(),
                    pz.getZonaFisica());
                this.predioZonas.add(zona);
            }

        }

    }

    public Double getAreaConstruccion() {
        return areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    public Double getAreaTerreno() {
        return areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    public String getDestinoValor() {
        return destinoValor;
    }

    public void setDestinoValor(String destinoValor) {
        this.destinoValor = destinoValor;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public int getNumeroCostruccionesPredio() {
        return numeroCostruccionesPredio;
    }

    public void setNumeroCostruccionesPredio(int numeroCostruccionesPredio) {
        this.numeroCostruccionesPredio = numeroCostruccionesPredio;
    }

    public List<String> getDireccion() {
        return direccion;
    }

    public void setDireccion(List<String> direccion) {
        this.direccion = direccion;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getNumeroPredialAterior() {
        return numeroPredialAterior;
    }

    public void setNumeroPredialAterior(String numeroPredialAterior) {
        this.numeroPredialAterior = numeroPredialAterior;
    }

    public List<UnidadConstruccionVO> getUnidadConstruccions() {
        return unidadConstruccions;
    }

    public void setUnidadConstruccions(
        List<UnidadConstruccionVO> unidadConstruccions) {
        this.unidadConstruccions = unidadConstruccions;
    }

    public List<ZonaVO> getPredioZonas() {
        return predioZonas;
    }

    public void setPredioZonas(List<ZonaVO> predioZonas) {
        this.predioZonas = predioZonas;
    }

    public String getMatriculaInmobiliaria() {
        return matriculaInmobiliaria;
    }

    public void setMatriculaInmobiliaria(String matriculaInmobiliaria) {
        this.matriculaInmobiliaria = matriculaInmobiliaria;
    }

}
