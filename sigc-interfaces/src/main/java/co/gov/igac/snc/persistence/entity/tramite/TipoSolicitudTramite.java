package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * TipoSolicitudTramite entity. @author MyEclipse Persistence Tools
 *
 * @modified by: pedro.garcia. what: - cambio de tipo de datos TimeStamp a Date - adición de
 * anotaciones para la secuencia del id - adición de anotación @Temporal en los getters de campos
 * tipo Date - adición de namedQueries - adición de atributo transient para guardar el valor del
 * tipo de trámite
 *
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "findByTipoSolicitud",
        query = "SELECT tst FROM TipoSolicitudTramite tst WHERE tst.tipoSolicitud = :tipoSolP"
    )
})
@Table(name = "TIPO_SOLICITUD_TRAMITE", schema = "SNC_TRAMITE", uniqueConstraints =
    @UniqueConstraint(columnNames = {
    "TIPO_SOLICITUD", "TIPO_TRAMITE"}))
public class TipoSolicitudTramite implements java.io.Serializable {

    // Fields
    private Long id;
    private String tipoSolicitud;
    private String tipoTramite;
    private String usuarioLog;
    private Date fechaLog;

    private String tipoTramiteValor;
    // Constructors

    /** default constructor */
    public TipoSolicitudTramite() {
    }

    /** full constructor */
    public TipoSolicitudTramite(Long id, String tipoSolicitud,
        String tipoTramite, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.tipoSolicitud = tipoSolicitud;
        this.tipoTramite = tipoTramite;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipoSolicitudTramite_ID_SEQ")
    @SequenceGenerator(name = "TipoSolicitudTramite_ID_SEQ", sequenceName =
        "TIPO_SOLICITUD_TRAMITE_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_SOLICITUD", nullable = false, length = 30)
    public String getTipoSolicitud() {
        return this.tipoSolicitud;
    }

    public void setTipoSolicitud(String tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    @Column(name = "TIPO_TRAMITE", nullable = false, length = 30)
    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Transient
    public String getTipoTramiteValor() {
        return this.tipoTramiteValor;
    }

    public void setTipoTramiteValor(String tipoTramiteValor) {
        this.tipoTramiteValor = tipoTramiteValor;
    }

}
