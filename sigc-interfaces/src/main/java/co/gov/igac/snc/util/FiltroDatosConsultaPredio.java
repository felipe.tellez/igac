/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Filtro para los campos de la consulta de predios
 *
 * @author pedro.garcia
 * @modify david.cifuentes -> adición del método validateSegmentsNumeroPredial
 */
public class FiltroDatosConsultaPredio implements Serializable {

    private static final long serialVersionUID = -5172998811646233385L;

    private String territorialId;
    private String departamentoId;
    private String municipioId;
    private String uoc;

    /**
     * segmentos del número predial
     */
    private String numeroPredialS1;
    private String numeroPredialS2;
    private String numeroPredialS3;
    private String numeroPredialS4;
    private String numeroPredialS5;
    private String numeroPredialS6;
    private String numeroPredialS7;
    private String numeroPredialS8;
    private String numeroPredialS9;
    private String numeroPredialS10;
    private String numeroPredialS11;
    private String numeroPredialS12;

    private String direccionPredio;
    private String numeroPredialAnterior;
    private String numeroPredial;
    private String nombrePropietario;
    private String identificacionPropietario;

    /**
     * datos de la búsqueda avanzada
     */
    private String numeroRegistro;

    private BigDecimal consecutivoCatastral;

    private String usoConstruccionId;

    private String nip;

    private String destinoPredioId;
    private double areaTerrenoMin;
    private double areaTerrenoMax;
    private double areaConstruidaMin;
    private double areaConstruidaMax;
    private String zona;

    /**
     * adicionados por cambios en el procedimiento de búsqueda
     */
    private String primerNombrePropietario;
    private String segundoNombrePropietario;
    private String primerApellidoPropietario;
    private String segundoApellidoPropietario;
    private String razonSocialPropietario;
    private String tipoPersonaPropietario;

    private double avaluoDesde;
    private double avaluoHasta;
    private Long ano;

    //--------    methods     ------------------------------------------------------------
    public double getAvaluoDesde() {
        return this.avaluoDesde;
    }

    public Long getAno() {
        return ano;
    }

    public void setAno(Long ano) {
        this.ano = ano;
    }

    public void setAvaluoDesde(double avaluoDesde) {
        this.avaluoDesde = avaluoDesde;
    }

    public double getAvaluoHasta() {
        return this.avaluoHasta;
    }

    public void setAvaluoHasta(double avaluoHasta) {
        this.avaluoHasta = avaluoHasta;
    }

    public String getTipoPersonaPropietario() {
        return this.tipoPersonaPropietario;
    }

    public void setTipoPersonaPropietario(String tipoPersonaPropietario) {
        this.tipoPersonaPropietario = tipoPersonaPropietario;
    }

    public String getPrimerNombrePropietario() {
        return this.primerNombrePropietario;
    }

    public void setPrimerNombrePropietario(String primerNombrePropietario) {
        this.primerNombrePropietario = primerNombrePropietario;
    }

    public String getSegundoNombrePropietario() {
        return this.segundoNombrePropietario;
    }

    public void setSegundoNombrePropietario(String segundoNombrePropietario) {
        this.segundoNombrePropietario = segundoNombrePropietario;
    }

    public String getPrimerApellidoPropietario() {
        return this.primerApellidoPropietario;
    }

    public void setPrimerApellidoPropietario(String primerApellidoPropietario) {
        this.primerApellidoPropietario = primerApellidoPropietario;
    }

    public String getSegundoApellidoPropietario() {
        return this.segundoApellidoPropietario;
    }

    public void setSegundoApellidoPropietario(String segundoApellidoPropietario) {
        this.segundoApellidoPropietario = segundoApellidoPropietario;
    }

    public String getRazonSocialPropietario() {
        return this.razonSocialPropietario;
    }

    public void setRazonSocialPropietario(String razonSocialPropietario) {
        this.razonSocialPropietario = razonSocialPropietario;
    }

    public String getTerritorialId() {
        return this.territorialId;
    }

    public void setTerritorialId(String territorialId) {
        this.territorialId = territorialId;
    }

    public String getDepartamentoId() {
        return this.departamentoId;
    }

    public void setDepartamentoId(String departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getMunicipioId() {
        return this.municipioId;
    }

    public void setMunicipioId(String municipioId) {
        this.municipioId = municipioId;
    }

    public String getNumeroPredialS1() {
        return this.numeroPredialS1;
    }

    public void setNumeroPredialS1(String numeroPredialS1) {
        this.numeroPredialS1 = numeroPredialS1;
    }

    public String getNumeroPredialS2() {
        return this.numeroPredialS2;
    }

    public void setNumeroPredialS2(String numeroPredialS2) {
        this.numeroPredialS2 = numeroPredialS2;
    }

    public String getNumeroPredialS3() {
        return this.numeroPredialS3;
    }

    public void setNumeroPredialS3(String numeroPredialS3) {
        this.numeroPredialS3 = numeroPredialS3;
    }

    public String getNumeroPredialS4() {
        return this.numeroPredialS4;
    }

    public void setNumeroPredialS4(String numeroPredialS4) {
        this.numeroPredialS4 = numeroPredialS4;
    }

    public String getNumeroPredialS5() {
        return this.numeroPredialS5;
    }

    public void setNumeroPredialS5(String numeroPredialS5) {
        this.numeroPredialS5 = numeroPredialS5;
    }

    public String getNumeroPredialS6() {
        return this.numeroPredialS6;
    }

    public void setNumeroPredialS6(String numeroPredialS6) {
        this.numeroPredialS6 = numeroPredialS6;
    }

    public String getNumeroPredialS7() {
        return this.numeroPredialS7;
    }

    public void setNumeroPredialS7(String numeroPredialS7) {
        this.numeroPredialS7 = numeroPredialS7;
    }

    public String getNumeroPredialS8() {
        return this.numeroPredialS8;
    }

    public void setNumeroPredialS8(String numeroPredialS8) {
        this.numeroPredialS8 = numeroPredialS8;
    }

    public String getNumeroPredialS9() {
        return this.numeroPredialS9;
    }

    public void setNumeroPredialS9(String numeroPredialS9) {
        this.numeroPredialS9 = numeroPredialS9;
    }

    public String getNumeroPredialS10() {
        return this.numeroPredialS10;
    }

    public void setNumeroPredialS10(String numeroPredialS10) {
        this.numeroPredialS10 = numeroPredialS10;
    }

    public String getNumeroPredialS11() {
        return this.numeroPredialS11;
    }

    public void setNumeroPredialS11(String numeroPredialS11) {
        this.numeroPredialS11 = numeroPredialS11;
    }

    public String getNumeroPredialS12() {
        return numeroPredialS12;
    }

    public void setNumeroPredialS12(String numeroPredialS12) {
        this.numeroPredialS12 = numeroPredialS12;
    }

    public String getDireccionPredio() {
        return this.direccionPredio;
    }

    public void setDireccionPredio(String direccionPredio) {
        this.direccionPredio = direccionPredio;
    }

    public String getNumeroPredialAnterior() {
        return this.numeroPredialAnterior;
    }

    public void setNumeroPredialAnterior(String numeroPredialAnterior) {
        this.numeroPredialAnterior = numeroPredialAnterior;
    }

    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getNombrePropietario() {
        return this.nombrePropietario;
    }

    public void setNombrePropietario(String nombrePropietario) {
        this.nombrePropietario = nombrePropietario;
    }

    public String getIdentificacionPropietario() {
        return identificacionPropietario;
    }

    public void setIdentificacionPropietario(String identificacionPropietario) {
        this.identificacionPropietario = identificacionPropietario;
    }

    public String getUsoConstruccionId() {
        return this.usoConstruccionId;
    }

    public void setUsoConstruccionId(String usoConstruccionId) {
        this.usoConstruccionId = usoConstruccionId;
    }

    public String getDestinoPredioId() {
        return this.destinoPredioId;
    }

    public void setDestinoPredioId(String destinoPredioId) {
        this.destinoPredioId = destinoPredioId;
    }

    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    public BigDecimal getConsecutivoCatastral() {
        return this.consecutivoCatastral;
    }

    public void setConsecutivoCatastral(BigDecimal consecutivoCatastral) {
        this.consecutivoCatastral = consecutivoCatastral;
    }

    public String getNip() {
        return this.nip;
    }

    public void setNip(String chip) {
        this.nip = chip;
    }

    public double getAreaTerrenoMin() {
        return this.areaTerrenoMin;
    }

    public void setAreaTerrenoMin(double areaTerrenoMin) {
        this.areaTerrenoMin = areaTerrenoMin;
    }

    public double getAreaTerrenoMax() {
        return this.areaTerrenoMax;
    }

    public void setAreaTerrenoMax(double areaTerrenoMax) {
        this.areaTerrenoMax = areaTerrenoMax;
    }

    public double getAreaConstruidaMin() {
        return this.areaConstruidaMin;
    }

    public void setAreaConstruidaMin(double areaConstruidaMin) {
        this.areaConstruidaMin = areaConstruidaMin;
    }

    public double getAreaConstruidaMax() {
        return this.areaConstruidaMax;
    }

    public void setAreaConstruidaMax(double areaConstruidaMax) {
        this.areaConstruidaMax = areaConstruidaMax;
    }

    public String getUoc() {
        return uoc;
    }

    public void setUoc(String uoc) {
        this.uoc = uoc;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    //-------------------------------------------------------------------------------------------------
    public FiltroDatosConsultaPredio() {
//		this.departamentoId = "";
//		this.municipioId = "";
//		this.territorialId = "";
    }
//--------------------------------------------------------------------------------------------------

    public void assembleNumeroPredialFromSegments() {
        StringBuilder numeroPredialAssembled;
        numeroPredialAssembled = new StringBuilder();
        if (this.numeroPredialS1 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS1);
        }
        if (this.numeroPredialS2 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS2);
        }
        if (this.numeroPredialS3 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS3);
        }
        if (this.numeroPredialS4 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS4);
        }
        if (this.numeroPredialS5 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS5);
        }
        if (this.numeroPredialS6 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS6);
        }
        if (this.numeroPredialS7 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS7);
        }
        if (this.numeroPredialS8 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS8);
        }
        if (this.numeroPredialS9 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS9);
        }
        if (this.numeroPredialS10 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS10);
        }
        if (this.numeroPredialS11 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS11);
        }
        if (this.numeroPredialS12 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialS12);
        }
        this.numeroPredial = numeroPredialAssembled.toString();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Separa un numero predial en sus componentes
     *
     * @author christian.rodriguez
     * @param numeroPredial cadena con el numero predial completo
     */
    public void disassembleNumeroPredialInSegments(String numeroPredial) {

        this.numeroPredialS1 = numeroPredial.substring(0, 2);
        this.numeroPredialS2 = numeroPredial.substring(2, 5);
        this.numeroPredialS3 = numeroPredial.substring(5, 7);
        this.numeroPredialS4 = numeroPredial.substring(7, 9);
        this.numeroPredialS5 = numeroPredial.substring(9, 11);
        this.numeroPredialS6 = numeroPredial.substring(11, 13);
        this.numeroPredialS7 = numeroPredial.substring(13, 17);
        this.numeroPredialS8 = numeroPredial.substring(17, 21);
        this.numeroPredialS9 = numeroPredial.substring(21, 22);
        this.numeroPredialS10 = numeroPredial.substring(22, 24);
        this.numeroPredialS11 = numeroPredial.substring(24, 26);
        this.numeroPredialS12 = numeroPredial.substring(26, 30);

        this.numeroPredial = numeroPredial;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método que valida que todos los segmentos del número predial sean válidos Éste método se debe
     * ejecutar antes del assembleNumeroPredialFromSegments para que no concatene null por error.
     *
     * @author david.cifuentes
     */
    public boolean validateSegmentsNumeroPredial() {

        if (this.numeroPredialS1 == null || this.numeroPredialS1.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS2 == null || this.numeroPredialS2.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS3 == null || this.numeroPredialS3.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS4 == null || this.numeroPredialS4.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS5 == null || this.numeroPredialS5.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS6 == null || this.numeroPredialS6.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS7 == null || this.numeroPredialS7.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS8 == null || this.numeroPredialS8.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS9 == null || this.numeroPredialS9.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS10 == null || this.numeroPredialS10.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS11 == null || this.numeroPredialS11.trim().isEmpty()) {
            return false;
        }
        if (this.numeroPredialS12 == null || this.numeroPredialS12.trim().isEmpty()) {
            return false;
        }
        return true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     * @return
     */
    public boolean isPersonaNatural() {
        boolean answer = false;
        if (this.tipoPersonaPropietario != null) {
            answer =
                (this.tipoPersonaPropietario.equals(EPersonaTipoPersona.NATURAL.getCodigo())) ?
                true : false;
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     * @return
     */
    public boolean isPersonaJuridica() {
        boolean answer = false;
        if (this.tipoPersonaPropietario != null) {
            answer =
                (this.tipoPersonaPropietario.equals(EPersonaTipoPersona.JURIDICA.getCodigo())) ?
                true : false;
        }

        return answer;
    }

//end of class
}
