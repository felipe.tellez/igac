package co.gov.igac.snc.persistence.entity.conservacion;

import java.sql.Timestamp;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * HPredioServidumbre entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "H_PREDIO_SERVIDUMBRE", schema = "SNC_CONSERVACION")
public class HPredioServidumbre implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -6276195996648138065L;
    private HPredioServidumbreId id;
    private HPredio HPredio;
    private Long predioId;
    private String servidumbre;
    private Timestamp fechaInscripcionCatastral;
    private String cancelaInscribe;
    private String usuarioLog;
    private Timestamp fechaLog;

    // Constructors
    /** default constructor */
    public HPredioServidumbre() {
    }

    /** minimal constructor */
    public HPredioServidumbre(HPredioServidumbreId id, HPredio HPredio,
        Long predioId, String servidumbre, String usuarioLog,
        Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predioId = predioId;
        this.servidumbre = servidumbre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public HPredioServidumbre(HPredioServidumbreId id, HPredio HPredio,
        Long predioId, String servidumbre,
        Timestamp fechaInscripcionCatastral, String cancelaInscribe,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predioId = predioId;
        this.servidumbre = servidumbre;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "HPredioId", column = @Column(name = "H_PREDIO_ID", nullable =
            false, precision = 10, scale = 0)),
        @AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false, precision =
            10, scale = 0))})
    public HPredioServidumbreId getId() {
        return this.id;
    }

    public void setId(HPredioServidumbreId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "H_PREDIO_ID", nullable = false, insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Column(name = "PREDIO_ID", nullable = false, precision = 10, scale = 0)
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    @Column(name = "SERVIDUMBRE", nullable = false, length = 30)
    public String getServidumbre() {
        return this.servidumbre;
    }

    public void setServidumbre(String servidumbre) {
        this.servidumbre = servidumbre;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    public Timestamp getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Timestamp fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

}
