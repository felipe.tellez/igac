package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the ASIGNACION database table.
 *
 */
@Entity
public class Asignacion implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fecha;
    private Date fechaCorreoElectronico;
    private Date fechaLog;
    private String funcionarioAsignador;
    private String observaciones;
    private String territorialCodigo;
    private String usuarioLog;

    private List<AvaluoAsignacionProfesional> avaluoAsignacionProfesionals;

    public Asignacion() {
    }

    @Id
    @SequenceGenerator(name = "ASIGNACION_ID_GENERATOR", sequenceName = "ASIGNACION_ID_SEQ",
        allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASIGNACION_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_CORREO_ELECTRONICO")
    public Date getFechaCorreoElectronico() {
        return this.fechaCorreoElectronico;
    }

    public void setFechaCorreoElectronico(Date fechaCorreoElectronico) {
        this.fechaCorreoElectronico = fechaCorreoElectronico;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "FUNCIONARIO_ASIGNADOR")
    public String getFuncionarioAsignador() {
        return this.funcionarioAsignador;
    }

    public void setFuncionarioAsignador(String funcionarioAsignador) {
        this.funcionarioAsignador = funcionarioAsignador;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "TERRITORIAL_CODIGO")
    public String getTerritorialCodigo() {
        return this.territorialCodigo;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.territorialCodigo = territorialCodigo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to AvaluoAsignacionProfesional
    @OneToMany(mappedBy = "ordenPractica")
    public List<AvaluoAsignacionProfesional> getAvaluoAsignacionProfesionals() {
        return this.avaluoAsignacionProfesionals;
    }

    public void setAvaluoAsignacionProfesionals(
        List<AvaluoAsignacionProfesional> avaluoAsignacionProfesionals) {
        this.avaluoAsignacionProfesionals = avaluoAsignacionProfesionals;
    }

}
