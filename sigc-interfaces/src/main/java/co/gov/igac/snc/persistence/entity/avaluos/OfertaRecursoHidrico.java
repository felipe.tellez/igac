package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the OFERTA_RECURSO_HIDRICO database table.
 *
 */
@Entity
@Table(name = "OFERTA_RECURSO_HIDRICO")
public class OfertaRecursoHidrico implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private Date fechaLog;
    private String recursoHidrico;
    private String usuarioLog;
    private OfertaInmobiliaria ofertaInmobiliaria;

    public OfertaRecursoHidrico() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OFERTA_RECURSO_HIDRICO_ID_SEQ")
    @SequenceGenerator(name = "OFERTA_RECURSO_HIDRICO_ID_SEQ", sequenceName =
        "OFERTA_RECURSO_HIDRICO_ID_SEQ", allocationSize = 1)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "RECURSO_HIDRICO")
    public String getRecursoHidrico() {
        return this.recursoHidrico;
    }

    public void setRecursoHidrico(String recursoHidrico) {
        this.recursoHidrico = recursoHidrico;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to OfertaInmobiliaria
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OFERTA_INMOBILIARIA_ID")
    public OfertaInmobiliaria getOfertaInmobiliaria() {
        return this.ofertaInmobiliaria;
    }

    public void setOfertaInmobiliaria(OfertaInmobiliaria ofertaInmobiliaria) {
        this.ofertaInmobiliaria = ofertaInmobiliaria;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((recursoHidrico == null) ? 0 : recursoHidrico.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OfertaRecursoHidrico other = (OfertaRecursoHidrico) obj;
        if (recursoHidrico == null) {
            if (other.recursoHidrico != null) {
                return false;
            }
        } else if (!recursoHidrico.equals(other.recursoHidrico)) {
            return false;
        }
        return true;
    }

}
