package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles motivos de un estado de trámite.
 *
 * @author david.cifuentes
 */
public enum EMotivoEstadoTramite {

    MOTIVO_ARCHIVADO2(Long.valueOf(4)),
    OTRO(Long.valueOf(3)),
    POR_ESCANEO(Long.valueOf(5)),
    POR_DOCUMENTOS(Long.valueOf(1)),
    POR_RECLASIFICAR(Long.valueOf(2));

    private Long id;

    private EMotivoEstadoTramite(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}
