package co.gov.igac.snc.persistence.entity.vistas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Clase que se usa como pk para la vista VProfesionalAvaluosContrato. Hay que usarla porque como no
 * hay un campo que pueda servir de id</br></br>
 *
 * La combinación de campos definida como pk es profesionalAvaluosId, contratoId
 *
 * @author rodrigo.hernandez
 */
@Embeddable
public class VProfesionalAvaluosContratoPK implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5519685748204744416L;

    private String profesionalAvaluosId;
    private String contratoId;

    @Column(name = "PROFESIONAL_AVALUOS_ID")
    public String getProfesionalAvaluosId() {
        return profesionalAvaluosId;
    }

    public void setProfesionalAvaluosId(String profesionalAvaluosId) {
        this.profesionalAvaluosId = profesionalAvaluosId;
    }

    @Column(name = "CONTRATO_ID")
    public String getContratoId() {
        return contratoId;
    }

    public void setContratoId(String contratoId) {
        this.contratoId = contratoId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime *
            result +
            ((this.profesionalAvaluosId == null) ? 0 :
                this.profesionalAvaluosId.hashCode());
        result = prime * result +
            ((this.contratoId == null) ? 0 : this.contratoId.hashCode());

        return result;

    }

    @Override
    public boolean equals(Object another) {
        return this.equals((VProfesionalAvaluosContratoPK) another);
    }
}
