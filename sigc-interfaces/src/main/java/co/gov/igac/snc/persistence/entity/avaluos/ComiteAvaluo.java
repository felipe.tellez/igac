package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

/**
 * The persistent class for the COMITE_AVALUO database table.
 *
 */
@Entity
@Table(name = "COMITE_AVALUO")
public class ComiteAvaluo implements Serializable {

    private static final long serialVersionUID = 4852147645909176732L;

    private Long id;
    private Long actaDocumentoId;
    private Long actaFirmadaDocumentoId;
    private String conclusiones;
    private Long controlCalidadAvaluoId;
    private Date fecha;
    private Date fechaLog;
    private String lugar;
    private String numeroActa;
    private String usuarioLog;
    private List<ComiteAvaluoParticipante> comiteAvaluoParticipantes;
    private List<ComiteAvaluoPonente> comiteAvaluoPonentes;

    public ComiteAvaluo() {
    }

    @Id
    @SequenceGenerator(name = "COMITE_AVALUO_ID_GENERATOR", sequenceName = "COMITE_AVALUO_ID_SEQ",
        allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMITE_AVALUO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACTA_DOCUMENTO_ID")
    public Long getActaDocumentoId() {
        return this.actaDocumentoId;
    }

    public void setActaDocumentoId(Long actaDocumentoId) {
        this.actaDocumentoId = actaDocumentoId;
    }

    @Column(name = "ACTA_FIRMADA_DOCUMENTO_ID")
    public Long getActaFirmadaDocumentoId() {
        return this.actaFirmadaDocumentoId;
    }

    public void setActaFirmadaDocumentoId(Long actaFirmadaDocumentoId) {
        this.actaFirmadaDocumentoId = actaFirmadaDocumentoId;
    }

    public String getConclusiones() {
        return this.conclusiones;
    }

    public void setConclusiones(String conclusiones) {
        this.conclusiones = conclusiones;
    }

    @Column(name = "CONTROL_CALIDAD_AVALUO_ID")
    public Long getControlCalidadAvaluoId() {
        return this.controlCalidadAvaluoId;
    }

    public void setControlCalidadAvaluoId(Long controlCalidadAvaluoId) {
        this.controlCalidadAvaluoId = controlCalidadAvaluoId;
    }

    @Temporal(TemporalType.DATE)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getLugar() {
        return this.lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    @Column(name = "NUMERO_ACTA")
    public String getNumeroActa() {
        return this.numeroActa;
    }

    public void setNumeroActa(String numeroActa) {
        this.numeroActa = numeroActa;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to ComiteAvaluoParticipante
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "comiteAvaluo")
    public List<ComiteAvaluoParticipante> getComiteAvaluoParticipantes() {
        return this.comiteAvaluoParticipantes;
    }

    public void setComiteAvaluoParticipantes(
        List<ComiteAvaluoParticipante> comiteAvaluoParticipantes) {
        this.comiteAvaluoParticipantes = comiteAvaluoParticipantes;
    }

    // bi-directional many-to-one association to ComiteAvaluoPonente
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "comiteAvaluo")
    public List<ComiteAvaluoPonente> getComiteAvaluoPonentes() {
        return this.comiteAvaluoPonentes;
    }

    public void setComiteAvaluoPonentes(
        List<ComiteAvaluoPonente> comiteAvaluoPonentes) {
        this.comiteAvaluoPonentes = comiteAvaluoPonentes;
    }

    // ---------------------------------------------------------------------------------
    /**
     * método que genera una cadena de texto con los nombres de los ponentes del comité
     *
     * @author christian.rodriguez
     * @return {@link String} con los nombres de los ponentes, vacio si no se encontraron ponentes o
     * no se precargaron los {@link #comiteAvaluoPonentes}
     */
    @Transient
    public String getCadenaNombresPonentes() {

        String nombrePonentes = "";

        if (Hibernate.isInitialized(this.comiteAvaluoPonentes)) {
            for (ComiteAvaluoPonente ponente : this.comiteAvaluoPonentes) {
                if (Hibernate.isInitialized(ponente.getProfesionalAvaluos())) {
                    if (!nombrePonentes.isEmpty()) {
                        nombrePonentes = nombrePonentes.concat(", ");
                    }
                    nombrePonentes = nombrePonentes.concat(ponente
                        .getProfesionalAvaluos().getNombreCompleto());
                }
            }
        }

        return nombrePonentes;
    }

}
