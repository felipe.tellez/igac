package co.gov.igac.snc.dao.sig.vo;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Value object para transportar la información de los productos geográficos generados por el
 * sistema
 *
 *
 * @author juan.mendez
 *
 */
public class ProductoCatastralResultado {

    //Identificador del tipo de producto según lo definido en el servidor de productos
    //( ver http://srvcas1.igac.gov.co/redmine/projects/igacsigc/wiki/Listado_de_Productos )
    private String tipoProducto;
    //Identificador del archivo en el sistema documental
    private String idArchivo;
    //Nombre de la imagen generada
    private String nombreArchivo;

    //Código asociado al producto
    private String codigo;

    //Escala Asociada al producto
    private String escala;

    /**
     *
     */
    public ProductoCatastralResultado(String idArchivo, String nombreArchivo, String tipoProducto,
        String codigo, String escala) {
        this.idArchivo = idArchivo;
        this.nombreArchivo = nombreArchivo;
        this.tipoProducto = tipoProducto;
        this.codigo = codigo;
        this.escala = escala;
    }

    /**
     *
     * @return
     */
    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public String getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(String idArchivo) {
        this.idArchivo = idArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEscala() {
        return escala;
    }

    public void setEscala(String escala) {
        this.escala = escala;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
