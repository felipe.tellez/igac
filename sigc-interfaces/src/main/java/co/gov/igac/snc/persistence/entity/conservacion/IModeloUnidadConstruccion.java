package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;
import java.util.List;

import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/*
 * MODIFICACIONES PROPIAS A LA INTERFACE IModeloUnidadConstruccion:
 *
 * @modified leidy.gonzalez :: Adición del atributo anioCancelacion y sus respectivos get y set ::
 * 22/05/2014
 *
 */
public interface IModeloUnidadConstruccion extends IProyeccionObject {

    public abstract Long getId();

    public abstract void setId(Long id);

    public abstract UsoConstruccion getUsoConstruccion();

    public abstract void setUsoConstruccion(UsoConstruccion usoConstruccion);

    public abstract String getUnidad();

    public abstract void setUnidad(String unidad);

    public abstract Long getCalificacionAnexoId();

    public abstract void setCalificacionAnexoId(Long calificacionAnexoId);

    public abstract String getTipoConstruccion();

    public abstract void setTipoConstruccion(String tipoConstruccion);

    public abstract String getDescripcion();

    public abstract void setDescripcion(String descripcion);

    public abstract String getObservaciones();

    public abstract void setObservaciones(String observaciones);

    public abstract String getTipoCalificacion();

    public abstract void setTipoCalificacion(String tipoCalificacion);

    public abstract Double getTotalPuntaje();

    public abstract void setTotalPuntaje(Double totalPuntaje);

    public abstract String getTipificacion();

    public abstract void setTipificacion(String tipificacion);

    public abstract String getTipificacionValor();

    public abstract void setTipificacionValor(String tipificacionValor);

    public abstract Integer getAnioConstruccion();

    public abstract void setAnioConstruccion(Integer anioConstruccion);

    public abstract Double getAreaConstruida();

    public abstract void setAreaConstruida(Double areaConstruida);

    public abstract Double getAvaluo();

    public abstract void setAvaluo(Double avaluo);

    public abstract Integer getTotalBanios();

    public abstract void setTotalBanios(Integer totalBanios);

    public abstract Integer getTotalHabitaciones();

    public abstract void setTotalHabitaciones(Integer totalHabitaciones);

    public abstract Integer getTotalLocales();

    public abstract void setTotalLocales(Integer totalLocales);

    public abstract Integer getTotalPisosUnidad();

    public abstract void setTotalPisosUnidad(Integer totalPisosUnidad);

    public abstract Integer getTotalPisosConstruccion();

    public abstract void setTotalPisosConstruccion(
        Integer totalPisosConstruccion);

    public abstract Double getValorM2Construccion();

    public abstract void setValorM2Construccion(Double valorM2Construccion);

    public abstract String getPisoUbicacion();

    public abstract void setPisoUbicacion(String pisoUbicacion);

    public abstract String getCancelaInscribe();

    public abstract void setCancelaInscribe(String cancelaInscribe);

    public abstract List<PUnidadConstruccionComp> getPUnidadConstruccionComps();

    public abstract void setPUnidadConstruccionComps(
        List<PUnidadConstruccionComp> PUnidadConstruccionComps);

    public abstract String getTipoDominio();

    public abstract void setTipoDominio(String tipoDominio);

    public abstract Date getFechaInscripcionCatastral();

    public abstract void setFechaInscripcionCatastral(
        Date fechaInscripcionCatastral);

    public abstract String getCancelaInscribeValor();

    public abstract void setCancelaInscribeValor(String cancelaInscribeValor);

    public abstract Integer getAnioCancelacion();

    public abstract void setAnioCancelacion(Integer anioConstruccion);

}
