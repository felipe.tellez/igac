package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para el dominio AVALUO_PREDIO_ANEXO_FUENTE
 *
 * @author felipe.cadena
 */
public enum EAvaluoPredioAnexoFuente implements ECampoDetalleAvaluo {

    MEDIDA_CINTA_METRICA("1", "Medidas a cinta métrica");

    private String codigo;
    private String valor;

    private EAvaluoPredioAnexoFuente(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

    @Override
    public String getValor() {
        return valor;
    }
}
