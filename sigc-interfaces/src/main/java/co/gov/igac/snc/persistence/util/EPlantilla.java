/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con datos de la tabla plantilla
 *
 * @author juan.agudelo
 */
//OJO: el pirobín del autor nombró como 'mensaje' a lo que en la tabla corresponde al campo 'título'
public enum EPlantilla {

    ENCABEZADO_CORREO_ELECTRONICO("ENCABEZADO_CORREO_ELECTRONICO",
        "Plantilla del encabezado de correo electrónico"),
    MENSAJE_COMUNICACION_APROBACION_DE_COMISON("MENSAJE_COMUNICACION_APROBACION_DE_COMISON",
        "Mensaje que se envia por correo comunicando la aprobación de la comisión."),
    MENSAJE_COMUNICACION_APROBACION_CC_TERRITORIAL("MENSAJE_COMUNICACION_APROBACION_CC_TERRITORIAL",
        "Mensaje para notificación de la generación del oficio remisorio control de calidad de territorial."),
    MENSAJE_REPORTE_CONTROL_CALIDAD_GIT_AVALUOS("MENSAJE_REPORTE_CONTROL_CALIDAD_GIT_AVALUOS",
        "Mensaje para notificación de la generación del Reporte de control de calidad  de  GIT de Avalúos."),
    MENSAJE_COMUNICACION_NO_APROBACION_AVALUO("MENSAJE_COMUNICACION_NO_APROBACION_AVALUO",
        "Mensaje de notificación de la generación de oficio de no aprobación de avalúo"),
    MENSAJE_COMUNICACION_ORDEN_AJUSTE("MENSAJE_COMUNICACION_ORDEN_AJUSTE",
        "Mensaje correo electronico para la notificación responsable SIG de manzanas que necesitan ajuste, en el proceso de alistamiento en Actualización"),
    MENSAJE_COMUNICACION_RECHAZO_DE_COMISON("MENSAJE_COMUNICACION_RECHAZO_DE_COMISON",
        "Mensaje que se envia por correo comunicando el rechazo de la comisión."),
    MENSAJE_COMUNICACION_RETIRO_TRAMITE_DE_COMISON("MENSAJE_COMUNICACION_RETIRO_TRAMITE_DE_COMISON",
        "Mensaje que se envia por correo comunicando el retiro de un trámite de la comisión."),
    MENSAJE_COMUNICACION_SOLICITUD_AMPLIACION_AVALUO(
        "MENSAJE_COMUNICACION_SOLICITUD_AMPLIACION_AVALUO",
        "Mensaje de comunicación de solicitud de ampliación de tiempos de trámite de avalúos."),
    MENSAJE_COMUNICACION_SOLICITUD_CANCELACION_AVALUO(
        "MENSAJE_COMUNICACION_SOLICITUD_CANCELACION_AVALUO",
        "Mensaje de comunicación de solicitud de cancelación de trámite de avalúos."),
    MENSAJE_COMUNICACION_SOLICITUD_SUSPENCION_AVALUO(
        "MENSAJE_COMUNICACION_SOLICITUD_SUSPENCION_AVALUO",
        "Mensaje de comunicación de solicitud de suspención de trámite de avalúos."),
    MENSAJE_CONVOCATORIA_PONENCIA_AVALUO("MENSAJE_CONVOCATORIA_PONENCIA_AVALUO",
        "Mensaje convocatoria para la presentación de ponencia de avalúos"),
    MENSAJE_DERECHO_PETICION_O_TUTELA("MENSAJE_DERECHO_PETICION_O_TUTELA",
        "Mensaje derecho de petición o tutela"),
    MENSAJE_DE_CANCELACION_TRAMITE("MENSAJE_DE_CANCELACION_TRAMITE_AL_EJECUTOR",
        "mensaje de cancelacion de trámite en cualqueir instante."),
    MENSAJE_TRAMITE_CATASTRAL_PREDIO_AVALUO("MENSAJE_TRAMITE_CATASTRAL_PREDIO_AVALUO",
        "Mensaje de notificación para informar que un predio requiere trámite catastral"),
    MENSAJE_SOLICITUD_ORDEN_AJUSTES_PERIMETRO_URBANO(
        "MENSAJE_SOLICITUD_ORDEN_AJUSTES_PERIMETRO_URBANO",
        "Mensaje que se envia por correo de los ajustes a plano de perímetro urbano con fines catastrales."),
    MENSAJE_SOLICITUD_ORDEN_AJUSTES_PROYECTO_NOM_VIAL(
        "MENSAJE_SOLICITUD_ORDEN_AJUSTES_PROYECTO_NOM_VIAL",
        "Mensaje que se envia por correo de los ajustes a plano de nomenclatura vial para fines catastrales."),
    MENSAJE_REPORTE_CONTROL_CALIDAD_TERRITORIAL("MENSAJE_REPORTE_CONTROL_CALIDAD_TERRITORIAL",
        "Mensaje para enviar reporte de control de calidad de territorial a avaluadores"),
    MENSAJE_RRHH_COMISION_RECORRIDO_PRELIMINAR("MENSAJE_RRHH_COMISION_RECORRIDO_PRELIMINAR",
        "Mensaje para el correo de comunicación al recurso humano seleccionado para comisión al recorrido preliminar de un municipio"),
    MENSAJE_SOLICITUD_DOCUMENTOS_FALTANTES("MENSAJE_SOLICITUD_DOCUMENTOS_FALTANTES",
        "Mensaje para la solicitud de documentos faltantes de una solicitud de avalúo"),
    MENSAJE_SOLICITUD_CORRECCIONES_WEB("MENSAJE_SOLICITUD_CORRECCIONES_WEB",
        "Mensaje para solicitar correcciones en una solicitud ingresada via web."),
    MENSAJE_SOLICITUD_DIGITALIZACION_PERIMETRO_URBANO(
        "MENSAJE_SOLICITUD_DIGITALIZACION_PERIMETRO_URBANO",
        "Mensaje que se envia por correo de la solicitud digitalización de perímetro urbano con fines catastrales al responsable SIG."),
    MENSAJE_SOLICITUD_DIGITALIZACION_PROYECTO_NOM_VIAL(
        "MENSAJE_SOLICITUD_DIGITALIZACION_PROYECTO_NOM_VIAL",
        "Mensaje que se envia por correo de la solicitud de actualización de nomenclatura a responsable SIG."),
    MENSAJE_EVALUACION_DESEMPENO_PROFESIONAL_AVALUADOR(
        "MENSAJE_EVALUACION_DESEMPENO_PROFESIONAL_AVALUADOR",
        "Mensaje que se envía por correo de la evaluacion de desempeño de un avaluador"),
    MENSAJE_AJUSTES_CONTROL_CALIDAD_LEVANTOPOGRAFICO(
        "MENSAJE_AJUSTES_CONTROL_CALIDAD_LEVANTOPOGRAFICO",
        "Mensaje para ajustes de control de calidad de levantamiento topografico"),
    MENSAJE_OBSERVACIONES_ELABORACION_COTIZACION("MENSAJE_OBSERVACIONES_ELABORACION_COTIZACION",
        "Observaciones a elaboración de cotización "),
    MENSAJE_COMUNICACION_CANCELACION_CREACION_TRAMITE(
        "MENSAJE_COMUNICACION_CANCELACION_CREACION_TRAMITE",
        "Comunicación de  cancelación y/o creación de trámites"),
    MENSAJE_COMUNICACION_REGISTRO_TRAMITEL_PRODUCTOS(
        "MENSAJE_COMUNICACION_REGISTRO_TRAMITEL_PRODUCTOS",
        "Mensaje que se envia por correo comunicando  al responsable de conservación la inconsistencia de un registro de trámite catastral."),

    MENSAJE_RESTITUCION_TIERRAS_BLOQUEO_RESOLUCION("MENSAJE_RESTITUCION_TIERRAS_BLOQUEO_RESOLUCION",
        "Mensaje que se envía por correo a la oficina de restitución de tierras cuando se bloquea un predio con resolución ya emitida."),
    MENSAJE_ENTIDAD_BLOQUEO_RESOLUCION("MENSAJE_ENTIDAD_BLOQUEO_RESOLUCION",
        "Mensaje que se envía por correo a la entidad que ordena el bloqueo cuando se bloquea un predio con resolución ya emitida."),
    MENSAJE_ENTIDAD_BLOQUEO_SUSPENDER_TRAMITE("MENSAJE_ENTIDAD_BLOQUEO_SUSPENDER_TRAMITE",
        "Mensaje que se envía por correo a la entidad que  ordena el bloqueo  cuando se bloquea un predio  que ya paso por la actividad  determinar precedencia."),
    MENSAJE_DETALLE_BLOQUEO_SUSPENDER_TRAMITE("MENSAJE_DETALLE_BLOQUEO_SUSPENDER_TRAMITE",
        "Detalle del mensaje que se envía por correo al ordenar el bloqueo  cuando un predio  que ya paso por la actividad  determinar precedencia  ."),
    MENSAJE_PIE_BLOQUEO_SUSPENDER_TRAMITE("MENSAJE_PIE_BLOQUEO_SUSPENDER_TRAMITE",
        "Pie del mensaje que se envía por correo al ordenar el bloqueo  cuando un predio  que ya paso por la actividad  determinar precedencia."),

    MENSAJE_COMUNICACION_REGISTRO_REPORTES_PREDIALES(
        "MENSAJE_COMUNICACION_REGISTRO_REPORTES_PREDIALES",
        "Mensaje que se envia por correo comunicando  al usuario logeado de cada territorial, sobre que el reporte ya fue generado para ser visualizado."),

    MENSAJE_RECALCULO_AVALUOS_ACTUALIZACION("MENSAJE_RECALCULO_AVALUOS_ACTUALIZACION",
        "Mensaje que se envia por correo comunicando al usuario de actualizacion para informar que ya se realizó el recalculo de avaluos."),
    MENSAJE_TRAMITES_AVANZADOS("MENSAJE_TRAMITES_AVANZADOS",
        "Mensaje que se envia por correo comunicando al responsable o coordinador que algunas tramites no se avanzaron por ya lo habia hecho otro usuario.");

    private String codigo;
    private String mensaje;

    private EPlantilla(String codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

}
