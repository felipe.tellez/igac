package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import java.util.LinkedList;

/**
 * The persistent class for the FM_MODELO_CONSTRUCCION database table.
 *
 * @modified juan.agudelo 21-03-12 Adición de la secuencia FM_MODELO_CONSTRUCCION_ID_SEQ
 * @modified david.cifuentes :: Adicion del campo usoConstruccion :: 20/04/2012 Adicion del campo
 * tipoCalificacion :: 25/04/2012
 *
 */
@Entity
@Table(name = "FM_MODELO_CONSTRUCCION")
public class FmModeloConstruccion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3166966831388354635L;

    private long id;
    private Short anioConstruccion;
    private Double areaConstruida;
    private Long calificacionAnexoId;
    private String descripcion;
    private Double dimension;
    private String dimensionUnidadMedida;
    private Date fechaLog;
    private String observaciones;
    private String tipificacion;
    private String tipoConstruccion;
    private Byte totalBanios;
    private Byte totalHabitaciones;
    private Byte totalLocales;
    private Byte totalPisosConstruccion;
    private Byte totalPisosUnidad;
    private Double totalPuntaje;
    private String usuarioLog;
    private List<FmConstruccionComponente> fmConstruccionComponentes;
    private FichaMatrizModelo fichaMatrizModelo;
    private List<ModeloConstruccionFoto> modeloConstruccionFotos;

    private UsoConstruccion usoConstruccion;
    private String tipoCalificacion;

    public FmModeloConstruccion() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FM_MODELO_CONSTRUCCION_ID_SEQ")
    @SequenceGenerator(name = "FM_MODELO_CONSTRUCCION_ID_SEQ", sequenceName =
        "FM_MODELO_CONSTRUCCION_ID_SEQ", allocationSize = 1)
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "ANIO_CONSTRUCCION")
    public Short getAnioConstruccion() {
        return this.anioConstruccion;
    }

    public void setAnioConstruccion(Short anioConstruccion) {
        this.anioConstruccion = anioConstruccion;
    }

    @Column(name = "AREA_CONSTRUIDA")
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "CALIFICACION_ANEXO_ID")
    public Long getCalificacionAnexoId() {
        return this.calificacionAnexoId;
    }

    public void setCalificacionAnexoId(Long calificacionAnexoId) {
        this.calificacionAnexoId = calificacionAnexoId;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getDimension() {
        return this.dimension;
    }

    public void setDimension(Double dimension) {
        this.dimension = dimension;
    }

    @Column(name = "DIMENSION_UNIDAD_MEDIDA")
    public String getDimensionUnidadMedida() {
        return this.dimensionUnidadMedida;
    }

    public void setDimensionUnidadMedida(String dimensionUnidadMedida) {
        this.dimensionUnidadMedida = dimensionUnidadMedida;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getTipificacion() {
        return this.tipificacion;
    }

    public void setTipificacion(String tipificacion) {
        this.tipificacion = tipificacion;
    }

    @Column(name = "TIPO_CONSTRUCCION")
    public String getTipoConstruccion() {
        return this.tipoConstruccion;
    }

    public void setTipoConstruccion(String tipoConstruccion) {
        this.tipoConstruccion = tipoConstruccion;
    }

    @Column(name = "TOTAL_BANIOS")
    public Byte getTotalBanios() {
        return this.totalBanios;
    }

    public void setTotalBanios(Byte totalBanios) {
        this.totalBanios = totalBanios;
    }

    @Column(name = "TOTAL_HABITACIONES")
    public Byte getTotalHabitaciones() {
        return this.totalHabitaciones;
    }

    public void setTotalHabitaciones(Byte totalHabitaciones) {
        this.totalHabitaciones = totalHabitaciones;
    }

    @Column(name = "TOTAL_LOCALES")
    public Byte getTotalLocales() {
        return this.totalLocales;
    }

    public void setTotalLocales(Byte totalLocales) {
        this.totalLocales = totalLocales;
    }

    @Column(name = "TOTAL_PISOS_CONSTRUCCION")
    public Byte getTotalPisosConstruccion() {
        return this.totalPisosConstruccion;
    }

    public void setTotalPisosConstruccion(Byte totalPisosConstruccion) {
        this.totalPisosConstruccion = totalPisosConstruccion;
    }

    @Column(name = "TOTAL_PISOS_UNIDAD")
    public Byte getTotalPisosUnidad() {
        return this.totalPisosUnidad;
    }

    public void setTotalPisosUnidad(Byte totalPisosUnidad) {
        this.totalPisosUnidad = totalPisosUnidad;
    }

    @Column(name = "TOTAL_PUNTAJE")
    public Double getTotalPuntaje() {
        return this.totalPuntaje;
    }

    public void setTotalPuntaje(Double totalPuntaje) {
        this.totalPuntaje = totalPuntaje;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to FmConstruccionComponente
    @OneToMany(mappedBy = "fmModeloConstruccion")
    public List<FmConstruccionComponente> getFmConstruccionComponentes() {
        return this.fmConstruccionComponentes;
    }

    public void setFmConstruccionComponentes(
        List<FmConstruccionComponente> fmConstruccionComponentes) {
        this.fmConstruccionComponentes = fmConstruccionComponentes;
    }

    // bi-directional many-to-one association to FichaMatrizModelo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FICHA_MATRIZ_MODELO_ID")
    public FichaMatrizModelo getFichaMatrizModelo() {
        return this.fichaMatrizModelo;
    }

    public void setFichaMatrizModelo(FichaMatrizModelo fichaMatrizModelo) {
        this.fichaMatrizModelo = fichaMatrizModelo;
    }

    // bi-directional many-to-one association to ModeloConstruccionFoto
    @OneToMany(mappedBy = "fmModeloConstruccion", fetch = FetchType.LAZY)
    public List<ModeloConstruccionFoto> getModeloConstruccionFotos() {
        return this.modeloConstruccionFotos;
    }

    public void setModeloConstruccionFotos(
        List<ModeloConstruccionFoto> modeloConstruccionFotos) {
        this.modeloConstruccionFotos = modeloConstruccionFotos;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USO_ID", nullable = true)
    public UsoConstruccion getUsoConstruccion() {
        return this.usoConstruccion;
    }

    public void setUsoConstruccion(UsoConstruccion usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    @Column(name = "TIPO_CALIFICACION", length = 30)
    public String getTipoCalificacion() {
        return this.tipoCalificacion;
    }

    public void setTipoCalificacion(String tipoCalificacion) {
        this.tipoCalificacion = tipoCalificacion;
    }

    /**
     * Retorna una entidad tipo PFmModeloConstruccion a partir del objeto actual se usa para hacer
     * operaciones definidas sobre proyecciones, pero que no existe proyeccion en BD
     *
     *
     * @author felipe.cadena
     *
     * @return
     */
    public PFmModeloConstruccion obtenerProyectadoDesdeOriginal() {

        PFmModeloConstruccion resultado = new PFmModeloConstruccion();

        if (this.anioConstruccion != null) {
            resultado.setAnioConstruccion(this.anioConstruccion.intValue());
        }
        resultado.setAreaConstruida(this.areaConstruida);
        resultado.setCalificacionAnexoId(this.calificacionAnexoId);
        resultado.setDescripcion(this.descripcion);
        resultado.setDimension(this.dimension);
        resultado.setDimensionUnidadMedida(this.dimensionUnidadMedida);
        resultado.setFechaLog(this.fechaLog);
        resultado.setId(this.id);
        resultado.setObservaciones(this.observaciones);
        //resultado.setPFichaMatrizModelo(null);
        List<PFmConstruccionComponente> pComponetes = new LinkedList<PFmConstruccionComponente>();
        for (FmConstruccionComponente com : this.fmConstruccionComponentes) {
            PFmConstruccionComponente pCom = com.obtenerProyectadoDesdeOriginal();
            pComponetes.add(pCom);
        }
        resultado.setPFmConstruccionComponentes(pComponetes);

        resultado.setTipificacion(this.tipificacion);
        resultado.setTipoCalificacion(this.tipoCalificacion);
        resultado.setTipoConstruccion(this.tipoConstruccion);
        if (this.totalBanios != null) {
            resultado.setTotalBanios(this.totalBanios.intValue());
        }
        if (this.totalHabitaciones != null) {
            resultado.setTotalHabitaciones(this.totalHabitaciones.intValue());
        }
        if (this.totalLocales != null) {
            resultado.setTotalLocales(this.totalLocales.intValue());
        }
        if (this.totalPisosConstruccion != null) {
            resultado.setTotalPisosConstruccion(this.totalPisosConstruccion.intValue());
        }
        if (this.totalPisosUnidad != null) {
            resultado.setTotalPisosUnidad(this.totalPisosUnidad.intValue());
        }
        resultado.setTotalPuntaje(this.totalPuntaje);
        resultado.setUsoConstruccion(this.usoConstruccion);
        resultado.setUsuarioLog(this.usuarioLog);

        List<PModeloConstruccionFoto> pfotos = new LinkedList<PModeloConstruccionFoto>();
        for (ModeloConstruccionFoto foto : this.modeloConstruccionFotos) {
            PModeloConstruccionFoto pCom = foto.obtenerProyectadoDesdeOriginal();
            pfotos.add(pCom);
        }

        return resultado;

    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static FmModeloConstruccion parseFmModeloConstruccion(
        HFmModeloConstruccion hFmModeloConstruccion) {
        FmModeloConstruccion fmModeloConstruccion = new FmModeloConstruccion();

        fmModeloConstruccion.setId(hFmModeloConstruccion.getId().getId());
        fmModeloConstruccion.setAnioConstruccion(hFmModeloConstruccion.getAnioConstruccion());
        fmModeloConstruccion.setAreaConstruida(hFmModeloConstruccion.getAreaConstruida());
        fmModeloConstruccion.setCalificacionAnexoId(hFmModeloConstruccion.getCalificacionAnexoId());
        fmModeloConstruccion.setDescripcion(hFmModeloConstruccion.getDescripcion());
        fmModeloConstruccion.setDimension(hFmModeloConstruccion.getDimension());
        fmModeloConstruccion.setDimensionUnidadMedida(hFmModeloConstruccion.
            getDimensionUnidadMedida());
        fmModeloConstruccion.setFechaLog(hFmModeloConstruccion.getFechaLog());
        fmModeloConstruccion.setObservaciones(hFmModeloConstruccion.getObservaciones());
        fmModeloConstruccion.setTipificacion(hFmModeloConstruccion.getTipificacion());
        fmModeloConstruccion.setTipoConstruccion(hFmModeloConstruccion.getTipoConstruccion());
        fmModeloConstruccion.setTotalBanios(hFmModeloConstruccion.getTotalBanios());
        fmModeloConstruccion.setTotalHabitaciones(hFmModeloConstruccion.getTotalHabitaciones());
        fmModeloConstruccion.setTotalLocales(hFmModeloConstruccion.getTotalLocales());
        fmModeloConstruccion.setTotalPisosConstruccion(hFmModeloConstruccion.
            getTotalPisosConstruccion());
        fmModeloConstruccion.setTotalPisosUnidad(hFmModeloConstruccion.getTotalPisosUnidad());
        fmModeloConstruccion.setTotalPuntaje(hFmModeloConstruccion.getTotalPuntaje());
        fmModeloConstruccion.setUsuarioLog(hFmModeloConstruccion.getUsuarioLog());
        fmModeloConstruccion.setFichaMatrizModelo(FichaMatrizModelo.parseFichaMatrizModelo(
            hFmModeloConstruccion.getHFichaMatrizModelo()));
        fmModeloConstruccion.setUsoConstruccion(hFmModeloConstruccion.getUsoConstruccion());
        fmModeloConstruccion.setTipoCalificacion(hFmModeloConstruccion.getTipoCalificacion());

        /*
         * juan.cruz::19012::06/12/2017::Por cuestiones de tiempo de entrega y que no son necesarios
         * en el proceso actual, estos dos objetos no se llenan.
         *
         * List<FmConstruccionComponente> fmConstruccionComponentes; List<ModeloConstruccionFoto>
         * modeloConstruccionFotos;
         *
         */
        return fmModeloConstruccion;
    }
}
