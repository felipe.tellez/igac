package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the FICHA_MATRIZ_MODELO database table.
 *
 * @modified juan.agudelo 21-03-12 Adición de la secuencia FICHA_MATRIZ_MODELO_ID_SEQ cambio de
 * bigdecimal por unidades acordes al campo.
 *
 * david.cifuentes :: Adicion del campo descripcion :: 12/04/2012 Eliminación del campo usoId por
 * cambio en el modelo de datos :: 20/04/2012 Eliminación del campo tipoCalificacion por cambio en
 * el modelo de datos :: 25/04/2012
 *
 */
@Entity
@Table(name = "FICHA_MATRIZ_MODELO", schema = "SNC_CONSERVACION")
public class FichaMatrizModelo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1586227438971472764L;

    private Long id;
    private Double areaConstruida;
    private Double areaTerreno;
    private Date fechaLog;
    private String nombre;
    private Integer numeroUnidades;
    private Long planoDocumentoId;
    private String usuarioLog;
    private FichaMatriz fichaMatriz;
    private List<FmModeloConstruccion> fmModeloConstruccions;

    private String descripcion;

    private String estado;

    public FichaMatrizModelo() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FICHA_MATRIZ_MODELO_ID_SEQ")
    @SequenceGenerator(name = "FICHA_MATRIZ_MODELO_ID_SEQ", sequenceName =
        "FICHA_MATRIZ_MODELO_ID_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA_CONSTRUIDA")
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "AREA_TERRENO")
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "NUMERO_UNIDADES")
    public Integer getNumeroUnidades() {
        return this.numeroUnidades;
    }

    public void setNumeroUnidades(Integer numeroUnidades) {
        this.numeroUnidades = numeroUnidades;
    }

    @Column(name = "PLANO_DOCUMENTO_ID")
    public Long getPlanoDocumentoId() {
        return this.planoDocumentoId;
    }

    public void setPlanoDocumentoId(Long planoDocumentoId) {
        this.planoDocumentoId = planoDocumentoId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to FichaMatriz
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FICHA_MATRIZ_ID")
    public FichaMatriz getFichaMatriz() {
        return this.fichaMatriz;
    }

    public void setFichaMatriz(FichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    // bi-directional many-to-one association to FmModeloConstruccion
    @OneToMany(mappedBy = "fichaMatrizModelo", fetch = FetchType.LAZY)
    public List<FmModeloConstruccion> getFmModeloConstruccions() {
        return this.fmModeloConstruccions;
    }

    public void setFmModeloConstruccions(
        List<FmModeloConstruccion> fmModeloConstruccions) {
        this.fmModeloConstruccions = fmModeloConstruccions;
    }

    @Column(length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Retorna una entidad tipo PFichaMatrizModelo a partir del objeto actual se usa para hacer
     * operaciones definidas sobre proyecciones, pero que no existe proyeccion en BD
     *
     *
     * @author felipe.cadena
     *
     * @return
     */
    public PFichaMatrizModelo obtenerProyectadoDesdeOriginal() {
        PFichaMatrizModelo resultado = new PFichaMatrizModelo();

        resultado.setAreaConstruida(this.areaConstruida);
        resultado.setAreaTerreno(this.areaTerreno);
        resultado.setDescripcion(this.descripcion);
        resultado.setFechaLog(this.fechaLog);
        resultado.setNombre(this.nombre);
        if (this.numeroUnidades != null) {
            resultado.setNumeroUnidades(this.numeroUnidades.doubleValue());
        }
        if (this.planoDocumentoId != null) {
            resultado.setPlanoDocumentoId(BigDecimal.valueOf(this.planoDocumentoId));
        }
        resultado.setUsuarioLog(this.usuarioLog);

        List<PFmModeloConstruccion> pmcs = new LinkedList<PFmModeloConstruccion>();
        for (FmModeloConstruccion mc : this.fmModeloConstruccions) {
            PFmModeloConstruccion pmc = mc.obtenerProyectadoDesdeOriginal();
            pmcs.add(pmc);
        }

        resultado.setPFmModeloConstruccions(pmcs);

        return resultado;

    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static FichaMatrizModelo parseFichaMatrizModelo(HFichaMatrizModelo hFichaMatrizModelo) {
        FichaMatrizModelo fichaMatrizModelo = new FichaMatrizModelo();

        fichaMatrizModelo.setId(hFichaMatrizModelo.getId().getId());
        fichaMatrizModelo.setAreaConstruida(hFichaMatrizModelo.getAreaConstruida());
        fichaMatrizModelo.setAreaTerreno(hFichaMatrizModelo.getAreaTerreno());
        fichaMatrizModelo.setFechaLog(hFichaMatrizModelo.getFechaLog());
        fichaMatrizModelo.setNombre(hFichaMatrizModelo.getNombre());
        fichaMatrizModelo.setNumeroUnidades(hFichaMatrizModelo.getNumeroUnidades());
        fichaMatrizModelo.setPlanoDocumentoId(hFichaMatrizModelo.getPlanoDocumentoId());
        fichaMatrizModelo.setUsuarioLog(hFichaMatrizModelo.getUsuarioLog());
        fichaMatrizModelo.setFichaMatriz(FichaMatriz.parseFichaMatriz(hFichaMatrizModelo.
            getHFichaMatriz()));

        List<FmModeloConstruccion> fmModeloConstruccions = new ArrayList<FmModeloConstruccion>();
        for (HFmModeloConstruccion aHFmModeloConstruccion : hFichaMatrizModelo.
            getHFmModeloConstruccions()) {
            fmModeloConstruccions.add(FmModeloConstruccion.parseFmModeloConstruccion(
                aHFmModeloConstruccion));
        }

        fichaMatrizModelo.setFmModeloConstruccions(fmModeloConstruccions);
        fichaMatrizModelo.setDescripcion(hFichaMatrizModelo.getDescripcion());
        fichaMatrizModelo.setEstado(hFichaMatrizModelo.getEstado());

        return fichaMatrizModelo;
    }

}
