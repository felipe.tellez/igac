package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ACTUALIZACION_NOMENCLATURA_VIA database table.
 */
/**
 * Modificaciones a la clase ActualizacionNomenclaturaVia:
 */
@Entity
@Table(name = "ACTUALIZACION_NOMENCLATURA_VIA", schema = "SNC_ACTUALIZACION")
public class ActualizacionNomenclaturaVia implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long acuerdoDocumentoId;
    private String casosViasUsoPrivado;
    private String ejeReferenciaCalles;
    private String ejeReferenciaCarreras;
    private Date fechaLog;
    private String nombreAlcalde;
    private Long oficioDocumentoId;
    private String puntoPartidaCarreraPrimera;
    private String usuarioLog;
    private Actualizacion actualizacion;
    private List<NomenclaturaVialCondicion> nomenclaturaVialCondicions;
    private List<NomenclaturaVialEntidad> nomenclaturaVialEntidads;
    private List<NomenclaturaVialNorma> nomenclaturaVialNormas;

    public ActualizacionNomenclaturaVia() {
    }

    /**
     * Constructor con los campos obligatorios
     */
    public ActualizacionNomenclaturaVia(Long id, Actualizacion actualizacion,
        Date fechaLog, String usuarioLog) {
        this.id = id;
        this.actualizacion = actualizacion;
        this.fechaLog = fechaLog;
        this.usuarioLog = usuarioLog;
    }

    /**
     * Full constructor
     */
    public ActualizacionNomenclaturaVia(Long id, Actualizacion actualizacion,
        Long acuerdoDocumentoId, String casosViasUsoPrivado,
        String ejeReferenciaCalles, String ejeReferenciaCarreras,
        Date fechaLog, String nombreAlcalde, String normasIncluidas,
        Long oficioDocumentoId, String puntoPartidaCarreraPrimera,
        String usuarioLog,
        List<NomenclaturaVialCondicion> nomenclaturaVialCondicions,
        List<NomenclaturaVialEntidad> nomenclaturaVialEntidads,
        List<NomenclaturaVialNorma> nomenclaturaVialNormas) {
        this.id = id;
        this.actualizacion = actualizacion;
        this.acuerdoDocumentoId = acuerdoDocumentoId;
        this.casosViasUsoPrivado = casosViasUsoPrivado;
        this.ejeReferenciaCalles = casosViasUsoPrivado;
        this.ejeReferenciaCarreras = ejeReferenciaCarreras;
        this.fechaLog = fechaLog;
        this.nombreAlcalde = nombreAlcalde;
        this.nomenclaturaVialNormas = nomenclaturaVialNormas;
        this.oficioDocumentoId = oficioDocumentoId;
        this.puntoPartidaCarreraPrimera = puntoPartidaCarreraPrimera;
        this.usuarioLog = usuarioLog;
        this.nomenclaturaVialCondicions = nomenclaturaVialCondicions;
        this.nomenclaturaVialEntidads = nomenclaturaVialEntidads;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ACTUALIZACION_NOMENCLAT_ID_GENERATOR")
    @SequenceGenerator(name = "ACTUALIZACION_NOMENCLAT_ID_GENERATOR", sequenceName =
        "ACTUALIZACION_NOMENCLAT_ID_SEQ", allocationSize = 1)
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACUERDO_DOCUMENTO_ID", precision = 10)
    public Long getAcuerdoDocumentoId() {
        return this.acuerdoDocumentoId;
    }

    public void setAcuerdoDocumentoId(Long acuerdoDocumentoId) {
        this.acuerdoDocumentoId = acuerdoDocumentoId;
    }

    @Column(name = "CASOS_VIAS_USO_PRIVADO", length = 600)
    public String getCasosViasUsoPrivado() {
        return this.casosViasUsoPrivado;
    }

    public void setCasosViasUsoPrivado(String casosViasUsoPrivado) {
        this.casosViasUsoPrivado = casosViasUsoPrivado;
    }

    @Column(name = "EJE_REFERENCIA_CALLES", length = 600)
    public String getEjeReferenciaCalles() {
        return this.ejeReferenciaCalles;
    }

    public void setEjeReferenciaCalles(String ejeReferenciaCalles) {
        this.ejeReferenciaCalles = ejeReferenciaCalles;
    }

    @Column(name = "EJE_REFERENCIA_CARRERAS", length = 600)
    public String getEjeReferenciaCarreras() {
        return this.ejeReferenciaCarreras;
    }

    public void setEjeReferenciaCarreras(String ejeReferenciaCarreras) {
        this.ejeReferenciaCarreras = ejeReferenciaCarreras;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NOMBRE_ALCALDE", length = 100)
    public String getNombreAlcalde() {
        return this.nombreAlcalde;
    }

    public void setNombreAlcalde(String nombreAlcalde) {
        this.nombreAlcalde = nombreAlcalde;
    }

    @Column(name = "OFICIO_DOCUMENTO_ID", precision = 10)
    public Long getOficioDocumentoId() {
        return this.oficioDocumentoId;
    }

    public void setOficioDocumentoId(Long oficioDocumentoId) {
        this.oficioDocumentoId = oficioDocumentoId;
    }

    @Column(name = "PUNTO_PARTIDA_CARRERA_PRIMERA", length = 600)
    public String getPuntoPartidaCarreraPrimera() {
        return this.puntoPartidaCarreraPrimera;
    }

    public void setPuntoPartidaCarreraPrimera(String puntoPartidaCarreraPrimera) {
        this.puntoPartidaCarreraPrimera = puntoPartidaCarreraPrimera;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to Actualizacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTUALIZACION_ID", nullable = false)
    public Actualizacion getActualizacion() {
        return this.actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    // bi-directional many-to-one association to NomenclaturaVialCondicion
    @OneToMany(mappedBy = "actualizacionNomenclaturaVia")
    public List<NomenclaturaVialCondicion> getNomenclaturaVialCondicions() {
        return this.nomenclaturaVialCondicions;
    }

    public void setNomenclaturaVialCondicions(
        List<NomenclaturaVialCondicion> nomenclaturaVialCondicions) {
        this.nomenclaturaVialCondicions = nomenclaturaVialCondicions;
    }

    // bi-directional many-to-one association to NomenclaturaVialEntidad
    @OneToMany(mappedBy = "actualizacionNomenclaturaVia")
    public List<NomenclaturaVialEntidad> getNomenclaturaVialEntidads() {
        return this.nomenclaturaVialEntidads;
    }

    public void setNomenclaturaVialEntidads(
        List<NomenclaturaVialEntidad> nomenclaturaVialEntidads) {
        this.nomenclaturaVialEntidads = nomenclaturaVialEntidads;
    }

    // bi-directional many-to-one association to NomenclaturaVialNorma
    @OneToMany(mappedBy = "actualizacionNomenclaturaVia")
    public List<NomenclaturaVialNorma> getNomenclaturaVialNormas() {
        return this.nomenclaturaVialNormas;
    }

    public void setNomenclaturaVialNormas(
        List<NomenclaturaVialNorma> nomenclaturaVialNormas) {
        this.nomenclaturaVialNormas = nomenclaturaVialNormas;
    }

}
