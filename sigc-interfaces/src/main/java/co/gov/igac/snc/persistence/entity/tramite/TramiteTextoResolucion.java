package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * TramiteTextoResolucion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRAMITE_TEXTO_RESOLUCION", schema = "SNC_TRAMITE", uniqueConstraints =
    @UniqueConstraint(columnNames = "TRAMITE_ID"))
public class TramiteTextoResolucion implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7007745216202312673L;
    // Fields

    /**
     *
     */
    private Long id;
    private Tramite tramite;
    private ModeloResolucion modeloResolucion;
    private String titulo;
    private String considerando;
    private String resuelve;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public TramiteTextoResolucion() {
    }

    /** minimal constructor */
    public TramiteTextoResolucion(Long id, Tramite tramite,
        ModeloResolucion modeloResolucion, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.modeloResolucion = modeloResolucion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramiteTextoResolucion(Long id, Tramite tramite,
        ModeloResolucion modeloResolucion, String titulo,
        String considerando, String resuelve, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.modeloResolucion = modeloResolucion;
        this.titulo = titulo;
        this.considerando = considerando;
        this.resuelve = resuelve;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tramite_texto_resolucio_id_seq")
    @SequenceGenerator(name = "tramite_texto_resolucio_id_seq", sequenceName =
        "tramite_texto_resolucio_id_seq", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", unique = true, nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODELO_RESOLUCION_ID", nullable = false)
    public ModeloResolucion getModeloResolucion() {
        return this.modeloResolucion;
    }

    public void setModeloResolucion(ModeloResolucion modeloResolucion) {
        this.modeloResolucion = modeloResolucion;
    }

    @Column(name = "TITULO", length = 600)
    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Column(name = "CONSIDERANDO")
    public String getConsiderando() {
        return this.considerando;
    }

    public void setConsiderando(String considerando) {
        this.considerando = considerando;
    }

    @Column(name = "RESUELVE")
    public String getResuelve() {
        return this.resuelve;
    }

    public void setResuelve(String resuelve) {
        this.resuelve = resuelve;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
