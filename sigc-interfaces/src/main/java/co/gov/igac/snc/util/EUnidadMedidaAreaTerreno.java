package co.gov.igac.snc.util;

/**
 * Enumeración para el dominio OFERTA_UNIDAD_CONSTRUCCION
 *
 * @author ariel.ortiz
 *
 * @modified rodrigo.hernandez - adición de atributo valor y de datos segun dominio
 * OFERTA_UNIDAD_CONSTRUCCION
 */
public enum EUnidadMedidaAreaTerreno {

    FANEGADA("FANEGADA", "Fanegada"),
    HECTAREA("HECTAREAS", "Hectárea"),
    KILOMETRO_CUADRADO("KILOMETRO CUADRADO", "Kilómetro cuadrado"),
    METRO_CUADRADO("METRO CUADRADO", "Metro cuadrado"),
    CUADRA("CUADRA", "Cuadra"),
    PLAZA("PLAZA", "Plaza");

    private String codigo;
    private String valor;

    private EUnidadMedidaAreaTerreno(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getValor() {
        return valor;
    }

}
