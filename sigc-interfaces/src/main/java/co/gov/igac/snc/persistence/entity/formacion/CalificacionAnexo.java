package co.gov.igac.snc.persistence.entity.formacion;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import co.gov.igac.snc.util.Constantes;

/**
 * CalificacionAnexo entity. @author MyEclipse Persistence Tools
 *
 * @modified by: pedro.garcia what: - adición de método toString(), hashCode(), equals() - adición
 * de named queries
 */
@Entity
@NamedQueries({@NamedQuery(
        name = "findByUsoConstruccionId",
        query =
        "from CalificacionAnexo ca join fetch ca.usoConstruccion where ca.usoConstruccion.id = :idUC")})
@Table(name = "CALIFICACION_ANEXO", schema = "SNC_FORMACION")
public class CalificacionAnexo implements java.io.Serializable, Comparable {

    // Fields
    private Long id;
    private UsoConstruccion usoConstruccion;
    private String descripcion;
    private Short puntos;
    private String usuarioLog;
    private Timestamp fechaLog;

    // Constructors
    /** default constructor */
    public CalificacionAnexo() {
    }

    /** full constructor */
    public CalificacionAnexo(Long id, UsoConstruccion usoConstruccion,
        String descripcion, Short puntos, String usuarioLog,
        Timestamp fechaLog) {
        this.id = id;
        this.usoConstruccion = usoConstruccion;
        this.descripcion = descripcion;
        this.puntos = puntos;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USO_CONSTRUCCION_ID", nullable = false)
    public UsoConstruccion getUsoConstruccion() {
        return this.usoConstruccion;
    }

    public void setUsoConstruccion(UsoConstruccion usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    @Column(name = "DESCRIPCION", nullable = false, length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "PUNTOS", nullable = false, precision = 4, scale = 0)
    public Short getPuntos() {
        return this.puntos;
    }

    public void setPuntos(Short puntos) {
        this.puntos = puntos;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

//-------  pilas!!!  -------------------------------------------------------------------------------
    /**
     * @return
     */
    @Override
    public String toString() {
        StringBuilder objectAsString;
        String stringSeparator = Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR;

        objectAsString = new StringBuilder();
        objectAsString.append(this.id).append(stringSeparator);

        //OJO: esta hp basura no, aunque el id del usoConstruccion fuera null, no lanzaba excepcion.
        //     porquería. me tuvo reloco
        objectAsString.append(this.usoConstruccion.getId()).append(stringSeparator);

        objectAsString.append(this.descripcion).append(stringSeparator);
        objectAsString.append(this.puntos);

        return objectAsString.toString();
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode();
    }

    /**
     * este lo genera el netbeans automáticamente al implementar el hashCode()
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CalificacionAnexo other = (CalificacionAnexo) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }

        //OJO: güeva estaba comparando dos Long cuando lo que quiero comparar es el int value de ellos
        if (this.usoConstruccion.getId().intValue() != other.usoConstruccion.getId().intValue()) {
            return false;
        }
        if ((this.descripcion == null) ? (other.descripcion != null) : !this.descripcion.equals(
            other.descripcion)) {
            return false;
        }
        if (this.puntos != other.puntos &&
            (this.puntos == null || !this.puntos.equals(other.puntos))) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof CalificacionAnexo) {
            CalificacionAnexo ca = (CalificacionAnexo) o;
            return this.descripcion.compareTo(ca.getDescripcion());
        }
        return 0;
    }

}
