package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los posibles valores del dominio DOCUMENTO_ARCHIVO_ANEXO_EST
 *
 * @author juan.agudelo
 */
public enum EDocumentoArchivoAnexoEstado {

    //D: valor ("codigo")
    CARGADO("2"),
    RECIBIDO("1");

    private String codigo;

    EDocumentoArchivoAnexoEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
