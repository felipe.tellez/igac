package co.gov.igac.snc.util;

/**
 * Enumeración con las posibles acciones de Ingresar/Modificar/Eliminar Ofertas
 *
 * @author ariel.ortiz
 */
public enum EIngresarModificarOfertas {

    ADICIONAR("adicionar"),
    ELIMINAR("eliminar"),
    MODIFICAR("modificar");

    private String codigo;

    private EIngresarModificarOfertas(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
