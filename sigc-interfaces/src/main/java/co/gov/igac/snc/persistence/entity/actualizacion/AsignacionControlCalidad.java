package co.gov.igac.snc.persistence.entity.actualizacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ASIGNACION_CONTROL_CALIDAD database table.
 *
 */
@Entity
@Table(name = "ASIGNACION_CONTROL_CALIDAD")
public class AsignacionControlCalidad implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4953973892189410369L;
    private Long id;
    private Date fecha;
    private Date fechaLog;
    private String medicionChequeo;
    private String observaciones;
    private String resultado;
    private String resultadoMedicion;
    private String usuarioLog;
    private LevantamientoAsignacion levantamientoAsignacion;
    private RecursoHumano recursoHumano;
    private List<ControlCalidadCriterio> controlCalidadCriterios;
    private List<ControlCalidadMuestra> controlCalidadMuestras;

    public AsignacionControlCalidad() {
    }

    @Id
    @SequenceGenerator(name = "ASIGNACION_CONTROL_CALI_ID_GENERATOR", sequenceName =
        "ASIGNACION_CONTROL_CALI_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "ASIGNACION_CONTROL_CALI_ID_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "MEDICION_CHEQUEO", length = 250)
    public String getMedicionChequeo() {
        return this.medicionChequeo;
    }

    public void setMedicionChequeo(String medicionChequeo) {
        this.medicionChequeo = medicionChequeo;
    }

    @Column(length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(length = 30)
    public String getResultado() {
        return this.resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Column(name = "RESULTADO_MEDICION", length = 250)
    public String getResultadoMedicion() {
        return this.resultadoMedicion;
    }

    public void setResultadoMedicion(String resultadoMedicion) {
        this.resultadoMedicion = resultadoMedicion;
    }

    @Column(name = "USUARIO_LOG", length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to LevantamientoAsignacion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LEVANTAMIENTO_ASIGNACION_ID", nullable = false)
    public LevantamientoAsignacion getLevantamientoAsignacion() {
        return this.levantamientoAsignacion;
    }

    public void setLevantamientoAsignacion(
        LevantamientoAsignacion levantamientoAsignacion) {
        this.levantamientoAsignacion = levantamientoAsignacion;
    }

    // bi-directional many-to-one association to RecursoHumano
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECURSO_HUMANO_ID")
    public RecursoHumano getRecursoHumano() {
        return this.recursoHumano;
    }

    public void setRecursoHumano(RecursoHumano recursoHumano) {
        this.recursoHumano = recursoHumano;
    }

    // bi-directional many-to-one association to ControlCalidadCriterio
    @OneToMany(mappedBy = "asignacionControlCalidad")
    public List<ControlCalidadCriterio> getControlCalidadCriterios() {
        return this.controlCalidadCriterios;
    }

    public void setControlCalidadCriterios(
        List<ControlCalidadCriterio> controlCalidadCriterios) {
        this.controlCalidadCriterios = controlCalidadCriterios;
    }

    // bi-directional many-to-one association to ControlCalidadMuestra
    @OneToMany(mappedBy = "asignacionControlCalidad")
    public List<ControlCalidadMuestra> getControlCalidadMuestras() {
        return this.controlCalidadMuestras;
    }

    public void setControlCalidadMuestras(
        List<ControlCalidadMuestra> controlCalidadMuestras) {
        this.controlCalidadMuestras = controlCalidadMuestras;
    }

}
