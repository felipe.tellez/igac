package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the PROFESIONAL_CONTRATO_ADICION database table.
 *
 * @modified christian.rodriguez agregado atributo profesionalAvaluosContrato
 *
 */
@Entity
@Table(name = "PROFESIONAL_CONTRATO_ADICION")
public class ProfesionalContratoAdicion implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long adicionDocumentoId;
    private Date fechaHasta;
    private Date fechaLog;
    private Long numero;
    private String tipo;
    private String usuarioLog;
    private Double valor;

    private ProfesionalAvaluosContrato profesionalAvaluosContrato;

    public ProfesionalContratoAdicion() {
    }

    @Id
    @SequenceGenerator(name = "PROFESIONAL_CONTRATO_ADICION_ID_GENERATOR", sequenceName =
        "CONTRATO_INTERADMINIS_AD_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "PROFESIONAL_CONTRATO_ADICION_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ADICION_DOCUMENTO_ID")
    public Long getAdicionDocumentoId() {
        return this.adicionDocumentoId;
    }

    public void setAdicionDocumentoId(Long adicionDocumentoId) {
        this.adicionDocumentoId = adicionDocumentoId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_HASTA")
    public Date getFechaHasta() {
        return this.fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public Long getNumero() {
        return this.numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    // bi-directional many-to-one association to ProfesionalAvaluosContrato
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESIONAL_AVALUOS_CONTRAT_ID", nullable = false)
    public ProfesionalAvaluosContrato getProfesionalAvaluosContrato() {
        return this.profesionalAvaluosContrato;
    }

    public void setProfesionalAvaluosContrato(ProfesionalAvaluosContrato profesionalAvaluosContrato) {
        this.profesionalAvaluosContrato = profesionalAvaluosContrato;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

}
