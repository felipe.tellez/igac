package co.gov.igac.snc.persistence.entity.tramite;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the TRAMITE_INCONSISTENCIA database table.
 *
 * Modificaciones propias a la clase {@link TramiteInconsistencia}:
 *
 * @modified :: david.cifuentes :: Se implementa la interfaz Cloneable y se agrega método clone ::
 * 17/08/2013
 */
@Entity
@Table(name = "TRAMITE_INCONSISTENCIA", schema = "SNC_TRAMITE")
public class TramiteInconsistencia implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String depurada;
    private Date fechaLog;
    private String inconsistencia;
    private String numeroPredial;
    private String tipo;
    private TramiteDepuracion tramiteDepuracion;
    private Tramite tramite;
    private String usuarioLog;

    public TramiteInconsistencia() {
    }

    @Id
    @SequenceGenerator(name = "TRAMITE_INCONSISTENCIA_ID_GENERATOR", sequenceName =
        "TRAMITE_INCONSISTENCIA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "TRAMITE_INCONSISTENCIA_ID_GENERATOR")
    @Column(unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDepurada() {
        return this.depurada;
    }

    public void setDepurada(String depurada) {
        this.depurada = depurada;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    public String getInconsistencia() {
        return this.inconsistencia;
    }

    public void setInconsistencia(String inconsistencia) {
        this.inconsistencia = inconsistencia;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_DEPURACION_ID")
    public TramiteDepuracion getTramiteDepuracion() {
        return this.tramiteDepuracion;
    }

    public void setTramiteDepuracion(TramiteDepuracion tramiteDepuracion) {
        this.tramiteDepuracion = tramiteDepuracion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // --------------------------------------- //
    /**
     *
     * Método que realiza un clone del objeto {@link TramiteInconsistencia}
     *
     * @author david.cifuentes
     */
    @Override
    public Object clone() {
        Object object;
        try {
            object = super.clone();
        } catch (CloneNotSupportedException ex) {
            object = null;
        }
        return object;
    }

}
