package co.gov.igac.snc.persistence.entity.conservacion;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Transient;

/**
 * HPredio entity. @author MyEclipse Persistence Tools
 *
 * @modified david.cifuentes 31-10-11 Se agregó el atributo Date fechaRegistro david.cifuentes Se
 * modifica el atributo CirculoRegistral, Municipio y Departamento :: 14/09/15
 * @modified david.cifuentes Se modifica el mapeo de CirculoRegistral y Departamento :: 17/07/2016
 *
 */
@Entity
@NamedQueries({@NamedQuery(name = "findHPredioByConsecutivoCatastral", query =
        "from HPredio hpredio where hpredio.consecutivoCatastral= :consecutivoCatastral")})
@Table(name = "H_PREDIO", schema = "SNC_CONSERVACION")
public class HPredio implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = 7773761336972534085L;
    private Long id;
    private Timestamp fecha;
    private String tipoHistoria;
    private Long tramiteId;
    private Long documentoId;
    private Predio predioId;
    private Departamento departamento;
    private Municipio municipio;
    private String tipoAvaluo;
    private String sectorCodigo;
    private String barrioCodigo;
    private String manzanaCodigo;
    private String predio;
    private String condicionPropiedad;
    private String edificio;
    private String piso;
    private String unidad;
    private String numeroPredial;
    private String numeroPredialAnterior;
    private String nip;
    private BigDecimal consecutivoCatastral;
    private BigDecimal consecutivoCatastralAnterior;
    private String zonaUnidadOrganica;
    private String localidadCodigo;
    private String corregimientoCodigo;
    private String nombre;
    private String destino;
    private String tipo;
    private Double areaTerreno;
    private CirculoRegistral circuloRegistral;
    private String numeroRegistro;
    private String numeroRegistroAnterior;
    private String tipoCatastro;
    private String este;
    private String norte;
    private String estado;
    private String estrato;
    private String chip;
    private String numeroUltimaResolucion;
    private Short anioUltimaResolucion;
    private Double areaConstruccion;
    private Date fechaInscripcionCatastral;
    private Date fechaRegistro;
    private String cancelaInscribe;
    private String usuarioLog;
    private Timestamp fechaLog;
    private List<HFoto> HFotos = new ArrayList<HFoto>();
    private List<HPersonaPredio> HPersonaPredios = new ArrayList<HPersonaPredio>();
    private List<HUnidadConstruccionComp> HUnidadConstruccionComps =
        new ArrayList<HUnidadConstruccionComp>(
            0);
    private List<HFichaMatriz> HFichaMatrizs = new ArrayList<HFichaMatriz>();
    private List<HPersonaPredioPropiedad> HPersonaPredioPropiedads =
        new ArrayList<HPersonaPredioPropiedad>(
            0);
    private List<HPersona> HPersonas = new ArrayList<HPersona>();
    private List<HPredioAvaluoCatastral> HPredioAvaluoCatastrals =
        new ArrayList<HPredioAvaluoCatastral>(
            0);
    private List<HUnidadConstruccion> HUnidadConstruccions = new ArrayList<HUnidadConstruccion>(
        0);
    private List<HPredioZona> HPredioZonas = new ArrayList<HPredioZona>();
    private List<HPredioServidumbre> HPredioServidumbres = new ArrayList<HPredioServidumbre>(
        0);
    private List<HPredioDireccion> HPredioDireccions = new ArrayList<HPredioDireccion>(
        0);

    // Constructors
    /** default constructor */
    public HPredio() {
    }

    /** minimal constructor */
    public HPredio(Long id, Timestamp fecha, String tipoHistoria,
        Predio predioId, Departamento departamento, Municipio municipio,
        String tipoAvaluo, String sectorCodigo, String barrioCodigo,
        String manzanaCodigo, String predio, String condicionPropiedad,
        String edificio, String piso, String unidad, String numeroPredial,
        String destino, String tipo, Double areaTerreno, String tipoCatastro,
        String estado, Double areaConstruccion, String usuarioLog,
        Timestamp fechaLog) {
        this.id = id;
        this.fecha = fecha;
        this.tipoHistoria = tipoHistoria;
        this.predioId = predioId;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoAvaluo = tipoAvaluo;
        this.sectorCodigo = sectorCodigo;
        this.barrioCodigo = barrioCodigo;
        this.manzanaCodigo = manzanaCodigo;
        this.predio = predio;
        this.condicionPropiedad = condicionPropiedad;
        this.edificio = edificio;
        this.piso = piso;
        this.unidad = unidad;
        this.numeroPredial = numeroPredial;
        this.destino = destino;
        this.tipo = tipo;
        this.areaTerreno = areaTerreno;
        this.tipoCatastro = tipoCatastro;
        this.estado = estado;
        this.areaConstruccion = areaConstruccion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public HPredio(Long id, Timestamp fecha, String tipoHistoria,
        Long tramiteId, Long documentoId, Predio predioId,
        Departamento departamento, Municipio municipio,
        String tipoAvaluo, String sectorCodigo, String barrioCodigo,
        String manzanaCodigo, String predio, String condicionPropiedad,
        String edificio, String piso, String unidad, String numeroPredial,
        String numeroPredialAnterior, String nip,
        BigDecimal consecutivoCatastral,
        BigDecimal consecutivoCatastralAnterior, String zonaUnidadOrganica,
        String localidadCodigo, String corregimientoCodigo, String nombre,
        String destino, String tipo, Double areaTerreno,
        CirculoRegistral circuloRegistral, String numeroRegistro,
        String numeroRegistroAnterior, String tipoCatastro, String este,
        String norte, String estado, String estrato, String chip,
        Double areaConstruccion, Date fechaInscripcionCatastral,
        Date fechaRegistro,
        String cancelaInscribe, String usuarioLog, Timestamp fechaLog,
        List<HFoto> HFotos, List<HPersonaPredio> HPersonaPredios,
        List<HUnidadConstruccionComp> HUnidadConstruccionComps,
        List<HFichaMatriz> HFichaMatrizs,
        List<HPersonaPredioPropiedad> HPersonaPredioPropiedads,
        List<HPersona> HPersonas,
        List<HPredioAvaluoCatastral> HPredioAvaluoCatastrals,
        List<HUnidadConstruccion> HUnidadConstruccions,
        List<HPredioZona> HPredioZonas,
        List<HPredioServidumbre> HPredioServidumbres,
        List<HPredioDireccion> HPredioDireccions,
        String numeroUltimaResolucion, Short anioUltimaResolucion) {
        this.id = id;
        this.fecha = fecha;
        this.tipoHistoria = tipoHistoria;
        this.tramiteId = tramiteId;
        this.documentoId = documentoId;
        this.predioId = predioId;
        this.departamento = departamento;
        this.municipio = municipio;
        this.tipoAvaluo = tipoAvaluo;
        this.sectorCodigo = sectorCodigo;
        this.barrioCodigo = barrioCodigo;
        this.manzanaCodigo = manzanaCodigo;
        this.predio = predio;
        this.condicionPropiedad = condicionPropiedad;
        this.edificio = edificio;
        this.piso = piso;
        this.unidad = unidad;
        this.numeroPredial = numeroPredial;
        this.numeroPredialAnterior = numeroPredialAnterior;
        this.nip = nip;
        this.consecutivoCatastral = consecutivoCatastral;
        this.consecutivoCatastralAnterior = consecutivoCatastralAnterior;
        this.zonaUnidadOrganica = zonaUnidadOrganica;
        this.localidadCodigo = localidadCodigo;
        this.corregimientoCodigo = corregimientoCodigo;
        this.nombre = nombre;
        this.destino = destino;
        this.tipo = tipo;
        this.areaTerreno = areaTerreno;
        this.circuloRegistral = circuloRegistral;
        this.numeroRegistro = numeroRegistro;
        this.numeroRegistroAnterior = numeroRegistroAnterior;
        this.tipoCatastro = tipoCatastro;
        this.este = este;
        this.norte = norte;
        this.estado = estado;
        this.estrato = estrato;
        this.chip = chip;
        this.areaConstruccion = areaConstruccion;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
        this.fechaRegistro = fechaRegistro;
        this.cancelaInscribe = cancelaInscribe;
        this.numeroUltimaResolucion = numeroUltimaResolucion;
        this.anioUltimaResolucion = anioUltimaResolucion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.HFotos = HFotos;
        this.HPersonaPredios = HPersonaPredios;
        this.HUnidadConstruccionComps = HUnidadConstruccionComps;
        this.HFichaMatrizs = HFichaMatrizs;
        this.HPersonaPredioPropiedads = HPersonaPredioPropiedads;
        this.HPersonas = HPersonas;
        this.HPredioAvaluoCatastrals = HPredioAvaluoCatastrals;
        this.HUnidadConstruccions = HUnidadConstruccions;
        this.HPredioZonas = HPredioZonas;
        this.HPredioServidumbres = HPredioServidumbres;
        this.HPredioDireccions = HPredioDireccions;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "FECHA", nullable = false, length = 7)
    public Timestamp getFecha() {
        return this.fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    @Column(name = "TIPO_HISTORIA", nullable = false, length = 30)
    public String getTipoHistoria() {
        return this.tipoHistoria;
    }

    public void setTipoHistoria(String tipoHistoria) {
        this.tipoHistoria = tipoHistoria;
    }

    @Column(name = "TRAMITE_ID", precision = 10, scale = 0)
    public Long getTramiteId() {
        return this.tramiteId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    @Column(name = "DOCUMENTO_ID", precision = 10, scale = 0)
    public Long getDocumentoId() {
        return this.documentoId;
    }

    public void setDocumentoId(Long documentoId) {
        this.documentoId = documentoId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Predio predioId) {
        this.predioId = predioId;
    }

    // D: cambiados para reflejar el cambio de tipo de dato en estas columnas
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO")
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO")
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "TIPO_AVALUO", nullable = false, length = 2)
    public String getTipoAvaluo() {
        return this.tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    @Column(name = "SECTOR_CODIGO", nullable = false, length = 2)
    public String getSectorCodigo() {
        return this.sectorCodigo;
    }

    public void setSectorCodigo(String sectorCodigo) {
        this.sectorCodigo = sectorCodigo;
    }

    @Column(name = "BARRIO_CODIGO", nullable = false, length = 4)
    public String getBarrioCodigo() {
        return this.barrioCodigo;
    }

    public void setBarrioCodigo(String barrioCodigo) {
        this.barrioCodigo = barrioCodigo;
    }

    @Column(name = "MANZANA_CODIGO", nullable = false, length = 4)
    public String getManzanaCodigo() {
        return this.manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    @Column(name = "PREDIO", nullable = false, length = 4)
    public String getPredio() {
        return this.predio;
    }

    public void setPredio(String predio) {
        this.predio = predio;
    }

    @Column(name = "CONDICION_PROPIEDAD", nullable = false, length = 1)
    public String getCondicionPropiedad() {
        return this.condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    @Column(name = "EDIFICIO", nullable = false, length = 2)
    public String getEdificio() {
        return this.edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    @Column(name = "PISO", nullable = false, length = 2)
    public String getPiso() {
        return this.piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    @Column(name = "UNIDAD", nullable = false, length = 4)
    public String getUnidad() {
        return this.unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    @Column(name = "NUMERO_PREDIAL", nullable = false, length = 30)
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "NUMERO_PREDIAL_ANTERIOR", length = 20)
    public String getNumeroPredialAnterior() {
        return this.numeroPredialAnterior;
    }

    public void setNumeroPredialAnterior(String numeroPredialAnterior) {
        this.numeroPredialAnterior = numeroPredialAnterior;
    }

    @Column(name = "NIP", length = 20)
    public String getNip() {
        return this.nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    @Column(name = "CONSECUTIVO_CATASTRAL", precision = 20, scale = 0)
    public BigDecimal getConsecutivoCatastral() {
        return this.consecutivoCatastral;
    }

    public void setConsecutivoCatastral(BigDecimal consecutivoCatastral) {
        this.consecutivoCatastral = consecutivoCatastral;
    }

    @Column(name = "CONSECUTIVO_CATASTRAL_ANTERIOR", precision = 20, scale = 0)
    public BigDecimal getConsecutivoCatastralAnterior() {
        return this.consecutivoCatastralAnterior;
    }

    public void setConsecutivoCatastralAnterior(
        BigDecimal consecutivoCatastralAnterior) {
        this.consecutivoCatastralAnterior = consecutivoCatastralAnterior;
    }

    @Column(name = "ZONA_UNIDAD_ORGANICA", length = 30)
    public String getZonaUnidadOrganica() {
        return this.zonaUnidadOrganica;
    }

    public void setZonaUnidadOrganica(String zonaUnidadOrganica) {
        this.zonaUnidadOrganica = zonaUnidadOrganica;
    }

    @Column(name = "LOCALIDAD_CODIGO", length = 7)
    public String getLocalidadCodigo() {
        return this.localidadCodigo;
    }

    public void setLocalidadCodigo(String localidadCodigo) {
        this.localidadCodigo = localidadCodigo;
    }

    @Column(name = "CORREGIMIENTO_CODIGO", length = 7)
    public String getCorregimientoCodigo() {
        return this.corregimientoCodigo;
    }

    public void setCorregimientoCodigo(String corregimientoCodigo) {
        this.corregimientoCodigo = corregimientoCodigo;
    }

    @Column(name = "NOMBRE", length = 250)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "DESTINO", nullable = false, length = 30)
    public String getDestino() {
        return this.destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "AREA_TERRENO", nullable = false, precision = 18)
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CIRCULO_REGISTRAL", nullable = true)
    public CirculoRegistral getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(CirculoRegistral circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "NUMERO_REGISTRO", length = 20)
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "NUMERO_REGISTRO_ANTERIOR", length = 20)
    public String getNumeroRegistroAnterior() {
        return this.numeroRegistroAnterior;
    }

    public void setNumeroRegistroAnterior(String numeroRegistroAnterior) {
        this.numeroRegistroAnterior = numeroRegistroAnterior;
    }

    @Column(name = "TIPO_CATASTRO", nullable = false, length = 2)
    public String getTipoCatastro() {
        return this.tipoCatastro;
    }

    public void setTipoCatastro(String tipoCatastro) {
        this.tipoCatastro = tipoCatastro;
    }

    @Column(name = "ESTE", length = 20)
    public String getEste() {
        return this.este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    @Column(name = "NORTE", length = 20)
    public String getNorte() {
        return this.norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    @Column(name = "ESTADO", nullable = false, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "ESTRATO", length = 30)
    public String getEstrato() {
        return this.estrato;
    }

    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    @Column(name = "CHIP", length = 14)
    public String getChip() {
        return this.chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    @Column(name = "AREA_CONSTRUCCION", nullable = false, precision = 18)
    public Double getAreaConstruccion() {
        return this.areaConstruccion;
    }

    public void setAreaConstruccion(Double areaConstruccion) {
        this.areaConstruccion = areaConstruccion;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_REGISTRO", length = 7)
    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "NUMERO_ULTIMA_RESOLUCION", length = 30)
    public String getNumeroUltimaResolucion() {
        return this.numeroUltimaResolucion;
    }

    public void setNumeroUltimaResolucion(String numeroUltimaResolucion) {
        this.numeroUltimaResolucion = numeroUltimaResolucion;
    }

    @Column(name = "ANIO_ULTIMA_RESOLUCION", precision = 4, scale = 0)
    public Short getAnioUltimaResolucion() {
        return this.anioUltimaResolucion;
    }

    public void setAnioUltimaResolucion(Short anioUltimaResolucion) {
        this.anioUltimaResolucion = anioUltimaResolucion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HFoto> getHFotos() {
        return this.HFotos;
    }

    public void setHFotos(List<HFoto> HFotos) {
        this.HFotos = HFotos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HPersonaPredio> getHPersonaPredios() {
        return this.HPersonaPredios;
    }

    public void setHPersonaPredios(List<HPersonaPredio> HPersonaPredios) {
        this.HPersonaPredios = HPersonaPredios;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HUnidadConstruccionComp> getHUnidadConstruccionComps() {
        return this.HUnidadConstruccionComps;
    }

    public void setHUnidadConstruccionComps(
        List<HUnidadConstruccionComp> HUnidadConstruccionComps) {
        this.HUnidadConstruccionComps = HUnidadConstruccionComps;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HFichaMatriz> getHFichaMatrizs() {
        return this.HFichaMatrizs;
    }

    public void setHFichaMatrizs(List<HFichaMatriz> HFichaMatrizs) {
        this.HFichaMatrizs = HFichaMatrizs;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HPersonaPredioPropiedad> getHPersonaPredioPropiedads() {
        return this.HPersonaPredioPropiedads;
    }

    public void setHPersonaPredioPropiedads(
        List<HPersonaPredioPropiedad> HPersonaPredioPropiedads) {
        this.HPersonaPredioPropiedads = HPersonaPredioPropiedads;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HPersona> getHPersonas() {
        return this.HPersonas;
    }

    public void setHPersonas(List<HPersona> HPersonas) {
        this.HPersonas = HPersonas;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HPredioAvaluoCatastral> getHPredioAvaluoCatastrals() {
        return this.HPredioAvaluoCatastrals;
    }

    public void setHPredioAvaluoCatastrals(
        List<HPredioAvaluoCatastral> HPredioAvaluoCatastrals) {
        this.HPredioAvaluoCatastrals = HPredioAvaluoCatastrals;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HUnidadConstruccion> getHUnidadConstruccions() {
        return this.HUnidadConstruccions;
    }

    public void setHUnidadConstruccions(
        List<HUnidadConstruccion> HUnidadConstruccions) {
        this.HUnidadConstruccions = HUnidadConstruccions;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HPredioZona> getHPredioZonas() {
        return this.HPredioZonas;
    }

    public void setHPredioZonas(List<HPredioZona> HPredioZonas) {
        this.HPredioZonas = HPredioZonas;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HPredioServidumbre> getHPredioServidumbres() {
        return this.HPredioServidumbres;
    }

    public void setHPredioServidumbres(
        List<HPredioServidumbre> HPredioServidumbres) {
        this.HPredioServidumbres = HPredioServidumbres;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HPredio")
    public List<HPredioDireccion> getHPredioDireccions() {
        return this.HPredioDireccions;
    }

    public void setHPredioDireccions(List<HPredioDireccion> HPredioDireccions) {
        this.HPredioDireccions = HPredioDireccions;
    }

    /**
     * Método que retorna si el pPredio hace referencia a una ficha matriz.
     *
     * @author jonathan.chacon
     * @return
     */
    @Transient
    public boolean isEsPredioFichaMatriz() {
        boolean answer = false;
        if (this.HFichaMatrizs != null && !this.HFichaMatrizs.isEmpty()) {
            for (HFichaMatriz pfm : this.HFichaMatrizs) {
                if (pfm.getHPredio() != null &&
                    pfm.getHPredio().getId() != null &&
                    this.id.longValue() == pfm.getHPredio().getId()
                    .longValue()) {
                    answer = true;
                    break;
                }
            }
        }
        return answer;
    }

}
