package co.gov.igac.snc.persistence.entity.generales;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.util.Constantes;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

/**
 * TipoDocumento entity. @author MyEclipse Persistence Tools
 *
 * @modified by: david.cifuentes:	adición de método toString(), hashCode(), equals()
 * @modified by: pedro.garcia what: adición de la secuencia para el id
 */
@Entity
@Table(name = "TIPO_DOCUMENTO", schema = "SNC_GENERALES")
public class TipoDocumento implements java.io.Serializable, Comparable {

    // Fields
    private Long id;
    private Numeracion numeracionDocumento;
    private String nombre;
    private String clase;
    private String usuarioLog;
    private Date fechaLog;
    private String generado;
    private Set<Plantilla> plantillas = new HashSet<Plantilla>(0);
    private List<Documento> documentos = new ArrayList<Documento>();

    // Constructors
    /** default constructor */
    public TipoDocumento() {
    }

    /** minimal constructor */
    public TipoDocumento(Long id, Numeracion numeracionDocumento,
        String nombre, String clase, String generado, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.numeracionDocumento = numeracionDocumento;
        this.nombre = nombre;
        this.clase = clase;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.generado = generado;
    }

    /** full constructor */
    public TipoDocumento(Long id, Numeracion numeracionDocumento,
        String nombre, String clase, String generado, String usuarioLog, Date fechaLog,
        Set<Plantilla> plantillas, List<Documento> documentos) {
        this.id = id;
        this.numeracionDocumento = numeracionDocumento;
        this.nombre = nombre;
        this.clase = clase;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.plantillas = plantillas;
        this.documentos = documentos;
        this.generado = generado;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipoDocumento_ID_SEQ")
    @SequenceGenerator(name = "TipoDocumento_ID_SEQ", sequenceName = "TIPO_DOCUMENTO_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUMERACION_ID", nullable = false)
    public Numeracion getNumeracionDocumento() {
        return this.numeracionDocumento;
    }

    public void setNumeracionDocumento(Numeracion numeracionDocumento) {
        this.numeracionDocumento = numeracionDocumento;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "CLASE", nullable = false, length = 30)
    public String getClase() {
        return this.clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoDocumento")
    public Set<Plantilla> getPlantillas() {
        return this.plantillas;
    }

    public void setPlantillas(Set<Plantilla> plantillas) {
        this.plantillas = plantillas;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoDocumento")
    public List<Documento> getDocumentos() {
        return this.documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }

    @Column(name = "GENERADO", nullable = false, length = 2)
    public String getGenerado() {
        return this.generado;
    }

    public void setGenerado(String generado) {
        this.generado = generado;
    }

    // ------------------------------------------//
    /**
     * @author david.cifuentes
     * @return
     */
    @Override
    public String toString() {
        StringBuilder objectAsString;
        String stringSeparator = Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR;

        objectAsString = new StringBuilder();
        objectAsString.append(this.id).append(stringSeparator);
        if (this.numeracionDocumento != null) {
            objectAsString.append(this.numeracionDocumento.getId()).append(stringSeparator);
        }
        if (this.nombre != null) {
            objectAsString.append(this.nombre).append(stringSeparator);
        }
        if (this.clase != null) {
            objectAsString.append(this.clase).append(stringSeparator);
        }
        if (this.generado != null) {
            objectAsString.append(this.generado);
        }

        return objectAsString.toString();
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoDocumento other = (TipoDocumento) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof TipoDocumento) {
            TipoDocumento td = (TipoDocumento) o;
            return this.nombre.compareTo(td.getNombre());
        }
        return 0;
    }
}
