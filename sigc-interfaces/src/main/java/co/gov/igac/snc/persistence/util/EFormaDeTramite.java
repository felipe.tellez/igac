package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente al dominio de forma de trámite
 *
 * @author javier.aponte
 */
public enum EFormaDeTramite {
    NUEVO("Nuevo"),
    RADICADO("Radicado");

    private String valor;

    private EFormaDeTramite(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

}
