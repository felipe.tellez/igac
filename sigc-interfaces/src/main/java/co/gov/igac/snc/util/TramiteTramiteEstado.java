package co.gov.igac.snc.util;

import java.io.Serializable;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;

/**
 * Objeto que recuepra un trámite y un TramiteEstado vigente asociado al trámite
 *
 * @author juan.agudelo
 *
 */
public class TramiteTramiteEstado implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6445584706810085167L;

    private Tramite tramite;
    private TramiteEstado tramiteEstado;

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public TramiteEstado getTramiteEstado() {
        return tramiteEstado;
    }

    public void setTramiteEstado(TramiteEstado tramiteEstado) {
        this.tramiteEstado = tramiteEstado;
    }

    public TramiteTramiteEstado(Tramite tramite, TramiteEstado tramiteEstado) {
        super();
        this.tramite = tramite;
        this.tramiteEstado = tramiteEstado;
    }

    public TramiteTramiteEstado() {
        super();
    }

}
