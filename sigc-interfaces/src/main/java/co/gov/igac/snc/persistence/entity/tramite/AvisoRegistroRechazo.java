package co.gov.igac.snc.persistence.entity.tramite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * AvisoRegistroRechazo entity. @author MyEclipse Persistence Tools
 *
 * @modified by pedro.garcia - adición de anotación @Temporal a los campos Date (experimental) -
 * cambio de tipo de datos TimeStamp a Date - adición de anotaciones para la secuencia del id -
 * cambio de tipo de datos Set a List - adición de método transient getFirst
 */
@Entity
@Table(name = "AVISO_REGISTRO_RECHAZO", schema = "SNC_TRAMITE")
public class AvisoRegistroRechazo implements java.io.Serializable {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -1871802670115380147L;

    private Long id;
    private SolicitudAvisoRegistro solicitudAvisoRegistro;
    private String motivoNoProcede;
    private String observaciones;
    private String usuarioLog;
    private Date fechaLog;
    private List<AvisoRegistroRechazoPredio> avisoRegistroRechazoPredios =
        new ArrayList<AvisoRegistroRechazoPredio>();

    // Constructors
    /** default constructor */
    public AvisoRegistroRechazo() {
    }

    /** minimal constructor */
    public AvisoRegistroRechazo(Long id,
        SolicitudAvisoRegistro solicitudAvisoRegistro, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.solicitudAvisoRegistro = solicitudAvisoRegistro;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public AvisoRegistroRechazo(Long id,
        SolicitudAvisoRegistro solicitudAvisoRegistro,
        String motivoNoProcede, String observaciones, String usuarioLog,
        Date fechaLog,
        List<AvisoRegistroRechazoPredio> avisoRegistroRechazoPredios) {
        this.id = id;
        this.solicitudAvisoRegistro = solicitudAvisoRegistro;
        this.motivoNoProcede = motivoNoProcede;
        this.observaciones = observaciones;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.avisoRegistroRechazoPredios = avisoRegistroRechazoPredios;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AvisoRegistroRechazo_ID_SEQ")
    @SequenceGenerator(name = "AvisoRegistroRechazo_ID_SEQ",
        sequenceName = "AVISO_REGISTRO_RECHAZO_ID_SEQ", allocationSize = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLICITUD_AVISO_REGISTRO_ID", nullable = false)
    public SolicitudAvisoRegistro getSolicitudAvisoRegistro() {
        return this.solicitudAvisoRegistro;
    }

    public void setSolicitudAvisoRegistro(
        SolicitudAvisoRegistro solicitudAvisoRegistro) {
        this.solicitudAvisoRegistro = solicitudAvisoRegistro;
    }

    @Column(name = "MOTIVO_NO_PROCEDE", length = 20)
    public String getMotivoNoProcede() {
        return this.motivoNoProcede;
    }

    public void setMotivoNoProcede(String motivoNoProcede) {
        this.motivoNoProcede = motivoNoProcede;
    }

    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(value = TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "avisoRegistroRechazo")
    public List<AvisoRegistroRechazoPredio> getAvisoRegistroRechazoPredios() {
        return this.avisoRegistroRechazoPredios;
    }

    public void setAvisoRegistroRechazoPredios(
        List<AvisoRegistroRechazoPredio> avisoRegistroRechazoPredios) {
        this.avisoRegistroRechazoPredios = avisoRegistroRechazoPredios;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna el primero de la lista de AvisoRagistroRechazoPredio. Si la lista es nula o vacía
     * retorna un new. Se usa en al interfaz para poder pintar solo el primero.
     *
     * @return
     * @author pedro.garcia
     */
    @Transient
    public AvisoRegistroRechazoPredio getFirst() {

        AvisoRegistroRechazoPredio answer;

        if (this.avisoRegistroRechazoPredios != null && this.avisoRegistroRechazoPredios.size() > 0) {
            answer = this.avisoRegistroRechazoPredios.get(0);
        } else {
            answer = new AvisoRegistroRechazoPredio();
        }

        return answer;
    }

}
