package co.gov.igac.snc.util;

public enum ESeccionesCancelacionInscripcion {
    CONSTRUCCION_CONVENCIONAL("Construcción convencional"),
    CONSTRUCCION_NO_CONVENCIONAL("Construcción no convencional"),
    DATOS_UBICACION_PREDIO("Datos ubicación predio"),
    DETALLE_AVALUO("Detalle del avalúo"),
    DETALLE_UNIDAD_CONSTRUCCION("Detalle unidad de construcción"),
    DIRECCIONES_PREDIO("Direcciones del predio"),
    FICHA_MATRIZ("Ficha Matriz PH o condominio"),
    PROPIETARIOS_Y_POSEEDORES("Propietarios y poseedores"),
    REFERENCIAS_CARTOGRAFICAS("Referencias cartográficas del predio"),
    UNIDADES_CONSTRUCCION("Unidades de construcción");
    private String nombre;

    private ESeccionesCancelacionInscripcion(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}
