/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util.constantes;

/**
 *
 * Clase que contiene constantes usadas para la navegación web
 *
 * @author pedro.garcia
 *
 */
public class ConstantesNavegacionWeb {

    /*
     * nombre de la página en la que se realiza la actividad 'comisinar trámites de terreno'
     */
    public static final String COMISIONAR_TRAMITES_TERRENO = "manejoComisionesTramites";

    public static final String INDEX = "index";
    public static final String LISTA_TAREAS = "/tareas/listaTareas.jsf";
    public static final String VER_PROYECCION = "/conservacion/validacion/revisarProyeccion.jsf";

    public static final String RUTA_RELATIVA_CARPETA_IMAGENES_TEMPORALES = "/imagenesTemporales/";
    public static final String DEVOLVER_TRAMITE = "devolverTramite";
    public static final String PROYECTAR = "proyectar";
    public static final String TRAMITES_PARA_REALIZAR_EN_TERRENO = "tramitesParaRealizarEnTerreno";
    public static final String GESTIONAR_TRAMITE_NO_PROCEDE = "gestionarTramiteNoProcede";
    public static final String SOLICITAR_PRUEBAS = "solicitarPruebas";
    public static final String ASOCIAR_TRAMITES_NUEVOS = "asociarTramitesNuevos";
    public static final String ELABORAR_AUTO_DE_PRUEBAS = "elaborarAutoDePruebas";
    public static final String ENVIAR_A_DEPURACION_GEOGRAFICA = "enviarADepuracionGeografica";
    public static final String REGISTRAR_CORRECCIONES_A_PRODUCTO =
        "registrarCorreccionesAProductosEntregados";
    public static final String ESTABLECE_PROCEDENCIA_SOLICICTUD = "estableceProcedenciaSolicitud";
    public static final String ESTABLECE_PROCEDENCIA_SOLICICTUD_INDIVIDUAL =
        "estableceProcedenciaSolicitudIndividual";

}
