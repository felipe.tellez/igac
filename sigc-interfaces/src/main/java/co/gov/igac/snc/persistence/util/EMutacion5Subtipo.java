/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.persistence.util;

/**
 *
 * Enumeración para el dominio MUTACION_5_SUBTIPO
 *
 * @author pedro.garcia
 * @modified juan.agudelo adición del atributo nombre
 */
public enum EMutacion5Subtipo {

    //valor - código
    OMITIDO("OMITIDO", "Omitido"),
    NUEVO("NUEVO", "Nuevo");

    private String codigo;
    private String nombre;

    private EMutacion5Subtipo(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getNombre() {
        return nombre;
    }

}
