/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.util;

import java.io.Serializable;

/**
 * Clase que encapsula la informacion de consulta del servicio web de consulta catastral.
 *
 * @author andres.eslava
 */
public class FiltroConsultaCatastralWebService implements Serializable {

    private static final long serialVersionUID = 1698354015746003878L;

    private String Predio_numeroPredialNuevo;
    private String Predio_numeroPredialAnterior;
    private String Predio_matricula;

    private String Persona_tipoIdentificacion;
    private String Persona_numeroIdentificacion;
    private String Persona_razonSocial;
    private String Persona_primerApellido;
    private String Persona_segundoApellido;
    private String Persona_primerNombre;
    private String Persona_segundoNombre;

    private String PredioDireccion_direccion;

    private String Filtro_pagina;

    public FiltroConsultaCatastralWebService() {
    }

    public String getPredio_matricula() {
        return Predio_matricula;
    }

    public void setPredio_matricula(String Predio_matricula) {
        this.Predio_matricula = Predio_matricula;
    }

    public String getPersona_segundoApellido() {
        return Persona_segundoApellido;
    }

    public void setPersona_segundoApellido(String Persona_segundoApellido) {
        this.Persona_segundoApellido = Persona_segundoApellido;
    }

    public String getPersona_primerNombre() {
        return Persona_primerNombre;
    }

    public void setPersona_primerNombre(String Persona_primerNombre) {
        this.Persona_primerNombre = Persona_primerNombre;
    }

    public String getPersona_segundoNombre() {
        return Persona_segundoNombre;
    }

    public void setPersona_segundoNombre(String Persona_segundoNombre) {
        this.Persona_segundoNombre = Persona_segundoNombre;
    }

    public String getPredio_numeroPredialNuevo() {
        return Predio_numeroPredialNuevo;
    }

    public void setPredio_numeroPredialNuevo(String Predio_numeroPredialNuevo) {
        this.Predio_numeroPredialNuevo = Predio_numeroPredialNuevo;
    }

    public String getPredio_numeroPredialAnterior() {
        return Predio_numeroPredialAnterior;
    }

    public void setPredio_numeroPredialAnterior(String Predio_numeroPredialAnterior) {
        this.Predio_numeroPredialAnterior = Predio_numeroPredialAnterior;
    }

    public String getPersona_numeroIdentificacion() {
        return Persona_numeroIdentificacion;
    }

    public void setPersona_numeroIdentificacion(String Persona_numeroIdentificacion) {
        this.Persona_numeroIdentificacion = Persona_numeroIdentificacion;
    }

    public String getPersona_razonSocial() {
        return Persona_razonSocial;
    }

    public void setPersona_razonSocial(String Persona_razonSocial) {
        this.Persona_razonSocial = Persona_razonSocial;
    }

    public String getPersona_primerApellido() {
        return Persona_primerApellido;
    }

    public void setPersona_primerApellido(String Persona_primerApellido) {
        this.Persona_primerApellido = Persona_primerApellido;
    }

    public String getPersona_tipoIdentificacion() {
        return Persona_tipoIdentificacion;
    }

    public void setPersona_tipoIdentificacion(String Persona_tipoIdentificacion) {
        this.Persona_tipoIdentificacion = Persona_tipoIdentificacion;
    }

    public String getPredioDireccion_direccion() {
        return PredioDireccion_direccion;
    }

    public void setPredioDireccion_direccion(String PredioDireccion_direccion) {
        this.PredioDireccion_direccion = PredioDireccion_direccion;
    }

    public String getFiltro_pagina() {
        return Filtro_pagina;
    }

    public void setFiltro_pagina(String Filtro_pagina) {
        this.Filtro_pagina = Filtro_pagina;
    }

}
