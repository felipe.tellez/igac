package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * MODIFICACIONES PROPIAS A LA CLASE PPredioAvaluoCatastral:
 *
 * @author fabio.navarrete -> Se agregó el método y atributo transient para consultar el valor de la
 * propiedad cancelaInscribe -> Se agregó el método equals. Compara por ÚNICAMENTE por id para
 * determinar si es o no igual
 * @author juan.agudelo 27/05/11 Cambio Departamento y Municipio de String a Objetos y se les
 * permitio null metodos get y set para falsaTradicion2 para SiNo
 */
/*
 * @modified pedro.garcia 30-07-2012 adición de anotaciones @Temporal para los campos tipo fecha
 */
@Entity
@Table(name = "P_PERSONA_PREDIO_PROPIEDAD", schema = "SNC_CONSERVACION")
public class PPersonaPredioPropiedad implements java.io.Serializable, IProyeccionObject {

    /**
     *
     */
    private static final long serialVersionUID = 6524727808976781357L;
    // Fields

    private Long id;
    private PPersonaPredio PPersonaPredio;
    private String tipo;
    private String entidadEmisora;
    private Departamento departamento;
    private Municipio municipio;
    private String falsaTradicion;
    private Date fechaRegistro;
    private String numeroRegistro;
    private String modoAdquisicion;
    private Double valor;
    private String tipoTitulo;
    private String numeroTitulo;
    private Date fechaTitulo;
    private String libro;
    private String tomo;
    private String pagina;
    private String justificacion;
    private Documento documentoSoporte;
    private String cancelaInscribe;
    private Date fechaLog;
    private String usuarioLog;

    private String cancelaInscribeValor;

    // Constructors
    /** default constructor */
    public PPersonaPredioPropiedad() {
    }

    /** minimal constructor */
    public PPersonaPredioPropiedad(Long id, PPersonaPredio PPersonaPredio,
        String tipo, Date fechaLog, String usuarioLog) {
        this.id = id;
        this.PPersonaPredio = PPersonaPredio;
        this.tipo = tipo;
        this.fechaLog = fechaLog;
        this.usuarioLog = usuarioLog;
    }

    /** full constructor */
    public PPersonaPredioPropiedad(Long id, PPersonaPredio PPersonaPredio,
        String tipo, String entidadEmisora, Departamento departamento,
        Municipio municipio, String falsaTradicion, Date fechaRegistro,
        String numeroRegistro, String modoAdquisicion, Double valor,
        String tipoTitulo, String numeroTitulo, Date fechaTitulo,
        String libro, String tomo, String pagina, String justificacion,
        Documento documentoSoporte, String cancelaInscribe, Date fechaLog,
        String usuarioLog) {
        this.id = id;
        this.PPersonaPredio = PPersonaPredio;
        this.tipo = tipo;
        this.entidadEmisora = entidadEmisora;
        this.departamento = departamento;
        this.municipio = municipio;
        this.falsaTradicion = falsaTradicion;
        this.fechaRegistro = fechaRegistro;
        this.numeroRegistro = numeroRegistro;
        this.modoAdquisicion = modoAdquisicion;
        this.valor = valor;
        this.tipoTitulo = tipoTitulo;
        this.numeroTitulo = numeroTitulo;
        this.fechaTitulo = fechaTitulo;
        this.libro = libro;
        this.tomo = tomo;
        this.pagina = pagina;
        this.justificacion = justificacion;
        this.documentoSoporte = documentoSoporte;
        this.cancelaInscribe = cancelaInscribe;
        this.fechaLog = fechaLog;
        this.usuarioLog = usuarioLog;
    }

    /**
     * Copy Constructor
     *
     * @param personaPredioPropiedad
     */
    public PPersonaPredioPropiedad(
        PPersonaPredioPropiedad personaPredioPropiedad) {
        this.cancelaInscribe = personaPredioPropiedad.getCancelaInscribe();
        this.cancelaInscribeValor = personaPredioPropiedad.getCancelaInscribeValor();
        this.departamento = personaPredioPropiedad.getDepartamento();
        this.documentoSoporte = personaPredioPropiedad.getDocumentoSoporte();
        this.entidadEmisora = personaPredioPropiedad.getEntidadEmisora();
        this.falsaTradicion = personaPredioPropiedad.getFalsaTradicion();
        this.fechaLog = personaPredioPropiedad.getFechaLog();
        this.fechaRegistro = personaPredioPropiedad.getFechaRegistro();
        this.fechaTitulo = personaPredioPropiedad.getFechaTitulo();
        this.id = personaPredioPropiedad.getId();
        this.justificacion = personaPredioPropiedad.getJustificacion();
        this.libro = personaPredioPropiedad.getLibro();
        this.modoAdquisicion = personaPredioPropiedad.getModoAdquisicion();
        this.municipio = personaPredioPropiedad.getMunicipio();
        this.numeroRegistro = personaPredioPropiedad.getNumeroRegistro();
        this.numeroTitulo = personaPredioPropiedad.getNumeroTitulo();
        this.pagina = personaPredioPropiedad.getPagina();
        this.PPersonaPredio = personaPredioPropiedad.getPPersonaPredio();
        this.tipo = personaPredioPropiedad.getTipo();
        this.tipoTitulo = personaPredioPropiedad.getTipoTitulo();
        this.tomo = personaPredioPropiedad.getTomo();
        this.usuarioLog = personaPredioPropiedad.getUsuarioLog();
        this.valor = personaPredioPropiedad.getValor();
    }

    // Property accessors
    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSONA_PREDIO_PROPIEDA_ID_SEQ")
    @SequenceGenerator(name = "PERSONA_PREDIO_PROPIEDA_ID_SEQ", sequenceName =
        "PERSONA_PREDIO_PROPIEDA_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSONA_PREDIO_ID", nullable = false)
    public PPersonaPredio getPPersonaPredio() {
        return this.PPersonaPredio;
    }

    public void setPPersonaPredio(PPersonaPredio PPersonaPredio) {
        this.PPersonaPredio = PPersonaPredio;
    }

    @Column(name = "TIPO", nullable = false, length = 30)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "ENTIDAD_EMISORA", length = 100)
    public String getEntidadEmisora() {
        return this.entidadEmisora;
    }

    public void setEntidadEmisora(String entidadEmisora) {
        this.entidadEmisora = entidadEmisora;
    }

    @Column(name = "FALSA_TRADICION", length = 2)
    public String getFalsaTradicion() {
        return this.falsaTradicion;
    }

    public void setFalsaTradicion(String falsaTradicion) {
        this.falsaTradicion = falsaTradicion;
    }

    @Column(name = "FECHA_REGISTRO", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Column(name = "NUMERO_REGISTRO", length = 20)
    public String getNumeroRegistro() {
        return this.numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    @Column(name = "MODO_ADQUISICION", length = 30)
    public String getModoAdquisicion() {
        return this.modoAdquisicion;
    }

    public void setModoAdquisicion(String modoAdquisicion) {
        this.modoAdquisicion = modoAdquisicion;
    }

    @Column(name = "VALOR", precision = 18)
    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Column(name = "TIPO_TITULO", length = 30)
    public String getTipoTitulo() {
        return this.tipoTitulo;
    }

    public void setTipoTitulo(String tipoTitulo) {
        this.tipoTitulo = tipoTitulo;
    }

    @Column(name = "NUMERO_TITULO", length = 20)
    public String getNumeroTitulo() {
        return this.numeroTitulo;
    }

    public void setNumeroTitulo(String numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }

    @Column(name = "FECHA_TITULO", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaTitulo() {
        return this.fechaTitulo;
    }

    public void setFechaTitulo(Date fechaTitulo) {
        this.fechaTitulo = fechaTitulo;
    }

    @Column(name = "LIBRO", length = 20)
    public String getLibro() {
        return this.libro;
    }

    public void setLibro(String libro) {
        this.libro = libro;
    }

    @Column(name = "TOMO", length = 20)
    public String getTomo() {
        return this.tomo;
    }

    public void setTomo(String tomo) {
        this.tomo = tomo;
    }

    @Column(name = "PAGINA", length = 20)
    public String getPagina() {
        return this.pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    @Column(name = "JUSTIFICACION", length = 2000)
    public String getJustificacion() {
        return this.justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOPORTE_DOCUMENTO_ID", nullable = true)
    public Documento getDocumentoSoporte() {
        return this.documentoSoporte;
    }

    public void setDocumentoSoporte(Documento documentoSoporte) {
        this.documentoSoporte = documentoSoporte;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30, nullable = true)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_CODIGO", nullable = true)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = true)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Transient
    public boolean getFalsaTradicion2() {
        if (falsaTradicion != null) {
            return this.falsaTradicion.equals(ESiNo.SI.getCodigo());
        } else {
            return false;
        }
    }

    public void setFalsaTradicion2(boolean selected) {
        if (selected) {
            this.falsaTradicion = ESiNo.SI.getCodigo();
        } else {
            this.falsaTradicion = ESiNo.NO.getCodigo();
        }
    }

    @Transient
    public String getCancelaInscribeValor() {
        return cancelaInscribeValor;
    }

    public void setCancelaInscribeValor(String cancelaInscribeValor) {
        this.cancelaInscribeValor = cancelaInscribeValor;
    }

    @Override
    public boolean equals(Object o) {
        if (this.id == null || o == null ||
            !(o instanceof PPersonaPredioPropiedad)) {
            return false;
        }
        if (((PPersonaPredioPropiedad) o).getId() == null) {
            return false;
        }
        return this.id.equals(((PPersonaPredioPropiedad) o).getId());
    }

}
