package co.gov.igac.snc.fachadas;

import javax.ejb.Remote;

/**
 * Interfaz Remote para el acceso a los servicios de IActualizacion
 *
 * Nota: Los Métodos se declaran en la interfaz Local (IActualizacionLocal)
 *
 * @author juan.mendez
 * @version 2.0
 */
@Remote
public interface IActualizacion extends IActualizacionLocal {

}
