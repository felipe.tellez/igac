package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the AVALUO_PREDIO_CULTIVO database table.
 *
 */
@Entity
@Table(name = "AVALUO_PREDIO_CULTIVO")
public class AvaluoPredioCultivo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String aspectoTecnico;
    private Avaluo avaluo;
    private Long avaluoPredioId;
    private Double cantidad;
    private String descripcion;
    private Double edad;
    private Date fechaLog;
    private String tipo;
    private String unidadMedida;
    private String usuarioLog;
    private Double valorTotal;
    private Double valorUnidad;

    //Transients para mostrar información del predio
    private String numeroPredial;
    private String codigoMunicipio;
    private String codigoDepartamento;

    public AvaluoPredioCultivo() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_PREDIO_CULTIVO_ID_GENERATOR", sequenceName =
        "AVALUO_PREDIO_CULTIVO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "AVALUO_PREDIO_CULTIVO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ASPECTO_TECNICO")
    public String getAspectoTecnico() {
        return this.aspectoTecnico;
    }

    public void setAspectoTecnico(String aspectoTecnico) {
        this.aspectoTecnico = aspectoTecnico;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AVALUO_ID", nullable = false)
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    @Column(name = "AVALUO_PREDIO_ID")
    public Long getAvaluoPredioId() {
        return this.avaluoPredioId;
    }

    public void setAvaluoPredioId(Long avaluoPredioId) {
        this.avaluoPredioId = avaluoPredioId;
    }

    public Double getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getEdad() {
        return this.edad;
    }

    public void setEdad(Double edad) {
        this.edad = edad;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "UNIDAD_MEDIDA")
    public String getUnidadMedida() {
        return this.unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR_TOTAL")
    public Double getValorTotal() {
        return this.valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    @Column(name = "VALOR_UNIDAD")
    public Double getValorUnidad() {
        return this.valorUnidad;
    }

    public void setValorUnidad(Double valorUnidad) {
        this.valorUnidad = valorUnidad;
    }

    @Column(name = "TIPO")
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipoP) {
        this.tipo = tipoP;
    }

    @Transient
    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Transient
    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    @Transient
    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

}
