package co.gov.igac.snc.persistence.entity.tramite;

import co.gov.igac.snc.persistence.util.EComisionEstado;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 * Comision entity.
 */
/*
 *
 * @modified by: pedro.garcia. what: - cambio de tipo de datos TimeStamp a Date - cambio de tipo de
 * datos Set a List - adición de anotaciones para la secuencia del id - adición de anotación
 * @Temporal en los getters de campos tipo Date
 *
 * @modified by: javier.aponte: -	adición método constructor - Se agregarón los métodos Transient
 * getEjecutores y setEjecutores que sirven para obtener y establecer la lista de los nombres de los
 * funcionarios ejecutores de cada trámite asociado con la comisión
 *
 * @modified pedro.garcia adición de campo transient para los estados de una comisión
 * 'comisionEstados' 06-12-2013 adición de método transient isComisionCancelada
 */
@Entity
@Table(name = "COMISION", schema = "SNC_TRAMITE")
public class Comision implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1956227729553241947L;

    // Fields
    private Long id;
    private String numero;
    private String objeto;
    private Date fechaInicio;
    private Date fechaFin;
    private String placaVehiculo;
    private String conductor;
    private Double duracion;
    private String observaciones;
    private Double duracionAjuste;
    private String razonAjuste;
    private Double duracionTotal;
    private String estado;
    private Long memorandoDocumentoId;
    private String detalleInformeComision;
    private Long informeDocumentoId;
    private String tipo;
    private String usuarioLog;
    private Date fechaLog;
    private List<ComisionTramite> comisionTramites = new ArrayList<ComisionTramite>();

    private List<String> ejecutores;

    /**
     * atributo transient que guarda la lista de los ComisionEstado de una comisión
     */
    private List<ComisionEstado> comisionEstados;

    // Constructors
    /** default constructor */
    public Comision() {
    }

    /** minimal constructor */
    public Comision(Long id, Date fechaInicio, Date fechaFin,
        Double duracionAjuste, Double duracionTotal, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.duracionAjuste = duracionAjuste;
        this.duracionTotal = duracionTotal;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /**
     * ********************* NO BORRAR
     *
     ******************************
     * @author javier.aponte Se usa en un query. Tiene todos los campos que son obligatorios y los
     * que se necesitan mostrar
     */
    public Comision(Long id, String numero, Date fechaInicio, Date fechaFin, String estado,
        Double duracion, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.numero = numero;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.estado = estado;
        this.duracion = duracion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Comision(Long id, String numero, String objeto, Date fechaInicio,
        Date fechaFin, String placaVehiculo, String conductor,
        Double duracion, String observaciones, Double duracionAjuste,
        String razonAjuste, Double duracionTotal, String estado,
        Long memorandoDocumentoId, String detalleInformeComision,
        Long informeDocumentoId, String usuarioLog, Date fechaLog,
        List<ComisionTramite> comisionTramites) {
        this.id = id;
        this.numero = numero;
        this.objeto = objeto;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.placaVehiculo = placaVehiculo;
        this.conductor = conductor;
        this.duracion = duracion;
        this.observaciones = observaciones;
        this.duracionAjuste = duracionAjuste;
        this.razonAjuste = razonAjuste;
        this.duracionTotal = duracionTotal;
        this.estado = estado;
        this.memorandoDocumentoId = memorandoDocumentoId;
        this.detalleInformeComision = detalleInformeComision;
        this.informeDocumentoId = informeDocumentoId;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.comisionTramites = comisionTramites;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMISION_ID_SEQ")
    @SequenceGenerator(name = "COMISION_ID_SEQ", sequenceName = "COMISION_ID_SEQ", allocationSize =
        1)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NUMERO", length = 20)
    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "OBJETO", length = 2000)
    public String getObjeto() {
        return this.objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    @Column(name = "FECHA_INICIO", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Column(name = "FECHA_FIN", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Column(name = "PLACA_VEHICULO", length = 20)
    public String getPlacaVehiculo() {
        return this.placaVehiculo;
    }

    public void setPlacaVehiculo(String placaVehiculo) {
        this.placaVehiculo = placaVehiculo;
    }

    @Column(name = "CONDUCTOR", length = 100)
    public String getConductor() {
        return this.conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    @Column(name = "DURACION", precision = 6, scale = 1)
    public Double getDuracion() {
        return this.duracion;
    }

    public void setDuracion(Double duracion) {
        this.duracion = duracion;
    }

    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "DURACION_AJUSTE", nullable = false, precision = 6, scale = 1)
    public Double getDuracionAjuste() {
        return this.duracionAjuste;
    }

    public void setDuracionAjuste(Double duracionAjuste) {
        this.duracionAjuste = duracionAjuste;
    }

    @Column(name = "RAZON_AJUSTE", length = 2000)
    public String getRazonAjuste() {
        return this.razonAjuste;
    }

    public void setRazonAjuste(String razonAjuste) {
        this.razonAjuste = razonAjuste;
    }

    @Column(name = "DURACION_TOTAL", nullable = false, precision = 6, scale = 1)
    public Double getDuracionTotal() {
        return this.duracionTotal;
    }

    public void setDuracionTotal(Double duracionTotal) {
        this.duracionTotal = duracionTotal;
    }

    @Column(name = "ESTADO", length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "MEMORANDO_DOCUMENTO_ID", precision = 10, scale = 0)
    public Long getMemorandoDocumentoId() {
        return this.memorandoDocumentoId;
    }

    public void setMemorandoDocumentoId(Long memorandoDocumentoId) {
        this.memorandoDocumentoId = memorandoDocumentoId;
    }

    @Column(name = "DETALLE_INFORME_COMISION", length = 2000)
    public String getDetalleInformeComision() {
        return this.detalleInformeComision;
    }

    public void setDetalleInformeComision(String detalleInformeComision) {
        this.detalleInformeComision = detalleInformeComision;
    }

    @Column(name = "INFORME_DOCUMENTO_ID", precision = 10, scale = 0)
    public Long getInformeDocumentoId() {
        return this.informeDocumentoId;
    }

    public void setInformeDocumentoId(Long informeDocumentoId) {
        this.informeDocumentoId = informeDocumentoId;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "comision")
    public List<ComisionTramite> getComisionTramites() {
        return this.comisionTramites;
    }

    public void setComisionTramites(List<ComisionTramite> comisionTramites) {
        this.comisionTramites = comisionTramites;
    }

    @Column(name = "TIPO", length = 50)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipoComision) {
        this.tipo = tipoComision;
    }

//---------------------------------------------------------------------------------------------
    /**
     * @author javier.aponte
     */
    @Transient
    public List<String> getEjecutores() {
        return ejecutores;
    }

    public void setEjecutores(List<String> ejecutores) {
        this.ejecutores = ejecutores;
    }
//--------------------------------------------------------------------------------------------------

    @Transient
    public List<ComisionEstado> getComisionEstados() {
        return this.comisionEstados;
    }

    public void setComisionEstados(List<ComisionEstado> comisionEstados) {
        this.comisionEstados = comisionEstados;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna true si esta comisión tiene estado cancelada. </br>
     * Se usa en la interfaces para no quemar valores.
     *
     * @author pedro.garcia
     * @return
     */
    @Transient
    public boolean isComisionCancelada() {
        return this.estado.equals(EComisionEstado.CANCELADA.getCodigo());
    }

}
