package co.gov.igac.snc.persistence.entity.conservacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;

/**
 * MODIFICACIONES PROPIAS A LA CLASE UnidadConstruccion:
 *
 * @author fabio.navarrete -> Se agregó el serialVersionUID. -> Se agregó el campo tipificacionValor
 * para almacenar el valor del dominio.
 *
 * @modified juan.agudelo 21-03-12 Adición de la secuencia UNIDAD_CONSTRUCCION_ID_SEQ juan.agudelo
 * 18-04-12 adición de atributos transient unidadConstruccionCompsConv y
 * unidadConstruccionCompsNoConv
 *
 * @modified david.cifuentes :: Adición del atributo booleano 'selected' y los métodos transient
 * isSelected, y setSelected :: 21/09/2015
 */
@Entity
@NamedQueries({@NamedQuery(name = "findUnidadConstruccionByPredioANDUnidad", query =
        "from UnidadConstruccion unidad " +
        "where unidad.unidad = :uni AND " + "unidad.predio = :predio")})
@Table(name = "UNIDAD_CONSTRUCCION", schema = "SNC_CONSERVACION")
public class UnidadConstruccion implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 9003319305585221544L;

    // Fields
    private Long id;
    private Integer anioConstruccion;
    private Double areaConstruida;
    private Double avaluo;
    private Long calificacionAnexoId;
    private String descripcion;
    private Date fechaInscripcionCatastral;
    private String observaciones;
    private String pisoUbicacion;
    private Predio predio;
    private String tipificacion;
    private String tipoCalificacion;
    private String tipoConstruccion;
    private String tipoDominio;
    private Integer totalBanios;
    private Integer totalHabitaciones;
    private Integer totalLocales;
    private Integer totalPisosConstruccion;
    private Integer totalPisosUnidad;
    private Double totalPuntaje;
    private String unidad;
    private UsoConstruccion usoConstruccion;
    private Double valorM2Construccion;
    private String usuarioLog;
    private Date fechaLog;
    private List<UnidadConstruccionComp> unidadConstruccionComps =
        new ArrayList<UnidadConstruccionComp>();
    private Integer anioCancelacion;

    // Transient
    private String tipificacionValor;
    private List<UnidadConstruccionComp> unidadConstruccionCompsConv;
    private List<UnidadConstruccionComp> unidadConstruccionCompsNoConv;
    private boolean selected;
    private Double areaConstruidaRestante;

    // Constructors
    /** default constructor */
    public UnidadConstruccion() {
    }

    /** minimal constructor */
    public UnidadConstruccion(Long id, Predio predio,
        UsoConstruccion usoConstruccion, String unidad,
        Long calificacionAnexoId, String tipoConstruccion,
        String tipoCalificacion, Double totalPuntaje, String tipificacion,
        Integer anioConstruccion, Double areaConstruida, Double avaluo,
        Integer totalPisosUnidad, Integer totalPisosConstruccion,
        Double valorM2Construccion, String usuarioLog, Date fechaLog,
        String tipoDominio) {
        this.id = id;
        this.predio = predio;
        this.usoConstruccion = usoConstruccion;
        this.unidad = unidad;
        this.calificacionAnexoId = calificacionAnexoId;
        this.tipoConstruccion = tipoConstruccion;
        this.tipoCalificacion = tipoCalificacion;
        this.totalPuntaje = totalPuntaje;
        this.tipificacion = tipificacion;
        this.anioConstruccion = anioConstruccion;
        this.areaConstruida = areaConstruida;
        this.avaluo = avaluo;
        this.totalPisosUnidad = totalPisosUnidad;
        this.totalPisosConstruccion = totalPisosConstruccion;
        this.valorM2Construccion = valorM2Construccion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public UnidadConstruccion(Long id, Predio predio,
        UsoConstruccion usoConstruccion, String unidad,
        Long calificacionAnexoId, String tipoConstruccion,
        String descripcion, String observaciones, String tipoCalificacion,
        Double totalPuntaje, String tipificacion, Integer anioConstruccion,
        Double areaConstruida, Double avaluo, Integer totalBanios,
        Integer totalHabitaciones, Integer totalLocales, Integer totalPisosUnidad,
        Integer totalPisosConstruccion, Double valorM2Construccion,
        String pisoUbicacion, String usuarioLog, Date fechaLog,
        List<UnidadConstruccionComp> unidadConstruccionComps,
        String tipoDominio) {
        this.id = id;
        this.predio = predio;
        this.usoConstruccion = usoConstruccion;
        this.unidad = unidad;
        this.calificacionAnexoId = calificacionAnexoId;
        this.tipoConstruccion = tipoConstruccion;
        this.descripcion = descripcion;
        this.observaciones = observaciones;
        this.tipoCalificacion = tipoCalificacion;
        this.totalPuntaje = totalPuntaje;
        this.tipificacion = tipificacion;
        this.anioConstruccion = anioConstruccion;
        this.areaConstruida = areaConstruida;
        this.avaluo = avaluo;
        this.totalBanios = totalBanios;
        this.totalHabitaciones = totalHabitaciones;
        this.totalLocales = totalLocales;
        this.totalPisosUnidad = totalPisosUnidad;
        this.totalPisosConstruccion = totalPisosConstruccion;
        this.valorM2Construccion = valorM2Construccion;
        this.pisoUbicacion = pisoUbicacion;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.unidadConstruccionComps = unidadConstruccionComps;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UNIDAD_CONSTRUCCION_ID_SEQ")
    @SequenceGenerator(name = "UNIDAD_CONSTRUCCION_ID_SEQ", sequenceName =
        "UNIDAD_CONSTRUCCION_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USO_ID", nullable = false)
    public UsoConstruccion getUsoConstruccion() {
        return this.usoConstruccion;
    }

    public void setUsoConstruccion(UsoConstruccion usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    @Column(name = "UNIDAD", nullable = false, length = 50)
    public String getUnidad() {
        return this.unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    @Column(name = "CALIFICACION_ANEXO_ID", precision = 10, scale = 0)
    public Long getCalificacionAnexoId() {
        return this.calificacionAnexoId;
    }

    public void setCalificacionAnexoId(Long calificacionAnexoId) {
        this.calificacionAnexoId = calificacionAnexoId;
    }

    @Column(name = "TIPO_CONSTRUCCION", nullable = false, length = 2)
    public String getTipoConstruccion() {
        return this.tipoConstruccion;
    }

    public void setTipoConstruccion(String tipoConstruccion) {
        this.tipoConstruccion = tipoConstruccion;
    }

    @Column(name = "DESCRIPCION", length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "TIPO_CALIFICACION", nullable = false, length = 30)
    public String getTipoCalificacion() {
        return this.tipoCalificacion;
    }

    public void setTipoCalificacion(String tipoCalificacion) {
        this.tipoCalificacion = tipoCalificacion;
    }

    @Column(name = "TOTAL_PUNTAJE", nullable = false, precision = 10)
    public Double getTotalPuntaje() {
        return this.totalPuntaje;
    }

    public void setTotalPuntaje(Double totalPuntaje) {
        this.totalPuntaje = totalPuntaje;
    }

    @Column(name = "TIPIFICACION", nullable = false, length = 30)
    public String getTipificacion() {
        return this.tipificacion;
    }

    public void setTipificacion(String tipificacion) {
        this.tipificacion = tipificacion;
    }

    @Column(name = "ANIO_CONSTRUCCION", nullable = true, precision = 4, scale = 0)
    public Integer getAnioConstruccion() {
        return this.anioConstruccion;
    }

    public void setAnioConstruccion(Integer anioConstruccion) {
        this.anioConstruccion = anioConstruccion;
    }

    @Column(name = "AREA_CONSTRUIDA", nullable = false, precision = 18)
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "AVALUO", nullable = false, precision = 18)
    public Double getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Double avaluo) {
        this.avaluo = avaluo;
    }

    @Column(name = "TOTAL_BANIOS", precision = 2, scale = 0)
    public Integer getTotalBanios() {
        return this.totalBanios;
    }

    public void setTotalBanios(Integer totalBanios) {
        this.totalBanios = totalBanios;
    }

    @Column(name = "TOTAL_HABITACIONES", precision = 2, scale = 0)
    public Integer getTotalHabitaciones() {
        return this.totalHabitaciones;
    }

    public void setTotalHabitaciones(Integer totalHabitaciones) {
        this.totalHabitaciones = totalHabitaciones;
    }

    @Column(name = "TOTAL_LOCALES", precision = 2, scale = 0)
    public Integer getTotalLocales() {
        return this.totalLocales;
    }

    public void setTotalLocales(Integer totalLocales) {
        this.totalLocales = totalLocales;
    }

    @Column(name = "TOTAL_PISOS_UNIDAD", nullable = false, precision = 2, scale = 0)
    public Integer getTotalPisosUnidad() {
        return this.totalPisosUnidad;
    }

    public void setTotalPisosUnidad(Integer totalPisosUnidad) {
        this.totalPisosUnidad = totalPisosUnidad;
    }

    @Column(name = "TOTAL_PISOS_CONSTRUCCION", nullable = false, precision = 2, scale = 0)
    public Integer getTotalPisosConstruccion() {
        return this.totalPisosConstruccion;
    }

    public void setTotalPisosConstruccion(Integer totalPisosConstruccion) {
        this.totalPisosConstruccion = totalPisosConstruccion;
    }

    @Column(name = "VALOR_M2_CONSTRUCCION", nullable = false, precision = 18)
    public Double getValorM2Construccion() {
        return this.valorM2Construccion;
    }

    public void setValorM2Construccion(Double valorM2Construccion) {
        this.valorM2Construccion = valorM2Construccion;
    }

    @Column(name = "PISO_UBICACION", length = 2)
    public String getPisoUbicacion() {
        return this.pisoUbicacion;
    }

    public void setPisoUbicacion(String pisoUbicacion) {
        this.pisoUbicacion = pisoUbicacion;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "unidadConstruccion")
    public List<UnidadConstruccionComp> getUnidadConstruccionComps() {
        return this.unidadConstruccionComps;
    }

    public void setUnidadConstruccionComps(
        List<UnidadConstruccionComp> unidadConstruccionComps) {
        this.unidadConstruccionComps = unidadConstruccionComps;
    }

    @Column(name = "TIPO_DOMINIO", nullable = false, length = 30)
    public String getTipoDominio() {
        return this.tipoDominio;
    }

    public void setTipoDominio(String tipoDominio) {
        this.tipoDominio = tipoDominio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INSCRIPCION_CATASTRAL")
    public Date getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Date fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "ANIO_CANCELACION", nullable = true, precision = 4, scale = 0)
    public Integer getAnioCancelacion() {
        return this.anioCancelacion;
    }

    public void setAnioCancelacion(Integer anioCancel) {
        this.anioCancelacion = anioCancel;
    }

    // -------------------------------------------------------------------------
    @Transient
    public String getTipificacionValor() {
        return tipificacionValor;
    }

    public void setTipificacionValor(String tipificacionValor) {
        this.tipificacionValor = tipificacionValor;
    }

    // -------------------------------------------------------------------------
    @Transient
    public List<UnidadConstruccionComp> getUnidadConstruccionCompsConv() {

        if (unidadConstruccionComps != null &&
            !unidadConstruccionComps.isEmpty()) {
            unidadConstruccionCompsConv = new ArrayList<UnidadConstruccionComp>();

            for (UnidadConstruccionComp ucc : unidadConstruccionComps) {

                if (ucc.getUnidadConstruccion() != null &&
                    ucc.getUnidadConstruccion()
                        .getTipoConstruccion()
                        .equals(EUnidadTipoConstruccion.CONVENCIONAL
                            .getCodigo())) {

                    unidadConstruccionCompsConv.add(ucc);
                }
            }
        }

        return unidadConstruccionCompsConv;
    }

    @Transient
    public List<UnidadConstruccionComp> getUnidadConstruccionCompsNoConv() {

        if (unidadConstruccionComps != null &&
            !unidadConstruccionComps.isEmpty()) {
            unidadConstruccionCompsNoConv = new ArrayList<UnidadConstruccionComp>();

            for (UnidadConstruccionComp ucc : unidadConstruccionComps) {

                if (ucc.getUnidadConstruccion() != null &&
                    ucc.getUnidadConstruccion()
                        .getTipoConstruccion()
                        .equals(EUnidadTipoConstruccion.NO_CONVENCIONAL
                            .getCodigo())) {

                    unidadConstruccionCompsNoConv.add(ucc);
                }
            }
        }

        return unidadConstruccionCompsNoConv;
    }

    // -------------------------------------------------------------------//
    /**
     * Método que retorna si está o no seleccionada la PUnidadConstruccion
     */
    @Transient
    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     *
     * Transformación de una entidad histórica a un método en firme
     *
     * @author juan.cruz
     * @param entidadHistorica
     * @return
     */
    public static UnidadConstruccion parseUnidadConstruccion(HUnidadConstruccion hUnidadConstruccion) {
        UnidadConstruccion unidadConstruccion = new UnidadConstruccion();

        unidadConstruccion.setId(hUnidadConstruccion.getId().getId());
        unidadConstruccion.setPredio(hUnidadConstruccion.getPredio());
        unidadConstruccion.setUsoConstruccion(hUnidadConstruccion.getUsoConstruccion());
        unidadConstruccion.setUnidad(hUnidadConstruccion.getUnidad());
        unidadConstruccion.setCalificacionAnexoId(hUnidadConstruccion.getCalificacionAnexoId());
        unidadConstruccion.setTipoConstruccion(hUnidadConstruccion.getTipoConstruccion());
        unidadConstruccion.setDescripcion(hUnidadConstruccion.getDescripcion());
        unidadConstruccion.setObservaciones(hUnidadConstruccion.getObservaciones());
        unidadConstruccion.setTipoCalificacion(hUnidadConstruccion.getTipoCalificacion());
        unidadConstruccion.setTotalPuntaje(hUnidadConstruccion.getTotalPuntaje());
        unidadConstruccion.setTipificacion(hUnidadConstruccion.getTipificacion());
        unidadConstruccion.setTotalPuntaje(hUnidadConstruccion.getTotalPuntaje());
        unidadConstruccion.setTipificacion(hUnidadConstruccion.getTipificacion());
        unidadConstruccion.setAnioConstruccion(hUnidadConstruccion.getAnioConstruccion());
        unidadConstruccion.setAreaConstruida(hUnidadConstruccion.getAreaConstruida());
        unidadConstruccion.setAvaluo(hUnidadConstruccion.getAvaluo());
        unidadConstruccion.setTotalBanios(hUnidadConstruccion.getTotalBanios());
        unidadConstruccion.setTotalHabitaciones(hUnidadConstruccion.getTotalHabitaciones());
        unidadConstruccion.setTotalLocales(hUnidadConstruccion.getTotalLocales());
        unidadConstruccion.setTotalPisosUnidad(hUnidadConstruccion.getTotalPisosUnidad());
        unidadConstruccion.
            setTotalPisosConstruccion(hUnidadConstruccion.getTotalPisosConstruccion());
        unidadConstruccion.setValorM2Construccion(hUnidadConstruccion.getValorM2Construccion());
        unidadConstruccion.setPisoUbicacion(hUnidadConstruccion.getPisoUbicacion());
        unidadConstruccion.setUsuarioLog(hUnidadConstruccion.getUsuarioLog());
        unidadConstruccion.setFechaLog(hUnidadConstruccion.getFechaLog());

        List<HUnidadConstruccionComp> hUnidadConstruccionComps = hUnidadConstruccion.
            getHUnidadConstruccionComps();
        List<UnidadConstruccionComp> unidadConstruccionComps =
            new ArrayList<UnidadConstruccionComp>();
        if (hUnidadConstruccionComps != null) {
            for (HUnidadConstruccionComp unaHUnidadConstruccionComp : hUnidadConstruccionComps) {
                unidadConstruccionComps.add(UnidadConstruccionComp.parseUnidadConstruccionComp(
                    unaHUnidadConstruccionComp));
            }
        }
        unidadConstruccion.setUnidadConstruccionComps(unidadConstruccionComps);
        unidadConstruccion.setAnioCancelacion(hUnidadConstruccion.getAnioCancelacion());

        return unidadConstruccion;
    }

    // -------------------------------------------------------------------//
	/**
	 * Método que retorna el área construida restante de una construcción vigente,
	 * cuya área previa se encuentra asociada a diferentes {@link PUnidadConstruccion}
	 */
	@Transient
	public Double getAreaConstruidaRestante() {
		if(this.areaConstruidaRestante == null){
			return this.areaConstruida;
		}
		return this.areaConstruidaRestante;
	}

	public void setAreaConstruidaRestante(Double areaConstruidaRestante) {
		this.areaConstruidaRestante = areaConstruidaRestante;
    }

}
