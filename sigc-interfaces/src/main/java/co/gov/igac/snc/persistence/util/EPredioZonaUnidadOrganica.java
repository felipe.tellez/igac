package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los código del dominio PREDIO_ZONA_UNIDAD_ORGANICA
 *
 * @author david.cifuentes
 */
public enum EPredioZonaUnidadOrganica {

    RURAL("00"),
    URBANA("01");

    private String codigo;

    private EPredioZonaUnidadOrganica(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }
}
