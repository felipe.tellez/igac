package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @description Filtro de datos para la consulta de reportes de radicación
 * @version 1.0
 * @author david.cifuentes
 *
 */
public class FiltroDatosConsultaReportesRadicacion implements Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -8191143239137302418L;

    private Long tipoReporteId;
    private String formato;
    private String territorialCodigo;
    private String UOCCodigo;
    private String departamentoCodigo;
    private String municipioCodigo;
    private Date fechaInicio;
    private Date fechaFin;
    private String rol;
    private String funcionario;
    private String subproceso;
    private String actividad;
    private String clasificacion;
    private String diasTranscurridos;
    private String codigoReporte;

    /**
     * Default constructor
     */
    public FiltroDatosConsultaReportesRadicacion() {
    }

    public Long getTipoReporteId() {
        return tipoReporteId;
    }

    public void setTipoReporteId(Long tipoReporteId) {
        this.tipoReporteId = tipoReporteId;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getTerritorialCodigo() {
        return territorialCodigo;
    }

    public void setTerritorialCodigo(String territorialCodigo) {
        this.territorialCodigo = territorialCodigo;
    }

    public String getUOCCodigo() {
        return UOCCodigo;
    }

    public void setUOCCodigo(String uOCCodigo) {
        UOCCodigo = uOCCodigo;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getCodigoReporte() {
        return codigoReporte;
    }

    public void setCodigoReporte(String codigoReporte) {
        this.codigoReporte = codigoReporte;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public String getSubproceso() {
        return subproceso;
    }

    public void setSubproceso(String subproceso) {
        this.subproceso = subproceso;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getDiasTranscurridos() {
        return diasTranscurridos;
    }

    public void setDiasTranscurridos(String diasTranscurridos) {
        this.diasTranscurridos = diasTranscurridos;
    }

    public Date getDiasTranscurridosDesde() {
        if (this.diasTranscurridos != null) {
            String[] desde = this.diasTranscurridos.split("-");
            int diasDesde = new Integer(desde[0]) * -1;
            Calendar hoy = Calendar.getInstance();
            hoy.add(Calendar.DATE, diasDesde);
            return hoy.getTime();
        }
        return new Date();
    }

    public Date getDiasTranscurridosHasta() {
        if (this.diasTranscurridos != null) {
            String[] hasta = this.diasTranscurridos.split("-");
            int diasHasta = new Integer(hasta[1]) * -1;
            Calendar hoy = Calendar.getInstance();
            hoy.add(Calendar.DATE, diasHasta);
            return hoy.getTime();
        }
        return new Date();
    }

}
