package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * The persistent class for the PROFESIONAL_AVALUOS_CONTRATO database table.
 */
/*
 * @modified felipe.cadena - se cambian el tipo de dato BigDecimal a Long en los campos
 * contratoDocumentoId y anio. - se cambian el tipo de dato BigDecimal a Double en el campo valor
 * @modified christian.rodriguez agregado atributo profesionalContratoAdicions @modified
 * christian.rodriguez agregado atributo profesionalContratoCesions @modified christian.rodriguez
 * agregado método transient para obtener el valor total de las adiciones del contrato @modified
 * christian.rodriguez agregado método transient para obtener el valor total de las pagos de salud
 * del contrato @modified christian.rodriguez agregado método transient para obtener el valor total
 * de las pagos de pensión del contrato @modified christian.rodriguez agregado método transient para
 * obtener el valor total de los pagos causados
 *
 * @modified pedro.garcia - adición de método isContratoActivo
 *
 * @modified christian.rodriguez se cambia el tipo de dato del interventor de String a
 * ProfesionalAvaluo
 */
@Entity
@Table(name = "PROFESIONAL_AVALUOS_CONTRATO")
public class ProfesionalAvaluosContrato implements Serializable {

    private static final long serialVersionUID = 2166762775171987252L;

    private Long id;
    private String activo;
    private Long anio;
    private String conActaFinalizacion;
    private Long contratoDocumentoId;
    private String contratoNumero;
    private Date fechaDesde;
    private Date fechaHasta;
    private Date fechaLog;
    private String generaIva;
    private ProfesionalAvaluo interventor;
    private ProfesionalAvaluo profesionalAvaluo;
    private String usuarioLog;
    private Double valor;

    private List<ProfesionalContratoPago> profesionalContratoPagos;
    private List<ProfesionalContratoAdicion> profesionalContratoAdicions;
    private List<ProfesionalContratoCesion> profesionalContratoCesions;

    public ProfesionalAvaluosContrato() {
    }

    @Id
    @SequenceGenerator(name = "PROFESIONAL_AVALUOS_CONTRATO_ID_GENERATOR", sequenceName =
        "PROFESIONAL_AVALUOS_CON_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "PROFESIONAL_AVALUOS_CONTRATO_ID_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false, precision = 10)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ACTIVO", nullable = false, length = 2)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "ANIO", nullable = false, precision = 4)
    public Long getAnio() {
        return this.anio;
    }

    public void setAnio(Long anio) {
        this.anio = anio;
    }

    @Column(name = "CON_ACTA_FINALIZACION", nullable = false, length = 2)
    public String getConActaFinalizacion() {
        return this.conActaFinalizacion;
    }

    public void setConActaFinalizacion(String conActaFinalizacion) {
        this.conActaFinalizacion = conActaFinalizacion;
    }

    @Column(name = "CONTRATO_DOCUMENTO_ID", precision = 10)
    public Long getContratoDocumentoId() {
        return this.contratoDocumentoId;
    }

    public void setContratoDocumentoId(Long contratoDocumentoId) {
        this.contratoDocumentoId = contratoDocumentoId;
    }

    @Column(name = "CONTRATO_NUMERO", nullable = false, length = 50)
    public String getContratoNumero() {
        return this.contratoNumero;
    }

    public void setContratoNumero(String contratoNumero) {
        this.contratoNumero = contratoNumero;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DESDE")
    public Date getFechaDesde() {
        return this.fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_HASTA")
    public Date getFechaHasta() {
        return this.fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "GENERA_IVA", nullable = false, length = 2)
    public String getGeneraIva() {
        return this.generaIva;
    }

    public void setGeneraIva(String generaIva) {
        this.generaIva = generaIva;
    }

    // bi-directional many-to-one association to ProfesionalAvaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INTERV_PROFESIONAL_AVALUOS_ID", nullable = false)
    public ProfesionalAvaluo getInterventor() {
        return this.interventor;
    }

    public void setInterventor(ProfesionalAvaluo interventor) {
        this.interventor = interventor;
    }

    //bi-directional many-to-one association to ProfesionalAvaluo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESIONAL_AVALUOS_ID", nullable = false)
    public ProfesionalAvaluo getProfesionalAvaluo() {
        return this.profesionalAvaluo;
    }

    public void setProfesionalAvaluo(ProfesionalAvaluo profesionalAvaluo) {
        this.profesionalAvaluo = profesionalAvaluo;
    }

    @Column(name = "USUARIO_LOG", length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VALOR", nullable = false, precision = 10)
    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    //bi-directional many-to-one association to ProfesionalContratoPago
    @OneToMany(mappedBy = "profesionalAvaluosContrato")
    public List<ProfesionalContratoPago> getProfesionalContratoPagos() {
        return this.profesionalContratoPagos;
    }

    public void setProfesionalContratoPagos(List<ProfesionalContratoPago> profesionalContratoPagos) {
        this.profesionalContratoPagos = profesionalContratoPagos;
    }

    // bi-directional many-to-one association to ProfesionalContratoAdicion
    @OneToMany(mappedBy = "profesionalAvaluosContrato")
    public List<ProfesionalContratoAdicion> getProfesionalContratoAdicions() {
        return this.profesionalContratoAdicions;
    }

    public void setProfesionalContratoAdicions(
        List<ProfesionalContratoAdicion> profesionalContratoAdicions) {
        this.profesionalContratoAdicions = profesionalContratoAdicions;
    }

    // bi-directional many-to-one association to ProfesionalContratoCesion
    @OneToMany(mappedBy = "profesionalAvaluosContrato")
    public List<ProfesionalContratoCesion> getProfesionalContratoCesions() {
        return this.profesionalContratoCesions;
    }

    public void setProfesionalContratoCesions(
        List<ProfesionalContratoCesion> profesionalContratoCesions) {
        this.profesionalContratoCesions = profesionalContratoCesions;
    }

    /**
     * Método para indicar si el contrato tiene un acta de finalización
     *
     * @author rodrigo.hernandez
     *
     * @return -
     * <b>true</b> si el contrato tiene un acta de finalización</br>
     * <b>false</b> si el contrato <b>NO</b> tiene un acta de finalización</br>
     */
    @Transient
    public boolean getTieneActaFinalizacion() {
        boolean result = true;

        if (this.conActaFinalizacion.equals(ESiNo.NO.getCodigo())) {
            result = false;
        }

        return result;
    }

    /**
     * Método que calcula la fecha de terminación del contrato del profesional, corresponde a la
     * mayor fecha entre la fecha de finalizaicón del contrato y las fechas de las adiciones
     * realizadas
     *
     * @author christian.rodriguez
     * @return fehc ade terminación del contrato
     */
    @Transient
    public Date getFechaTerminacion() {
        Date fechaTerminacion = this.fechaHasta;
        if (this.profesionalContratoAdicions != null &&
            Hibernate.isInitialized(this.profesionalContratoAdicions.isEmpty())) {

            for (ProfesionalContratoAdicion adicionContrato : this.profesionalContratoAdicions) {
                if (fechaTerminacion.before(adicionContrato.getFechaHasta())) {
                    fechaTerminacion = adicionContrato.getFechaHasta();
                }
            }
        }
        return fechaTerminacion;
    }

    /**
     * Método que calcula el total de las adiciones del contrato
     *
     * @author christian.rodriguez
     * @return valor total de las adiciones del contrato
     */
    @Transient
    public Double getValorTotalAdiciones() {
        Double totalAdiciones = 0.0;
        if (this.profesionalContratoAdicions != null &&
            Hibernate.isInitialized(this.profesionalContratoAdicions.isEmpty())) {

            for (ProfesionalContratoAdicion adicionContrato : this.profesionalContratoAdicions) {
                totalAdiciones += adicionContrato.getValor();
            }
        }
        return totalAdiciones;
    }

    /**
     * Método que calcula el total de los pagos de pension
     *
     * @author christian.rodriguez
     * @return valor total de los pagos de pension
     */
    @Transient
    public Double getValorTotalPagosPension() {
        Double totalPagosPension = 0.0;
        if (this.profesionalContratoPagos != null &&
            Hibernate.isInitialized(this.profesionalContratoPagos)) {

            for (ProfesionalContratoPago pagoContrato : this.profesionalContratoPagos) {
                totalPagosPension += pagoContrato.getValorPagarPension();
            }
        }
        return totalPagosPension;
    }

    /**
     * Método que calcula el total de los pagos de pension
     *
     * @author christian.rodriguez
     * @return valor total de los pagos de pension
     */
    @Transient
    public Double getValorTotalPagosSalud() {
        Double totalPagosSalud = 0.0;

        if (this.profesionalContratoPagos != null &&
            Hibernate.isInitialized(this.profesionalContratoPagos)) {

            for (ProfesionalContratoPago pagoContrato : this.profesionalContratoPagos) {
                totalPagosSalud += pagoContrato.getValorPagarSalud();
            }
        }
        return totalPagosSalud;
    }

    /**
     * Método que calcula el total del contrato con adiciones
     *
     * @author christian.rodriguez
     * @return valor total del contrato con adiciones
     */
    @Transient
    public Double getValorTotalConAdiciones() {
        Double valortotalConAdiciones = 0.0;

        if (this.getValor() != null) {
            valortotalConAdiciones += this.getValor();
        }

        if (this.getValorTotalAdiciones() != null) {
            valortotalConAdiciones += this.getValorTotalAdiciones();
        }

        return valortotalConAdiciones;
    }

    /**
     * Método que calcula el total de los pagos causados
     *
     * @author christian.rodriguez
     * @return valor total de los pagos causados
     */
    @Transient
    public Double getValorTotalPagosCausados() {
        Double totalPagosCausados = 0.0;

        if (this.profesionalContratoPagos != null &&
            Hibernate.isInitialized(this.profesionalContratoPagos)) {

            for (ProfesionalContratoPago pagoContrato : this.profesionalContratoPagos) {
                if (pagoContrato.isCausado()) {
                    totalPagosCausados += pagoContrato.getValorPagado();
                }
            }
        }
        return totalPagosCausados;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna true si este contrato está marcado como activo
     *
     * @author pedro.garcia
     */
    @Transient
    //D: OJO: este era el que generaba error cuando se llamaba isActivo. Supongo que era porque al
    //        llamarse así, hacía suponer a la JVM que el atributo 'activo' es un boolean, cuando en
    //        realidad es un String. Parece que no se hacía caso de la anotación 'transient'
    public boolean isContratoActivo() {

        if (this.activo.compareTo(ESiNo.SI.getCodigo()) == 0) {
            return true;
        } else {
            return false;
        }
    }

}
