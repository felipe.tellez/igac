package co.gov.igac.snc.util;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.excepciones.ExcepcionSNC;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * Clase utilitaria para la lectura del archivo .properties con la configuración para SNC
 *
 * @author felipe.cadena
 *
 */
public class SNCPropertiesUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SNCPropertiesUtil.class);

    private static SNCPropertiesUtil instance;

    private ResourceBundle rs;
    private Properties prop;

    /**
     *
     */
    private SNCPropertiesUtil() {
        try {
            //rs = ResourceBundle.getBundle("application");
            prop = new Properties();
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream stream = loader.getResourceAsStream("application.properties");
            prop.load(stream);
            LOGGER.debug("Properties loads OK!");

        } catch (Exception e) {
            LOGGER.debug("Error cargando properties");
        }

    }

    public static synchronized SNCPropertiesUtil getInstance()
        throws ExcepcionSNC {
        if (instance == null) {
            instance = new SNCPropertiesUtil();
        }
        return instance;
    }

    /**
     *
     * @param propertyName
     * @return
     */
    public String getProperty(String propertyName) {
        String propertyValue = null;
        try {
            propertyValue = prop.getProperty(propertyName);
            LOGGER.debug("Propertie value : " + propertyValue);
        } catch (Exception e) {
            LOGGER.debug("Error cargando properties");
        }
        return propertyValue;
    }

}
