package co.gov.igac.snc.util;

/**
 * Lista las posibles inconsistencias geograficas que pueda tener un predio de la bd alfanumerica.
 *
 * @author felipe.cadena
 *
 */
public enum EInconsistenciasGeograficas {

    MEJORA_SIN_TERRENO("Mejora sin predio de terreno asociado", "5"),
    NO_MANZANA("Predio sin manzana o vereda asociado", "4"),
    NO_ZONAS("Predio sin zonas", "204"),
    NO_PREDIO("Numero predial sin polígono asociado", "39"),
    NUMERO_PREDIAL_REPETIDO("Número de predio repetido", "1"),
    NO_FEATURE("No existe el feature en la bd", "300"),
    //valor que se registra en la tabla inconsistencias cuando no existe ninguna asociada al tramite
    NO_INCONSISTENCIA("No se encuentran inconsistencias geográficas", "0");

    private String nombre;
    private String codigoSig;

    EInconsistenciasGeograficas(String nombre, String codigoSig) {
        this.nombre = nombre;
        this.codigoSig = codigoSig;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getCodigoSig() {
        return this.codigoSig;
    }

}
