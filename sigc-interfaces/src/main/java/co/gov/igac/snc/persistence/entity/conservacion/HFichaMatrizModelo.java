package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * The persistent class for the H_FICHA_MATRIZ_MODELO database table.
 *
 *
 * @modified david.cifuentes :: Adicion del campo descripcion :: 20/04/2012 Eliminación del campo
 * usoId por cambio en el modelo de datos :: 20/04/2012 Eliminación del campo tipoCalificacion por
 * cambio en el modelo de datos :: 25/04/2012
 */
@Entity
@Table(name = "H_FICHA_MATRIZ_MODELO")
public class HFichaMatrizModelo implements Serializable {

    private static final long serialVersionUID = 1L;
    private HFichaMatrizModeloPK id;
    private BigDecimal fichaMatrizId;
    private Double areaConstruida;
    private Double areaTerreno;
    private String cancelaInscribe;
    private Date fechaLog;
    private String nombre;
    private Integer numeroUnidades;
    private Long planoDocumentoId;
    private String usuarioLog;
    private HPredio HPredio;
    private String descripcion;
    private List<HFmModeloConstruccion> HFmModeloConstruccions;
    private HFichaMatriz HFichaMatriz;
    private String estado;

    public HFichaMatrizModelo() {
    }

    @EmbeddedId
    public HFichaMatrizModeloPK getId() {
        return this.id;
    }

    public void setId(HFichaMatrizModeloPK id) {
        this.id = id;
    }

    @Column(name = "AREA_CONSTRUIDA")
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "AREA_TERRENO")
    public Double getAreaTerreno() {
        return this.areaTerreno;
    }

    public void setAreaTerreno(Double areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "FICHA_MATRIZ_ID")
    public BigDecimal getFichaMatrizId() {
        return this.fichaMatrizId;
    }

    public void setFichaMatrizId(BigDecimal fichaMatrizId) {
        this.fichaMatrizId = fichaMatrizId;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "NUMERO_UNIDADES")
    public Integer getNumeroUnidades() {
        return this.numeroUnidades;
    }

    public void setNumeroUnidades(Integer numeroUnidades) {
        this.numeroUnidades = numeroUnidades;
    }

    @Column(name = "PLANO_DOCUMENTO_ID")
    public Long getPlanoDocumentoId() {
        return this.planoDocumentoId;
    }

    public void setPlanoDocumentoId(Long planoDocumentoId) {
        this.planoDocumentoId = planoDocumentoId;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @ManyToOne
    @JoinColumn(name = "H_PREDIO_ID", insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @Column(length = 600)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Transient
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HFichaMatrizModelo")
    public List<HFmModeloConstruccion> getHFmModeloConstruccions() {
        return HFmModeloConstruccions;
    }

    public void setHFmModeloConstruccions(List<HFmModeloConstruccion> HFmModeloConstruccions) {
        this.HFmModeloConstruccions = HFmModeloConstruccions;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "H_PREDIO_ID", referencedColumnName = "H_PREDIO_ID", nullable = false,
            insertable = false, updatable = false),
        @JoinColumn(name = "FICHA_MATRIZ_ID", referencedColumnName = "ID", nullable = false,
            insertable = false, updatable = false)})
    public HFichaMatriz getHFichaMatriz() {
        return HFichaMatriz;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setHFichaMatriz(HFichaMatriz HFichaMatriz) {
        this.HFichaMatriz = HFichaMatriz;
    }

}
