package co.gov.igac.snc.persistence.entity.generales;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the CONFIGURACION_REPORTE database table.
 *
 */
@Entity
@Table(name = "CONFIGURACION_REPORTE")
public class ConfiguracionReporte implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2945336019511217654L;
    private Long id;
    private String definicionXml;
    private Date fechaLog;
    private String nombre;
    private String usuarioLog;

    public ConfiguracionReporte() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONFIGURACION_REPORTE_ID_SEQ")
    @SequenceGenerator(name = "CONFIGURACION_REPORTE_ID_SEQ", sequenceName =
        "CONFIGURACION_REPORTE_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DEFINICION_XML")
    public String getDefinicionXml() {
        return this.definicionXml;
    }

    public void setDefinicionXml(String definicionXml) {
        this.definicionXml = definicionXml;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NOMBRE", length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
