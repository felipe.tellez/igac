/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.util;

import java.io.Serializable;
import java.util.Date;

/**
 * Función que realiza la lógica para determinar si un predio o una persona se encuentran bloqueados
 * hoy ademas de la funcionalidad para mapear los parametros que consume el query
 *
 * @author juan.agudelo
 *
 */
public class FuncionPredioPersonaBloqueoHoy implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1009744746663894393L;

    /**
     * Método que retorna la logica del query para determinar si un bloqueo es valido hoy
     *
     * @author juan.agudelo
     */
    private static final String queryPredioPersonaBloqueoHoy =
        "((pb.fechaInicioBloqueo<= :fechaActual " +
        " AND pb.fechaTerminaBloqueo> :fechaActual" +
        " AND pb.fechaDesbloqueo IS null)" +
        " OR (pb.fechaInicioBloqueo<= :fechaActual " +
        " AND pb.fechaTerminaBloqueo IS null" +
        " AND pb.fechaDesbloqueo IS null)" +
        " OR (pb.fechaInicioBloqueo<= :fechaActual " +
        " AND pb.fechaDesbloqueo> :fechaActual))";

    /**
     * Método que retorna el nombre del parametro que consume el query cadenaPredioPersonaBloqueoHoy
     *
     * @author juan.agudelo
     */
    private static final String nameParameterPredioPersonaBloqueoHoy = "fechaActual";

    /**
     * Método que retorna el parametro que consume el query cadenaPredioPersonaBloqueoHoy
     *
     * @author juan.agudelo
     */
    private static final Date parameterPredioPersonaBloqueoHoy = new Date(
        System.currentTimeMillis());

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public static String getQueryprediopersonabloqueohoy() {
        return queryPredioPersonaBloqueoHoy;
    }

    public static String getNameparameterprediopersonabloqueohoy() {
        return nameParameterPredioPersonaBloqueoHoy;
    }

    public static Date getParameterprediopersonabloqueohoy() {
        return parameterPredioPersonaBloqueoHoy;
    }

}
