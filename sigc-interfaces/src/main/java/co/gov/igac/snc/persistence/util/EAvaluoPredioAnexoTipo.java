package co.gov.igac.snc.persistence.util;

/**
 * Enumeracion para el dominio AVALUO_PREDIO_ANEXO_TIPO
 *
 * @author felipe.cadena
 */
public enum EAvaluoPredioAnexoTipo implements ECampoDetalleAvaluo {

    ENRAMADA("1", "Enramada"),
    ESTABLO("2", "Establo");

    private String codigo;
    private String valor;

    private EAvaluoPredioAnexoTipo(String codigo, String valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

    @Override
    public String getValor() {
        return valor;
    }
}
