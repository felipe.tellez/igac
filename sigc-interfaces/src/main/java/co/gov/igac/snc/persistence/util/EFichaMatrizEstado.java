package co.gov.igac.snc.persistence.util;

/**
 * Enumeración con los código del dominio CANCELACION_MASIVA_ESTADO
 *
 * @author leidy.gonzalez
 */
public enum EFichaMatrizEstado {

    ACTIVO("ACTIVO"),
    CANCELADO("CANCELADO");

    private String codigo;

    private EFichaMatrizEstado(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
