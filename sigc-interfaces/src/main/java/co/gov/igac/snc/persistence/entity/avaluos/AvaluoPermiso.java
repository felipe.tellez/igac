package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.persistence.entity.generales.Municipio;

/**
 * The persistent class for the AVALUO_PERMISO database table.
 *
 */
@Entity
@Table(name = "AVALUO_PERMISO")
public class AvaluoPermiso implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Long dias;
    private Date fechaDesde;
    private Date fechaHasta;
    private Date fechaLog;
    private ProfesionalAvaluo profesionalAvaluos;
    private String usuarioLog;
    private Avaluo avaluo;

    public AvaluoPermiso() {
    }

    @Id
    @SequenceGenerator(name = "AVALUO_PERMISO_ID_GENERATOR", sequenceName = "AVALUO_PERMISO_ID_SEQ",
        allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AVALUO_PERMISO_ID_GENERATOR")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDias() {
        return this.dias;
    }

    public void setDias(Long dias) {
        this.dias = dias;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_DESDE")
    public Date getFechaDesde() {
        return this.fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_HASTA")
    public Date getFechaHasta() {
        return this.fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESIONAL_AVALUOS_ID")
    public ProfesionalAvaluo getProfesionalAvaluos() {
        return this.profesionalAvaluos;
    }

    public void setProfesionalAvaluos(ProfesionalAvaluo profesionalAvaluos) {
        this.profesionalAvaluos = profesionalAvaluos;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to Avaluo
    @ManyToOne
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

}
