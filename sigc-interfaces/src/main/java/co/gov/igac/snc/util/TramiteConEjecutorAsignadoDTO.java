package co.gov.igac.snc.util;

/**
 * @author javier.aponte Clase utilizada para tener los datos de trámites con ejecutor asignado, se
 * usa en la actividad de asignar trámites
 */
public class TramiteConEjecutorAsignadoDTO implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4045674075371131175L;

    private Long id;
    private String clasificacion;
    private String funcionarioEjecutor;

    // Constructors
    /** minimo */
    public TramiteConEjecutorAsignadoDTO() {
    }

    /** full constructor */
    public TramiteConEjecutorAsignadoDTO(Long id, String clasificacion,
        String funcionarioEjecutor) {
        this.id = id;
        this.clasificacion = clasificacion;
        this.funcionarioEjecutor = funcionarioEjecutor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getFuncionarioEjecutor() {
        return funcionarioEjecutor;
    }

    public void setFuncionarioEjecutor(String funcionarioEjecutor) {
        this.funcionarioEjecutor = funcionarioEjecutor;
    }

}
