package co.gov.igac.snc.persistence.entity.avaluos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CONTROL_CALIDAD_RECOLECTOR database table.
 *
 * @author rodrigo.hernandez
 */
@Entity
@Table(name = "CONTROL_CALIDAD_RECOLECTOR")
public class ControlCalidadRecolector implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private Date fechaLog;
    private Double mediaMuestralVariable;
    private Double ponderadoOfertas;
    private String recolectorId;
    private Double totalOfertasMuestra;
    private Double totalOfertasRecolectadas;
    private String usuarioLog;
    private Double varianzaPoblacionalVariable;
    private ControlCalidad controlCalidad;

    public ControlCalidadRecolector() {
    }

    @Id
    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTROL_CALIDAD_RECOLEC_ID_SEQ")
    @SequenceGenerator(name = "CONTROL_CALIDAD_RECOLEC_ID_SEQ", sequenceName =
        "CONTROL_CALIDAD_RECOLEC_ID_SEQ", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "MEDIA_MUESTRAL_VARIABLE")
    public Double getMediaMuestralVariable() {
        return this.mediaMuestralVariable;
    }

    public void setMediaMuestralVariable(Double mediaMuestralVariable) {
        this.mediaMuestralVariable = mediaMuestralVariable;
    }

    @Column(name = "PONDERADO_OFERTAS")
    public Double getPonderadoOfertas() {
        return this.ponderadoOfertas;
    }

    public void setPonderadoOfertas(Double ponderadoOfertas) {
        this.ponderadoOfertas = ponderadoOfertas;
    }

    @Column(name = "RECOLECTOR_ID")
    public String getRecolectorId() {
        return this.recolectorId;
    }

    public void setRecolectorId(String recolectorId) {
        this.recolectorId = recolectorId;
    }

    @Column(name = "TOTAL_OFERTAS_MUESTRA")
    public Double getTotalOfertasMuestra() {
        return this.totalOfertasMuestra;
    }

    public void setTotalOfertasMuestra(Double totalOfertasMuestra) {
        this.totalOfertasMuestra = totalOfertasMuestra;
    }

    @Column(name = "TOTAL_OFERTAS_RECOLECTADAS")
    public Double getTotalOfertasRecolectadas() {
        return this.totalOfertasRecolectadas;
    }

    public void setTotalOfertasRecolectadas(Double totalOfertasRecolectadas) {
        this.totalOfertasRecolectadas = totalOfertasRecolectadas;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "VARIANZA_POBLACIONAL_VARIABLE")
    public Double getVarianzaPoblacionalVariable() {
        return this.varianzaPoblacionalVariable;
    }

    public void setVarianzaPoblacionalVariable(Double varianzaPoblacionalVariable) {
        this.varianzaPoblacionalVariable = varianzaPoblacionalVariable;
    }

    //bi-directional many-to-one association to ControlCalidad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTROL_CALIDAD_ID")
    public ControlCalidad getControlCalidad() {
        return this.controlCalidad;
    }

    public void setControlCalidad(ControlCalidad controlCalidad) {
        this.controlCalidad = controlCalidad;
    }

}
