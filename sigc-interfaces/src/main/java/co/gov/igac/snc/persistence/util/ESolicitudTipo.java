package co.gov.igac.snc.persistence.util;

//TODO::fabio.navarrete::01-08-2011::DOCUMENTAR!!!!::pedro.garcia
public enum ESolicitudTipo {

    TRAMITE_CATASTRAL("1"),
    AVISOS("2"),
    VIA_GUBERNATIVA("3"),
    AUTOAVALUO("4"),
    REVISION_AVALUO("5"),
    CONSERVACION_DINAMICA("6"),
    REVOCATORIA_DIRECTA("7"),
    PRODUCTOS_CATASTRALES("8"),
    TRAMITE_ACTUALIZACION("9");

    private String codigo;

    private ESolicitudTipo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
