package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteRadicacionEspecial;
import co.gov.igac.snc.persistence.util.IProyeccionObject;

/**
 * The persistent class for the P_FICHA_MATRIZ_PREDIO database table.
 *
 */
/**
 * MODIFICACIONES PROPIAS A LA CLASE PFichaMatrizPredio:
 *
 * @modified david.cifuentes :: 18/11/11 :: Se agregó el método transient
 * getFormattedNumeroPredialCompleto que se encontraba en la clase Predio.
 *
 * juan.agudelo 18-04-12 Adición del atributo transient valorAvaluo, este campo debe ser asignado
 * desde un ejb.
 *
 * david.cifuentes :: Se agregó el atributo consecutivoUnidad :: 11/09/2013
 *
 * david.cifuentes :: Se agregó el atributo transient selected y sus respectivos get y set ::
 * 21/10/2013
 *
 * david.cifuentes :: Se mapea el atributo número predial original :: 22/05/2015
 *
 * felipe.cadena :: se agrega el campo originalTramite :: 08-05-2015
 *
 * david.cifuentes :: Adición de la variable estado :: 08/09/15 ::
 *
 * david.cifuentes :: Adición del método transient isEsOriginalTramite :: 08/03/2016
 *
 * jonathan.chacon :: Adición de ficha matriz proviene para englobe virtual
 */
@Entity
@Table(name = "P_FICHA_MATRIZ_PREDIO")
public class PFichaMatrizPredio implements Serializable, IProyeccionObject {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String cancelaInscribe;
    private Double coeficiente;
    private Date fechaLog;
    private String numeroPredial;
    private String usuarioLog;
    private PFichaMatriz PFichaMatriz;
    private Double consecutivoUnidad;
    private String numeroPredialOriginal;
    private String estado;
    private FichaMatriz provieneFichaMatriz;

    /**
     * Marca los predios iniciales de un tramite, solo aplica para tramites de condiciones
     * especiales {@link ETramiteRadicacionEspecial}
     */
    private String originalTramite;

    // Transient
    private Double valorAvaluo;
    private boolean selected;

    private String formatedNumPredial;

    public PFichaMatrizPredio() {
    }

    @Id
    @GeneratedValue(generator = "FICHA_MATRIZ_PREDIO_ID_SEQ")
    @GenericGenerator(name = "FICHA_MATRIZ_PREDIO_ID_SEQ", strategy =
        "co.gov.igac.snc.persistence.util.CustomProyeccionIdGenerator", parameters = {
            @Parameter(name = "sequence", value = "FICHA_MATRIZ_PREDIO_ID_SEQ"),
            @Parameter(name = "allocationSize", value = "1")})
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CANCELA_INSCRIBE")
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "COEFICIENTE", precision = 16)
    public Double getCoeficiente() {
        return this.coeficiente;
    }

    public void setCoeficiente(Double coeficiente) {
        this.coeficiente = coeficiente;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG")
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "NUMERO_PREDIAL")
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    @Column(name = "USUARIO_LOG")
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    // bi-directional many-to-one association to PFichaMatriz
    @ManyToOne
    @JoinColumn(name = "FICHA_MATRIZ_ID")
    public PFichaMatriz getPFichaMatriz() {
        return this.PFichaMatriz;
    }

    public void setPFichaMatriz(PFichaMatriz PFichaMatriz) {
        this.PFichaMatriz = PFichaMatriz;
    }

    @Column(name = "CONSECUTIVO_UNIDAD")
    public Double getConsecutivoUnidad() {
        return this.consecutivoUnidad;
    }

    public void setConsecutivoUnidad(Double consecutivoUnidad) {
        this.consecutivoUnidad = consecutivoUnidad;
    }

    @Column(name = "ORIGINAL_TRAMITE", length = 2)
    public String getOriginalTramite() {
        return this.originalTramite;
    }

    public void setOriginalTramite(String resultado) {
        this.originalTramite = resultado;
    }

    @Column(name = "NUMERO_PREDIAL_ORIGINAL")
    public String getNumeroPredialOriginal() {
        return this.numeroPredialOriginal;
    }

    public void setNumeroPredialOriginal(String numeroPredialOriginal) {
        this.numeroPredialOriginal = numeroPredialOriginal;
    }

    @Column(name = "ESTADO", nullable = true, length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    // bi-directional many-to-one association to PFichaMatriz
    @ManyToOne
    @JoinColumn(name = "PROVIENE_FICHA_MATRIZ_ID")
    public FichaMatriz getProvieneFichaMatriz() {
        return this.provieneFichaMatriz;
    }

    public void setProvieneFichaMatriz(FichaMatriz FichaMatriz) {
        this.provieneFichaMatriz = FichaMatriz;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author fabio.navarrete Método que arma el número predial
     * @return Número predial completo con el formato especificado.
     */
    @Transient
    public String getFormattedNumeroPredialCompleto() {
        formatedNumPredial = new String();

        // Departamento
        formatedNumPredial = numeroPredial.substring(0, 2).concat("-");
        // Municipio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(2, 5)).concat("-");
        // Tipo Avalúo
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(5, 7)).concat("-");
        // Sector
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(7, 9)).concat("-");
        // Barrio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(9, 13)).concat("-");
        // Manzana
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(13, 17)).concat("-");
        // Predio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(17, 21)).concat("-");
        // Condición Propiedad
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(21, 22)).concat("-");
        // Edificio
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(22, 24)).concat("-");
        // Piso
        formatedNumPredial = formatedNumPredial.concat(
            numeroPredial.substring(24, 26)).concat("-");
        // Unidad
        formatedNumPredial = formatedNumPredial.concat(numeroPredial.substring(
            26, 30));
        return formatedNumPredial;
    }

    // -------------------------------------------------------------------------
    @Transient
    public Double getValorAvaluo() {
        return valorAvaluo;
    }

    public void setValorAvaluo(Double valorAvaluo) {
        this.valorAvaluo = valorAvaluo;
    }

    /**
     * Determina si el {@link PFichaMatrizPredio} está seleccionado en alguna pantalla o sitio en
     * que se requiera.
     *
     * @return
     */
    @Transient
    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * Si el numero predial de uno de los predios originales en desenglobes por etapas cambia
     * retorna el nuevo numero, sino retorna vacio. Si el predio no es predio original retorna vacio
     *
     * @return
     */
    @Transient
    public String getNumeroPredialNuevo() {
        if (this.originalTramite == null || this.originalTramite.isEmpty()) {
            return "";
        }
        if (this.numeroPredialOriginal == null ||
            this.numeroPredialOriginal.isEmpty()) {
            return "";
        }
        if (this.numeroPredial.equals(this.numeroPredialOriginal)) {
            return "";
        }

        return numeroPredial;
    }

    /**
     * Si el numero predial de uno de los predios originales en desenglobes por etapas cambia
     * retorna el nuevo numero, sino retorna vacio. Si el predio no es predio original retorna vacio
     *
     * @return
     */
    @Transient
    public String getNumeroPredialNuevoConFormato() {
        if (this.originalTramite == null || this.originalTramite.isEmpty()) {
            return "";
        }
        if (this.numeroPredialOriginal == null ||
            this.numeroPredialOriginal.isEmpty()) {
            return "";
        }
        if (this.numeroPredial.equals(this.numeroPredialOriginal)) {
            return "";
        }

        return this.formatNumeroPredial(numeroPredial);
    }

    /**
     * Siempre retorna el numero predial del predio original aun si este ha cambiado
     *
     * @return
     */
    @Transient
    public String getNumeroPredialAntiguo() {
        if (this.originalTramite == null || this.originalTramite.isEmpty()) {
            return this.numeroPredial;
        }
        if (this.numeroPredialOriginal == null ||
            this.numeroPredialOriginal.isEmpty()) {
            return this.numeroPredial;
        }
        if (this.numeroPredial.equals(this.numeroPredialOriginal)) {
            return this.numeroPredial;
        }

        return numeroPredialOriginal;
    }

    /**
     * Siempre retorna el numero predial del predio original aun si este ha cambiado
     *
     * @return
     */
    @Transient
    public String getNumeroPredialAntiguoConFormato() {
        if (this.originalTramite == null || this.originalTramite.isEmpty()) {
            return this.formatNumeroPredial(this.numeroPredial);
        }
        if (this.numeroPredialOriginal == null ||
            this.numeroPredialOriginal.isEmpty()) {
            return this.formatNumeroPredial(this.numeroPredial);
        }
        if (this.numeroPredial.equals(this.numeroPredialOriginal)) {
            return this.formatNumeroPredial(this.numeroPredial);
        }

        return this.formatNumeroPredial(this.numeroPredialOriginal);
    }

    /**
     * Retorna el numero predial con formato
     *
     * @param numero
     * @return
     */
    private String formatNumeroPredial(String numero) {
        String formatedNumPredial = "";

        if (numero != null && !numero.isEmpty()) {
            // Departamento
            formatedNumPredial = numero.substring(0, 2).concat("-");
            // Municipio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(2, 5)).concat("-");
            // Tipo Avalúo
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(5, 7)).concat("-");
            // Sector
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(7, 9)).concat("-");
            // Comuna
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(9, 11)).concat("-");
            // Barrio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(11, 13)).concat("-");
            // Manzana
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(13, 17)).concat("-");
            // Predio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(17, 21)).concat("-");
            // Condición Propiedad
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(21, 22)).concat("-");
            // Edificio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(22, 24)).concat("-");
            // Piso
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(24, 26)).concat("-");
            // Unidad
            formatedNumPredial = formatedNumPredial.concat(numero.substring(26,
                30));
        }
        return formatedNumPredial;

    }

    /**
     * Método que determina si el {@link PFichaMatrizPredio} es un sótano a partir de su número
     * predial
     *
     * @return
     */
    @Transient
    public boolean isSotano() {
        if (numeroPredial != null) {
            Long piso = new Long(numeroPredial.substring(24, 26));
            if (piso != null && piso.longValue() > 85) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Determina si un {@link PFichaMatrizPredio} es original del trámite, en trámites masivos
     *
     * @author david.cifuentes
     * @return
     */
    @Transient
    public boolean isEsOriginalTramite() {

        if (this.originalTramite != null) {
            if (ESiNo.SI.getCodigo().equals(this.originalTramite)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PFichaMatrizPredio)) {
            return false;
        }
        if (this.id == null) {
            return false;
        }
        PFichaMatrizPredio castOther = (PFichaMatrizPredio) obj;
        return (this.id.equals(castOther.getId()));
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
