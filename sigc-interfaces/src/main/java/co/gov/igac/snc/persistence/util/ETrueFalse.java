package co.gov.igac.snc.persistence.util;

/**
 * Enumeración correspondiente para los valores true y false
 *
 * @author franz.gamba
 */
public enum ETrueFalse {
    TRUE("true"),
    FALSE("false");

    private String codigo;

    private ETrueFalse(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
