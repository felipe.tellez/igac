package co.gov.igac.snc.persistence.entity.conservacion;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;

/*
 * MODIFICACIONES PROPIAS A LA CLASE HUnidadConstruccion
 *
 * @modified leidy.gonzalez :: Adición del atributo anioCancelacion y sus respectivos get y set ::
 * 22/05/2014
 *
 */
/**
 * HUnidadConstruccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "H_UNIDAD_CONSTRUCCION", schema = "SNC_CONSERVACION")
public class HUnidadConstruccion implements Serializable {

    // Fields
    private static final long serialVersionUID = 2949782354574506414L;
    protected HUnidadConstruccionId id;
    private Predio predio;
    private String unidad;
    private Long calificacionAnexoId;
    private String tipoConstruccion;
    private String descripcion;
    private String observaciones;
    private String tipoCalificacion;
    private Double totalPuntaje;
    private String tipificacion;
    private UsoConstruccion usoConstruccion;
    private Integer anioConstruccion;
    private Double areaConstruida;
    private Double avaluo;
    private Integer totalBanios;
    private Integer totalHabitaciones;
    private Integer totalLocales;
    private Integer totalPisosUnidad;
    private Integer totalPisosConstruccion;
    private Double valorM2Construccion;
    private String pisoUbicacion;
    private String tipoDominio;
    private Timestamp fechaInscripcionCatastral;
    private String cancelaInscribe;
    private Long provienePredioId;
    private String provieneUnidad;
    private String usuarioLog;
    private Timestamp fechaLog;
    private List<HUnidadConstruccionComp> HUnidadConstruccionComps =
        new ArrayList<HUnidadConstruccionComp>();

    private Integer anioCancelacion;
    private HPredio HPredio;

    // Constructors
    /** default constructor */
    public HUnidadConstruccion() {
    }

    /** minimal constructor
     *
     * @param id
     * @param HPredio
     * @param predio
     * @param unidad
     * @param tipoConstruccion
     * @param tipoCalificacion
     * @param totalPuntaje
     * @param tipificacion
     * @param anioConstruccion
     * @param areaConstruida
     * @param avaluo
     * @param totalPisosUnidad
     * @param totalPisosConstruccion
     * @param valorM2Construccion
     * @param tipoDominio
     * @param usuarioLog
     * @param fechaLog
     */
    public HUnidadConstruccion(HUnidadConstruccionId id, HPredio HPredio,
        Predio predio, String unidad, String tipoConstruccion,
        String tipoCalificacion, Double totalPuntaje, String tipificacion,
        Integer anioConstruccion, Double areaConstruida, Double avaluo,
        Integer totalPisosUnidad, Integer totalPisosConstruccion,
        Double valorM2Construccion, String tipoDominio, String usuarioLog,
        Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predio = predio;
        this.unidad = unidad;
        this.tipoConstruccion = tipoConstruccion;
        this.tipoCalificacion = tipoCalificacion;
        this.totalPuntaje = totalPuntaje;
        this.tipificacion = tipificacion;
        this.anioConstruccion = anioConstruccion;
        this.areaConstruida = areaConstruida;
        this.avaluo = avaluo;
        this.totalPisosUnidad = totalPisosUnidad;
        this.totalPisosConstruccion = totalPisosConstruccion;
        this.valorM2Construccion = valorM2Construccion;
        this.tipoDominio = tipoDominio;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor
     *
     * @param id
     * @param HPredio
     * @param predio
     * @param unidad
     * @param calificacionAnexoId
     * @param tipoConstruccion
     * @param descripcion
     * @param observaciones
     * @param tipoCalificacion
     * @param totalPuntaje
     * @param tipificacion
     * @param usoConstruccion
     * @param anioConstruccion
     * @param areaConstruida
     * @param avaluo
     * @param totalBanios
     * @param totalHabitaciones
     * @param totalLocales
     * @param totalPisosUnidad
     * @param totalPisosConstruccion
     * @param valorM2Construccion
     * @param pisoUbicacion
     * @param tipoDominio
     * @param fechaInscripcionCatastral
     * @param cancelaInscribe
     * @param usuarioLog
     * @param fechaLog
     */
    public HUnidadConstruccion(HUnidadConstruccionId id, HPredio HPredio,
        Predio predio, String unidad, Long calificacionAnexoId,
        String tipoConstruccion, String descripcion, String observaciones,
        String tipoCalificacion, Double totalPuntaje, String tipificacion,
        UsoConstruccion usoConstruccion, Integer anioConstruccion, Double areaConstruida,
        Double avaluo, Integer totalBanios, Integer totalHabitaciones,
        Integer totalLocales, Integer totalPisosUnidad,
        Integer totalPisosConstruccion, Double valorM2Construccion,
        String pisoUbicacion, String tipoDominio,
        Timestamp fechaInscripcionCatastral, String cancelaInscribe,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.HPredio = HPredio;
        this.predio = predio;
        this.unidad = unidad;
        this.calificacionAnexoId = calificacionAnexoId;
        this.tipoConstruccion = tipoConstruccion;
        this.descripcion = descripcion;
        this.observaciones = observaciones;
        this.tipoCalificacion = tipoCalificacion;
        this.totalPuntaje = totalPuntaje;
        this.tipificacion = tipificacion;
        this.usoConstruccion = usoConstruccion;
        this.anioConstruccion = anioConstruccion;
        this.areaConstruida = areaConstruida;
        this.avaluo = avaluo;
        this.totalBanios = totalBanios;
        this.totalHabitaciones = totalHabitaciones;
        this.totalLocales = totalLocales;
        this.totalPisosUnidad = totalPisosUnidad;
        this.totalPisosConstruccion = totalPisosConstruccion;
        this.valorM2Construccion = valorM2Construccion;
        this.pisoUbicacion = pisoUbicacion;
        this.tipoDominio = tipoDominio;
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
        this.cancelaInscribe = cancelaInscribe;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @EmbeddedId
    public HUnidadConstruccionId getId() {
        return this.id;
    }

    public void setId(HUnidadConstruccionId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "H_PREDIO_ID", nullable = false, insertable = false, updatable = false)
    public HPredio getHPredio() {
        return this.HPredio;
    }

    public void setHPredio(HPredio HPredio) {
        this.HPredio = HPredio;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PREDIO_ID", nullable = false)
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    @Column(name = "UNIDAD", nullable = false, length = 50)
    public String getUnidad() {
        return this.unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    @Column(name = "CALIFICACION_ANEXO_ID", precision = 10, scale = 0)
    public Long getCalificacionAnexoId() {
        return this.calificacionAnexoId;
    }

    public void setCalificacionAnexoId(Long calificacionAnexoId) {
        this.calificacionAnexoId = calificacionAnexoId;
    }

    @Column(name = "TIPO_CONSTRUCCION", nullable = false, length = 30)
    public String getTipoConstruccion() {
        return this.tipoConstruccion;
    }

    public void setTipoConstruccion(String tipoConstruccion) {
        this.tipoConstruccion = tipoConstruccion;
    }

    @Column(name = "DESCRIPCION", length = 200)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "TIPO_CALIFICACION", nullable = false, length = 30)
    public String getTipoCalificacion() {
        return this.tipoCalificacion;
    }

    public void setTipoCalificacion(String tipoCalificacion) {
        this.tipoCalificacion = tipoCalificacion;
    }

    @Column(name = "TOTAL_PUNTAJE", nullable = false, precision = 10)
    public Double getTotalPuntaje() {
        return this.totalPuntaje;
    }

    public void setTotalPuntaje(Double totalPuntaje) {
        this.totalPuntaje = totalPuntaje;
    }

    @Column(name = "TIPIFICACION", nullable = false, length = 30)
    public String getTipificacion() {
        return this.tipificacion;
    }

    public void setTipificacion(String tipificacion) {
        this.tipificacion = tipificacion;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USO_ID", nullable = false)
    public UsoConstruccion getUsoConstruccion() {
        return this.usoConstruccion;
    }

    public void setUsoConstruccion(UsoConstruccion usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    @Column(name = "ANIO_CONSTRUCCION", nullable = false, precision = 4, scale = 0)
    public Integer getAnioConstruccion() {
        return this.anioConstruccion;
    }

    public void setAnioConstruccion(Integer anioConstruccion) {
        this.anioConstruccion = anioConstruccion;
    }

    @Column(name = "AREA_CONSTRUIDA", nullable = false, precision = 18)
    public Double getAreaConstruida() {
        return this.areaConstruida;
    }

    public void setAreaConstruida(Double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    @Column(name = "AVALUO", nullable = false, precision = 18)
    public Double getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Double avaluo) {
        this.avaluo = avaluo;
    }

    @Column(name = "TOTAL_BANIOS", precision = 2, scale = 0)
    public Integer getTotalBanios() {
        return this.totalBanios;
    }

    public void setTotalBanios(Integer totalBanios) {
        this.totalBanios = totalBanios;
    }

    @Column(name = "TOTAL_HABITACIONES", precision = 2, scale = 0)
    public Integer getTotalHabitaciones() {
        return this.totalHabitaciones;
    }

    public void setTotalHabitaciones(Integer totalHabitaciones) {
        this.totalHabitaciones = totalHabitaciones;
    }

    @Column(name = "TOTAL_LOCALES", precision = 2, scale = 0)
    public Integer getTotalLocales() {
        return this.totalLocales;
    }

    public void setTotalLocales(Integer totalLocales) {
        this.totalLocales = totalLocales;
    }

    @Column(name = "TOTAL_PISOS_UNIDAD", nullable = false, precision = 2, scale = 0)
    public Integer getTotalPisosUnidad() {
        return this.totalPisosUnidad;
    }

    public void setTotalPisosUnidad(Integer totalPisosUnidad) {
        this.totalPisosUnidad = totalPisosUnidad;
    }

    @Column(name = "TOTAL_PISOS_CONSTRUCCION", nullable = false, precision = 2, scale = 0)
    public Integer getTotalPisosConstruccion() {
        return this.totalPisosConstruccion;
    }

    public void setTotalPisosConstruccion(Integer totalPisosConstruccion) {
        this.totalPisosConstruccion = totalPisosConstruccion;
    }

    @Column(name = "VALOR_M2_CONSTRUCCION", nullable = false, precision = 18)
    public Double getValorM2Construccion() {
        return this.valorM2Construccion;
    }

    public void setValorM2Construccion(Double valorM2Construccion) {
        this.valorM2Construccion = valorM2Construccion;
    }

    @Column(name = "PISO_UBICACION", length = 30)
    public String getPisoUbicacion() {
        return this.pisoUbicacion;
    }

    public void setPisoUbicacion(String pisoUbicacion) {
        this.pisoUbicacion = pisoUbicacion;
    }

    @Column(name = "TIPO_DOMINIO", nullable = false, length = 30)
    public String getTipoDominio() {
        return this.tipoDominio;
    }

    public void setTipoDominio(String tipoDominio) {
        this.tipoDominio = tipoDominio;
    }

    @Column(name = "FECHA_INSCRIPCION_CATASTRAL", length = 7)
    public Timestamp getFechaInscripcionCatastral() {
        return this.fechaInscripcionCatastral;
    }

    public void setFechaInscripcionCatastral(Timestamp fechaInscripcionCatastral) {
        this.fechaInscripcionCatastral = fechaInscripcionCatastral;
    }

    @Column(name = "CANCELA_INSCRIBE", length = 30)
    public String getCancelaInscribe() {
        return this.cancelaInscribe;
    }

    public void setCancelaInscribe(String cancelaInscribe) {
        this.cancelaInscribe = cancelaInscribe;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "ANIO_CANCELACION", nullable = true, precision = 4, scale = 0)
    public Integer getAnioCancelacion() {
        return anioCancelacion;
    }

    public void setAnioCancelacion(Integer anioCancelacion) {
        this.anioCancelacion = anioCancelacion;
    }

    @Column(name = "PROVIENE_PREDIO_ID")
    public Long getProvienePredioId() {
        return provienePredioId;
    }

    public void setProvienePredioId(Long provienePredioId) {
        this.provienePredioId = provienePredioId;
    }

    @Column(name = "PROVIENE_UNIDAD")
    public String getProvieneUnidad() {
        return provieneUnidad;
    }

    public void setProvieneUnidad(String provieneUnidad) {
        this.provieneUnidad = provieneUnidad;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "HUnidadConstruccion")
    public List<HUnidadConstruccionComp> getHUnidadConstruccionComps() {
        return this.HUnidadConstruccionComps;
    }

    public void setHUnidadConstruccionComps(
        List<HUnidadConstruccionComp> HUnidadConstruccionComps) {
        this.HUnidadConstruccionComps = HUnidadConstruccionComps;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HUnidadConstruccion)) {
            return false;
        }
        HUnidadConstruccion other = (HUnidadConstruccion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

}
