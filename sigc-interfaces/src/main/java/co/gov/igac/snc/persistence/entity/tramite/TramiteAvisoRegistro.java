package co.gov.igac.snc.persistence.entity.tramite;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * TramiteAvisoRegistro entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRAMITE_AVISO_REGISTRO", schema = "SNC_TRAMITE")
public class TramiteAvisoRegistro implements java.io.Serializable {

    // Fields
    private Long id;
    private Tramite tramite;
    private Long predioId;
    private String circuloRegistral;
    private String departamentoId;
    private String municipioId;
    private String estado;
    private String motivoNoProcede;
    private Long documentoSoporteId;
    private Timestamp fechaDocumentoSoporte;
    private String observaciones;
    private String usuarioLog;
    private Timestamp fechaLog;

    // Constructors
    /** default constructor */
    public TramiteAvisoRegistro() {
    }

    /** minimal constructor */
    public TramiteAvisoRegistro(Long id, Tramite tramite, Long predioId,
        String circuloRegistral, String departamentoId, String municipioId,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.predioId = predioId;
        this.circuloRegistral = circuloRegistral;
        this.departamentoId = departamentoId;
        this.municipioId = municipioId;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public TramiteAvisoRegistro(Long id, Tramite tramite, Long predioId,
        String circuloRegistral, String departamentoId, String municipioId,
        String estado, String motivoNoProcede, Long documentoSoporteId,
        Timestamp fechaDocumentoSoporte, String observaciones,
        String usuarioLog, Timestamp fechaLog) {
        this.id = id;
        this.tramite = tramite;
        this.predioId = predioId;
        this.circuloRegistral = circuloRegistral;
        this.departamentoId = departamentoId;
        this.municipioId = municipioId;
        this.estado = estado;
        this.motivoNoProcede = motivoNoProcede;
        this.documentoSoporteId = documentoSoporteId;
        this.fechaDocumentoSoporte = fechaDocumentoSoporte;
        this.observaciones = observaciones;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID", nullable = false)
    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    @Column(name = "PREDIO_ID", nullable = false, precision = 10, scale = 0)
    public Long getPredioId() {
        return this.predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    @Column(name = "CIRCULO_REGISTRAL", nullable = false, length = 100)
    public String getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(String circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "DEPARTAMENTO_ID", nullable = false, length = 2)
    public String getDepartamentoId() {
        return this.departamentoId;
    }

    public void setDepartamentoId(String departamentoId) {
        this.departamentoId = departamentoId;
    }

    @Column(name = "MUNICIPIO_ID", nullable = false, length = 5)
    public String getMunicipioId() {
        return this.municipioId;
    }

    public void setMunicipioId(String municipioId) {
        this.municipioId = municipioId;
    }

    @Column(name = "ESTADO", length = 30)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "MOTIVO_NO_PROCEDE", length = 20)
    public String getMotivoNoProcede() {
        return this.motivoNoProcede;
    }

    public void setMotivoNoProcede(String motivoNoProcede) {
        this.motivoNoProcede = motivoNoProcede;
    }

    @Column(name = "SOPORTE_DOCUMENTO_ID", precision = 10, scale = 0)
    public Long getDocumentoSoporteId() {
        return this.documentoSoporteId;
    }

    public void setDocumentoSoporteId(Long documentoSoporteId) {
        this.documentoSoporteId = documentoSoporteId;
    }

    @Column(name = "FECHA_DOCUMENTO_SOPORTE", length = 7)
    public Timestamp getFechaDocumentoSoporte() {
        return this.fechaDocumentoSoporte;
    }

    public void setFechaDocumentoSoporte(Timestamp fechaDocumentoSoporte) {
        this.fechaDocumentoSoporte = fechaDocumentoSoporte;
    }

    @Column(name = "OBSERVACIONES", length = 2000)
    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Timestamp getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Timestamp fechaLog) {
        this.fechaLog = fechaLog;
    }

}
