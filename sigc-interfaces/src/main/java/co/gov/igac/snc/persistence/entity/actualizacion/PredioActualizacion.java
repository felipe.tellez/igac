/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.persistence.entity.actualizacion;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Detalle del proceso de actualizacion por predio
 *
 * @author felipe.cadena
 */
@Entity
@Table(name = "AX_PREDIO_TRAMITE", schema = "SNC_ACTUALIZACION")
public class PredioActualizacion implements Serializable {

    private static final long serialVersionUID = -2219934966617987967L;
    private Long id;
    private String numeroPredialInicial;
    private String numeroPrediosAsociados;
    private String tipoTramtie;
    private String mutacionClase;
    private String mutacionSubtipo;
    private int ordenTramite;
    private int conteoTramites;
    private String numeroPredialNuevo;
    private String datosRectificar;
    private String datosComplementar;
    private Tramite tramite;

    @Id
    @Column(name = "ID_PREDIO_TRAMITE", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NUMERO_PREDIAL_INICIAL")
    public String getNumeroPredialInicial() {
        return numeroPredialInicial;
    }

    public void setNumeroPredialInicial(String numeroPredialInicial) {
        this.numeroPredialInicial = numeroPredialInicial;
    }

    @Column(name = "NUMERO_PREDIOS_ASOCIADOS")
    public String getNumeroPrediosAsociados() {
        return numeroPrediosAsociados;
    }

    public void setNumeroPrediosAsociados(String numeroPrediosAsociados) {
        this.numeroPrediosAsociados = numeroPrediosAsociados;
    }

    @Column(name = "TIPO_TRAMITE")
    public String getTipoTramtie() {
        return tipoTramtie;
    }

    public void setTipoTramtie(String tipoTramtie) {
        this.tipoTramtie = tipoTramtie;
    }

    @Column(name = "MUTACION_CLASE")
    public String getMutacionClase() {
        return mutacionClase;
    }

    public void setMutacionClase(String mutacionClase) {
        this.mutacionClase = mutacionClase;
    }

    @Column(name = "MUTACION_SUBTIPO")
    public String getMutacionSubtipo() {
        return mutacionSubtipo;
    }

    public void setMutacionSubtipo(String mutacionSubtipo) {
        this.mutacionSubtipo = mutacionSubtipo;
    }

    @Column(name = "ORDEN_TRAMITE")
    public int getOrdenTramite() {
        return ordenTramite;
    }

    public void setOrdenTramite(int ordenTramite) {
        this.ordenTramite = ordenTramite;
    }

    @Column(name = "CONTEO_TRAMITES")
    public int getConteoTramites() {
        return conteoTramites;
    }

    public void setConteoTramites(int conteoTramites) {
        this.conteoTramites = conteoTramites;
    }

    @Column(name = "NUMERO_PREDIAL_NUEVO")
    public String getNumeroPredialNuevo() {
        return numeroPredialNuevo;
    }

    public void setNumeroPredialNuevo(String numeroPredialNuevo) {
        this.numeroPredialNuevo = numeroPredialNuevo;
    }

    @Column(name = "DATOS_RECTIFICAR")
    public String getDatosRectificar() {
        return datosRectificar;
    }

    public void setDatosRectificar(String datosRectificar) {
        this.datosRectificar = datosRectificar;
    }

    @Column(name = "DATOS_COMPLEMENTAR")
    public String getDatosComplementar() {
        return datosComplementar;
    }

    public void setDatosComplementar(String datosComplementar) {
        this.datosComplementar = datosComplementar;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAMITE_ID")
    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

}
