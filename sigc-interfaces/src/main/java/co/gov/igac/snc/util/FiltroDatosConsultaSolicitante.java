/**
 * Filtro para los campos de la consulta de solicitanteSolictud
 *
 * @author david.cifuentes
 */
package co.gov.igac.snc.util;

import java.io.Serializable;

import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;

public class FiltroDatosConsultaSolicitante implements Serializable {

    private static final long serialVersionUID = -9172243477091829567L;

    /**
     * Segmentos del Solicitante
     */
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String digitoVerificacion;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String nombreCompleto;
    private String razonSocial;
    private String tipoPersona;
    private String sigla;

    private Long idSolicitante;

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getDigitoVerificacion() {
        return digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getNombreCompleto() {

        if (this.nombreCompleto != null && !this.nombreCompleto.isEmpty()) {
            return this.nombreCompleto;
        }

        StringBuffer sb = new StringBuffer();
        sb.append("");
        if (this.tipoIdentificacion != null && this.tipoIdentificacion.equals(
            EPersonaTipoIdentificacion.NIT.getCodigo())) {
            if (this.razonSocial != null) {
                sb.append(this.razonSocial).append(" ");
            }
        } else {
            if (this.primerNombre != null) {
                sb.append(this.primerNombre).append(" ");
            }
            if (this.segundoNombre != null) {
                sb.append(this.segundoNombre).append(" ");
            }
            if (this.primerApellido != null) {
                sb.append(this.primerApellido).append(" ");
            }
            if (this.segundoApellido != null) {
                sb.append(this.segundoApellido).append(" ");
            }
        }
        return sb.toString();
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public void setIdSolicitante(Long idSolicitante) {
        this.idSolicitante = idSolicitante;
    }

    public Long getIdSolicitante() {
        return idSolicitante;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

}
