package co.gov.igac.snc.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Clase que reduce la informacion de los sotanos de la torre en una Ficha matriz para ser mostrados
 * en las tareas geograficas
 *
 * @author franz.gamba
 */
public class FichaMatrizSotanoVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7160213129823275154L;
    private String numero;
    private List<FichaMatrizPredioVO> predios;

    /** Empty Constructor */
    public FichaMatrizSotanoVO() {

    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public List<FichaMatrizPredioVO> getPredios() {
        return predios;
    }

    public void setPredios(List<FichaMatrizPredioVO> predios) {
        this.predios = predios;
    }

}
