package co.gov.igac.snc.persistence.entity.tramite;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * DuracionTramite entity. @author MyEclipse Persistence Tools
 *
 * @modified by pedro.garcia what: - cambio de tipo de datos de las fechas (de TimeStamp a Date)
 */
@Entity
@Table(name = "DURACION_TRAMITE", schema = "SNC_TRAMITE")
public class DuracionTramite implements java.io.Serializable {

    // Fields
    private Long id;
    private String tipoTramite;
    private String tipoAvaluo;
    private String sede;
    private Double areaDesde;
    private Double areaHasta;
    private Double normaDia;
    private Date fechaDesde;
    private Date fechaHasta;
    private Long documentoSoporteId;
    private Date fechaLog;
    private String usuarioLog;

    // Constructors
    /** default constructor */
    public DuracionTramite() {
    }

    /** minimal constructor */
    public DuracionTramite(Long id, String tipoTramite, String tipoAvaluo,
        String sede, Double areaDesde, Double areaHasta, Double normaDia,
        Date fechaDesde, Date fechaLog, String usuarioLog) {
        this.id = id;
        this.tipoTramite = tipoTramite;
        this.tipoAvaluo = tipoAvaluo;
        this.sede = sede;
        this.areaDesde = areaDesde;
        this.areaHasta = areaHasta;
        this.normaDia = normaDia;
        this.fechaDesde = fechaDesde;
        this.fechaLog = fechaLog;
        this.usuarioLog = usuarioLog;
    }

    /** full constructor */
    public DuracionTramite(Long id, String tipoTramite, String tipoAvaluo,
        String sede, Double areaDesde, Double areaHasta, Double normaDia,
        Date fechaDesde, Date fechaHasta,
        Long documentoSoporteId, Date fechaLog, String usuarioLog) {
        this.id = id;
        this.tipoTramite = tipoTramite;
        this.tipoAvaluo = tipoAvaluo;
        this.sede = sede;
        this.areaDesde = areaDesde;
        this.areaHasta = areaHasta;
        this.normaDia = normaDia;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.documentoSoporteId = documentoSoporteId;
        this.fechaLog = fechaLog;
        this.usuarioLog = usuarioLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TIPO_TRAMITE", nullable = false, length = 30)
    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    @Column(name = "TIPO_AVALUO", nullable = false, length = 30)
    public String getTipoAvaluo() {
        return this.tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    @Column(name = "SEDE", nullable = false, length = 2)
    public String getSede() {
        return this.sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    @Column(name = "AREA_DESDE", nullable = false, precision = 12)
    public Double getAreaDesde() {
        return this.areaDesde;
    }

    public void setAreaDesde(Double areaDesde) {
        this.areaDesde = areaDesde;
    }

    @Column(name = "AREA_HASTA", nullable = false, precision = 12)
    public Double getAreaHasta() {
        return this.areaHasta;
    }

    public void setAreaHasta(Double areaHasta) {
        this.areaHasta = areaHasta;
    }

    @Column(name = "NORMA_DIA", nullable = false, precision = 8)
    public Double getNormaDia() {
        return this.normaDia;
    }

    public void setNormaDia(Double normaDia) {
        this.normaDia = normaDia;
    }

    @Column(name = "FECHA_DESDE", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaDesde() {
        return this.fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    @Column(name = "FECHA_HASTA", length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaHasta() {
        return this.fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    @Column(name = "DOCUMENTO_SOPORTE_ID", precision = 10, scale = 0)
    public Long getDocumentoSoporteId() {
        return this.documentoSoporteId;
    }

    public void setDocumentoSoporteId(Long documentoSoporteId) {
        this.documentoSoporteId = documentoSoporteId;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

}
