package co.gov.igac.snc.persistence.util;

/**
 * Enumeración que representa los datos contenidos en el Dominio SOLICITUD_ORIGEN_PROCURADURIA
 *
 *
 * Datos de posibles tipos de solicitud segun el tipo de solicitante PROCURADURIA
 *
 *
 * @author rodrigo.hernandez
 *
 */
public enum ESolicitudOrigenProcuraduria {

    ADMINISTRATIVO("FA", "Administrativo"),
    INVESTIGACION("FI", "Investigación");

    private String codigo;
    private String valor;

    private ESolicitudOrigenProcuraduria(String codigo, String valor) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getValor() {
        return this.valor;
    }

}
