package co.gov.igac.persistence.entity.generales;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * MODIFICACIONES PROPIAS A LA CLASE Zona: -> Se agregó el serialVersionUID. -> Se agregó el Named
 * Query: findMunicipioByCodigo -> Se agregó el método Transient getCodigo3Digitos que sirve para
 * obtener el código de municipio sin los dígitos de departamento.
 */
/**
 * Zona entity.
 *
 * @author lorena.salamanca FGWL Comparable, Comparator
 */
@Cacheable
@Entity
@NamedQueries({@NamedQuery(name = "zona.findZonaByCodigo", query =
        "from Zona m where m.codigo = :codigo")})
@Table(name = "ZONA", schema = "IGAC_GENERALES")
public class Zona implements java.io.Serializable, Comparable<Zona> {

    /**
     *
     */
    private static final long serialVersionUID = -2214204648399624672L;
    /**
     *
     */

    // Fields
    private String codigo;
    private Municipio municipio;
    private String nombre;
    private String usuarioLog;
    private Date fechaLog;
    private Set<EstructuraOrganizacional> estructuraOrganizacionals =
        new HashSet<EstructuraOrganizacional>(
            0);
    private List<Jurisdiccion> jurisdiccions = new ArrayList<Jurisdiccion>(0);

    /**
     * Método creado para retornar el código requerido en la interfaz gráfica que debe tener sólo
     * los tres últimos dígitos (excluyendo el código de departamento)
     *
     * @return
     */
    @Transient
    public String getCodigo3Digitos() {
        return codigo.substring(codigo.length() - 2, codigo.length());
    }

    // Constructors
    /** default constructor */
    public Zona() {
    }

    /** minimal constructor */
    public Zona(String codigo, Municipio municipio, String nombre,
        String usuarioLog, Date fechaLog) {
        this.codigo = codigo;
        this.municipio = municipio;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Zona(String codigo, Municipio municipio, String nombre,
        String usuarioLog, Date fechaLog,
        Set<EstructuraOrganizacional> estructuraOrganizacionals,
        List<Jurisdiccion> jurisdiccions) {
        this.codigo = codigo;
        this.municipio = municipio;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.estructuraOrganizacionals = estructuraOrganizacionals;
        this.jurisdiccions = jurisdiccions;
    }

    // Property accessors
    @Id
    @Column(name = "CODIGO", nullable = false, length = 5)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "municipio")
    public Set<EstructuraOrganizacional> getEstructuraOrganizacionals() {
        return this.estructuraOrganizacionals;
    }

    public void setEstructuraOrganizacionals(
        Set<EstructuraOrganizacional> estructuraOrganizacionals) {
        this.estructuraOrganizacionals = estructuraOrganizacionals;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "municipio")
    public List<Jurisdiccion> getJurisdiccions() {
        return this.jurisdiccions;
    }

    public void setJurisdiccions(List<Jurisdiccion> jurisdiccions) {
        this.jurisdiccions = jurisdiccions;
    }

    public int compareTo(Zona p) {
        return this.codigo.compareTo(p.getCodigo());
    }

    public static Comparator<Zona> getComparatorNombre() {
        return new Comparator<Zona>() {
            public int compare(Zona p1, Zona p2) {
                return p1.nombre.compareTo(p2.nombre);
            }
        };
    }

}
