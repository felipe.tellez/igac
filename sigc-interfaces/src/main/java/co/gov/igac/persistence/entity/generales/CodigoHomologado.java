package co.gov.igac.persistence.entity.generales;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * CodigoHomologado entity. @author MyEclipse Persistence Tools
 */
@Cacheable
@Entity
@Table(name = "CODIGO_HOMOLOGADO", schema = "IGAC_GENERALES")
public class CodigoHomologado implements java.io.Serializable {

    // Fields
    /**
     * Serial
     */
    private static final long serialVersionUID = 8684845746004106736L;

    private Long id;
    private String nombreCodigo;
    private String sistema1;
    private String sistema2;
    private String valorSistema1;
    private String valorSistema2;
    private Date fechaDesde;
    private Date fechaHasta;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public CodigoHomologado() {
    }

    /** minimal constructor */
    public CodigoHomologado(Long id, String nombreCodigo, String sistema1,
        String sistema2, Date fechaDesde, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.nombreCodigo = nombreCodigo;
        this.sistema1 = sistema1;
        this.sistema2 = sistema2;
        this.fechaDesde = fechaDesde;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public CodigoHomologado(Long id, String nombreCodigo, String sistema1,
        String sistema2, String valorSistema1, String valorSistema2,
        Date fechaDesde, Date fechaHasta, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.nombreCodigo = nombreCodigo;
        this.sistema1 = sistema1;
        this.sistema2 = sistema2;
        this.valorSistema1 = valorSistema1;
        this.valorSistema2 = valorSistema2;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NOMBRE_CODIGO", nullable = false, length = 100)
    public String getNombreCodigo() {
        return this.nombreCodigo;
    }

    public void setNombreCodigo(String nombreCodigo) {
        this.nombreCodigo = nombreCodigo;
    }

    @Column(name = "SISTEMA_1", nullable = false, length = 30)
    public String getSistema1() {
        return this.sistema1;
    }

    public void setSistema1(String sistema1) {
        this.sistema1 = sistema1;
    }

    @Column(name = "SISTEMA_2", nullable = false, length = 30)
    public String getSistema2() {
        return this.sistema2;
    }

    public void setSistema2(String sistema2) {
        this.sistema2 = sistema2;
    }

    @Column(name = "VALOR_SISTEMA_1", length = 250)
    public String getValorSistema1() {
        return this.valorSistema1;
    }

    public void setValorSistema1(String valorSistema1) {
        this.valorSistema1 = valorSistema1;
    }

    @Column(name = "VALOR_SISTEMA_2", length = 250)
    public String getValorSistema2() {
        return this.valorSistema2;
    }

    public void setValorSistema2(String valorSistema2) {
        this.valorSistema2 = valorSistema2;
    }

    @Column(name = "FECHA_DESDE", nullable = false, length = 7)
    public Date getFechaDesde() {
        return this.fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    @Column(name = "FECHA_HASTA", length = 7)
    public Date getFechaHasta() {
        return this.fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
