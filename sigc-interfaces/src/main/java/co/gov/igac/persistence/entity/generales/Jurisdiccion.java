package co.gov.igac.persistence.entity.generales;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Jurisdiccion entity. @author MyEclipse Persistence Tools
 *
 * @modified by pedro.garcia - adición de anotaciones para la generación por secuencia del id
 *
 */
@Cacheable
@Entity
@Table(name = "JURISDICCION", schema = "IGAC_GENERALES")
public class Jurisdiccion implements java.io.Serializable {

    // Fields
    /**
     * Serial
     */
    private static final long serialVersionUID = -1725037325586369953L;

    private Long id;
    private EstructuraOrganizacional estructuraOrganizacional;
    private Municipio municipio;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public Jurisdiccion() {
    }

    /** full constructor */
    public Jurisdiccion(Long id,
        EstructuraOrganizacional estructuraOrganizacional,
        Municipio municipio, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.estructuraOrganizacional = estructuraOrganizacional;
        this.municipio = municipio;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Jurisdiccion0_ID_SEQ")
    @SequenceGenerator(name = "Jurisdiccion0_ID_SEQ", sequenceName = "JURISDICCION_ID_SEQ",
        allocationSize = 1)
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ESTRUCTURA_ORGANIZACIONAL_COD", nullable = false)
    public EstructuraOrganizacional getEstructuraOrganizacional() {
        return this.estructuraOrganizacional;
    }

    public void setEstructuraOrganizacional(
        EstructuraOrganizacional estructuraOrganizacional) {
        this.estructuraOrganizacional = estructuraOrganizacional;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
