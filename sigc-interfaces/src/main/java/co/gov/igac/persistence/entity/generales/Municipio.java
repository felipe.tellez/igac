package co.gov.igac.persistence.entity.generales;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.util.Constantes;

/**
 * MODIFICACIONES PROPIAS A LA CLASE Municipio: -> Se agregó el serialVersionUID. -> Se agregó el
 * Named Query: findMunicipioByCodigo -> Se agregó el método Transient getCodigo3Digitos que sirve
 * para obtener el código de municipio sin los dígitos de departamento.
 */
/**
 * Municipio entity. @author MyEclipse Persistence Tools FGWL Comparable, Comparator
 */
@Cacheable
@Entity
@NamedQueries({@NamedQuery(name = "municipio.findMunicipioByCodigo", query =
        "from Municipio m where m.codigo = :codigo")})
@Table(name = "MUNICIPIO", schema = "IGAC_GENERALES")
public class Municipio implements java.io.Serializable, Comparable<Municipio> {

    /**
     *
     */
    private static final long serialVersionUID = 641668052860632367L;
    // Fields

    private String codigo;
    private Departamento departamento;
    private String nombre;
    private String usuarioLog;
    private Date fechaLog;
    private Set<EstructuraOrganizacional> estructuraOrganizacionals =
        new HashSet<EstructuraOrganizacional>(
            0);
    private List<Jurisdiccion> jurisdiccions = new ArrayList<Jurisdiccion>(0);

    /**
     * Método creado para retornar el código requerido en la interfaz gráfica que debe tener sólo
     * los tres últimos dígitos (excluyendo el código de departamento)
     *
     * @return
     */
    @Transient
    public String getCodigo3Digitos() {
        return codigo.substring(2);
    }
    // Constructors

    /** default constructor */
    public Municipio() {
    }

    /** minimal constructor */
    public Municipio(String codigo, Departamento departamento, String nombre,
        String usuarioLog, Date fechaLog) {
        this.codigo = codigo;
        this.departamento = departamento;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Municipio(String codigo, Departamento departamento, String nombre,
        String usuarioLog, Date fechaLog,
        Set<EstructuraOrganizacional> estructuraOrganizacionals,
        List<Jurisdiccion> jurisdiccions) {
        this.codigo = codigo;
        this.departamento = departamento;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.estructuraOrganizacionals = estructuraOrganizacionals;
        this.jurisdiccions = jurisdiccions;
    }

    // Property accessors
    @Id
    @Column(name = "CODIGO", nullable = false, length = 5)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTAMENTO_ID", nullable = false)
    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "municipio")
    public Set<EstructuraOrganizacional> getEstructuraOrganizacionals() {
        return this.estructuraOrganizacionals;
    }

    public void setEstructuraOrganizacionals(
        Set<EstructuraOrganizacional> estructuraOrganizacionals) {
        this.estructuraOrganizacionals = estructuraOrganizacionals;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "municipio")
    public List<Jurisdiccion> getJurisdiccions() {
        return this.jurisdiccions;
    }

    public void setJurisdiccions(List<Jurisdiccion> jurisdiccions) {
        this.jurisdiccions = jurisdiccions;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Municipio || null != obj) {
            Municipio d = (Municipio) obj;
            if (this.codigo == null) {
                return false;
            }
            return this.codigo.equals(d.getCodigo());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.codigo != null ? this.codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Municipio p) {
        return this.codigo.compareTo(p.codigo);
    }

    public static Comparator<Municipio> getComparatorNombre() {
        return new Comparator<Municipio>() {
            @Override
            public int compare(Municipio p1, Municipio p2) {
                return p1.nombre.compareTo(p2.nombre);
            }
        };
    }

    /**
     * Retorna este objeto como un string. No tiene en cuenta los atributos que son listas de otros
     * objetos, ni los campos de log
     *
     * @author david.cifuentes
     */
    @Override
    public String toString() {

        StringBuilder objectAsString;
        String stringSeparator = Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR;

        objectAsString = new StringBuilder();
        objectAsString.append((this.codigo != null) ? this.codigo : "").append(stringSeparator);
        objectAsString.append((this.nombre != null) ? this.nombre : "").append(stringSeparator);

        return objectAsString.toString();
    }

}
