package co.gov.igac.persistence.entity.generales;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * CirculoRegistral entity.
 *
 * @modified by: pedro.garcia what: + declaración de implementación de Comparable + implementación
 * de método compareTo + implementación de método getNombreComparator
 */
@Cacheable
@Entity
@Table(name = "CIRCULO_REGISTRAL", schema = "IGAC_GENERALES")
public class CirculoRegistral implements java.io.Serializable, Comparable<CirculoRegistral> {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = -5982792357517246445L;

    private String codigo;
    private String nombre;
    private String oficina;
    private String usuarioLog;
    private Date fechaLog;
    private Set<CirculoRegistralMunicipio> circuloRegistralMunicipios =
        new HashSet<CirculoRegistralMunicipio>(
            0);

    // Constructors
    /** default constructor */
    public CirculoRegistral() {
    }

    /** minimal constructor */
    public CirculoRegistral(String codigo, String nombre, String oficina,
        String usuarioLog, Date fechaLog) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.oficina = oficina;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public CirculoRegistral(String codigo, String nombre, String oficina,
        String usuarioLog, Date fechaLog,
        Set<CirculoRegistralMunicipio> circuloRegistralMunicipios) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.oficina = oficina;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.circuloRegistralMunicipios = circuloRegistralMunicipios;
    }

    // Property accessors
    @Id
    @Column(name = "CODIGO", unique = true, nullable = false, length = 10)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "OFICINA", nullable = false, length = 100)
    public String getOficina() {
        return this.oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "circuloRegistral")
    public Set<CirculoRegistralMunicipio> getCirculoRegistralMunicipios() {
        return this.circuloRegistralMunicipios;
    }

    public void setCirculoRegistralMunicipios(
        Set<CirculoRegistralMunicipio> circuloRegistralMunicipios) {
        this.circuloRegistralMunicipios = circuloRegistralMunicipios;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Implementación del método compareTo para hacer esta clase un comparable
     *
     * @author pedro.garcia
     * @param o
     * @return
     */
    @Override
    public int compareTo(CirculoRegistral cr) {
        int answer;
        answer = this.codigo.compareToIgnoreCase(cr.getCodigo());
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna un Comparator que usa el atributo 'nombre' de esta clase para hacer la comparación de
     * dos instancias
     *
     * @author pedro.garcia
     * @return
     */
    public static Comparator<CirculoRegistral> getNombreComparator() {

        Comparator<CirculoRegistral> answer;

        answer = new Comparator<CirculoRegistral>() {

            @Override
            public int compare(CirculoRegistral cr1, CirculoRegistral cr2) {
                int answer;
                answer = cr1.nombre.compareToIgnoreCase(cr2.nombre);
                return answer;
            }
        };

        return answer;
    }

}
