package co.gov.igac.persistence.entity.generales;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the HORARIO_ATENCION database table.
 *
 */
@Cacheable
@Entity
@Table(name = "HORARIO_ATENCION", schema = "IGAC_GENERALES")
public class HorarioAtencionTerritorial implements Serializable {

    private static final long serialVersionUID = 1L;

    private String estructuraOrganizacionalCod;
    //lunes
    private Date lunesHoraInicialManhana;
    private Date lunesHoraFinalManhana;
    private Date lunesHoraInicialTarde;
    private Date lunesHoraFinalTarde;
    //martes
    private Date martesHoraInicialManhana;
    private Date martesHoraFinalManhana;
    private Date martesHoraInicialTarde;
    private Date martesHoraFinalTarde;
    //miercoles
    private Date miercolesHoraInicialManhana;
    private Date miercolesHoraFinalManhana;
    private Date miercolesHoraInicialTarde;
    private Date miercolesHoraFinalTarde;
    //jueves
    private Date juevesHoraInicialManhana;
    private Date juevesHoraFinalManhana;
    private Date juevesHoraInicialTarde;
    private Date juevesHoraFinalTarde;
    //viernes
    private Date viernesHoraInicialManhana;
    private Date viernesHoraFinalManhana;
    private Date viernesHoraInicialTarde;
    private Date viernesHoraFinalTarde;
    //sabado
    private Date sabadoHoraInicialManhana;
    private Date sabadoHoraFinalManhana;
    private Date sabadoHoraInicialTarde;
    private Date sabadoHoraFinalTarde;
    //domingo
    private Date domingoHoraInicialManhana;
    private Date domingoHoraFinalManhana;
    private Date domingoHoraInicialTarde;
    private Date domingoHoraFinalTarde;

    private String usuarioLog;
    private Date fechaLog;

    public HorarioAtencionTerritorial() {
    }

    @Id
    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD", length = 20)
    public String getEstructuraOrganizacionalCod() {
        return this.estructuraOrganizacionalCod;
    }

    public void setEstructuraOrganizacionalCod(
        String estructuraOrganizacionalCod) {
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "LUN_INI_AM")
    public Date getLunesHoraInicialManhana() {
        return lunesHoraInicialManhana;
    }

    public void setLunesHoraInicialManhana(Date lunesHoraInicialManhana) {
        this.lunesHoraInicialManhana = lunesHoraInicialManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "LUN_FIN_AM")
    public Date getLunesHoraFinalManhana() {
        return lunesHoraFinalManhana;
    }

    public void setLunesHoraFinalManhana(Date lunesHoraFinalManhana) {
        this.lunesHoraFinalManhana = lunesHoraFinalManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "LUN_INI_PM")
    public Date getLunesHoraInicialTarde() {
        return lunesHoraInicialTarde;
    }

    public void setLunesHoraInicialTarde(Date lunesHoraInicialTarde) {
        this.lunesHoraInicialTarde = lunesHoraInicialTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "LUN_FIN_PM")
    public Date getLunesHoraFinalTarde() {
        return lunesHoraFinalTarde;
    }

    public void setLunesHoraFinalTarde(Date lunesHoraFinalTarde) {
        this.lunesHoraFinalTarde = lunesHoraFinalTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "MAR_INI_AM")
    public Date getMartesHoraInicialManhana() {
        return martesHoraInicialManhana;
    }

    public void setMartesHoraInicialManhana(Date martesHoraInicialManhana) {
        this.martesHoraInicialManhana = martesHoraInicialManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "MAR_FIN_AM")
    public Date getMartesHoraFinalManhana() {
        return martesHoraFinalManhana;
    }

    public void setMartesHoraFinalManhana(Date martesHoraFinalManhana) {
        this.martesHoraFinalManhana = martesHoraFinalManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "MAR_INI_PM")
    public Date getMartesHoraInicialTarde() {
        return martesHoraInicialTarde;
    }

    public void setMartesHoraInicialTarde(Date martesHoraInicialTarde) {
        this.martesHoraInicialTarde = martesHoraInicialTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "MAR_FIN_PM")
    public Date getMartesHoraFinalTarde() {
        return martesHoraFinalTarde;
    }

    public void setMartesHoraFinalTarde(Date martesHoraFinalTarde) {
        this.martesHoraFinalTarde = martesHoraFinalTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "MIE_INI_AM")
    public Date getMiercolesHoraInicialManhana() {
        return miercolesHoraInicialManhana;
    }

    public void setMiercolesHoraInicialManhana(Date miercolesHoraInicialManhana) {
        this.miercolesHoraInicialManhana = miercolesHoraInicialManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "MIE_FIN_AM")
    public Date getMiercolesHoraFinalManhana() {
        return miercolesHoraFinalManhana;
    }

    public void setMiercolesHoraFinalManhana(Date miercolesHoraFinalManhana) {
        this.miercolesHoraFinalManhana = miercolesHoraFinalManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "MIE_INI_PM")
    public Date getMiercolesHoraInicialTarde() {
        return miercolesHoraInicialTarde;
    }

    public void setMiercolesHoraInicialTarde(Date miercolesHoraInicialTarde) {
        this.miercolesHoraInicialTarde = miercolesHoraInicialTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "MIE_FIN_PM")
    public Date getMiercolesHoraFinalTarde() {
        return miercolesHoraFinalTarde;
    }

    public void setMiercolesHoraFinalTarde(Date miercolesHoraFinalTarde) {
        this.miercolesHoraFinalTarde = miercolesHoraFinalTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "JUE_INI_AM")
    public Date getJuevesHoraInicialManhana() {
        return juevesHoraInicialManhana;
    }

    public void setJuevesHoraInicialManhana(Date juevesHoraInicialManhana) {
        this.juevesHoraInicialManhana = juevesHoraInicialManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "JUE_FIN_AM")
    public Date getJuevesHoraFinalManhana() {
        return juevesHoraFinalManhana;
    }

    public void setJuevesHoraFinalManhana(Date juevesHoraFinalManhana) {
        this.juevesHoraFinalManhana = juevesHoraFinalManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "JUE_INI_PM")
    public Date getJuevesHoraInicialTarde() {
        return juevesHoraInicialTarde;
    }

    public void setJuevesHoraInicialTarde(Date juevesHoraInicialTarde) {
        this.juevesHoraInicialTarde = juevesHoraInicialTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "JUE_FIN_PM")
    public Date getJuevesHoraFinalTarde() {
        return juevesHoraFinalTarde;
    }

    public void setJuevesHoraFinalTarde(Date juevesHoraFinalTarde) {
        this.juevesHoraFinalTarde = juevesHoraFinalTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "VIE_INI_AM")
    public Date getViernesHoraInicialManhana() {
        return viernesHoraInicialManhana;
    }

    public void setViernesHoraInicialManhana(Date viernesHoraInicialManhana) {
        this.viernesHoraInicialManhana = viernesHoraInicialManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "VIE_FIN_AM")
    public Date getViernesHoraFinalManhana() {
        return viernesHoraFinalManhana;
    }

    public void setViernesHoraFinalManhana(Date viernesHoraFinalManhana) {
        this.viernesHoraFinalManhana = viernesHoraFinalManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "VIE_INI_PM")
    public Date getViernesHoraInicialTarde() {
        return viernesHoraInicialTarde;
    }

    public void setViernesHoraInicialTarde(Date viernesHoraInicialTarde) {
        this.viernesHoraInicialTarde = viernesHoraInicialTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "VIE_FIN_PM")
    public Date getViernesHoraFinalTarde() {
        return viernesHoraFinalTarde;
    }

    public void setViernesHoraFinalTarde(Date viernesHoraFinalTarde) {
        this.viernesHoraFinalTarde = viernesHoraFinalTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "SAB_INI_AM")
    public Date getSabadoHoraInicialManhana() {
        return sabadoHoraInicialManhana;
    }

    public void setSabadoHoraInicialManhana(Date sabadoHoraInicialManhana) {
        this.sabadoHoraInicialManhana = sabadoHoraInicialManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "SAB_FIN_AM")
    public Date getSabadoHoraFinalManhana() {
        return sabadoHoraFinalManhana;
    }

    public void setSabadoHoraFinalManhana(Date sabadoHoraFinalManhana) {
        this.sabadoHoraFinalManhana = sabadoHoraFinalManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "SAB_INI_PM")
    public Date getSabadoHoraInicialTarde() {
        return sabadoHoraInicialTarde;
    }

    public void setSabadoHoraInicialTarde(Date sabadoHoraInicialTarde) {
        this.sabadoHoraInicialTarde = sabadoHoraInicialTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "SAB_FIN_PM")
    public Date getSabadoHoraFinalTarde() {
        return sabadoHoraFinalTarde;
    }

    public void setSabadoHoraFinalTarde(Date sabadoHoraFinalTarde) {
        this.sabadoHoraFinalTarde = sabadoHoraFinalTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "DOM_INI_AM")
    public Date getDomingoHoraInicialManhana() {
        return domingoHoraInicialManhana;
    }

    public void setDomingoHoraInicialManhana(Date domingoHoraInicialManhana) {
        this.domingoHoraInicialManhana = domingoHoraInicialManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "DOM_FIN_AM")
    public Date getDomingoHoraFinalManhana() {
        return domingoHoraFinalManhana;
    }

    public void setDomingoHoraFinalManhana(Date domingoHoraFinalManhana) {
        this.domingoHoraFinalManhana = domingoHoraFinalManhana;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "DOM_INI_PM")
    public Date getDomingoHoraInicialTarde() {
        return domingoHoraInicialTarde;
    }

    public void setDomingoHoraInicialTarde(Date domingoHoraInicialTarde) {
        this.domingoHoraInicialTarde = domingoHoraInicialTarde;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "DOM_FIN_PM")
    public Date getDomingoHoraFinalTarde() {
        return domingoHoraFinalTarde;
    }

    public void setDomingoHoraFinalTarde(Date domingoHoraFinalTarde) {
        this.domingoHoraFinalTarde = domingoHoraFinalTarde;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
