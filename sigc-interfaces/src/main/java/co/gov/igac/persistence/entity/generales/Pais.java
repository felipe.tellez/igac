package co.gov.igac.persistence.entity.generales;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;

/**
 * Pais entity. @author MyEclipse Persistence Tools FGWL Se hizo Comparable FGWL Se agrego
 * Comparator
 */
@Cacheable
@Entity
@Table(name = "PAIS", schema = "IGAC_GENERALES")
public class Pais implements java.io.Serializable, Comparable<Pais> {

    // Fields
    /**
     *
     */
    private static final long serialVersionUID = 108817586063381827L;
    private String codigo;
    private String nombre;
    private String usuarioLog;
    private Date fechaLog;
    private Set<Departamento> departamentos = new HashSet<Departamento>(0);

    // Constructors
    /** default constructor */
    public Pais() {
    }

    /** minimal constructor */
    public Pais(String codigo, String nombre, String usuarioLog, Date fechaLog) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Pais(String codigo, String nombre, String usuarioLog, Date fechaLog,
        Set<Departamento> departamentos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.departamentos = departamentos;
    }

    // Property accessors
    @Id
    @Column(name = "CODIGO", nullable = false, length = 10)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name = "NOMBRE", nullable = false, length = 50)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "pais")
    public Set<Departamento> getDepartamentos() {
        return this.departamentos;
    }

    public void setDepartamentos(Set<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public int compareTo(Pais p) {
        return StringUtils.leftPad(this.codigo, 3, '0').compareTo(StringUtils.leftPad(p.codigo, 3,
            '0'));
    }

    public static Comparator<Pais> getComparatorNombre() {
        return new Comparator<Pais>() {
            public int compare(Pais p1, Pais p2) {
                return p1.nombre.compareTo(p2.nombre);
            }
        };
    }

}
