package co.gov.igac.persistence.entity.generales;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * CirculoRegistralMunicipio entity. @author MyEclipse Persistence Tools
 */
@Cacheable
@Entity
@Table(name = "CIRCULO_REGISTRAL_MUNICIPIO", schema = "IGAC_GENERALES")
public class CirculoRegistralMunicipio implements java.io.Serializable {

    // Fields
    /**
     * Serial
     */
    private static final long serialVersionUID = -7081744887053427009L;

    private Long id;
    private Municipio municipio;
    private CirculoRegistral circuloRegistral;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public CirculoRegistralMunicipio() {
    }

    /** full constructor */
    public CirculoRegistralMunicipio(Long id, Municipio municipio,
        CirculoRegistral circuloRegistral, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.municipio = municipio;
        this.circuloRegistral = circuloRegistral;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    // Property accessors
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO", nullable = false)
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CIRCULO_REGISTRAL_CODIGO", nullable = false)
    public CirculoRegistral getCirculoRegistral() {
        return this.circuloRegistral;
    }

    public void setCirculoRegistral(CirculoRegistral circuloRegistral) {
        this.circuloRegistral = circuloRegistral;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

}
