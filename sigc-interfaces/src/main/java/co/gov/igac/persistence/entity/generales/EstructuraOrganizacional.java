package co.gov.igac.persistence.entity.generales;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.util.Constantes;

/**
 * EstructuraOrganizacional entity.
 */
/*
 * FGWL Comparable y Comparator
 *
 * fabio.navarrete: -> se crea el método transient isDireccionGeneral -> se crea el método transient
 * isSubdireccion -> se crea el método transient isOficinaApoyo
 *
 */
@Cacheable
@Entity
@Table(name = "ESTRUCTURA_ORGANIZACIONAL", schema = "IGAC_GENERALES")
public class EstructuraOrganizacional implements java.io.Serializable,
    Comparable<EstructuraOrganizacional> {

    /**
     *
     */
    private static final long serialVersionUID = -9183905881915709319L;
    // Fields

    private String codigo;
    private Funcionario funcionario;
    private Municipio municipio;
    private String nombre;
    private String tipo;
    private String estructuraOrganizacionalCod;
    private String direccion;
    private String telefono;
    private String usuarioLog;
    private Date fechaLog;
    private Set<Jurisdiccion> jurisdiccions = new HashSet<Jurisdiccion>(0);
    private Set<Funcionario> funcionarios = new HashSet<Funcionario>(0);

    // Constructors
    /** default constructor */
    public EstructuraOrganizacional() {
    }

    /** minimal constructor */
    public EstructuraOrganizacional(String codigo, String nombre, String tipo,
        String usuarioLog, Date fechaLog) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.tipo = tipo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public EstructuraOrganizacional(String codigo, Funcionario funcionario,
        Municipio municipio, String nombre, String tipo,
        String estructuraOrganizacionalCod, String direccion,
        String telefono, String usuarioLog, Date fechaLog,
        Set<Jurisdiccion> jurisdiccions, Set<Funcionario> funcionarios) {
        this.codigo = codigo;
        this.funcionario = funcionario;
        this.municipio = municipio;
        this.nombre = nombre;
        this.tipo = tipo;
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
        this.direccion = direccion;
        this.telefono = telefono;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.jurisdiccions = jurisdiccions;
        this.funcionarios = funcionarios;
    }

    // Property accessors
    @Id
    @Column(name = "CODIGO", nullable = false, length = 20)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FUNCIONARIO_ID")
    public Funcionario getFuncionario() {
        return this.funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MUNICIPIO_CODIGO")
    public Municipio getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "TIPO", nullable = false, length = 50)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "ESTRUCTURA_ORGANIZACIONAL_COD", nullable = false, length = 20)
    public String getEstructuraOrganizacionalCod() {
        return this.estructuraOrganizacionalCod;
    }

    public void setEstructuraOrganizacionalCod(String estructuraOrganizacionalCod) {
        this.estructuraOrganizacionalCod = estructuraOrganizacionalCod;
    }

    @Column(name = "DIRECCION", length = 100)
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "TELEFONO", length = 50)
    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
        "estructuraOrganizacional")
    public Set<Jurisdiccion> getJurisdiccions() {
        return this.jurisdiccions;
    }

    public void setJurisdiccions(Set<Jurisdiccion> jurisdiccions) {
        this.jurisdiccions = jurisdiccions;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
        "estructuraOrganizacional")
    public Set<Funcionario> getFuncionarios() {
        return this.funcionarios;
    }

    public void setFuncionarios(Set<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

//--------------------------------------------------------------------------------------------------
    @Override
    public int compareTo(EstructuraOrganizacional p) {
        return this.codigo.compareTo(p.codigo);
    }

    public static Comparator<EstructuraOrganizacional> getComparatorNombre() {
        return new Comparator<EstructuraOrganizacional>() {
            @Override
            public int compare(EstructuraOrganizacional p1, EstructuraOrganizacional p2) {
                return p1.nombre.compareTo(p2.nombre);
            }
        };
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Determina si la actual estructura organizacional es la Dirección General
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isDireccionGeneral() {
        if (this.codigo != null) {
            return this.codigo.equals(Constantes.DIRECCION_GENERAL_CODIGO);
        }
        return false;
    }

    /**
     * Determina si la actual estructura organizacional es una Subdirección
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isSubdireccion() {
        if (this.tipo != null) {
            return this.tipo.equals(EEstructuraOrganizacional.SUBDIRECCION.getTipo());
        }
        return false;
    }

    /**
     * Determina si la actual estructura organizacional es una Oficina de apoyo
     *
     * @return
     * @author fabio.navarrete
     */
    @Transient
    public boolean isOficinaApoyo() {
        if (this.tipo != null) {
            return this.tipo.equals(EEstructuraOrganizacional.OFICINA_APOYO.getTipo());
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EstructuraOrganizacional) {
            if (((EstructuraOrganizacional) obj).getCodigo().equals(this.codigo)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna este objeto como un string. No tiene en cuenta los atributos que son listas de otros
     * objetos, ni los campos de log
     *
     * @author david.cifuentes
     */
    @Override
    public String toString() {

        StringBuilder objectAsString;
        String stringSeparator = Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR;

        objectAsString = new StringBuilder();
        objectAsString.append((this.codigo != null) ? this.codigo : "").append(stringSeparator);
        objectAsString.append((this.nombre != null) ? this.nombre : "").append(stringSeparator);
        objectAsString.append((this.tipo != null) ? this.tipo : "").append(stringSeparator);
        objectAsString.append((this.estructuraOrganizacionalCod != null) ?
            this.estructuraOrganizacionalCod : "").append(stringSeparator);

        return objectAsString.toString();
    }

}
