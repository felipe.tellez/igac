package co.gov.igac.persistence.entity.generales;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Profesion entity. @author MyEclipse Persistence Tools
 */
@Cacheable
@Entity
@Table(name = "PROFESION", schema = "IGAC_GENERALES")
public class Profesion implements java.io.Serializable {

    // Fields
    /**
     * Serial
     */
    private static final long serialVersionUID = -6925960225069283578L;

    private Long id;
    private String nombre;
    private String usuarioLog;
    private Date fechaLog;
    private Set<Funcionario> funcionarios = new HashSet<Funcionario>(0);

    // Constructors
    /** default constructor */
    public Profesion() {
    }

    /** minimal constructor */
    public Profesion(Long id, String nombre, String usuarioLog, Date fechaLog) {
        this.id = id;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Profesion(Long id, String nombre, String usuarioLog, Date fechaLog,
        Set<Funcionario> funcionarios) {
        this.id = id;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.funcionarios = funcionarios;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "profesion")
    public Set<Funcionario> getFuncionarios() {
        return this.funcionarios;
    }

    public void setFuncionarios(Set<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

}
