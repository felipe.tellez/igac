package co.gov.igac.persistence.entity.generales;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.gov.igac.snc.util.Constantes;

/**
 * MODIFICACIONES PROPIAS A LA CLASE Departamento: -> Se agregó el serialVersionUID. -> Se agregó el
 * Named Query: findDepartamentoByCodigo
 */
/**
 * Departamento entity. @author MyEclipse Persistence Tools FGWL Comparable y Comparator
 */
@Cacheable
@Entity
@NamedQueries({@NamedQuery(name = "findDepartamentoByCodigo", query =
        "from Departamento d where d.codigo = :codigo")})
@Table(name = "DEPARTAMENTO", schema = "IGAC_GENERALES")
public class Departamento implements java.io.Serializable, Comparable<Departamento> {

    /**
     *
     */
    private static final long serialVersionUID = 1278757040789257108L;
    // Fields

    private String codigo;
    private Pais pais;
    private String nombre;
    private String usuarioLog;
    private Date fechaLog;
    private Set<Municipio> municipios = new HashSet<Municipio>(0);

    // Constructors
    /** default constructor */
    public Departamento() {
    }

    /** minimal constructor */
    public Departamento(String codigo, Pais pais, String nombre,
        String usuarioLog, Date fechaLog) {
        this.codigo = codigo;
        this.pais = pais;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Departamento(String codigo, Pais pais, String nombre,
        String usuarioLog, Date fechaLog, Set<Municipio> municipios) {
        this.codigo = codigo;
        this.pais = pais;
        this.nombre = nombre;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.municipios = municipios;
    }

    // Property accessors
    @Id
    @Column(name = "CODIGO", nullable = false, length = 2)
    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PAIS_ID", nullable = false)
    public Pais getPais() {
        return this.pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    @Column(name = "NOMBRE", nullable = false, length = 100)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "departamento")
    public Set<Municipio> getMunicipios() {
        return this.municipios;
    }

    public void setMunicipios(Set<Municipio> municipios) {
        this.municipios = municipios;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Departamento || null != obj) {
            Departamento d = (Departamento) obj;
            if (this.codigo == null) {
                return false;
            }
            return this.codigo.equals(d.getCodigo());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.codigo != null ? this.codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Departamento p) {
        return this.codigo.compareTo(p.codigo);
    }

    public static Comparator<Departamento> getComparatorNombre() {
        return new Comparator<Departamento>() {
            @Override
            public int compare(Departamento p1, Departamento p2) {
                return p1.nombre.compareTo(p2.nombre);
            }
        };
    }

    /**
     * Retorna este objeto como un string. No tiene en cuenta los atributos que son listas de otros
     * objetos, ni los campos de log
     *
     * @author david.cifuentes
     */
    @Override
    public String toString() {

        StringBuilder objectAsString;
        String stringSeparator = Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR;

        objectAsString = new StringBuilder();
        objectAsString.append((this.codigo != null) ? this.codigo : "").append(stringSeparator);
        objectAsString.append((this.nombre != null) ? this.nombre : "").append(stringSeparator);

        return objectAsString.toString();
    }
}
