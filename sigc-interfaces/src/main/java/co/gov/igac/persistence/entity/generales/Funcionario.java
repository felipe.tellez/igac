package co.gov.igac.persistence.entity.generales;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Funcionario entity. @author MyEclipse Persistence Tools
 */
@Cacheable
@Entity
@Table(name = "FUNCIONARIO", schema = "IGAC_GENERALES")
public class Funcionario implements java.io.Serializable {

    // Fields
    /**
     * Serial
     */
    private static final long serialVersionUID = -578256546205470239L;

    @GeneratedValue
    private Long id;
    private EstructuraOrganizacional estructuraOrganizacional;
    private Profesion profesion;
    private Long identificacion;
    private String nombres;
    private String apellidos;
    private String correoElectronico;
    private String tipo;
    private String cargo;
    private String inactivo;
    private String usuarioLog;
    private Date fechaLog;
    private Set<EstructuraOrganizacional> estructuraOrganizacionals =
        new HashSet<EstructuraOrganizacional>(
            0);

    // Constructors
    /** default constructor */
    public Funcionario() {
    }

    /** minimal constructor */
    public Funcionario(Long id, Long identificacion, String nombres,
        String apellidos, String tipo, String inactivo, String usuarioLog,
        Date fechaLog) {
        this.id = id;
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.tipo = tipo;
        this.inactivo = inactivo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
    }

    /** full constructor */
    public Funcionario(Long id,
        EstructuraOrganizacional estructuraOrganizacional,
        Profesion profesion, Long identificacion, String nombres,
        String apellidos, String correoElectronico, String tipo,
        String cargo, String inactivo, String usuarioLog, Date fechaLog,
        Set<EstructuraOrganizacional> estructuraOrganizacionals) {
        this.id = id;
        this.estructuraOrganizacional = estructuraOrganizacional;
        this.profesion = profesion;
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correoElectronico = correoElectronico;
        this.tipo = tipo;
        this.cargo = cargo;
        this.inactivo = inactivo;
        this.usuarioLog = usuarioLog;
        this.fechaLog = fechaLog;
        this.estructuraOrganizacionals = estructuraOrganizacionals;
    }

    // Property accessors
    @Id
    @Column(name = "ID", nullable = false, precision = 10, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ESTRUCTURA_ORGANIZACIONAL_COD")
    public EstructuraOrganizacional getEstructuraOrganizacional() {
        return this.estructuraOrganizacional;
    }

    public void setEstructuraOrganizacional(
        EstructuraOrganizacional estructuraOrganizacional) {
        this.estructuraOrganizacional = estructuraOrganizacional;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROFESION_ID")
    public Profesion getProfesion() {
        return this.profesion;
    }

    public void setProfesion(Profesion profesion) {
        this.profesion = profesion;
    }

    @Column(name = "IDENTIFICACION", nullable = false, precision = 10, scale = 0)
    public Long getIdentificacion() {
        return this.identificacion;
    }

    public void setIdentificacion(Long identificacion) {
        this.identificacion = identificacion;
    }

    @Column(name = "NOMBRES", nullable = false, length = 50)
    public String getNombres() {
        return this.nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @Column(name = "APELLIDOS", nullable = false, length = 50)
    public String getApellidos() {
        return this.apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Column(name = "CORREO_ELECTRONICO", length = 100)
    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Column(name = "TIPO", nullable = false, length = 20)
    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "CARGO", length = 100)
    public String getCargo() {
        return this.cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Column(name = "INACTIVO", nullable = false, length = 1)
    public String getInactivo() {
        return this.inactivo;
    }

    public void setInactivo(String inactivo) {
        this.inactivo = inactivo;
    }

    @Column(name = "USUARIO_LOG", nullable = false, length = 100)
    public String getUsuarioLog() {
        return this.usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_LOG", nullable = false, length = 7)
    public Date getFechaLog() {
        return this.fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "funcionario")
    public Set<EstructuraOrganizacional> getEstructuraOrganizacionals() {
        return this.estructuraOrganizacionals;
    }

    public void setEstructuraOrganizacionals(
        Set<EstructuraOrganizacional> estructuraOrganizacionals) {
        this.estructuraOrganizacionals = estructuraOrganizacionals;
    }

}
