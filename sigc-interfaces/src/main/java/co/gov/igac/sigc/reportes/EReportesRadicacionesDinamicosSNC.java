package co.gov.igac.sigc.reportes;

/**
 *
 * @author leidy.gonzalez
 */
public enum EReportesRadicacionesDinamicosSNC {

    PREDIOS_CON_MATRICULAS_REGISTRALES(235L, "RADICACIONES DE PREDIOS CON MATRÍCULAS REGISTRALES"),
    RADICACIONES_ROL_FUNCIONARIO_CONSOLIDADO(85L, "RADICACIONES ROL/FUNCIONARIO CONSOLIDADO"),
    RADICACIONES_ROL_FUNCIONARIO_DETALLADO(220L, "RADICACIONES ROL/FUNCIONARIO DETALLADO"),
    RADICACIONES_CONSOLIDADO(221L, "RADICACIONES CONSOLIDADO"),
    RADICACIONES_DETALLADO(222L, "RADICACIONES DETALLADO"),
    RADICACIONES_TIPO_TRAMITE(223L, "RADICACIONES TIPO TRAMITE"),
    RADICACIONES_TERRENO_Y_OFICINA(224L, "RADICACIONES TERRENO Y OFICINA"),
    RADICACIONES_FINALIZADAS(225L, "RADICACIONES FINALIZADAS"),
    RADICACIONES_EN_PROCESO(226L, "RADICACIONES EN PROCESO"),
    RADICACIONES_CANCELADAS(227L, "RADICACIONES CANCELADAS"),
    RADICACIONES_ARCHIVADAS(228L, "RADICACIONES ARCHIVADAS"),
    RADICACIONES_RECHAZADAS(229L, "RADICACIONES RECHAZADAS"),
    RADICACIONES_POR_ACTIVIDAD(230L, "RADICACIONES POR ACTIVIDAD"),
    RADICACIONES_DERECHO_DE_PETICION(231L, "RADICACIONES DERECHO DE PETICION"),
    RADICACIONES_POR_TUTELA(232L, "RADICACIONES POR TUTELA"),
    RADICACIONES_TRAMITES_Y_ACTIVIDADES(233L, "TRAMITES Y ACTIVIDADES");

    private Long idConfiguracionReporte;
    private String nombreConfiguracionReporte;

    private EReportesRadicacionesDinamicosSNC(Long idConfiguracionReporte,
        String nombreConfiguracionReporte) {
        this.idConfiguracionReporte = idConfiguracionReporte;
    }

    /**
     * Método que devuelve el id de configuración de reporte
     *
     * @return
     */
    public Long getIdConfiguracionReporte() {
        return idConfiguracionReporte;
    }

    /**
     * Método que devuelve el nombre de configuración de reporte
     *
     * @return
     */
    public String getNombreConfiguracionReporte() {
        return nombreConfiguracionReporte;
    }

}
