package co.gov.igac.sigc.reportes;

/**
 *
 * @author juan.mendez
 * @author javier.aponte
 * @author javier.barajas
 */
public enum EReportesPredialesDinamicosSNC {

    DATOS_BASICOS(2L),
    DATOS_BASICOS_SIN_PROPIETARIO(3L),
    DATOS_EXTENDIDOS(4L),
    DATOS_EXTENDIDOS_SIN_PROPIETARIO(5L),
    DATOS_JURIDICOS(6L),
    DATOS_FISICOS(7L),
    DATOS_ECONOMICOS(8L),
    DATOS_CONSTRUCCIONES_CONVENCIONALES(9L),
    DATOS_CONSTRUCCIONES_NO_CONVENCIONALES(10L),
    DATOS_FICHA_MATRIZ(11L);

    private Long idConfiguracionReporte;

    private EReportesPredialesDinamicosSNC(Long idConfiguracionReporte) {
        this.idConfiguracionReporte = idConfiguracionReporte;
    }

    /**
     * Método que devuelve el id de configuración de reporte
     *
     * @return
     */
    public Long getIdConfiguracionReporte() {
        return idConfiguracionReporte;
    }

}
