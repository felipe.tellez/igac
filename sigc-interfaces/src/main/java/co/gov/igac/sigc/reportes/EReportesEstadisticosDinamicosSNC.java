package co.gov.igac.sigc.reportes;

/**
 * Enumeración que contiene los id's de los reportes dinamicos estadísticos que están almacenados en
 * la tabla CONFIGURACION_REPORTE
 *
 * @author javier.aponte
 *
 */
public enum EReportesEstadisticosDinamicosSNC {

    ESTADISTICAS_MANZANAS_A_NIVEL_DE_SECTOR(1L);

    private Long idConfiguracionReporte;

    private EReportesEstadisticosDinamicosSNC(Long idConfiguracionReporte) {
        this.idConfiguracionReporte = idConfiguracionReporte;
    }

    /**
     * Método que devuelve el id de configuración de reporte
     *
     * @return
     */
    public Long getIdConfiguracionReporte() {
        return idConfiguracionReporte;
    }

}
