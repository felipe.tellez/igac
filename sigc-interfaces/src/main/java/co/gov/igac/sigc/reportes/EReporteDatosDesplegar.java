package co.gov.igac.sigc.reportes;

/**
 * Enumeración que contiene los id's de los tipos de datos a desplegar en los reportes prediales
 *
 * @author javier.aponte
 * @modified by leidy.gonzalez
 */
public enum EReporteDatosDesplegar {

    DATOS_BASICOS_DE_PREDIO(2L),
    DATOS_BASICOS_SIN_PROPIETARIO(1L),
    DATOS_BASICOS_ECONOMICOS_PREDIO(3L),
    DATOS_BASICOS_JURIDICOS_PREDIO(4L),
    DATOS_BASICOS_PREDIO_Y_CONSTRUCCIONES_CONVENCIONALES(5L),
    DATOS_BASICOS_PREDIO_Y_CONSTRUCCIONES_NO_CONVENCIONALES(6L),
    DATOS_BASICOS_Y_FISICOS_DE_PREDIO(7L),
    DATOS_FICHA_MATRIZ(8L),
    DATOS_EXTENDIDOS_DE_PREDIOS(9L),
    DATOS_EXTENDIDOS_DE_PREDIOS_Y_PROPIETARIOS(10L);

    private Long codigo;

    private EReporteDatosDesplegar(Long codigo) {
        this.codigo = codigo;
    }

    /**
     * Método que devuelve el código del reporte
     *
     * @return
     */
    public Long getCodigo() {
        return codigo;
    }

}
