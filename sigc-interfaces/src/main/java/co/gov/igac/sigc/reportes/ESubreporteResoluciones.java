package co.gov.igac.sigc.reportes;

/**
 * Contiene los nombres de los subreportes de la parte del resuelve de las resoluciones
 *
 * @author javier.aponte
 *
 */
public enum ESubreporteResoluciones {

    RESOLUCION_CANCELACION("Cancelacion"),
    RESOLUCION_CANCELACION_MASIVA("CancelacionMasiva"),
    RESOLUCION_COMPLEMENTACION("Complementacion"),
    RESOLUCION_MODIFICACION_INFORMACION_CATASTRAL("ModificacionInformacionCatastral"),
    RESOLUCION_MUTACION_PRIMERA("MutacionPrimera"),
    RESOLUCION_MUTACION_SEGUNDA("MutacionSegunda"),
    RESOLUCION_MUTACION_TERCERA("MutacionTercera"),
    RESOLUCION_MUTACION_CUARTA("MutacionCuarta"),
    RESOLUCION_MUTACION_QUINTA("MutacionQuinta"),
    RESOLUCION_RECTIFICACION("Rectificacion"),
    RESOLUCION_RECTIFICACION_MATRIZ("RectificacionMatriz"),
    RESOLUCION_REVISION_AVALUO("RevisionAvaluo"),
    RESOLUCION_MUTACION_TERCERA_MASIVA("MutacionTerceraMasiva");

    private String urlSubreporte;

    private ESubreporteResoluciones(String urlSubreporte) {
        this.urlSubreporte = urlSubreporte;
    }

    /**
     *
     * @return
     */
    public String getUrlSubreporte() {
        return this.urlSubreporte;
    }
}
