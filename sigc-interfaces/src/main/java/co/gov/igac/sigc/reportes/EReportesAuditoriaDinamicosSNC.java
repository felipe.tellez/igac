package co.gov.igac.sigc.reportes;

/**
 *
 * @author juan.mendez
 * @author javier.aponte
 * @author javier.barajas
 */
public enum EReportesAuditoriaDinamicosSNC {

    AUDITORIA_NUMERO_RESOLUCIONES(92L),
    AUDITORIA_LISTA_CANCELACIONES(93L),
    AUDITORIA_LISTA_CAMBIOS_AREA(94L),
    AUDITORIA_LISTA_VARIAS_MUTACIONES(95L),
    AUDITORIA_LISTA_RESOLUCIONES(96L),
    AUDITORIA_LISTA_CANCELA_INSCRIBE_IGUAL(97L),
    AUDITORIA_LISTA_CANCELA_INSCRIBE_MAYOR(98L),
    AUDITORIA_LISTA_CANCELA_INSCRIBE_MENOR(99L),
    AUDITORIA_LISTA_INSCRIPCIONES_DEFINITIVAS(100L);

    private Long idConfiguracionReporte;

    private EReportesAuditoriaDinamicosSNC(Long idConfiguracionReporte) {
        this.idConfiguracionReporte = idConfiguracionReporte;
    }

    /**
     * Método que devuelve el id de configuración de reporte
     *
     * @return
     */
    public Long getIdConfiguracionReporte() {
        return idConfiguracionReporte;
    }

}
