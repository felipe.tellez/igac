package co.gov.igac.sigc.reportes;

/**
 * Enumeración que contiene las rutas en el servidor de jasperServer de cada uno de los reportes que
 * hay en el sistema
 *
 * @author juan.mendez
 * @author javier.aponte
 * @modified leidy.gonzalez::12615::21/05/2015 Se modifica ruta de reporte
 * REPORTES_PREDIALES_DATOS_CONSTRUCCIONES_NO_CONVENCIONALES
 */
public enum EReporteServiceSNC {

    //Url para verificar que el servidor de reportes está funcionando correctamente
    REPORTE_PRUEBA("/reports/gestioncatastral/interno/documentosdetrabajo/prueba", "Prueba"),
    ASIGNAR_RESPONSABLE_ACTUALIZACION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/asignarResponsableactualizacion",
        "Asignar Responsable Actualizacion"),
    AUTO_DE_PRUEBAS(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/autoDePruebas",
        "Auto De Pruebas"),
    AUTORIZACION_CONTRATACION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/autorizacionContratacion",
        "Autorizacion Contratacion"),
    AVISO_PROPIETARIO_CON_DIRECCION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/validacion/avisoPropietarioConDireccion",
        "Aviso Propietario Con Dirección"),
    BLOQUEO_MASIVO_PERSONAS(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/personasBloqueadas",
        "Personas Bloqueadas"),
    BLOQUEO_MASIVO_PREDIOS(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/prediosBloqueados",
        "Predios Bloqueados"),
    CANCELACION_TRAMITE(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/cancelacionTramite",
        "Cancelación Trámite"),
    CERTIFICADO_NACIONAL_POR_PROPIETARIO_POR_PERSONA(
        "/reports/gestioncatastral/interno/productoscartograficos/certificadNacionalPorPersona",
        "Certificado nacional por propietario por predio"),
    CERTIFICADO_NACIONAL_POR_PROPIETARIO_POR_PREDIO(
        "/reports/gestioncatastral/interno/productoscartograficos/certificadoNacionalConPropietarios",
        "Certificado nacional por propietario por predio"),
    COMUNICACION_AUTO_DE_PRUEBAS_A_ENTIDAD_EXTERNA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/comunicacionAutoDePruebasAEntidadesExternas",
        "Comunicacion Auto De Pruebas A Entidades Externas"),
    COMUNICACION_AUTO_DE_PRUEBAS_A_GRUPO_INTERNO_DE_TRABAJO(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/comunicacionAutoDePruebasAGrupoInternoDeTrabajo",
        "Comunicacion Auto De Pruebas A Grupo Interno De Trabajo"),
    COMUNICACION_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_O_AFECTADOS(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/comunicacionAutoDePruebasASolicitantesYOAfectados",
        "Comunicacion Auto De Pruebas A Solicitantes Y O Afectados"),
    COMUNICACION_DE_NOTIFICACION_PERSONAL(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/validacion/comunicacionDeNotificacionPersonal",
        "Comunicacion De Notificacion Personal"),
    COMUNICACION_DE_NOTIFICACION_PERSONAL_MASIVA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/validacion/comunicacionDeNotificacionPersonalMasiva",
        "Comunicacion De Notificacion Personal Masiva"),
    COMUNICACION_RESOLUCION_ALCALDE(
        "/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/comunicacionResolucionalcalde",
        "Comunicacion Resolucion Alcalde"),
    CONSTANCIA_RADICACION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/tramite/radicacion/constanciaRadicacion",
        "Constancia Radicacion"),
    AVISOS_REGISTRO_DESCARTADOS(
        "/reports/gestioncatastral/interno/documentosdetrabajo/tramite/Solicitud/AvisosRegistroDescartados",
        "Avisos Registro Descartados"),
    DETALLE_PREDIO(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/detallePredio",
        "Detalle Predio"),
    DILIGENCIA_NOTIFICACION_PERSONAL(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/validacion/diligenciaNotificacionPersonal",
        "Diligencia Notificacion Personal"),
    EDICTO(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/validacion/edicto",
        "Edicto"),
    FICHA_PREDIAL_DIGITAL(
        "/reports/gestioncatastral/interno/productoscartograficos/fichaPredialDigital",
        "Ficha Predial Digital"),
    GENERAR_COMUNICACIONES(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/generacionComunicaciones",
        "Generar Comunicaciones"),
    GESTION_PROYECTO_NOMENCLATURA_VIAL_PROYECTO_ACUERDO(
        "/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/alistamiento/gestionarProyectoNomenclaturaVialProyectoAcuerdo",
        "Gestionar Proyecto Nomenclatura Vial Proyecto Acuerdo"),
    INSTALACION_COMISION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/alistamiento/instalacionComision",
        "Instalacion Comision"),
    INFORMACION_DE_OFERTAS(
        "/reports/gestioncatastral/interno/documentosdetrabajo/avaluos/ofertainmobiliaria/informacionDeOfertas",
        "Informacion De Ofertas"),
    INFORME_VISITA_TERRENO_CON_RESULTADO_LEVANTAMIENTO(
        "/reports/gestioncatastral/interno/documentosdetrabajo/depuracion/informeVisitaTerrenoConResultadoLevantamiento",
        "Informe Visita Terreno Con Resultado Levantamiento"),
    INFORME_LEVANTAMIENTO_TOPOGRAFICO(
        "/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/alistamiento/informeLevantamientoTopografico",
        "informe Levantamiento Topografico"),
    INFORME_SUSTENTANDO_CONCEPTO_DE_AUTOAVALUO_O_REVISION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/informeSustentandoConceptoDeAutoavaluoORevision",
        "Informe Sustentando Concepto De Autoavaluo O Revision"),
    GENERACION_FORMULARIO_SBC(
        "/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/alistamiento/generacionFormularioSBC",
        "Generacion Formulario SBC"),
    GENERACION_FORMULARIO_SBC_EN_BLANCO(
        "/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/alistamiento/generacionformulariossbcBlanco",
        "Generacion formularios sbc Blanco"),
    MEMORANDO_COMISION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/asignacion/memorandoComision",
        "Memorando Comision"),
    MEMORANDO_COMISION_DEPURACION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/asignacion/memorandoComisionDepuracion",
        "Memorando Comisión Depuración"),
    MEMORANDO_COMISION_OFERTAS(
        "/reports/gestioncatastral/interno/documentosdetrabajo/avaluos/ofertainmobiliaria/memorandoComisionOfertas",
        "Memorando Comision Ofertas"),
    MEMORANDO_ESTADO_COMISION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/asignacion/memorandoEstadoComision",
        "Memorando Estado Comision"),
    MEMORANDO_RETIRO_TRAMITE_COMISION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/asignacion/memorandoRetirarTramiteComision",
        "Memorando Retirar Tramite Comision"),
    MODELO_RESOLUCION_CANCELACION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionCancelacion",
        "Resolucion Mutacion Cancelacion"),
    MODELO_RESOLUCION_COMPLEMENTACION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionComplemementacion",
        "Resolucion Complemementacion"),
    MODELO_MODIFICACION_INFORMACION_CATASTRAL(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionModificarInformacionCatastral",
        "Resolucion Modificar Informacion Catastral"),
    MODELO_RESOLUCION_MUTACION_PRIMERA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionPrimera",
        "Resolucion Mutacion Primera"),
    MODELO_RESOLUCION_MUTACION_SEGUNDA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionSegunda",
        "Resolucion Mutacion Segunda"),
    MODELO_RESOLUCION_MUTACION_SEGUNDA_MASIVA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionSegundaMasiva",
        "Resolucion Mutacion Segunda Masiva"),
    MODELO_RESOLUCION_MUTACION_TERCERA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionTercera",
        "Resolucion Mutacion Tercera"),
    MODELO_RESOLUCION_MUTACION_TERCERA_MASIVA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionTerceraMasiva",
        "Resolucion Mutacion Tercera Masiva"),
    MODELO_RESOLUCION_MUTACION_CUARTA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionCuarta",
        "Resolucion Mutacion Cuarta"),
    MODELO_RESOLUCION_MUTACION_QUINTA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionQuinta",
        "Resolucion Mutacion Quinta"),
    MODELO_RESOLUCION_MUTACION_QUINTA_MASIVA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionQuintaMasiva",
        "Resolucion Mutacion Quinta Masiva"),
    MODELO_RESOLUCION_RECTIFICACION(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionRectificacion",
        "Resolucion Rectificacion"),
    MODELO_RESOLUCION_RECTIFICACION_MATRIZ(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionRectificacionMatriz",
        "Resolucion Rectificacion Matriz"),
    MODELO_RESOLUCION_REVISION_AVALUO(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionRevisionAvaluo",
        "Resolucion Revision Avaluo"),
    MODELO_RESOLUCION_VIA_GUBERNATIVA(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionViaGubernativa",
        "Resolucion Vía Gubernativa"),
    MODELO_RESOLUCION_CANCELACION_MASIVA(
            "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionCancelacionMasiva","Resolucion Cancelacion Masiva"),
			
	MODELO_RESOLUCION_MUTACION_SEGUNDA_ENGLOBE_PH(
        "/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionSegundaEnglobePH",
        "Resolucion Mutacion Segunda Englobe PH"),
    //***Modelos de resolución para actualización :: javier.aponte :: 21/09/2015*****
			
	MODELO_RESOLUCION_CANCELACION_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionCancelacionActualizacion","Resolucion Mutacion Cancelacion Actualizacion"),
	MODELO_RESOLUCION_COMPLEMENTACION_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionComplemementacionActualizacion","Resolucion Complemementacion Actualizacion"),
	MODELO_MODIFICACION_INFORMACION_CATASTRAL_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionModificarInformacionCatastralActualizacion","Resolucion Modificar Informacion Catastral Actualizacion"),
	MODELO_RESOLUCION_MUTACION_PRIMERA_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionPrimeraActualizacion","Resolucion Mutacion Primera Actualizacion"),
	MODELO_RESOLUCION_MUTACION_SEGUNDA_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionSegundaActualizacion","Resolucion Mutacion Segunda Actualizacion"),
	MODELO_RESOLUCION_MUTACION_TERCERA_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionTerceraActualizacion","Resolucion Mutacion Tercera Actualizacion"),
	MODELO_RESOLUCION_MUTACION_TERCERA_MASIVA_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionTerceraMasivaActualizacion","Resolucion Mutacion Tercera Másiva Actualizacion"),
	MODELO_RESOLUCION_MUTACION_CUARTA_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionCuartaActualizacion","Resolucion Mutacion Cuarta Actualizacion"),
	MODELO_RESOLUCION_MUTACION_QUINTA_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionMutacionQuintaActualizacion","Resolucion Mutacion Quinta Actualizacion"),
	MODELO_RESOLUCION_RECTIFICACION_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionRectificacionActualizacion","Resolucion Rectificacion Actualizacion"),
	MODELO_RESOLUCION_RECTIFICACION_MATRIZ_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionRectificacionMatrizActualizacion","Resolucion Rectificacion Matriz Actualizacion"),
	MODELO_RESOLUCION_REVISION_AVALUO_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionRevisionAvaluoActualizacion","Resolucion Revision Avaluo Actualizacion"),
	MODELO_RESOLUCION_VIA_GUBERNATIVA_ACTUALIZACION(
					"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/ejecucion/resolucionViaGubernativaActualizacion","Resolucion Vía Gubernativa Actualizacion"),
					
	OFERTAS_INMOBILIARIAS(
			"/reports/gestioncatastral/interno/documentosdetrabajo/avaluos/ofertainmobiliaria/ofertasInmobiliarias","Ofertas Inmobiliarias"),
	OFICIO_NO_PROCEDENCIA_AUTOAVALUO_REVISION_Y_VIA_GUBERNATIVA(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/asignacion/oficioNoProcedenciaAutoavaluoRevisionYViaGubernativa","Oficio No Procedencia Autoavaluo Revision Y Via Gubernativa"),
	OFICIO_NO_PROCEDENCIA_DE_MUTACIONES(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/asignacion/oficioNoProcedenciaDeMutaciones","Oficio No Procedencia De Mutaciones"),
	OFICIO_GESTION_ACUERDO_PERIMETRO_URBANO(
			"/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/alistamiento/gestionarProyectoPerimetroUrbanoFinesCatastralesOficioRemision", "Gestionar Proyecto Perimetro Urbano Fines Catastrales Oficio Remision"),
	PROYECTO_GESTION_ACUERDO_PERIMETRO_URBANO(
			"/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/alistamiento/gestionarProyectoPerimetroUrbanoFinesCatastralesProyectoAcuerdo","Gestionar Proyecto Perimetro Urbano Fines Catastrales Proyecto Acuerdo"),					
	CERTIFICADO_PLANO_PREDIAL_CATASTRAL(
			"/reports/gestioncatastral/interno/productoscartograficos/certificadoPlanoPredialCatastral","Certificado Plano Predial Catastral"),
	CERTIFICADO_PLANO_PREDIAL_CATASTRAL_5(
			"/reports/gestioncatastral/interno/productoscartograficos/certificadoPlanoPredialCatastral5","Certificado Plano Predial Catastral 5"),
    CERTIFICADO_PLANO_PREDIAL_CATASTRAL_8(
			"/reports/gestioncatastral/interno/productoscartograficos/certificadoPlanoPredialCatastral8","Certificado Plano Predial Catastral 8"),
    CERTIFICADO_PLANO_PREDIAL_CATASTRAL_9(
			"/reports/gestioncatastral/interno/productoscartograficos/certificadoPlanoPredialCatastral9","Certificado Plano Predial Catastral 9"),
	RESOLUCION_ACTUALIZACION_FORMACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/resolucionActualizacionFormacion", "Resolucion Actualizacion Formacion"),			
	SOLICITUD_INFO_POT(
			"/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/solicitudInfopot","Solicitud Info Pot"),	
	SOLICITUD_DESPLAZAMIENTO_INCODER(
			"/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/solicitudDesplazamientoincoder","Solicitud Desplazamiento Incoder"),	
	SOLICITUD_DOCUMENTOS_FALTANTES(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/asignacion/solicitudDocumentosFaltantesRevisionAutoavaluoYMutaciones","Solicitud Documentos Faltantes Revision Autoavaluo Y Mutaciones"),
	SOLICITUD_PARQUES_NATURALES(
			"/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/solicitudParquenaturales","Solicitud Parque Naturales"),
	SOLICITUD_PREDIOS_INCODER(
			"/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/solicitudPrediosincoder", "Solicitud Predios Incoder"),	
	SOLICITUD_PREDIOS_OTRAS_ENTIDADES(
			"/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/solicitudPrediosotrasentidades","Solicitud Predios Otras Entidades"),
	SOLICITUD_REGUARDOS_INDIGENAS(
			"/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/planificacion/solicitudResguardosindigenas","Solicitud Resguardos Indigenas"),
			
	EDICTO_ACTUALIZACION(
			"/reports/gestioncatastral/interno/documentosdetrabajo/conservacion/validacion/edictoActualizacion","Edicto Actualizacion"),		

	PLANTILLA("/reports/gestioncatastral/interno/plantilla","Plantilla"),
			
	//Url's de los reportes prediales		
	REPORTES_PREDIALES_DATOS_BASICOS_ESTADISTICA("/reports/gestioncatastral/interno/documentosdetrabajo/prediales/datosBasicos", "Reporte datos básicos"),
	
	//Url's de los registros prediales
	REPORTES_PREDIALES_DATOS_BASICOS_SIN_PROPIETARIO("/reports/gestioncatastral/interno/productoscartograficos/reportesPredialesDatosBasicosSinPropietarios", "Reportes Prediales Datos Basicos Sin Propietarios"),
	REPORTES_PREDIALES_DATOS_BASICOS("/reports/gestioncatastral/interno/productoscartograficos/reportesPredialesDatosBasicos", "Reportes Prediales Datos Basicos Sin Propietarios"),
	REPORTES_PREDIALES_DATOS_CONSTRUCCIONES_CONVENCIONALES("/reports/gestioncatastral/interno/productoscartograficos/reportesDatosConstruccionesConvencionales", "Reportes Datos Construcciones Convencionales"),
	REPORTES_PREDIALES_DATOS_CONSTRUCCIONES_NO_CONVENCIONALES("/reports/gestioncatastral/interno/productoscartograficos/reportesPredialesDatosConstruccionesNoConvencionales", "Reportes Datos Construcciones No Convencionales"),
	REPORTES_PREDIALES_DATOS_ECONOMICOS("/reports/gestioncatastral/interno/productoscartograficos/registrosPredialesDatosEconomicos", "Reportes Prediales Datos Economicos"),
	REPORTES_PREDIALES_DATOS_JURIDICOS("/reports/gestioncatastral/interno/productoscartograficos/registrosPredialesDatosJuridicos", "Reportes Prediales DatosJuridicos"),
	REPORTES_PREDIALES_DATOS_EXTENDIDOS("/reports/gestioncatastral/interno/productoscartograficos/reportesPredialesDatosExtendidos", "Reportes Prediales Datos Extendidos"),
	REPORTES_PREDIALES_DATOS_EXTENDIDOS_SIN_PROPIETARIO("/reports/gestioncatastral/interno/productoscartograficos/reportesPredialesDatosExtendidosSinPropietario", "Reportes Prediales Datos Extendidos Sin Propietario"),
	REPORTES_PREDIALES_DATOS_FISICOS("/reports/gestioncatastral/interno/productoscartograficos/registrosPredialesDatosFisicos", "Reportes Prediales Datos Fisicos"),
	REPORTES_PREDIALES_DATOS_FICHA_MATRIZ("/reports/gestioncatastral/interno/productoscartograficos/reportesFichaMatriz", "Reportes Ficha Matriz"),
	
	//Url's de los registros prediales
	REGISTROS_PREDIALES_DATOS_BASICOS("/reports/gestioncatastral/interno/productoscartograficos/registrosPredialesDatosBasicos", "Registros prediales datos básicos"),
	REGISTROS_PREDIALES_DATOS_BASICOS_SIN_PROPIETARIO("/reports/gestioncatastral/interno/productoscartograficos/registrosPredialesDatosBasicosSinPropietario", "Registros prediales datos básicos sin propietario"),
	REGISTROS_PREDIALES_DATOS_EXTENDIDOS("/reports/gestioncatastral/interno/productoscartograficos/registrosPredialesDatosExtendidos", "Registros prediales datos extendidos"),
			
	//Url's de los reportes estadísticos
	ESTADISTICO_DISTRIBUCION_POR_RANGOS("/reports/gestioncatastral/interno/documentosdetrabajo/estadisticos/distribucionPorRangos", "Distribución Por Rangos"),
	
	ESTADISTICO_PREDIOS_POR_ZONA("/reports/gestioncatastral/interno/documentosdetrabajo/estadisticos/estadisticasPrediosPorZona", "Estadísticas predios por zona"),
	
	ESTADISTICO_GLOBALES_POR_DEPARTAMENTO("/reports/gestioncatastral/interno/documentosdetrabajo/estadisticos/estadisticasGlobalesPorDepartamento", "Estadísticas globales por departamento"),
		
	//****Módelos de reportes de actualizacion express :: 29/12/2015 *** 
	AX_AVALUO_POR_PREDIO("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/listadosActualizacion/avaluoPorPredio", "Avalúo por predio"),
	AX_ESTADISTICA_POR_DESTINO("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/listadosActualizacion/estadisticaPorDestino", "Incremento de avalúo por sector"),
	AX_ESTADISTICA_POR_ZONA_FISICA("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/listadosActualizacion/estadisticaPorZonaFisica", "Diferencia de área por predio"),
	AX_ESTADISTICA_POR_ZONA_GEOECONOMICA("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/listadosActualizacion/estadisticaPorZonaGeoeconomica", "Estadística por Zona Geoeconomica"),
	AX_INCREMENTO_AREA_CONSTRUIDA("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/listadosActualizacion/incrementoAreaConstruida", "Incremento de área construida"),
	AX_INCREMENTO_AVALUO_POR_SECTOR("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/listadosActualizacion/incrementoAvaluoPorSector", "Incremento de avalúo por sector"),
	AX_PREDIO_DIFERENCIA_AREA("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/listadosActualizacion/predioDiferenciaArea", "Diferencia de área por predio"),
	AX_PREDIO_NO_LIQUIDADO("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/listadosActualizacion/prediosNoLiquidados", "Predios no liquidados"),
	AX_PREDIO_POR_USO_CONSTRUCCION("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/listadosActualizacion/prediosPorUsoConstruccion", "Predios por uso construcción"),
	
	//***Reportes de terreno o construcción actualización :: 18/08/2016
	REPORTE_DE_TERRENO("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/reporteDeTerreno", "Reporte de terreno"),
	REPORTE_DE_CONSTRUCCION("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/reporteDeConstruccion", "Reporte de construcción"),
	
	//****Constancia de radicación de solicitud de actualización
	CONSTANCIA_RADICACION_SOLICITUD_ACTUALIZACION("/reports/gestioncatastral/interno/documentosdetrabajo/actualizacion/constanciaDeRadicacion", "Constancia de radicación de solicitud de actualización");
		
	
	private String urlReporte;
	private String nombreReporte;

    private EReporteServiceSNC(String urlReporte, String nombreReporte) {
        this.urlReporte = urlReporte;
        this.nombreReporte = nombreReporte;
    }

    /**
     *
     * @return
     */
    public String getUrlReporte() {
        return this.urlReporte;
    }

    /**
     *
     * @return
     */
    public String getNombreReporte() {
        return this.nombreReporte;
    }
}
