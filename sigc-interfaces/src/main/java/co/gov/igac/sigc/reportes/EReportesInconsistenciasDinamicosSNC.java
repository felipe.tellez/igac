package co.gov.igac.sigc.reportes;

/**
 *
 * @author juan.mendez
 * @author javier.aponte
 * @author javier.barajas
 */
public enum EReportesInconsistenciasDinamicosSNC {

    INCONSISTENCIA_FALTA_PROPIETARIOS(2L),
    INCONSISTENCIA_NUMERO_PREDIAL_REPETIDO(76L),
    INCONSISTENCIA_NOMBRE_CEDULA(77L),
    INCONSISTENCIA_CEDULA_NOMBRE(78L),
    INCONSISTENCIA_PROPIETARIOS_SIN_CEDULA(79L),
    INCONSISTENCIA_PROPIETARIOS_SIN_NUMERO_REGISTRO(80L),
    INCONSISTENCIA_LISTA_PREDIOS_SIN_NOMBRE(81L),
    INCONSISTENCIA_LISTA_PREDIOS_VIGENCIA(82L),
    INCONSISTENCIA_LISTA_AVALUO_CATASTRAL(83L),
    INCONSISTENCIA_LISTA_MEJORA(84L),
    INCONSISTENCIA_LISTA_DESTINO_ECONOMICO(85L),
    INCONSISTENCIA_LISTA_DIRECCION(86L),
    INCONSISTENCIA_LISTA_GEOGRAFICA_PREDIOS_SIN_REPRESENTACION(87L),
    INCONSISTENCIA_LISTA_GEOGRAFICA_PREDIOS_SIN_DATOS(88L),
    INCONSISTENCIA_LISTA_PUNTAJE0_ACONSTDIF0_USOCONVENCIONAL(89L),
    INCONSISTENCIA_LISTA_PUNTAJEDIF0_ACONST0_USONOCONVENCIONAL(90L),
    INCONSISTENCIA_LISTA_PUNTAJEDIF0_ACONST0(91L);

    private Long idConfiguracionReporte;

    private EReportesInconsistenciasDinamicosSNC(Long idConfiguracionReporte) {
        this.idConfiguracionReporte = idConfiguracionReporte;
    }

    /**
     * Método que devuelve el id de configuración de reporte
     *
     * @return
     */
    public Long getIdConfiguracionReporte() {
        return idConfiguracionReporte;
    }

}
