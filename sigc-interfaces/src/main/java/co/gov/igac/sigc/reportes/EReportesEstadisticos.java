package co.gov.igac.sigc.reportes;

/**
 * Enumeración que contiene los id's de los reportes estadísticos que están en la tabla dominio
 *
 * @author javier.aponte
 *
 */
public enum EReportesEstadisticos {

    ESTADISTICAS_MANZANAS_A_NIVEL_DE_SECTOR(1L, null),
    DISTRIBUCION_POR_RANGOS(2L, EReporteServiceSNC.ESTADISTICO_DISTRIBUCION_POR_RANGOS),
    PREDIOS_POR_ZONA(3L, EReporteServiceSNC.ESTADISTICO_PREDIOS_POR_ZONA),
    GLOBALES_POR_DEPARTAMENTO(4L, EReporteServiceSNC.ESTADISTICO_PREDIOS_POR_ZONA);

    private Long codigo;

    private EReporteServiceSNC reporte;

    private EReportesEstadisticos(Long codigo, EReporteServiceSNC reporteAGenerar) {
        this.codigo = codigo;
        this.reporte = reporteAGenerar;
    }

    /**
     * Método que devuelve el código del reporte
     *
     * @return
     */
    public Long getCodigo() {
        return codigo;
    }

    /**
     * Método que devuelve el reporte a generar
     */
    public EReporteServiceSNC getReporte() {
        return reporte;
    }

}
