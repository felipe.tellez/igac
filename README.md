<h1>SNC &ndash; Sistema Nacional Catastral</h1>
<p>&nbsp;</p>
<h2>Introducci&oacute;n</h2>
<p>El Sistema Nacional Catastral se define como el instrumento que permite producir, analizar y divulgar informaci&oacute;n obtenida de los procesos catastrales y de aval&uacute;os para apoyar la administraci&oacute;n y el mercado eficiente de la tierra, la protecci&oacute;n jur&iacute;dica de la propiedad y facilitar el intercambio de informaci&oacute;n con las diferentes entidades del estado.</p>
<p>El objetivo del Sistema de Informaci&oacute;n SNC es contar con una plataforma tecnol&oacute;gica, bajo est&aacute;ndares de calidad que permitan prestar un servicio oportuno y seguro a las diferentes entidades del estado y al ciudadano.</p>
<p>&nbsp;</p>
<h2>Caracter&iacute;sticas</h2>
<p>&nbsp;</p>
<p>Las caracter&iacute;sticas del Sistema de Informaci&oacute;n SNC, las cuales se desagregan en 4 niveles, as&iacute;:</p>
<h3>Tecnol&oacute;gico</h3>
<ul>
<li>Manejo centralizado</li>
<li>Servidores en alta disponibilidad</li>
<li>Control de costos de plataforma.</li>
<li>Integraci&oacute;n componentes de manejo de procesos, datos geogr&aacute;ficos y elementos documentales</li>
</ul>
<h3>Informaci&oacute;n</h3>
<ul>
<li>Cohesi&oacute;n entre el dato geogr&aacute;fico y alfanum&eacute;rico.</li>
<li>Manejo hist&oacute;rico de los datos catastrales, geogr&aacute;ficos y descriptivos.</li>
<li>Modelo de datos geogr&aacute;fico unificado y continuo</li>
</ul>
<h3>Institucional</h3>
<ul>
<li>Cumplimiento a cabalidad de los objetivos misionales institucionales.</li>
<li>Instrumento de gesti&oacute;n Catastral.</li>
<li>Conformidad con la resoluci&oacute;n 070</li>
</ul>
<h3>Usuario</h3>
<ul>
<li>Informaci&oacute;n en l&iacute;nea</li>
<li>Tr&aacute;mites y productos v&iacute;a Web</li>
<li>Seguimiento del tr&aacute;mite catastral en l&iacute;nea</li>
<li>Servicios interinstitucionales &ndash; IPER</li>
</ul>
<h2>Componentes SNC -JEE</h2>
<p>Los componentes que conforman el SNC-JEE (Com&uacute;nmente denominado &ldquo;Web&rdquo;), que se aloja como proyecto en este repositorio, son los siguientes:</p>
<table style="height: 271px;" width="680">
<tbody>
<tr>
<td width="294">
<p>snc-jee&nbsp;&nbsp;</p>
</td>
<td width="499">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="294">
<p>&nbsp;&nbsp; docs</p>
</td>
<td width="499">
<p>Documentaci&oacute;n general</p>
</td>
</tr>
<tr>
<td width="294">
<p>&nbsp;&nbsp; sigc-ear</p>
</td>
<td width="499">
<p>Proyecto que empaqueta y los dem&aacute;s proyectos del SNC-JEE</p>
</td>
</tr>
<tr>
<td width="294">
<p>&nbsp;&nbsp; sigc-ejb-business-integration-test</p>
</td>
<td width="499">
<p>En este proyecto se realizan las diferentes prueba de integraci&oacute;n</p>
</td>
</tr>
<tr>
<td width="294">
<p>&nbsp;&nbsp; sigc-ejb-business</p>
</td>
<td width="499">
<p>Proyecto que contiene programadas todas las reglas de negocio del SNC</p>
</td>
</tr>
<tr>
<td width="294">
<p>&nbsp;&nbsp; sigc-interfaces</p>
</td>
<td width="499">
<p>Interfaces para comunicar para ser usadas por los diferentes proyectos.</p>
</td>
</tr>
<tr>
<td width="294">
<p>&nbsp;&nbsp; sigc-web-public</p>
</td>
<td width="499">
<p>Capa de presentaci&oacute;n WEB del SNC</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>M&aacute;s Informaci&oacute;n de SNC-JEE</p>
<p>En el siguiente enlace a la Wiki, hay informaci&oacute;n m&aacute;s detallada del proyecto general:</p>
<p><a href="http://redmine.igac.gov.co/projects/igacsigc/wiki">SNC Wiki</a></p>
<p>Bajo la secci&oacute;n &ldquo;Aspectos T&eacute;cnicos&rdquo; encuentra toda la informaci&oacute;n t&eacute;cnica del proyecto, Arquitectura, Componentes de desarrollo, Manuales, Tutoriales, entre otros.</p>